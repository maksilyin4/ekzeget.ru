> Ветки:

- develop - ветка для разработки https://front.dev.ekzeget.ladcloud.ru/
- test - ветка для тестов https://test.ekzeget.ru/
- master - ветка для прода https://ekzeget.ru/

> Как развернуть проект локально:

- `nvm use 14.15.3`
- `npm i`
- `npm run ssr:dev`
  Проект запускается с помощью пакета scripts-run-os, он помогает устанавливать переменные окружения под разные системы (windows, mac или linux)

Если не получается запустить с помощью npm, можно попробовать:

- `yarn i`
- `yarn run ssr:dev`

Управлять сменой контейнеров для выкатки на контур (пока не настроили CI) можно с помощью расширения (для vscode) [Remote - SSH](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh)

> Далее инструкция:

1. Установить расширение, должна появится вкладка (телевизор), кликнуть на нее.
2. Далее в шапке нажать + и тогда появится popup где нужно ввести ssh (уточнить логин и пароль у менеджера или техлида). На данный момент есть два контура, это [прод](https://ekzeget.ru/) и [тестовый](https://test.ekzeget.ru/). Каждый из них нужно завести и подключится.
3. В качестве конфига указать свой (первый в списке должен быть)
4. Как только появится структура папок, нужно перейти в папку фронта и переключится на нужную ветку (сейчас мы уже на удаленном сервере и можем косякнуть).
5. Затем ветку нужно подтянуть, чтобы обновить изменения на сервере. С помощью `git pull origin/test`
6. Изменения подтянулись, теперь нужно выполнить команды для деплоя:

- `npm i`
- `npm run build`
- `npm run start:dev`

7. `pm2 show` покажет что нода запущена, удалить или перезапустить контейнер можно так же с ее помощью

> Обновление environment ([https://test.ekzeget.ru/](https://test.ekzeget.ru/) и [https://ekzeget.ru/](https://ekzeget.ru/))
* Папка проекта на [https://ekzeget.ru/](https://ekzeget.ru/) - `ekzeget_node_frontend`
* Папка проекта на [https://test.ekzeget.ru/](https://test.ekzeget.ru/) - `ekzeget_front`

1. Подключиться по ssh:
* `ssh ekzeget@test.ekzeget.ru` для test 
* `ssh ekzeget@ekzeget.ru` для prod
2. (Опционально) Делаем backup:
* `rm -rf ekzeget_frontend_backup` удаляем старый backup
* `cp -R ekzeget_frontend ekzeget_frontend_backup` копируем текущий фронт в бэкап
3. Переходим в папку с фронтом, переключаемся на нужную ветку либо обновляем текущую
4. Выполняем `npm run clean` и `npm run build`
5. `pm2 list` для получения списка запущенных процессов `pm2`, ищем там `ssr-server-node`
   и берем из таблицы его `id`, который потом подставляем в `pm2 delete ${id}`
6. Для запуска frontend-процесса выполняем `npm run start:dev` (или `npm run start:prod` для прода)
   и `pm2 save` для сохранения конфигурации на случай перезагрузки
