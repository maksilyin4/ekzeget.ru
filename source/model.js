import { createAtom } from '@reatom/core';

export const ssrReAtomAtom = createAtom(
  {},
  (_, state = process.env.BROWSER ? window?.INIT_STATE_REATOM || {} : {}) => {
    return state;
  },
);
