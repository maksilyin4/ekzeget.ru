import {useState} from 'react'

export const useChangeActive = (initialState) => {
  const [stateChange, setStateChange] = useState(initialState)
  const handleChange = (value) => {
    if(stateChange.includes(value)) {
      setStateChange(stateChange.filter(val => val !== value))
    } else {
      setStateChange([...stateChange, value])
    }
  }

  return {
    handleChange,
    stateChange
  }
}
