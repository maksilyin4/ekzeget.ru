import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

import { getMediaViews } from '../store/mediateka/action';

export const useMediaViews = mediaId => {
  const dispatch = useDispatch();

  const [isMediaViewsFetching, setMediaViewsFetchingStatus] = useState(true);

  useEffect(() => {
    if (mediaId) {
      dispatch(getMediaViews(mediaId));
    }
    setMediaViewsFetchingStatus(false);
  }, [mediaId]);

  return isMediaViewsFetching;
};
