import { useLocation } from 'react-router-dom';

export function useGetParams() {
  const { search } = useLocation();
  const query = new URLSearchParams(search);

  const state = {};
  // eslint-disable-next-line no-restricted-syntax
  for (const [key, value] of query.entries()) {
    if (key === 'book_ids') {
      state[key] = value.split(',').map(Number);
    } else {
      state[key] = +value;
    }
  }

  if (Object.keys(state).length) {
    return state;
  }

  return null;
}
