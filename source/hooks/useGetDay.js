import { useParams } from 'react-router-dom/cjs/react-router-dom.min';

export default function useGetDayParams() {
  const { day } = useParams();
  return day?.replace('day-', '') || '';
}
