import { useDispatch, useSelector } from 'react-redux';
import { userPlans } from '~dist/selectors/ReadingPlan';
import readingPlanActions, { getUserReadingPlan } from '../dist/actions/ReadingPlan';
import { planDetail } from '../dist/selectors/ReadingPlan';
import { useEffect, useMemo } from 'react';
import { findCurrentReadingDay } from '../components/CustomPlanModule/utils/getActualCurrentDay';

const { getReadingPlanDetail } = readingPlanActions;

export default function useProgressPlan() {
  const userPlan = useSelector(userPlans);
  const detailPlan = useSelector(planDetail);
  const dispatch = useDispatch();
  const planId = Object.keys(userPlan)?.[0];
  const closedPlans = userPlan?.[planId]?.closedPlans;
  const currentPlanDay = findCurrentReadingDay(userPlan?.[planId]);

  useEffect(() => {
    if (!userPlan) {
      dispatch(getUserReadingPlan());
    }
  }, []);

  useEffect(() => {
    if (userPlan && planId) {
      dispatch(getReadingPlanDetail(planId));
    }
  }, [userPlan]);

  const progress = useMemo(() => {
    if (closedPlans && (currentPlanDay || currentPlanDay === 0) && detailPlan && userPlan) {
      const variants = Object.entries(detailPlan).map(([key, value]) => {
        if (value.every(plan => closedPlans.some(val => val.id === plan.plan_id))) {
          return {
            day: Number(key),
            variant: 'pass',
          };
        }
        if (Number(key) < currentPlanDay) {
          return {
            day: Number(key),
            variant: 'remaind',
          };
        }
        return {
          day: Number(key),
          variant: 'default',
        };
      });
      return variants;
    }
    return [];
  }, [closedPlans, currentPlanDay, userPlan, detailPlan]);
  const closedDays = progress.filter(v => v.variant === 'pass').map(d => d.day);
  const remainderDays = progress.filter(v => v.variant === 'remaind').map(d => d.day);
  const allDays =
    progress.filter(v => v.variant === 'default').map(d => d.day).length +
    closedDays.length +
    remainderDays.length;

  return {
    closedDays,
    remainderDays,
    allDays,
  };
}
