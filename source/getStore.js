import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import ReduxThunk from 'redux-thunk';
import rootReducer from './dist/reducers/RootReducer';

const initialStore = (process.env.BROWSER && window.REDUX_INITIAL_STORE) || {};
export const initialState = (process.env.BROWSER && window.DIRECT_PROPS) || {};
export const urlApollo = (process.env.BROWSER && window.URL_APOLLO) || '';
export const baseUlr = (process.env.BROWSER && window.URL_APOLLO) || '';
export const storageCDN = (process.env.BROWSER && window.STORAGE_CDN) || '';

// вернуть, если вернемся к доработке фронта
const middlewares = [ReduxThunk];
// const middlewares = [ReduxThunk];
const { NODE_ENV } = process.env;

export const getStore = (rootReducer, initialStore, middlewares) => {
  const middleware =
    NODE_ENV === 'development'
      ? composeWithDevTools(applyMiddleware(...middlewares))
      : applyMiddleware(...middlewares);

  // вернуть, если снова потребуется logger
  // const middleware = applyMiddleware(...middlewares);

  const store = createStore(rootReducer, initialStore, middleware);

  if (module.hot) {
    module.hot.accept('./dist/reducers/RootReducer', () => {
      // eslint-disable-next-line global-require
      const nextRootReducer = require('./dist/reducers/RootReducer');
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
};

export const store = getStore(rootReducer, initialStore, middlewares);
