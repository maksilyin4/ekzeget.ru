import { createContext } from 'react';

export const MediaContext = createContext({});
export const MediaBodyContext = createContext({});
