import React from 'react';
import { ApolloProvider } from '@apollo/react-hooks';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router-dom';
import { CookiesProvider } from 'react-cookie';
import Loadable from 'react-loadable';

import Main from './components/Main/Main';

const modules = [];
const context = {};

const getApp = (url, store, directProps, client, cookies) => () => (
  <Loadable.Capture report={moduleName => modules.push(moduleName)}>
    <ApolloProvider client={client || {}}>
      <Provider store={store}>
        <CookiesProvider cookies={cookies}>
          <StaticRouter location={url || '/'} context={context}>
            <Main directProps={directProps} />
          </StaticRouter>
        </CookiesProvider>
      </Provider>
    </ApolloProvider>
  </Loadable.Capture>
);

export default getApp;
