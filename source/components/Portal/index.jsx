import React, { useEffect } from 'react';
import { Portal } from 'react-portal';
import cx from 'classnames';

import Icon from '../Icons/Icons';

import './style';

const PortalPage = ({ children, className = '', closePortal = f => f }) => {
  const handleClickPortal = ({ target }) => {
    if (typeof target.className === 'string' && target.className.includes('default-portal')) {
      closePortal();
    }
  };

  useEffect(() => {
    window.addEventListener('click', handleClickPortal);
    return () => {
      window.removeEventListener('click', handleClickPortal);
    };
  }, []);
  return (
    <Portal>
      <div className={cx('portal-container default-portal', { className })}>
        <button className="portal-close" onClick={closePortal}>
          <Icon icon="close" />
        </button>
        <div className="portal-content">{children}</div>
      </div>
    </Portal>
  );
};

export default PortalPage;
