import React from 'react';

export default function HTMLCommentRender({ comment }) {
  const html = `<!--${comment}-->`;
  const callback = instance => {
    if (instance&&instance.ParentNode) {
      instance.outerHTML = html;
    }
  };
  
  //return <script ref={callback} dangerouslySetInnerHTML={{ __html: html }} />;
  return <script ref={callback} dangerouslySetInnerHTML={{ __html: `</script>${html}<script>` }} />;
}
