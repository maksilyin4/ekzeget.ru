import React from 'react';
import Loadable from 'react-loadable';
import Preloader from '../Preloader/Preloader';

const LoadableBar = Loadable({
  loader: () => import('./ReadingGospelAudio'),
  loading() {
    return (
      <div className="preloader-relative">
        <Preloader />
      </div>
    );
  },
  delay: 300,
});

export default LoadableBar;
