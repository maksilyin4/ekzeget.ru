import React from 'react';
import LazyLoad from 'react-lazyload';
import AudioPlayer from '../AudioPlayer/AudioPlayer';

import { bibleCDN } from '~utils/common';

import './readingGospelAudio.scss';

const ReadingGospelAudio = () => (
  <div className="paper__rga">
    <div className="rga">
      <div className="rga__head">
        <h2 className="rga__title">
          Молитва перед чтением Евангелия. <span>Читает диакон Сергий Нежборт.</span>
        </h2>
      </div>
      <div className="rga__body">
        <LazyLoad height={120}>
          <AudioPlayer src={`${bibleCDN}molitva.mp3`} srcToDownload={`${bibleCDN}molitva.mp3`} />
        </LazyLoad>
      </div>
    </div>
  </div>
);

export default ReadingGospelAudio;
