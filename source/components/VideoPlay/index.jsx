import React, { memo } from 'react';
import cx from 'classnames';
//import { STORAGE_CDN } from '../../ecosystem.config';
import './videoPlay.scss';

import { youtubeParser } from './utilsVideoPlay';

const VideoMicrodata = ({
    description,
    length,
    path,
    preview,
    author,
    title,
    uploadTime,
}) => (
    <div className="video-schema-meta">
        <meta itemProp="description" content={description} />
        <meta itemProp="duration" content={length} />
        <link itemProp="url" href={path} />
        <link itemProp="thumbnailUrl" href={`${STORAGE_CDN}${preview}`} />
        <meta itemProp="name" content={`${author} ${title}`} />
        <meta itemProp="uploadDate" content={uploadTime} />
        <meta itemProp="isFamilyFriendly" content="true" />
        <span
            className="video-schema__hidden"
            itemProp="thumbnail"
            itemScope
            itemType="http://schema.org/ImageObject"
        >
            <img
                itemProp="contentUrl"
                src={preview}
                alt={`${author} ${title}`}
            />
            <meta itemProp="width" content="300px" />
            <meta itemProp="height" content="300px" />
        </span>
    </div>
);

const EmbeddedIframePlayer = ({ path = '', title = '', wrapperClass = '' }) => (
    <iframe
        title={title}
        className={cx('video-play-iframe', wrapperClass)}
        src={path}
        allowFullScreen
        frameBorder="0"
        allow="autoplay; encrypted-media; fullscreen; picture-in-picture"
        loading="lazy"
    />
);

const Html5Player = ({ preview = '', path = '' }) => (
    <video
        src={path}
        width="100%"
        poster={`${STORAGE_CDN}${preview}`}
        controls
    />
);
const VideoPlay = memo((props) => {
    let ConcretePlayer;
    let path = props.path;

    if (props.path.includes('youtube.com')) {
        path = `https://www.youtube.com/embed/${youtubeParser(props.path)}`;
        ConcretePlayer = EmbeddedIframePlayer;
    } else if (props.path.includes('vk.com')) {
        ConcretePlayer = EmbeddedIframePlayer;
    } else {
        ConcretePlayer = Html5Player;
    }

    return (
        <div className={cx('video-play-container', props.className)}>
            <div itemScope itemType="http://schema.org/VideoObject">
                <VideoMicrodata {...props} path={path} />
                <ConcretePlayer {...props} path={path} />
            </div>
        </div>
    );
});

VideoPlay.propTypes = {
    preview: '',
    uploadTime: '',
    length: 0,
    description: '',
    path: '',
    author: '',
    title: '',
    wrapperClass: '',
    className: '',
};

export { VideoPlay };
