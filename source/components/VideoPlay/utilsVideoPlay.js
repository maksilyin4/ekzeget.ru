export const youtubeParser = url => {
  const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
  const match = url.match(regExp);

  if (match) {
    const matchFilter = match.filter(el => el);
    const index = matchFilter.length - 1;

    return matchFilter[index].length === 11 ? matchFilter[index] : false;
  }
  return false;
};
