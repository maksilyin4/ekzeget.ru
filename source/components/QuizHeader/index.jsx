import R from 'ramda';
import React, { useState, useEffect } from 'react';
import cx from 'classnames';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import { Popup } from '../../dist/browserUtils';
import { getTutorial } from '../../dist/quizAPI';
import Breadcrumbs from '../Breadcrumbs/Breadcrumbs';
import { getIsMobile } from '../../dist/selectors/screen';
import { getCurrentQuestionData, getQuestions } from '../../dist/selectors/Quiz';
import { DEFAULT_URL_TO_ALL_QUESTIONS } from '../../dist/utils';

const maxWordsForTitle = 4;

const QuizHeader = ({ locationState = {}, pathname = '' }) => {
  const isMobile = useSelector(getIsMobile);
  const questions = useSelector(getQuestions);
  const currentQuestion = useSelector(getCurrentQuestionData);

  const [breadArr, updateBreadArr] = useState([
    { path: '/', title: 'Главная' },
    { path: '', title: 'Викторина' },
  ]);
  const [h_one] = useState('Библейская викторина');
  const [tutorialData, setStateTutorial] = useState({});
  const [isQuizMainPage, setStatusQuizMainPage] = useState(false);

  useEffect(() => {
    getTutorial().then(data => setStateTutorial(data));
  }, []);

  useEffect(() => {
    const questionPathname = pathname.includes('/voprosi/');
    const resultsPathname = pathname.includes('/resultati');

    if (questionPathname) {
      const {
        questionId,
        prevUrlAllQuestions = DEFAULT_URL_TO_ALL_QUESTIONS,
        isUniqQuestion,
      } = locationState;
      // Крошка будет содержать тайтл вопроса, если вопрос не зарег у пользователя, СЕО
      // или открыта через Все вопросы
      let newTitle = R.pathOr('Вопрос Викторины', ['question', 'title'], currentQuestion);

      if (!questions.length || isUniqQuestion) {
        newTitle = `${newTitle
          .split(' ')
          .slice(0, maxWordsForTitle)
          .join(' ')}...`;
      } else {
        newTitle = `Вопрос ${questionId}`;
      }

      updateBreadArr([
        { path: '/', title: 'Главная' },
        { path: '/bibleyskaya-viktorina/', title: 'Викторина' },
        { path: prevUrlAllQuestions, title: 'Все вопросы' },
        { path: '', title: newTitle },
      ]);
    } else if (resultsPathname) {
      updateBreadArr([
        { path: '/', title: 'Главная' },
        { path: '/bibleyskaya-viktorina/', title: 'Викторина' },
        { path: '', title: `Результаты` },
      ]);
    } else {
      updateBreadArr([
        { path: '/', title: 'Главная' },
        { path: '', title: 'Викторина' },
      ]);
    }

    const isMainPage = !(questionPathname || resultsPathname);
    setStatusQuizMainPage(isMainPage);
  }, [currentQuestion, locationState, pathname, questions]);

  const openPopup = () => {
    Popup.create(
      {
        title: null,
        content: (
          <iframe
            title="Обучающее видео Библейской викторины"
            src={tutorialData.url}
            frameBorder="0"
            allowFullScreen
            className="quiz-video"
          />
        ),
        className: 'chapter__manuscript-popup quiz-video',
      },
      true,
    );
  };

  return (
    <div className="quiz-page__header">
      <div className="wrapper">
        <div
          className={cx('quiz-page__header-items', {
            'quiz-page__header-quiz-list': !isQuizMainPage,
          })}>
          <Breadcrumbs data={breadArr} />
          <div className="page__head page__head_two-col quiz-title">
            <h1 className="page__title">{h_one}</h1>
          </div>
          {!isQuizMainPage && !isMobile && (
            <button className="btn quiz-btn" onClick={openPopup}>
              Обучающее видео
            </button>
          )}
        </div>
        <div className="quiz-faq">
          {isQuizMainPage && (
            <button className="btn quiz-btn" onClick={openPopup}>
              Обучающее видео
            </button>
          )}
          <Link
            to="/bibleyskaya-viktorina/resultati/"
            className={cx('quiz-stats quiz-stats__link', {
              'quiz-page__hide-stats-link': !isQuizMainPage,
            })}>
            <div className="quiz-stats__icon" />
            <span className="quiz-stats__text"> Статистика </span>
          </Link>
        </div>
      </div>
    </div>
  );
};

QuizHeader.propTypes = {
  locationState: PropTypes.shape({
    questionTitle: PropTypes.string,
  }),
  pathname: PropTypes.string,
};

export default QuizHeader;
