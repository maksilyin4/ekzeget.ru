import React from 'react';
import { Link } from 'react-router-dom';

import Preloader from '../Preloader/Preloader';

import './breadcrumbs.scss';

const Breadcrumbs = ({ data, isBread = false }) => {
  return (
    <nav className="breadcrumbs" itemScope itemType="http://schema.org/BreadcrumbList">
      {!isBread ? (
        data.map((item, i) => {
          return item.path !== '' ? (
            <div
              className="breadcrumbs__item"
              key={item.title + i}
              //    itemProp="itemListElement"
              itemScope
              itemType="http://schema.org/ListItem">
              <Link
                to={item.path}
                className="breadcrumbs__item-link"
                title={String(item.title)}
                itemProp="item"
                onMouseEnter={item.onMouseEnter}
                onMouseLeave={item.onMouseLeave}
                onFocus={item.getMouseObj}>
                <span itemProp="name">{item.title}</span>
                <meta itemProp="position" content={i + 1} />
              </Link>
            </div>
          ) : (
            <div
              className="breadcrumbs__item"
              /* eslint-disable-next-line react/no-array-index-key */
              key={item.title + i}
              //     itemProp="itemListElement"
              itemScope
              itemType="http://schema.org/ListItem">
              <div className="breadcrumbs__item breadcrumbs__item-link" itemProp="item">
                <span itemProp="name">{item.title}</span>
                <meta itemProp="position" content={i + 1} />
              </div>
            </div>
          );
        })
      ) : (
        <Preloader />
      )}
    </nav>
  );
};
export default Breadcrumbs;
