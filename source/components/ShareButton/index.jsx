import React, { memo, useEffect, useReducer, useRef } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import cx from 'classnames';
import nanoid from 'nanoid';

import screen from '../../dist/selectors/screen';
import { reducer, initialState, types } from './reducer';

import Icon from '../Icons/Icons';
import ShareList from './ShareList';

import { setPositions, removeListener } from './utilsShareButton';

import './style.scss';

const ShareButton = ({ className = '', isNotShowText, urlShare, text = [], verseInfo }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const popupId = nanoid();
  const popupShare = `popup-share-${popupId}`;

  const refIcon = useRef(null);
  const refListShare = useRef(null);

  const { phone } = useSelector(screen);

  const refreshPosition = () => {
    const { targetsState } = state;

    const [top, left, right] = setPositions({ target: targetsState, refIcon, phone, refListShare });

    dispatch({ type: types.SET_POSITION, payload: { top, left, right } });
  };

  const closePopover = e => {
    if (!e.target.classList.contains(popupShare)) {
      dispatch({ type: types.SET_IS_OPEN, payload: false });
      removeListener(closePopover, refreshPosition);
    }
  };

  useEffect(() => {
    if (state.isOpen) {
      window.addEventListener('click', closePopover);
      window.addEventListener('scroll', refreshPosition);
    }
    return () => {
      removeListener(closePopover, refreshPosition);
    };
  }, [state.isOpen]);

  const openPopover = e => {
    const target = e.currentTarget;
    const [top, left, right] = setPositions({ target, refIcon, phone, refListShare });

    dispatch({
      type: types.SET_CHANGE_ALL,
      payload: { position: { top, left, right }, targetsState: target, isOpen: true },
    });
  };

  const {
    position: { top, left, right },
    isOpen,
  } = state;

  return (
    <div className={cx('share-social', className, { isActive: isOpen })} onClick={openPopover}>
      <div className={cx('share-social-icon', popupShare)} ref={refIcon}>
        <Icon icon="shares" type="shares" />
      </div>
      {!isNotShowText && <p className="chapter__verses-action__text">Поделиться</p>}

      <div className={cx('shares-popup', popupShare, { isOpen })}>
        <div
          ref={refListShare}
          className={cx('shares-box', popupShare)}
          style={{ top, left, right }}>
          <ShareList
            verseInfo={verseInfo}
            urlShare={urlShare || (process.env.BROWSER && window.location.href)}
            text={text}
          />
        </div>
      </div>
    </div>
  );
};

ShareButton.propTypes = {
  className: PropTypes.string,
  isNotShowText: PropTypes.bool,
  urlShare: PropTypes.string,
  text: PropTypes.arrayOf(PropTypes.string),
};

export default memo(ShareButton);
