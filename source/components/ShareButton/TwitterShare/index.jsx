import React from 'react';

// twitter не дает отправить больше чем 255 символов
const maxCountForComment = 255;

const TwitterShare = ({ url, children, className, text = '', isAdaptive }) => {
  const handleClick = () => {
    const description = text
      ? text
          .split('\n')
          .map(t => `${t} `)
          .join('')
      : ' ';

    const comment =
      description.length > maxCountForComment
        ? `${description.substring(0, maxCountForComment)}...`
        : description;

    const linkShare = isAdaptive
      ? `https://mobile.twitter.com/intent/tweet?text=${comment}&url=${url}`
      : `https://twitter.com/intent/tweet?text=${comment}&url=${url}`;
    window.open(linkShare, '_blank');
  };
  return (
    <button onClick={handleClick} className={className}>
      {children}
    </button>
  );
};

export default TwitterShare;
