const initialState = {
  targetsState: {},
  position: { top: 0, left: 0, right: 'auto' },
  isOpen: false,
};

const types = {
  SET_POSITION: 'SET_POSITION',
  SET_CHANGE_ALL: 'SET_CHANGE_ALL',
  SET_IS_OPEN: 'SET_IS_OPEN',
};

const reducer = (state, action) => {
  switch (action.type) {
    case types.SET_POSITION:
      return { ...state, position: action.payload };
    case types.SET_CHANGE_ALL:
      return { ...state, ...action.payload };
    case types.SET_IS_OPEN:
      return { ...state, isOpen: action.payload };
    default:
      return state;
  }
};

export { initialState, reducer, types };
