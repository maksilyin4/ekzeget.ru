import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';

import { getIsAdaptive } from '../../../dist/selectors/screen';

import Icon from '../../Icons/Icons';
import { setDataShare, setTextForShare } from './utilsShare';

const ShareList = ({ urlShare, text = [], verseInfo }) => {
  const isAdaptive = useSelector(store => getIsAdaptive(store));
  const url = urlShare?.includes('bible')
    ? `${urlShare}?verse=${verseInfo && verseInfo[0] ? verseInfo[0].verseNumber : 1}`
    : urlShare;
  const data = useMemo(() => setDataShare(isAdaptive), [isAdaptive]);
  const textSend = text.sort((a, b) =>
    a.localeCompare(b, undefined, { numeric: true, sensitivity: 'base' }),
  );
  const textShare = useMemo(() => setTextForShare(textSend), [text]);
  return (
    <div className="share-list">
      <div className="share-content">
        {data.map(({ icon, ComponentShare, title }) => (
          <ComponentShare
            key={icon}
            url={url}
            text={textShare}
            quote={textShare}
            via={textShare}
            isAdaptive={isAdaptive}
            description={textShare}
            className="share-content-item">
            <Icon icon={icon} title={title} />
          </ComponentShare>
        ))}
      </div>
    </div>
  );
};

export default ShareList;
