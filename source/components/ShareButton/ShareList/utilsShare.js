import { OKShareButton, FacebookShareButton } from 'react-share';

import TelegramShare from '../TelegramShare/';
import VKShare from '../VKShare/';
import TwitterShare from '../TwitterShare/';
import ViberShare from '../ViberShare/';
import WhatsappShare from '../WhatsappShare/';

const socialShare = [
  {
    icon: 'vk',
    ComponentShare: VKShare,
    title: 'Vk',
    isDesktop: true,
  },
  {
    icon: 'facebook',
    ComponentShare: FacebookShareButton,
    title: 'Facebook',
    isDesktop: true,
  },
  {
    icon: 'twitter',
    ComponentShare: TwitterShare,
    title: 'Twitter',
    isDesktop: true,
  },
  {
    icon: 'ok-social',
    ComponentShare: OKShareButton,
    title: 'Ok',
    isDesktop: true,
  },
  {
    icon: 'whatsapp',
    ComponentShare: WhatsappShare,
    title: 'WhatsApp',
  },
  {
    icon: 'viber',
    ComponentShare: ViberShare,
    title: 'Viber',
  },
  {
    icon: 'telegram',
    ComponentShare: TelegramShare,
    title: 'Telegram',
    isDesktop: true,
  },
];

export const setTextForShare = (text = []) =>
  text && Array.isArray(text) ? text.map(txt => `${txt} \n`).join('') : text;

export const setDataShare = isAdaptive =>
  isAdaptive ? socialShare : socialShare.filter(({ isDesktop }) => isDesktop);
