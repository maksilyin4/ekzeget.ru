import React from 'react';

const TelegramShare = ({ url, children, className, text }) => {
  return (
    <a
      className={className}
      href={`tg://msg_url?url=${url}&text=${text}`}>
      {children}
    </a>
  );
};

export default TelegramShare;
