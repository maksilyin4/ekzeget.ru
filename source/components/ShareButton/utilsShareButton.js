const setLeftValue = ({ phone, isCheckSize, leftValue, widthSharePopup, widthMax }) => {
  if (phone) {
    return 'auto';
  }
  return isCheckSize ? widthMax - widthSharePopup - 20 : leftValue;
};

export const setPositions = ({ target, refIcon, phone, refListShare }) => {
  const { height } = refIcon.current?.getBoundingClientRect() ?? {};
  const { width: widthSharePopup } = refListShare.current?.getBoundingClientRect() ?? {};
  const widthMax = document.documentElement.clientWidth;

  const { top, left } = target.getBoundingClientRect();

  const topPadding = phone ? 25 : 30;
  const leftPadding = 50;

  const topValue = Math.round(top - height - topPadding);
  const leftValue = Math.round(left - leftPadding);
  const isCheckSize = leftValue + widthSharePopup > widthMax;

  const setTop = topValue < 0 && top > 0 ? height + topPadding : topValue;
  const setLeft = setLeftValue({ phone, isCheckSize, leftValue, widthSharePopup, widthMax });
  const setRight = phone ? 15 : 'auto';

  return [setTop, setLeft, setRight, height];
};

export const removeListener = (closePopover, refreshPosition) => {
  window.removeEventListener('click', closePopover);
  window.removeEventListener('scroll', refreshPosition);
};
