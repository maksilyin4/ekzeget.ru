import React from 'react';

import { WhatsappShareButton } from 'react-share';

const WhatsappShare = ({ url, children, className, text }) => {
  return (
    <WhatsappShareButton
      className={className}
      url={`${text} \n ${url}`}>
      {children}
    </WhatsappShareButton>
  );
};

export default WhatsappShare;
