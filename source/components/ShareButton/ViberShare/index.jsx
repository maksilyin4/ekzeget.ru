import React from 'react';

import { ViberShareButton } from 'react-share';

const ViberShare = ({ url, children, className, text }) => {
  return (
    <ViberShareButton
      className={className}
      url={`${url} \n ${text}`}>
      {children}
    </ViberShareButton>
  );
};

export default ViberShare;
