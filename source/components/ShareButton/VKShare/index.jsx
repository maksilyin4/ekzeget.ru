import React from 'react';

// vk не дает отправить больше чем 760 символов
const maxCountForComment = 760;

const VKShare = ({ url, children, className, text = '', isAdaptive }) => {
  const handleClick = () => {
    const description = text
      ? text
          .split('\n')
          .map(t => `${t} `)
          .join('')
      : ' ';

    const comment =
      description.length > maxCountForComment
        ? `${description.substring(0, maxCountForComment)}...`
        : description;

    const linkShare = isAdaptive
      ? `https://m.vk.com/share.php?url=${url}&comment=${comment}`
      : `https://vk.com/share.php?url=${url}&comment=${comment}`;

    window.open(linkShare, '_blank');
  };
  return (
    <button onClick={handleClick} className={className}>
      {children}
    </button>
  );
};

export default VKShare;
