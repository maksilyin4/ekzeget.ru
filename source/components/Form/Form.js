import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import './form.scss';
import Button from '../Button/Button';
import Icon from '../Icons/Icons';

class Form extends Component {
  static propTypes = {
    className: PropTypes.string,
    onSubmit: PropTypes.func,
    action: PropTypes.string,
    formRef: PropTypes.func,
  };

  render() {
    return (
      <form
        action={this.props.action}
        onSubmit={this.props.onSubmit}
        className={cx('form', this.props.className)}
        ref={this.props.formRef}>
        {this.props.children}
      </form>
    );
  }
}

export class FormGroup extends Component {
  static propTypes = {
    className: PropTypes.string,
  };

  render() {
    return (
      <fieldset className={cx('form__fieldset', this.props.className)}>
        {this.props.children}
      </fieldset>
    );
  }
}
export class FormLabel extends Component {
  static propTypes = {
    className: PropTypes.string,
    label: PropTypes.string,
  };

  render() {
    return (
      <fieldset className={cx('form__label', this.props.className)}>{this.props.label}</fieldset>
    );
  }
}
export class FormSubmit extends Component {
  static propTypes = {
    className: PropTypes.string,
    title: PropTypes.string,
    align: PropTypes.string,
    isPreLoader: PropTypes.bool,
  };

  render() {
    const { align, className } = this.props;

    return (
      <div className={cx('form__submit', className, { [`form__submit_align-${align}`]: align })}>
        {this.props.isPreLoader ? (
          <button className="btn rect fluid bold btn_disabled  form__submit-button" disabled>
            <Icon icon="button_preloader" />
            <span>Обработка</span>
          </button>
        ) : (
            <button className="btn rect fluid bold form__submit-button">
              {this.props.title}
            </button>
        )}
      </div>
    );
  }
}

export default Form;
