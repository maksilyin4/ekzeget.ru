import React from 'react';
import PropTypes from 'prop-types';

import Icon from '~components/Icons/Icons';

const BibleTabActionItem = ({
  title = '',
  className = 'tab__action-item',
  icon = '',
  onClick = f => f,
  children,
}) => (
  <div className={className} title={title} onClick={onClick}>
    {children || <Icon icon={icon} />}
  </div>
);

BibleTabActionItem.propTypes = {
  title: PropTypes.string,
  className: PropTypes.string,
  icon: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.node,
};

export default BibleTabActionItem;
