import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import cx from 'classnames';
import { useSelector, useDispatch } from 'react-redux';

import { getVerseParallel } from '~dist/actions/Verse';
import { getVerseIsLoadingParallel, getVerseDataParallel } from '~dist/selectors/Verse';

import BiblePreloader from '../../BiblePreloader';

import '../verseTabs.scss';
import './verseParallel.scss';

const VerseParallel = ({ currentCode, verseId }) => {
  const dispatch = useDispatch();
  const parallelDataItem = useSelector(getVerseDataParallel);
  const isLoadingParallel = useSelector(getVerseIsLoadingParallel);

  const parallelData = parallelDataItem && parallelDataItem[verseId];

  useEffect(() => {
    if (!parallelData) {
      dispatch(getVerseParallel(verseId));
    }
  }, [verseId, parallelData]);

  if (isLoadingParallel) {
    return <BiblePreloader />;
  }
  const isParallelData = !parallelData || (parallelData && !parallelData.parallels.length);
  return (
    <div className="verse__parallels-content">
      {isParallelData ? (
        <div className="empty-content">Для данного стиха, параллельные стихи отсутствуют</div>
      ) : (
        <div className="verse__parallels">
          {parallelData.parallels.map(parallel => (
            <div
              key={parallel.id}
              className={cx('verse__translate', {
                csya: currentCode === 'csya_old',
                greek: currentCode === 'grek',
              })}>
              {/* Проверка, потому что в объекте дефолтный перевод - st_text */}
              {currentCode !== 'st_text' ? (
                parallel.translates.map(
                  translate =>
                    translate.code.code === currentCode && (
                      <div key={`parallels-${parallel.id}`} className="verse__parallel">
                        <Link
                          className="verse__parallel-name"
                          to={`/bible/${parallel.book_code}/glava-${parallel.chapter}/?verse=${parallel.number}/`}>
                          {parallel.short}
                        </Link>
                        {!translate.text.length ? (
                          <span className="verse__parallel-text">Отсутствует описание</span>
                        ) : (
                          <span
                            className="verse__parallel-text"
                            dangerouslySetInnerHTML={{ __html: translate.text }}
                          />
                        )}
                      </div>
                    ),
                )
              ) : (
                <div key={`parallels-${parallel.id}`} className="verse__parallel">
                  <Link
                    className="verse__parallel-name"
                    to={`/bible/${parallel.book_code}/glava-${parallel.chapter}/?verse=${parallel.number}`}>
                    {parallel.short}
                  </Link>
                  <span
                    className="verse__parallel-text"
                    dangerouslySetInnerHTML={{ __html: parallel.text }}
                  />
                </div>
              )}
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default VerseParallel;
