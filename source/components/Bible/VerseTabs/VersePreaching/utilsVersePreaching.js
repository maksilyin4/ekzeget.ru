import R from 'ramda';

export const getHref = ({ excuse, preacher, name = '' }) => {
  const parentExcuseCode = excuse.parent && excuse.parent.code ? excuse.parent.code : '';
  const excuseCode = excuse.code || '';
  const preacherCode = preacher.code || '';
  let href = '/all-about-bible/propovedi/';
  if (parentExcuseCode && excuseCode && preacherCode) {
    href = `/all-about-bible/propovedi/${parentExcuseCode}/${excuseCode}/${preacherCode}/`;
  }
  if (name) {
    href += name;
  }
  return href;
};

export const parsePreaching = preachingForVerse => {
  const preaching = R.pathOr([], ['gp'], preachingForVerse);
  const preachingByPage = R.pathOr({}, ['gbs'], preachingForVerse);
  return { preaching, preachingByPage };
};
