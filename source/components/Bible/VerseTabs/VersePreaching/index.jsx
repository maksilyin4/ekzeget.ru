import R from 'ramda';
import React, { useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';

import { getPreaching } from '~dist/actions/BiblePreaching';
import { getPreachingIsLoading, getPreachingForVerse } from '~dist/selectors/BiblePreaching';
import getScreen from '~dist/selectors/screen';

import List, { ListItem } from '~components/List/List';
import Pagination from '~components/Pagination/Pagination';
import Preloader from '~components/Preloader/Preloader';

import { getHref, parsePreaching } from './utilsVersePreaching';
import { smoothScrollTo } from '~dist/utils';

import '../verseTabs.scss';

const VersePreaching = ({ verseId = '' }) => {
  const dispatch = useDispatch();

  const isLoadingPreaching = useSelector(getPreachingIsLoading);
  const preachingForVerse = useSelector(getPreachingForVerse);
  const screen = useSelector(getScreen);

  const { preaching, preachingByPage } = useMemo(() => parsePreaching(preachingForVerse), [
    preachingForVerse,
  ]);

  useEffect(() => {
    if (verseId) {
      dispatch(getPreaching(`?verse_id=${verseId}&page=${1}&per-page=${20}`));
    }
  }, [verseId]);

  const changePage = page => {
    const timeout = screen.isPhone ? 100 : 250;
    smoothScrollTo(timeout);

    dispatch(getPreaching(`?verse_id=${verseId}&page=${page}&per-page=${20}`));
  };

  if (isLoadingPreaching) {
    return <Preloader className="empty-content" />;
  }

  const isPagination = Math.ceil(R.pathOr(0, ['totalCount'], preachingByPage) / 20) > 1;

  return (
    <div className="verse__preaching-content">
      {!preaching.length ? (
        <div className="empty-content">
          <span>Для данного стиха проповеди не найдены</span>
        </div>
      ) : (
        <List className="verse-preaching-content-list">
          {preaching.map(({ excuse, preacher, id, theme, code }) => {
            const href = getHref({ excuse, preacher, name: code });
            return <ListItem key={id} title={preacher.name} href={href} themePreaching={theme} />;
          })}
        </List>
      )}
      {isPagination && (
        <Pagination
          page={preachingByPage.currentPage}
          count={20}
          totalCount={preachingByPage.totalCount}
          onClick={changePage}
          className="pagination"
        />
      )}
    </div>
  );
};

VersePreaching.propTypes = {
  verseId: PropTypes.string,
};

export default VersePreaching;
