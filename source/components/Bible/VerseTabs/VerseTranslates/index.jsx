import React from 'react';
import cx from 'classnames';
import { useSelector } from 'react-redux';

import { getVerseIsLoading, verseTranslates } from '~dist/selectors/Verse';

import BiblePreloader from '../../BiblePreloader';

import './verseTranslates.scss';

const VerseTranslates = () => {
  const verseTranslatesData = useSelector(verseTranslates);
  const isLoading = useSelector(getVerseIsLoading);

  if (isLoading) {
    return <BiblePreloader />;
  }

  return (
    <div className="verse_translates">
      {verseTranslatesData.map(
        verse =>
          verse.text && (
            <div key={verse.id} className="verse_translate">
              <div className="verse_translate-name">{verse.codeTitle}</div>
              <span
                className={cx('verse_translate-text', {
                  csya: verse.code === 'csya_old',
                  greek: verse.code === 'grek',
                })}
                dangerouslySetInnerHTML={{ __html: verse.text }}
              />
            </div>
          ),
      )}
    </div>
  );
};

export default VerseTranslates;
