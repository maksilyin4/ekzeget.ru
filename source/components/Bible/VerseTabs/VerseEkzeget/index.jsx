import React, { useCallback, useMemo } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { parsePathname } from '~utils/parsePathname';

import { linkToVerseLoading, linkToVerse } from '~dist/selectors/Verse';

import BiblePreloader from '../../BiblePreloader';

import './verseEkzeget.scss';

const VerseEkzeget = ({ chapterNum, verseNum, match: { params } }) => {
  const interpretation = useSelector(linkToVerse);
  const isLoadingVerse = useSelector(linkToVerseLoading);

  const filterVerse = useCallback(
    () => verse =>
      Number(parsePathname({ pathname: verse.url, index: 3 })) !== chapterNum &&
      Number(parsePathname({ pathname: verse.url, index: 4 })) !== verseNum,

    [chapterNum, verseNum],
  );
  const filterMap = useCallback(
    item => verse => (
      <div key={verse.id} className="exegetes__item">
        <span className="exegetes__short">{verse.short} – </span>
        <Link
          className="exegetes__link"
          to={{
            pathname: `${verse.url.replace('interpretation', 'bible')}`,
            state: {
              bookCode: params.book_code || params.code,
              chapterNum: params.chapter_num || params.chapter,
            },
          }}>
          {item.ekzeget.name}
        </Link>
      </div>
    ),
    [chapterNum, verseNum],
  );

  const data = interpretation?.interpretation || [];

  const interpretationData = useMemo(
    () => data.map(item => item.verses.filter(filterVerse).map(filterMap(item))),
    [data],
  );

  if (isLoadingVerse) {
    return <BiblePreloader />;
  }

  return (
    <div className="verse__ekzeget-content">
      {!data.length ? (
        <div className="empty-content">Информация отсутствует</div>
      ) : (
        <div className="verse-ekzeget-list">{interpretationData}</div>
      )}
    </div>
  );
};

export default withRouter(VerseEkzeget);
