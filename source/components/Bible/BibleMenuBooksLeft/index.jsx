import React, { useRef, useEffect, useState } from 'react';
import cx from 'classnames';
import { useCookies } from 'react-cookie';
import { useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { getIsDesktop } from '../../../dist/selectors/screen';

import NavTestament from '~components/NavTestament/NavTestament';
import OrphusInfo from '~components/OrphusInfo';

import { defaultHeightContentBible } from '../../../utils/common';

import './bibleMenuBooksLeft.scss';

const BibleMenuBooksLeft = ({ left, activeBook, activeChapter, activeTestament, isOtherBlock }) => {
  const { pathname } = useLocation();

  const isDesktop = useSelector(getIsDesktop);

  const [currentActiveTab, setCurrentActiveTab] = useState(1);
  const setCookies = useCookies()[1];

  const leftMenuRef = useRef(null);

  const getBackActiveTab = activeTab => {
    setCurrentActiveTab(activeTab);
  };

  useEffect(() => {
    setCookies(
      'heightLeftMenuBible',
      leftMenuRef.current?.clientHeight || defaultHeightContentBible,
    );
  }, [currentActiveTab, leftMenuRef.current?.clientHeight, activeBook, left, pathname, isDesktop]);

  return (
    <div className={cx('bible-side bible-nav', { collapse: left, isOtherBlock })}>
      <div ref={leftMenuRef} className="side__item paper">
        <NavTestament
          activeTestament={activeTestament}
          activeBook={activeBook}
          activeChapter={activeChapter}
          getBackActiveTab={getBackActiveTab}
          showChapter
        />
      </div>
      {isDesktop && (
        <div className="side__item paper">
          <OrphusInfo />
        </div>
      )}
    </div>
  );
};

export default BibleMenuBooksLeft;
