import React from 'react';
import cx from 'classnames';

import Preloader from '~components/Preloader/Preloader';

import './biblePreloader.scss';

const BiblePreloader = ({ isBorder }) => (
  <div
    className={cx('bible-content-preloader', {
      'bible-preloader-border': isBorder,
    })}>
    <Preloader />
  </div>
);

export default BiblePreloader;
