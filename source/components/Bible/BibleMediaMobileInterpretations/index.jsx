import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useCookies } from 'react-cookie';
import './bibleMediaMobileInter.scss';

import BibleMediaBtn from '~components/Bible/BibleMediaContentRight/BibleMedia/BibleMediaBtn';
import Icon from '~components/Icons/Icons';
import AnimateLiftRight from '~components/AnimateComponent/SlideLeftRight';

import BibleMedia from '../../Bible/BibleMediaContentRight';

const BibleMediaMobileInterpretations = ({
  bookCode,
  title,
  activeItem,
  chapter,
  baseUrl,
  verseCurrent,
}) => {
  const [cookies] = useCookies(['interpretationTab']);
  const { interpretationTab } = cookies;

  const [isShow, setIsShow] = useState(false);

  const handleClick = () => {
    setIsShow(!isShow);
  };

  const titleInter = title || 'Все толкования';

  return (
    <>
      <div className="bible-mobile-interpretation">
        <BibleMediaBtn
          className="bible-interpretation"
          title={titleInter}
          code="interpretations"
          onClick={handleClick}
        />
        <Icon icon="arrow-down" className="bible-mobile-interpretation-icon" />
      </div>
      <AnimateLiftRight show={isShow} className="bible-mobile-interpretation-menu">
        <button className="bible-mobile-back" onClick={handleClick}>
          <Icon icon="arrow-left" />
        </button>
        <BibleMedia
          chapter={chapter}
          bookCode={bookCode}
          activeItem={activeItem}
          baseUrl={baseUrl}
          verseCurrent={verseCurrent}
          onCloseInter={handleClick}
          isBibleMobile
        />
      </AnimateLiftRight>
    </>
  );
};

BibleMediaMobileInterpretations.propTypes = {
  bookCode: PropTypes.string,
  title: PropTypes.string,
};

export default BibleMediaMobileInterpretations;
