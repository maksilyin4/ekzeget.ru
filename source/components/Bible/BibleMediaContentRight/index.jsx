import React from 'react';
import cx from 'classnames';
import { useCookies } from 'react-cookie';

import BibleMedia from './BibleMedia/';

import { setStyleHeight } from '~utils/common';

import './bibleMediaContentRight.scss';

const BibleMediaContent = ({ left, collapse, extend, classNameRight, ...props }) => {
  const [{ heightLeftMenuBible }] = useCookies(['heightLeftMenuBible']);
  return (
    <div
      style={setStyleHeight({ height: props.withoutScroll ? 'auto' : heightLeftMenuBible })}
      className={cx('bible_right', classNameRight, {
        collapse,
        extend,
        'bible-not-left': left,
      })}>
      <BibleMedia {...props} />
    </div>
  );
};

export default BibleMediaContent;
