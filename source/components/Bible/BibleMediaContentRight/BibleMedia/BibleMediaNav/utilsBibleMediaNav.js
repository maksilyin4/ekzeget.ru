export const setNumberChapter = (chapterNum = '') => {
  const glavaArr = chapterNum.split('-');
  const glavaNumber = glavaArr[glavaArr.length - 1];
  return glavaNumber || '';
};

export const getTitleBibleMediaNav = chapter =>
  chapter ? `${chapter?.book?.menu} ${chapter?.chapter}:${chapter?.number}` : '';
