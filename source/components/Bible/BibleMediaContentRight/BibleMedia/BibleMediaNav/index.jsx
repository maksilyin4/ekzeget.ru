import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { Link, useRouteMatch, useLocation } from 'react-router-dom';

import './style.scss';

import BibleMediaBtn from '../BibleMediaBtn';
import { bible } from '~utils/common';
import { getTitleBibleMediaNav, setNumberChapter } from './utilsBibleMediaNav';
import Icon from '../../../../Icons/Icons';
import { useDispatch } from 'react-redux';
import { toggleOpenPlanAction } from '~dist/actions/ReadingPlan';
import cn from 'classnames';

const BibleMediaNav = ({
  bibleMediaCategories,
  toggleMediaType,
  userLoggedIn = false,
  activeMedia,
  typeMedia = 'mediaCat_',
  verseId = '',
  isMobile,
  chapter,
}) => {
  const { params = {} } = useRouteMatch();
  const { book_code, chapter_num = '', verse_id } = params;
  const dispatch = useDispatch();
  const openReading = () => dispatch(toggleOpenPlanAction(true));

  const numberGlava = useMemo(() => setNumberChapter(chapter_num), [chapter_num]);

  const type = activeMedia?.type || '';

  const title = `${chapter?.book?.menu} ${numberGlava} глава`;
  const subTitle = isMobile && verse_id ? ` (к стиху)` : undefined;
  const isPlan = useLocation().pathname.includes('reading');

  const hideReading = isMobile && isPlan ? () => dispatch(toggleOpenPlanAction(false)) : null;
  return (
    <nav className={cn('bible-media__nav', isPlan && 'z-max')}>
      {isMobile && verseId && (
        <Link className="bible-media__nav-btn_back" to={`/${bible}/${book_code}/${chapter_num}/`}>
          <BibleMediaBtn key="mobile_back" title={title} />
          <Icon type="back" icon="back" />
        </Link>
      )}

      {isMobile && !isPlan && (
        <Link to={`/${bible}/${book_code}/${chapter_num}/${verse_id ? `${verse_id}/` : ''}`}>
          <BibleMediaBtn
            key="mobile"
            title={verse_id ? getTitleBibleMediaNav(chapter) : 'Текст главы'}
            subTitle={isMobile && verse_id ? ` (стих)` : undefined}
            code="bibleText"
            type={type}
            onClick={toggleMediaType}
          />
        </Link>
      )}

      {isMobile && isPlan && (
        <div onClick={openReading}>
          <BibleMediaBtn
            key="mobile"
            title={verse_id ? getTitleBibleMediaNav(chapter) : 'Текст главы'}
            subTitle={isMobile && verse_id ? ` (стих)` : undefined}
            code="bibleText"
            type={type}
            onClick={toggleMediaType}
          />
        </div>
      )}

      {!isPlan && (
        <BibleMediaBtn
          key="interpretations"
          title="Толкования"
          subTitle={subTitle}
          code="interpretations"
          type={type}
          onClick={toggleMediaType}
          hideReading={hideReading}
        />
      )}
      {bibleMediaCategories.map(item => (
        <BibleMediaBtn
          key={item.id}
          title={item.title}
          subTitle={subTitle}
          code={`${typeMedia}${item.code || 'all'}`}
          type={type}
          onClick={toggleMediaType}
          hideReading={hideReading}
        />
      ))}
      <BibleMediaBtn
        key="questions"
        title="Вопросы"
        subTitle={subTitle}
        code="questions"
        type={type}
        onClick={toggleMediaType}
        hideReading={hideReading}
      />
      {!verseId && (
        <BibleMediaBtn
          key="preaching"
          title="Проповеди"
          code="preaching"
          type={type}
          onClick={toggleMediaType}
          hideReading={hideReading}
        />
      )}
      {userLoggedIn && !verseId && (
        <BibleMediaBtn
          key="favorites"
          title="Избранные стихи"
          code="favorites"
          type={type}
          onClick={toggleMediaType}
          hideReading={hideReading}
        />
      )}
    </nav>
  );
};

BibleMediaNav.propTypes = {
  bibleMediaCategories: PropTypes.object.isRequired,
  typeMedia: PropTypes.string,
  verseId: PropTypes.string,
  isMobile: PropTypes.bool,
  toggleMediaType: PropTypes.func,
  userLoggedIn: PropTypes.object,
  activeMedia: PropTypes.object,
  chapter: PropTypes.object,
};

export default BibleMediaNav;
