import React from 'react';
import PropTypes from 'prop-types';

import BibleMediaNav from '../BibleMediaNav';
import BiblePreloader from '~components/Bible/BiblePreloader';

import './bibleMediaNavigators.scss';

const BibleMediaNavigators = ({
  loading,
  categories,
  toggleMediaType,
  userLoggedIn,
  verseId,
  isMobile,
  chapter,
}) => {
  if (loading) {
    return <BiblePreloader isBorder />;
  }

  const bibleTitle = verseId ? 'Все к этому стиху' : 'Все к этой главе';

  return (
    <div className="bible-media__default bible-media-shadow-paper">
      <div className="bible-media__title">{bibleTitle}</div>
      <BibleMediaNav
        bibleMediaCategories={categories}
        toggleMediaType={toggleMediaType('bibleNavParent')}
        userLoggedIn={userLoggedIn}
        verseId={verseId}
        isMobile={isMobile}
        chapter={chapter}
      />
    </div>
  );
};

BibleMediaNavigators.propTypes = {
  verseId: PropTypes.string,
  isMobile: PropTypes.bool,
  userLoggedIn: PropTypes.bool,
  toggleMediaType: PropTypes.func,
  categories: PropTypes.array,
  loading: PropTypes.bool,
  chapter: PropTypes.object,
};

export default BibleMediaNavigators;
