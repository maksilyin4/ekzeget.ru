import React from 'react';
import cx from 'classnames';

import Button from '../../../../ButtonNew';

import './style.scss';

const BibleMediaBtn = ({
  title,
  hideReading,
  subTitle,
  code = '',
  type = '',
  onClick = f => f,
  className,
}) => {
  return (
    <div className={cx('bible-media__nav-item', className)}>
      <Button
        mod="gradient"
        className={cx('navigation__item', {
          active: code && type && code === type,
        })}
        title={title}
        subTitle={subTitle}
        onClick={() => {
          onClick(code, title);
          hideReading && hideReading();
        }}
      />
    </div>
  );
};

export default BibleMediaBtn;
