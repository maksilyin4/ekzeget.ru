import React, { useState, useMemo, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { BibleMediaContext } from './BibleMediaContext';

import BibleMediaHead from './BibleMediaHead';
import BibleMediaBody from './BibleMediaBody';
import BibleMediatekaSort from './BibleMediatekaSort';

import {
  parseExtendLayout,
  getChildrenMedia,
  getCategoryId,
  getIsShowContent,
} from './utilsBibleMediaTemplate';
import { getLocalStorage } from '~utils/localStorage';

import './bibleMediaContent.scss';

const BibleMediaContent = ({
  bibleMediaCategories,
  activeMedia,
  bibleNavChild,
  toggleMediaType,
  baseUrl,
  chapter,
  extendLayout = {},
  verseId = '',
  isBibleMobile,
  userLoggedIn,
  screen,
  withoutScroll,
  onCloseInter = f => f,
  ...props
}) => {
  const { type } = activeMedia;

  const isShowContent = isBibleMobile || getIsShowContent(type, screen.phone);

  const [state, setState] = useState({ parent: false, child: false });
  const [sorts, setSorts] = useState({ sort: 'alph', sortDirection: false });
  const [ekzegetFilter, setEkzegetFilter] = useState([]);
  const [researchFilter, setResearchFilter] = useState(false);

  useEffect(() => {
    const sessionSort = getLocalStorage('ekzegetSort');
    if (sessionSort) {
      setSorts(sessionSort);
    }
  }, [type]);

  const category_id = getCategoryId(type, 'mediaCat_');

  const childrenMedia = useMemo(() => {
    return getChildrenMedia({ bibleMediaCategories, category_id });
  }, [bibleMediaCategories, category_id]);

  const toggleOpen = setName => () => {
    setState({ ...state, [setName]: !state[setName] });
  };

  const callBackSort = value => {
    setSorts(value);
  };

  const { isShowBibleMediaNav, isShowBibleMediaMiddle, content, left } = parseExtendLayout(
    extendLayout,
  );

  const renderBibleMediaHead = useCallback(() => {
    const props = screen.phone ? { className: 'bible-media-head-mobile', isMobile: true } : {};

    return (
      <BibleMediaHead
        content={content}
        isShowBibleMediaNav={isShowBibleMediaNav}
        isShowBibleMediaMiddle={isShowBibleMediaMiddle}
        toggleMediaType={toggleMediaType}
        userLoggedIn={userLoggedIn}
        activeMedia={activeMedia}
        bibleMediaCategories={bibleMediaCategories}
        verseId={verseId}
        state={state}
        toggleOpen={toggleOpen}
        chapter={chapter}
        {...props}
      />
    );
  }, [
    screen.phone,
    content,
    verseId,
    userLoggedIn,
    activeMedia,
    bibleMediaCategories,
    state,
    chapter,
  ]);

  return (
    <BibleMediaContext.Provider
      value={{
        sorts,
        ekzegetFilter,
        researchFilter,
        content,
        left,
        category_id,
        isShowBibleMediaMiddle,
        isShowBibleMediaNav,
        userLoggedIn,
        screen,
        onCloseInter,
      }}>
      {screen.phone && !isBibleMobile && renderBibleMediaHead()}
      {isShowContent && (
        <div className="bible-media__content">
          {!screen.phone && renderBibleMediaHead()}
          {(type.includes('mediaCat_') ||
            type.includes('questions') ||
            type.includes('preaching') ||
            type.includes('interpretations')) && (
            <BibleMediatekaSort
              ekzegetFilter={ekzegetFilter}
              setEkzegetFilter={setEkzegetFilter}
              researchFilter={researchFilter}
              setResearchFilter={setResearchFilter}
              callBackSort={callBackSort}
              childrenMedia={childrenMedia}
              state={state}
              toggleOpen={toggleOpen}
              bibleNavChild={bibleNavChild}
              toggleMediaType={toggleMediaType}
            />
          )}
          <BibleMediaBody
            withoutScroll={withoutScroll}
            type={type}
            isShowBibleMediaMiddle={isShowBibleMediaMiddle}
            chapter={chapter}
            baseUrl={baseUrl}
            category_id={+category_id}
            verseId={verseId}
            sorts={sorts}
            {...props}
          />
        </div>
      )}
    </BibleMediaContext.Provider>
  );
};

BibleMediaContent.propTypes = {
  activeMedia: PropTypes.string,
  verseId: PropTypes.string,
  bibleMediaCategories: PropTypes.array,
  bibleNavChild: PropTypes.array,
  chapter: PropTypes.object,
  userLoggedIn: PropTypes.bool,
  toggleMediaType: PropTypes.func,
  toggleOpen: PropTypes.func,
  extendLayout: PropTypes.object,
  screen: PropTypes.object,
};

export default BibleMediaContent;
