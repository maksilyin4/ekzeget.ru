import React from 'react';
import cx from 'classnames';

import Icon from '~components/Icons/Icons';
import BibleMediaNavParent from '../../BibleMediaNav';
import BibleMediaNavChild from '../BibleMediaNavChild';

import './style.scss';

const BibleMediaNavSelect = ({
  isShowBibleMediaMiddle,
  isOpen,
  activeMedia,
  toggleOpen,
  categories,
  toggleMediaType,
  userLoggedIn,
  isShow,
  className,
  typeMedia,
  verseId,
  isMobile,
  chapter,
}) => {
  const title = activeMedia?.title;
  return (
    <div
      className={cx('bible-media__pulldown', className, {
        pulldown_middle: !isShowBibleMediaMiddle && !isMobile,
      })}>
      <button className={cx('bible-media__pulldown-head', { active: isOpen })} onClick={toggleOpen}>
        <p>
          {title ?? 'Все произведения'}
          {title && verseId && isMobile && (
            <span style={{ fontSize: '10px' }}>
              {activeMedia?.type === 'bibleText' ? ` (стих)` : ` (к стиху)`}
            </span>
          )}
        </p>
        <div className="bible-media__arrow-wrap">
          <Icon icon="angle-down" />
        </div>
      </button>
      <div className="bible-media__pulldown-body" onClick={toggleOpen}>
        {!isShow ? (
          <BibleMediaNavParent
            className="bible-media__nav"
            bibleMediaCategories={categories}
            toggleMediaType={toggleMediaType}
            userLoggedIn={userLoggedIn}
            activeMedia={activeMedia}
            typeMedia={typeMedia}
            verseId={verseId}
            isMobile={isMobile}
            chapter={chapter}
          />
        ) : (
          <BibleMediaNavChild
            className="bible-media__nav"
            bibleMediaCategories={categories}
            toggleMediaType={toggleMediaType}
            activeMedia={activeMedia}
            typeMedia={typeMedia}
          />
        )}
      </div>
    </div>
  );
};

export default BibleMediaNavSelect;
