import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import BibleMediaNav from '../../BibleMediaNav';
import BibleMediaNavSelect from '../BibleMediaNavSelect';

import './bibleMediaHead.scss';

const BibleMediaHead = ({
  chapter,
  content,
  isShowBibleMediaNav,
  isShowBibleMediaMiddle,
  toggleMediaType,
  userLoggedIn,
  activeMedia,
  bibleMediaCategories,
  verseId,
  state,
  toggleOpen,
  isMobile,
  className,
}) => (
  <div
    className={cx('bible-media__head', className, {
      content: !content,
    })}>
    {!isMobile && <div className="bible-media__title">Выберите раздел Медиатеки</div>}
    {isShowBibleMediaNav && !isMobile ? (
      <BibleMediaNav
        key={'parent'}
        bibleMediaCategories={bibleMediaCategories}
        toggleMediaType={toggleMediaType('bibleNavParent')}
        userLoggedIn={userLoggedIn}
        activeMedia={activeMedia}
        verseId={verseId}
        isMobile={isMobile}
        chapter={chapter}
      />
    ) : (
      <BibleMediaNavSelect
        key={'parent'}
        isShowBibleMediaMiddle={isShowBibleMediaMiddle}
        isOpen={state.parent}
        activeMedia={activeMedia}
        toggleOpen={toggleOpen('parent')}
        categories={bibleMediaCategories}
        toggleMediaType={toggleMediaType('bibleNavParent')}
        userLoggedIn={userLoggedIn}
        verseId={verseId}
        isMobile={isMobile}
        chapter={chapter}
      />
    )}
  </div>
);

BibleMediaHead.propTypes = {
  content: PropTypes.string,
  className: PropTypes.string,
  activeMedia: PropTypes.string,
  verseId: PropTypes.string,
  bibleMediaCategories: PropTypes.array,
  userLoggedIn: PropTypes.bool,
  toggleMediaType: PropTypes.func,
  toggleOpen: PropTypes.func,
  state: PropTypes.object,
  isMobile: PropTypes.bool,
  isShowBibleMediaNav: PropTypes.bool,
  isShowBibleMediaMiddle: PropTypes.bool,
};

export default BibleMediaHead;
