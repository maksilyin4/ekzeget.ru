export const multiTab = {
  chapter: 1,
  verse: 2,
};

export const getDefaultAllChildCategory = (title = 'Все произведения') => ({ id: 'all', title });

export const parseExtendLayout = extendLayout => {
  let isShowBibleMediaNav = false;
  const { right, content, left } = extendLayout;
  if (extendLayout) {
    isShowBibleMediaNav = !right && content && left;
  }
  return { isShowBibleMediaNav, isShowBibleMediaMiddle: Boolean(!content), content, left };
};

// ниже функция пока дорабатывается
const defaultIndex = (screen, content, left, isShowBibleMediaNav) => {
  let defaultCount = 4;

  if ((content && !left && !screen.tablet) || (!screen.phone && !isShowBibleMediaNav)) {
    defaultCount = 3;
  }

  return defaultCount;
};

function getNewMedia(medias, defaultCount) {
  const arr = [];
  let ind = 0;
  arr[ind] = [];
  medias.forEach(el => {
    if (arr[ind].length !== defaultCount) {
      arr[ind].push(el);
    } else {
      ind++;
      arr[ind] = [el];
    }
  });
  return arr;
}

export const getParseMedia = (medias = [], screen, content, left, isShowBibleMediaNav) => {
  if (medias.length) {
    const { desktop, tabletMedium } = screen;
    const isDevice = desktop || tabletMedium;
    if (isDevice) {
      const defaultCount = defaultIndex(screen, content, left, isShowBibleMediaNav);
      return getNewMedia(medias, defaultCount, screen);
    }
    return [medias];
  }
  return [];
};

export const getChildrenMedia = ({ bibleMediaCategories, category_id }) => {
  const category = bibleMediaCategories.find(category => category.code === category_id) || {
    children: [],
  };
  const defaultAll = getDefaultAllChildCategory(
    category_id === 'biblia-v-iskusstve' ? 'Все произведения' : undefined,
  );

  return category.children.length ? [defaultAll, ...category.children] : category.children;
};

export const getCategoryId = (type = '', setName = 'mediaCat_') =>
  type.includes(setName) ? type.split(setName)[1] : type;

export const getCategoryCode = (categoryChildId, category_id, interMultiTab = 1) => {
  const tabValueChapter = 1;
  const categoryId = 'video';
  const allCategoryChild = 'all';

  if (
    categoryChildId === allCategoryChild ||
    (category_id === categoryId && tabValueChapter === +interMultiTab)
  ) {
    return category_id;
  }
  return categoryChildId || category_id;
};

export const getSortMedia = (
  sorts = { sort: 'alph', sortDirection: 0 },
  categoryId = '',
  multiTabVerse,
) => {
  const isNotVideo = categoryId !== 'video';
  const { sort, sortDirection } = sorts;

  let orderBy = 'title';
  const order = sortDirection && isNotVideo ? 0 : 1;

  if (sort === 'chrone' && isNotVideo) {
    orderBy = 'created_at';
  }

  if (multiTabVerse && !isNotVideo) {
    orderBy = 'sequence';
  }

  return { order, orderBy };
};

export const getIsShowContent = (type, isPhone) => {
  if (isPhone && !(type === 'bibleText' || type === 'interpretations')) {
    return isPhone;
  }
  return !isPhone;
};
