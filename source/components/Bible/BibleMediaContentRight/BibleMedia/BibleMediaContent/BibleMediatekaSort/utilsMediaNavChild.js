import { getDefaultAllChildCategory } from '../utilsBibleMediaTemplate';

const alphSort = 'alph';

const categoryAll = 'all';
const categoryVideo = 'video';
const categoryMotivatory = 'motivatory';
const categoryBibleViskusstve = 'biblia-v-iskusstve';
const knigi = 'knigi';
const preaching = 'preaching';
const ikonografia = 'ikonografia';

const videoFilter = [
  'tolkovania-k-stiham',
  'biblia-otvecaet',
  'pravoslavnye-prazdniki',
  'prazdniki-i-sobytia',
  'biblejskie-temy',
  'tolkovania-svatyh',
];

const categoryIdNotShowTabs = [
  categoryBibleViskusstve,
  categoryMotivatory,
  knigi,
  preaching,
  ikonografia,
];
const categoryVideoTitle = 'видео';
const categoryMotivatoryTitle = 'мотиваторы';
const categoryQuestionsTitle = 'вопросы';

const categoryTitleNotShowSort = [
  categoryVideoTitle,
  categoryMotivatoryTitle,
  categoryQuestionsTitle,
];

const chapter = 1;

const getBibleNavChild = (bibleNavParent = {}, categoriesChild = [], bibleNavChild) => {
  if (Array.isArray(categoriesChild) && categoriesChild.length) {
    const { title, code } = categoriesChild[0];

    if (
      bibleNavParent?.title.toLowerCase() === categoryVideoTitle &&
      bibleNavChild?.type.includes(categoryAll)
    ) {
      return { type: `mediaCatChild_${code}`, title };
    }
  }

  return bibleNavChild;
};

const bibleMediaCategoriesFilter = ({ code }) => videoFilter.indexOf(code) !== -1;

const getBibleDataChild = bibleMediaCategories => {
  const videoChildCategories = bibleMediaCategories.filter(bibleMediaCategoriesFilter);
  return [getDefaultAllChildCategory(), ...videoChildCategories];
};

const getBibleData = (bibleNavParent, bibleMediaCategories) => {
  if (bibleNavParent?.title?.toLowerCase() === categoryVideoTitle) {
    return getBibleDataChild(bibleMediaCategories);
  }
  return bibleMediaCategories;
};

const getIsShowChildSelect = (category_id, interMultiTab) => {
  if (category_id === categoryVideo) {
    return +chapter === (+interMultiTab || 1);
  }
  return category_id === categoryMotivatory;
};

const getIsShowSort = title => {
  return !categoryTitleNotShowSort.includes(title?.toLowerCase());
};

const setSortEkzegetFilter = (id, ekzegetFilter) => {
  if (id) {
    return ekzegetFilter.includes(id)
      ? ekzegetFilter.filter(ekId => ekId !== id)
      : [...ekzegetFilter, id];
  }
  return [];
};

export {
  getIsShowChildSelect,
  categoryIdNotShowTabs,
  getBibleNavChild,
  getBibleData,
  getIsShowSort,
  preaching,
  setSortEkzegetFilter,
  alphSort,
};
