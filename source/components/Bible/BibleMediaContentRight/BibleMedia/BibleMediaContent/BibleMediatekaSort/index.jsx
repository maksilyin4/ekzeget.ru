import React, { useMemo, useEffect, useContext, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useCookies } from 'react-cookie';
import cx from 'classnames';
import { BibleMediaContext } from '../BibleMediaContext';

import { getInterpretators, getEkzegetTypes } from '~dist/selectors/ChapterInterpretators';

import BibleMediaSort from '../BibleMediaBody/BibleMediaSort';
import BibleMediaNavSelect from '../BibleMediaNavSelect';
import BibleMediaNavChild from '../BibleMediaNavChild';
import BibleMediaTabs from './BibleMediaTabs';

import { setLocalStorage, getLocalStorage } from '~utils/localStorage';

import {
  getIsShowChildSelect,
  categoryIdNotShowTabs,
  getBibleData,
  getIsShowSort,
  setSortEkzegetFilter,
  alphSort,
} from './utilsMediaNavChild';

import './style.scss';

const BibleMediatekaSort = ({
  childrenMedia,
  state,
  toggleOpen,
  toggleMediaType,
  callBackSort,
  setEkzegetFilter,
  ekzegetFilter,
  researchFilter,
  setResearchFilter,
}) => {
  const { sorts, content, isShowBibleMediaMiddle, userLoggedIn, category_id, screen } = useContext(
    BibleMediaContext,
  );

  const ekzegetTypes = useSelector(getEkzegetTypes);
  const dispatchRedux = useDispatch();

  const [{ interMultiTab, bibleNavParent, bibleNavChild }, setCookie] = useCookies([
    'interMultiTab',
    'bibleNavParent',
    'bibleNavChild',
  ]);

  const { sort, sortDirection } = sorts;

  const isPreachingType =
    bibleNavParent?.type === 'preaching' || bibleNavParent?.type === 'interpretations';

  const sortFunc = value => () => {
    callBackSort({ sort: value, sortDirection: !sortDirection });
    setLocalStorage('ekzegetSort', { sort: value, sortDirection: !sortDirection });
  };

  const sortType = useCallback(
    id => {
      const ekzegetFilters = setSortEkzegetFilter(id, ekzegetFilter);
      setEkzegetFilter(ekzegetFilters);
      setLocalStorage('ekzegetFilter', ekzegetFilters.join(','));
    },
    [ekzegetFilter],
  );

  const persistResearchFilter = useCallback(
      (isResearch) => {
    setResearchFilter(isResearch);
    setLocalStorage('researchFilter', isResearch ?? '');
  },
    [researchFilter]
  );

  const isCategoryIdNotShowTabs = categoryIdNotShowTabs.includes(category_id);
  const Component = isCategoryIdNotShowTabs ? 'div' : BibleMediaTabs;

  const isShowChildSelect = getIsShowChildSelect(category_id, interMultiTab);

  const categoriesChild = useMemo(() => getBibleData(bibleNavParent, childrenMedia), [
    childrenMedia,
    bibleNavParent,
  ]);

  useEffect(() => {
    const researchFilter = !!getLocalStorage('researchFilter');
    setResearchFilter(researchFilter);

    const ekzegetFilters = getLocalStorage('ekzegetFilter');

    if (ekzegetFilters) {
      setEkzegetFilter(ekzegetFilters.split(',').map(el => +el));
    }
    if (!bibleNavChild && categoriesChild.length) {
      const { code, title } = categoriesChild[0];
      setCookie('bibleNavChild', { type: `mediaCatChild_${code || 'all'}`, title }, { path: '/' });
    }
  }, []);

  useEffect(() => {
    if (sort !== alphSort) {
      setLocalStorage('ekzegetSort', { sort: alphSort, sortDirection: true });
    }
    if (ekzegetTypes.length === 0 && isPreachingType) {
      dispatchRedux(getInterpretators());
    }
  }, [isPreachingType]);

  const isShowSort = getIsShowSort(bibleNavParent?.title);

  return (
    <Component
      className={cx('bible-media-interpretations', {
        'bible-media-padding': isCategoryIdNotShowTabs,
      })}>
      {!isShowChildSelect && (
        <>
          {(screen.phone || !content) && childrenMedia.length !== 0 ? (
            <BibleMediaNavSelect
              key="child"
              isShowBibleMediaMiddle={isShowBibleMediaMiddle}
              className="bible-media-select-child"
              isOpen={state.child}
              categoryId={category_id}
              activeMedia={bibleNavChild}
              toggleOpen={toggleOpen('child')}
              categories={categoriesChild}
              toggleMediaType={toggleMediaType('bibleNavChild')}
              userLoggedIn={userLoggedIn}
              typeMedia="mediaCatChild_"
              isShow
            />
          ) : (
            <BibleMediaNavChild
              key="child"
              bibleMediaCategories={categoriesChild}
              toggleMediaType={toggleMediaType('bibleNavChild')}
              activeMedia={bibleNavChild}
              typeMedia="mediaCatChild_"
            />
          )}
        </>
      )}
      {isShowSort && (
        <BibleMediaSort
          ekzegetFilter={ekzegetFilter}
          researchFilter={researchFilter}
          setResearchFilter={persistResearchFilter}
          sortType={sortType}
          ekzegetTypes={ekzegetTypes}
          sortDirection={sortDirection}
          sort={sort}
          sortAlph={sortFunc('alph')}
          sortChrone={sortFunc('chrone')}
          isDisabledChrone={!isPreachingType}
          isDisabledTypes={bibleNavParent?.type !== 'interpretations' || +interMultiTab === 1 || interMultiTab === undefined}
        />
      )}
    </Component>
  );
};

export default BibleMediatekaSort;
