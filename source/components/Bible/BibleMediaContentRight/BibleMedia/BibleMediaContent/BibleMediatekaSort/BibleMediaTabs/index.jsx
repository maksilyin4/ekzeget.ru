import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { useCookies } from 'react-cookie';

import Tabs, { Tab, TabContent } from '~components/Tabs/Tabs';

import { getLabelTextForTabs } from './utilsBibleMediaTabs';

const BibleMediaTabs = ({ children, isShowBibleMediaMiddle }) => {
  const [activeContent, setActiveContent] = useState(1);

  const [{ interMultiTab, bibleNavParent }, setCookie] = useCookies([
    'interMultiTab',
    'bibleNavParent',
  ]);
  useEffect(() => {
    if (interMultiTab) {
      setActiveContent(+interMultiTab);
    }
  }, []);

  const handleChange = useCallback(
    value => {
      setActiveContent(value);
      return setCookie('interMultiTab', value, { path: '/' });
    },
    [activeContent],
  );

  const [labelTextLeft, labelTextRight] = getLabelTextForTabs(bibleNavParent);
  const isInVerseVideo =
    window.location.pathname.includes('stih') && bibleNavParent.type.includes('video');
  return (
    <Tabs
      activeTab={activeContent}
      callBack={handleChange}
      className={cx('tabs_in-paper bible-media-interpretations', {
        'bible-media-tabs-right': !isShowBibleMediaMiddle,
      })}
      type="justify">
      {!isInVerseVideo && (
        <Tab label={labelTextLeft.label} subscript={labelTextLeft.detailing} value={1} className="tab-common" classNameTabItem="tab-left" />
      )}
      {!isInVerseVideo && (
        <Tab label={labelTextRight.label} subscript={labelTextRight.detailing} value={2} className="tab-common" classNameTabItem="tab-right" />
      )}
      <TabContent>{children}</TabContent>
    </Tabs>
  );
};

BibleMediaTabs.propTypes = {
  isShowBibleMediaMiddle: PropTypes.bool,
  children: PropTypes.node,
};

export default BibleMediaTabs;
