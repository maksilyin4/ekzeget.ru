const labelsQuestions = [{label: 'По тексту'}, {label: 'По толкованиям'}];
const labelsOther = [{label: 'К главе'}, {label: 'К стихам'}];
const labelInter = [{label: 'Толкования', detailing: 'Святых'}, {label: 'Пояснения', detailing: 'экзегетов'}];

export const getLabelTextForTabs = bibleNavParent => {
  switch (bibleNavParent.type) {
    case 'questions':
      return labelsQuestions;
    case 'interpretations':
      return labelInter;
    default:
      return labelsOther;
  }
};
