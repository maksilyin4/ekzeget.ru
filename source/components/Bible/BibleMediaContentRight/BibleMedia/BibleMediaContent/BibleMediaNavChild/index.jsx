import React from 'react';

import PropTypes from 'prop-types';

import BibleMediaBtn from '../../BibleMediaBtn';

import './bibleMediaNavChild.scss';

const BibleMediaNavChild = ({
  className = 'bible-media-nav-child',
  bibleMediaCategories,
  toggleMediaType,
  activeMedia,
  typeMedia = 'mediaCat_',
}) => {
  const type = activeMedia ? activeMedia.type : `${typeMedia}all`;

  return (
    bibleMediaCategories.length > 0 && (
      <nav className={className}>
        {bibleMediaCategories.map(item => (
          <BibleMediaBtn
            key={item.id}
            title={item.title}
            code={`${typeMedia}${item.code || 'all'}`}
            type={type}
            onClick={toggleMediaType}
          />
        ))}
      </nav>
    )
  );
};

BibleMediaNavChild.propTypes = {
  bibleMediaCategories: PropTypes.object.isRequired,
  toggleMediaType: PropTypes.func,
  activeMedia: PropTypes.object,
};

export default BibleMediaNavChild;
