import React, { useMemo } from 'react';
import { Link, withRouter } from 'react-router-dom';
import AudioPlayer from '../../../../../../../../AudioPlayer/AudioPlayer';
import AudioTemplateItem from './AudioTemplateItem';

import { updatePathname } from '~utils/updatePathname';

import './audioTemplate.scss';

const AudioTemplate = ({
  data = {},
  categoryId = '',
  categoryChildId = '',
  bookId = '',
  chapterId = '',
  location: { pathname },
}) => {
  const { id, title, author, code, audio_href, media_bible } = data;

  const mediaBible = useMemo(
    () => media_bible.filter(({ chapter_id, verse }) => +chapter_id === +chapterId && verse),
    [media_bible, chapterId],
  );

  if (!audio_href) {
    return null;
  }
  const subCategory = categoryChildId !== 'all' ? `detail-${categoryChildId}/` : '';

  return (
    <div className="mb__item mb__item_audio" key={id}>
      <div className="mb-audio-head">
        <Link
          to={`/mediateka/${categoryId}/${subCategory}detail-${code}/?book=${bookId}&chapter=${chapterId}`}>
          <AudioTemplateItem title={title} className="mb-audio-title" />
        </Link>

        <AudioTemplateItem title={author && author.title} className="mb-audio-span" />
        <div className="mb-audio-verses">
          {mediaBible.map(({ id, verse }) => {
            const verseNumber = verse?.number;
            return (
              verseNumber && (
                <Link
                  key={id}
                  to={`${updatePathname(pathname)}stih-${verseNumber}`}
                  className="mb-audio-verse-link mb-audio-span">
                  {`Cт. ${verseNumber}`}
                </Link>
              )
            );
          })}
        </div>
      </div>
      <AudioPlayer src={audio_href} />
    </div>
  );
};

export default withRouter(AudioTemplate);
