import React from 'react';
import cx from 'classnames';

import './audioTempalteItem.scss';

const AudioTemplateItem = ({ title, className }) =>
  title && <p className={cx('audio-item', className)}>{title}</p>;

export default AudioTemplateItem;
