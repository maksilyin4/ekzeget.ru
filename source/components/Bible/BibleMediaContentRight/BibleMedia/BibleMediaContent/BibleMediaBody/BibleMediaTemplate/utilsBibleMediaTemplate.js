// TODO удалить если не нужно будет 14.01.20
// ппц но заказчик то так то сяк говорит - пока сделал по простому

export const getChapterOnly = (isNotTabs, verseId, multiTabChapter) => {
  if (isNotTabs && !verseId) {
    // return isNotTabs;
    return undefined;
  }
  if (isNotTabs && verseId) {
    // return !isNotTabs;
    return undefined;
  }
  return multiTabChapter;
};

export const getVerseOnly = (isNotTabs, verseId, multiTabVerse) => {
  if (isNotTabs && !verseId) {
    // return !isNotTabs;

    return undefined;
  }
  if (isNotTabs && verseId) {
    // return isNotTabs;
    return undefined;
  }
  return multiTabVerse;
};

export const useMultiTab = (interMultiTab = 1, multiTab) => {
  const multiTabChapter = interMultiTab && +interMultiTab === multiTab.chapter;
  const multiTabVerse = interMultiTab && +interMultiTab === multiTab.verse;
  return [multiTabChapter, multiTabVerse];
};
