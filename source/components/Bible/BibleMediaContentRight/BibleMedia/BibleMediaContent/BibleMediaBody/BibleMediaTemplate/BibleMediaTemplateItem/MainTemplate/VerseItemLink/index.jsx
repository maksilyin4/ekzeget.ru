import React from 'react';
import { Link } from 'react-router-dom';

import { bible } from '~utils/common';

const VerseItemLink = ({ mediaBible }) =>
  mediaBible.map(
    ({ id, verse, book, chapter }) =>
      // использовал optional chaining так как с бэка может прийти null
      verse?.number && (
        <Link
          key={id}
          to={`/${bible}/${book?.code}/glava-${chapter?.number}/stih-${verse?.number}/`}
          className="mb-video-verse">
          {`Cт. ${verse?.number}`}
        </Link>
      ),
  );

export default VerseItemLink;
