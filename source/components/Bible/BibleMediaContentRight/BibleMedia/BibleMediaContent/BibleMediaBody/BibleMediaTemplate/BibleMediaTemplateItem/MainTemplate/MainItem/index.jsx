import React from 'react';
import { Link } from 'react-router-dom';

export const MainItem = ({ url = '', text = '', className = '' }) =>
  url && (
    <Link to={url}>
      <span className={className}>{text}</span>
    </Link>
  );
