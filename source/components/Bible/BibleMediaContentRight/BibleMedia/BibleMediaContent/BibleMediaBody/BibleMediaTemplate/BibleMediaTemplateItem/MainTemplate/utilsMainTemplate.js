import { mediateka } from '~utils/common';
import { getType } from '~pages/Mediateka/MediaBody/MediaContent/MediaContentList/utilsMediaContentList';
import { urlToAPI } from '~dist/browserUtils';

export const getUrlForLink = ({ data, categoryId, bookId, template }) => {
  const { media_bible } = data;
  const infoBook = media_bible.find(({ book_id }) => +book_id === +bookId) || {};

  const { chapter_id, verse_id } = infoBook;
  return data.template === template
    ? `/${mediateka}/${categoryId}/?book=${bookId}&chapter=${chapter_id}&verse=${verse_id}&motivator=${data.id}`
    : `/${mediateka}/detail-${data.code}/`;
};

export const getUrlForImg = data =>
  data.image
    ? `${urlToAPI}${data.image.url}`
    : `/frontassets/images/mediaImg/media-${getType(data.type)}.png`;
