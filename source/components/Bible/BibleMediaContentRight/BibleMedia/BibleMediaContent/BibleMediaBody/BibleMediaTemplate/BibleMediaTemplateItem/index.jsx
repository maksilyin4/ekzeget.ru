import React from 'react';

import './style.scss';

import AudioTemplate from './AudioTemplate';
import MainTemplate from './MainTemplate';

export const BibleMediaTemplateItem = ({ data, categoryId, categoryChildId, ...props }) => {
  const getMediaTemplate = () => {
    switch (data.type) {
      case 'A':
        return (
          <AudioTemplate
            data={data}
            categoryId={categoryId}
            categoryChildId={categoryChildId}
            {...props}
          />
        );
      default:
        return <MainTemplate data={data} categoryId={categoryId} {...props} />;
    }
  };

  return getMediaTemplate();
};
