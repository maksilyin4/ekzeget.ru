import React, { useCallback, useMemo, useContext } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { useCookies } from 'react-cookie';
import { useSelector } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroll-component';

import './bibleMediaTemplate.scss';

import { BibleMediaContext } from '../../BibleMediaContext';

import screenReducer from '~dist/selectors/screen';
import useMediaQuery from '~apollo/query/mediaLibrary/media';

import { BibleMediaTemplateItem } from './BibleMediaTemplateItem';
import Preloader from '~components/Preloader/Preloader';
import BibleMediaBodyNotContent from '../BibleMediaBodyNotContent/';

import { fetchLoadScroll } from '~pages/Mediateka/MediaBody/utilsMediaBody';
import {
  getParseMedia,
  getCategoryId,
  getSortMedia,
  getCategoryCode,
  multiTab,
} from '../../utilsBibleMediaTemplate';
import { getChapterOnly, getVerseOnly, useMultiTab } from './utilsBibleMediaTemplate';
import { categoryIdNotShowTabs } from '../../BibleMediatekaSort/utilsMediaNavChild';
import { setStyleHeight, fetchInfiniteScroll } from '~utils/common';

const BibleMediaTemplate = ({ isShowBibleMediaMiddle, type, chapter, verseId }) => {
  const { sorts, content, left, isShowBibleMediaNav } = useContext(BibleMediaContext);

  const screen = useSelector(screenReducer);

  const [{ interMultiTab, bibleNavChild, heightLeftMenuBible }] = useCookies([
    'interMultiTab',
    'bibleNavChild',
    'heightLeftMenuBible',
  ]);

  const categoryId = getCategoryId(type, 'mediaCat_');
  const categoryChildId = getCategoryId(bibleNavChild?.type, 'mediaCatChild_');
  const category_code = getCategoryCode(categoryChildId, categoryId, interMultiTab);

  const [multiTabChapter, multiTabVerse] = useMultiTab(interMultiTab, multiTab);

  const isNotTabs = categoryIdNotShowTabs.includes(categoryId); // true = нет табов

  const bookId = +chapter.book_id;
  const chapterId = +chapter.id;
  const connected_to_book_id = +chapter.book_id;
  const connected_to_chapter_id = +chapter.id;
  const connected_to_verse_id = Number(verseId);

  const connected_to_verse_only = getVerseOnly(isNotTabs, verseId, multiTabVerse);
  const connected_to_chapter_only = getChapterOnly(isNotTabs, verseId, multiTabChapter);

  const connected = verseId ? { connected_to_verse_id } : { connected_to_chapter_id };

  const { order, orderBy } = getSortMedia(sorts, categoryId, multiTabVerse);

  const variables = {
    category_code,
    offset: 0,
    limit: 10,
    order,
    orderBy,
    connected_to_book_id,
    connected_to_verse_only,
    connected_to_chapter_only,
    ...connected,
  };

  const { medias, loading, fetchMore, isLoading, total } = useMediaQuery(variables);

  const media = useMemo(() => getParseMedia(medias, screen, content, left, isShowBibleMediaNav), [
    medias,
    screen,
    content,
    left,
    isShowBibleMediaNav,
  ]);

  // scroll loading media
  const onScrollContent = useCallback(
    event => {
      if (!loading && medias.length < total) {
        fetchLoadScroll({ event, data: medias, fetchMore, variables });
      }
    },
    [medias, loading],
  );

  const onScrollInfiniteMethod = useCallback(
    () => medias.length < total && fetchInfiniteScroll({ data: medias, fetchMore, variables }),
    [medias, loading, total],
  );

  const renderContent = useMemo(
    () => (
      <ul className="mb__list">
        {media.map((el, ind) => {
          return (
            <li
              className={cx('mb-item', {
                'mb-item-audio': el.every(({ type }) => type === 'A'),
              })}
              key={ind}>
              {el.map(item => (
                <BibleMediaTemplateItem
                  key={item.id}
                  data={item}
                  categoryId={categoryId}
                  categoryChildId={categoryChildId}
                  bookId={bookId}
                  chapterId={chapterId}
                  verseId={verseId}
                  interMultiTab={interMultiTab}
                />
              ))}
            </li>
          );
        })}
        {loading && <Preloader className="bible-media__empty" />}
      </ul>
    ),
    [media, loading, type],
  );

  if (isLoading) {
    return <Preloader className="bible-media__empty" />;
  }

  if (media.length === 0) {
    return <BibleMediaBodyNotContent />;
  }

  return screen.phone ? (
    <InfiniteScroll
      dataLength={media.length}
      next={onScrollInfiniteMethod}
      scrollThreshold="50%"
      hasMore>
      {renderContent}
    </InfiniteScroll>
  ) : (
    <div
      style={setStyleHeight({ height: heightLeftMenuBible, defaultHeight: 242 })}
      className={cx('mb__content', {
        'mb-middle': !isShowBibleMediaMiddle,
      })}
      onScroll={onScrollContent}>
      {renderContent}
    </div>
  );
};

BibleMediaTemplate.propTypes = {
  isShowBibleMediaMiddle: PropTypes.bool,
  chapter: PropTypes.object,
  type: PropTypes.string,
  verseId: PropTypes.string,
};

export default BibleMediaTemplate;
