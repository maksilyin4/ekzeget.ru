import React, { useMemo, useCallback } from 'react';
import { Link } from 'react-router-dom';
import cx from 'classnames';
import loadable from '@loadable/component';

import VerseItemLink from './VerseItemLink/';
import { MainItem } from './MainItem';

import { getUrlForLink, getUrlForImg } from './utilsMainTemplate';
import { defaultTemplate } from '~pages/Mediateka/MediaBody/utilsMediaBody';
import { multiTab } from '../../../../utilsBibleMediaTemplate';

const ExternalVideo = loadable(() => import('~components/ExternalVideo'), { ssr: false })

const MainTemplate = ({ data = {}, categoryId, bookId, chapterId, verseId, interMultiTab }) => {
  const { media_bible, id, template, title, description, author = {}, video_href } = data;

  const isMotivator = template === defaultTemplate.motivator;
  const isVideo = template === defaultTemplate.video;

  const isShowVerse = +interMultiTab === multiTab.verse && isVideo;

  const setFilterMediaBible = useCallback(
    ({ book_id, chapter_id, verse_id }) => {
      const isVerseOrChapter = verseId ? +verse_id !== +verseId : +chapter_id === +chapterId;
      return +book_id === +bookId && isVerseOrChapter;
    },
    [verseId, bookId, chapterId],
  );

  const mediaBible = useMemo(() => media_bible.filter(setFilterMediaBible), [
    setFilterMediaBible,
    media_bible,
  ]);

  const url = getUrlForLink({
    data,
    categoryId,
    bookId,
    template: defaultTemplate.motivator,
  });

  const titleText = title && !isMotivator ? title : data?.book?.title || '';

  return (
    <div className="mb__item mb__item_visual" key={id}>
      <div
        className={cx('mb__item-visual', {
          'mb-img': !isVideo,
        })}>
        {isVideo ? (
          <ExternalVideo className="external-video-padding" data={video_href} />
        ) : (
          <Link className="mb-img" to={url}>
            <img
              className={cx({
                'mb-img-motivator': isMotivator,
              })}
              src={getUrlForImg(data)}
              alt={`${title} ${description}`}
            />
          </Link>
        )}
      </div>

      <div className="mb__item-info">
        <div
          className={cx('mb-video-title', {
            'mb-motivator-title': isMotivator,
          })}>
          <MainItem key="title" url={url} text={titleText} className="mb__item-title" />
        </div>
        {author?.title && !isMotivator && (
          <MainItem key="author" url={url} text={author.title} className="mb__item-author" />
        )}

        {(isShowVerse || !isVideo) && (
          <div className="mb-video-verse-content">
            <VerseItemLink mediaBible={mediaBible} />
          </div>
        )}
      </div>
    </div>
  );
};

export default MainTemplate;
