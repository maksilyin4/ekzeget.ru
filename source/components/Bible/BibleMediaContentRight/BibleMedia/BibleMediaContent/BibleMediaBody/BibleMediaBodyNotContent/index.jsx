import React from 'react';

const BibleMediaBodyNotContent = ({ content = 'Нет контента' }) => (
  <div className="bible-media__empty">{content}</div>
);

export default BibleMediaBodyNotContent;
