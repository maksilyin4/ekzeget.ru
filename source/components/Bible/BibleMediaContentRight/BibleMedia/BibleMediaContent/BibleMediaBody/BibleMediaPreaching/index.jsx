import React, { useEffect, useContext, useMemo } from 'react';
import { useCookies } from 'react-cookie';
import { useSelector, useDispatch } from 'react-redux';

import { BibleMediaContext } from '../../BibleMediaContext';

import './style.scss';

import List, { ListItem } from '~components/List/List';
import Preloader from '~components/Preloader/Preloader';
import { getHref } from '~pages/Bible/utilsBible';
import { getPreachingByBook } from '~dist/actions/BiblePreaching';
import { getPreachingIsLoading, getPreachingForBible } from '~dist/selectors/BiblePreaching';

import { setStyleHeight } from '~utils/common';

const BibleMediaPreaching = ({ bookCode, numberChapter }) => {
  const { sorts } = useContext(BibleMediaContext);

  const [{ heightLeftMenuBible }] = useCookies(['heightLeftMenuBible']);

  const dispatchRedux = useDispatch();
  const isLoading = useSelector(getPreachingIsLoading);
  const preachersBible = useSelector(getPreachingForBible);

  useEffect(() => {
    dispatchRedux(getPreachingByBook({ book: bookCode, chapter: numberChapter }));
  }, [bookCode, numberChapter]);

  const preacherData = useMemo(() => {
    const { sort, sortDirection } = sorts || {};
    return [...preachersBible].sort((a, b) => {
      if (sort === 'alph') {
        return sortDirection
          ? a.preacher.name.localeCompare(b.preacher.name)
          : b.preacher.name.localeCompare(a.preacher.name);
      }
    });
  }, [preachersBible, sorts]);

  return !isLoading ? (
    <div
      className="bible-media-preacher"
      style={setStyleHeight({ height: heightLeftMenuBible, defaultHeight: 176 })}>
      {!preacherData.length ? (
        <div className="empty-content">Нет контента</div>
      ) : (
        <List>
          {preacherData.map(({ excuse, preacher, id, theme }) => {
            const href = getHref({ excuse, preacher });
            return <ListItem key={id} title={preacher.name} href={href} themePreaching={theme} />;
          })}
        </List>
      )}
    </div>
  ) : (
    <Preloader />
  );
};

export default BibleMediaPreaching;
