import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import { getFavoriteList } from '~dist/selectors/favorites';

import Button from '~components/Button/Button';
import BibleMediaBodyNotContent from '../BibleMediaBodyNotContent/';

import './style.scss';

const BibleMediaFavoriteVerses = ({ editFavorite, deleteFavorite, bookId, chapterNumber }) => {
  const favoritesVerseList = useSelector(getFavoriteList);

  const favoritesVerseForChapter = useMemo(
    () =>
      favoritesVerseList
        .filter(
          ({ verse: { book_id, chapter } }) => +book_id === bookId && +chapter === chapterNumber,
        )
        .sort((a, b) => Number(a.verse.number) - Number(b.verse.number)),
    [favoritesVerseList, bookId, chapterNumber],
  );

  return (
    <div className="favorites-verses">
      {favoritesVerseForChapter.length ? (
        <ul>
          {favoritesVerseForChapter.map(({ verse, id, tags }) => (
            <li className="favorites-verse" key={id}>
              <div className="favorites-verse__content">
                <span className="favorites-verse__num">{`Стих ${verse.number}`}</span>
                <span className="favorites-verse__tags">{tags.replace(/\,/g, ', ')}</span>
              </div>
              <div className="favorites-verse__actions">
                <Button
                  key="edit"
                  icon="edit"
                  alt="Редактировать"
                  type="circle"
                  onClick={() => editFavorite(verse.id)}
                  className="favorites-verse__action btn_circle"
                />
                <Button
                  key="delete"
                  icon="close"
                  alt="удалить"
                  type="circle"
                  onClick={() => deleteFavorite({ id, verseId: verse.id })}
                  className="favorites-verse__action btn_circle"
                />
              </div>
            </li>
          ))}
        </ul>
      ) : (
        <BibleMediaBodyNotContent />
      )}
    </div>
  );
};

BibleMediaFavoriteVerses.propTypes = {
  editFavorite: PropTypes.func,
  deleteFavorite: PropTypes.func,
  bookId: PropTypes.number,
  chapterNumber: PropTypes.number,
};

export default BibleMediaFavoriteVerses;
