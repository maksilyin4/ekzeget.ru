import React, { useState } from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';

import './bibleMediaSort.scss';

import Icon from '~components/Icons/Icons';
import BibleMediaSortItem from './BibleMediaSortItem';
import { RenderTypes } from './RenderTypes/';

const BibleMediaSort = ({
  sortDirection = false,
  sort = '',
  sortChrone = f => f,
  sortAlph = f => f,
  ekzegetFilter = [],
  ekzegetTypes = [],
  sortType = f => f,
  defaultId,
  isDisabledChrone,
  isDisabledTypes,
  researchFilter = false,
  setResearchFilter = f => f,
}) => {
  const [isListShow, setIsListShow] = useState(false);

  const handleClickSort = () => {
    setIsListShow(!isListShow);
  };
  return (
    <div className="bible-media-sorts">
      <BibleMediaSortItem
        active={sort === 'alph'}
        sortDirection={sortDirection}
        onClick={sortAlph}
        icon="sort_alph"
      />
      {!isDisabledChrone && (
        <BibleMediaSortItem
          active={sort === 'chrone'}
          sortDirection={sortDirection}
          onClick={sortChrone}
          icon="sort_chron"
          isDisabled={isDisabledChrone}
        />
      )}
      {ekzegetTypes.length > 0 && !isDisabledTypes && (
        <div className="bible-media-sort-popup">
          <div
            className={cx('bible-media-sort bible-inter-book', {
              active: isListShow,
              disabled: isDisabledTypes
            })}
            onClick={handleClickSort}>
            <Icon icon="sort_type" />
            <span className="filter-indicator">{ekzegetFilter.length + researchFilter || ''}</span>
          </div>
          {isListShow && (
            <RenderTypes
              ekzegetFilter={ekzegetFilter}
              setResearchFilter={setResearchFilter}
              researchFilter={researchFilter}
              ekzegetTypes={ekzegetTypes}
              sortType={sortType}
              defaultId={defaultId}
              setIsListShow={setIsListShow}
            />
          )}
        </div>
      )}
    </div>
  );
};

BibleMediaSort.propTypes = {
  sortChrone: PropTypes.func,
  sortAlph: PropTypes.func,

  sortDirection: PropTypes.bool,
  ekzegetFilter: PropTypes.array,
  researchFilter: PropTypes.bool,
  setResearchFilter: PropTypes.func,
  sort: PropTypes.string,
};

export default BibleMediaSort;
