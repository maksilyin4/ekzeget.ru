import React, { useEffect } from 'react';
import cx from 'classnames';
import { useSelector } from 'react-redux';

import './renderTypes.scss';

import { getIsAdaptive } from '~dist/selectors/screen';

import List, { ListItem } from '~components/List/List';
import Icon from "~components/Icons/Icons";
import {HOLY_FATHERS_ID} from "~dist/utils";

const popupTypes = 'popup-render-types';

export const RenderTypes = ({
  ekzegetFilter = [],
  ekzegetTypes = [],
  sortType,
  defaultId,
  setIsListShow,
  researchFilter,
  setResearchFilter,
}) => {
  const isAdaptive = useSelector(getIsAdaptive);
  useEffect(() => {
    const event = isAdaptive ? 'touchstart' : 'click';
    const delay = isAdaptive ? 300 : 0;
    const listener = event => {
      if (!event.target.classList.contains(popupTypes)) {
        setTimeout(() => {
          setIsListShow(false);
        }, delay);
      }
    };

    window.addEventListener(event, listener);
    return () => window.removeEventListener(event, listener);
  }, []);

  return (
    <List className="bible-types-list">
      <ListItem
          linkClassName={popupTypes}
          className={cx(popupTypes, { active: researchFilter }, 'research')}
          style={{"border-bottom": "8px  solid gold"}}
          key={defaultId}
          title={<span className={cx(popupTypes)}><Icon icon="research" />Исследования</span>}
          onClick={() => setResearchFilter(!researchFilter)}
      >
      </ListItem>
      <ListItem
        linkClassName={popupTypes}
        className={cx(popupTypes, { active: !ekzegetFilter.length })}
        key={defaultId}
        title={<span className={cx(popupTypes)}>Все экзегеты</span>}
        onClick={() => sortType(defaultId)}
      />
      {ekzegetTypes.map(
        type =>
          type &&
          type.id !== 0 && type.id !== HOLY_FATHERS_ID && (
            <ListItem
              linkClassName={popupTypes}
              className={cx({ active: ekzegetFilter.includes(type.id) })}
              key={type.id}
              title={<span className={cx(popupTypes)}>{type.title}</span>}
              onClick={() => sortType(type.id)}
            />
          ),
      )}
    </List>
  );
};
