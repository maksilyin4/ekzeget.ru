import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';

import Icon from '~components/Icons/Icons';

const BibleMediaSortItem = ({
  sortDirection,
  onClick,
  icon = 'sort_alph',
  active = false,
  isDisabled,
}) => {
  const clickHandler = isDisabled ? null : onClick;
  return (
    <button
      className={cx('bible-media-sort', {
        active,
        asc: sortDirection,
        desc: !sortDirection,
        disabled: isDisabled,
      })}
      onClick={clickHandler}>
      <Icon icon={icon} />
    </button>
  );
};

BibleMediaSortItem.propTypes = {
  onClick: PropTypes.func,
  sortDirection: PropTypes.bool,
  active: PropTypes.bool,
  icon: PropTypes.string,
  isDisabled: PropTypes.bool,
};

export default BibleMediaSortItem;
