import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useCookies } from 'react-cookie';

import Tabs, { Tab, TabContent } from '~components/Tabs/Tabs';

import './style.scss';

const BibleMediaInterMultiTab = ({ children }) => {
  const [cookies, setCookies] = useCookies(['interMultiTab']);
  const { interMultiTab } = cookies;

  const [activeContent, setActiveContent] = useState(+interMultiTab || 1);

  const handleChange = value => {
    setActiveContent(value);
    setCookies('interMultiTab', value, { path: '/' });
  };

  return (
    <Tabs
      activeTab={activeContent}
      callBack={handleChange}
      className="tabs_in-paper bible-media-interpretations"
      type="justify">
      <Tab label="К стихам" value={1} className="tab-common" classNameTabItem="tab-left" />
      <Tab label="К главе" value={2} className="tab-common" classNameTabItem="tab-right" />
      <TabContent>{children}</TabContent>
    </Tabs>
  );
};

BibleMediaInterMultiTab.propTypes = {
  children: PropTypes.node,
};

export default BibleMediaInterMultiTab;
