import React, { useMemo, useContext } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { useCookies } from 'react-cookie';
import { BibleMediaContext } from '../../BibleMediaContext';

import './style.scss';

import List from '~components/List/List';
import FilteredInterpretations from './FilteredInterpretations';

import { setFilterInter, setSortInter } from './utilsBibleMediaInterTabs';

import { setStyleHeight } from '~utils/common';

const BibleMediaInterTabs = ({
  isLoading,
  verseCurrent,
  verseNumber,
  activeItem,
  interpretators,
  hrefLink,
  className,
  isAddedEkzeget,
  sorts = {},
}) => {
  const { ekzegetFilter, researchFilter, onCloseInter } = useContext(BibleMediaContext);
  const { sort, sortDirection } = sorts;

  const [{ heightLeftMenuBible, interMultiTab }] = useCookies([
    'heightLeftMenuBible',
    'interMultiTab',
  ]);

  const activeContent = +interMultiTab || 1;

  const filteredInterpretations = useMemo(() => {
    const sortInter = setSortInter(interpretators, sort, sortDirection);

    return sortInter.filter(setFilterInter(ekzegetFilter, researchFilter, activeContent));
  }, [activeContent, interpretators, ekzegetFilter, researchFilter, sort, sortDirection]);

  return (
    <List
      className={cx('side__interpretators', className)}
      style={setStyleHeight({ height: heightLeftMenuBible, defaultHeight: 262 })}>
      <FilteredInterpretations
        handleClick={onCloseInter}
        filteredInterpretations={filteredInterpretations}
        hrefLink={hrefLink}
        isLoading={isLoading}
        verseCurrent={verseCurrent}
        verseNumber={verseNumber}
        activeItem={activeItem}
        activeContent={activeContent}
        isAddedEkzeget={isAddedEkzeget}
        sort={sort}
        className="bible-interpretators-items"
      />
    </List>
  );
};

BibleMediaInterTabs.propTypes = {
  isLoading: PropTypes.bool,
  verseCurrent: PropTypes.string,
  verseNumber: PropTypes.number,
  activeItem: PropTypes.string,
  hrefLink: PropTypes.string,
};

export default BibleMediaInterTabs;
