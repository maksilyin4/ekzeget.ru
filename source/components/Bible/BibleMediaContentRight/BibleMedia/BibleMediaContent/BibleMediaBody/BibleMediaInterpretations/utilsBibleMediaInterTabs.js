import R from 'ramda';
import {HOLY_FATHERS_ID} from "~dist/utils";

export const getSort = (one, two) => one?.name.localeCompare(two?.name);

export const setSortInter = (interpretators, sort, sortDirection) => {
  const interpretatorsNew = [...interpretators];
  if (sort === 'chrone') {
    return interpretatorsNew.sort((a, b) =>
      sortDirection ? Number(a.century) - Number(b.century) : Number(b.century) - Number(a.century),
    );
  }
  return interpretatorsNew.sort((a, b) =>
    sortDirection ? a.name.localeCompare(b.name) : b.name.localeCompare(a.name),
  );
};

export const setFilterInter = (ekzegetFilter = [], researchFilter, activeContent) => ({
  investigated,
  ekzeget_type: { id: type_id },
}) => {
  return (
      (activeContent === 1 && type_id === HOLY_FATHERS_ID)
      || (activeContent === 2
          && (type_id !== HOLY_FATHERS_ID
              && (ekzegetFilter.includes(type_id) || R.isEmpty(ekzegetFilter))
              && (!researchFilter || researchFilter && investigated) ))
  );
};
