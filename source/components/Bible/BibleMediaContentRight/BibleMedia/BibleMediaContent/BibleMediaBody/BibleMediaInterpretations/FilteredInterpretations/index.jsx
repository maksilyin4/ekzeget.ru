import R from 'ramda';
import React, { Fragment } from 'react';
import cx from 'classnames';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import { userLoggedIn } from '~dist/selectors/User';

import Preloader from '~components/Preloader/Preloader';
import { ListItem } from '~components/List/List';

import { getHref, getIsFav, getTitleInter } from './utilsFiltered';

const FilteredInterpretations = ({
  filteredInterpretations,
  isLoading,
  activeContent,
  sort,
  verseNumber,
  handleClick,
  activeItem,
  hrefLink,
  isAddedEkzeget,
  className = '',
}) => {
  const isUserLoggedIn = useSelector(userLoggedIn);

  if (!filteredInterpretations.length) {
    return (
      <div className="empty-content empty-content_side">
        {activeContent === 1 ? 'Толкования отсутствуют' : 'Пояснения отсутствуют'}
      </div>
    );
  }

  const titleWithoutInter = getTitleInter(verseNumber, activeContent);

  return (
    <>
      <ListItem
        onClick={handleClick}
        className={cx('filtered-interpretations-all', { active: !activeItem })}
        title={titleWithoutInter}
        href={getHref({ hrefLink, verseNumber })}
      />

      {isLoading ? (
        <Preloader />
      ) : (
        <div className={cx(className)}>
          {filteredInterpretations.map((ekzeget, ind) => (
            <Fragment key={ekzeget.id}>
              {sort === 'chrone' &&
                ekzeget.century &&
                R.pathOr(0, ['century'], filteredInterpretations[ind - 1]) !== ekzeget.century && (
                  <span className="side__interpretators-century">{ekzeget.century} век</span>
                )}
              <ListItem
                onClick={handleClick}
                className={cx({ active: activeItem && activeItem.includes(ekzeget.code) })}
                title={ekzeget.name}
                isFav={getIsFav({ isUserLoggedIn, isAddedEkzeget, ekzeget })}
                href={getHref({ ekzeget, hrefLink, verseNumber })}
              />
            </Fragment>
          ))}
        </div>
      )}
    </>
  );
};

FilteredInterpretations.propTypes = {
  filteredInterpretations: PropTypes.array,
  isLoading: PropTypes.bool,
  isAddedEkzeget: PropTypes.bool,
  hrefLink: PropTypes.string,
  activeItem: PropTypes.string,
  verseNumber: PropTypes.string,
  sort: PropTypes.string,
  activeContent: PropTypes.number,
};
export default FilteredInterpretations;
