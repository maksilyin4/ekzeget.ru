import React, { Component } from 'react';

import { withRouter } from 'react-router-dom';

import { compose } from 'redux';
import { connect } from 'react-redux';

import interpretatorsActions from '~dist/actions/ChapterInterpretators';
import {
  getIsLoading,
  getInterpretators,
  getEkzegetTypes,
} from '~dist/selectors/ChapterInterpretators';

function Wrapper(WrappedComponent) {
  return withRouter(
    class SideInterpretationWrapper extends Component {
      componentDidMount() {
        const { verseNumber, bookCode, chapterNumber, getInterpretators } = this.props;

        if (chapterNumber) {
          getInterpretators(bookCode, chapterNumber, verseNumber);
        }
      }

      componentDidUpdate(prevProps) {
        const {
          verseNumber,
          bookCode,
          chapterNumber,
          getInterpretators,
          isAdded,
          location,
        } = this.props;

        if (
          chapterNumber &&
          (location.pathname !== prevProps.location.pathname ||
            prevProps.chapterNumber !== chapterNumber ||
            isAdded !== prevProps.isAdded)
        ) {
          getInterpretators(bookCode, chapterNumber, verseNumber);
        }
      }

      render() {
        const {
          chapterNumber,
          baseUrl,
          bookCode, // isLoading, interpretators,
        } = this.props;

        const hrefLink = `/${baseUrl}/${bookCode}/glava-${chapterNumber}/`;

        return <WrappedComponent {...this.props} {...this.state} hrefLink={hrefLink} />;
      }
    },
  );
}

const mapStateToProps = store => ({
  isLoading: getIsLoading(store),
  interpretators: getInterpretators(store),
  ekzegetTypes: getEkzegetTypes(store),
});
const mapDispatchToProps = {
  ...interpretatorsActions,
};

const composeHOC = compose(connect(mapStateToProps, mapDispatchToProps), Wrapper);
export default composeHOC;
