export const getHref = ({ ekzeget, hrefLink, verseNumber }) => {
  if (ekzeget) {
    return verseNumber
      ? `${hrefLink}stih-${verseNumber}/tolkovatel-${ekzeget.code}/`
      : `${hrefLink}tolkovatel-${ekzeget.code}/`;
  }
  return verseNumber ? `${hrefLink}stih-${verseNumber}/` : `${hrefLink}`;
};

export const getIsFav = ({ isUserLoggedIn, isAddedEkzeget, ekzeget }) =>
  isUserLoggedIn && isAddedEkzeget && isAddedEkzeget[ekzeget.id];

export const getTitleInter = (verseNumber, activeContent) => {
  if (verseNumber) {
    return activeContent === 1 ? 'Все толкования' : 'Все пояснения';
  }
  return 'Вернуться к главе';
};
