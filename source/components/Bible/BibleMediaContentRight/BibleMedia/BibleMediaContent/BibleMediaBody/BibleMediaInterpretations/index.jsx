import React from 'react';
import PropTypes from 'prop-types';

import Wrapper from './Wrapper';

// import BibleMediaInterMultiTab from './BibleMediaInterMultiTab';
import BibleMediaInterTabs from './BibleMediaInterTabs';

import './style.scss';

const BibleMediaInterpretations = ({ isVerse, ...props }) => {
  return <BibleMediaInterTabs {...props} isVerse />;
};

// TODO не понятно то надо то нет - если не нужно удалить 11.12.19
// return isVerse ? (
//   <BibleMediaInterTabs {...props} isVerse={isVerse} />
// ) : (
//   <BibleMediaInterMultiTab>
//     <BibleMediaInterTabs {...props} isVerse={isVerse} />
//   </BibleMediaInterMultiTab>
// );

BibleMediaInterpretations.propTypes = {
  isVerse: PropTypes.string,
};

export default Wrapper(BibleMediaInterpretations);
