import React, { memo } from 'react';

import BibleMediaInterpretations from './BibleMediaInterpretations';
import BibleMediaFavoriteVerses from './BibleMediaFavoriteVerses';
import BibleMediaQuestions from './BibleMediaQuestions';
import BibleMediaPreaching from './BibleMediaPreaching';
import BibleMediaTemplate from './BibleMediaTemplate/';

const BibleMediaBody = ({
  type,
  bookCode,
  activeItem,
  activeSideContent,
  verseCurrent,
  editFavorite,
  deleteFavorite,
  verseId,
  isShowBibleMediaMiddle,
  withoutScroll,
  chapter,
  isUpdate,
  baseUrl,
  isAddedEkzeget,
  sorts,
}) => {
  const chapters = chapter || {};

  const bookId = +chapters?.book_id;
  const numberChapter = +chapters?.number;

  const { chapter: chapterNum, number } = chapters;
  const chapterNumber = chapterNum || number;
  switch (type) {
    case 'interpretations': {
      return (
        <BibleMediaInterpretations
          bookCode={bookCode}
          baseUrl={baseUrl}
          chapterNumber={chapterNumber}
          activeItem={activeItem}
          isVerse={verseId}
          activeSideContent={activeSideContent}
          verseNumber={verseCurrent}
          isShowBibleMediaMiddle={isShowBibleMediaMiddle}
          isUpdate={isUpdate}
          isAddedEkzeget={isAddedEkzeget}
          sorts={sorts}
        />
      );
    }
    case 'favorites':
      return (
        <BibleMediaFavoriteVerses
          bookId={bookId}
          verseId={verseId}
          chapterNumber={+chapterNumber}
          editFavorite={editFavorite}
          deleteFavorite={deleteFavorite}
        />
      );
    case 'questions':
      return (
        Object.keys(chapters).length > 0 && (
          <BibleMediaQuestions withoutScroll={withoutScroll} verseId={verseId} chapter={chapters} />
        )
      );
    case 'preaching':
      return (
        <BibleMediaPreaching bookId={bookId} bookCode={bookCode} numberChapter={numberChapter} />
      );
    default:
      if (type.includes('mediaCat_')) {
        return (
          Object.keys(chapters).length > 0 && (
            <BibleMediaTemplate
              type={type}
              chapter={chapters}
              verseId={verseId}
              isShowBibleMediaMiddle={isShowBibleMediaMiddle}
            />
          )
        );
      }
      return null;
  }
};

export default memo(BibleMediaBody);
