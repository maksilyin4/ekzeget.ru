import React, { useEffect, useMemo, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useCookies } from 'react-cookie';
import InfiniteScroll from 'react-infinite-scroll-component';

import './bibleMediaQuestions.scss';

import { getBibleMediaQuestions } from '~store/bible/bibleMediaQuestions/actions';
import {
  bibleMediaQuestionsReducer,
  bibleMediaQuestionsIsLoading,
} from '~store/bible/bibleMediaQuestions/selectors';
import getScreen from '~dist/selectors/screen';

import BibleMediaQuestionsItem from './BibleMediaQuestionsItem';
import BibleMediaBodyNotContent from '../BibleMediaBodyNotContent/';
import QuestionsPreloader from './QuestionsPreloader/';
import Preloader from '~components/Preloader/Preloader';

import { getScrollBottom } from '~utils/getScrollBottom';
import { getBibleQuestionsData, getTypeForQuestions } from './utilsBibleQuestions';
import { bibleyskayaViktorina, setStyleHeight } from '~utils/common';
import { Link } from 'react-router-dom';

const BibleMediaQuestions = ({ verseId, chapter, withoutScroll }) => {
  const verse_id = +verseId;
  const book_id = +chapter?.book_id;
  const chapter_id = +chapter?.id;
  const id = verse_id || chapter_id;
  const bibleMediaQuestions = useSelector(bibleMediaQuestionsReducer);
  const isLoading = useSelector(bibleMediaQuestionsIsLoading);
  const screen = useSelector(getScreen);
  const dispatch = useDispatch();

  const [{ interMultiTab, heightLeftMenuBible }] = useCookies([
    'interMultiTab',
    'heightLeftMenuBible',
  ]);

  const type = getTypeForQuestions(interMultiTab);

  const questionId = id + type;

  const [data, totalCount, page] = useMemo(
    () => getBibleQuestionsData(bibleMediaQuestions[questionId]),

    [bibleMediaQuestions, questionId],
  );

  useEffect(() => {
    if (!bibleMediaQuestions[questionId] && !withoutScroll) {
      dispatch(getBibleMediaQuestions({ book_id, verse_id, chapter_id, perPage: 10, type }));
    }
  }, [verse_id, chapter_id, book_id, questionId, type]);

  const handleScrollInfinity = useCallback(() => {
    if (!withoutScroll) {
      return (
        data.length !== totalCount &&
        dispatch(getBibleMediaQuestions({ book_id, verse_id, chapter_id, page, type }))
      );
    }
  }, [data, book_id, verse_id, chapter_id, type, totalCount, withoutScroll]);

  const handleScroll = useCallback(
    e => {
      if (!withoutScroll) {
        const { currentTarget } = e;
        const scrollBottom = getScrollBottom(currentTarget);

        if (scrollBottom) {
          handleScrollInfinity();
        }
      }
    },
    [data, book_id, verse_id, chapter_id, type],
  );

  const searchParams = new URLSearchParams({
    is_related_to_text: `${type === 'T' ? 1 : 0}`,
    is_related_to_interpretations: `${type === 'I' ? 1 : 0}`,
    book_ids: [data[0]?.verse.book_id].join(','),
    chapter_id: `${data[0]?.verse.chapter_id}`,
    question_id: `${data[0]?.id}`,
  }).toString();

  const isShowLoader = data.length !== totalCount;
  const renderContent = useMemo(
    () =>
      data.length ? (
        <div>
          {data.length > 2 && (
            <Link
              className="navigation__item bm-quiz-link"
              to={{
                pathname: `/${bibleyskayaViktorina}/`,
                search: `?${searchParams}`,
              }}
              target="_blank">
              Викторина к главе
            </Link>
          )}
          {data.slice(0, withoutScroll ? 10 : Infinity).map(({ id, title, verse, code }, i) => (
            <BibleMediaQuestionsItem
              key={id}
              dataNum={++i}
              title={title}
              verse={verse}
              verseId={verseId}
              questionCode={code}
            />
          ))}

          <QuestionsPreloader isLoading={isLoading} isShowLoader={isShowLoader} />
        </div>
      ) : (
        <BibleMediaBodyNotContent />
      ),
    [data, isLoading, isShowLoader],
  );

  if (isLoading && !bibleMediaQuestions[questionId]) {
    return (
      <div className="bm-questions">
        <Preloader />
      </div>
    );
  }

  return screen.phone ? (
    <InfiniteScroll
      dataLength={data.length}
      next={handleScrollInfinity}
      scrollThreshold="50%"
      hasMore>
      {renderContent}
    </InfiniteScroll>
  ) : (
    <div
      style={setStyleHeight({ height: heightLeftMenuBible, defaultHeight: 200 })}
      className="bm-questions"
      onScroll={handleScroll}>
      {renderContent}
    </div>
  );
};

export default BibleMediaQuestions;
