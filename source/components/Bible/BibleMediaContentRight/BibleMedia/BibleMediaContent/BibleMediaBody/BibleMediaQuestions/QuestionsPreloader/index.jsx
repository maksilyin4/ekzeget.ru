import React from 'react';

import Preloader from '~components/Preloader/Preloader';

export default ({ isLoading, isShowLoader }) => {
  if (!isShowLoader) {
    return null;
  }

  return isLoading ? (
    <div key="loading" className="bm-question loading-questions">
      <Preloader />
    </div>
  ) : (
    <div className="loading-questions-not" />
  );
};
