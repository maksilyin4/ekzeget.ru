import { bible } from '~utils/common';

export const getNumberQuestion = num => (num ? `${num}.` : '');
export const getNumberVerse = verse => {
  const number = verse?.number;
  return number ? `Стих ${number}` : '';
};

export const getNumberShort = verse => {
  const short = verse?.short;
  return short ? `(${short})` : '';
};

const getNumberForUrl = (num, text = '') => (num ? `${text}${num}` : '');

export const getUrlQuestion = (verse = {}) =>
  verse.book_code
    ? `/${bible}/${verse.book_code}/${getNumberForUrl(verse.chapter, 'glava-')}/${getNumberForUrl(
        verse.number,
        'stih-',
      )}/`
    : `/${bible}/`;
