import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import {
  getNumberQuestion,
  getNumberVerse,
  getNumberShort,
  getUrlQuestion,
} from './utilsBibleQuesItem';
import { bibleyskayaViktorinaVoprosi } from '~utils/common';

const BibleMediaQuestionsItem = ({
  dataNum = 1,
  title,
  verse = {},
  questionCode
}) => {
  return (
    title && (
      <div className="bm-question">
        <div className="bm-question-head">
          <p className="bm-question-num">{getNumberQuestion(dataNum)}</p>
          <p className="bm-question-verse">{getNumberVerse(verse)}</p>
        </div>
        <Link
          className="bm-question-text"
          to={`${bibleyskayaViktorinaVoprosi}${questionCode}/`}
          target="_blank">
          {title}
        </Link>
        <br />
        {Object.keys(verse).length > 0 && (
          <Link className="bm-question-text" to={getUrlQuestion(verse)}>
            {getNumberShort(verse)}
          </Link>
        )}
      </div>
    )
  );
};

BibleMediaQuestionsItem.propTypes = {
  dataNum: PropTypes.number,
  title: PropTypes.string,
};

export default BibleMediaQuestionsItem;
