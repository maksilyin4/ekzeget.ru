const byText = 'T';
const byTextTab = 1;
const byInterpreters = 'I';

export const getBibleQuestionsData = (bibleMediaQuestions = {}) => {
  const questions = bibleMediaQuestions.questions || [];
  const totalCount = Number(bibleMediaQuestions.totalCount || 0);
  const page = bibleMediaQuestions.page || 2;
  return [questions, totalCount, page];
};

export const getTypeForQuestions = (interMultiTab = 1) => {
  if (+interMultiTab === byTextTab) {
    return byText;
  }
  return byInterpreters;
};
