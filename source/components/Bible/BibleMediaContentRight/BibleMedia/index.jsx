import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { useCookies } from 'react-cookie';
import { useSelector } from 'react-redux';

import './style.scss';

import getScreen from '~dist/selectors/screen';
import { userLoggedIn as getUserLoggedIn } from '~dist/selectors/User';

import useCategoriesQuery from '../../../../apolloClient/query/mediaLibrary/categories';
import BibleMediaContent from './BibleMediaContent';
import BibleMediaNavigators from './BibleMediaNavigators';

import { setDefaultBibleNavParent } from './utilsMediaBible';

const notShowCategories = ['molitvoslov'];

const BibleMedia = ({
  chapter,
  bookCode,
  activeItem,
  isUpdate,
  activeSideContent,
  callBack,
  baseUrl = 'bible',
  withoutScroll,
  editFavorite,
  deleteFavorite,
  verseId,
  extendLayout,
  verseCurrent,
  isAddedEkzeget,
  isBibleMobile,
  onCloseInter,
}) => {
  const screen = useSelector(getScreen);
  const userLoggedIn = useSelector(getUserLoggedIn);
  const [{ bibleNavParent, bibleNavChild }, setCookie] = useCookies([
    'bibleNavParent',
    'bibleNavChild',
  ]);

  const { categories, loading } = useCategoriesQuery();

  const bibleMediaCategories = React.useMemo(() => {
    return categories.filter(f => !notShowCategories.find(findItem => findItem === f.code));
  }, [categories]);

  useEffect(() => {
    const type = bibleNavParent?.type;

    setDefaultBibleNavParent({ verseId, type, setCookie, screen, chapter });
  }, [verseId, screen, chapter]);

  const toggleMediaType = nameCategory => (type = '', title) => {
    setCookie(nameCategory, { type, title }, { path: '/' });

    if (nameCategory === 'bibleNavParent' && bibleNavChild) {
      setCookie(
        'bibleNavChild',
        {
          type: 'mediaCatChild_all',
          title: type.includes('biblia-v-iskusstve') ? 'Все произведения' : 'Все произведения',
        },
        { path: '/' },
      );
    }
  };

  const isMediaContent = bibleNavParent && !loading;

  return (
    <div
      className={cx('bible-media', {
        'bible-media-not-empty': bibleNavParent,
      })}>
      {isMediaContent ? (
        <BibleMediaContent
          baseUrl={baseUrl}
          withoutScroll={withoutScroll}
          bibleMediaCategories={bibleMediaCategories}
          activeMedia={bibleNavParent}
          bibleNavChild={bibleNavChild}
          toggleMediaType={toggleMediaType}
          chapter={chapter}
          bookCode={bookCode}
          activeItem={activeItem}
          isUpdate={isUpdate}
          activeSideContent={activeSideContent}
          callBack={callBack}
          editFavorite={editFavorite}
          deleteFavorite={deleteFavorite}
          verseId={verseId}
          screen={screen}
          userLoggedIn={userLoggedIn}
          extendLayout={extendLayout}
          verseCurrent={verseCurrent}
          isAddedEkzeget={isAddedEkzeget}
          isBibleMobile={isBibleMobile}
          onCloseInter={onCloseInter}
        />
      ) : (
        <BibleMediaNavigators
          loading={loading}
          categories={bibleMediaCategories}
          toggleMediaType={toggleMediaType}
          userLoggedIn={userLoggedIn}
          verseId={verseId}
          isMobile={screen.phone}
          chapter={chapter}
        />
      )}
    </div>
  );
};

BibleMedia.propTypes = {
  chapter: PropTypes.any,
  bookCode: PropTypes.any,
  activeItem: PropTypes.any,
  isUpdate: PropTypes.any,
  activeSideContent: PropTypes.any,
  callBack: PropTypes.any,
  editFavorite: PropTypes.any,
  deleteFavorite: PropTypes.any,
  extendLayout: PropTypes.bool,
};

export default BibleMedia;
