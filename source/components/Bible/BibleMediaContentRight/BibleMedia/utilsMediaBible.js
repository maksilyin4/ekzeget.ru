import { getTitleBibleMediaNav } from './BibleMediaNav/utilsBibleMediaNav';

export const setDefaultBibleNavParent = ({ verseId, type, setCookie, screen, chapter }) => {
  const { phone, tablet } = screen;
  if ((typeof type === 'undefined' || type === 'bibleText') && phone) {
    return setCookie(
      'bibleNavParent',
      { type: 'bibleText', title: verseId ? getTitleBibleMediaNav(chapter) : 'Текст главы' },
      { path: '/' },
    );
  }

  if (
    type &&
    ((verseId && type.includes('preaching')) ||
      (verseId && type.includes('favorites')) ||
      (!phone && type.includes('interpretations')) ||
      (tablet && type.includes('bibleText')))
  ) {
    return setCookie(
      'bibleNavParent',
      { type: 'interpretations', title: 'Толкования' },
      { path: '/' },
    );
  }
};
