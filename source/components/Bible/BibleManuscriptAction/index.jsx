import React from 'react';
import cx from 'classnames';
import Icon from '~components/Icons/Icons';

import './bibleManuscriptAction.scss';

const getIconForAction = icon => {
  switch (icon) {
    case 'ekzeget':
      return (
        <Icon className="chapter__verses-talks" icon="icon-talks" title="Иконка толкователей" />
      );
    case 'dictionary':
      return 'Аа';
    default:
      return <Icon icon={icon} type={icon} />;
  }
};

const BibleManuscriptAction = ({
  onClick,
  icon,
  text,
  active,
  disabled,
  className,
  classNameIcon = 'chapter__verses-action-icon',
  title = '',
}) => (
  <div className={cx('chapter__verses-action', className)} title={title}>
    <button className="chapter__verses-action-link" onClick={onClick} disabled={disabled}>
      <span
        className={cx(classNameIcon, {
          'chapter__verses-action-icon_disabled': disabled,
          'chapter__verses-action-icon_active': active && !disabled,
        })}>
        {getIconForAction(icon)}
      </span>
      <span className="chapter__verses-action__text">{text}</span>
    </button>
  </div>
);

export default BibleManuscriptAction;
