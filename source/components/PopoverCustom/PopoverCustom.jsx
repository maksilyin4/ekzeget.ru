import { usePopper } from 'react-popper';
import React, { useEffect, useRef, useState } from 'react';
import { createPortal } from 'react-dom';
import { useClickAway } from 'react-use';

const PopoverCustom = ({
  placement = 'auto',
  children,
  classNameContent,
  className,
  innerContent,
}) => {
  const [referenceElement, setReferenceElement] = useState(null);
  const [popperElement, setPopperElement] = useState(null);
  const ref = useRef(popperElement);
  useEffect(() => {
    ref.current = popperElement;
  }, [popperElement]);

  useClickAway(ref, e => {
    if (referenceElement && !referenceElement.contains(e.target)) {
      setOpenPopover(false);
    }
  });

  const { styles, attributes } = usePopper(referenceElement, popperElement, {
    placement,
  });
  const [isOpenPopover, setOpenPopover] = useState(false);

  return (
    <>
      <button
        className={className}
        onClick={() => setOpenPopover(!isOpenPopover)}
        type="button"
        ref={setReferenceElement}>
        {children}
      </button>
      {isOpenPopover &&
        createPortal(
          <div
            className={classNameContent}
            ref={setPopperElement}
            style={styles.popper}
            {...attributes.popper}>
            {innerContent}
          </div>,
          document.body,
        )}
    </>
  );
};

export default PopoverCustom;
