import React, { useEffect, useState, memo } from 'react';
import PropTypes from 'prop-types';

import { Popup } from '../../dist/browserUtils';

import { getLearnVideo } from '../../store/learnVideo';

import './LearnVideo.scss';

const LearnVideo = ({ query = 'bible_tutorial_video_url' }) => {
  const [urlVideo, setUrlVideo] = useState(null);

  useEffect(() => {
    let isSubscribed = true;

    async function fetchLearnVideo() {
      try {
        const data = await getLearnVideo({ query });
        const url = data.data.value;
        setUrlVideo(url);
      } catch (err) {
        console.error(err);
      }
    }

    if (isSubscribed) {
      fetchLearnVideo();
    }

    return () => {
      isSubscribed = false;
    };
  }, []);

  const openPopup = () => {
    Popup.create(
      {
        title: null,
        content: (
          <iframe
            title="Обучающее видео"
            src={urlVideo}
            frameBorder="0"
            allowFullScreen
            className="learn-video"
          />
        ),
        className: 'learn-video-popup',
      },
      true,
    );
  };
  return (
    <button className="learn-btn" onClick={openPopup}>
      Обучающее видео
    </button>
  );
};

LearnVideo.propTypes = {
  query: PropTypes.string,
};

export default memo(LearnVideo);
