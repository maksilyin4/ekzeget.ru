import React from 'react';
import cx from 'classnames';
import './pagination.scss';

const Pagination = ({ page, count, totalCount, onClick }) => {
  const items = [];
  const page_count = Math.ceil(totalCount / count);
  const range = 5;

  for (let i = 1; i <= page_count; i += 1) {
    const thisPage = i;
    if (i <= range || (i > page - range / 2 && i < page + range / 2) || i > page_count - 1) {
      if (items[items.length - 1] && i !== items[items.length - 1].props.pagenum + 1) {
        items.push(
          <button key={i - 1} className="pagination__item pagination__item_dot">
            ...
          </button>,
        );
      }
      items.push(
        <button
          key={i}
          pagenum={i}
          className={cx('pagination__item', { active: thisPage === page })}
          onClick={thisPage === page ? undefined : () => onClick(thisPage)}>
          {i}
        </button>,
      );
    }
  }

  return (
    <div className="pagination">
      {page !== 1 && (
        <button className="pagination__item" onClick={() => onClick(page - 1)}>
          {'<'}
        </button>
      )}
      {items}
      {page !== page_count && (
        <button className="pagination__item" onClick={() => onClick(page + 1)}>
          {'>'}
        </button>
      )}
    </div>
  );
};

export default Pagination;
