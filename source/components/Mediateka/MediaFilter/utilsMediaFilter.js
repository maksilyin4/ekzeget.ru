export const customStyles = {
  option: (styles, state) => {
    return {
      ...styles,
      background: !state.isSelected
        ? 'linear-gradient(180deg, #FFFFFF 0%, #F3F3F3 100%)'
        : 'linear-gradient(360deg, #FFDE88 0.92%, #EFAE04 98.65%);',
      boxShadow: state.isSelected
        ? 'inset 0px 1px 4px rgba(0, 0, 0, 0.5)'
        : '0px 10px 5px rgba(0, 0, 0, 0.05), 0px 1px 4px rgba(0, 0, 0, 0.2);',
      color: state.isSelected && 'inset 0px 1px 4px rgba(0, 0, 0, 0.5)',
    };
  },
  control: (styles, state) => {
    return {
      ...styles,
      borderRadius: '12px 0 0 12px',
      background: state.menuIsOpen
        ? 'linear-gradient(360deg, #FFDE88 0.92%, #EFAE04 98.65%)'
        : 'linear-gradient(180deg, #FFFFFF 0%, #F3F3F3 100%)',
      transition: '0.65s ease',
      ':hover': {
        ...styles[':hover'],
        border: '1px solid rgba(255, 255, 255, 0.5)',
        background: 'linear-gradient(360deg, #FFDE88 0.92%, #EFAE04 98.65%)',
      },
    };
  },
};
