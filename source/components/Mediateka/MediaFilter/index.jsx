import React, { memo } from 'react';
import cx from 'classnames';
import Select from 'react-select';

import { customStyles } from './utilsMediaFilter';

import './mediaFilter.scss';

const MediaFilter = ({
  styleName = 'media_filter_select_default',
  placeholder = 'фильтр',
  styleNamePrefix = '',
  options = [],
  value,
  isPrefixDefault = true,
  isMulti = false,
  isClearable = false,
  isDisabled = false,
  isDefaultStyles = true,
  noOptionsMessage = () => {},
  onChange = f => f,
  menuIsOpen,
}) => {
  return (
    <div className={`${styleName} media-filter-select`}>
      <Select
        options={options}
        onChange={onChange}
        value={value}
        placeholder={placeholder}
        classNamePrefix={cx(styleNamePrefix, {
          'media-filter-select_custom': isPrefixDefault,
        })}
        styles={isDefaultStyles && customStyles}
        isClearable={isClearable}
        isDisabled={isDisabled}
        isMulti={isMulti}
        noOptionsMessage={noOptionsMessage}
        menuIsOpen={menuIsOpen}
        isSearchable={false}
      />
    </div>
  );
};

export default memo(MediaFilter);
