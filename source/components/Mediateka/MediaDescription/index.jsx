import React, { useState, memo, useRef } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import MediaContentInfoItem from '~components/Mediateka/MediaContentInfoItem';
import ButtonMediaContentInfo from './ButtonMediaContentInfo/';

import { showBtn } from '../utilsMediateka';

import './mediaDescription.scss';

const MediaDescription = ({
  description = '',
  className = '',
  screen = true,
  defaultCount = 500,
  defaultShowBtn = 250,
  textBtnClose = 'Развернуть полное описание',
  textBtnOpen = 'Скрыть полное описание',
  classBtnContent = 'media-btn-info',
}) => {
  const refTo = useRef(null);
  const refDesc = useRef(null);
  const [isActiveDesc, setIsActiveDesc] = useState(false);

  if (!description) {
    return null;
  }

  const isHeight = description.length / 2 > defaultCount;

  const shouldShowBtn = showBtn(screen, description, defaultShowBtn);

  const toggleDesc = () => {
    if (refTo) {
      refTo.current.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
    }
    setIsActiveDesc(!isActiveDesc);
  };

  return (
    <>
      <div className="book_desc_text_content">
        <MediaContentInfoItem
          refTo={refTo}
          refDesc={refDesc}
          styleName={cx(`book-desc__text ${className}`, {
            defaultHeight: !className && shouldShowBtn,
            toggleHeight: isHeight,
            toggleActive: isActiveDesc,
            'book-desc__text-empty': !description.length,
          })}
          text={description}
        />
      </div>
      <ButtonMediaContentInfo
        isShow={shouldShowBtn}
        isActiveDesc={isActiveDesc}
        toggleDesc={toggleDesc}
        textBtnClose={textBtnClose}
        textBtnOpen={textBtnOpen}
        classBtnContent={classBtnContent}
      />
    </>
  );
};

MediaDescription.propTypes = {
  description: PropTypes.string,
  screen: PropTypes.bool,
  defaultCount: PropTypes.number,
  defaultShowBtn: PropTypes.number,
  textBtnClose: PropTypes.string,
  textBtnOpen: PropTypes.string,
  classBtnContent: PropTypes.string,
  className: PropTypes.string,
};

export default memo(MediaDescription);
