import React from 'react';

import './buttonMediaContentInfo.scss';

const ButtonMediaContentInfo = ({
  isShow = false,
  isActiveDesc = '',
  toggleDesc = f => f,
  classBtnContent = 'media-btn-info',
  textBtnClose = 'Развернуть полное описание',
  textBtnOpen = 'Скрыть полное описание',
}) =>
  isShow && (
    <div className={classBtnContent}>
      <button className="mediaBth" onClick={toggleDesc}>
        {isActiveDesc ? textBtnOpen : textBtnClose}
      </button>
    </div>
  );

export default ButtonMediaContentInfo;
