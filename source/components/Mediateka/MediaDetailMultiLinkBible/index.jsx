import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Icon from '~components/Icons/Icons';

import './mediaDetailMultiLinkBible.scss';

const MediaDetailMultiLinkBible = ({ linkTo = '' }) =>
  linkTo && (
    <div className="media-multi-audio-list-info-ready">
      <Icon icon="bible" />
      <Link to={linkTo}>Читать в Библии</Link>
    </div>
  );

MediaDetailMultiLinkBible.propTypes = {
  linkTo: PropTypes.string,
};

export default memo(MediaDetailMultiLinkBible);
