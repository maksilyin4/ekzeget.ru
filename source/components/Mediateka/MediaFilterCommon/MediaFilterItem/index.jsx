import React, { memo } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import './mediaFilterItem.scss';

const MediaFilterItem = ({
  refTo,
  className = '',
  classNameValue = '',
  isActive = false,
  onClick,
  value,
  placeholder = 'Введите',
  popupId,
  isDisabled = false,
}) => {
  return (
    <button
      ref={refTo}
      className={cx(`media-filter-default popup_${popupId}`, className, {
        'popup-disabled-on': isDisabled,
        activeButton: isActive,
      })}
      onClick={onClick}>
      <span
        className={cx(`media-filter-default-title popup_${popupId}`, classNameValue, {
          isMediaValue: value,
        })}>
        {value || placeholder}
      </span>
    </button>
  );
};

MediaFilterItem.propTypes = {
  refTo: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.instanceOf(PropTypes.any) }),
  ]),
  popupId: PropTypes.string.isRequired,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  isActive: PropTypes.bool.isRequired,
  classNameValue: PropTypes.string,
  className: PropTypes.string,
  isDisabled: PropTypes.bool,
};

export default memo(MediaFilterItem);
