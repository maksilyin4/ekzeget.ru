import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import { MediaContext } from '~context/MediaContext';

import { getMulti, getActiveMulti } from './utilsMediaFilterPopupItem';

import './mediaFilterPopupItem.scss';

const MediaFilterPopupItem = ({
  select = [],
  handleChange,
  queryFilter = {},
  valueDefault,
  classNameContent = '',
  title = '',
  popupId,
  classNameItem = '',
  classNamePopupItem = '',
  setName = '',
  isMulti,
}) => {
  const { phone, tablet } = useContext(MediaContext);
  const isDefaultMulti = isMulti && valueDefault && Array.isArray(valueDefault);
  return (
    <div className={`media-filter-default-popup-content ${classNameContent} popup_${popupId}`}>
      {title && <p>{title}</p>}
      <div className={`media-filter-default-popup-item  popup_${popupId} ${classNamePopupItem}`}>
        {select.map(({ value, label, ...rest }) => {
          return (
            <button
              key={value}
              onClick={() => {
                return handleChange(queryFilter[setName])(
                  getMulti({ value, label, rest, isDefaultMulti, valueDefault }),
                );
              }}
              className={cx(`media-default-item  ${classNameItem}`, {
                active: valueDefault && getActiveMulti({ value, isDefaultMulti, valueDefault }),
                [`popup_${popupId}`]: phone || tablet,
              })}>
              {label}
            </button>
          );
        })}
      </div>
    </div>
  );
};

MediaFilterPopupItem.propTypes = {
  select: PropTypes.array,
  queryFilter: PropTypes.object,
  valueDefault: PropTypes.any,
  popupId: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  classNameContent: PropTypes.string,
  title: PropTypes.string,
  classNameItem: PropTypes.string,
  classNamePopupItem: PropTypes.string,
  setName: PropTypes.string,
  isMulti: PropTypes.bool,
};

export default MediaFilterPopupItem;
