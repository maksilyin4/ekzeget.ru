export function getMulti({ value, label, rest, isDefaultMulti, valueDefault }) {
  if (isDefaultMulti) {
    const index = valueDefault.findIndex(({ value: values }) => values === value);
    if (index !== -1) {
      valueDefault.splice(index, 1);
      return valueDefault.length ? [...valueDefault] : null;
    }
    return [...valueDefault, { value, label, ...rest }];
  }
  if (!isDefaultMulti && valueDefault && valueDefault.value === value) {
    return null;
  }

  return { value, label, ...rest };
}

export const getActiveMulti = ({ value, valueDefault, isDefaultMulti }) => {
  if (isDefaultMulti) {
    return valueDefault.some(({ value: valueD }) => valueD === value);
  }
  return valueDefault.value === value;
};
