import React, { memo, useMemo, useRef, useState, useCallback, useEffect, useContext } from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';
import nanoid from 'nanoid';
import { useLocation } from 'react-router-dom';

import MediaFilterItem from './MediaFilterItem';

import { MediaContext } from '~context/MediaContext';
import Icons from '~components/Icons/Icons';

import './mediaFilterCommon.scss';

const MediaFilterCommon = ({
  value = '',
  placeholder,
  popupComponent,
  classNameContainer = '',
  classNamePopup = '',
  className = '',
  classNameValue = '',
  isDisabled = false,
  isClosePopup,
  isDelete,
  handleDelete = f => f,
  getIsActive = f => f,
  popupId: popupIdProps,
}) => {
  const { phone, tablet } = useContext(MediaContext);
  const { search } = useLocation();

  const buttonRef = useRef(null);
  const popupId = useMemo(() => popupIdProps || nanoid(10), []);
  const [isActive, setIsActive] = useState(false);

  const handleClick = useCallback(() => {
    setIsActive(!isActive);
    getIsActive(!isActive);
  }, [isActive]);

  useEffect(() => {
    if (isClosePopup && isActive && (phone || tablet)) {
      setIsActive(false);
    }
  }, [isClosePopup, phone, tablet, search]);

  useEffect(() => {
    const event = !phone && !tablet ? 'click' : 'touchstart';
    const delay = !phone && !tablet ? 0 : 300;

    const listener = e => {
      if (
        typeof e.target.className === 'string' &&
        !e.target.className.includes(`popup_${popupId}`)
      ) {
        setTimeout(() => {
          setIsActive(false);
          getIsActive(false);
        }, delay);
      }
    };

    window.addEventListener(event, listener);
    return () => window.removeEventListener(event, listener);
  }, [phone, tablet]);

  return (
    <div
      className={cx('media-filter-container', classNameContainer, {
        [`popup_${popupId}`]: popupIdProps,
      })}>
      <div className="media-filter-default-input">
        <MediaFilterItem
          refTo={buttonRef}
          popupId={popupId}
          className={className}
          isActive={isActive}
          onClick={handleClick}
          value={value}
          placeholder={placeholder}
          classNameValue={classNameValue}
          isDisabled={isDisabled}
        />
        {!isDelete && value && (
          <Icons icon="close" className="media-filter-default-delete" onClick={handleDelete} />
        )}
      </div>
      <div
        className={cx(`media-filter-default-popup popup_${popupId}`, classNamePopup, {
          active: isActive,
        })}>
        {popupComponent({ popupId })}
      </div>
    </div>
  );
};

MediaFilterCommon.propTypes = {
  popupComponent: PropTypes.node,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  classNameContainer: PropTypes.string,
  classNamePopup: PropTypes.string,
  classNameValue: PropTypes.string,
  className: PropTypes.string,
  isDisabled: PropTypes.bool,
  isDelete: PropTypes.bool,
  isClosePopup: PropTypes.bool,
  handleDelete: PropTypes.func,
  getIsActive: PropTypes.func,
};

export default memo(MediaFilterCommon);
