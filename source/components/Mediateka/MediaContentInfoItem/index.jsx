import React, { memo } from 'react';

import './mediaContentInfoItem.scss';

const MediaContentInfoItem = ({
  refTo,
  refDesc = { current: null },
  styleName = '',
  contentTitle,
  text = '',
  isOtherTemplate,
}) => {
  const getContent = () => {
    if (contentTitle) {
      return (
        <div className="book-info-content-span">
          <span className="book-info__content-title">{contentTitle}</span>
          {text}
        </div>
      );
    }
    if (isOtherTemplate) {
      return (
        <div className="book-info-content-other-template">
          <span>{text}</span>
        </div>
      );
    }
    return (
      <div className="book-info-content" ref={refDesc} dangerouslySetInnerHTML={{ __html: text }} />
    );
  };

  return (
    text && (
      <div ref={refTo} className={`book-info ${styleName}`}>
        {getContent()}
      </div>
    )
  );
};
export default memo(MediaContentInfoItem);
