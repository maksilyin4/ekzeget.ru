import React from 'react';

import MediaDetailMotivator from '~pages/Mediateka/MediaBody/MediaDetailContent/MediaDetailMotivator/';
import MediaMotivatorIcons from '~components/Mediateka/MediaMotivatorContentImg/MediaMotivatorIcons/';
import Portal from '~components/Portal/';

const MediaMotivatorPopup = ({
  content,
  currentContent = {},
  template,
  currentMediaId,
  closePortal,
}) => (
  <Portal closePortal={closePortal}>
    <MediaDetailMotivator content={content} index={currentMediaId} template={template} />
    <MediaMotivatorIcons url={currentContent?.image?.url ?? ''} currentBook={currentContent} />
  </Portal>
);

export default MediaMotivatorPopup;
