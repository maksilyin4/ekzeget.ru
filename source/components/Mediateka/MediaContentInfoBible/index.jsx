import React from 'react';
import { Link } from 'react-router-dom';

import { getComma, getNumberForUrl } from './utilsInfoBible';

const MediaContentInfoBible = ({ media_bible = [] }) => {
  return media_bible.map(({ id, book, verse, chapter }, i) => (
    <Link
      key={id}
      to={`/bible/${book?.code}/${getNumberForUrl(chapter?.number, 'glava-')}${getNumberForUrl(
        verse?.number,
        'stih-',
      )}`}
      className="media-content-info-link">
      {`${book?.short_title || ''} ${chapter?.number || ''}${
        verse ? `:${verse?.number}` : ''
      }${getComma(media_bible.length, i + 1)}`}
    </Link>
  ));
};

export default MediaContentInfoBible;
