const getComma = (sum, i) => (sum !== i ? ', ' : '');
const getNumberForUrl = (number, text = 'stih-') => (number ? `${text}${number}/` : '');

export { getComma, getNumberForUrl };
