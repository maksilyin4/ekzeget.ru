import { searchUrl, changeQuery } from '~utils/common';

export const initialState = {
  book: null,
  author: null,
  chapter: null,
  translate: null,
  preacher: null,
  verse: [],
};

export function getSelect({ data, dataValue = 'title', dataId = 'id' }) {
  if (data) {
    return data.map(el => ({ value: el[dataId], label: el[dataValue] }));
  }
}

// добавление и удаление query из url
export function changeUrlQuery({
  setName,
  select,
  history,
  search,
  queryFilter,
  addQueryUrl,
  deleteQueryUrlFilter,
  deleteQueryUrl,
}) {
  if (select) {
    if (setName === queryFilter.book) {
      const { search: searchQ, tags, sorts, authorsCategory, author } = queryFilter;
      changeQuery({
        search,
        history,
        queryFilters: [searchQ, tags, sorts, authorsCategory, author],
        setName,
        value: select.value,
      });
    } else if (setName === queryFilter.chapter) {
      const { chapter, verse, ...otherQueryFilters } = queryFilter;

      changeQuery({
        search,
        history,
        queryFilters: Object.values(otherQueryFilters),
        setName,
        value: select.value,
      });
    } else {
      const state = setName === queryFilter.verse ? select.map(el => el.value) : select.value;
      addQueryUrl({ search, history, name: setName, state });
    }
  } else {
    switch (setName) {
      case queryFilter.book:
        deleteQueryUrlFilter({ search, history, name: setName });
        break;
      case queryFilter.chapter:
        deleteQueryUrlFilter({
          search,
          history,
          name: setName,
          del: [queryFilter.verse, queryFilter.chapter],
        });
        break;
      case queryFilter.author:
        deleteQueryUrlFilter({
          search,
          history,
          name: setName,
          del: [queryFilter.author, queryFilter.authorsCategory],
        });
        break;
      default:
        deleteQueryUrl({ search, history, name: setName });
    }
  }
}

export const setStateForFilter = ({
  search,
  queryFilter,
  setState,
  state,
  getChapter,
  getVerse,
}) => {
  const urlParams = new URLSearchParams(search);
  const book = urlParams.get(queryFilter.book);
  const author = urlParams.get(queryFilter.author);
  const chapter = urlParams.get(queryFilter.chapter);
  const verse = urlParams.get(queryFilter.verse);
  const preacher = urlParams.get(queryFilter.preacher);
  const translate = urlParams.get(queryFilter.translate);
  const ekzeget = urlParams.get(queryFilter.ekzeget);

  const isSetState = book || author || chapter || verse || translate || preacher || ekzeget;

  if (isSetState) {
    setState({
      ...state,
      book,
      author,
      chapter,
      verse: verse ? verse.split(',') : [],
      translate,
      preacher,
      ekzeget,
    });

    if (book) {
      getChapter({ variables: { book_id: +book } });
    }
    if (chapter) {
      getVerse({ variables: { connected_to_chapter_id: +chapter } });
    }
  }
};

export const getIsResetFilter = (state = {}, querySort = '') => {
  const checkClearState = Object.values(state).filter(el => (Array.isArray(el) ? el.length : el));
  const isCheckSort = !querySort.includes('');
  return checkClearState.length || isCheckSort;
};

export const setSelectAuthors = (authors = [], query) => {
  const authorsFind = authors.find(({ value }) => +value === +query) || {};
  return authorsFind.authors || [];
};

export const setSelectAlphabet = ({ data = [], letter }) => {
  return letter
    ? data.filter(({ label }) => label.charAt(0).toUpperCase() === letter.toUpperCase())
    : [];
};

export const setIsFetchSearch = (pathname = '') => {
  const isFetchSearchTranslate = pathname.includes(searchUrl.bible);
  const isFetchSearchPreachers = pathname.includes(searchUrl.preachers);
  const isFetchSearchEkzeget = pathname.includes(searchUrl.interpretations);
  const isShowAuthors = !(
    isFetchSearchTranslate ||
    pathname.includes(searchUrl.quiz) ||
    pathname.includes(searchUrl.preachers) ||
    isFetchSearchEkzeget
  );

  return { isFetchSearchTranslate, isFetchSearchPreachers, isFetchSearchEkzeget, isShowAuthors };
};

export const setValueSelect = ({
  setName,
  select,
  queryFilter,
  setState,
  state,
  getChapter,
  setTranslate,
}) => {
  const defaultSelect = select === null ? select : select.value;

  switch (setName) {
    case queryFilter.book: {
      if (select) {
        getChapter({ variables: { book_id: +select.value } });
      }
      const reState = select
        ? { [setName]: select.value, chapter: null, verse: [] }
        : { [setName]: select, chapter: null, verse: [] };
      setState({ ...state, ...reState });
      break;
    }

    case queryFilter.verse: {
      setState({
        ...state,
        [setName]: select !== null ? select.map(({ value }) => value) : [],
      });
      break;
    }
    case queryFilter.chapter: {
      setState({
        ...state,
        [setName]: defaultSelect,
        verse: [],
      });
      break;
    }

    case queryFilter.translate: {
      setState({
        ...state,
        [setName]: defaultSelect,
      });

      setTranslate(select);
      break;
    }
    default: {
      setState({
        ...state,
        [setName]: defaultSelect,
      });

      break;
    }
  }
};
