import React, { useState, useMemo, useEffect, memo, useCallback } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import cx from 'classnames';
// css
import './mediaFilters.scss';
// redux
import { getTranslatesForSelectSearch } from '~dist/selectors/translates';
import { getIsMobile } from '~dist/selectors/screen';
// apollo
import useEkzegetsLazyQuery from '~apollo/query/mediaLibrary/ekzegets';
import usePreachersLazyQuery from '~apollo/query/mediaLibrary/preachers';
import useAuthorsLazyQuery from '~apollo/query/mediaLibrary/authors';
import useBookQuery from '~apollo/query/mediaLibrary/book';
import useChapterLazyQuery from '~apollo/query/mediaLibrary/chapter';
import useVerseLazyQuery from '~apollo/query/mediaLibrary/verse';
// components
import BtnMedia from '~components/Mediateka/BtnMedia';
import MediaSort from '../MediaHeader/MediaSort/';
import MediaCategoryAuthorsSelect from './MediaCategoryAuthorsSelect/';
import MediaCategoryAuthorsButtons from './MediaCategoryAuthorsButtons/';
import MediaFilterCommon from '~components/Mediateka/MediaFilterCommon';
import MediaFilterPopupBook from './MediaFilterPopupBook';
import MediaFilterPopupItem from '~components/Mediateka/MediaFilterCommon/MediaFilterPopupItem';
import Alphabet from '~components/Alphabet';
// utils
import {
  deleteQueryUrlFilter,
  deleteQueryUrl,
  addQueryUrl,
  queryFilter,
  getQueryUrl,
} from '~pages/Mediateka/MediaBody/utilsMediaBody';
import {
  initialState,
  getSelect,
  changeUrlQuery,
  setStateForFilter,
  getIsResetFilter,
  setSelectAuthors,
  setSelectAlphabet,
  setValueSelect,
} from './utilsMediaFilters';
import { setIsFetchSearch } from '~components/Mediateka/MediaFilters/utilsMediaFilters';
import { useTranslate } from '~utils/useTranslate';
import { defaultTranslate, searchUrl, mediateka, changeQuery } from '~utils/common';

const MediaFilters = () => {
  const { search, pathname } = useLocation();
  const history = useHistory();

  // TODO  если будет не нужно удалить 25.03.20
  // const [{ translates }] = useCookies(['translates']);

  const setTranslate = useTranslate();

  const phone = useSelector(getIsMobile);
  const translatesSelect = useSelector(getTranslatesForSelectSearch);

  const querySort = getQueryUrl({ search, tag: queryFilter.sort }) || '';
  const queryAuthorsCategory = getQueryUrl({
    search,
    tag: queryFilter.authorsCategory,
  });
  const queryAuthor = getQueryUrl({
    search,
    tag: queryFilter.author,
  });

  const queryLetter = getQueryUrl({
    search,
    tag: queryFilter.letter,
  });

  const [state, setState] = useState(initialState);

  const [isActive, setIsActive] = useState(false);

  const {
    isFetchSearchTranslate,
    isFetchSearchPreachers,
    isFetchSearchEkzeget,
    isShowAuthors,
  } = setIsFetchSearch(pathname);

  // graphQL
  const [getEkzegets, ekzegets, ekzegetAlphabet] = useEkzegetsLazyQuery();
  const [getAuthors, authors] = useAuthorsLazyQuery();
  const { booksOld, booksNew } = useBookQuery();
  const [getPreachers, preachers, preacherAlphabet] = usePreachersLazyQuery();
  const [getChapter, chapter] = useChapterLazyQuery();
  const [getVerse, verse] = useVerseLazyQuery();

  useEffect(() => {
    if (pathname.includes(`/${mediateka}/`)) {
      deleteQueryUrl({ search, history, name: queryFilter.translate });
    }

    setStateForFilter({
      search,
      queryFilter,
      setState,
      state,
      getChapter,
      getVerse,
    });
  }, []);

  useEffect(() => {
    if (!(isFetchSearchTranslate || isFetchSearchPreachers || isFetchSearchEkzeget)) {
      getAuthors();
    }

    if (isFetchSearchPreachers) {
      getPreachers();
    }
    if (isFetchSearchEkzeget) {
      getEkzegets();
    }
  }, [isFetchSearchTranslate, isFetchSearchPreachers, isFetchSearchEkzeget]);

  // selects
  const selectAuthor = useMemo(() => setSelectAuthors(authors, queryAuthorsCategory), [
    authors,
    queryAuthorsCategory,
  ]);
  const selectBookNew = useMemo(() => getSelect({ data: booksNew, dataValue: 'menu' }), [booksNew]);
  const selectBookOld = useMemo(() => getSelect({ data: booksOld, dataValue: 'menu' }), [booksOld]);
  const selectChapter = useMemo(() => getSelect({ data: chapter, dataValue: 'number' }), [chapter]);
  const selectVerse = useMemo(() => getSelect({ data: verse, dataId: 'id', dataValue: 'number' }), [
    verse,
  ]);
  const selectEkzeget = useMemo(() => setSelectAlphabet({ data: ekzegets, letter: queryLetter }), [
    ekzegets,
    queryLetter,
  ]);
  const selectPreachers = useMemo(
    () => setSelectAlphabet({ data: preachers, letter: queryLetter }),
    [preachers, queryLetter],
  );

  const valueBook = useMemo(() => {
    return state.book
      ? [...selectBookNew, ...selectBookOld].find(s => +s.value === +state.book)
      : null;
  }, [selectBookNew, selectBookOld, state && state.book]);

  const valueChapter = useMemo(() => {
    return selectChapter && state.chapter
      ? selectChapter.find(s => +s.value === +state.chapter)
      : null;
  }, [chapter, state && state.chapter]);

  const { valueVerse, valueShowVerse } = useMemo(() => {
    const arr = [];
    const valueVerse =
      selectVerse && state.verse && state.chapter
        ? selectVerse.filter(({ value, label }) => {
            const isValue = state.verse.includes(value);
            if (isValue) {
              arr.push(label);
              return isValue;
            }
          })
        : [];

    return { valueVerse, valueShowVerse: arr.join(', ') };
  }, [verse, state && state.verse]);

  const valueAuthor = useMemo(() => {
    return selectAuthor && state.author ? selectAuthor.find(s => +s.value === +state.author) : null;
  }, [selectAuthor, state?.author]);

  const valueTranslate = useMemo(() => {
    if (translatesSelect && state.translate) {
      const [rus, other] = translatesSelect;

      return rus && other ? [...rus, ...other].find(s => s.value === state.translate) : null;
    }
    return null;
  }, [translatesSelect, state?.translate]);

  const valuePreacher = useMemo(() => {
    if (preachers && state.preacher) {
      return preachers.find(s => s.value === state.preacher) || null;
    }
    return null;
  }, [preachers, state?.preacher]);

  const valueEkzeget = useMemo(() => {
    if (selectEkzeget && state.ekzeget) {
      return selectEkzeget.find(s => s.value === state.ekzeget) || null;
    }
    return null;
  }, [selectEkzeget, state?.ekzeget]);

  const bookValue = valueBook?.label;
  const chapterValue = valueChapter?.label;
  const verseValue = valueVerse.length ? valueShowVerse : null;
  const translateValue = valueTranslate?.label;
  const authorValue = valueAuthor?.label;
  const authorId = valueAuthor?.value;
  const preacherValue = valuePreacher?.label;
  const ekzegetValue = valueEkzeget?.label;

  useEffect(() => {
    if (!search) {
      setState(initialState);
    }
  }, [search]);

  useEffect(() => {
    if (queryAuthorsCategory && authorId && !queryAuthor) {
      history.replace(`${search}&author=${authorId}`);
    }
    // при обновлении страницы есть id автор но state является не назначенным
    // if (queryAuthorsCategory && authorId === undefined && queryAuthor) {
    //   deleteQueryUrl({ search, history, name: queryFilter.author });
    // }
  }, [queryAuthorsCategory, queryAuthor, authorId]);

  useEffect(() => {
    // когда закрыли попап авторов и не выбран автор
    if (!isActive && !valueAuthor?.value && queryAuthorsCategory && selectAuthor.length) {
      deleteQueryUrlFilter({
        search,
        history,
        del: [queryFilter.authorsCategory, queryFilter.author],
      });
    }
    // когда закрыли попап толкований и не выбран экзегет
    if (!isActive && !valueEkzeget?.value && queryLetter && selectEkzeget.length) {
      deleteQueryUrlFilter({
        search,
        history,
        del: [queryFilter.letter, queryFilter.ekzeget],
      });
    }
    if (!isActive && !valuePreacher?.value && queryLetter && selectPreachers.length) {
      deleteQueryUrlFilter({
        search,
        history,
        del: [queryFilter.letter, queryFilter.preacher],
      });
    }
  }, [
    isActive,
    selectAuthor,
    queryAuthorsCategory,
    authorValue,
    valueEkzeget,
    selectEkzeget,
    queryLetter,
    selectPreachers,
    valuePreacher,
  ]);

  const resetFilter = useCallback(() => {
    const { tags, cat, sort, ...prevQueryFilter } = queryFilter;

    const isResetFilter = getIsResetFilter(state, querySort);

    if (isResetFilter) {
      setState(initialState);
      deleteQueryUrlFilter({
        search,
        history,
        del: Object.values(prevQueryFilter),
      });
    }
  }, [search, history, state, querySort]);

  const handleDelete = setName => () => {
    switch (setName) {
      case queryFilter.book:
        setState({
          ...state,
          [setName]: null,
          chapter: null,
          verse: [],
        });
        break;
      case queryFilter.chapter:
        setState({ ...state, [setName]: null, verse: [] });
        break;
      case queryFilter.translate: {
        setState({ ...state, [setName]: null, verse: [] });

        setTranslate(defaultTranslate);
        break;
      }
      default:
        setState({ ...state, [setName]: null });
        break;
    }
    changeUrlQuery({
      setName,
      history,
      search,
      queryFilter,
      addQueryUrl,
      deleteQueryUrlFilter,
      deleteQueryUrl,
    });
  };

  const handleChange = useCallback(
    setName => select => {
      setValueSelect({
        setName,
        select,
        queryFilter,
        setState,
        state,
        getChapter,
        setTranslate,
      });

      if (setName === queryFilter.chapter && select) {
        getVerse({ variables: { connected_to_chapter_id: +select.value } });
      }

      changeUrlQuery({
        setName,
        select,
        history,
        search,
        queryFilter,
        addQueryUrl,
        deleteQueryUrlFilter,
        deleteQueryUrl,
      });
    },
    [search, state.verse],
  );

  const handleClickCategoryAuthors = useCallback(
    id => () => {
      const { author, authorsCategory, ...otherQueryFilters } = queryFilter;
      changeQuery({
        search,
        history,
        queryFilters: Object.values(otherQueryFilters),
        setName: authorsCategory,
        value: id,
      });
    },
    [search, history],
  );

  const handleLetter = letter => () => {
    if (letter) {
      addQueryUrl({
        search,
        history,
        name: queryFilter.letter,
        state: letter,
      });
    }
  };

  const getIsActive = isActive => {
    setIsActive(isActive);
  };

  return (
    <div className="media-filter">
      <MediaFilterCommon
        key={queryFilter.book}
        className="media-filter-book"
        classNameContainer="media-filter-book-container"
        classNameValue="media-filter-book-title"
        classNamePopup="media-filter-book-popup"
        value={bookValue}
        placeholder="Связь с книгами Библии"
        handleDelete={handleDelete(queryFilter.book)}
        isClosePopup={bookValue}
        popupComponent={props => (
          <MediaFilterPopupBook
            valueBook={valueBook}
            selectBookNew={selectBookNew}
            selectBookOld={selectBookOld}
            handleChange={handleChange}
            queryFilter={queryFilter}
            setName={queryFilter.book}
            {...props}
          />
        )}
      />
      <MediaFilterCommon
        key={queryFilter.chapter}
        className="media-filter-chapter"
        classNameContainer="media-filter-chapter-container"
        classNamePopup="media-filter-chapter-popup"
        value={chapterValue}
        placeholder="С главами"
        isDisabled={!state.book}
        handleDelete={handleDelete(queryFilter.chapter)}
        isClosePopup={chapterValue}
        popupComponent={props => (
          <MediaFilterPopupItem
            valueDefault={valueChapter}
            select={selectChapter}
            handleChange={handleChange}
            queryFilter={queryFilter}
            classNamePopupItem="media-filter-chapter-popup-item"
            setName={queryFilter.chapter}
            {...props}
          />
        )}
      />

      <MediaFilterCommon
        key={queryFilter.verse}
        className="media-filter-verse"
        classNameContainer="media-filter-verse-container"
        classNamePopup="media-filter-verse-popup"
        value={verseValue}
        placeholder="Со стихами"
        handleDelete={handleDelete(queryFilter.verse)}
        isDisabled={!state.chapter}
        isClosePopup={verseValue}
        popupComponent={props => (
          <MediaFilterPopupItem
            {...props}
            valueDefault={valueVerse}
            select={selectVerse}
            handleChange={handleChange}
            queryFilter={queryFilter}
            classNamePopupItem="media-filter-verse-popup-item"
            setName={queryFilter.verse}
            isMulti
          />
        )}
      />

      {phone && <MediaSort />}

      {isShowAuthors && (
        <MediaFilterCommon
          key={queryFilter.author}
          className="media-filter-author"
          classNameContainer="media-filter-author-container"
          classNamePopup="media-filter-author-popup"
          value={authorValue}
          placeholder="Все авторы"
          handleDelete={handleDelete(queryFilter.author)}
          isClosePopup={authorValue}
          getIsActive={getIsActive}
          popupComponent={props => {
            return (
              <div className="media-filter-author-popup-container">
                {phone ? (
                  <MediaCategoryAuthorsSelect
                    authors={authors}
                    queryAuthorsCategory={queryAuthorsCategory}
                    popupId={props.popupId}
                  />
                ) : (
                  <MediaCategoryAuthorsButtons
                    handleClickCategoryAuthors={handleClickCategoryAuthors}
                    selectAuthor={selectAuthor}
                    authors={authors}
                    queryAuthorsCategory={queryAuthorsCategory}
                    popupId={props.popupId}
                  />
                )}
                {selectAuthor.length > 0 && (
                  <MediaFilterPopupItem
                    valueDefault={valueAuthor}
                    select={selectAuthor}
                    handleChange={handleChange}
                    queryFilter={queryFilter}
                    classNamePopupItem="media-filter-author-popup-item"
                    setName={queryFilter.author}
                    {...props}
                  />
                )}
              </div>
            );
          }}
        />
      )}

      {isFetchSearchTranslate && (
        <MediaFilterCommon
          key={queryFilter.translate}
          className="media-filter-translates"
          classNameContainer="media-filter-translates-container"
          classNamePopup="media-filter-translates-popup"
          value={translateValue}
          placeholder="Переводы"
          handleDelete={handleDelete(queryFilter.translate)}
          isClosePopup={translateValue}
          getIsActive={getIsActive}
          popupComponent={props => {
            return (
              <div className="media-filter-translates-popup-container">
                {translatesSelect.map((el, i) => (
                  <MediaFilterPopupItem
                    key={i}
                    valueDefault={valueTranslate}
                    select={el}
                    handleChange={handleChange}
                    queryFilter={queryFilter}
                    classNamePopupItem="media-filter-translates-popup-item"
                    setName={queryFilter.translate}
                    {...props}
                  />
                ))}
              </div>
            );
          }}
        />
      )}
      {isFetchSearchEkzeget && (
        <MediaFilterCommon
          key={queryFilter.ekzeget}
          className="media-filter-translates"
          classNameContainer="media-filter-translates-container"
          classNamePopup="media-filter-translates-popup media-filter-other-popup"
          value={ekzegetValue}
          placeholder="Все экзегеты"
          handleDelete={handleDelete(queryFilter.ekzeget)}
          isClosePopup={ekzegetValue}
          getIsActive={getIsActive}
          popupComponent={props => {
            return (
              <div className="media-filter-translates-popup-container media-filter-alphabet">
                <Alphabet
                  alphabet={ekzegetAlphabet}
                  activeLetter={queryLetter}
                  popupId={props.popupId}
                  onClick={handleLetter}
                />
                {selectEkzeget.length > 0 && (
                  <MediaFilterPopupItem
                    valueDefault={valueEkzeget}
                    select={selectEkzeget}
                    handleChange={handleChange}
                    queryFilter={queryFilter}
                    classNamePopupItem="media-filter-translates-popup-item"
                    setName={queryFilter.ekzeget}
                    {...props}
                  />
                )}
              </div>
            );
          }}
        />
      )}
      {isFetchSearchPreachers && (
        <MediaFilterCommon
          key={queryFilter.preacher}
          className="media-filter-translates"
          classNameContainer="media-filter-translates-container"
          classNamePopup="media-filter-translates-popup media-filter-other-popup"
          value={preacherValue}
          placeholder="Все проповедники"
          handleDelete={handleDelete(queryFilter.preacher)}
          isClosePopup={preacherValue}
          getIsActive={getIsActive}
          popupComponent={props => {
            return (
              <div className="media-filter-translates-popup-container media-filter-alphabet">
                <Alphabet
                  alphabet={preacherAlphabet}
                  activeLetter={queryLetter}
                  popupId={props.popupId}
                  onClick={handleLetter}
                />
                {selectPreachers.length > 0 && (
                  <MediaFilterPopupItem
                    valueDefault={valuePreacher}
                    select={selectPreachers}
                    handleChange={handleChange}
                    queryFilter={queryFilter}
                    classNamePopupItem="media-filter-translates-popup-item"
                    setName={queryFilter.preacher}
                    {...props}
                  />
                )}
              </div>
            );
          }}
        />
      )}
      <div className={cx('media-filter-btn', { 'full-btn': pathname.includes(searchUrl.quiz) })}>
        <BtnMedia onClick={resetFilter} text="Сбросить" />
      </div>
    </div>
  );
};

export default memo(MediaFilters);
