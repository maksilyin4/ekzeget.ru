import React from 'react';
import cx from 'classnames';

const MediaCategoryAuthorsButtons = ({
  selectAuthor = [],
  authors = [],
  queryAuthorsCategory,
  popupId,
  handleClickCategoryAuthors,
}) => {
  return (
    <div
      className={cx('media-filter-author-popup-buttons', {
        defaultPadding: selectAuthor.length === 0,
      })}>
      {authors.map(({ value, label }) => (
        <button
          key={value}
          className={cx('media-default-item', `popup_${popupId}`, {
            active: +value === +queryAuthorsCategory,
          })}
          onClick={handleClickCategoryAuthors(value)}>
          {label}
        </button>
      ))}
    </div>
  );
};

export default MediaCategoryAuthorsButtons;
