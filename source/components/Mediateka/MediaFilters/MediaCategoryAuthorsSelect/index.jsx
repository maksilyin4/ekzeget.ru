import React, { useState, useCallback, useEffect } from 'react';
import { useLocation, useHistory } from 'react-router-dom';

import MediaFilterCommon from '~components/Mediateka/MediaFilterCommon';
import MediaFilterPopupItem from '~components/Mediateka/MediaFilterCommon/MediaFilterPopupItem';

import { queryFilter, deleteQueryUrlFilter } from '~pages/Mediateka/MediaBody/utilsMediaBody';

import { changeQuery } from '~utils/common';

import './mediaCategoryAuthors.scss';

const MediaCategoryAuthorsSelect = ({ authors = [], popupId, queryAuthorsCategory }) => {
  const { search } = useLocation();
  const history = useHistory();

  const [categoryAuthors, setCategoryAuthors] = useState('');

  useEffect(() => {
    if (!queryAuthorsCategory && categoryAuthors) {
      setCategoryAuthors('');
    }
    if (queryAuthorsCategory && !categoryAuthors && authors.length) {
      const categoryAuthorTitle = authors.find(({ value }) => +value === +queryAuthorsCategory);

      setCategoryAuthors(categoryAuthorTitle.label);
    }
  }, [queryAuthorsCategory, authors]);

  const handleChange = useCallback(
    () => select => {
      setCategoryAuthors(select.label);

      if (select) {
        const { author, authorsCategory, ...otherQueryFilters } = queryFilter;
        changeQuery({
          search,
          history,
          queryFilters: Object.values(otherQueryFilters),
          setName: authorsCategory,
          value: select.value,
        });
      }
    },
    [search],
  );

  const handleDelete = useCallback(() => {
    deleteQueryUrlFilter({
      search,
      history,
      del: [queryFilter.authorsCategory],
    });
    setCategoryAuthors('');
  }, [search, history]);

  return (
    <MediaFilterCommon
      key="categoryAuthors"
      className="media-filter-authors"
      classNameContainer="media-filter-authors-container"
      classNamePopup="media-filter-authors-popup"
      value={categoryAuthors}
      placeholder="Категория авторов"
      isClosePopup={categoryAuthors.length > 0}
      popupId={popupId}
      handleDelete={handleDelete}
      popupComponent={() => (
        <MediaFilterPopupItem
          select={authors}
          handleChange={handleChange}
          classNamePopupItem="media-filter-authors-popup-item"
          popupId={popupId}
        />
      )}
    />
  );
};

export default MediaCategoryAuthorsSelect;
