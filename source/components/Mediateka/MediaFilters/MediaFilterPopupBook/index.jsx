import React from 'react';

import MediaFilterPopupItem from '~components/Mediateka/MediaFilterCommon/MediaFilterPopupItem';

const MediaFilterPopupBook = ({
  handleChange,
  queryFilter,
  valueBook,
  selectBookNew,
  selectBookOld,
  popupId = '',
  setName = '',
  ...props
}) => (
  <>
    <MediaFilterPopupItem
      key="new"
      valueDefault={valueBook}
      select={selectBookNew}
      classNameContent="media-book-content-new"
      title="Новый Завет"
      handleChange={handleChange}
      queryFilter={queryFilter}
      popupId={popupId}
      setName={setName}
      {...props}
    />
    <MediaFilterPopupItem
      key="old"
      valueDefault={valueBook}
      select={selectBookOld}
      classNameContent="media-book-content-old"
      title="Ветхий Завет"
      handleChange={handleChange}
      queryFilter={queryFilter}
      popupId={popupId}
      setName={setName}
      {...props}
    />
  </>
);

export default MediaFilterPopupBook;
