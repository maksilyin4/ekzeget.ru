import React from 'react';
import { useSelector } from 'react-redux';

import { getIsTabletPhone, getIsMobile } from '../../../dist/selectors/screen';

import MediaSearch from './MediaSearch';
import MediaSort from './MediaSort';
import MediaTags from '~pages/Mediateka/MediaBody/MediaBodyLeftBlock/MediaTags/';

import './mediaHeaderStyles.scss';

const MediaHeader = ({ isShowTags }) => {
  const isMobile = useSelector(getIsMobile);
  const isAdaptive = useSelector(getIsTabletPhone);

  return (
    <div className="materials__header">
      <MediaSearch />
      {!isMobile && <MediaSort />}
      {isAdaptive && isShowTags && <MediaTags />}
    </div>
  );
};

export default React.memo(MediaHeader);
