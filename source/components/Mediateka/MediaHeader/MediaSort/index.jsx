import React, { useState, useMemo, useCallback, useEffect } from 'react';
import { useCookies } from 'react-cookie';
import { useLocation, useHistory } from 'react-router-dom';

import './mediaSortStyle.scss';

import MediaFilterCommon from '~components/Mediateka/MediaFilterCommon';
import MediaFilterPopupItem from '~components/Mediateka/MediaFilterCommon/MediaFilterPopupItem';

import { options, getCookieSetUrl, changeSort, setOptionsForSearch } from './utilsMediaSort';

import {
  addQueryUrl,
  queryFilter,
  getQueryUrl,
  deleteQueryUrlFilter,
} from '~pages/Mediateka/MediaBody/utilsMediaBody';
import { mediateka } from '~utils/common';

const mediatekaLink = `/${mediateka}/`;

const MediaSort = () => {
  const queryName = queryFilter.sort;
  const { search, pathname } = useLocation();
  const history = useHistory();

  const querySort = getQueryUrl({ tag: queryName, search });

  const [cookies, setCookie] = useCookies(['sort']);
  const [state, setState] = useState({});

  const isMedia = pathname.includes(mediatekaLink);

  const sortData = useMemo(() => (isMedia ? options : options.filter(setOptionsForSearch)), [
    isMedia,
  ]);

  useEffect(() => {
    getCookieSetUrl({
      cookies,
      querySort,
      search,
      history,
      query: queryName,
      setState,
    });
  }, []);

  useEffect(() => {
    const { orderBy, order } = changeSort({ searchSort: querySort, setCookie, isMedia });
    setState({ orderBy, order });

    if (querySort && !isMedia && (search.includes('created_at') || search.includes('views'))) {
      const orderQuery = order ? `,${order}` : '';
      const sort = `${orderBy}${orderQuery}`;
      const query = orderBy ? `?${queryName}=${sort}` : '?';
      history.replace(query);
    }
  }, [querySort, isMedia, pathname]);

  const changeMediaSelect = useCallback(
    () => select => {
      if (select) {
        const { orderBy, order } = state;
        if (!select.orderBy) {
          deleteQueryUrlFilter({
            search,
            history,
            del: [queryFilter.sort],
          });
        } else if (select.orderBy && (select.order !== +order || select.orderBy !== orderBy)) {
          addQueryUrl({
            search,
            history,
            name: queryFilter.sort,
            state: [select.orderBy, String(select.order)],
          });
        }
      }
    },

    [search, state.orderBy, state.order],
  );

  const valueSort = useMemo(() => {
    const { orderBy = '', order = '' } = state;
    return sortData.find(opt => +opt.order === +order.replace(',', '') && opt.orderBy === orderBy);
  }, [state.orderBy, state.order]);

  const value = valueSort?.label;

  return (
    <MediaFilterCommon
      key="sort"
      className="media-filter-sort"
      classNameContainer="media-filter-sort-container"
      classNamePopup="media-filter-sort-popup"
      value={value}
      placeholder="Сортировка"
      isClosePopup={value}
      isDelete
      popupComponent={props => (
        <MediaFilterPopupItem
          valueDefault={valueSort}
          select={sortData}
          handleChange={changeMediaSelect}
          queryFilter={queryFilter}
          classNamePopupItem="media_filter_sort_popup_item"
          setName="sort"
          {...props}
        />
      )}
    />
  );
};

export default React.memo(MediaSort);
