export const options = [
  {
    label: 'Сортировка по умолчанию',
    value: '',
    order: '',
    orderBy: '',
  },
  {
    label: 'Библейская последовательность',
    value: 'sequence',
    order: '',
    orderBy: 'sequence',
  },
  {
    label: 'Дата публикации (по возрастанию)',
    value: 'date1',
    order: 1,
    orderBy: 'created_at',
  },
  {
    label: 'Дата публикации (по убыванию)',
    value: 'date0',
    order: 0,
    orderBy: 'created_at',
  },
  { label: 'Название А-Я', value: 'title0', order: 0, orderBy: 'title' },
  { label: 'Название Я-А', value: 'title1', order: 1, orderBy: 'title' },
  { label: 'По просмотрам (по возрастанию)', value: 'views0', orderBy: 'views', order: 0 },
  { label: 'По просмотрам  (по убыванию)', value: 'views1', orderBy: 'views', order: 1 },
  // { label: 'Дата создания автором (по возрастанию)', value: 'date' },
  // { label: 'Дата создания автором (по убыванию)', value: 'date' },
];

const setHistorySort = ({ orderBy, order, search, query, history }) => {
  const orderQuery = order ? `,${order}` : '';
  const sort = `${orderBy}${orderQuery}`;

  const searchSort = search ? `${search}&${query}=${sort}` : `?${query}=${sort}`;
  history.replace(searchSort);
};

export const getCookieSetUrl = ({ cookies, querySort, search, history, query, setState }) => {
  const { sort: cookieSort } = cookies;

  if (cookieSort) {
    const [orderBy, order] = cookieSort?.split(',') || [];
    if (orderBy && order) {
      setState({ orderBy, order });

      if (!querySort) {
        setHistorySort({ orderBy, order, search, query, history });
      }
    }
  }
};

export const changeSort = ({ searchSort = '', setCookie, isMedia }) => {
  let orderBy;
  let order;

  if (searchSort) {
    const [orderByState, orderState] = searchSort.split(',');
    orderBy = orderByState;
    order = orderState ? `,${orderState}` : '';
    setCookie('sort', `${orderBy}${order}`, { path: '/' });
  }
  if ((searchSort?.includes('created_at') || searchSort?.includes('views')) && !isMedia) {
    orderBy = '';
    order = '';
    setCookie('sort', `${''}`, { path: '/' });
  }
  if (!searchSort) {
    setCookie('sort', `${''}`, { path: '/' });
  }

  return { orderBy, order };
};

export const setOptionsForSearch = ({ value }) =>
  !value.includes('date') && !value.includes('views');
