import React, { useState, useContext, useEffect, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useLocation, useHistory } from 'react-router-dom';
import { useDebounce } from 'react-debounce-hook';

import { MediaContext } from '~context/MediaContext';

import { getInputField } from '../../../../dist/selectors/search';
import { handleReset } from '../../../../dist/actions/search';

import searchIcon from '~assets/images/layout/search.svg';
import {
  addQueryUrl,
  deleteQueryUrl,
  queryFilter,
  getQueryUrl,
} from '~pages/Mediateka/MediaBody/utilsMediaBody';

import './mediaSearch.scss';

const MediaSearch = () => {
  const { search, pathname } = useLocation();
  const history = useHistory();

  const isCheckPage = pathname.includes('/search/');

  const inputField = useSelector(getInputField);

  const dispatch = useDispatch();

  const { handleSearch = f => f } = useContext(MediaContext);

  const searchQuery = useMemo(() => getQueryUrl({ tag: queryFilter.search, search }) || '', [
    search,
  ]);

  const [state, setState] = useState({ searchState: '', searchValue: '' });

  const [debouncedSearch, setDebouncedSearch] = useState('');

  useDebounce(state.searchState, setDebouncedSearch);

  useEffect(() => {
    return () => {
      if (isCheckPage) {
        history.replace('?');
        dispatch(handleReset());
      }
    };
  }, []);

  useEffect(() => {
    setState({ searchState: searchQuery, searchValue: searchQuery });
  }, [searchQuery]);

  useEffect(() => {
    if (inputField && isCheckPage) {
      setState({ searchState: inputField, searchValue: inputField });
    }
  }, [inputField, isCheckPage]);

  useEffect(() => {
    handleSearch(debouncedSearch);

    if (debouncedSearch) {
      addQueryUrl({
        search,
        history,
        name: queryFilter.search,
        state: debouncedSearch.trim(),
      });
    }
  }, [debouncedSearch]);

  const onKeyDown = e => {
    if (e.keyCode === 13) {
      handleSearch(debouncedSearch);
    }
  };

  const onChangeSearch = e => {
    const { value } = e.target;
    if (!value) {
      deleteQueryUrl({ search, history, name: queryFilter.search });
    }
    dispatch(handleReset());
    setState({ searchState: value, searchValue: value });
  };

  return (
    <div className="media-library__search media-library__search-container">
      <div className="media-library__search-wrapper">
        <input
          className="media-library__search-inpt"
          type="text"
          placeholder="Поиск по медиатеке"
          value={state.searchValue}
          onChange={onChangeSearch}
          onKeyDown={onKeyDown}
        />
        <div className="media-library__search-icon-container">
          <img className="media-library__search-icon" src={searchIcon} alt="Поиск" />
        </div>
      </div>
    </div>
  );
};

export default MediaSearch;
