export const getType = type => {
  switch (type) {
    case 'I':
      return 'image';
    case 'V':
      return 'video';
    case 'A':
      return 'audio';
    case 'B':
      return 'book';
    default:
      return 'book';
  }
};

export function showBtn(screen, value, defaultShowBtn = 210) {
  return screen ? value.length >= defaultShowBtn : value.length >= 110;
}
