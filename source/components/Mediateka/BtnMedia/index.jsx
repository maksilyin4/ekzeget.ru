import React, { memo } from 'react';
import PropTypes from 'prop-types';

import Preloader from '~components/Preloader/Preloader';

import './btnMedia.scss';

const BtnMedia = ({
  styleName = 'mediaBth',
  onClick = f => f,
  text = '',
  refTo,
  loading = false,
  error,
}) => (
  <button className={`media-btn-base ${styleName}`} onClick={onClick} ref={refTo}>
    {!loading ? text : <Preloader />}
    {error && <div className="media-btn-error">Ошибка! Попробуйте еще раз</div>}
  </button>
);

BtnMedia.propTypes = {
  styleName: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func,
};

export default memo(BtnMedia);
