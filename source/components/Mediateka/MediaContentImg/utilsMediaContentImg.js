import { youtubeParser } from '~components/VideoPlay/utilsVideoPlay';
import { getType } from '../utilsMediateka';
import { urlToAPI } from '../../../dist/browserUtils';

const setDefaultSrcImg = type => {
  return `/frontassets/images/mediaImg/media-${getType(type)}.png`;
};

const setVideoSrcImg = videoId => {
  return `https://i.ytimg.com/vi/${videoId}/hqdefault.jpg`;
};

export const setSrcVideo = (videoHref, type) => {
  const videoId = youtubeParser(videoHref);
  return videoId ? setVideoSrcImg(videoId) : setDefaultSrcImg(type);
};

export const setSrcImg = (imageUrl, type) => {
  return imageUrl ? `${urlToAPI}${imageUrl}` : setDefaultSrcImg(type);
};

export const setSrcWebp = webp => `${urlToAPI}${webp}`;
