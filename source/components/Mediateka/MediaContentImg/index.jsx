import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import cx from 'classnames';

import defaultBookIcon from './defaultBookIcon.svg';

import { setSrcVideo, setSrcImg, setSrcWebp } from './utilsMediaContentImg';

const MediaContentImg = React.memo(
  ({
    type = '',
    title = '',
    description = '',
    imageUrl = '',
    videoHref = '',
    style = {},
    styleName = '',
    styleNameVideo = '',
    urlLink = '',
    isLink,
    optimizedImg = [],
    onClick = () => {},
    imgRef,
  }) => {
    const webp = optimizedImg[0]?.webp;

    const srcImg =
      videoHref && !imageUrl ? setSrcVideo(videoHref, type) : setSrcImg(imageUrl, type);

    const Component = isLink ? 'div' : Link;
    const componentProps = isLink ? {} : { to: urlLink };

    return (
      <Component
        className={cx(styleName, styleNameVideo)}
        style={style}
        onClick={onClick}
        {...componentProps}>
        <>
          {type !== null ? (
            <picture>
              {webp && (
                <>
                  <source media="(min-width: 769px)" srcSet={setSrcWebp(webp)} type="image/webp" />
                  <source srcSet={setSrcWebp(webp)} type="image/webp" />
                </>
              )}
              <source media="(min-width: 769px)" srcSet={srcImg} />
              <img
                ref={imgRef}
                src={srcImg}
                alt={`${title} ${description}`}
                title={title}
                className="book-info__img"
              />
            </picture>
          ) : (
            <img
              ref={imgRef}
              src={defaultBookIcon}
              className="book-info__img book-info_img_default"
              alt="Изображение книги"
            />
          )}
        </>
      </Component>
    );
  },
);

MediaContentImg.propTypes = {
  type: PropTypes.string,
  title: PropTypes.string,
  imageUrl: PropTypes.string,
  styleName: PropTypes.string,
  styleNameVideo: PropTypes.string,
  urlLink: PropTypes.string,
  videoHref: PropTypes.string,
  isLink: PropTypes.bool,
  onClick: PropTypes.func,
};

export default MediaContentImg;
