import React from 'react';
import cx from 'classnames';

import './mediaNavMobile.scss';

import MediaFilter from '~components/Mediateka/MediaFilter/';

export const MediaMenuMobile = ({
  options = [],
  optionsChildren = [],
  value = '',
  valueChild = '',
  handleChange = f => f,
  handleChangeChildren = f => f,
  isClearable,
}) => (
  <div className="media-nav-mobile">
    <MediaFilter
      options={options}
      value={value}
      onChange={handleChange}
      styleName="media-nav-mobile-parent"
      placeholder="menu"
      styleNamePrefix={cx('media-nav-select-mobile', {
        'media-nav-select-child': optionsChildren.length,
      })}
    />

    {optionsChildren.length > 0 && (
      <MediaFilter
        options={optionsChildren}
        value={valueChild}
        onChange={handleChangeChildren}
        styleName="media-nav-mobile-children"
        placeholder="Выберите"
        styleNamePrefix="media-nav-select-mobile media-nav-select-mobile-child"
        isClearable={isClearable}
      />
    )}
  </div>
);
