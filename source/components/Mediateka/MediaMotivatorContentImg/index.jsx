import React from 'react';

import { urlToAPI } from '~dist/browserUtils';

import MediaContentImg from '~components/Mediateka/MediaContentImg';
import MediaMotivatorIcons from './MediaMotivatorIcons/';

import './mediaMotivatorContentImg';

const MediaMotivatorContentImg = ({
  book,
  imageUrl,
  className = 'media-motivator-img',
  openPopup,
  isShowIcons,
}) => (
  <div className={className}>
    <MediaContentImg
      type={book?.type}
      title={book?.title}
      description={book?.description}
      url={urlToAPI}
      imageUrl={book.image && book.image.url}
      optimizedImg={(book.image && book.image.optimized) || []}
      styleName={'media-motivator-link-img'}
      onClick={openPopup}
      isLink
    />
    {isShowIcons && <MediaMotivatorIcons url={imageUrl} currentBook={book} />}
  </div>
);

export default MediaMotivatorContentImg;
