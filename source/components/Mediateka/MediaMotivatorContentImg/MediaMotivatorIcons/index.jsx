import React from 'react';
import Icons from '~components/Icons/Icons';
import ShareButton from '../../../ShareButton/';

import { urlToAPI } from '../../../../dist/browserUtils';

const MediaMotivatorIcons = ({ url, currentBook = {} }) => {
  const { id, title = ' Экзегет' } = currentBook;

  const pathname = process.env.BROWSER && window.location.href;
  const urlShare = `${pathname}&motivator=${id}`;

  return (
    <div className="media-motivator-icons">
      <a href={`${urlToAPI}${url}`} download title={title}>
        <Icons className="media-icons-item" icon="download_media" />
      </a>
      <ShareButton className="media-motivator-share" urlShare={urlShare} isNotShowText />
    </div>
  );
};

export default MediaMotivatorIcons;
