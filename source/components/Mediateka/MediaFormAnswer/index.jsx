import React from 'react';

import './mediaFormAnswer.scss';

const MediaFormAnswer = () => <div className="media-form-answer">Спасибо за Ваш комментарий.</div>;

export default MediaFormAnswer;
