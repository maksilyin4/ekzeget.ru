import React from 'react';
import cx from 'classnames';

import Button from '../../ButtonNew';

export const ChapterItems = ({ chaptersCount, bookCode, changeIsShow, className }) => {
  const chapters = [];
  for (let i = 1; i <= chaptersCount; i += 1) {
    chapters.push(
      <Button
        className={cx('testament__book-chapter', className)}
        key={i}
        onClick={() => changeIsShow()}
        type="NavLink"
        mod="gradient"
        title={i}
        to={`/bible/${bookCode}/glava-${i}/`}
      />,
    );
  }

  const count = chapters.length > 10 ? 10 : chapters.length;

  return (
    <div
      className={cx('testament__book-chapters', className)}
      style={{ gridTemplateColumns: `repeat(${count}, 1fr)` }}>
      {chapters}
    </div>
  );
};
