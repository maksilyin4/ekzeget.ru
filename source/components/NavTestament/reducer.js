export const initialState = {
  isShow: true,
  isActive: false,
  bookCode: '',
};

export const types = {
  CHANGE_IS_SHOW: 'CHANGE_IS_SHOW',
  CHANGE_IS_ACTIVE: 'CHANGE_IS_ACTIVE',
  CHANGE_IS_SHOW_EXIT: 'CHANGE_IS_SHOW_EXIT',
};

export const reducer = (state, action) => {
  switch (action.type) {
    case types.CHANGE_IS_SHOW: {
      return { ...state, isShow: !state.isShow, isActive: false };
    }
    case types.CHANGE_IS_ACTIVE: {
      return { ...state, isShow: true, isActive: true, bookCode: action.bookCode };
    }
    case types.CHANGE_IS_SHOW_EXIT: {
      return { ...state, isShow: !state.isShow, isActive: false };
    }
    default:
      return state;
  }
};
