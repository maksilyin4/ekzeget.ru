import React, { useEffect, useCallback, useReducer } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import cx from 'classnames';
import nanoid from 'nanoid';
import List, { ListItem } from '../List/List';

import { initialState, reducer, types } from './reducer';

import { getIsAdaptive } from '~dist/selectors/screen';

import { ChapterItems } from './ChapterItems/';

import { smoothScrollTo } from '~dist/utils';

const ListItems = ({ list, activeBook, showChapter, withScroll }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const isAdaptive = useSelector(getIsAdaptive);
  const popupId = nanoid();

  const popupTypes = `popup-chapter-${popupId}`;

  useEffect(() => {
    const event = isAdaptive ? 'touchstart' : 'click';
    const delay = isAdaptive ? 300 : 0;

    const listener = event => {
      if (!event.target.classList.contains(popupTypes)) {
        setTimeout(() => {
          dispatch({ type: types.CHANGE_IS_SHOW });
        }, delay);
      }
    };

    if (state.isActive) {
      window.addEventListener(event, listener);
    }
    return () => window.removeEventListener(event, listener);
  }, [state.isActive]);

  const changeIsShow = useCallback(() => {
    dispatch({ type: types.CHANGE_IS_SHOW });

    smoothScrollTo();
  }, []);

  const setShowChapters = useCallback(
    value => () => {
      dispatch({ type: types.CHANGE_IS_ACTIVE, bookCode: value });
    },
    [],
  );

  const { isShow, isActive, bookCode } = state;

  return (
    <List className={cx({ testament__book_scroll: withScroll })}>
      {list.map(book => (
        <div
          key={book.id}
          className={cx('testament__book', {
            active: isActive && bookCode === book.code,
          })}>
          <ListItem
            active={activeBook === book.code}
            title={book.title}
            onClick={setShowChapters(book.code)}>
            {showChapter && isShow && (
              <ChapterItems
                className={popupTypes}
                chaptersCount={book.parts}
                bookCode={book.code}
                changeIsShow={changeIsShow}
              />
            )}
          </ListItem>
        </div>
      ))}
    </List>
  );
};

ListItems.propTypes = {
  list: PropTypes.array.isRequired,
  activeBook: PropTypes.string.isRequired,
  showChapter: PropTypes.bool.isRequired,
};

export default ListItems;
