import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import {
  getTestamentIsLoading,
  getNewTestament,
  getOldTestament,
} from '../../dist/selectors/Bible';
import getScreen from '../../dist/selectors/screen';

import Preloader from '../Preloader/Preloader';
import Tabs, { Tab, TabContent } from '../Tabs/Tabs';
import ListItems from './ListItems';

import './navTestament.scss';

const NavTestament = ({
  activeTestament,
  activeBook,
  showChapter = false,
  getBackActiveTab = f => f,
}) => {
  const testamentIsLoading = useSelector(getTestamentIsLoading);
  const newTestament = useSelector(getNewTestament);
  const oldTestament = useSelector(getOldTestament);
  const screen = useSelector(getScreen);

  const [activeTab, setActiveTab] = useState(2);

  useEffect(() => {
    setActiveTab(Number(activeTestament));
  }, [activeTestament]);

  const tabToggle = value => {
    setActiveTab(value);
  };

  useEffect(() => {
    getBackActiveTab(activeTab);
  }, [activeTab]);

  const currentTestament = activeTab === 1 ? newTestament : oldTestament;

  if (testamentIsLoading) {
    return (
      <div className="testament__nav">
        <Preloader />
      </div>
    );
  }

  return (
    <>
      {(screen.desktop || screen.tabletMedium) && (
        <div className="testament__nav">
          <Tabs
            activeTab={activeTab}
            callBack={tabToggle}
            className="bible-tabs tabs_in-paper"
            type="justify">
            <Tab label="Новый завет" value={2} />
            <Tab label="Ветхий завет" value={1} />
            <TabContent>
              <ListItems
                list={currentTestament}
                activeBook={activeBook}
                showChapter={showChapter}
              />
            </TabContent>
          </Tabs>
        </div>
      )}
    </>
  );
};

NavTestament.propTypes = {
  activeBook: PropTypes.string,
  showChapter: PropTypes.bool,
  activeTestament: PropTypes.number,
};

export default NavTestament;
