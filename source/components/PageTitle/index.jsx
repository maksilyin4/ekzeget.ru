import React from 'react';
import './styles.scss';
import classNames from 'classnames';
import { Link } from 'react-router-dom';

export default function ReadingPageTitle({
  children,
  className,
  classNameButton,
  withMargin,
  button = '',
  onClick,
  href,
}) {
  return (
    <div className={'rpt-wrapper'}>
      <h1 className={classNames('rpt-title', className, withMargin && 'rpt-margin')}>{children}</h1>
      {button !== '' ? (
        href ? (
          <Link className={classNames('rpt-btn', classNameButton)} to={href}>
            {button}
          </Link>
        ) : (
          <button onClick={onClick} className={classNames('rpt-btn', classNameButton)}>
            {button}
          </button>
        )
      ) : null}
    </div>
  );
}
