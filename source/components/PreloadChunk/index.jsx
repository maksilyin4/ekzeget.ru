import Book from '../../pages/Bible/Book';
import Chapter from '../../pages/Bible/Chapter';

import BibleGroup from '../../pages/BibleGroup/BibleGroupLoadable';
import GroupItem from '../../pages/BibleGroup/GroupItem/GroupItemLoadable';
import AddBibleGroup from '../../pages/BibleGroup/AddGroup/AddGroupLoadable';
import BibleMap from '../../pages/BibleMap/BibleMapLoadable';
import Dictonaries from '../../pages/Dictonaries';
import Exegetes from '../../pages/Exegetes';
import Lecture from '../../pages/Lecture';
import Home from '../../pages/Home';
import Preaching from '../../pages/Preaching';
import Quiz from '../../pages/Quiz/QuizLoadable';
import QuizQuestions from '../QuizQuestions/QuizQuestionsLoadable';
import QuizResult from '../QuizResults/QuizResultsLoadable';
import Video from '../../pages/Video';
import AllAboutBible from '../../pages/AboutBible';
import Mediateka from '../../pages/Mediateka';
import LK from '../../pages/Lk';
import ReadingPlan from '~pages/ReadingPlan';

let myTimeout;

const onMouseBibleBook = () => Book.preload();
const onMouseBibleChapter = () => Chapter.preload();

const onMouseBible = () => {
  onMouseBibleBook();
  onMouseBibleChapter();
};

const onMouseBibleGroups = () => {
  BibleGroup.preload();
  GroupItem.preload();
  AddBibleGroup.preload();
};

const onMouseBiblemaps = () => BibleMap.preload();

const onMouseDictonaries = () => Dictonaries.preload();

const onMouseExegetes = () => Exegetes.preload();

const onMouseLecture = () => Lecture.preload();

const onMouseHome = () => Home.preload();
const onMousePlan = () => ReadingPlan.preload();

const onMousePreaching = () => Preaching.preload();

const onMouseQuiz = () => {
  Quiz.preload();
  QuizQuestions.preload();
  QuizResult.preload();
};

const onMouseVideo = () => Video.preload();

const onMouseAllAboutBible = () => AllAboutBible.preload();

const onMouseMediateka = () => Mediateka.preload();
export const preloadLk = LK.preload;

export const onMouseObj = {
  onMouseBible,
  onMouseBibleGroups,
  onMouseBiblemaps,
  onMouseDictonaries,
  onMouseExegetes,
  onMouseHome,
  onMouseLecture,
  onMousePreaching,
  onMouseQuiz,
  onMouseVideo,
  onMousePlan,
  onMouseAllAboutBible,
  onMouseMediateka,
  onMouseBibleBook,
  onMouseBibleChapter,
};

export const onMouseEnter = target => {
  myTimeout = setTimeout(() => {
    target();
  }, 500);
};

export const onMouseLeave = () => clearTimeout(myTimeout);
