import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { bookmarks as bookmarksSelector, getIsLoading } from '../../../dist/selectors/Bookmarks';
import Button from '../../Button/Button';
import Icon from '../../Icons/Icons';

export const GetBookmark = memo(({ short, type }) => {
  const bookmarks = useSelector(bookmarksSelector);
  const isLoading = useSelector(getIsLoading);

  return type === 'circle' ? (
    !isLoading && (
      <Button
        className="active-bible-bookmark"
        type="circle"
        icon="bookmark"
        style={{
          backgroundColor: bookmarks.bookmark[short].color,
        }}
      />
    )
  ) : (
    <div title="Удалить закладку">
      <Icon
        icon="bookmark"
        style={{
          color: bookmarks.bookmark[`${short}`].color,
        }}
      />
    </div>
  );
});

GetBookmark.propTypes = {
  short: PropTypes.string,
  type: PropTypes.string,
};
