import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import cx from 'classnames';
import { CirclePicker } from 'react-color';

import { Popup } from '../../../dist/browserUtils';
import { addStatus, addIsLoading } from '../../../dist/selectors/Bookmarks';
import { userLoggedIn } from '../../../dist/selectors/User';
import bookmarksActions from '../../../dist/actions/Bookmarks';
import { sendSocialActions } from '../../../dist/utils';
import Button from '../../Button/Button';
import Icon from '../../Icons/Icons';
import { popupAuth } from '~components/Popups/PopupAuth';

import './addBookmark.scss';

const refreshPosition = () => {
  Popup.refreshPosition();
};

if (Popup) {
  Popup.addCloseListener(() => {
    window.removeEventListener('scroll', refreshPosition);
  });
}

class AddBookmark extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpened: false,
    };
  }

  _selectBookmark = color => {
    this.props.addBookmark(`mark=${this.props.short}&color=${color.hex.toUpperCase()}`);

    this.setState({ isOpened: false });
    setTimeout(() => {
      Popup.close();
    }, 1000);
  };

  _renderColorPicker = element => {
    if (this.props.userIsLoggedIn) {
      sendSocialActions({
        ga: ['event', 'event_name', { event_category: 'click_zakladka', event_action: 'click' }],
        ym: 'click_zakladka',
      });
      this.setState({ isOpened: true });
      window.addEventListener('scroll', refreshPosition);
      Popup.plugins().popover(
        <div className="add-bookmark__popover">
          <div className="pulldown__head">
            <div className="pulldown__title">Добавить закладку</div>
          </div>
          <div className="pulldown__body">
            <CirclePicker onChange={this._selectBookmark} />
            <div className="add-bookmark__notice">Выберите цвет</div>
          </div>
        </div>,
        element,
      );
    } else {
      const text = this.props.isVerse ? 'стих' : 'главу';
      popupAuth(`Добавить ${text} в закладку`);
    }
  };

  render() {
    const { type } = this.props;
    return (
      <div
        className={cx('add-bookmark', this.props.className, { active: this.state.isOpened })}
        ref={button => {
          this.buttonRef = button;
        }}>
        {type === 'circle' ? (
          <Button
            type="circle"
            icon="bookmark"
            onClick={() => this._renderColorPicker(this.buttonRef)}
          />
        ) : (
          <div title="Добавить закладку">
            <Icon icon="bookmark" onClick={() => this._renderColorPicker(this.buttonRef)} />
          </div>
        )}
      </div>
    );
  }
}

AddBookmark.propTypes = {
  short: PropTypes.string,
  type: PropTypes.string,
  className: PropTypes.string,
  userIsLoggedIn: PropTypes.bool,
};

const mapStateToProps = state => ({
  addIsLoading: addIsLoading(state),
  addStatus: addStatus(state),
  userIsLoggedIn: userLoggedIn(state),
});
const mapDispatchToProps = { ...bookmarksActions };

export default connect(mapStateToProps, mapDispatchToProps)(AddBookmark);
