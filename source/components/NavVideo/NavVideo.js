import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Icon from '../Icons/Icons';

class NavVideo extends Component {
  static propTypes = {
    chaptersCount: PropTypes.number,
    bookId: PropTypes.number,
    currentPage: PropTypes.number,
    activePlaylist: PropTypes.string,
  };

  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
    };
  }

  _toggleList = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  render() {
    const { chaptersCount, currentPage, activePlaylist, bookId } = this.props;
    return (
      <div className="bible__nav">
        {currentPage - 1 !== 0 && (
          <Link
            to={`/video/${activePlaylist}/${bookId}/${currentPage - 1}/`}
            className="bible__nav-item bible__nav-item_prev">
            <Icon icon="angle-left" />
            {`Глава ${currentPage - 1}`}
          </Link>
        )}
        {currentPage + 1 <= chaptersCount && (
          <Link
            to={`/video/${activePlaylist}/${bookId}/${currentPage + 1}/`}
            className="bible__nav-item bible__nav-item_next">
            {`Глава ${currentPage + 1}`}
            <Icon icon="angle-right" />
          </Link>
        )}
      </div>
    );
  }
}

export default NavVideo;
