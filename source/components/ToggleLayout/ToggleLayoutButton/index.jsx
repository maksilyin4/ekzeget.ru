import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Link } from 'react-router-dom';

import ToggleLayoutIcon from './ToggleLayoutIcon';

import { getText } from '../utilsToggleLayout';

const ToggleLayoutButton = ({
  position,
  text,
  toggleFunc,
  iconHide = '',
  iconShow = '',
  positionState,
  urlText = 'Вернуться',
  urlBack,
}) => {
  const isPosition = positionState[position];

  return urlBack && position === 'content' ? (
    <Link
      to={urlBack}
      className={cx('toggle-layout__item ', {
        'toggle-layout__item_collapsed': isPosition,
      })}>
      <ToggleLayoutIcon iconShow={iconShow} iconHide={iconHide} isPosition={isPosition} />
      {urlText}
    </Link>
  ) : (
    <button
      className={cx('toggle-layout__item ', {
        'toggle-layout__item_collapsed': isPosition,
      })}
      onClick={() => toggleFunc(position)}>
      <ToggleLayoutIcon iconShow={iconShow} iconHide={iconHide} isPosition={isPosition} />
      {getText({ isPosition, text, position })}
    </button>
  );
};

ToggleLayoutButton.propTypes = {
  text: PropTypes.string,
  iconHide: PropTypes.string,
  iconShow: PropTypes.string,
  urlBack: PropTypes.string,
  toggleFunc: PropTypes.func,
  position: PropTypes.string,
  positionState: PropTypes.object,
  urlText: PropTypes.string,
};

export default ToggleLayoutButton;
