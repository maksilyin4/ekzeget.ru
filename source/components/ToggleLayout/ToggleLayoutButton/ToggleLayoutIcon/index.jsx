import React from 'react';
import cx from 'classnames';

import Icon from '../../../Icons/Icons';

const ToggleLayoutIcon = ({ iconShow, iconHide, isPosition }) => {
  return (
    (iconHide || iconShow) && (
      <Icon
        className={cx('toggle-layout-icon', {
          'icon-is-show': isPosition,
        })}
        icon={isPosition ? iconShow : iconHide}
      />
    )
  );
};

export default ToggleLayoutIcon;
