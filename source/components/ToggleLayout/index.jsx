import React, { useMemo } from 'react';
import PropTypes from 'prop-types';

import ToggleLayoutButton from './ToggleLayoutButton';

import { toggleLayoutButton, filterToggleLayout } from './utilsToggleLayout';

import './toggleLayout.scss';

const ToggleLayout = ({
  toggleFunc,
  screen,
  urlText,
  urlBack,
  left = false,
  content = false,
  right = false,
}) => {
  const { tablet } = screen;

  const toggleLayoutButtons = useMemo(() => {
    if (tablet) {
      return toggleLayoutButton.filter(filterToggleLayout);
    }
    return toggleLayoutButton;
  }, [tablet]);

  return (
    <div className="toggle-layout wrapper_multi-col">
      {toggleLayoutButtons.map(({ className, position, text, iconHide, iconShow }) => (
        <div key={position} className={className}>
          <ToggleLayoutButton
            position={position}
            text={text}
            toggleFunc={toggleFunc}
            iconHide={iconHide}
            iconShow={iconShow}
            urlText={urlText}
            urlBack={urlBack}
            positionState={{ left, content, right }}
          />
        </div>
      ))}
    </div>
  );
};

ToggleLayout.propTypes = {
  left: PropTypes.bool,
  right: PropTypes.bool,
  content: PropTypes.bool,
  toggleFunc: PropTypes.func,
  screen: PropTypes.object,
  urlText: PropTypes.string,
  urlBack: PropTypes.string,
};
export default ToggleLayout;
