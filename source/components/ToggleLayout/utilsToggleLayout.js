export const toggleLayoutButton = [
  {
    className: 'side bible-nav-width',
    position: 'left',
    text: 'меню',
    iconHide: 'arrow-bible-left',
    iconShow: 'arrow-bible-right',
  },
  {
    className: 'content-center',
    position: 'content',
    text: 'текст',
    iconHide: 'arrow-bible-left',
    iconShow: 'arrow-bible-right',
  },
  {
    className: 'bible-side-right bible-nav-width',
    position: 'right',
    text: 'медиа',
    iconHide: 'arrow-bible-right',
    iconShow: 'arrow-bible-left',
  },
];

export const getText = ({ isPosition, text, position }) => {
  const contentText = position === 'content' ? 'Показать' : 'Развернуть';
  return `${isPosition ? contentText : 'Скрыть'} ${text}`;
};

export const filterToggleLayout = ({ position }) => position !== 'left';
