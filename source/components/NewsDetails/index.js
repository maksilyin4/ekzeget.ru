import R from 'ramda';
import React, { PureComponent } from 'react';
import dayjs from 'dayjs';
import ruLocale from 'dayjs/locale/ru';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import Breadcrumbs from '../Breadcrumbs/Breadcrumbs';
import Preloader from '../Preloader/Preloader';
import { AXIOS } from '../../dist/ApiConfig';
import './styles.scss';
import { smoothScrollTo } from '../../dist/utils';

export default class NewsDetails extends PureComponent {
  state = {
    isFetching: false,
    content: R.pathOr(null, ['directProps', 'articleDetail'], this.props),
  };

  componentDidMount() {
    smoothScrollTo(800);
    this.setState({
      isFetching: true,
    });

    const {
      match: {
        params: { newDetail },
      },
    } = this.props;

    AXIOS.get(`article/detail/${newDetail}`).then(
      ({ data, status }) => {
        if (status === 200) {
          this.setState({
            isFetching: false,
            content: data.article,
          });
        } else {
          this.setState({
            isFetching: false,
          });
        }
      },
      () => {
        this.setState({
          isFetching: false,
        });
      },
    );
  }

  _breadItems = () => {
    const { content } = this.state;
    const path = this.getUrl(this.props.match);
    return [
      { path: '/', title: 'Главная' },
      { path, title: 'Новости и обновления' },
      { path: '', title: !!content && content.title },
    ];
  };

  backUrl = id => {
    const path = this.getUrl(this.props.match);
    switch (id) {
      case 1:
        return path;
      case 3:
        return '/bible-group';
      default:
        return path;
    }
  };

  getUrl = match => {
    const {
      params: { newDetail },
      url,
    } = match;
    return url.replace(`/${newDetail}/`, '');
  };

  render() {
    const { isFetching, content } = this.state;

    const isContent = !!content;
    const categoryId = R.pathOr(1, ['category_id'], content);
    return (
      <div className="page news__details">
        <div className="wrapper">
          <Breadcrumbs data={this._breadItems()} />
          <div className="page__head page__head_two-col">
            <h1 className="page__title">
              {isContent && content.title}
              {isContent && (
                <Helmet title={`${content.title}  Новости || Экзегет.ру Библейский портал`} />
              )}
            </h1>
          </div>
        </div>
        <div className="page__body news__details__body">
          <div className="wrapper wrapper_two-col">
            <div className="news__details__content">
              {isContent ? (
                <div
                  className="news__details__content-text text-format"
                  dangerouslySetInnerHTML={{ __html: content.body }}
                />
              ) : (
                <div className="news__details__content-text text-format">
                  {isFetching ? <Preloader /> : <p>Такой статьи нет в базе</p>}
                </div>
              )}
              <Link className="breadcrumbs__item-link" to={this.backUrl(categoryId)}>
                Вернуться к списку новостей
              </Link>
            </div>
            <div className="news__details__date">
              {isContent && (
                <div className="news__details__date-content">
                  <p>Новость добавлена</p>
                  <p>
                    {dayjs
                      .unix(content.created_at)
                      .locale('ru', ruLocale)
                      .format('DD MMM YYYY')}
                  </p>
                </div>
              )}
              {/* <SubscribeForm /> */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
