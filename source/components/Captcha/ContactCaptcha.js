import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import cx from 'classnames';
import { AXIOS } from '../../dist/ApiConfig';
import Preloader from '../Preloader/Preloader';
import './styles.scss';

const refresh = require('../../assets/images/icons/refresh-captcha.svg');

// TODO: скорректировать url для img
// капча временно не работает, проблема с кроссдоменными запросами
export default class ContactCaptcha extends Component {
  state = {
    isFetch: false,
    url: null,
  };

  componentDidMount() {
    this.getCaptcha();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value === '' && this.props.value !== '') this.getCaptcha();
  }

  getCaptcha = () => {
    this.setState({ isFetch: true });
    AXIOS.get('site/captcha/', {
      withCredentials: true, // с этим параметром ключи генерируются, но вылетает CORS ошибка
    }).then(
      ({ data: { captcha }, status }) => {
        if (status === 200) {
          this.setState({
            url: captcha,
            isFetch: false,
          });
        }
      },
      err => {
        this.setState({ isFetch: false });
        console.log(err);
      },
    );
  };

  render() {
    const { url, isFetch } = this.state;
    const { value, onChange, className } = this.props;

    return (
      <div className="contact__captcha">
        <div className="user_form_input">
          <label htmlFor="contacts-verifyCode">
            <input
              type="text"
              className={cx(className)}
              name="verifyCode"
              id="contacts-verifyCode"
              onChange={onChange}
              value={value}
              autoComplete="off"
              placeholder="Введите код с картинки *"
              required
            />
          </label>
        </div>
        {isFetch ? (
          <Preloader />
        ) : (
          <div className="contact__captcha__image">
            {url ? <img src={`data:image/png;base64,${url}`} alt="Captcha" /> : null}
            <button className="btn" type="button" onClick={this.getCaptcha}>
              <img src={refresh} alt="button" />
            </button>
          </div>
        )}
      </div>
    );
  }
}

ContactCaptcha.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};
