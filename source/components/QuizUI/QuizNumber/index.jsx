import React, { memo } from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';

import { statusesTypes } from '~dist/utils';

import './styles.scss';

const QuizNumber = ({ questionIndex, currentQuestionId = 0, status, isCorrectMsg, onClick }) => (
  <li
    className={cx('quiz-questions__item', {
      'quiz-question__completed': status === statusesTypes.OK,
      'quiz-question__failed': status === statusesTypes.NO,
      'interactive': onClick
    })}
    onClick={onClick}
  >
    <span
      className={cx('quiz-questions__desc', {
        'quiz-question__current': questionIndex + 1 === currentQuestionId && isCorrectMsg === null,
      })}>
      {questionIndex + 1}
    </span>
  </li>
);

QuizNumber.propTypes = {
  questionIndex: PropTypes.number,
  currentQuestionId: PropTypes.number,
  status: PropTypes.string,
  isCorrectMsg: PropTypes.bool,
};

export default memo(QuizNumber);
