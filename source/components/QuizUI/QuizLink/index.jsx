import React, { memo } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import './styles.scss';

const QuizLink = ({
  text,
  path,
  className = 'btn quiz-ui__link',
  getNewQuestion = f => f,
  isIcon = true,
}) => (
  <Link className={className} to={path} onClick={getNewQuestion}>
    {text}
    {isIcon && <i className="quiz-ui__right" />}
  </Link>
);

QuizLink.defaultProps = {
  path: '/bibleyskaya-viktorina/',
  text: 'Назад к Викторине',
};

QuizLink.propTypes = {
  path: PropTypes.string,
  text: PropTypes.string,
  getNewQuestion: PropTypes.func,
  className: PropTypes.string,
  isIcon: PropTypes.bool,
};

export default memo(QuizLink);
