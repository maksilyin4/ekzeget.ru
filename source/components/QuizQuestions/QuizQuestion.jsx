import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { QuizQuestionsContext } from './index';

const QuizQuestion = ({ isCorrect, text, value, questionId, answerId, answers }) => {
  const [getUpdateCorrectMsg] = useContext(QuizQuestionsContext);

  const getCorrect = () => {
    let boolAnswer = false;
    let correctItem = answers.find(item => item.is_correct);

    if (isCorrect) {
      boolAnswer = true;
    }

    if (!correctItem) {
      correctItem = 'В данном вопросе отсутствует верный ответ';
    } else {
      correctItem = correctItem.text;
    }

    getUpdateCorrectMsg(boolAnswer, text, questionId, answerId, correctItem);
  };

  return (
    <div className="quiz-question" onClick={getCorrect}>
      <span className="quiz-question__number"> {value + 1}: </span>
      <p className="quiz-question__text">{text}</p>
    </div>
  );
};

QuizQuestion.propTypes = {
  isCorrect: PropTypes.bool,
  text: PropTypes.string,
  value: PropTypes.number,
  questionId: PropTypes.number,
  answerId: PropTypes.number,
  answers: PropTypes.arrayOf({
    is_correct: PropTypes.bool,
  }),
};

export default QuizQuestion;
