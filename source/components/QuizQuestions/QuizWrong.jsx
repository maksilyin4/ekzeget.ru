import React, { memo } from 'react';
import PropTypes from 'prop-types';

const QuizWrong = ({ message }) => (
  <div className="quiz-wrong">
    <span className="quiz-wrong__title"> Вы ответили неверно! </span>
    <span className="quiz-correct__desc"> Неверный ответ: </span>
    <p className="quiz-wrong__text">{message}</p>
  </div>
);

QuizWrong.propTypes = {
  message: PropTypes.string,
};

export default memo(QuizWrong);
