import React, { memo } from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';
import QuizQuestionInterpretations from "~components/QuizQuestions/QuizQuestionInterpretations";

const QuizCorrect = ({ isCorrectMsg, description, interpretations, verse, correctMessage }) => {
  return (
    <div className="quiz-question__correct quiz-correct">
      <div className="quiz-correct__head">
        <span
          className={cx('quiz-correct__title', {
            'quiz-correct__desc': !isCorrectMsg,
          })}>
          {isCorrectMsg ? 'Вы ответили верно!' : 'Правильный ответ:'}{' '}
        </span>
        <p
          className={cx('quiz-correct__question', {
            'quiz-correct__question-size': !isCorrectMsg,
          })}>
          {correctMessage}
        </p>
      </div>
      <div className="quiz-correct__body">
        <div className="quiz-correct__explanation explanation">
          <span className="explanation__title"> Пояснение: </span>
          <div className="explanation__text" dangerouslySetInnerHTML={{ __html: description }} />
        </div>

        {interpretations && interpretations.length > 0 && (
          <QuizQuestionInterpretations interpretations={interpretations} verse={verse} />
        )}
      </div>
    </div>
  );
};

QuizCorrect.propTypes = {
  isCorrectMsg: PropTypes.bool,
  description: PropTypes.string,
  interpretations: PropTypes.array,
  verse: PropTypes.object,
  correctMessage: PropTypes.string,
};

export default memo(QuizCorrect);
