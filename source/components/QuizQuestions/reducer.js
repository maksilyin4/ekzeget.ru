export const questionInitialState = {
  statuses: [],
  isCorrectMsg: null,
  message: '',
  correctMessage: '',
  isLoadingAnswer: false,
  isFinish: false,
};

export const types = {
  UPDATE_ALL: 'UPDATE_ALL',
  NEXT_STEP: 'NEXT_STEP',
  STATUSES_ARR: 'STATUSES_ARR',
  STATUS_MESSAGE: 'STATUS_MESSAGE',
  MESSAGE: 'MESSAGE',
  CORRECT_MESSAGE: 'CORRECT_MESSAGE',
  IS_LOADING_ANSWER: 'IS_LOADING_ANSWER',
  IS_FINISH: 'IS_FINISH',
  CURRENT_QUESTION_ID: 'CURRENT_QUESTION_ID',
};

export const reducer = (state, action) => {
  const {
    statuses,
    isCorrectMsg,
    message,
    correctMessage,
    isLoadingAnswer,
    completed,
    isFinish,
    currentQuestionId,
  } = action;
  switch (action.type) {
    case types.UPDATE_ALL:
      return { ...state, ...completed };
    case types.NEXT_STEP:
      return { ...state, currentQuestionId, isCorrectMsg, message };
    case types.STATUSES_ARR:
      return { ...state, statuses };
    case types.STATUS_MESSAGE:
      return { ...state, isCorrectMsg };
    case types.MESSAGE:
      return { ...state, message };
    case types.CORRECT_MESSAGE:
      return { ...state, correctMessage };
    case types.IS_LOADING_ANSWER:
      return { ...state, isLoadingAnswer };
    case types.IS_FINISH:
      return { ...state, isFinish };
    case types.CURRENT_QUESTION_ID:
      return { ...state, currentQuestionId };
    default:
      return state;
  }
};
