import React, { memo } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import QuizQuestionBibleTitle from './QuizQuestionBibleTitle';
import QuizQuestionMain from './QuizQuestionMain';
import QuizCorrect from './QuizCorrect';
import QuizLink from '../QuizUI/QuizLink';
import Preloader from '../Preloader/Preloader';

const QuizQuestionForNewUser = ({
  questionTitleRef,
  questionsRef,
  mainTitle,
  isLoadingAnswer,
  isCorrectMsg,
  message,
  question,
  correctMessage,
  currentId,
  isSafari,
}) => (
  <>
    <div
      className={cx('quiz-questions__header', {
        'quiz-testament__header-safari': isSafari,
      })}>
      <QuizQuestionBibleTitle questionRef={questionTitleRef} />
    </div>
    <div className="quiz-questions quiz-testaments quiz-questions__for-new-user" ref={questionsRef}>
      {/* Сценарий, когда при быстром выборе новых вопросов на странице Все вопросы не успевают обновится данные */}
      {/* null в случае если первый заход */}
      {currentId === null || currentId === question.id ? (
        <QuizQuestionMain
          title={mainTitle}
          isLoadingAnswer={isLoadingAnswer}
          isCorrectMsg={isCorrectMsg}
          message={message}
          question={question}
          correctMessage={correctMessage}
        />
      ) : (
        <Preloader />
      )}
      {/* Для СЕО. Говорят, скрытый текст норм индексируется щас. */}
      <div className="quiz-questions__correct-answer-hidden">
        <QuizCorrect
          isCorrectMsg={isCorrectMsg}
          description={question.description}
          interpretations={question.interpretations}
          verse={question.verse}
          correctMessage={correctMessage}
        />
      </div>
      {isCorrectMsg !== null && (
        <div className="quiz-link__wrapper quiz-link__start-question">
          <QuizLink
            text="Начать викторину"
            className="quiz-testaments__to-quiz red btn"
            path="/bibleyskaya-viktorina/"
            isIcon={false}
          />
        </div>
      )}
    </div>
  </>
);

QuizQuestionForNewUser.propTypes = {
  questionTitleRef: PropTypes.shape({
    current: PropTypes.objectOf(PropTypes.element),
  }),
  questionsRef: PropTypes.shape({
    current: PropTypes.objectOf(PropTypes.element),
  }),
  mainTitle: PropTypes.string,
  isLoadingAnswer: PropTypes.bool,
  isCorrectMsg: PropTypes.oneOfType([null, PropTypes.bool]),
  message: PropTypes.object,
  question: PropTypes.shape({
    description: PropTypes.string,
    comment: PropTypes.string,
    interpretations: PropTypes.array,
    verse: PropTypes.object
  }),
  correctMessage: PropTypes.string,
  currentId: PropTypes.number,
  isSafari: PropTypes.bool,
};

export default memo(QuizQuestionForNewUser);
