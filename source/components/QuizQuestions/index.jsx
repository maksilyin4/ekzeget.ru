import R from 'ramda';
import React, { createContext, useCallback, useEffect, useMemo, useReducer, useRef } from 'react';
import { connect, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import cx from 'classnames';

import { getAnswer } from '../../dist/quizAPI';
import { getQuestionParams, smoothScrollTo, statusesTypes } from '../../dist/utils';

import { getCurrentQuestion } from '../../dist/actions/Quiz';
import {
  getCurrentQuestionData,
  getIsLoadedCurrentQuestion,
  getQuestions,
  getQuestionsIsLoading,
  getQuestionsStatus,
  getErrorQuestion,
} from '../../dist/selectors/Quiz';
import { getIsTablet } from '../../dist/selectors/screen';
import { questionInitialState, reducer, types } from './reducer';

import QuizNumberQuestion from './QuizNumberQuestion';
import QuizLink from '../QuizUI/QuizLink';
import Preloader from '../Preloader/Preloader';
import { DESC_QUIZ, TITLE_QUIZ } from '../../pages/Quiz/constants';
import QuizQuestionBibleTitle from './QuizQuestionBibleTitle';
import QuizQuestionMain from './QuizQuestionMain';
import QuizQuestionForNewUser from './QuizQuestionForNewUser';

import './styles.scss';
import { getIsSafari } from '../../store/browser/selectors';
import AudioPlayer from '../AudioPlayer/AudioPlayer';
import AudioCheckbox from '~components/AudioPlayer/AudioCheckbox';
import { useLocalStorage } from 'src/hooks/useLocalStorage';
import { useParams } from 'react-router-dom/cjs/react-router-dom.min';
import { userPlans } from '~dist/selectors/ReadingPlan';
import { getUserReadingPlan } from '../../dist/actions/ReadingPlan';

export const QuizQuestionsContext = createContext(null);

const mapStateToProps = state => ({
  questions: getQuestions(state),
  isLoading: getQuestionsIsLoading(state),
  userPlans: userPlans(state),
  questionsStatus: getQuestionsStatus(state),
  isTablet: getIsTablet(state),
  questionData: getCurrentQuestionData(state),
  isLoadedCurrentQuestion: getIsLoadedCurrentQuestion(state),
});

const mapDispatchToProps = { getCurrentQuestion, getUserReadingPlan };

const sizeOfQuiz = 10;
const yPositionQuizContainer = {
  afterUpdateQuiz: {
    default: 50,
    tablet: 140,
  },
  initNewQuestion: {
    default: 50,
    tablet: 140,
  },
};

const QuizQuestions = ({
  history: { push, location = {} },
  questions,
  isLoading,
  questionsStatus,
  isTablet,
  getUserReadingPlan,
  userPlans,
  questionData,
  getCurrentQuestion,
  isLoadedCurrentQuestion,
}) => {
  const isSafari = useSelector(getIsSafari);
  const errorQuestion = useSelector(getErrorQuestion);

  const [isAutoplay, setAutoplay] = useLocalStorage('is_autoplay', false);

  // Сценарий если нам надо воспроизвести уникальный вопрос вне рег вопроса в Викторине
  // как для СЕО или нового пользователя, но через страницу Все вопросы
  const locationState = useMemo(
    () => ({
      isUniqQuestion: location.state?.isUniqQuestion ?? false,
      questionId: location.state?.questionId ?? 1,
      currentId: location.state?.currentId ?? null,
    }),
    [location.state],
  );

  const { day, plan_id } = useParams();
  const isPlanQuiz = Boolean(plan_id && day);

  const userPlanId = Object.keys(userPlans)?.[0];

  useEffect(() => {
    if (!userPlanId && day && plan_id) {
      getUserReadingPlan();
    }
  }, [day, plan_id]);

  const [
    {
      statuses,
      isCorrectMsg,
      message,
      correctMessage,
      isLoadingAnswer,
      isFinish,
      currentQuestionId,
    },
    dispatch,
  ] = useReducer(reducer, {
    ...questionInitialState,
    currentQuestionId: locationState.questionId,
  });
  const questionTitleRef = useRef(null);
  const questionsRef = useRef(null);

  const { questionsData } = useSelector(state => ({
    questionsData: state.quiz?.queezeData?.questions || [],
  }));

  const { questionId } = locationState;

  useEffect(() => {
    if (!questionsData.length) {
      return;
    }

    const currentQuestion = questionsData[questionId - 1];
    const { submittedAnswerId } = currentQuestion;

    if (!submittedAnswerId) {
      return;
    }

    const { answers } = currentQuestion;
    const currentMessage = answers.find(({ id }) => id === +submittedAnswerId)?.text;

    dispatch({
      type: types.MESSAGE,
      message: currentMessage,
    });
  }, [questionId, questionsData]);

  useEffect(() => {
    if (locationState.isUniqQuestion) {
      dispatch({
        type: types.UPDATE_ALL,
        completed: {
          ...questionInitialState,
          currentQuestionId: locationState.questionId,
        },
      });
    }
  }, [locationState.isUniqQuestion, locationState.questionId]);

  const scrollToQuizContainer = useCallback(
    yPositions => {
      let newYPosition = isTablet ? yPositions.tablet : yPositions.default;

      if (questionTitleRef.current) {
        newYPosition = questionTitleRef.current.offsetTop + 50;
      }

      smoothScrollTo(null, { top: newYPosition, behavior: 'smooth' });
    },
    [isTablet],
  );

  // Did mount
  useEffect(() => {
    // For SSR
    if (!questions.length || locationState.isUniqQuestion) {
      const pattern = /day-\d+\/\d+\//;
      const currentQuestionId = location.pathname
        .replace('/bibleyskaya-viktorina/voprosi/', '')
        .replace(pattern, '')
        .replace('/', '');
      getCurrentQuestion(currentQuestionId);
    }

    // Максимальное колличество вопросов - 10
    // Если пользователь решит выбрать несуществующий вопрос
    if (locationState.questionId > sizeOfQuiz) {
      push('/bibleyskaya-viktorina/resultati/');
    }
  }, [locationState.isUniqQuestion]);

  useEffect(() => {
    const { initNewQuestion } = yPositionQuizContainer;

    scrollToQuizContainer(initNewQuestion);
  }, [scrollToQuizContainer]);

  // Сценарий: пользователь ответил на 1 вопрос, открыл 2. Решил вернуться назад через браузер,
  // то необходимо проверить статус 1 вопроса и учесть вариант, что вопрос зарег у пользователя
  useEffect(() => {
    const status = statuses[locationState.questionId - 1];

    if (
      statuses.length &&
      !status.includes(statusesTypes.NA) &&
      isCorrectMsg === null &&
      !locationState.isUniqQuestion
    ) {
      // Ставим дефолт задержку на обработку состояний при клике назад в браузере
      setTimeout(() => dispatch({ type: types.STATUS_MESSAGE, isCorrectMsg: status !== 'NO' }));
    }
  }, [statuses, locationState.questionId, isCorrectMsg, locationState.isUniqQuestion]);

  const getNextQuestionStep = value => {
    // Скрытие сообщения со статусом ответа, тк перешли на новый вопрос
    dispatch({
      type: types.NEXT_STEP,
      isCorrectMsg: null,
      message: null,
      currentQuestionId: value,
    });
  };

  // Update
  useEffect(() => {
    if (questionsStatus.length && !locationState.isUniqQuestion) {
      // Могут быть NA и другие статусы
      if (questionsStatus[locationState.questionId - 1].includes(statusesTypes.NO)) {
        dispatch({ type: types.STATUS_MESSAGE, isCorrectMsg: false });
      }
      if (questionsStatus[locationState.questionId - 1].includes(statusesTypes.OK)) {
        dispatch({ type: types.STATUS_MESSAGE, isCorrectMsg: true });
      }
    }

    if (questionsStatus.length && !statuses.length) {
      dispatch({ type: types.STATUSES_ARR, statuses: questionsStatus });
    }

    if (currentQuestionId !== locationState.questionId) {
      getNextQuestionStep(locationState.questionId);
    }

    if (currentQuestionId === questions.length && isCorrectMsg !== null && !isFinish) {
      dispatch({ type: types.IS_FINISH, isFinish: true });
    }
  }, [
    currentQuestionId,
    locationState.questionId,
    isCorrectMsg,
    questions,
    questionsStatus,
    statuses.length,
    isFinish,
  ]);

  const checkIsLastItem = (isCorrectMsg, currentQuestionId, questionsLength) =>
    isCorrectMsg !== null && currentQuestionId < questionsLength;

  const getIsLastItem = useMemo(
    () => checkIsLastItem(isCorrectMsg, currentQuestionId, questions.length),
    [isCorrectMsg, currentQuestionId, questions.length],
  );

  const getUpdateCorrectMsg = (isCorrect, message, questionId, answerId, correctText) => {
    dispatch({ type: types.IS_LOADING_ANSWER, isLoadingAnswer: true });

    const completed = {
      isCorrectMsg: isCorrect,
      message,
      correctMessage: correctText,
      isLoadingAnswer: false,
    };

    if (isLoadedCurrentQuestion && (!questions.length || locationState.isUniqQuestion)) {
      dispatch({
        type: types.UPDATE_ALL,
        completed,
      });
    } else {
      getAnswer(questionId, answerId).then(({ queeze: { questions } }) => {
        completed.statuses = questions.map(question => question.status);

        dispatch({
          type: types.UPDATE_ALL,
          completed,
        });
      });
    }

    const { afterUpdateQuiz } = yPositionQuizContainer;
    scrollToQuizContainer(afterUpdateQuiz);
  };

  const getNewQuestion = useCallback(() => {
    const { initNewQuestion } = yPositionQuizContainer;
    scrollToQuizContainer(initNewQuestion);
    getNextQuestionStep(currentQuestionId + 1);
  }, [scrollToQuizContainer, currentQuestionId]);

  const getCurrentTitle = useCallback(
    (quizText, isDesc = false) => {
      let questionTitle = '';

      if (isLoadedCurrentQuestion) {
        questionTitle = R.pathOr('', ['question', 'title'], questionData);
      } else {
        questionTitle = R.pathOr('', [[currentQuestionId - 1], 'title'], questions);
      }

      let questionTitleSubMsg = isDesc ? DESC_QUIZ : TITLE_QUIZ;

      if (questionTitle.length) {
        if (isDesc) {
          questionTitleSubMsg = `${quizText} ${questionTitle} `;
        } else {
          questionTitleSubMsg = `${questionTitle} ${quizText}`;
        }
      }

      return questionTitleSubMsg;
    },
    [questions, currentQuestionId],
  );

  const questionRouteParams = useMemo(() => {
    if (sizeOfQuiz > locationState.questionId && questions.length && !isPlanQuiz) {
      return getQuestionParams(questions, locationState.questionId);
    }
    return getQuestionParams(
      questions,
      locationState.questionId,
      undefined,
      `day-${day}/${plan_id}/`,
    );
  }, [locationState.questionId, questions]);

  const uniqQuestion = useMemo(() => {
    let actualQuestion = R.pathOr({}, ['question'], questionData);

    // Сценарий если пользователь открывает вопрос из зарег. и активной Викторины,
    // но со страницы Все вопросы, где не учитывается зарег Викторина и надо показать как для нового юзера
    if (locationState.isUniqQuestion && !Object.keys(actualQuestion).length) {
      const currentQuestions = R.pathOr([], ['current', 'questions'], questionData);
      actualQuestion = currentQuestions.find(({ id }) => id === locationState.currentId) || {};
    }

    return actualQuestion;
  }, [locationState.isUniqQuestion, location.state, questionData]);

  // Сценарий, когда пользователь вбил несуществующий вопрос
  // Убрал так как существующие вопросы тоже попдают под 404
  if (errorQuestion) {
    push('/404/');
  }

  return (
    <QuizQuestionsContext.Provider value={[getUpdateCorrectMsg]}>
      <Helmet>
        <title> {getCurrentTitle('- Библейская викторина Экзегета', false)} </title>
        <meta
          name="description"
          content={getCurrentTitle(`Библейская викторина Экзегета: `, true)}
        />
      </Helmet>
      {/* Если пользователь (или робот) попал на страницу вопроса, при этом у него нет активной викторины */}
      {/* То показываем этот обрубок, а isLoadedCurrentQuestion стоит выше прелоадера, тк SSR */}
      {isLoadedCurrentQuestion && (!questions.length || locationState.isUniqQuestion) ? (
        <QuizQuestionForNewUser
          questionTitleRef={questionTitleRef}
          questionsRef={questionsRef}
          mainTitle={uniqQuestion.title}
          isLoadingAnswer={isLoadingAnswer}
          isCorrectMsg={isCorrectMsg}
          message={message}
          question={uniqQuestion}
          correctMessage={correctMessage}
          currentId={locationState.currentId}
          isSafari={isSafari}
        />
      ) : isLoading ? (
        <Preloader />
      ) : !questions.length ? (
        <p className="quiz__not-found"> Вопросы не найдены или вы все прошли </p>
      ) : (
        <>
          <div
            className={cx('quiz-questions__header', {
              'quiz-testament__header-safari': isSafari,
            })}>
            <QuizQuestionBibleTitle questionRef={questionTitleRef} />
            {/* 1 условие, что вопрос неию ТЗ, второе, чт больше 10 по услово у вопроса есть ли ответы */}
            {/* Следующий вопрос возможен: если макс вопрос < 10, вопросы отсутствуют или верного ответа нет */}
            {/* временно закомменчу для выката на стейдж */}
            <div>
              <AudioPlayer
                className={'quiz-audio-player'}
                forQuiz
                autoPlay={isAutoplay}
                forQuizData={{ isCorrectMsg, message, currentQuestionId }}
                src={''}
                title={''}
              />
              <AudioCheckbox isAutoplay={isAutoplay} setAutoplay={setAutoplay} />
            </div>
            {currentQuestionId < sizeOfQuiz && isCorrectMsg !== null && (
              <div className="quiz-link__wrapper">
                <QuizLink
                  text="Следующий вопрос"
                  getNewQuestion={getNewQuestion}
                  path={questionRouteParams}
                />
              </div>
            )}
            {isFinish && (
              <div className="quiz-link__wrapper quiz-link__wrapper-to-results">
                <QuizLink
                  className="quiz-link__to-results btn quiz-ui__link"
                  text="Посмотреть результаты"
                  path={
                    isPlanQuiz
                      ? `/group/reading/${userPlanId}/day-${day}/result/?plan_id=${plan_id}`
                      : '/bibleyskaya-viktorina/resultati/'
                  }
                />
              </div>
            )}
          </div>
          <div className="quiz-questions quiz-testaments" ref={questionsRef}>
            <QuizQuestionMain
              title={questions[currentQuestionId - 1].title}
              isLoadingAnswer={isLoadingAnswer}
              isCorrectMsg={isCorrectMsg}
              message={message}
              question={questions[currentQuestionId - 1]}
              correctMessage={correctMessage}
            />
            <QuizNumberQuestion
              href={
                isPlanQuiz
                  ? `/group/reading/${userPlanId}/day-${day}/result/?plan_id=${plan_id}`
                  : '/bibleyskaya-viktorina/resultati/'
              }
              questionsRef={questionsRef}
              getIsLastItem={getIsLastItem}
              questions={questions.length}
              isFinish={isFinish}
              currentQuestionId={currentQuestionId}
              questionsStatuses={statuses}
              push={push}
              getNewQuestion={getNewQuestion}
              isCorrectMsg={isCorrectMsg}
              questionRouteParams={questionRouteParams}
            />
          </div>
        </>
      )}
    </QuizQuestionsContext.Provider>
  );
};

QuizQuestions.defaultProps = {
  questions: [],
  isLoading: false,
};

QuizQuestions.propTypes = {
  questions: PropTypes.array,
  isLoading: PropTypes.bool,
  history: PropTypes.object,
  questionsStatus: PropTypes.object,
  isTablet: PropTypes.bool,
  questionData: PropTypes.shape({
    question: PropTypes.shape({
      title: PropTypes.string,
    }),
  }),
  getCurrentQuestion: PropTypes.func,
  isLoadedCurrentQuestion: PropTypes.bool,
};

export default connect(mapStateToProps, mapDispatchToProps)(QuizQuestions);
