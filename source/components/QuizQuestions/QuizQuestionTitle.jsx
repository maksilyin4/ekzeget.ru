import React from 'react';
import PropTypes from 'prop-types';

const QuizQuestionTitle = ({ title = '' }) => <h1 className="quiz-questions__title">{title}</h1>;

QuizQuestionTitle.propTypes = {
  title: PropTypes.string,
};

export default QuizQuestionTitle;
