import React, {memo, useState} from 'react';
import PropTypes from 'prop-types';
import Icon from "~components/Icons/Icons";
import cx from "classnames";

const QuizQuestionInterpretation = ({interpretation}) => {
    const [isOpen, setIsOpen] = useState(false);

    return (
        <>
            <div className="quiz-answer-interpretations__item">
                <button
                    onClick={() => setIsOpen(!isOpen)}
                    className={cx('nav-side__link quiz-answer-interpretations__toggle', {
                        active: isOpen,
                    })}
                >
                    {interpretation.ekzeget.name}
                    <Icon icon='angle-down'/>
                </button>

            </div>

            {isOpen && <div className="comment__text" dangerouslySetInnerHTML={{__html: interpretation.parsed_text}}/>}
        </>

    );
};

QuizQuestionInterpretation.propTypes = {
    interpretation: PropTypes.object,
};

export default memo(QuizQuestionInterpretation);
