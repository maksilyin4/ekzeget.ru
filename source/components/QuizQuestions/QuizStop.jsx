import React from 'react';
import PropTypes from 'prop-types';

import { completeQuiz } from '../../dist/quizAPI';

const QuizStop = ({ push }) => {
  const completeQueeze = () => {
    completeQuiz(push, '/bibleyskaya-viktorina/');
  };

  return (
    <div className="quiz-again__container">
      <button className="quiz-question__quit-link grey btn" onClick={completeQueeze}>
        Завершить викторину
      </button>
    </div>
  );
};

QuizStop.propTypes = {
  push: PropTypes.func,
};

export default QuizStop;
