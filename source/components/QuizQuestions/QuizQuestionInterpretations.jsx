import React, {memo} from 'react';
import PropTypes from 'prop-types';
import QuizQuestionInterpretation from "~components/QuizQuestions/QuizQuestionInterpretation";
import {Link} from "react-router-dom";

const QuizQuestionInterpretations = ({interpretations, verse}) => {
    if (!verse.book_code && verse.book) {
        verse.book_code = verse.book.code;
    }
    if (!verse.short && verse.book) {
        verse.short = verse.book.short_title + '. ' + verse.chapter + ':' + verse.number;
    }

    // todo: gross, that's what named routes exist for
    const verseUrl = `/bible/${verse.book_code}/glava-${verse.chapter}/stih-${verse.number}/`;
    return (
        <div className="quiz-correct__comment comment">
            <p className="verse-text" dangerouslySetInnerHTML={{__html: verse.text}}/>
            <span>
                <Link className="content__group-link" to={verseUrl}>
                    {verse.short}
                </Link>
            </span>
            <span className="comment__title"> Подробный комментарий: </span>
            <div className="quiz-answer-interpretations">
                {interpretations.map(interpretation => <QuizQuestionInterpretation interpretation={interpretation}
                                                                                   key={interpretation.id}/>)}
            </div>
        </div>
    )
};

QuizQuestionInterpretations.propTypes = {
    interpretations: PropTypes.array,
    verse: PropTypes.object,
};

export default memo(QuizQuestionInterpretations);
