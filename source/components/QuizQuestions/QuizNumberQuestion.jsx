import React from 'react';
import PropTypes from 'prop-types';

import QuizLink from '../QuizUI/QuizLink';
import QuizStop from './QuizStop';
import QuizNumber from '../QuizUI/QuizNumber/';

const QuizNumberQuestion = ({
  questions,
  href,
  currentQuestionId,
  questionsRef,
  getIsLastItem,
  isFinish,
  questionsStatuses,
  push,
  getNewQuestion,
  isCorrectMsg,
  questionRouteParams,
}) => (
  <>
    {questionsRef.current && (
      <div className="quiz-questions__count">
        <ul className="quiz-questions quiz-questions-not-mobile">
          {[...Array(questions).keys()].map(questionIndex => (
            <QuizNumber
              key={questionIndex}
              status={questionsStatuses[questionIndex]}
              questionIndex={questionIndex}
              currentQuestionId={currentQuestionId}
              isCorrectMsg={isCorrectMsg}
            />
          ))}
        </ul>
        <ul className="quiz-questions quiz-questions-mobile">
          {[...Array(questions).keys()].slice(0, questions / 2).map(questionIndex => (
            <QuizNumber
              key={questionIndex}
              status={questionsStatuses[questionIndex]}
              questionIndex={questionIndex}
              currentQuestionId={currentQuestionId}
              isCorrectMsg={isCorrectMsg}
            />
          ))}
        </ul>
        <ul className="quiz-questions quiz-questions-mobile">
          {[...Array(questions).keys()].slice(questions / 2).map(questionIndex => (
            <QuizNumber
              key={questionIndex}
              status={questionsStatuses[questionIndex]}
              questionIndex={questionIndex}
              currentQuestionId={currentQuestionId}
              isCorrectMsg={isCorrectMsg}
            />
          ))}
        </ul>
      </div>
    )}
    {getIsLastItem && (
      <div className="quiz-link__wrapper">
        <QuizLink
          getNewQuestion={getNewQuestion}
          text="Следующий вопрос"
          path={questionRouteParams}
        />
        <QuizStop push={push} />
      </div>
    )}
    {isFinish && (
      <div className="quiz-link__wrapper quiz-link__wrapper-to-results">
        <QuizLink
          className="quiz-link__to-results btn quiz-ui__link"
          text="Посмотреть результаты"
          path={href}
        />
      </div>
    )}
  </>
);

QuizNumberQuestion.propTypes = {
  questions: PropTypes.array,
  currentQuestionId: PropTypes.number,
  questionsRef: PropTypes.shape({
    current: PropTypes.oneOfType([null, PropTypes.func]),
  }),
  getIsLastItem: PropTypes.func,
  isFinish: PropTypes.bool,
  questionsStatuses: PropTypes.array,
  push: PropTypes.func,
  getNewQuestion: PropTypes.func,
  isCorrectMsg: PropTypes.bool,
  questionRouteParams: PropTypes.shape({
    pathname: PropTypes.string,
    state: PropTypes.shape({
      questionId: PropTypes.number,
      questionTitle: PropTypes.string,
    }),
  }),
};

export default QuizNumberQuestion;
