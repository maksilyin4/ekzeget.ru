import React, { memo } from 'react';
import PropTypes from 'prop-types';

import QuizQuestionTitle from './QuizQuestionTitle';
import Preloader from '../Preloader/Preloader';
import QuizQuestionBody from './QuizQuestionBody';

const QuizQuestionMain = ({
  title,
  isLoadingAnswer,
  isCorrectMsg,
  message,
  question,
  correctMessage,
}) => (
  <>
    <QuizQuestionTitle title={title} />
    {isLoadingAnswer && <Preloader className="quiz-questions__preloader" />}
    <QuizQuestionBody
      isCorrectMsg={isCorrectMsg}
      message={message}
      question={question}
      correctMessage={correctMessage}
    />
  </>
);

QuizQuestionMain.propTypes = {
  title: PropTypes.string,
  isLoadingAnswer: PropTypes.bool,
  isCorrectMsg: PropTypes.oneOfType([null, PropTypes.bool]),
  message: PropTypes.object,
  correctMessage: PropTypes.string,
  question: PropTypes.object,
};

export default memo(QuizQuestionMain);
