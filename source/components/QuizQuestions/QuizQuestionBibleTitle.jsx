import React, { memo } from 'react';
import PropTypes from 'prop-types';

const QuizQuestionBibleTitle = ({ questionRef }) => (
  <div className="page__head page__head_two-col quiz-title">
    <span className="page__title" ref={questionRef}>
      Библейская викторина
    </span>
  </div>
);

QuizQuestionBibleTitle.propTypes = {
  questionRef: PropTypes.shape({
    current: PropTypes.objectOf(PropTypes.element),
  }),
};

export default memo(QuizQuestionBibleTitle);
