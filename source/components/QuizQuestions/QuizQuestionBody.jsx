import React from 'react';
import PropTypes from 'prop-types';

import QuizQuestion from './QuizQuestion';
import QuizCorrect from './QuizCorrect';
import QuizWrong from './QuizWrong';

const QuizQuestionBody = ({ isCorrectMsg, question, message, correctMessage }) =>
  isCorrectMsg === null ? (
    question.answers.map(({ id, is_correct, text }, index) => (
      <QuizQuestion
        key={id}
        questionId={question.id}
        answerId={id}
        value={index}
        isCorrect={is_correct}
        text={text}
        answers={question.answers}
      />
    ))
  ) : (
    <>
      {!isCorrectMsg && <QuizWrong message={message} />}
      <QuizCorrect
        isCorrectMsg={isCorrectMsg}
        description={question.description}
        interpretations={question.interpretations}
        verse={question.verse}
        correctMessage={correctMessage}
      />
    </>
  );

QuizQuestionBody.propTypes = {
  isCorrectMsg: PropTypes.bool,
  question: PropTypes.shape({
    id: PropTypes.number,
    is_correct: PropTypes.bool,
    text: PropTypes.string,
    answers: PropTypes.array,
    interpretations: PropTypes.array,
    verse: PropTypes.object,
  }),
  message: PropTypes.string,
  correctMessage: PropTypes.string,
};

export default QuizQuestionBody;
