import React, { useEffect, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Helmet from 'react-helmet';

import { getQuestions } from '../../dist/actions/Quiz';
import {
  getIsQuestionsLoaded,
  getAllQuestions,
  getTotalQuestions,
  errorAllQuestions,
} from '../../dist/selectors/Quiz';
import Preloader from '../Preloader/Preloader';
import Pagination from '../Pagination/Pagination';
import { DEFAULT_COUNT_QUESTIONS, DEFAULT_TEXT_QUIZ, smoothScrollTo } from '../../dist/utils';
import QuizLink from '../QuizUI/QuizLink';

import './styles.scss';

const QuizAllQuestions = ({
  match: {
    params: { id = '1' },
  },
  history: {
    push,
    location: { pathname },
  },
}) => {
  const dispatch = useDispatch();
  const questions = useSelector(getAllQuestions);
  const totalCount = useSelector(getTotalQuestions);
  const isLoaded = useSelector(getIsQuestionsLoaded);
  const { message } = useSelector(errorAllQuestions);

  useEffect(() => {
    dispatch(getQuestions(id));
    smoothScrollTo();
  }, [id]);

  const changeQuestionPage = useCallback(page => {
    push(`/bibleyskaya-viktorina/voprosi-${page}/`);
  }, []);

  return (
    <div className="quiz">
      <Helmet>
        <title> {`${DEFAULT_TEXT_QUIZ} - все вопросы викторины (страница ${id})`} </title>
        <meta
          name="description"
          content={`${DEFAULT_TEXT_QUIZ} - смотреть все вопросы викторины (страница ${id})`}
        />
      </Helmet>
      <div className="quiz__link-container">
        <QuizLink
          text="Начать викторину"
          className="quiz-testaments__to-quiz red btn"
          path="/bibleyskaya-viktorina/"
          isIcon={false}
        />
      </div>
      {isLoaded ? (
        !questions && message ? (
          <span className="quiz__not-found"> Страница не найдена</span>
        ) : (
          <ul className="quiz__questions">
            {questions.map(({ id, title, code }) => (
              <li key={id} className="quiz__question">
                <Link
                  to={{
                    pathname: `/bibleyskaya-viktorina/voprosi/${code}/`,
                    state: {
                      questionTitle: title,
                      questionId: 1,
                      isUniqQuestion: true,
                      currentId: id,
                      prevUrlAllQuestions: pathname,
                    },
                  }}
                  className="quiz__question-link">
                  {title}
                  <div className="quiz__arrow-wrapper">
                    <span className="quiz__arrows">
                      <span className="quiz__arrow" />
                      <span className="quiz__arrow" />
                    </span>
                  </div>
                </Link>
              </li>
            ))}
          </ul>
        )
      ) : (
        <Preloader />
      )}
      {totalCount > DEFAULT_COUNT_QUESTIONS && (
        <Pagination
          page={+id}
          count={DEFAULT_COUNT_QUESTIONS}
          totalCount={totalCount}
          onClick={changeQuestionPage}
        />
      )}
    </div>
  );
};

QuizAllQuestions.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
      url: PropTypes.string,
    }),
  }),
  history: PropTypes.shape({
    push: PropTypes.func,
    location: PropTypes.shape({
      pathname: PropTypes.string,
    }),
  }),
};

export default QuizAllQuestions;
