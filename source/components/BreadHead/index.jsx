import React from 'react';
import PropTypes from 'prop-types';

import LearnVideo from '../LearnVideo/LearnVideo';
import Breadcrumbs from '../Breadcrumbs/Breadcrumbs';

import './breadHead.scss';

const BreadHead = ({
  bread = [],
  query = 'bible_tutorial_video_url',
  isBread = false,
  isDisabledLearnVideo,
}) => (
  <div className="bread-head">
    <Breadcrumbs data={bread} isBread={isBread} />
    {!isDisabledLearnVideo && <LearnVideo query={query} />}
  </div>
);

BreadHead.propTypes = {
  bread: PropTypes.array,
  query: PropTypes.string,
  isBread: PropTypes.bool,
};

export default BreadHead;
