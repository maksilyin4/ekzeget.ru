import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Link, NavLink } from 'react-router-dom';
import Icon from '../Icons/Icons';

import './style.scss';

const getComponent = type => {
  let Component;
  switch (type) {
    case 'NavLink':
      Component = NavLink;
      break;

    case 'Link':
      Component = Link;
      break;

    default:
      Component = 'button';
      break;
  }
  return Component;
};

const Button = ({ type = 'button', mod, className, title, icon, subTitle, ...props }) => {
  const mods = Array.isArray(mod) ? mod.map(mod => ` btn_${mod}`) : `btn_${mod}`;

  const Component = getComponent(type);

  return (
    <Component className={cx('btn', mods, className)} {...props}>
      {title && (
        <p>
          {title} {subTitle && <span className="btn-subTitle">{subTitle}</span>}
        </p>
      )}

      {icon && <Icon icon={icon} />}
    </Component>
  );
};

Button.propTypes = {
  type: {
    type: PropTypes.oneOf(['Link', 'NavLink', 'button']),
    defaultValue: 'button',
  },
  mod: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  className: PropTypes.string,
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  icon: PropTypes.string,
};

export default Button;
