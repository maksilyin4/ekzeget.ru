import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import './pullDown.scss';

class Pulldown extends Component {
  static propTypes = {
    isOpen: PropTypes.bool,
    title: PropTypes.string,
    actions: PropTypes.object,
    content: PropTypes.object,
    // position: PropTypes.string,
  };

  constructor(props) {
    super(props);

    this.state = {
      isOpen: props.isOpen,
    };
  }

  componentDidMount() {}

  componentWillReceiveProps(nextProps) {
    if (nextProps.isOpen !== this.props.isOpen) this.setState({ isOpen: nextProps.isOpen });
  }

  // _close = pulldown => {
  // document.addEventListener('click', (e) => {
  //     let isChild = pulldown.childNodes.forEach((el) => el === e.target);
  //     if (e.target !== pulldown && !isChild) {
  //         this.setState({isOpen: false})
  //     }
  // })
  // };

  render() {
    const { isOpen } = this.state;
    const { title, actions, content, className } = this.props;
    return (
      <div className={cx('pulldown', className, { active: isOpen })}>
        <div className="pulldown__head">
          <div className="pulldown__title">{title}</div>
          {actions && <div className="pulldown__actions">{actions}</div>}
        </div>
        <div className="pulldown__body">{content}</div>
      </div>
    );
  }
}

export default Pulldown;
