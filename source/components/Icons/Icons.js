import React, { memo } from 'react';
import cx from 'classnames';

import renderIcon from './renderIcon';

import './icons.scss';

const Icon = props => {
  const { icon, type, ...restProps } = props;
  const data = renderIcon(icon);
  return (
    <span className={cx('icon', { [`icon_${type}`]: type })} {...restProps}>
      <svg
        className="svg-inline"
        role="img"
        xmlns="http://www.w3.org/2000/svg"
        viewBox={data.viewBox}>
        {data.code}
      </svg>
    </span>
  );
};

export default memo(Icon);
