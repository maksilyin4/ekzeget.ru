import React from 'react';

export const skipNext = ({ color, ...props } = {}) => {
  return (
    <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <title>{'Play, Next'}</title>
      <g fill="none">
        <path d="M0 0h24v24H0z" />
        <path
          stroke="#000"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth={1.5}
          d="M18 6.75v10.5M12.925 11.121L6.586 7.159A1.037 1.037 0 005 8.038v7.923c0 .814.896 1.311 1.586.879l6.338-3.962a1.035 1.035 0 00.001-1.757z"
        />
      </g>
    </svg>
  );
};

export const rewindForward = ({ color, ...props } = {}) => {
  return (
    <svg
      width={24}
      height={24}
      {...props}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <path
        d="M6.776 8.02v1.512h1.236v6.467h1.692V8.02H6.776zM13.309 10.816h-.804l.132-1.26H15.6V8.02h-4.32l-.444 4.032.168.228h2.232c.708 0 1.188.48 1.188 1.152 0 .647-.456 1.115-1.068 1.115-.612 0-1.056-.444-1.068-1.056H10.56c.012 1.537 1.188 2.652 2.796 2.652 1.596 0 2.796-1.152 2.796-2.712 0-1.511-1.176-2.615-2.844-2.615z"
        fill="#000"
      />
      <path
        d="M20.424 7.698H17.53h2.893V4.805M21 12.002a9 9 0 11-1.095-4.304"
        stroke="#000"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export const rewindBack = ({ color, ...props } = {}) => {
  return (
    <svg
      width={24}
      height={24}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}>
      <path
        d="M7.776 8.02v1.512h1.236v6.467h1.692V8.02H7.776zM14.309 10.816h-.804l.132-1.26H16.6V8.02h-4.32l-.444 4.032.168.228h2.232c.708 0 1.188.48 1.188 1.152 0 .647-.456 1.115-1.068 1.115-.612 0-1.056-.444-1.068-1.056H11.56c.012 1.537 1.188 2.652 2.796 2.652 1.596 0 2.796-1.152 2.796-2.712 0-1.511-1.176-2.615-2.844-2.615z"
        fill="#000"
      />
      <path
        d="M3.576 7.698H6.47 3.576V4.805M3 12.002a9 9 0 101.095-4.304"
        stroke="#000"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

// настоящая кнопка Стоп (сохраню код на случай, если Андрей Викторович передумает)
export const stop = ({ color, ...props } = {}) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      // enable-background="new 0 0 24 24"
      height="40px"
      viewBox="0 0 24 24"
      width="40px"
      {...props}
      // fill="currentColor">
      fill="#9e9e9e">
      <g>
        <rect fill="none" height="24" width="24" />
      </g>
      <g>
        <g>
          <rect height="8" width="8" x="8" y="8" />
          <path d="M12,2C6.48,2,2,6.48,2,12s4.48,10,10,10s10-4.48,10-10S17.52,2,12,2z M12,20c-4.41,0-8-3.59-8-8c0-4.41,3.59-8,8-8 s8,3.59,8,8C20,16.41,16.41,20,12,20z" />
        </g>
      </g>
    </svg>
  );
};

export const replayAsStopIcon = ({ color, ...props } = {}) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      height="30px"
      viewBox="0 0 24 24"
      width="30px"
      {...props}
      fill="#000">
      <g>
        <path d="M12 5V1L7 6l5 5V7c3.31 0 6 2.69 6 6s-2.69 6-6 6-6-2.69-6-6H4c0 4.42 3.58 8 8 8s8-3.58 8-8-3.58-8-8-8z" />
      </g>
    </svg>
  );
};

export const muteOffForMobile = ({ color, ...props } = {}) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      height="30px"
      viewBox="0 0 24 24"
      width="30px"
      {...props}
      fill="#000">
      <g>
        <path d="M3 9v6h4l5 5V4L7 9H3zm13.5 3c0-1.77-1.02-3.29-2.5-4.03v8.05c1.48-.73 2.5-2.25 2.5-4.02zM14 3.23v2.06c2.89.86 5 3.54 5 6.71s-2.11 5.85-5 6.71v2.06c4.01-.91 7-4.49 7-8.77s-2.99-7.86-7-8.77z" />
      </g>
    </svg>
  );
};

export const muteOnForMobile = ({ color, ...props } = {}) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      height="30px"
      viewBox="0 0 24 24"
      width="30px"
      {...props}
      fill="#000">
      <g>
        <path d="M16.5 12c0-1.77-1.02-3.29-2.5-4.03v2.21l2.45 2.45c.03-.2.05-.41.05-.63zm2.5 0c0 .94-.2 1.82-.54 2.64l1.51 1.51C20.63 14.91 21 13.5 21 12c0-4.28-2.99-7.86-7-8.77v2.06c2.89.86 5 3.54 5 6.71zM4.27 3L3 4.27 7.73 9H3v6h4l5 5v-6.73l4.25 4.25c-.67.52-1.42.93-2.25 1.18v2.06c1.38-.31 2.63-.95 3.69-1.81L19.73 21 21 19.73l-9-9L4.27 3zM12 4L9.91 6.09 12 8.18V4z" />
      </g>
    </svg>
  );
};
