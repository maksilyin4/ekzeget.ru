export const social = [
  {
    icon: 'vk',
    arg: ['https://vk.com/ekzeget', 'social_vk'],
  },
  {
    icon: 'facebook',
    arg: ['https://www.facebook.com/ekzeget.ru/', 'social_fb'],
  },
  {
    icon: 'twitter',
    arg: ['https://twitter.com/EkzegetRU', 'social_tw'],
  },
  {
    icon: 'ok-social',
    arg: ['https://ok.ru/ekzegetru', 'social_ok'],
  },
  {
    icon: 'instagram',
    arg: ['https://www.instagram.com/ekzegetru/', 'social_ig'],
  },
  {
    icon: 'youtube',
    arg: ['https://www.youtube.com/channel/UCymsjx24eU3kRFUjeBsh47A', 'social_yt'],
  },
  {
    icon: 'yandex-dzen',
    arg: ['https://zen.yandex.ru/id/5b683072995e0f00add1402e', 'social_yz'],
  },
  {
    icon: 'tiktok',
    arg: ['https://www.tiktok.com/@ekzeget.ru', 'social_tt'],
  },
];
