import React, { useCallback } from 'react';

import { sendSocialActions } from '../../dist/utils';
import Icon from '../Icons/Icons';
import { social } from './utilsSocial';
import './social.scss';
import HTMLCommentRender from '~components/HtmlComm/HtmlComm';

const Social = ({ title }) => {
  const handleClick = useCallback(
    (url, socialName) => () => {
      sendSocialActions({
        ga: [
          'event',
          'event_name',
          { event_category: 'social', event_action: 'perehod', event_label: socialName },
        ],
        ym: socialName,
      });
      window.open(url, '_blank');
    },
    [],
  );

  return (
    <div className="social">
      {title && <div className="social__title">{title}</div>}
      <div className="social__links">
        {social.map(({ icon, arg }) => (
          <button
            key={icon}
            onClick={handleClick(...arg)}
            className="social__links-item"
            rel="nofollow noopener noreferrer"
            title={icon}>
            <Icon icon={icon} className="social__links-icon" />
          </button>
        ))}
        <div>
        <HTMLCommentRender comment="googleoff: all" />
        <noindex>
          <a
            href="https://t.me/ekzeget"
            className="social__links-item"
            target="_blank"
            title="Телеграм"
            rel="nofollow noopener noreferrer">
            <Icon icon="telegram" className="social__links-icon" />
          </a>
        </noindex>
        <HTMLCommentRender comment="/googleoff: all" />
        </div>
      </div>
    </div>
  );
};

export default Social;
