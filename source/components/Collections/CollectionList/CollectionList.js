import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import cx from 'classnames';
import './collectionList.scss';

class CollectionList extends Component {
  // static propTypes = {
  //   mod: PropTypes.string,
  // };

  render() {
    // const { items, mod } = this.props;
    return (
      <div className="collection">
        <div className="collection__list">{this.props.children}</div>
      </div>
    );
  }
}

export class CollectionItem extends Component {
  static propTypes = {
    title: PropTypes.any,
    href: PropTypes.string,
  };

  render() {
    const { title, href, mod, onClick } = this.props;
    return (
      <div
        className={cx('collection__list-item', {
          'collection__list-item_underline': mod === 'underline',
        })}>
        <Link
          to={href}
          className={cx('collection__list-link', {
            'collection__list-link_underline': mod === 'underline',
          })}
          onClick={onClick}
          title={String(title)}>
          {title}
        </Link>
      </div>
    );
  }
}

export default CollectionList;
