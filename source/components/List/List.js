/* eslint-disable max-classes-per-file */
// поправлено в другой таске
import React, { Component, PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Link } from 'react-router-dom';

import Icon from '../Icons/Icons';
import './list.scss';

class List extends Component {
  static propTypes = {
    className: PropTypes.string,
  };

  render() {
    const { className, style = {} } = this.props;
    return (
      <div style={style} className={cx('list', className)} ref={this.props.setListRef}>
        {this.props.children}
      </div>
    );
  }
}

export class ListItem extends PureComponent {
  static propTypes = {
    title: PropTypes.any,
    href: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func,
    state: PropTypes.object,
    linkClassName: PropTypes.string,
  };

  render() {
    const {
      title,
      href,
      state = {},
      actions,
      className,
      onClick = f => f,
      children,
      isFav,
      onMouseEnter,
      onMouseLeave,
      themePreaching,
      toRef,
      linkClassName = '',
      active = false,
    } = this.props;

    return (
      <div
        className={cx('list__item', className)}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
        ref={toRef}>
        {href ? (
          <Link
            to={{
              pathname: href,
              state: { ...state },
            }}
            className={cx('list__item-link', linkClassName, { active })}
            onClick={onClick}>
            {themePreaching ? (
              <div className="list__item-link_preaching">
                <div className="preaching-author">{title}</div>
                <p>{themePreaching}</p>
              </div>
            ) : (
              title
            )}
            {isFav && <Icon icon="favorites" type="list__item-fav" />}
          </Link>
        ) : (
          <span className={cx('list__item-link', linkClassName, { active })} onClick={onClick}>
            {title}
          </span>
        )}
        {actions && <div className="list__item-actions">{actions}</div>}
        {children}
      </div>
    );
  }
}

export default List;
