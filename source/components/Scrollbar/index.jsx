import React from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import './styles.scss';

const Scrollbar = props => {
  const { children, autoHide = true, renderView, toRef } = props;

  return (
    <Scrollbars
      ref={toRef}
      style={{ height: '100%', overflow: 'hidden', overflowX: 'hidden', overflowY: 'scroll' }}
      autoHide={autoHide}
      autoHeight
      autoHeightMin={10}
      autoHeightMax="100%"
      thumbMinSize={100}
      renderTrackVertical={props => <div {...props} className="track-vertical" />}
      renderTrackHorizontal={props => <div {...props} className="track-horizontal" />}
      renderView={renderView}
      universal
      hideTracksWhenNotNeeded>
      {children}
    </Scrollbars>
  );
};

export default Scrollbar;
