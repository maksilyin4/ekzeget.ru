import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import Handlebars from 'handlebars';
import Helmet from 'react-helmet';

import './pageHead.scss';

const PageHead = ({
  meta = null,
  metaContext = null,
  h_one = '',
  //тест пока нет внедрения
  isClient=false,
  subTitle = '',
  isLoad,
  onClick,
  textBtn = 'У вас есть вопрос о библейских группах?',
}) => {
  const { metaData, metaContextData } = useSelector(state => {
    const metaSelector = state.metaData;
    return {
      metaData: metaSelector?.meta || {},
      metaContextData: metaSelector?.metaContext,
    };
  });

  return (
    <div className="page_head page_head_two-col">
      <Helmet>
        <title>{isClient ? h_one : `${Handlebars.compile(meta?.title || metaData?.title || h_one || '')(
          metaContext || metaContextData,
        )}`}</title>
      </Helmet>

      <div className="page-head-container">
        {isLoad ? null : (
          <>
            <div className="page_content">
              <h1 className="page_title">
                {isClient ? h_one :`${Handlebars.compile(meta?.h_one || metaData?.h_one || h_one || '')(
                  metaContext || metaContextData,
                )}`}
              </h1>

              {subTitle.length > 0 && <p className="inner-page__subtitle">{subTitle}</p>}
            </div>
            {onClick && (
              <button className="page-desc-link" onClick={onClick}>
                <p>{textBtn}</p>
              </button>
            )}
          </>
        )}
      </div>
    </div>
  );
};

PageHead.propTypes = {
  isLoad: PropTypes.bool,
  // h_one: PropTypes.string,
  subTitle: PropTypes.string,
  onClick: PropTypes.func,
};
export default PageHead;
