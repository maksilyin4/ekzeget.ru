import React, { useEffect, useRef, useState } from 'react';
import './styles.scss';
import Comment from './Comment';
import { connect } from 'react-redux';
import { getComments } from '~store/commentsPlan/selectors';
import {
  replyComment,
  createComment,
  deleteCommentThunk,
} from '../../../store/commentsPlan/actions';
import { wordEnding } from '~utils/wordEnding';
import InputComment from './InputComment';
import { useClickAway } from 'react-use';
import { userGroup } from '~store/groups/selectors';
import { userData } from '~dist/selectors/User';
import { getUser } from '~dist/actions/User';

function CommentsPlan({
  comments,
  replyComment,
  createComment,
  chosenDay,
  planId,
  getUser,
  user,
  groupId,
  userGroup,
  deleteCommentThunk,
}) {
  const openNext = 10;
  const [show, setShow] = useState(openNext);

  const [typed, setTyped] = useState('');
  const [typedAnswer, setTypedAnswer] = useState('');
  const [openInput, setOpenInput] = useState(-1);

  const handleSend = async () => {
    if (typed.trim()) {
      createComment(planId, groupId, typed).then(() => {
        setTyped('');
      });
    }
  };

  useEffect(() => {
    getUser();
  }, []);

  const length = comments?.length;
  const commentsName = wordEnding(length, ['комментарий', 'комментария', 'комментариев']);
  const ref = useRef();

  useClickAway(ref, () => {
    setOpenInput(-1);
  });

  return (
    <div className="comments__wrapper">
      <div className="comments__head">
        <div className="comments__head-title">
          {length} {commentsName} к дню {chosenDay}
        </div>
        {length > 20 && (
          <div onClick={() => setShow(length)} className="comments__head-button">
            Развернуть все {length} {commentsName}
          </div>
        )}
      </div>
      <div ref={ref} className="comments__inner">
        <InputComment handleSend={handleSend} onChange={v => setTyped(v)} typed={typed} />
        {comments.slice(0, show).map((comment, i) => (
          <Comment
            userId={user.user?.id}
            mentorId={userGroup?.mentor_id}
            planId={chosenDay}
            deleteComment={deleteCommentThunk}
            groupId={groupId}
            isOpenInput={openInput === i}
            onOpenInput={() => setOpenInput(i)}
            setTyped={setTypedAnswer}
            replyComment={replyComment}
            typed={typedAnswer}
            comment={comment}
          />
        ))}
      </div>
      {length > openNext && length > show && (
        <div className="open__next-wrapper">
          <div onClick={() => setShow(prev => prev + openNext)} className="open__next">
            Развернуть дальше
          </div>
        </div>
      )}
    </div>
  );
}

const mapStateToProps = state => ({
  comments: getComments(state),
  userGroup: userGroup(state),
  user: userData(state),
});

const mapDispatchToProps = {
  replyComment,
  createComment,
  getUser,
  deleteCommentThunk,
};

export default connect(mapStateToProps, mapDispatchToProps)(CommentsPlan);
