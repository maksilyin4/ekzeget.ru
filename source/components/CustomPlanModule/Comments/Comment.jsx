import React, { useState } from 'react';
import './styles.scss';
import ReadNext from '../ReadNext/ReadNext';
import { wordEnding } from '~utils/wordEnding';
import cn from 'classnames';
import { getName } from '~components/PlanAddUsers';
import dayjs from 'dayjs';
import InputComment from './InputComment';

function formatDateDifference(inputDate) {
  const now = dayjs();
  const date = dayjs(new Date(inputDate));

  const minuteDifference = now.diff(date, 'minute');
  const hourDifference = now.diff(date, 'hour');
  const dayDifference = now.diff(date, 'day');
  const monthDifference = now.diff(date, 'month');
  const yearDifference = now.diff(date, 'year');

  if (minuteDifference < 1) {
    return 'только что';
  }
  if (minuteDifference < 60) {
    return `${minuteDifference} ${wordEnding(minuteDifference, [
      'минуту',
      'минуты',
      'минут',
    ])} назад`;
  }
  if (hourDifference < 19) {
    return `${hourDifference} ${wordEnding(hourDifference, ['час', 'часа', 'часов'])} назад`;
  }
  if (dayDifference < 30) {
    return `${dayDifference} ${wordEnding(dayDifference, ['день', 'дня', 'дней'])} назад`;
  }
  if (monthDifference < 12) {
    return `${monthDifference} ${wordEnding(monthDifference, [
      'месяц',
      'месяца',
      'месяцев',
    ])} назад`;
  }
  return `${yearDifference} ${wordEnding(yearDifference, ['год', 'года', 'лет'])} назад`;
}

export default function Comment({
  comment,
  replyComment,
  planId,
  userId,
  groupId,
  typed,
  mentorId,
  setTyped,
  deleteComment,
  onOpenInput,
  isOpenInput,
}) {
  const [openAnswers, setOpenAnswers] = useState(false);
  const isMeMentor = userId === mentorId;
  const handleSend = async () => {
    if (typed.trim()) {
      replyComment(planId, groupId, typed, comment.id).then(() => {
        setTyped('');
      });
    }
  };

  return (
    <div className="comment-block">
      <div className="comment__head">
        <div
          className={cn(
            'comment__name',
            mentorId === comment?.author_id && 'comment__name-mentor',
          )}>
          {getName(comment.author)}
        </div>
        <div className="comment__time">{formatDateDifference(comment.created_at)}</div>
      </div>
      {comment.content && (
        <div className="comment__text">
          <ReadNext text={comment.content} className={'comment__read-next'} />
        </div>
      )}
      {isOpenInput && (
        <InputComment
          className={'input__answer'}
          handleSend={handleSend}
          onChange={v => setTyped(v)}
          typed={typed}
        />
      )}
      <div className="bnts_wrapper">
        <button
          onClick={() => {
            setTyped(`${getName(comment.author)}, `);
            onOpenInput();
          }}
          className="answer__btn">
          Ответить
        </button>
        {isMeMentor && (
          <button onClick={() => deleteComment(comment.id)} className="answer__btn">
            Удалить
          </button>
        )}
      </div>
      {comment.replies?.length > 0 && (
        <div
          onClick={() => setOpenAnswers(!openAnswers)}
          className={cn('open-answers', openAnswers && 'open-answers-active')}>
          <svg
            width="24"
            height="13"
            viewBox="0 0 24 13"
            fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <path d="M1 1L12.3548 11L23 1" stroke="black" strokeWidth="2" />
          </svg>
          <span>
            {comment.replies.length}{' '}
            {wordEnding(comment.replies.length, ['ответ', 'ответа', 'ответов'])}
          </span>
        </div>
      )}
      {openAnswers && comment.replies && comment.replies.length > 0 && (
        <div className="answers-block">
          {comment.replies.map((answer, i) => (
            <div className="comment-block" key={i}>
              <div className="comment__head">
                <div className="comment__name">{getName(answer.author)}</div>
                <div className="comment__time">{formatDateDifference(answer.created_at)}</div>
              </div>
              {answer.content && (
                <div className="comment__text">
                  <ReadNext text={answer.content} className={'comment__read-next'} />
                </div>
              )}
              <button
                onClick={() => {
                  setTyped(`${getName(answer.author)}, `);
                  onOpenInput();
                }}
                className="answer__btn">
                Ответить
              </button>
              {isMeMentor && (
                <button onClick={() => deleteComment(answer.id)} className="answer__btn">
                  Удалить
                </button>
              )}
            </div>
          ))}
        </div>
      )}
    </div>
  );
}
