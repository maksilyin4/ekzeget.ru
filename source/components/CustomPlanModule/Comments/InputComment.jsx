import React from 'react';
import './styles.scss';
import classNames from 'classnames';

export default function InputComment({ typed, onChange, handleSend, className }) {
  return (
    <div className={classNames('comments__input-wrapper', className)}>
      <input
        onChange={e => onChange(e.target.value)}
        onKeyDown={event => {
          if (event.key === 'Enter') {
            handleSend();
          }
        }}
        value={typed}
        className={'comments__input'}
        type="text"
        placeholder="Введите комментарий"
      />
      {typed.trim() && (
        <button onClick={handleSend} className="comments_send">
          {'>'}
        </button>
      )}
    </div>
  );
}
