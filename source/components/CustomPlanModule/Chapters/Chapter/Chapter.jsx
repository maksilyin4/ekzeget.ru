import React from 'react';
import './styles.scss';
import cn from 'classnames';
import passed from '~assets/images/passed-ch.png';

export default function ChapterCustomPlan({ chapter, onClick, active, i }) {
  return (
    <div onClick={onClick} className={cn('chapter__wrapper', active && 'chapter__selected')}>
      {chapter.is_closed && <img className="chapter__passed-icon" src={passed} alt="" />}
      <div className="chapter__circle" />
      <div className="chapter__circe-shadow" />
      <div className="chapter__body">
        <div className={cn('chapter__num', chapter.isPassed && 'chapter__num-passed')}>
          <span>{i + 1}</span>
        </div>
        <div className="chapter__name">{chapter.short}</div>
      </div>
    </div>
  );
}
