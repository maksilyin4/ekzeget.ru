import React from 'react';
import './styles.scss';
import ChapterCustomPlan from './Chapter/Chapter';
import { connect } from 'react-redux';
import { setActiveTab } from '../../../dist/actions/ReadingPlan';
import { activeTabUserRP } from '~dist/selectors/ReadingPlan';
import cn from 'classnames';
import Slider from 'react-slick';

function NextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <svg
      className={cn(className, 'arrow-ch next')}
      style={style}
      onClick={onClick}
      xmlns="http://www.w3.org/2000/svg"
      width="12"
      height="14"
      viewBox="0 0 12 14"
      fill="none">
      <path d="M11.998 7L4.99805 0.937822L4.99805 13.0622L11.998 7Z" fill="#2F2F2F" />
      <path d="M9.33398 7L2.33398 0.937822L2.33398 13.0622L9.33398 7Z" fill="white" />
    </svg>
  );
}

function PrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <svg
      className={cn(className, 'arrow-ch prev')}
      style={style}
      onClick={onClick}
      xmlns="http://www.w3.org/2000/svg"
      width="12"
      height="14"
      viewBox="0 0 12 14"
      fill="none">
      <path d="M0.000976563 7L7.00098 13.0622L7.00098 0.937822L0.000976563 7Z" fill="#2F2F2F" />
      <path d="M2.66504 7L9.66504 13.0622V0.937822L2.66504 7Z" fill="white" />
    </svg>
  );
}

function Chapters({ chapters, className, setActiveTab, activeTab }) {
  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    swipeToSlide: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
        },
      },
    ],

    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
  };

  return (
    <div className={cn('chapters__slider', className)}>
      <Slider className="slider__wrapper" {...settings}>
        {chapters.map((ch, i) => (
          <ChapterCustomPlan
            i={i}
            active={activeTab.short === ch.short}
            onClick={() => {
              setActiveTab(ch);
            }}
            chapter={ch}
            key={ch.id}
          />
        ))}
      </Slider>
    </div>
  );
}

const mapStateToProps = state => ({
  activeTab: activeTabUserRP(state),
});

const mapDispatchToProps = {
  setActiveTab,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chapters);
