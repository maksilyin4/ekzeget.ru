import React, { useState } from 'react';

export default function ReadNext({ text, className }) {
  const [isOpen, setOpen] = useState(false);
  const split = 200;
  return text.length < split ? (
    <>{text}</>
  ) : (
    <>
      {isOpen ? text : `${text.slice(0, split)}...`}
      {!isOpen && (
        <button onClick={() => setOpen(true)} className={className}>
          Читать далее
        </button>
      )}
    </>
  );
}
