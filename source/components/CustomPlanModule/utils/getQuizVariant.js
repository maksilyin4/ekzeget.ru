export const getQuizVariant = (correct, variants, defaultVariant) => {
  if (!correct && typeof correct !== 'number') {
    return defaultVariant;
  }
  if (correct < 6) {
    return variants[0];
  }
  if (correct < 8) {
    return variants[1];
  }
  if (correct < 10) {
    return variants[2];
  }
  return variants[3];
};
