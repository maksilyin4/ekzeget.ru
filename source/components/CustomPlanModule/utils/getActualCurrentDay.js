import dayjs from 'dayjs';
import R from 'ramda';

export const getActualCurrentDay = (currentDay, lastDay) => {
  if (currentDay < 1) {
    return {
      text: 'План еще не начался',
      day: 1,
    };
  }
  if (currentDay > lastDay) {
    return {
      text: 'План окончен',
      day: 1,
    };
  }
  return {
    text: `Сегодня ${currentDay}-й день чтений`,
    day: currentDay,
  };
};

export const scheduleLength = schedule => {
  return Object.values(schedule)?.filter(v => v > 0)?.length || 1;
};

export function findCurrentReadingDay(userPlans) {
  const [startDate, schedule] = [
    R.pathOr('', ['start_date'], userPlans),
    R.pathOr('', ['schedule'], userPlans),
  ];

  const daysPerWeek = scheduleLength(schedule);
  const daysPassed = dayjs().diff(dayjs(startDate), 'day');
  const fullWeeks = Math.floor(daysPassed / (7 * daysPerWeek));

  const remainingDays = daysPassed % (7 * daysPerWeek);
  const plannedReadDays = fullWeeks * daysPerWeek + Math.min(remainingDays, daysPerWeek);

  return plannedReadDays < 0 ? 0 : plannedReadDays;
}
