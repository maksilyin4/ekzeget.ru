import React from 'react';
import cn from 'classnames';
import { useAtom } from '@reatom/react';
import { planDetailsAtom } from '../../../pages/Lk/ReadingPlanDetail/model';
import './styles.scss';
import { Link } from 'react-router-dom';
import R from 'ramda';
import { useMedia } from 'react-use';
import Slider from 'react-slick';
import useGetDayParams from '../../../hooks/useGetDay';
import { getActualCurrentDay } from '../utils/getActualCurrentDay';
import useProgressPlan from '../../../hooks/useProgressPlan';

function NextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <svg
      className={cn(className, 'arrow-days next')}
      style={style}
      onClick={onClick}
      width="13"
      height="16"
      viewBox="0 0 13 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <path d="M12.5225 7.86719L5.52246 1.372L5.52246 14.3624L12.5225 7.86719Z" fill="#2F2F2F" />
      <path d="M9.8584 7.86719L2.8584 1.372L2.8584 14.3624L9.8584 7.86719Z" fill="white" />
    </svg>
  );
}

function PrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <svg
      className={cn(className, 'arrow-days prev')}
      style={style}
      onClick={onClick}
      width="13"
      height="16"
      viewBox="0 0 13 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <path d="M0.526367 7.86719L7.52637 14.3624L7.52637 1.372L0.526367 7.86719Z" fill="#2F2F2F" />
      <path d="M3.19043 7.86719L10.1904 14.3624V1.372L3.19043 7.86719Z" fill="white" />
    </svg>
  );
}

export default function SidebarCustomPlan({ userPlans, group }) {
  const isTablet = useMedia('(max-width: 1024px)');
  const [{ currentDay, getDay }] = useAtom(planDetailsAtom);
  const dayUrl = useGetDayParams();

  const { closedDays, remainderDays } = useProgressPlan();
  const url = i =>
    group?.id
      ? `/group/reading/${userPlans.plan.id}/day-${i + 1}/`
      : `/reading-plan/${userPlans.plan.id}/day-${i + 1}/`;

  const [userPlan] = React.useMemo(() => [R.pathOr(0, ['plan', 'lenght'], userPlans)], [userPlans]);
  const days = new Array(userPlan).fill(1);
  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 7,
    slidesToScroll: 5,
    swipeToSlide: true,
    responsive: [
      {
        breakpoint: 850,
        settings: {
          slidesToShow: 6,
          slidesToScroll: 3,
        },
      },
    ],

    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
  };

  return !isTablet ? (
    <aside className="cpd-aside__wrapper">
      {!!currentDay && (
        <div className="cpd__aside-top">{getActualCurrentDay(currentDay, userPlan).text}</div>
      )}
      <div className="cpd__aside-scroll">
        {days.map((_, i) => (
          <Link
            to={url(i)}
            onClick={() => getDay(i + 1)}
            className={cn('cpd__aside-day', Number(dayUrl) === i + 1 && 'cpd__aside-day--active')}
            key={i}>
            {i + 1} день
            <>
              {closedDays?.includes(i + 1) ? <div className="cpd__dot" /> : null}
              {remainderDays?.includes(i + 1) && i < currentDay && i + 1 !== currentDay ? (
                <div className="cpd__dot dot-red" />
              ) : null}
            </>
          </Link>
        ))}
      </div>
    </aside>
  ) : (
    <div className="slider-wrapper-d">
      {!!currentDay && (
        <div className="cpd__aside-top">{getActualCurrentDay(currentDay, userPlan).text}</div>
      )}
      <Slider className="slider-plan" {...settings}>
        {days.map((d, i) => (
          <Link
            to={url(i)}
            className={cn(
              'plan-day',
              closedDays?.includes(i + 1) && 'passed',
              remainderDays?.includes(i + 1) && i + 1 < currentDay && 'late',
              Number(dayUrl) === i + 1 && 'select-day',
            )}
            key={i}>
            {i + 1} день
          </Link>
        ))}
      </Slider>
    </div>
  );
}
