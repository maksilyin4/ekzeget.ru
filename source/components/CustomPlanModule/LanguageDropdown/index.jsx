import React, { useRef, useState } from 'react';
import BibleActionTranslates from '../../BibleActionPanel/BibleActionTranslates';
import { useCookies } from 'react-cookie';
import { useClickAway } from 'react-use';
import './styles.scss';
import classNames from 'classnames';

export default function LanguageDropdown() {
  const [cookies] = useCookies(['translates']);
  const [isOpen, setOpen] = useState(false);
  const ref = useRef();
  const refBtn = useRef();

  useClickAway(ref, e => {
    if (refBtn && refBtn.current && !refBtn.current.contains(e.target)) setOpen(false);
  });

  return (
    <div className={classNames('dropdown-wrapper', isOpen && 'open-dropdown')}>
      <div ref={refBtn} className="cpm__translate-dropdown" onClick={() => setOpen(!isOpen)}>
        {cookies.translates && cookies.translates.title}
        <svg
          width="14"
          height="12"
          viewBox="0 0 14 12"
          fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path d="M7 11.9971L13.0622 4.99707H0.937822L7 11.9971Z" fill="#2F2F2F" />
          <path d="M7 9.33301L13.0622 2.33301H0.937822L7 9.33301Z" fill="white" />
        </svg>
      </div>
      {isOpen && (
        <div ref={ref} className="cpm__translates">
          <BibleActionTranslates />
        </div>
      )}
    </div>
  );
}
