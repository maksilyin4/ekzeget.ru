import React from 'react';
import './styles.scss';
import ReadingProgressBar from '~components/ReadingProgressBar/ReadingProgressBar';
import Chapters from '../Chapters';
import { useMedia } from 'react-use';
import TranslatesSelect from '~components/Translates';
import { connect } from 'react-redux';
import readingPlanActions from '../../../dist/actions/ReadingPlan';
import { userPlans } from '~dist/selectors/ReadingPlan';
// import Slider from 'react-slick';
// import classNames from 'classnames';

function CustomPlanMain({ children, userPlans: data }) {
  const isMobile = useMedia('(max-width: 768px)');

  const userPlans = Object.values(data)[0];
  const readings = userPlans?.reading?.flat(1);
  return (
    <>
      <div className="cpd-main">
        <section className="cpd-main_block">
          <div className="cpd__top">
            {!isMobile && (
              <>
                <ReadingProgressBar className={'cpd_progress'} userPlans={userPlans} />
              </>
            )}
            <div className="chapters-translate">
              {!isMobile && readings?.length > 0 && <Chapters chapters={readings} />}
              <TranslatesSelect isPlan isLk />
            </div>
          </div>
          <div>{children}</div>
        </section>
      </div>
    </>
  );
}

const mapStateToProps = state => ({
  userPlans: userPlans(state),
});

const mapDispatchToProps = {
  ...readingPlanActions,
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomPlanMain);
