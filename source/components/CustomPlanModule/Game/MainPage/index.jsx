import React, { useMemo } from 'react';
import './styles.scss';

// Картинки
import img1 from '~assets/images/quiz/forget/1-forget.png';
import img2 from '~assets/images/quiz/forget/2-forget.png';
import img3 from '~assets/images/quiz/forget/3-forget.jpg';
import img4 from '~assets/images/quiz/forget/4-forget.png';
import img5 from '~assets/images/quiz/forget/5-forget.png';
import img6 from '~assets/images/quiz/forget/6-forget.png';

import { getRandomElement } from '~utils/getRandomElement';

export default function MainPageGame() {
  const img = useMemo(() => {
    return getRandomElement([img1, img2, img3, img4, img5, img6]);
  }, []);
  return (
    <div>
      <div className="img__wrapper">
        <img className="game__image" src={img} alt="" />
      </div>
      <div className="game__alert">
        Вы забыли ответить на 10 вопросов по этой главе. <br />
        Уровень усвоения материала не выявлен. Статистика тоже не сохранится.
      </div>
    </div>
  );
}
