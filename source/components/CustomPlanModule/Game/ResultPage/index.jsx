import React, { useEffect, useMemo, useState } from 'react';
import './styles.scss';
import resultImage from '~assets/images/result-image.png';
import resultImageGold from '~assets/images/gold-res.png';
import passImage from '~assets/images/chapter-pass.png';
import { useMedia } from 'react-use';
import { wordEnding } from '~utils/wordEnding';
import { connect } from 'react-redux';
import readingPlanActions, { setActiveTab } from '../../../../dist/actions/ReadingPlan';
import { getQuizByPlanId } from '../../../../dist/actions/Quiz';
import { completedQueezePlan } from '~dist/selectors/Quiz';
import { useLocation } from 'react-router-dom';

// изображения для рандомных результатов 40
import img1_40 from '~assets/images/quiz/40/1-40.png';
import img2_40 from '~assets/images/quiz/40/2-40.png';
import img3_40 from '~assets/images/quiz/40/3-40.png';
import img4_40 from '~assets/images/quiz/40/4-40.png';
import img5_40 from '~assets/images/quiz/40/5-40.png';
import img6_40 from '~assets/images/quiz/40/6-40.png';

// 60
import img1_60 from '~assets/images/quiz/60/1-60.png';
import img2_60 from '~assets/images/quiz/60/2-60.png';
import img3_60 from '~assets/images/quiz/60/3-60.png';
import img4_60 from '~assets/images/quiz/60/4-60.png';
import img5_60 from '~assets/images/quiz/60/5-60.png';
import img6_60 from '~assets/images/quiz/60/6-60.png';

// 80
import img1_80 from '~assets/images/quiz/80/1-80.png';
import img2_80 from '~assets/images/quiz/80/2-80.jpg';
import img3_80 from '~assets/images/quiz/80/3-80.png';
import img4_80 from '~assets/images/quiz/80/4-80.png';
import img5_80 from '~assets/images/quiz/80/5-80.png';
import img6_80 from '~assets/images/quiz/80/6-80.png';

// 100
import img1_100 from '~assets/images/quiz/100/1-100.png';
import img2_100 from '~assets/images/quiz/100/2-100.png';
import img3_100 from '~assets/images/quiz/100/3-100.jpg';
import img4_100 from '~assets/images/quiz/100/4-100.png';
import img5_100 from '~assets/images/quiz/100/5-100.png';

// new day
import new_1 from '~assets/images/quiz/new-day/1-new-day.jpg';
import new_2 from '~assets/images/quiz/new-day/2-new-day.png';
import new_3 from '~assets/images/quiz/new-day/3-new-day.png';
import new_4 from '~assets/images/quiz/new-day/4-new-day.png';
import new_5 from '~assets/images/quiz/new-day/5-new-day.png';
import new_6 from '~assets/images/quiz/new-day/6-new-day.jpg';
import { getQuizVariant } from '~components/CustomPlanModule/utils/getQuizVariant';
import { getRandomElement } from '~utils/getRandomElement';
import classNames from 'classnames';
import Preloader from '~components/Preloader/Preloader';

const questions = [
  'Какие чувства я испытываю после чтения?',
  'Какие мысли возникли у меня после чтения?',
  'Понимаю ли я чему учит эта глава?',
  'Нахожу ли я ответы на свои вопросы в Библии?',
  'Что значит для меня чтение Библии?',
  'Чем мне поможет знание Библии?',
  'На какие вопросы я нахожу ответы в Библии?',
  'Какую пользу я получаю, читая слова Библии?',
  'Что меня зацепило в этой главе?',
  'Понимаю ли я смысл прочитанного?',
  'Какой смысл кроется в словах этой главы?',
  'Помогает ли мне чтение Библии в жизни?',
  'Что из прочитанного я могу применить в жизни?',
  'Какие слова побуждают меня задуматься?',
  'Какие вопросы возникли у меня после чтения?',
  'Какие ассоциации возникли у меня после чтения?',
  'Что из этой главы я могу применить в качестве совета?',
  'Что я могу изменить в своей жизни после прочтения главы?',
  'Какие слова удивили меня в этой главе?',
  'Что нового я открыл для себя после чтения?',
  'Вижу ли я аналогии в жизни, читая слова Библии?',
  'Есть ли то, чего я не могу принять?',
  'Есть ли то, что меня насторожило?',
  'Есть ли то, над чем мне стоит задуматься?',
  'Могу ли я что-то исправить в своей жизни?',
  'Могу ли я помочь людям, применяя слова Библии в жизни?',
  'Могу ли я стать лучше после чтения Библии?',
  'Какой смысл заложен в словах Библии?',
  'Приближает ли меня к Богу чтение глав Библии?',
  'Как я могу изменить мир вокруг себя, применяя слова Библии в жизни?',
  'Слышу ли я Слова Бога, обращающегося ко мне?',
  'О чем мне лично сказал Господь в этой главе?',
  'Чувствую ли я непосредственное общение с Богом, читая Библию?',
];

function ResultPageGame({ quiz, userPlans, getQuizByPlanId, setActiveTab, isLast }) {
  const isMobile = useMedia('(max-width: 768px)');
  const correct = quiz?.meta?.correct;
  const location = useLocation();
  const urlParams = new URLSearchParams(location.search);
  const planIdParams = urlParams.get('plan_id');

  const images40 = [img1_40, img2_40, img3_40, img4_40, img5_40, img6_40];
  const images60 = [img1_60, img2_60, img3_60, img4_60, img5_60, img6_60];
  const images80 = [img1_80, img2_80, img3_80, img4_80, img5_80, img6_80];
  const images100 = [img1_100, img2_100, img3_100, img4_100, img5_100];
  const imagesNewDay = [new_1, new_2, new_3, new_4, new_5, new_6];

  const questionsRes = useMemo(() => {
    const firstIndex = Math.floor(Math.random() * questions.length);
    let secondIndex;
    do {
      secondIndex = Math.floor(Math.random() * questions.length);
    } while (secondIndex === firstIndex);

    // Возвращаем массив с двумя случайными вопросами
    return [questions[firstIndex], questions[secondIndex]];
  }, []);

  const [imageRes, setImageRes] = useState('');
  useEffect(() => {
    if (typeof isLast === 'boolean') {
      if (isLast && !imageRes && (correct || correct === 0)) {
        setImageRes(getRandomElement(imagesNewDay));
      } else if ((correct || correct === 0) && !imageRes) {
        const img = getQuizVariant(
          correct,
          [
            getRandomElement(images40),
            getRandomElement(images60),
            getRandomElement(images80),
            getRandomElement(images100),
          ],
          img1_40,
        );
        setImageRes(img);
      }
    }
  }, [correct, isLast, quiz]);

  useEffect(() => {
    if (planIdParams && userPlans.reading) {
      getQuizByPlanId(planIdParams);
      const tab = userPlans.reading.flat().find(p => p.id === Number(planIdParams));
      if (tab) {
        setActiveTab(tab);
      }
    }
  }, [planIdParams, userPlans]);

  const titleRes = useMemo(
    () =>
      getQuizVariant(
        correct,
        ['', 'Хороший результат!', 'Отличный результат!', 'Браво! Вы гений!'],
        '',
      ),
    [correct],
  );

  return quiz ? (
    <div>
      <div className="img__wrapper">
        <img className="pass_image" src={passImage} alt="" />
        <img className="game__image" src={imageRes} alt="" />
      </div>
      <div className="result__alert">
        <div className="result__img-wrapper">
          <img src={correct > 5 ? resultImageGold : resultImage} alt="" />
          <span className={classNames(correct > 5 && 'gold')}>{correct * 10}</span>
        </div>
        <div>
          {correct > 5 && <div className="result__title-answer">{titleRes}</div>}
          Вы усвоили{' '}
          <span className={classNames(correct > 5 && 'green_mark')}>{correct * 10}%</span>{' '}
          материала, {correct < 6 && <br />}
          ответив на {correct} {wordEnding(correct, ['вопрос', 'вопроса', 'вопросов'])} из 10.{' '}
          <br />
          {correct < 6 &&
            'Для достижения лучшего результата прочтите главу еще раз и попробуйте повторно ответить на вопросы.'}
        </div>
      </div>
      <div className="text-result">
        {questionsRes[0]}
        {!isMobile && <br />}
        <br />
        {questionsRes[1]}
      </div>
    </div>
  ) : (
    <Preloader />
  );
}

const mapStateToProps = state => ({
  quiz: completedQueezePlan(state),
});

const mapDispatchToProps = {
  ...readingPlanActions,
  setActiveTab,
  getQuizByPlanId,
};

export default connect(mapStateToProps, mapDispatchToProps)(ResultPageGame);
