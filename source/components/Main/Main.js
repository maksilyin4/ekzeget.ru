import React, { Component } from 'react';
import { withCookies } from 'react-cookie';
import { hot } from 'react-hot-loader';

import { Popup, LS } from '../../dist/browserUtils';
import HeaderArea from '../Layout/HeaderArea/HeaderArea';
import ContentArea from '../Layout/ContentArea/ContentArea';
import FooterArea from '../Layout/FooterArea/';
import ButtonToTop from '../ButtonToTop';

import '../../assets/sass/main.scss';
import { baseUlr } from '../../getStore';

if (Popup) {
  Popup.registerPlugin('popover', function create(content, target) {
    this.create({
      content,
      className: 'popover',
      noOverlay: true,
      position: function position(box) {
        const bodyRect = document.body.getBoundingClientRect();
        const btnRect = target.getBoundingClientRect();
        const btnHeight = btnRect.height;
        const btnOffsetTop = btnRect.top - bodyRect.top;
        const btnOffsetLeft = btnRect.left - bodyRect.left;
        const scroll = document.documentElement.scrollTop || document.body.scrollTop;

        box.style.top = `${btnOffsetTop + btnHeight + 10 - scroll}px`;
        box.style.left = `${btnOffsetLeft + target.offsetWidth - box.offsetWidth}px`;
        box.style.margin = 0;
        box.style.opacity = 1;
      },
    });
  });
}

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = { error: null };

    if (process.env.BROWSER) {
      if (!LS.get('urlFromBible')) {
        LS.set('urlFromBible', 'initial');
      }

      LS.set('isFirstLoad', 'first');
    }
  }

  componentDidCatch(error) {
    if (process.env.NODE_ENV === 'production') {
      const { cookies } = this.props;

      LS.set('urlFromBible', 'initial');

      const cookiesAll = cookies.getAll();
      if (cookiesAll) {
        Object.keys(cookiesAll).forEach(cookieName => cookies.remove(cookieName));
      }

      this.setState({ error });
    }
  }

  render() {
    const { directProps, initialState } = this.props;

    const { error } = this.state;

    if (process.env.NODE_ENV === 'production' && error) {
      return (
        <div className="error">
          <p className="error-description">Произошла ошибка, приносим свои извинения</p>
          <p className="error-description">
            Дабы улучшить наш проект, отправьте нам описание своих действий, которые привели к
            ошибке
          </p>
          <p className="error-description">
            Мы обязательно рассмотрим и в скором времени исправим, спасибо!
          </p>
          <a className="error-link" href={baseUlr}>
            Вернуться на главную страницу сайта Экзегет{' '}
          </a>
        </div>
      );
    }

    return (
      <div className="page-wrapper">
        <HeaderArea />
        <ContentArea directProps={directProps} initialState={initialState} />
        <FooterArea />
        {Popup && <Popup />}
        <ButtonToTop scrollStepInPx="50" delayInMs="4" />
      </div>
    );
  }
}

export default hot(module)(withCookies(Main));
