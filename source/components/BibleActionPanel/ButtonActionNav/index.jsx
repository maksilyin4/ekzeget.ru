import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import cx from 'classnames';

import Icon from '../../Icons/Icons';

import { getUrlBibleActionPanel, getNavNum } from '../utilsBibleActionPanel';

const ButtonActionNav = ({
  bookCode,
  pagesCount,
  currentPage,
  navText,
  icon = 'angle-right',
  className = '',
  type = '',
  search,
  chapterLink = '',
  verseLink = '',
  ekzeget = '',
  isMobile = false,
}) => {
  const navNum = getNavNum({ type, pagesCount, currentPage });
  const labelText = isMobile ? navNum : `${navText} ${navNum}`;
  return (
    <Link
      className={cx('bap__item bap__item_nav', className)}
      to={getUrlBibleActionPanel({
        bookCode,
        search,
        navNum,
        chapterLink,
        verseLink,
        ekzeget,
      })}>
      <span className="bap__item-label">
        <span className="bap__item-label-hidden-text">{pagesCount ? labelText : ''}</span>
        <Icon icon={icon} />
      </span>
    </Link>
  );
};

ButtonActionNav.propTypes = {
  bookCode: PropTypes.string,
  className: PropTypes.string,
  search: PropTypes.string,
  type: PropTypes.string,
  icon: PropTypes.string,
  pagesCount: PropTypes.number,
  currentPage: PropTypes.number,
  navText: PropTypes.string,
  chapterLink: PropTypes.string,
  verseLink: PropTypes.string,
};

export default ButtonActionNav;
