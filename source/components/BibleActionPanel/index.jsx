import React, { useState, useMemo, useEffect } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { useCookies } from 'react-cookie';
import { withRouter } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import './style.scss';

import { getBooks } from '../../dist/actions/Bible';
import { getNewOldTestament } from '../../dist/selectors/Bible';
import { getIsMobile } from '../../dist/selectors/screen';
import { setDataToStoreFromBible } from '~dist/utils';

import BibleActionBooks from './BibleActionBooks';
import BibleActionChapters from './BibleActionChapters';
import BibleActionTranslates from './BibleActionTranslates';
import BibleActionBody from './BibleActionBody';
import ButtonActionNav from './ButtonActionNav';

import { getActiveMenu, getPanelCenter } from './utilsBibleActionPanel';

const getBibleContent = ({
  type,
  pagesCount,
  bookCode,
  search,
  currentPage,
  newTestament,
  oldTestament,
  chapterLink,
  verseLink,
  ekzeget,
}) => {
  switch (type) {
    case 'book':
      return (
        <BibleActionBooks
          newTestament={newTestament}
          oldTestament={oldTestament}
          chapterLink={chapterLink}
          verseLink={verseLink}
          ekzeget={ekzeget}
        />
      );
    case 'chapter':
      return (
        <div className="bible-content-item bap-popup">
          <BibleActionChapters
            pagesCount={pagesCount}
            currentPage={currentPage}
            bookCode={bookCode}
            search={search}
            chapterLink={chapterLink}
            verseLink={verseLink}
            ekzeget={ekzeget}
          />
        </div>
      );

    case 'translate':
      return (
        <div className="bible-content-item bap-popup">
          <BibleActionTranslates />
        </div>
      );

    default:
      break;
  }
};

const BibleActionPanel = ({
  pagesCount,
  currentPage,
  bookCode = '',
  bookTitle,
  navText,
  location: { search, pathname },
  match: {
    params: { chapter_num, verse_id, ekzeget = '', tolkovatel },
  },
}) => {
  const [activeMenu, setActiveMenu] = useState('');
  const [cookies] = useCookies(['translates']);
  const dispatch = useDispatch();
  const { newTestament, oldTestament } = useSelector(getNewOldTestament);

  const isErrorBookLink = useMemo(
    () =>
      bookCode &&
      [...newTestament, ...oldTestament].some(({ code }) =>
        code.toLowerCase().includes(bookCode.toLowerCase()),
      ),
    [bookCode, newTestament, oldTestament],
  );

  const isMobile = useSelector(getIsMobile);

  useEffect(() => {
    if (newTestament.length === 0 && oldTestament.length === 0) {
      dispatch(getBooks());
    }

    const listener = e => {
      if (typeof e.target.className === 'string' && !e.target.className.includes('bap-popup')) {
        setActiveMenu('');
      }
    };

    window.addEventListener('click', listener);
    return () => window.removeEventListener('click', listener);
  }, []);

  useEffect(() => {
    // for SEO

    if (!isNaN(currentPage)) {
      setDataToStoreFromBible(pathname, search);
    }
  }, [currentPage, isErrorBookLink, tolkovatel, ekzeget]);

  const isChapter = pathname.includes('glava');

  const togglePullDown = type => setActiveMenu(type === activeMenu ? '' : type);

  const panelCenter = useMemo(() => {
    const panel = getPanelCenter({
      activeMenu,
      togglePullDown,
      bookTitle,
      currentPage,
      currentTranslate: cookies.translates,
      isChapter,
      verse_id,
      isMobile,
    });
    return isChapter ? panel : panel.filter(el => el.id !== 'translate');
  }, [bookTitle, currentPage, cookies.translates?.id, activeMenu, verse_id, isMobile]);

  const [chapterLink, verseLink] = useMemo(() => {
    const verse = verse_id ? `stih-${verse_id}` : '';
    return [chapter_num, verse];
  }, [chapter_num, verse_id]);

  return (
    <div className="bap bap-nav">
      {currentPage > 0 && (
        <ButtonActionNav
          key="prev"
          bookCode={bookCode}
          pagesCount={pagesCount}
          currentPage={currentPage}
          navText={navText}
          search={search}
          icon="angle-left"
          className="bap__item_nav-prev"
          type="prev"
          chapterLink={chapterLink}
          verseLink={verseLink}
          ekzeget={ekzeget}
          isMobile={isMobile}
        />
      )}

      {panelCenter.map(({ id, onClick, active, className, title }) => (
        <BibleActionBody
          key={id}
          onClick={onClick}
          active={active}
          className={className}
          title={title}
        />
      ))}

      <ButtonActionNav
        key="next"
        bookCode={bookCode}
        pagesCount={pagesCount}
        currentPage={currentPage}
        navText={navText}
        search={search}
        icon="angle-right"
        className="bap__item_nav-next"
        type="next"
        chapterLink={chapterLink}
        verseLink={verseLink}
        ekzeget={ekzeget}
        isMobile={isMobile}
      />
      <div className={cx('bap__item-content', { ...getActiveMenu(activeMenu, isChapter) })}>
        {getBibleContent({
          type: activeMenu,
          pagesCount,
          bookCode,
          search,
          currentPage,
          newTestament,
          oldTestament,
          chapterLink,
          verseLink,
          ekzeget,
        })}
      </div>
    </div>
  );
};

BibleActionPanel.propTypes = {
  pagesCount: PropTypes.number,
  bookCode: PropTypes.string,
  bookTitle: PropTypes.string,
  currentPage: PropTypes.number,
  navText: PropTypes.string,
  location: PropTypes.object,
};

export default withRouter(BibleActionPanel);
