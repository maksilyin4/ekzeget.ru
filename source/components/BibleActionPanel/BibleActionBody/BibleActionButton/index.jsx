import React from 'react';
import PropTypes from 'prop-types';

import Icon from '../../../Icons/Icons';
import Preloader from '../../../Preloader/Preloader';

import './bibleActionButton.scss';

const BibleActionButton = ({ bookTitle, onClick = f => f }) => (
  <button className="bible-action-button bap-popup" onClick={onClick}>
    <span className="bap__item-label bap-popup">
      {bookTitle ? (
        <>
          <span className="bap-popup">{bookTitle}</span>
          <Icon icon="angle-down" />
        </>
      ) : (
        <Preloader />
      )}
    </span>
  </button>
);

BibleActionButton.propTypes = {
  bookTitle: PropTypes.string,
  onClick: PropTypes.func,
};

export default BibleActionButton;
