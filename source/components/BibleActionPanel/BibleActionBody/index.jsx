import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import BibleActionButton from './BibleActionButton';

const BibleActionBody = ({ className, active, title, onClick }) => (
  <div
    className={cx('bap__item bap__item-dropdown', className, {
      active,
    })}>
    <BibleActionButton className={className} active={active} bookTitle={title} onClick={onClick} />
  </div>
);

BibleActionBody.propTypes = {
  className: PropTypes.string,
  active: PropTypes.bool,
  title: PropTypes.string,
  onClick: PropTypes.func,
};

export default BibleActionBody;
