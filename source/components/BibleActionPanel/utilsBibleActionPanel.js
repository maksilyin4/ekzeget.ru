export const getUrlBibleActionPanel = ({
  bookCode,
  search,
  navNum,
  chapterLink,
  verseLink = '',
  ekzeget,
}) => {
  const ekzegetEnd = ekzeget ? `${ekzeget}/` : '';
  const verseLinkEnd = verseLink
    ? `${chapterLink}/${verseLink.split('-')[0]}-${navNum}`
    : `glava-${navNum}`;

  return `/bible/${bookCode}/${verseLinkEnd}/${ekzegetEnd}${search}`;
};

export const getEkzegetEnd = ekzeget => (ekzeget ? `${ekzeget}/` : '');

export const getNavNum = ({ type, pagesCount, currentPage }) => {
  let num = 0;

  if (type === 'prev') {
    num = currentPage - 1 === 0 ? pagesCount : currentPage - 1;
  }
  if (type === 'next') {
    num = currentPage + 1 > pagesCount ? 1 : currentPage + 1;
  }
  return num;
};

export const getActiveMenu = (type, isChapter) => {
  switch (type) {
    case 'book':
      return { active: true, 'bible-book': 'bible-book' };
    case 'chapter':
      return {
        active: true,
        'bible-chapter': 'bible-chapter',
        'not-chapter': !isChapter && 'not-chapter',
      };
    case 'translate':
      return { active: true, 'bible-translate': 'bible-translate' };

    default:
      return { active: false };
  }
};
const getNameUrl = (verseId, currentPage, isMobile) => {
  const text = verseId ? 'Стих' : 'Глава';
  return isMobile ? currentPage : `${text} ${currentPage}`;
};

const getNameTitleMobile = isMobile => (isMobile ? 'Глава' : 'Выберите главу');

const getTitle = ({ currentPage, verse_id, isMobile }) =>
  currentPage ? getNameUrl(verse_id, currentPage, isMobile) : getNameTitleMobile(isMobile);

export const getPanelCenter = ({
  activeMenu,
  togglePullDown,
  bookTitle,
  currentPage,
  currentTranslate,
  isChapter,
  verse_id,
  isMobile = false,
}) => [
  {
    id: 'book',
    className: 'bap__item_books  bap-popup',
    active: activeMenu === 'book',
    title: bookTitle,
    onClick: () => togglePullDown('book'),
  },
  {
    id: 'chapter',
    className: `bap__item_chapters  bap-popup ${!isChapter ? 'bap__item_not_chapters' : ''}`,
    active: activeMenu === 'chapter',
    title: getTitle({ currentPage, verse_id, isMobile }),
    onClick: () => togglePullDown('chapter'),
  },
  {
    id: 'translate',
    className: 'bap__item_translate  bap-popup',
    active: activeMenu === 'translate',
    title: currentTranslate && currentTranslate.title,
    onClick: () => togglePullDown('translate'),
  },
];
