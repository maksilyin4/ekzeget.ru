import React, { useCallback } from 'react';
import { useCookies } from 'react-cookie';
import { useSelector } from 'react-redux';

import { getTranslatesForSelectBible } from '~dist/selectors/translates';

import BibleActionTranslatesItem from './BibleActionTranslatesItem';

import { useTranslate } from '~utils/useTranslate';

const BibleActionTranslates = ({ onlyButtons }) => {
  const [cookies] = useCookies(['translates', 'translate']);
  const { translates: translatesCookie } = cookies;

  const setTranslate = useTranslate();

  const translates = useSelector(getTranslatesForSelectBible);

  const handleClick = useCallback(
    translate => () => {
      if (translatesCookie.id !== translate.id) {
        setTranslate(translate);
      }
    },
    [translatesCookie],
  );

  return translates.map((el, i) => (
    <BibleActionTranslatesItem
      onlyButtons={onlyButtons}
      key={i}
      translates={el}
      translatesCookie={translatesCookie}
      handleClick={handleClick}
    />
  ));
};

export default BibleActionTranslates;
