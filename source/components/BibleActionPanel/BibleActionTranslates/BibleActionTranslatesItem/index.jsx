import React from 'react';

import Button from '../../../ButtonNew';

const BibleActionTranslatesItem = ({ translates, onlyButtons, handleClick, translatesCookie }) =>
  onlyButtons ? (
    translates.map(translate => (
      <Button
        key={translate.code}
        mod="gradient"
        className={{ active: translatesCookie.code === translate.code }}
        title={translate.title}
        onClick={handleClick(translate)}
      />
    ))
  ) : (
    <div className="bible-translate-item bap-popup">
      {translates.map(translate => (
        <Button
          key={translate.code}
          mod="gradient"
          className={{ active: translatesCookie.code === translate.code }}
          title={translate.title}
          onClick={handleClick(translate)}
        />
      ))}
    </div>
  );

export default BibleActionTranslatesItem;
