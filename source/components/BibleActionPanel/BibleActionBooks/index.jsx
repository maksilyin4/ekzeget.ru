import React from 'react';
import PropTypes from 'prop-types';

import BibleActionBooksList from './BibleActionBooksList';

import './style.scss';

const BibleActionBooks = ({
  oldTestament = [],
  newTestament = [],
  chapterLink = '',
  verseLink = '',
  ekzeget = '',
}) => {
  return (
    <div className="bab bap-popup">
      <BibleActionBooksList
        key="newTestament"
        testament={newTestament}
        className="bab__testament_new bap-popup"
        headText="Новый завет"
        chapterLink={chapterLink}
        verseLink={verseLink}
        ekzeget={ekzeget}
      />
      <BibleActionBooksList
        key="oldTestament"
        testament={oldTestament}
        className="bab__testament_old bap-popup"
        headText="Ветхий завет"
        chapterLink={chapterLink}
        verseLink={verseLink}
        ekzeget={ekzeget}
      />
    </div>
  );
};

BibleActionBooks.propTypes = {
  newTestament: PropTypes.array.isRequired,
  oldTestament: PropTypes.array.isRequired,
  chapterLink: PropTypes.string,
  verseLink: PropTypes.string,
  ekzeget: PropTypes.string,
};

export default BibleActionBooks;
