import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { useCookies } from 'react-cookie';

import Button from '../../../ButtonNew';
import { getEkzegetEnd } from '../../utilsBibleActionPanel';

const BibleActionBooksList = ({
  testament = [],
  className = 'bab__testament_old',
  headText = 'Новый завет',
  chapterLink = '',
  verseLink = '',
  ekzeget = '',
}) => {
  const [cookies, setCookie] = useCookies(['bookTitle']);

  const endLink = verseLink ? `${chapterLink}/${verseLink}/${getEkzegetEnd(ekzeget)}` : chapterLink;

  const handleClick = (menu, title) => () => {
    const { bookTitle = { title: '' } } = cookies;
    if (bookTitle.title !== title) {
      setCookie('bookTitle', { title, menu }, { path: '/' });
    }
  };

  return (
    <div className={cx('bab__testament bap-popup', className)}>
      <h3 className="bab__head bap-popup">{headText}</h3>
      <nav className="bab__list bap-popup">
        {testament.map(({ code, title, menu, testament }) => (
          <Button
            key={code}
            type="NavLink"
            mod="gradient bab__item"
            to={{
              pathname: `/bible/${code}/${endLink}`,
              state: { bookTestamentId: +testament.id },
            }}
            title={menu}
            onClick={handleClick(menu, title)}
          />
        ))}
      </nav>
    </div>
  );
};

BibleActionBooksList.propTypes = {
  testament: PropTypes.array,
  className: PropTypes.string,
  headText: PropTypes.string,
  chapterLink: PropTypes.string,
  verseLink: PropTypes.string,
  ekzeget: PropTypes.string,
};

export default BibleActionBooksList;
