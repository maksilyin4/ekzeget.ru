import React, { useCallback } from 'react';
import Button from '../ButtonNew';
import { getEkzegetEnd } from './utilsBibleActionPanel';

const BibleActionChapters = ({
  pagesCount,
  currentPage,
  bookCode,
  search,
  verseLink = '',
  chapterLink = '',
  ekzeget = '',
}) => {
  const chapterList = [];
  const getUrlTitle = useCallback(
    i => (verseLink ? `${chapterLink}/stih-${i}/${getEkzegetEnd(ekzeget)}` : `glava-${i}/`),
    [chapterLink, verseLink, ekzeget],
  );

  for (let i = 1; i <= pagesCount; i++) {
    chapterList.push(
      <Button
        key={i}
        className={{ active: currentPage === i }}
        type="NavLink"
        mod="gradient"
        title={i}
        to={`/bible/${bookCode}/${getUrlTitle(i)}${search}`}
      />,
    );
  }

  return chapterList;
};

export default BibleActionChapters;
