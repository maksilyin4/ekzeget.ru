export default function calcParallelBooks(allPlan) {
  // первые 7 дней, этого достаточно, чтобы узнать кол-во параллельных глав
  const firstSevenKeys = Object.keys(allPlan).slice(0, 7);

  const plan = Object.values(
    firstSevenKeys.reduce((acc, key) => {
      acc[key] = allPlan[key];
      return acc;
    }, {}),
  )
    .flat(1)
    .map(({ book_code }) => book_code);

  const uniqueBooks = Array.from(new Set(plan));
  return uniqueBooks.length;
}
