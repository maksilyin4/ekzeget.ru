import React, { useMemo } from 'react';
import ReadingPlanPopupHead from '../Popups/ReadingPlanDetail/reading-plan-head';
import './ReadingPlanTable.scss';
import cn from 'classnames';
import { v4 as uuidv4 } from 'uuid';
import dayjs from 'dayjs';
import weekday from 'dayjs/plugin/weekday';
import ru from 'dayjs/locale/ru';
import { getBooks } from '../../dist/actions/Bible';
import { connect } from 'react-redux';
import { booksReducer } from '~dist/selectors/Bible';
import calcParallelBooks from './model/calcParallelBooks';

dayjs.extend(weekday);

const generateSchedule = (startDate, endDate, workDays) => {
  const schedule = [];
  let currentDate = dayjs(new Date(startDate)).locale(ru);
  const end = dayjs(new Date(endDate));

  const workDayIndices = workDays.map(day => {
    const days = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];
    return days.indexOf(day);
  });
  const totalDays = end.diff(currentDate, 'day');

  let addedDays = 0;

  while (addedDays < totalDays + 1) {
    const currentWeekday = currentDate.weekday();
    if (workDayIndices.includes(currentWeekday)) {
      schedule.push({
        date: currentDate.format('D MMM'),
        weekday: currentDate.format('dd').toLowerCase(),
      });
    }
    addedDays++;
    currentDate = currentDate.add(1, 'day');
  }

  return schedule;
};

function ReadingPlanTable({ plan, isMin, head }) {
  const { startDate, endDate, weekDays } = head ?? {};
  const schedule = useMemo(() => {
    if (startDate && endDate && weekDays && plan) {
      return generateSchedule(startDate, endDate, weekDays, Object.keys(plan).length);
    }
    return plan ? Object.values(plan) : [];
  }, [startDate, endDate, weekDays, plan]);

  const renderReadingList = useMemo(() => {
    const planArray = plan ? Object.values(plan) : [];
    return (
      schedule &&
      schedule?.map((day, i) =>
        planArray[i] ? (
          <div key={uuidv4()} className="rpd__item">
            <div className="rpd__item-day">
              {startDate ? (
                <>
                  {`${day.date}`.replace('.', '')}, <span>{day.weekday}</span>
                </>
              ) : (
                <>День {i + 1}</>
              )}
            </div>
            <div className="rpd__item-duration">{i + 1}</div>
            <div>
              <div className="rpd__item-right">
                {planArray[i]?.map(item => (
                  <div key={uuidv4()} className="rpd__item-link">
                    {item.short}
                  </div>
                ))}
              </div>
            </div>
          </div>
        ) : (
          <></>
        ),
      )
    );
  }, [plan, schedule]);

  return (
    <div className={cn('rpd__scroll-container', isMin && 'rpd__scroll-container_min')}>
      {head && (
        <ReadingPlanPopupHead parallelBooks={calcParallelBooks(plan)} head={head} isMin={isMin} />
      )}
      <div className="rpd__list list__scroll">{renderReadingList}</div>
    </div>
  );
}

const selectorProps = state => ({
  books: booksReducer(state),
});

const actionProps = {
  getBooks,
};

export default connect(selectorProps, actionProps)(ReadingPlanTable);
