import React from 'react';
import cn from 'classnames';
import './index.scss';
import { Link } from 'react-router-dom';

export default function CustomButtonCP({
  size = 'm',
  to,
  color = 'primary',
  type,
  children,
  style,
  disabled,
  onClick,
  className,
}) {
  return to ? (
    <Link
      style={style}
      to={to}
      className={cn(
        'cp__button-main',
        {
          'cp__button-s': size === 's',
          'cp__button-m': size === 'm',
          'cp__button-yellow': color === 'primary',
          'cp__button-gray': color === 'gray',
          'cp__button-blue': color === 'blue',
        },
        className,
      )}>
      {children}
    </Link>
  ) : (
    <button
      style={style}
      type={type}
      onClick={onClick}
      disabled={disabled}
      className={cn(
        'cp__button-main',
        {
          'cp__button-s': size === 's',
          'cp__button-m': size === 'm',
          'cp__button-yellow': color === 'primary',
          'cp__button-gray': color === 'gray',
          'cp__button-blue': color === 'blue',
        },
        className,
      )}>
      {children}
    </button>
  );
}
