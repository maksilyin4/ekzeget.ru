import React from 'react';
import CustomFieldCreatePlan from '../CustomField';
import './index.scss';

export default function index({
  title,
  countChapter,
  countParallelChapter,
  onChange,
  onChangeParallel,
}) {
  return (
    <div className="cp__block-head">
      <h3 className="cp__head-title">{title}</h3>
      <div className="cp__label-head">
        <span className="cp__label-field cp__head-label">Количество глав в день</span>
        <CustomFieldCreatePlan
          className="cp__min-field--one"
          value={countChapter}
          onChange={e => {
            const v = Number(e.target.value);
            if (isNaN(v)) {
              return;
            }
            if (v > 9) {
              return;
            }
            onChange(Number(e.target.value));
          }}
          type="min"
        />
      </div>
      <div className="cp__label-head">
        <span className="cp__label-field cp__head-label">Количество читаемых параллельно книг</span>
        <CustomFieldCreatePlan
          className="cp__min-field--one"
          value={countParallelChapter}
          onChange={e => {
            const v = Number(e.target.value);
            if (isNaN(v)) {
              return;
            }
            if (v > 3) {
              return;
            }
            onChangeParallel(Number(e.target.value));
          }}
          type="min"
        />
      </div>
    </div>
  );
}
