import React from 'react';
import PropTypes from 'prop-types';
import './index.scss';
import cn from 'classnames';

function CustomFieldCreatePlan({ value, disabled, onChange, type, className }) {
  switch (type) {
    case 'textarea':
      return (
        <textarea
          type="text"
          disabled={disabled}
          value={value}
          onChange={onChange}
          className={cn('cp__field', className)}
        />
      );
    case 'min':
      return (
        <input
          type="text"
          value={value}
          disabled={disabled}
          onChange={onChange}
          className={cn('cp__min-field', className)}
        />
      );
    default:
      return (
        <input
          type="text"
          value={value}
          onChange={onChange}
          disabled={disabled}
          className={cn('cp__field', className)}
        />
      );
  }
}

CustomFieldCreatePlan.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  type: PropTypes.string,
  className: PropTypes.string,
};

export default CustomFieldCreatePlan;
