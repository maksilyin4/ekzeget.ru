import React, { useState } from 'react';
import './index.scss';
import CustomButtonCP from '../CustomButtonCp';
import cn from 'classnames';
import { useUpdateEffect } from 'react-use';

export default function BooksAccordion({
  books,
  countBranches,
  changeName,
  disabled,
  setFieldValue,
  activeBooks,
}) {
  const [isOpen, setIsOpen] = useState(disabled);
  const [activeCountBranches, setActiveCountBranches] = useState(countBranches);

  useUpdateEffect(() => {
    if (countBranches) {
      setActiveCountBranches(countBranches);
      setFieldValue(changeName, {
        branch1: [],
        branch2: [],
        branch3: [],
      });
    }
  }, [countBranches]);

  const isIncludeBook = (book, branch) => {
    return activeBooks[`branch${branch}`]?.map(e => e.id).includes(book.id);
  };

  const handleClick = (book, indexBranch) => {
    const numB = indexBranch + 1;
    const filterBranch = num => activeBooks[`branch${num}`].filter(elem => elem.id !== book.id);

    const checkBranchAndApplyValue = num => {
      if (num === numB) {
        return [...activeBooks[`branch${numB}`], book];
      }
      return filterBranch(num);
    };

    if (isIncludeBook(book, numB)) {
      setFieldValue(changeName, { ...activeBooks, [`branch${numB}`]: filterBranch(numB) });
    } else {
      setFieldValue(changeName, {
        branch1: checkBranchAndApplyValue(1),
        branch2: checkBranchAndApplyValue(2),
        branch3: checkBranchAndApplyValue(3),
      });
    }
  };

  return (
    <>
      <div
        onClick={() => setIsOpen(!isOpen)}
        className={cn(
          'cp__block-head--yellow',
          !isOpen && 'cp__block-head--gray',
          disabled && 'cp__block-disabled',
        )}>
        <h3 className="cp-ac__title">Выбрать книги и распределить по параллельным веткам</h3>
        <svg
          className={cn('cp__arrow-accordion', isOpen && 'cp__arrow-accordion-open')}
          xmlns="http://www.w3.org/2000/svg"
          width="24"
          height="13"
          viewBox="0 0 24 13"
          fill="none">
          <path d="M23 12L11.6452 2L1 12" stroke="black" strokeWidth="2" />
        </svg>
      </div>
      {isOpen && (
        <>
          <div data-cols={activeCountBranches} className="cp__block-content">
            {new Array(activeCountBranches).fill(<></>).map((_, indexBranch) => (
              <div className="cp__branch-1">
                <div className="cp__branch-title">Ветка {indexBranch + 1}</div>
                <div className="cp__chapters-wrapper ">
                  {books?.books?.map(book => (
                    <>
                      <CustomButtonCP
                        disabled={disabled}
                        size="s"
                        className="cp__chapters-btn"
                        color={isIncludeBook(book, indexBranch + 1) ? 'primary' : 'gray'}
                        onClick={() => handleClick(book, indexBranch)}
                        key={book.id}>
                        {book.short_title}
                      </CustomButtonCP>
                    </>
                  ))}
                </div>
              </div>
            ))}
          </div>
        </>
      )}
    </>
  );
}
