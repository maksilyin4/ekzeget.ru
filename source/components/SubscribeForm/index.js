import React, { Component } from 'react';
import './styles.scss';

export default class SubscribeForm extends Component {
  state = {
    email: '',
  };

  handleSubmit = event => {
    event.preventDefault();
  };

  handleInput = event => {
    const { name, value } = event.currentTarget;
    this.setState({
      [name]: value,
    });
  };

  render() {
    const { email } = this.state;
    return (
      <div className="subscribe">
        <form className="subscribe__form" onSubmit={this.handleSubmit}>
          {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
          <label htmlFor="subscribe-email">Подпишитесь на новости и обновления</label>
          <div className="subscribe__form__input">
            <input
              type="email"
              name="email"
              id="subscribe-email"
              value={email}
              onChange={this.handleInput}
              autoComplete="off"
              required
            />
            {/* eslint-disable-next-line react/no-unescaped-entities */}
            <button type="submit">></button>
          </div>
        </form>
      </div>
    );
  }
}
