import React from 'react';
import Transition from 'react-transition-group/Transition';
import cx from 'classnames';

class AnimateLiftRight extends React.Component {
  defaultStyle = {
    transition: 'transform 300ms ease-in-out',
    transform: 'translateX(-100%)',
  };

  transitionStyles = {
    entering: { transform: 'translateX(-100%)' },
    entered: { transform: 'translateX(0)' },
  };

  render() {
    const { show, timeout, children, className } = this.props;
    return (
      <Transition in={show} timeout={timeout} mountOnEnter unmountOnExit>
        {state => (
          <div
            className={cx(!!className && className)}
            style={{
              ...this.defaultStyle,
              ...this.transitionStyles[state],
            }}>
            {children}
          </div>
        )}
      </Transition>
    );
  }
}

AnimateLiftRight.defaultProps = {
  timeout: {
    enter: 100,
    exit: 300,
  },
};

export default AnimateLiftRight;
