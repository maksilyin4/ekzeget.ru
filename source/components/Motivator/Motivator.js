import React, { Component } from 'react';
import Icon from '../Icons/Icons';
import LightBox from './MotivatorLightBox';

import './motivator.scss';
import { urlToAPI } from '../../dist/browserUtils';

class Motivator extends Component {
  state = {
    url: urlToAPI,
    isOpen: false,
  };

  getFullWidth = () => {
    this.setState({
      isOpen: true,
    });
  };

  getFullWidthByImage = () => {
    if (!this.props.screen.phone) {
      this.setState({
        isOpen: true,
      });
    }
  };

  closeLightbox = () => {
    this.setState({
      isOpen: false,
    });
  };

  render() {
    const { images } = this.props;
    const { url, isOpen } = this.state;
    return (
      <div>
        {images.small !== null && images.small.path.length > 0 && (
          <div>
            <div className="motivator__actions">
              <button className="action action__search" onClick={this.getFullWidth}>
                <Icon icon="search" />
              </button>
              <a
                href={`${url}${images.big.path}`}
                download
                target="_blank"
                rel="noopener noreferrer">
                <button className="action">
                  <Icon icon="download" />
                </button>
              </a>
            </div>
            <li
              onClick={this.getFullWidthByImage}
              className="motivator"
              style={{
                backgroundImage: `url(${url}${images.small.path})`,
                width: `${images.small.width}px`,
                height: `${images.small.height}px`,
              }}
            />
          </div>
        )}
        {isOpen && <LightBox urlTest={url} onClose={this.closeLightbox} image={images.big.path} />}
      </div>
    );
  }
}

export default Motivator;
