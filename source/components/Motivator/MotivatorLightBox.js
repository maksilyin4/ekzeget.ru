import React, { Component } from 'react';
import { Lightbox } from 'react-modal-image';

class LightBox extends Component {
  render() {
    const { urlTest, onClose, image } = this.props;
    return <Lightbox large={`${urlTest}${image}`} hideZoom onClose={onClose} />;
  }
}

export default LightBox;
