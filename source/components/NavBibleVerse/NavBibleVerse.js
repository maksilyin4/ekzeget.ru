import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Icon from '../Icons/Icons';
// import { AXIOS } from '../../dist/ApiConfig';

class NavBibleVerse extends Component {
  static propTypes = {
    // bookShort: PropTypes.string,
    chapterNumber: PropTypes.number,
    currentVerse: PropTypes.number,
    chapterLength: PropTypes.number,
  };

  render() {
    const { currentVerse, activeBook, chapterNumber, chapterLength } = this.props;

    return (
      <div className="bible__nav">
        {currentVerse - 1 !== 0 && (
          <Link
            to={`/bible/${activeBook}/glava-${chapterNumber}/stih-${currentVerse - 1}/`}
            className="bible__nav-item bible__nav-item_prev">
            <Icon icon="angle-left" />
            {`Стих ${currentVerse - 1}`}
          </Link>
        )}
        {currentVerse + 1 <= chapterLength && (
          <Link
            to={`/bible/${activeBook}/glava-${chapterNumber}/stih-${currentVerse + 1}/`}
            className="bible__nav-item bible__nav-item_next">
            {`Стих ${currentVerse + 1}`}
            <Icon icon="angle-right" />
          </Link>
        )}
      </div>
    );
  }
}

export default NavBibleVerse;
