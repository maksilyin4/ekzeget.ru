import React from 'react';
import ReadingProgressBar from '~components/ReadingProgressBar/ReadingProgressBar';
import './styles.scss';
import { findCurrentReadingDay } from '~components/CustomPlanModule/utils/getActualCurrentDay';
import { percent } from '~components/PlanAddUsers/table';
import useProgressPlan from '../../hooks/useProgressPlan';

const Item = ({ name, count }) => {
  return (
    <div className="stats__item">
      <span>{name}</span>
      <span>{count}</span>
    </div>
  );
};

export default function MyPlanResult({ userPlans, stats }) {
  const { closedDays, remainderDays } = useProgressPlan();
  const currentPlanDay = findCurrentReadingDay(userPlans);
  const passDays = closedDays.length;
  const quizzes = stats?.quizzes;

  const getStats = d => (quizzes ? d : 0);

  return (
    <div className="stats__wrapper">
      <ReadingProgressBar userPlans={userPlans} className="stats-progress" withTop />
      <h4 className="stats__title">Личные результаты</h4>
      <div className="stats__container">
        <Item name={'Пройдено дней по плану всего'} count={getStats(currentPlanDay)} />
        <Item name={'Вы прошли  дней'} count={getStats(passDays)} />
        <Item name={'Отставание в днях'} count={getStats(remainderDays.length)} />
        <Item name={'Отвечено вопросов Вами'} count={percent(quizzes?.allTime?.completedPercent)} />
        <Item name={'Верных ответов'} count={percent(quizzes?.allTime?.accuracy)} />
        <Item name={'Комментариев всего'} count={getStats(stats?.comments?.total)} />
      </div>
    </div>
  );
}
