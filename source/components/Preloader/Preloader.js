import React from 'react';
import cx from 'classnames';
import './preloader.scss';

const Preloader = props => (
  <div className={cx('preloader', props.className)}>
    <div className="preloader__icon">
      <img src="/frontassets/images/icons/preloader.svg" alt="" />
    </div>
  </div>
);

export default React.memo(Preloader);
