import React, { PureComponent } from 'react';
import LIBody from './LIBody';
import getLatestInterpretations from './serviceAPI';

import './latestInterpritations.scss';

class LatestInterpritations extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isFetching: false,
      list: this.props.latestInterpretationsList || [],
    };
  }

  componentDidMount() {
    if (this.state.list > 0) return;
    this.setState({ isFetching: true });
    if (!this.state.list.length) {
      getLatestInterpretations().then(data => this.setState({ ...data }));
    }
  }

  render() {
    const { list, isFetching } = this.state;

    return (
      <div className="latest-interpritations">
        <LIBody isFetching={isFetching} list={list} />
      </div>
    );
  }
}

export default LatestInterpritations;
