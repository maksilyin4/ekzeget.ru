import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import Preloader from '../Preloader/Preloader';

export default class LIBody extends PureComponent {
  render() {
    const { isFetching, list } = this.props;

    const renderPreloader = () =>
      isFetching ? (
        <div className="news__preloader">
          <Preloader />
        </div>
      ) : (
        <p className="empty-content">Толкования еще не добавлялись</p>
      );

    return (
      <div className="latest-interpritations__body">
        <div className="latest-interpritations__head">
          <h2 className="section__title">Толкование Библии</h2>
        </div>
        {list.length === 0 && renderPreloader()}
        {list.length > 0 && (
          <div className="latest-interpritations__list">
            {list.map(({ id, verse, author, date, username, code }) => (
              // investigated,
              <div key={id} className="latest-interpritations__item">
                <div className="latest-interpritations__item-link">
                  <Link
                    to={`/bible/${verse.book_code}/glava-${verse?.chapter}/stih-${verse?.number}/${code}/`}>
                    <span className="latest-interpritations__item-verse">{verse?.short}</span>
                    <span className="latest-interpritations__item-author">{author}</span>
                  </Link>
                </div>
                <div className="latest-interpritations__item-added">
                  Добавлено
                  <span className="latest-interpritations__item-date">{` ${date} `}</span>
                  пользователем
                  <span className="latest-interpritations__item-username">{` ${username} `}</span>
                </div>
              </div>
            ))}
          </div>
        )}
      </div>
    );
  }
}
