import React from 'react';
import Loadable from 'react-loadable';
import Preloader from '../Preloader/Preloader';

const LoadableBar = Loadable({
  loader: () => import('./LatestInterpritations'),
  loading() {
    return (
      <div>
        <Preloader />
      </div>
    );
  },
  render(loaded) {
    const Component = loaded.default;
    return <Component />;
  },
  delay: 300,
});

export default LoadableBar;
