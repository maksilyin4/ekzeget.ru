import R from 'ramda';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Select from 'react-select';
import exegetInterpritationsActions from '../../dist/actions/ExegetInterpritations';
import {
  getExegetInterpritationsIsLoading,
  getExegetInterpritations,
} from '../../dist/selectors/ExegetInterpritations';
import Preloader from '../Preloader/Preloader';
import CollectionList, { CollectionItem } from '../Collections/CollectionList/CollectionList';
import Pagination from '../Pagination/Pagination';
import { declOfNumStr } from '../../dist/utils';
import { toolbarConfig } from './toolbarConfig';

import './exegetList.scss';

class ExegetList extends Component {
  static propTypes = {
    interpritations: PropTypes.object,
    exegetId: PropTypes.number,
    query: PropTypes.string,
  };

  constructor(props) {
    super(props);

    this.state = {
      perPage: { value: 20, label: 20 },
    };
  }

  componentDidMount() {
    this.props.getExegetInterpritations(
      this.props.exegetId,
      `?q=${this.props.query}&per-page=${this.state.perPage.value}`,
    );
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.query !== this.props.query) {
      this.props.getExegetInterpritations(
        this.props.exegetId,
        `?q=${this.props.query}&per-page=${this.state.perPage.value}`,
      );
    }
    if (prevState.perPage !== this.state.perPage) {
      this.props.getExegetInterpritations(
        this.props.exegetId,
        `?q=${this.props.query}&per-page=${this.state.perPage.value}`,
      );
    }
  }

  _changePage = page => {
    this.props.getExegetInterpritations(
      this.props.exegetId,
      `?page=${page}&per-page=${this.state.perPage.value}&q=${this.props.query}`,
    );
  };

  _handleSelect = e => {
    this.setState({ perPage: e });
  };

  render() {
    const { interpritations, code } = this.props;

    return Object.keys(interpritations).length === 0 ? (
      <Preloader />
    ) : (
      <div className="exeget__list">
        <div className="exeget__list-head">
          <div className="exeget__list-count">
            {`Найдено ${declOfNumStr(R.pathOr(0, ['pages', 'totalCount'], interpritations), [
              'толкование',
              'толкования',
              'толкований',
            ])} экзегета`}
          </div>
          <div className="exeget__list-pager">
            <span>На странице</span>
            <Select
              isSearchable={false}
              options={[
                { value: 20, label: 20 },
                { value: 40, label: 40 },
                { value: 60, label: 60 },
              ]}
              clearable={false}
              onChange={this._handleSelect}
              value={this.state.perPage}
              simpleValue
              toolbarConfig={toolbarConfig}
              classNamePrefix="ekzeget"
            />
          </div>
        </div>
        <CollectionList>
          {interpritations &&
            interpritations.verse.map(verse => (
              <CollectionItem
                key={verse.id}
                href={`/bible/${verse.book_code}/glava-${verse.chapter}/stih-${verse.number}/${code}`}
                title={`${verse.short}`}
                mod="underline"
              />
            ))}
        </CollectionList>
        {Math.ceil(interpritations.pages.totalCount / this.state.perPage.value) > 1 && (
          <Pagination
            page={interpritations.pages.currentPage}
            count={this.state.perPage.value}
            totalCount={interpritations.pages.totalCount}
            onClick={this._changePage}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: getExegetInterpritationsIsLoading(state),
  interpritations: getExegetInterpritations(state),
});

const mapDispatchToProps = { ...exegetInterpritationsActions };

export default connect(mapStateToProps, mapDispatchToProps)(ExegetList);
