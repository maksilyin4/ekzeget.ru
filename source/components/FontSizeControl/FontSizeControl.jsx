import React, { Component } from 'react';

import Icon from '~components/Icons/Icons';

import { LS } from '../../dist/browserUtils';
import {
  defaultreadingfontsize as defaultReadingFontSize,
  defaultrootfontsize as defaultRootFontSize,
  maxrootfontsize as maxRootFontSize,
  deltarootfontsize as deltaRootFontSize,
  deltareadingfontsize as deltaReadingFontSize,
} from './FontSizeControl.scss';

class FontSizeControl extends Component {
  componentDidMount() {
    this.setRootFontSize(+LS.get('rootFontSize') || parseFloat(defaultRootFontSize));

    if (!FontSizeControl.stylesheet) {
      FontSizeControl.stylesheet = this.createStylesheet();
      this.setReadingFontSize(+LS.get('readingFontSize') || parseFloat(defaultReadingFontSize));
    }
  }

  createStylesheet = () => {
    const style = document.createElement('style');
    style.appendChild(document.createTextNode(''));
    document.head.appendChild(style);

    style.sheet.insertRule(
      `
                [class*="content-area"], 
                [class*="text-area"],
                [class*="content__paper"],
                [class*="__content-text"],
                [class*="__desc"],
                [class*="item__dopInfo"],
                .font-zoomable {}`,
      0,
    );
    return style.sheet;
  };

  setRootFontSize = fontSize => {
    document.documentElement.style.fontSize = `${fontSize}%`;
    LS.set('rootFontSize', fontSize);
  };

  setReadingFontSize = fontSize => {
    FontSizeControl.stylesheet.cssRules[0].style.fontSize = `${fontSize}rem`;
    LS.set('readingFontSize', fontSize);
  };

  increment = () => {
    if (+LS.get('rootFontSize') < parseFloat(maxRootFontSize)) {
      this.setRootFontSize(+LS.get('rootFontSize') + +deltaRootFontSize);
    } else {
      this.setReadingFontSize(+LS.get('readingFontSize') + +deltaReadingFontSize);
    }
  };

  decrement = () => {
    if (+LS.get('readingFontSize') > parseFloat(defaultReadingFontSize)) {
      const readingFontSize = +LS.get('readingFontSize');
      this.setReadingFontSize(readingFontSize - +deltaReadingFontSize);
    } else {
      const rootFontSize = +LS.get('rootFontSize');
      this.setRootFontSize(rootFontSize - +deltaRootFontSize);
    }
  };

  render() {
    return (
      <div className="font-resizer">
        <div onClick={this.decrement} className="font-resizer__icon-container">
          <Icon icon="button-decrease-font" title="Уменьшить размер шрифта" />
        </div>
        <div onClick={this.increment} className="font-resizer__icon-container">
          <Icon icon="button-increase-font" title="Увеличить размер шрифта" />
        </div>
      </div>
    );
  }
}

export default FontSizeControl;
