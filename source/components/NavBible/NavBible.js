import React, { PureComponent } from 'react';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Icon from '../Icons/Icons';

class NavBible extends PureComponent {
  static propTypes = {
    chaptersCount: PropTypes.number,
    currentPage: PropTypes.number,
    activeBook: PropTypes.string,
  };

  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
    };
  }

  _toggleList = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  render() {
    const { chaptersCount, currentPage, activeBook, location } = this.props;
    const intNumberPage = Number(currentPage);
    return (
      <div className="bible__nav">
        {intNumberPage - 1 !== 0 && (
          <Link
            to={`/bible/${activeBook}/glava-${intNumberPage - 1}/${location.search}`}
            className="bible__nav-item bible__nav-item_prev">
            <Icon icon="angle-left" />
            {`Глава ${intNumberPage - 1}`}
          </Link>
        )}
        {intNumberPage + 1 <= chaptersCount && (
          <Link
            to={`/bible/${activeBook}/glava-${intNumberPage + 1}/${location.search}`}
            className="bible__nav-item bible__nav-item_next">
            {`Глава ${intNumberPage + 1}`}
            <Icon icon="angle-right" />
          </Link>
        )}
      </div>
    );
  }
}

export default withRouter(NavBible);
