import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import dayjs from 'dayjs';
import ruLocale from 'dayjs/locale/ru';
import loadable from '@loadable/component';

import getLIDAction from '../../dist/actions/LectionInDay';
import { getLIDIsLoading, getLID } from '../../dist/selectors/LectionInDay';
import { getCurrentTranslateCode } from '../../dist/selectors/translates';
import { getIsMobile } from '../../dist/selectors/screen';
import Tabs, { Tab, TabContent } from '../Tabs/Tabs';
import Preloader from '../Preloader/Preloader';
import Icon from '../Icons/Icons';
import DatePickerCustom from './DatePicker';
import calendarIcon from './calendar.svg';

import './lection-in-day.scss';
import LectionInCurrent from './LectionInCurrent';
import useDailyMediasQueryVideo from '../../apolloClient/query/mediaLibrary/mediasDailyVideo';

const formatDate = date => {
  let dd = date.getDate();
  if (dd < 10) dd = `0${dd}`;

  let mm = date.getMonth() + 1;
  if (mm < 10) mm = `0${mm}`;

  const yy = date.getFullYear();
  return `${dd}.${mm}.${yy}`;
};

const ExternalVideo = loadable(() => import('~components/ExternalVideo'), { ssr: false })

const LectionInDay = () => {
  const dispatch = useDispatch();

  const lidIsLoading = useSelector(state => getLIDIsLoading(state));
  const { reading } = useSelector(state => getLID(state));
  const isMobile = useSelector(state => getIsMobile(state));
  const currentCode = useSelector(state => getCurrentTranslateCode(state));

  // detect if all tabs are present; if so, make adjustments to fit them
  const areAllTabsPresent = reading.gospel_reading && reading.apostolic_reading && reading.morning_reading && reading.more_reading;

  const [getDate, setGetDate] = useState(dayjs().format('DD.MM.YYYY'));
  const [visualDate, setVisualDate] = useState(
    dayjs()
      .locale('ru', ruLocale)
      .format('DD MMMM YYYY'),
  );
  const [activeContent, setActiveContent] = useState(
    reading.title.length > 0 ? getActiveContent() : null,
  );

  function getActiveContent() {
    const { gospel_reading, apostolic_reading, morning_reading, more_reading } = reading;

    if (gospel_reading !== '') return 1;
    if (gospel_reading === '' && apostolic_reading !== '') return 2;
    if (gospel_reading === '' && apostolic_reading === '' && morning_reading !== '') {
      return 3;
    }
    if (
      gospel_reading === '' &&
      apostolic_reading === '' &&
      morning_reading === '' &&
      more_reading !== ''
    ) {
      return 4;
    }
    return null;
  }

  const _handleChange = date => {
    const day = dayjs(date);
    const dayFormat = day.format('DD.MM.YYYY');
    const dayRevFormat = day.format('YYYY-MM-DD');
    setVisualDate(day.locale('ru', ruLocale).format('DD MMMM YYYY'));
    setGetDate(dayFormat);
    dispatch(getLIDAction(dayFormat)).then(() => {
      setActiveContent(reading.title.length > 0 ? getActiveContent() : null);
    });
    refetch({
      daily_media: dayRevFormat
        .split('.')
        .reverse()
        .join('-'),
    });
  };

  const nextDay = () => {
    let date = getDate.split('.');
    date = new Date(Number(date[2]), Number(date[1]) - 1, Number(date[0]));
    date.setDate(date.getDate() + 1);
    const newDate = formatDate(date);
    setGetDate(newDate);
    setVisualDate(
      dayjs(date)
        .locale('ru', ruLocale)
        .format('DD MMMM YYYY'),
    );
    dispatch(getLIDAction(newDate)).then(() => {
      setActiveContent(reading.title.length > 0 ? getActiveContent() : null);
    });
    refetch({
      daily_media: newDate
        .split('.')
        .reverse()
        .join('-'),
    });
  };

  const prevDay = () => {
    let date = getDate.split('.');
    date = new Date(Number(date[2]), Number(date[1]) - 1, Number(date[0]));
    date.setDate(date.getDate() - 1);
    const newDate = formatDate(date);
    setGetDate(newDate);
    setVisualDate(
      dayjs(date)
        .locale('ru', ruLocale)
        .format('DD MMMM YYYY'),
    );
    dispatch(getLIDAction(newDate)).then(() => {
      setActiveContent(reading.title.length > 0 ? getActiveContent() : null);
    });
    refetch({
      daily_media: newDate
        .split('.')
        .reverse()
        .join('-'),
    });
  };

  function _renderCalendarButton() {
    return (
      <div className="reading-ctrl__calendar" id="js-reading-day-calendar">
        <img src={calendarIcon} alt="Календарь" className="reading-ctrl__icon" />
      </div>
    );
  }

  useEffect(() => {
    dispatch(getLIDAction(getDate));
  }, []);

  const { medias = [], refetch } = useDailyMediasQueryVideo({
    daily_media: getDate
      .split('.')
      .reverse()
      .join('-'),
  });

  return (
    <div className="container">
      <header className="level-header">
        <div className="level-header__title">
          <h2 className="level-title">Чтение Библии на каждый день</h2>
          {isMobile && (
            <div className="reading-header__datepicker">
              <DatePickerCustom calendarIcon={_renderCalendarButton()} onChange={_handleChange} />
            </div>
          )}
        </div>
        <div className="level-lead">
          {visualDate}
          {lidIsLoading ? (
            <Preloader />
          ) : (
            <>
              {reading.title.map((title, i) => (
                <p key={i}>{title}</p>
              ))}
              <div className="reading-ctrl">
                <button
                  className="reading-ctrl__prev"
                  id="js-reading-day-prev"
                  onClick={prevDay}
                  aria-label="Previous">
                  <Icon icon="angle-left" />
                </button>
                <button
                  className="reading-ctrl__next"
                  id="js-reading-day-next"
                  onClick={nextDay}
                  aria-label="Next">
                  <Icon icon="angle-right" />
                </button>
                {!isMobile && (
                  <div className="reading-ctrl__datepicker">
                    <DatePickerCustom
                      calendarIcon={_renderCalendarButton()}
                      // minDate={startDate}
                      onChange={_handleChange}
                    />
                  </div>
                )}
              </div>
            </>
          )}
        </div>
      </header>
      <div className="reading">
        {lidIsLoading && reading !== undefined ? (
          <Preloader />
        ) : (
          <>
            {medias.length && medias[0]?.video_href ? (
              <ExternalVideo data={medias[0]?.video_href} />
            ) : null}
            <Tabs
              activeTab={activeContent}
              callBack={data => setActiveContent(data)}
              className="reading-tabs">
              {reading.gospel_reading && <Tab label="Евангелие" value={1} mod="big" />}
              {reading.apostolic_reading && <Tab label="Апостол" value={2} mod="big" />}
              {reading.morning_reading && <Tab label="Утреня" value={3} mod="big" />}
              {!areAllTabsPresent && reading.more_reading && <Tab label="Дополнительные" value={4} mod="big" />}
              {areAllTabsPresent  && <Tab label="Доп." value={4} mod="big" />}

              {activeContent === 1 && reading.gospel_reading && (
                <TabContent>
                  <LectionInCurrent data={reading.gospel_reading} currentCode={currentCode} />
                </TabContent>
              )}
              {activeContent === 2 && reading.apostolic_reading && (
                <TabContent>
                  <LectionInCurrent data={reading.apostolic_reading} currentCode={currentCode} />
                </TabContent>
              )}
              {activeContent === 3 && reading.morning_reading && (
                <TabContent>
                  <LectionInCurrent data={reading.morning_reading} currentCode={currentCode} />
                </TabContent>
              )}
              {activeContent === 4 && reading.more_reading && (
                <TabContent>
                  <LectionInCurrent data={reading.more_reading} currentCode={currentCode} />
                </TabContent>
              )}
            </Tabs>
          </>
        )}
      </div>
    </div>
  );
};

export default LectionInDay;
