import React, { PureComponent } from 'react';
import LectionInDayReading from './LectionInDayReading';

class LectionInCurrent extends PureComponent {
  render() {
    const { data = [], currentCode } = this.props;
    return (
      <div className="lectioninday-content">
        {data !== null ? (
          data.map(({ title, info, verse }) => (
            <LectionInDayReading
              key={title}
              title={title}
              info={info}
              verse={verse}
              currentCode={currentCode}
            />
          ))
        ) : (
          <p className="reading__group-empty">Текущее чтение отсутствует</p>
        )}
      </div>
    );
  }
}

export default LectionInCurrent;
