import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import cx from 'classnames';

class LectionInDayReading extends PureComponent {
  state = {
    showDesc: false,
  };

  toggleActive = () => {
    this.setState({ showDesc: !this.state.showDesc });
  };

  render() {
    const { title, info, verse, currentCode } = this.props;
    return (
      <div className="reading__group">
        <div className="reading__group-head">
          <Link
            to={`/bible/${info.book_code}/glava-${info.chapter}/?verse=${info.verses.match(
              /\d+:[0-9: ,–-]+/,
            )}`}
            className="reading__group-link">
            {title}
          </Link>
        </div>
        <div
          className={cx('reading__text', {
            active: this.state.showDesc,
          })}>
          {verse.length > 5 && !this.state.showDesc
            ? verse.slice(0, 5).map(item => (
                <span key={item.id} className="chapter__verse-home">
                  <sup className="chapter__verse-number">{item.number}</sup>
                  <span
                    className={cx('chapter__verse-desc', {
                      csya: currentCode === 'csya_old',
                      greek: currentCode === 'grek',
                    })}
                    dangerouslySetInnerHTML={{ __html: item.text }}
                  />
                </span>
              ))
            : verse.map(item => (
                <span key={item.id} className="chapter__verse-home">
                  <sup className="chapter__verse-number">{item.number}</sup>
                  <span
                    className={cx('chapter__verse-desc', {
                      csya: currentCode === 'csya_old',
                      greek: currentCode === 'grek',
                    })}
                    dangerouslySetInnerHTML={{ __html: item.text }}
                  />
                </span>
              ))}
        </div>
        {verse.length > 5 && (
          <button className="page__desc-toggle text_underline" onClick={this.toggleActive}>
            {this.state.showDesc ? 'Скрыть описание' : 'Читать полностью'}
          </button>
        )}
      </div>
    );
  }
}

export default LectionInDayReading;
