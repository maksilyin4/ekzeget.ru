import React from 'react';
import Loadable from 'react-loadable';
import Preloader from '../../Preloader/Preloader';

const LoadableBar = Loadable({
  loader: () => import('./date-picker'),
  loading() {
    return (
      <div>
        <Preloader />
      </div>
    );
  },
  delay: 300,
});

export default LoadableBar;
