import React from 'react';
import DatePicker from 'react-date-picker';
import './style.scss';

const DatePickerCustom = ({ calendarIcon, minDate, onChange }) => {
  return (
    <DatePicker
      className="reading__datePicker"
      calendarIcon={calendarIcon}
      calendarClassName="lectionsinday__datapicker"
      minDate={minDate}
      onChange={onChange}
      locale="RU"
    />
  );
};

export default DatePickerCustom;
