import React, { useState, useEffect, useMemo, useCallback } from 'react';
import PropTypes from 'prop-types';

import QuizButton from './QuizButton';

const QuizTestament = ({
  title,
  testaments,
  books,
  setBooksArr,
  isActiveDefault = false,
  testamentName,
}) => {
  const [isActiveAll, changeActiveTestaments] = useState(isActiveDefault);

  // Используем для форсе дизейбла всех кнопок при дизейбле лейбла
  // с чекбоксом Все
  const [isForceDisableTestaments, setForceTestaments] = useState(false);

  useEffect(() => {
    // Для проверки на наличие выбранных книг при isActiveDefault
    if (isActiveAll) {
      const testamentIdArr = testaments.map(({ id }) => id);
      setBooksArr(testamentIdArr);
    }
  }, []);

  // Для аптейда только при одной книге
  const getIsCheckedBook = useCallback(
    (bookId, isChecked) => {
      let filteredBooks = null;

      if (isChecked) {
        setBooksArr([...books, bookId]);
        setForceTestaments(false);
      } else {
        filteredBooks = books.filter(item => item !== bookId);
        setBooksArr(filteredBooks);
      }
    },
    [books, setBooksArr],
  );

  // Два условия:
  // 1) При лейбле Все
  // 2) При клике по книге, когда Все чекбокс активный
  const getUpdateAllBooks = useCallback(
    (e, isActiveUnique, bookId) => {
      let filteredTestament = null;
      let testamentIdArr = testaments.map(({ id }) => id);

      changeActiveTestaments(!isActiveAll);

      if (!isActiveAll) {
        // Для удаления дубликатов
        setBooksArr([...new Set([...books, ...testamentIdArr])]);
        setForceTestaments(true);
      } else {
        // Если чекбокс Все неактивный, то мы чистим массив для нужного завета
        filteredTestament = books.filter(item => !testamentIdArr.includes(item));

        // Если обыл 2 сценарий
        if (isActiveUnique) {
          testamentIdArr = testamentIdArr.filter(item => item === bookId);
          filteredTestament = [...filteredTestament, ...testamentIdArr];
        }

        setBooksArr(filteredTestament);
      }
    },
    [books, isActiveAll, setBooksArr, testaments],
  );

  const renderQuizButtons = useMemo(
    () =>
      testaments.map(testament => (
        <QuizButton
          key={testament.id}
          id={testament.id}
          getIsCheckedBook={getIsCheckedBook}
          testamentTitle={testament.menu}
          isActiveAll={isActiveAll}
          getUpdateAllBooks={getUpdateAllBooks}
          isForceDisableTestaments={isForceDisableTestaments}
        />
      )),
    [testaments, getIsCheckedBook, isActiveAll, getUpdateAllBooks, isForceDisableTestaments],
  );

  if (testaments.length === 0) {
    return null;
  }

  return (
    <div className="quiz-testament__container">
      <div className="quiz-testament__head">
        <h2 className="quiz-testament__title"> {title} </h2>
        <label className="container-label" htmlFor={`quiz-testament-${testamentName}`}>
          Все
          <input
            type="checkbox"
            checked={isActiveAll}
            onChange={getUpdateAllBooks}
            id={`quiz-testament-${testamentName}`}
          />
          <span className="checkmark" />
        </label>
      </div>
      <ul className="quiz-testaments__btns">{renderQuizButtons}</ul>
    </div>
  );
};

QuizTestament.defaultProps = {
  title: 'Завет',
};

QuizTestament.propTypes = {
  title: PropTypes.string,
  testaments: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      menu: PropTypes.string,
    }),
  ),
  books: PropTypes.array,
  setBooksArr: PropTypes.func,
  isActiveDefault: PropTypes.bool,
  testamentName: PropTypes.string,
};

export default QuizTestament;
