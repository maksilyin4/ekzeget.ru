import React, { useState, useEffect } from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';

const QuizButton = ({
  testamentTitle,
  isActiveAll,
  getIsCheckedBook,
  id,
  getUpdateAllBooks,
  isForceDisableTestaments,
}) => {
  const [isActive, changeActiveState] = useState(false);

  useEffect(() => {
    if (isForceDisableTestaments) {
      changeActiveState(false);
    }
  }, [isForceDisableTestaments]);

  const getUpdateButton = () => {
    changeActiveState(!isActive);

    // Если взаимодействуем с чекбоксом Все
    if (isActiveAll) {
      getUpdateAllBooks(null, !isActive, id);
    } else {
      getIsCheckedBook(id, !isActive);
    }
  };

  return (
    <li className="quiz-testament__item">
      <button
        className={cx('btn quiz-testament__btn', {
          'quiz-testament__btn--active': isActive || isActiveAll,
        })}
        onClick={getUpdateButton}>
        {testamentTitle}
      </button>
    </li>
  );
};

QuizButton.defaultProps = {
  testamentTitle: 'Библейская книга',
  isForceDisableTestaments: false,
};

QuizButton.propTypes = {
  testamentTitle: PropTypes.string,
  isActiveAll: PropTypes.bool,
  getIsCheckedBook: PropTypes.func,
  id: PropTypes.number,
  getUpdateAllBooks: PropTypes.func,
  isForceDisableTestaments: PropTypes.bool,
};

export default QuizButton;
