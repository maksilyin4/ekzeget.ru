import R from 'ramda';
import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import cx from 'classnames';

import { createQuiz } from '../../dist/quizAPI';

import {
  getNewBooksList,
  getOldBooksList,
  getBookListIsLoading,
} from '../../store/booksList/selectors';
import { getLatestQuestion } from '../../dist/selectors/Quiz';

import { getBooksList } from '../../store/booksList/actions';
import { getCurrentQueeze } from '../../dist/actions/Quiz';

import Preloader from '../../components/Preloader/Preloader';
import QuizTestament from './QuizTestament';
import QuizLink from '../QuizUI/QuizLink';
import { DESC_QUIZ, TITLE_QUIZ } from '../../pages/Quiz/constants';
import { smoothScrollTo } from '../../dist/utils';
import { useGetParams } from 'src/hooks/useGetParams';

const QuizTestamentContainer = ({
  oldTestament,
  newTestament,
  getBooksList,
  isLoadingBookList,
  getCurrentQueeze,
  history: { push },
  latestQuestion,
}) => {
  // POST принимает только 0/1
  const [isInterpretation, setBoolInterpretation] = useState(0);
  const [isRelatedToText, setBoolText] = useState(1);
  const [books, setBooksArr] = useState([]);
  const [isCheckedText, setCheckedText] = useState(true);
  const [isCheckedInterpretation, setCheckedInterpretation] = useState(false);
  const [isExistQuiz, setStatusQuiz] = useState(null);
  const [arrOfWarnings, updateWarningMsg] = useState([]);
  const [isLoadQuiz, updateLoadQuiz] = useState(false);

  const bibleParams = useGetParams();

  // Did mount with []
  useEffect(() => {
    if (bibleParams) {
      getInitQuiz();
    }

    // Checking for SSR
    if (!oldTestament.length && !newTestament.length) {
      getBooksList();
    }

    // Если уже есть созданная викторина, заносим данные в стейт менеджер и отображаем кнопку
    getCurrentQueeze(false, []).then(res => {
      const status = R.pathOr(null, ['error', 'status'], res);
      setStatusQuiz(status !== 404);
    });

    smoothScrollTo();
  }, []);

  const getInitQuiz = () => {
    let paramBooks;
    let paramText;
    let paramInterpretation;
    let paramChapterId;
    let paramVerseId;
    let paramQuestionId;
    updateLoadQuiz(true);

    if (bibleParams) {
      paramBooks = bibleParams.book_ids.map((item, index) => `&book_ids[${index}]=${item}`);
      paramText = `?is_related_to_text=${bibleParams.is_related_to_text}`;
      paramInterpretation = `&is_related_to_interpretations=${bibleParams.is_related_to_interpretations}`;
      paramChapterId = bibleParams.chapter_id && `&chapter_id=${bibleParams.chapter_id}`;
      paramVerseId = bibleParams.verse_id && `&verse_id=${bibleParams.verse_id}`;
      paramQuestionId = `&question_id=${bibleParams.question_id}`;

      createQuiz(
        paramText,
        paramInterpretation,
        paramBooks,
        paramChapterId,
        paramVerseId,
        paramQuestionId,
        push,
        getCurrentQueeze,
        updateWarningMsg,
        updateLoadQuiz,
      );
    } else {
      const isChecked = isCheckedText || isCheckedInterpretation;

      if (isChecked && books.length) {
        updateWarningMsg([]);

        paramBooks = books.map(item => `book_ids[]=${item}`).join('&');
        paramText = `?is_related_to_text=${isRelatedToText}`;
        paramInterpretation = `&is_related_to_interpretations=${isInterpretation}`;

        // paramBooks содержит в себе массив параметров для книг

        if (paramBooks.length > 0) paramBooks = `&${paramBooks}`;

        createQuiz(
          paramText,
          paramInterpretation,
          paramBooks,
          paramChapterId,
          paramVerseId,
          paramQuestionId,
          push,
          getCurrentQueeze,
          updateWarningMsg,
          updateLoadQuiz,
        );
      } else {
        const warnings = [];

        const isChecked = !isCheckedText && !isCheckedInterpretation;

        if (isChecked) {
          warnings.push('Необходимо выбрать "По тексту" или "По толкованиям"');
        }

        if (!books.length) {
          warnings.push('Необходимо выбрать хотя бы одну книгу');
        }

        updateWarningMsg(warnings);
        smoothScrollTo(null, { top: 300, behavior: 'smooth' });
        setTimeout(() => updateLoadQuiz(false), 500);
      }
    }
  };
  return (
    <>
      <Helmet>
        <title> {TITLE_QUIZ} </title>
        <meta name="description" content={DESC_QUIZ} />
      </Helmet>
      {isLoadingBookList ? (
        <Preloader />
      ) : (
        <>
          {isLoadQuiz && (
            <div className="quiz-preload">
              <Preloader />
            </div>
          )}
          <div
            className={cx('quiz-testament__btn-container', {
              'quiz-testament__with-next-link': isExistQuiz,
              'quiz-testament__continue': isExistQuiz,
            })}>
            <>
              <div />
              <button
                onClick={getInitQuiz}
                className={cx('quiz-testaments__to-quiz red btn', {
                  'quiz-btn__disabled': isExistQuiz === null,
                })}>
                Начать викторину
              </button>
              <div className="quiz-continue">
                {isExistQuiz && <QuizLink path={latestQuestion} text="Продолжить" />}
              </div>
            </>
          </div>
          <ul className="quiz-warnings">
            {arrOfWarnings.map((warning, index) => (
              // eslint-disable-next-line react/no-array-index-key
              <li className="quiz-warning" key={index}>
                <span className="quiz-warning__desc"> {warning} </span>
              </li>
            ))}
          </ul>
          <div className="quiz-testaments__checkboxes">
            <label className="container-label" htmlFor="quiz-text">
              По тексту
              <input
                type="checkbox"
                onChange={({ target }) => {
                  setCheckedText(!isCheckedText);
                  setBoolText(target.checked ? '1' : '0');
                }}
                checked={isCheckedText}
                id="quiz-text"
              />
              <span className="checkmark" />
            </label>
            {
              <label className="container-label" htmlFor="quiz-chapter">
                По толкованиям
                <input
                  type="checkbox"
                  onChange={({ target }) => {
                    setCheckedInterpretation(!isCheckedInterpretation);
                    setBoolInterpretation(target.checked ? '1' : '0');
                  }}
                  checked={isCheckedInterpretation}
                  id="quiz-chapter"
                />
                <span className="checkmark" />
              </label>
            }
          </div>
          <div className="quiz-testaments">
            <QuizTestament
              title="Новый завет"
              testaments={newTestament}
              testamentName="new"
              setBooksArr={setBooksArr}
              books={books}
            />
            <QuizTestament
              title="Ветхий завет"
              testaments={oldTestament}
              testamentName="old"
              setBooksArr={setBooksArr}
              books={books}
            />
          </div>
          <Link to="/bibleyskaya-viktorina/voprosi-1/" className="quiz__all-questions">
            Все вопросы
          </Link>
        </>
      )}
    </>
  );
};

const mapStateToProps = state => ({
  isLoadingBookList: getBookListIsLoading(state),
  newTestament: getNewBooksList(state),
  oldTestament: getOldBooksList(state),
  latestQuestion: getLatestQuestion(state),
});

const mapDispatchToProps = {
  getCurrentQueeze,
  getBooksList,
};

QuizTestamentContainer.defaultProps = {
  isLoadingBookList: true,
  oldTestament: [],
  newTestament: [],
};

QuizTestamentContainer.propTypes = {
  oldTestament: PropTypes.array,
  newTestament: PropTypes.array,
  isLoadingBookList: PropTypes.bool,
  getBooksList: PropTypes.func,
  getCurrentQueeze: PropTypes.func,
  history: PropTypes.shape({
    push: PropTypes.func,
  }),
  latestQuestion: PropTypes.func,
};

export default connect(mapStateToProps, mapDispatchToProps)(QuizTestamentContainer);
