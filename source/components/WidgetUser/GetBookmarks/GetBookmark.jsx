import R from 'ramda';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Popup } from '../../../dist/browserUtils';
import { bookmarkNeedUpd, bookmarks, getIsLoading } from '../../../dist/selectors/Bookmarks';
import bookmarksActions from '../../../dist/actions/Bookmarks';
import Icon from '../../Icons/Icons';
import List, { ListItem } from '../../List/List';

import './getBookmark.scss';

class GetBookmark extends Component {
  componentDidMount() {
    this.props.getBookmarks();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.bookmarkNeedUpd) {
      this.props.getBookmarks();
    }
  }

  _renderPulldown = (bookmarks, element) => {
    window.addEventListener('scroll', refreshPosition);
    Popup.plugins().popover(
      bookmarks.length > 0 ? (
        <List className="widget__bookmarks">
          {bookmarks.map((bookmark, i) => (
            <ListItem
              key={i}
              title={
                <span className="lk__bookmark-item">
                  <Icon
                    type="circle"
                    icon="bookmark"
                    style={{ backgroundColor: bookmark.color, color: '#ffffff' }}
                  />
                  {(bookmark.info.book && bookmark.info.book.title) || ''}, Глава{' '}
                  {bookmark.info.chapter === 0 && bookmark.bookmark.match(/glava-\d+/g)
                    ? bookmark.bookmark
                        .match(/glava-\d+/g)
                        .join('')
                        .replace('glava-', '')
                    : bookmark.info.chapter}
                  {bookmark.info.verse ? ` стих ${bookmark.info.number}` : ''}
                </span>
              }
              href={`/bible/${bookmark.info.book && bookmark.info.book.code}/glava-${
                bookmark.info.chapter === 0 && bookmark.bookmark.match(/glava-\d+/g)
                  ? bookmark.bookmark
                      .match(/glava-\d+/g)
                      .join('')
                      .replace('glava-', '')
                  : bookmark.info.chapter
              }/${bookmark.info.number ? `stih-${bookmark.info.number}` : ''}`}
            />
          ))}
        </List>
      ) : (
        <div className="empty-content empty-content_side">Вы еще не добавляли закладок</div>
      ),
      element,
    );
  };

  render() {
    const { bookmarks, getIsLoading } = this.props;
    const items = !getIsLoading && Object.values(R.pathOr({}, ['bookmark'], bookmarks));
    return (
      !getIsLoading && (
        <button
          className="user__bookmarks"
          title="Выбранные главы"
          ref={button => {
            this.buttonRef = button;
          }}
          onClick={() => this._renderPulldown(items, this.buttonRef)}>
          <Icon type="circle" icon="bookmark" />
        </button>
      )
    );
  }
}

const refreshPosition = () => {
  Popup.refreshPosition();
};

if (Popup) {
  Popup.addCloseListener(() => {
    window.removeEventListener('scroll', refreshPosition);
  });
}

const mapStateToProps = state => ({
  getIsLoading: getIsLoading(state),
  bookmarks: bookmarks(state),
  bookmarkNeedUpd: bookmarkNeedUpd(state),
});
const mapDispatchToProps = {
  ...bookmarksActions,
};

export default connect(mapStateToProps, mapDispatchToProps)(GetBookmark);
