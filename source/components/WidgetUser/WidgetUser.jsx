import React, { useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { useCookies } from 'react-cookie';

import { Popup } from '../../dist/browserUtils';
import { getIsMobile } from '../../dist/selectors/screen';
import userActions from '../../dist/actions/User';
import { userLoggedIn, userLoggedInUserId } from '../../dist/selectors/User';
import PopupAuth from '../Popups/Auth/Auth';
import GetBookmarks from './GetBookmarks/GetBookmark';
import LinkUserLk from './LinkUserLk';
import Icon from '../Icons/Icons';

import './widgetUser.scss';
import LogoutConfirmation from "~components/Popups/LogoutConfirmation/LogoutConfirmation";

const WidgetUser = ({ location: { search }, userLoggedIn, isPhone, logoutUser, userId }) => {
  const [cookies, setCookie, removeCookie] = useCookies(['userId']);

  const authPopup = e => {
    if (e) e.preventDefault();

    if (Popup) {
      Popup.create(
        {
          title: 'Вход',
          content: <PopupAuth />,
          className: 'popup_auth not-header',
        },
        true,
      );
    }
  };

  useEffect(() => {
    if (search === '?auth-need' && !userLoggedIn) {
      authPopup();
    }
  }, [userLoggedIn, search]);

  useEffect(() => {
    const { userId: cookieUserId } = cookies;
    if (userId && +cookieUserId !== +userId) {
      setCookie('userId', userId, { path: '/' });
    }
  }, [userId, cookies.userId]);
    function confirmLogout(e) {
        e.preventDefault();

        if (Popup) {
            Popup.create(
                {
                    title: 'Вы уверены, что хотите выйти?',
                    content: <LogoutConfirmation />,
                    className: 'popup_auth not-header',
                },
                true,
            );
        }
    }

  return userLoggedIn ? (
    <div className="widget-user auth-user">
      <div className="widget-user__actions onlyDesktop">
        <GetBookmarks />
      </div>
      <LinkUserLk className="onlydesktop">Личный кабинет</LinkUserLk>
      {isPhone && (
        <LinkUserLk className="notDesktop">
          <Icon className="header__profile-img" icon="lk" title="Профиль" />
        </LinkUserLk>
      )}
      <button className="widget-user__logout" title="Выйти" onClick={confirmLogout}>
        <Icon icon="sign-out" />
      </button>
    </div>
  ) : (
    <div className="widget-user">
      <a href="#" onClick={authPopup} className="header-auth__login header-auth__link">
        <Icon icon="sign-in" title="Войти на сайт" />
        <span className="onlyDesktop">Войти на сайт</span>
      </a>
    </div>
  );
};

WidgetUser.propTypes = {
  userLoggedIn: PropTypes.bool,
  isPhone: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  userId: userLoggedInUserId(state),
  userLoggedIn: userLoggedIn(state),
  isPhone: getIsMobile(state),
});

const mapDispatchToProps = {
  ...userActions,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(WidgetUser));
