import React from 'react';
import { Link } from 'react-router-dom';
import cx from 'classnames';

const LinkUserLk = ({ children, className = 'onlydesktop' }) => {
  return (
    <Link to="/lk/" className={cx('header-auth__login header-auth__link', className)}>
      {children}
    </Link>
  );
};

export default LinkUserLk;
