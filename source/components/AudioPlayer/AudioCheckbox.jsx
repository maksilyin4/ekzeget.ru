import React from 'react';

const AudioCheckbox = ({ isAutoplay, setAutoplay }) => {
  const changeHandler = () => {
    setAutoplay((prev) => !prev);
  }

  return (
    <div style={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
      <input
        name={'autoplay'}
        type={'checkbox'}
        checked={isAutoplay}
        onChange={changeHandler}
      />
      {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
      <label htmlFor="autoplay">Автовоспроизведение</label>
    </div>
  );
};

export default AudioCheckbox;