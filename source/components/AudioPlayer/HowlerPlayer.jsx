import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import ReactHowler from 'react-howler';
import raf from 'raf'; // requestAnimationFrame polyfill
import { useHistory, useParams } from 'react-router-dom';
import { updateDisplayDuration } from './utils';
import fancyTimeFormat from '~utils/fancyTimeFormat';

import Volume from './Control/Volume';
import AudioTrack from './Control/AudioTrack';
import DownloadTrack from './Control/DownloadTrack';

import PanelControl from './Control/PanelControl';

import './audioplayer.scss';

let lastAudio = null;

const HowlerPlayer = ({
  // speakerBook, // кто читает главу
  displayDuration, // продолжительность дорожки в формате ММ:СС
  fullWidth, // определяет ширину плеера как 100%
  className,
  src: audioHref, // ссылка на дорожку в CDN
  download,
  fileId,
  srcToDownload, // ссылка для скачивания
  play, // автовоспроизведение
  bookNext,
  callBack,
  isNotLoadingSong,
  numberOfChapters, // количество глав
  withSwitchForChapter, // использовать переключение по главам
  currentTranslate, // информация о переводе
}) => {
  const src = audioHref.includes('https://') ? audioHref : audioHref.replace('http', 'https');
  const song = useRef(null);
  const trackRange = useRef(null);
  const volumeRange = useRef(null);
  const trackRangeBefore = useRef(null);
  const volumeRangeBefore = useRef(null);
  const rafs = useRef(null);

  const obj = {
    isPlay: false,
    playing: false,
    isMute: false,
    volume: 1.0,
    isExist: true,
  };

  const [state, setState] = useState(obj);
  const [audioValue, setAudio] = useState('0:00');
  const [seek, setSeek] = useState(0);
  const [duration, setDuration] = useState(0);

  const [displayLength, setDisplayLength] = useState('0:00');

  const history = useHistory();
  const params = useParams();

  let partPath = null;
  let partNumber = null;
  let numberOfPartForNextTrack = null;
  let numberOfPartForPreviousTrack = null;
  let fullPathForNextTrack = null;
  let fullPathForPreviousTrack = null;

  if (withSwitchForChapter) {
    partPath = params?.chapter_num.split('-')[0]; // вытащить начало пути главы
    partNumber = +params?.chapter_num.split('-')[1]; // вытащить номер главы
    numberOfPartForNextTrack = partNumber < numberOfChapters ? partNumber + 1 : partNumber; // установить новое значение главы для следующей дорожки
    numberOfPartForPreviousTrack = partNumber <= numberOfChapters ? partNumber - 1 : partNumber; // установить новое значение главы для предыдущей дорожки
    fullPathForNextTrack = `/bible/${params?.book_code}/${partPath}-${numberOfPartForNextTrack}`; // сформировать полный путь для следующей дорожки
    fullPathForPreviousTrack = `/bible/${params?.book_code}/${partPath}-${numberOfPartForPreviousTrack}`; // сформировать полный путь для предыдущей дорожки
  }

  useEffect(() => {
    updateDisplayDuration(displayDuration, setDisplayLength);
  }, [displayDuration]);

  useEffect(() => {
    if (!isNotLoadingSong) {
      song.current.load();
    }
  }, [isNotLoadingSong]);

  // перемотка на value сек вперед или назад
  function rewindHandler(value) {
    const currentSeek = song.current.seek();
    const currentDuration = song.current.duration();
    const currentVolume = song.current.volume(); // замыкание предыдущего значения звука

    if (value < 0 && currentSeek < Math.abs(value)) {
      song.current.seek(0);
      setSeek(0);
      return;
    }

    if (value + currentSeek >= currentDuration) {
      // если был передан параметр withSwitchForChapter и текущая глава не последняя
      if (withSwitchForChapter && numberOfPartForNextTrack !== partNumber) {
        goToTrack(currentVolume, fullPathForNextTrack, true);
        return;
      }
      song.current.seek(0);
      setSeek(0);
      return;
    }

    song.current.seek(currentSeek + value);
    setSeek(currentSeek + value);
  }

  const skipHandler = value => {
    const currentVolume = song.current.volume(); // замыкание предыдущего значения звука

    if (value === 'next') {
      goToTrack(currentVolume, fullPathForNextTrack, false);
    } else {
      goToTrack(currentVolume, fullPathForPreviousTrack, false);
    }
    // сброс значений
    setState({ ...state, isPlay: true, playing: true });
  };

  function updateAudio() {
    const { isPlay } = state;

    if (!isPlay) {
      raf.cancel(rafs.current);
      return;
    }
    try {
      const seekSong = song.current.seek();
      const isSeek = isNaN(+seekSong);

      if (!isSeek) {
        setAudio(fancyTimeFormat(seekSong));
        setSeek(seekSong);
        setDuration(song.current.duration());
        updateDisplayDuration(displayDuration, setDisplayLength);
      }
    } catch (error) {
      setState({ ...state, isPlay: false, playing: false });
    }

    rafs.current = raf(updateAudio);
  }

  useEffect(() => {
    // останавливаем анимацию
    rafs.current = raf(updateAudio);
    return () => raf.cancel(rafs.current);
  }, [state.isPlay]);

  useEffect(() => {
    song.current.stop();

    // переход на след аудио
    if (play) {
      togglePlayPause();
    }
  }, [src, play]);

  useEffect(() => {
    // останавливаем предыдущее аудио
    if (lastAudio?.props?.src !== src) {
      setState({ ...state, isPlay: false, playing: false });
    }
  }, [lastAudio, src]);

  const downloadByFieldId = React.useCallback(() => {
    download(fileId);
  }, [download, fileId]);

  function togglePlayPause() {
    setState({
      ...state,
      isPlay: !state.isPlay,
      playing: !state.playing,
    });

    lastAudio = song.current;
  }

  function handleOnLoadError() {
    setState({ ...state, isExist: false });
  }

  function toggleMute() {
    setState({ ...state, isMute: !state.isMute });
  }

  function changeVolume(event) {
    const value = parseFloat(event.target.value);
    setState({
      ...state,
      volume: value,
      isMute: value === 0,
    });
  }

  function changeTrack(event) {
    setSeek(Number(event.target.value));
    song.current.seek(parseFloat(event.target.value));
  }

  function handleOnLoad() {
    setDuration(song.current.duration());
  }

  function goToTrack(volume, path, isPlay) {
    history.push(path); // подставить новый путь в роут
    if (song.current) {
      song.current.seek(0); // установить значение воспроизведения
      setSeek(0); // установить значение ползунка
      setState({
        ...state,
        isPlay, // иконка
        playing: isPlay, // состояние проигрывания дорожки
        volume, // значение звука
        isMute: volume === 0, // состояние без звука/со звуком
      });
    }
  }

  function handleOnEnd() {
    const currentVolume = song.current.volume();
    setState({
      ...state,
      isPlay: false,
      playing: false,
    });

    if (bookNext) {
      callBack(bookNext);
    }
    // если был передан параметр withSwitchForChapter и текущая глава не последняя
    if (withSwitchForChapter && numberOfPartForNextTrack !== partNumber) {
      goToTrack(currentVolume, fullPathForNextTrack, true);
    }
  }

  const { isPlay, isMute, volume, isExist, playing } = state;

  if (volumeRange.current && volumeRangeBefore.current) {
    volumeRangeBefore.current.style.width = `${volumeRange.current.getBoundingClientRect().width *
      volume}px`;
  }
  if (trackRangeBefore.current) {
    trackRangeBefore.current.style.width = `${(trackRange.current.getBoundingClientRect().width /
      duration) *
      seek -
      1}px`;
  }

  const durationRender = duration || (song.current && song.current.duration());

  return (
    <div
      className={cx('audioplayer', className, {
        audioplayer_fullwidth: fullWidth,
        playing: duration !== 0 || displayLength !== 0,
        audioplayer__exist: isExist,
      })}>
      <div className="audioplayer__wrap-first-col audioplayer__wrap">
        <AudioTrack
          audioValue={audioValue}
          displayDuration={
            displayLength !== '0:00'
              ? displayLength
              : fancyTimeFormat(song.current && song.current.duration())
          }
          trackRange={trackRange}
          changeTrack={changeTrack}
          seek={seek}
          duration={durationRender}
          trackRangeBefore={trackRangeBefore}
        />
      </div>
      <PanelControl
        rewindHandler={rewindHandler}
        togglePlayPause={togglePlayPause}
        isPlay={isPlay}
        durationRender={duration}
        skipHandler={skipHandler}
        withSwitchForChapter={withSwitchForChapter}
      />
      <div className="audioplayer__wrap-second-col audioplayer__wrap">
        <Volume
          toggleMute={toggleMute}
          volumeRange={volumeRange}
          changeVolume={changeVolume}
          volumeRangeBefore={volumeRangeBefore}
          isMute={isMute}
          volume={volume}
        />
        {srcToDownload && (
          <DownloadTrack
            fileId={fileId}
            srcToDownload={srcToDownload}
            download={downloadByFieldId}
          />
        )}

        <ReactHowler
          ref={song}
          loop={false}
          preload={false}
          volume={volume}
          onEnd={handleOnEnd}
          mute={isMute}
          onLoad={handleOnLoad}
          playing={playing}
          onLoadError={handleOnLoadError}
          // html5 задваивается воспроизведение
          src={src}
        />
      </div>

      <div className="audioplayer__translate">{currentTranslate}</div>
    </div>
  );
};

HowlerPlayer.propTypes = {
  src: PropTypes.string,
  bookNext: PropTypes.object,
  callBack: PropTypes.func,
  // autoPlay: PropTypes.bool,
  srcToDownload: PropTypes.string,
  download: PropTypes.func,
};

export default HowlerPlayer;
