import React, { useEffect, useRef, useState } from 'react';
import H5AudioPlayer, { RHAP_UI } from 'react-h5-audio-player';
import { useHistory, useParams } from 'react-router';
import Select from 'react-select-me';
import { useSelector } from 'react-redux';
import cx from 'classnames';
import {
  rewindBack,
  rewindForward,
  skipNext,
  replayAsStopIcon,
  muteOnForMobile,
  muteOffForMobile,
} from '~components/Icons/SvgIcon';
import './audioplayer.scss';
import './ReactSelectMe.scss';

const iconStyle = {
  width: '30px',
  height: '30px'
};

const AudioPlayer = ({
  // fileId,
  bookNext,
  callBack,
  // srcToDownload, // ссылка для скачивания
  className,
  numberOfChapters, // количество глав
  withSwitchForChapter = false, // использовать переключение по главам
  downloadIcon, // если трек можно скачать
  src: audioHref, // ссылка на дорожку в CDN
  autoPlay = false, // автовоспроизведение, не работает на ios
  title,
  duration,
  preload,
  forQuiz, // указание, что плеер используется в викторине,
  forQuizData, // объект с данными о вопросе
}) => {
  const playerRef = useRef(null); // создание рефа для трека

  const { quiz } = useSelector(state => ({
    quiz: state.quiz,
  }));

  // для установки роутов аудио каждого вопроса в викторине
  const [srcQuiz, setSrcQiz] = useState('');

  // чтобы понять, когда воспроизвести аудио правильного ответа после сообщения об ошибке
  const [errorAudioMes, setErrorAudioMes] = useState(false);

  // определяем ширину экрана, чтобы понять - используется с мобильного или с десктопа
  const displayWidth = document.documentElement.clientWidth;

  // определим кнопку замьючивания для мобильных (включено или выключено)
  const [mute, muteSwitch] = useState(false);

  // определяем систему Пользователя
  const navigator = window.navigator;
  const ios = /iphone|ipod|ipad/i.test(navigator.userAgent)
    ? true
    : navigator.maxTouchPoints > 0 && /macintel/i.test(navigator.platform);

  // кастомная иконка "замьчивания" для мобильных
  const muteIcon = () => {
    return (
      <button
        aria-label="Mute"
        onClick={customMute}
        className="rhap_button-clear rhap_main-controls-button rhap_play-pause-button custom_mute"
        type="button">
        {mute ? muteOnForMobile() : muteOffForMobile()}
        {/* а вот это настоящая кнопка Стоп */}
        {/* {stop()} */}
      </button>
    );
  };

  const customMute = () => {
    playerRef.current.audio.current.muted = !playerRef.current.audio.current.muted;
    setTimeout(() => muteSwitch(!mute), 0);
  };

  // когда идет переключение вопроса или получаем ответ на вопрос в викторине,
  // меняем src и если не замьючено, то начинаем воспроизведение
  useEffect(() => {
    if (forQuiz) {
      let audioSrc;

      // запуск аудио нового вопроса
      switch (forQuizData?.isCorrectMsg) {
        // аудио нового вопроса (не первого)
        case null:
          audioSrc =
            quiz?.queezeData?.questions[forQuizData?.currentQuestionId - 1]?.mediaQuestion || '';
          break;
        // запуск аудио для правильного ответа
        case true:
          audioSrc =
            quiz?.queezeData?.questions[forQuizData?.currentQuestionId - 1]?.mediaAnswer || '';
          break;
        //  запуск аудио для неправильного ответа
        default:
          // устанавливаем флаг, что идет воспроизведение звука ошибки
          setErrorAudioMes(true);
          // аудио звука ошибки
          audioSrc = quiz?.queezeData?.wrong;
      }

      setSrcQiz(audioSrc);

      if (autoPlay) {
        playerRef.current.audio.current.play();
      }
    }
  }, [forQuizData]);

  const [speed, setSpeed] = useState('1x'); // скорость воспроизведения
  const src = audioHref.includes('https://') ? audioHref : audioHref.replace('http', 'https'); // путь ресурса по которому лежит трек

  const history = useHistory();
  const params = useParams();

  let partPath = null;
  let partNumber = null;
  let numberOfPartForNextTrack = null;
  let numberOfPartForPreviousTrack = null;
  let fullPathForNextTrack = null;
  let fullPathForPreviousTrack = null;

  if (withSwitchForChapter) {
    partPath = params?.chapter_num.split('-')[0]; // вытащить начало пути главы
    partNumber = +params?.chapter_num.split('-')[1]; // вытащить номер главы
    numberOfPartForNextTrack = partNumber < numberOfChapters ? partNumber + 1 : partNumber; // установить новое значение главы для следующей дорожки
    numberOfPartForPreviousTrack =
      partNumber <= numberOfChapters && partNumber > 1 ? partNumber - 1 : partNumber; // установить новое значение главы для предыдущей дорожки
    fullPathForNextTrack = `/bible/${params?.book_code}/${partPath}-${numberOfPartForNextTrack}`; // сформировать полный путь для следующей дорожки
    fullPathForPreviousTrack = `/bible/${params?.book_code}/${partPath}-${numberOfPartForPreviousTrack}`; // сформировать полный путь для предыдущей дорожки
  }

  // переход на след трек при клике на кнопку (предыдущий, следующий)
  const skipHandler = value => {
    if (value === 'next') {
      goToTrack(fullPathForNextTrack, !playerRef.current.audio.current.paused);
    } else {
      goToTrack(fullPathForPreviousTrack, !playerRef.current.audio.current.paused);
    }
  };

  // переход к заданному треку
  const goToTrack = (path, isPlay = false) => {
    history.push(path); // подставить новый путь в роут
    playerRef.current.audio.current.autoplay = isPlay;
  };

  // при завершении воспроизведения трека
  const handleOnEnd = () => {
    // если конец аудио в викторине, то ползунок перематывается в начало, другие аудио не воспроизводятся
    if (forQuiz) {
      // если аудио - это звук ошибки, то там нам не надо перематывать в начало
      if (!errorAudioMes) {
        playerRef.current.audio.current.currentTime = 0; // перемотка файла (ползунка) в начало
      }
      if (errorAudioMes) {
        // запускаем правильный ответ
        const audioSrc =
          quiz?.queezeData?.questions[forQuizData?.currentQuestionId - 1]?.mediaAnswer || '';
        setSrcQiz(audioSrc);

        if (autoPlay) {
          playerRef.current.audio.current.play();
        }

        // снимаем флаг, что идет воспроизведение звука ошибки
        setErrorAudioMes(false);
      }
    } else {
      if (bookNext) {
        callBack(bookNext);
      }
      // если был передан параметр withSwitchForChapter и текущая глава не последняя
      if (withSwitchForChapter && numberOfPartForNextTrack !== partNumber) {
        goToTrack(fullPathForNextTrack, true);
      }
    }
  };

  // регулировка скорости воспроизведения
  const changeSpeed = data => {
    setSpeed(data.label);
    if (playerRef.current) {
      playerRef.current.audio.current.playbackRate = data.value;
    }
  };

  // опции для регулировки скорости
  const options = [
    { value: 0.75, label: '0.75x' },
    { value: 1, label: '1x' },
    { value: 1.25, label: '1.25x' },
    { value: 1.5, label: '1.5x' },
    { value: 1.75, label: '1.75x' },
    { value: 2, label: '2x' },
  ];

  // иконка изменения скорости
  const speedIcon = () => {
    return <Select options={options} value={speed} onChange={changeSpeed} listPosition="top" />;
  };

  // иконка остановки аудио (не pause, а именно stop)
  const stopIcon = () => {
    return (
      <button
        aria-label="Stop"
        onClick={stopAudio}
        className="rhap_button-clear rhap_main-controls-button rhap_play-pause-button"
        type="button">
        {replayAsStopIcon()}
        {/* а вот это настоящая кнопка Стоп */}
        {/* {stop()} */}
      </button>
    );
  };

  // функционал для викторины
  const stopAudio = () => {
    playerRef.current.audio.current.pause(); // в плеере нет метода stop, ставим на паузу
    playerRef.current.audio.current.currentTime = 0; // перемотка файла (ползунка) в начало
  };

  const containerStyle = forQuiz ? 'rhap_sidebar-mode border-round' : 'rhap_sidebar-mode';

  return (
    <H5AudioPlayer
      ref={playerRef}
      autoPlay={autoPlay}
      preload={preload}
      defaultDuration={formatDuration(duration)}
      showSkipControls={withSwitchForChapter}
      showJumpControls={!forQuiz}
      customControlsSection={[
        RHAP_UI.MAIN_CONTROLS,
        RHAP_UI.ADDITIONAL_CONTROLS,
        forQuiz ? stopIcon() : null,
        ios ? <div /> : displayWidth > 731 ? null : <div />,
        // в зависмости от устройства или показываем громкость (из под капота) или вставляем кастомную
        // проверяем, если ios, то всегда кастомная кнопка mute, иначе проверяем размеры экрана
        ios ? muteIcon() : displayWidth > 731 ? RHAP_UI.VOLUME_CONTROLS : muteIcon(),
        forQuiz ? null : speedIcon(),
        forQuiz ? null : RHAP_UI.LOOP,
        forQuiz ? null : <div className="audioplayer__download">{downloadIcon}</div>,
      ]}
      src={forQuiz ? srcQuiz : src}
      onEnded={handleOnEnd}
      showFilledVolume
      progressJumpSteps={{ backward: 15000, forward: 15000 }}
      customAdditionalControls={[]}
      className={cx([className, containerStyle])}
      autoPlayAfterSrcChange={false}
      header={<div className="audioplayer__header">{title}</div>}
      customIcons={{
        rewind: rewindBack(iconStyle),
        forward: rewindForward(iconStyle),
        previous: (
          <div onClick={() => skipHandler('previous')}>
            {skipNext({
              ...iconStyle,
              style: { transform: 'rotate(-180deg)' },
            })}
          </div>
        ),
        next: <div onClick={() => skipHandler('next')}>{skipNext(iconStyle)}</div>,
        // на всякий случай оставлю реализацию настоящей кнопки Стоп (если Андрей Викторович в будущем передумает)
        // stop: <div onClick={() => skipHandler('next')}>{stop(iconStyle)}</div>,
        stop: <div onClick={() => skipHandler('next')}>{rewindBack(iconStyle)}</div>,
      }}
    />
  );
};

export default AudioPlayer;

function formatDuration(duration) {
  if (duration) {
    if (duration.length === 4 || duration.length === 7) { // m:ss || h:mm:ss
      return  `0${duration}`;
    }
    return duration;
  }
}
