import React, { Component } from 'react';
import cx from 'classnames';
import './textField.scss';
import PropTypes from 'prop-types';
import Icon from '../Icons/Icons';

class TextField extends Component {
  static propTypes = {
    id: PropTypes.string,
    type: PropTypes.string,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    defaultValue: PropTypes.string,
    error_text: PropTypes.string,
    required: PropTypes.bool,
    onChange: PropTypes.func,
    min: PropTypes.number,
    max: PropTypes.number,
    maxLength: PropTypes.number,
    showPasswdControl: PropTypes.string,
    rightSlot: PropTypes.element,
  };

  constructor(props) {
    super(props);

    this.searchRef = null;

    this.state = {
      showPassw: false,
      focusPassw: false,
    };
  }

  _showPassw = () => {
    this.setState({ showPassw: !this.state.showPassw });
  };

  _blurPassw = () => {
    this.setState({
      showPassw: false,
      // focusPassw: false
    });
  };

  _focusPassw = () => {
    this.setState({ focusPassw: true });
  };

  _searchSubmit = () => {
    this.searchRef.submit();
  };

  render() {
    const {
      redirectTo,
      value,
      defaultValue,
      error_text,
      onChange,
      placeholder,
      label,
      required,
      id,
      type,
      isSub,
      isReq = false,
      showPasswdControl = true,
      rightSlot,
    } = this.props;

    return (
      <div className={cx('textfield', { indent: isSub })}>
        {label && (
          // eslint-disable-next-line jsx-a11y/label-has-associated-control
          <label className="textfield__label">
            {label}
            {required ? '*' : ''}
          </label>
        )}

        {type === 'password' && (
          <div className="textfield__input-wrap">
            <input
              id={id}
              type={this.state.showPassw ? 'text' : 'password'}
              placeholder={placeholder}
              value={value}
              className="textfield__input textfield__password"
              onChange={onChange}
              onBlur={this._blurPassw}
              onFocus={this._focusPassw}
            />
            {this.state.focusPassw && showPasswdControl && (
              <div
                className={cx('textfield__input-eye', { active: this.state.showPassw })}
                onClick={this._showPassw}>
                <Icon icon="eye" />
              </div>
            )}
            {rightSlot && <div className="right-slot">{rightSlot}</div>}
            {error_text && <span className="textfield__error">{error_text}</span>}
          </div>
        )}

        {type === 'textarea' && (
          <div className="textfield__input-wrap">
            <textarea
              id={id}
              type={type}
              placeholder={placeholder}
              value={value}
              className="textfield__input"
              defaultValue={defaultValue}
              onChange={onChange}
              required={required}
            />
            {error_text && <span className="textfield__error">{error_text}</span>}
          </div>
        )}

        {type === 'search' && (
          <div className="textfield__input-wrap">
            <input
              id={id}
              type="text"
              placeholder={placeholder}
              value={value}
              className="textfield__input exegetes__input"
              onChange={onChange}
              ref={element => {
                this.searchRef = element;
              }}
              onKeyDown={key => key.keyCode === 13 && redirectTo()}
            />
            <button
              type="submit"
              title="Начать поиск"
              onClick={redirectTo}
              className="textfield__input-search">
              <Icon icon="search" />
            </button>
            {error_text && <span className="textfield__error">{error_text}</span>}
          </div>
        )}

        {type === 'number' && (
          <div className="textfield__input-wrap">
            <input
              id={id}
              type={type}
              placeholder={placeholder}
              value={value}
              className="textfield__input"
              defaultValue={defaultValue}
              onChange={onChange}
              min={this.props.min}
              max={this.props.max}
              maxLength={this.props.maxLength}
              pattern="[0-9]*"
              inputMode="numeric"
            />
            {error_text && <span className="textfield__error">{error_text}</span>}
          </div>
        )}

        {type !== 'number' && type !== 'search' && type !== 'textarea' && type !== 'password' && (
          <div className="textfield__input-wrap">
            <input
              id={id}
              type={type}
              placeholder={placeholder}
              value={value}
              className="textfield__input"
              defaultValue={defaultValue}
              onChange={onChange}
              required={isReq}
            />
            {error_text && <span className="textfield__error">{error_text}</span>}
          </div>
        )}
      </div>
    );
  }
}

export default TextField;
