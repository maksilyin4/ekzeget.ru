// Необходимо, тк Толкования входят в часть страниц Библии
export const isBibleActive = i => (match, location) => {
  if (i === 0) {
    const matches = ['bible', 'interpretation'];

    return matches.some(el => {
      const url = location.pathname.split('/')[1];
      return url === el;
    });
  }
  return match;
};
