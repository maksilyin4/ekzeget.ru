import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { NavLink, withRouter } from 'react-router-dom';

import { LS } from '../../dist/browserUtils';
import { onMouseEnter, onMouseLeave, onMouseObj } from '../PreloadChunk';
import { isBibleActive } from './utilsSiteNavigation';
import {
  defaultUrlForBible,
  mediateka,
  allAboutBible,
  bibleyskayaViktorina,
  bibleGroup,
} from '../../utils/common';

import './siteNavigation.scss';

class SiteNavigation extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      urlFromBible: 'initial',
    };
  }

  componentDidMount = () => {
    if (process.env.BROWSER) {
      const urlFromBible = LS.get('urlFromBible');

      if (urlFromBible) {
        this.setState({ urlFromBible });
      }
    }
  };

  componentDidUpdate(prevProps) {
    const {
      location: { pathname },
    } = this.props;

    if (prevProps.location.pathname !== pathname && process.env.BROWSER) {
      const urlFromBible = LS.get('urlFromBible') || 'initial';
      this.setState({
        urlFromBible,
      });
    }
  }

  getRouters = (urlFromBible, search) => [
    {
      pathname: `${urlFromBible === 'initial' ? defaultUrlForBible : urlFromBible}`,
      title: 'Библия',
      getMouseObj: onMouseObj.onMouseBible,
    },
    {
      pathname: `/reading-plan/`,
      title: 'План чтений',
      getMouseObj: onMouseObj.onMousePlan,
    },
    {
      pathname: `/${mediateka}/`,
      title: 'Медиатека',
      search,
      getMouseObj: onMouseObj.onMouseMediateka,
    },
    {
      pathname: `/${allAboutBible}/about-bible/`,
      title: 'Все о Библии',
      getMouseObj: onMouseObj.onMouseAllAboutBible,
    },
    // TODO удалить и весь раздел если не нужен будет более месяца 29.01.20
    // {
    //   pathname: `/video/`,
    //   title: 'Видео',
    //   getMouseObj: onMouseObj.onMouseVideo,
    // },
    {
      pathname: `/${bibleGroup}/about/`,
      title: 'Группы',
      getMouseObj: onMouseObj.onMouseBibleGroups,
    },
    {
      pathname: `/${bibleyskayaViktorina}/`,
      title: 'Викторина',
      getMouseObj: onMouseObj.onMouseQuiz,
    },
  ];

  render() {
    const {
      isMobile,
      location: { search },
    } = this.props;
    const { urlFromBible } = this.state;

    return (
      <nav className="site-nav">
        {isMobile && (
          <NavLink
            to="/"
            exact
            className="site-nav__item"
            activeClassName="active"
            title="Главная"
            onFocus={onMouseObj.onMouseHome}>
            Главная
          </NavLink>
        )}
        {this.getRouters(urlFromBible, search).map(
          ({ pathname, title, getMouseObj, search = '' }, i) => (
            <NavLink
              key={title}
              to={{
                pathname,
                search:
                  process.env.BROWSER && !window.location.href.includes('/search/') ? search : '?',
              }}
              isActive={isBibleActive(i)}
              className="site-nav__item"
              activeClassName="active"
              title={title}
              onMouseEnter={() => onMouseEnter(getMouseObj)}
              onMouseLeave={onMouseLeave}
              onFocus={getMouseObj}>
              {title}
            </NavLink>
          ),
        )}
        {isMobile && (
          <NavLink
            to="/lk/"
            className="site-nav__item"
            activeClassName="active"
            title="Личный кабинет">
            Личный кабинет
          </NavLink>
        )}
      </nav>
    );
  }
}

SiteNavigation.defaultProps = {
  isMobile: false,
};

SiteNavigation.propTypes = {
  isMobile: PropTypes.bool,
  location: PropTypes.shape({
    search: PropTypes.string,
  }),
};

export default withRouter(SiteNavigation);
