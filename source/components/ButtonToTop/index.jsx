import React, { Component } from 'react';
import Icon from '../Icons/Icons';

import './buttonToTop.scss';

class ButtonToTop extends Component {
  state = {
    intervalId: 0,
    pageYOffset: 1200,
    isActive: false,
  };

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  scrollStep = () => {
    const { intervalId } = this.state;
    const { scrollStepInPx } = this.props;

    if (window.pageYOffset === 0) {
      clearInterval(intervalId);
    }

    window.scroll(0, window.pageYOffset - scrollStepInPx);
  };

  scrollToTop = () => {
    const { delayInMs } = this.props;
    let { intervalId } = this.state;
    if (intervalId) clearInterval(intervalId);

    intervalId = setInterval(this.scrollStep, delayInMs);
    this.setState({ intervalId });
  };

  handleScroll = () => {
    if (window.pageYOffset >= this.state.pageYOffset) {
      this.setState({ isActive: true });
    } else {
      this.setState({ isActive: false });
    }
  };

  render() {
    const { isActive } = this.state;

    return isActive ? (
      <button title="Back to top" className="scroll" onClick={this.scrollToTop}>
        <Icon icon="angle-right" />
      </button>
    ) : null;
  }
}

export default ButtonToTop;
