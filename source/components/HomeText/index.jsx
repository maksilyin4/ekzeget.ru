import React from 'react';

import './styles.scss';

export const HomeText = ({ text }) => {
  return (
    <div className="paper__homeText">
      <div className="homeText" dangerouslySetInnerHTML={{ __html: text }}></div>
    </div>
  );
};
