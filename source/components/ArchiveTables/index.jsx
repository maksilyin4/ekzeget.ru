import React, { useEffect } from 'react';
import './styles.scss';
import { percent } from '~components/PlanAddUsers/table';
import { connect } from 'react-redux';
import { getArchivedPlans } from '../../dist/actions/ReadingPlan';
import { archivedPlans, archivedPlansLoading } from '~dist/selectors/ReadingPlan';
import { parseDateFrom } from '~pages/CreatePlan/CreateGroup/model/parseDate';
// import { userData } from '~dist/selectors/User';
// import { getName } from '~components/PlanAddUsers';
import { getUser } from '~dist/actions/User';
import Preloader from '~components/Preloader/Preloader';
import { userData } from '~dist/selectors/User';
import { getName } from '~components/PlanAddUsers';
// import { getGroupInfo } from '~dist/api/readingPlan/getGroup';
// import { getUserPlanStats } from '../../dist/api/readingPlan/getGroup';

function ArchiveTables({
  archivedPlans,
  archivedPlansLoading,
  userData,
  getUser,
  getArchivedPlans,
}) {
  // const user = useSelector(userData);
  useEffect(() => {
    getArchivedPlans();
    getUser();
  }, []);

  return (
    <section>
      {archivedPlansLoading && <Preloader />}
      {archivedPlans &&
        archivedPlans?.map((archiveTable, i) => (
          <Table user={userData?.user} key={i} table={archiveTable} />
        ))}
      {!archivedPlans.length && !archivedPlansLoading && (
        <h3>Вы пока не прочитали ни одного плана</h3>
      )}
    </section>
  );
}

const Table = ({ table, user }) => {
  const header = {
    soloUser: 'Участие в чтениях. Личная статистика',
    groupUser: 'Участие в чтениях в группе. Личная статистика',
    groupMentor: 'Руководство группой. Усредненная  статистика',
  };

  return (
    <div className="table__archive" key={table.id}>
      <div className="table__archive-head">{header[table.record_type]}</div>
      <div className="table-wrapper">
        <div className="row_archive row_archive__head">
          <div>Название</div>
          <div>Начало</div>
          <div>Финиш</div>
          <div>Дни</div>
          <div>Участники</div>
          <div>Отзывы</div>
          <div>Ответы</div>
          <div>Прав. ответы</div>
        </div>
        <div className="row_archive">
          <div>{table.plan_description}</div>
          <div>{parseDateFrom(table.start_date)}</div>
          <div>{parseDateFrom(table.stop_date)}</div>
          <div>{table.plan_length}</div>
          <div>{table.members_count}</div>
          <div>{table.comments_count}</div>
          <div>{percent(table.answers_percent)}</div>
          <div>{percent(table.correct_answers_percent)}</div>
        </div>
      </div>
      <div className="table__archive-description">
        {user?.id === table.mentor_id && table.group_id && <div>Руководитель: {getName(user)}</div>}
        <p>{table.plan_comment}</p>
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  archivedPlans: archivedPlans(state),
  userData: userData(state),
  archivedPlansLoading: archivedPlansLoading(state),
});
const mapDispatchToProps = {
  getArchivedPlans,
  getUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(ArchiveTables);
