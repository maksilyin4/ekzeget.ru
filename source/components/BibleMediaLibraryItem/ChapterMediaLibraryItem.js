import React from 'react';
import { Link } from 'react-router-dom';

import { ItemHeaderDesktop, ItemHeaderMobile } from './ItemHeader';
import { urlToAPI } from '../../dist/browserUtils';

const propTypes = {};

const defaultProps = {};

export default class ChapterMediaLibraryItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isShown: false,
      url: urlToAPI,
      isBook: true,
      isVideo: true,
      isAudio: true,
    };

    this.checkArray(this.props.book, 'isBook');
    this.checkArray(this.props.video, 'isVideo');
    this.checkArray(this.props.audio, 'isAudio');
  }

  checkArray = (files, type) => {
    let isEq = false;
    let typeUrl = 'download_url';

    if (files !== null && files !== undefined) {
      files.files.forEach(file => {
        typeUrl = 'download_url';

        if (type === 'isAudio' && !file.is_full) {
          typeUrl = 'url';
        }

        if (type === 'isVideo' && file.url.length > 0) {
          isEq = true;
        } else if (file[typeUrl].length > 0) {
          isEq = true;
        }
      });
    }

    this.state[type] = isEq;
  };

  changeStateText = () => {
    this.setState({
      isShown: !this.state.isShown,
    });
  };

  render() {
    const { isMobile, title, author, description, img, audio, video, idMaterial } = this.props;
    const { isShown, url, isBook, isVideo, isAudio } = this.state;
    return (
      <React.Fragment>
        <li className="media-book">
          {isMobile ? (
            <ItemHeaderMobile
              title={title}
              author={author}
              idMaterial={idMaterial}
              img={img}
              url={url}
              isAudio={isAudio}
              isVideo={isVideo}
              isBook={isBook}
            />
          ) : (
            <ItemHeaderDesktop
              title={title}
              author={author}
              img={img}
              url={url}
              idMaterial={idMaterial}
              isAudio={isAudio}
              isVideo={isVideo}
              isBook={isBook}
            />
          )}
          <div className="media-book__content">
            <div className="media-book__description">
              {!isShown && description.length > 210
                ? `${description.slice(0, 210)}...`
                : description}
            </div>
            <button onClick={this.changeStateText} className="media-book__description_show">
              {isShown ? 'Скрыть полное описание' : 'Развернуть полное описание'}
            </button>
          </div>
          <div className="media-book__connections">
            {audio !== null && audio.files.length > 1 && (
              <div className="media-book__connections-wrapper">
                <span className="media-book__name-note">Связанные аудио:</span>
                <ul className="media-book__inner-list">
                  {audio.files.slice(1).map(i => (
                    <li className="media-book__inner-record" key={i.id}>
                      <Link
                        to={`/media-library/section/all/info/${idMaterial}/listen/?${i.id}`}
                        className="media-book-link">
                        {i.title}
                      </Link>
                    </li>
                  ))}
                </ul>
              </div>
            )}
            {video !== null && video.files.length > 1 && (
              <div className="media-book__connections-wrapper">
                <span className="media-book__name-note">Связанные видео:</span>
                <ul className="media-book__inner-list">
                  {video.files.map(
                    i =>
                      i.is_full !== 1 && (
                        <li className="media-book__inner-record" key={i.id}>
                          <Link
                            to={`/media-library/section/all/info/${idMaterial}/watch/?${i.id}`}
                            className="media-book-link">
                            {i.title}
                          </Link>
                        </li>
                      ),
                  )}
                </ul>
              </div>
            )}
          </div>
        </li>
      </React.Fragment>
    );
  }
}

ChapterMediaLibraryItem.propTypes = propTypes;
ChapterMediaLibraryItem.defaultProps = defaultProps;
