import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Icon from '../Icons/Icons';
import defaultBookIcon from './defaultBookIcon.svg';
import volumeUpIcon from '../../assets/images/icons/volume-up.svg';

export class ItemHeaderDesktop extends Component {
  render() {
    const { title, author, img, url, idMaterial, isBook, isVideo, isAudio } = this.props;
    return (
      <div className="media-book__header">
        <div className="media-book__info">
          {img !== null ? (
            <Link to={`/media-library/section/all/info/${idMaterial}`}>
              <img
                src={`${url}${img.base_url}/${img.path}`}
                alt="Изображение книги"
                className="book-info__img"
              />
            </Link>
          ) : (
            <Link to={`/media-library/section/all/info/${idMaterial}`}>
              <img src={defaultBookIcon} className="book-info__img" alt="Изображение книги" />
            </Link>
          )}

          <div className="media-book__name">
            <div className="media-book__name-main">
              <Link to={`/media-library/section/all/info/${idMaterial}`}>{title}</Link>
            </div>
            <span className="media-book__name-note">{author}</span>
          </div>
        </div>
        <div className="media-book__actions">
          {isBook && (
            <Link
              to={`/media-library/section/all/info/${idMaterial}/`}
              className="media-book__action">
              <Icon icon="book" type="book" />
            </Link>
          )}
          {isAudio && (
            <Link
              to={`/media-library/section/all/info/${idMaterial}/listen`}
              className="media-book__action">
              <div>
                <img src={volumeUpIcon} alt="Вернуть громкость" title="Вернуть громкость" />
              </div>
            </Link>
          )}
          {isVideo && (
            <Link
              to={`/media-library/section/all/info/${idMaterial}/watch`}
              className="media-book__action">
              <Icon icon="play" type="play" />
            </Link>
          )}
        </div>
      </div>
    );
  }
}

export class ItemHeaderMobile extends Component {
  render() {
    const { title, author, idMaterial, img, url, isBook, isVideo, isAudio } = this.props;
    return (
      <div>
        <div className="media-book__header-mobile">
          {img !== null ? (
            <img
              src={`${url}${img.base_url}/${img.path}`}
              alt="Изображение книги"
              className="book-info__img"
            />
          ) : (
            <img src={defaultBookIcon} alt="Изображение книги" className="book-info__img" />
          )}
          <div className="media-book__actions">
            {isBook && (
              <Link
                to={`/media-library/section/all/info/${idMaterial}/`}
                className="media-book__action">
                <Icon icon="book" type="book" />
              </Link>
            )}
            {isAudio && (
              <Link
                to={`/media-library/section/all/info/${idMaterial}/listen`}
                className="media-book__action">
                <div>
                  <img src={volumeUpIcon} alt="Вернуть громкость" title="Вернуть громкость" />
                </div>
              </Link>
            )}
            {isVideo && (
              <Link
                to={`/media-library/section/all/info/${idMaterial}/watch`}
                className="media-book__action">
                <Icon icon="play" type="play" />
              </Link>
            )}
          </div>
        </div>
        <div className="media-book__name">
          <div className="media-book__name-main" style={{ textAlign: 'justify' }}>
            <Link to={`/media-library/section/all/info/${idMaterial}`}>{title}</Link>
          </div>
          <span className="media-book__name-note">{author}</span>
        </div>
      </div>
    );
  }
}
