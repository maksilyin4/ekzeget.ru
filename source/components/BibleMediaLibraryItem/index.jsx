import React, { Fragment } from 'react';

import ChapterMediaLibraryItem from './ChapterMediaLibraryItem';
import Preloader from '../Preloader/Preloader';

import './chapterMediaLibrary.scss';

const propTypes = {};

const defaultProps = {};

class ChapterMediaLibrary extends React.Component {
  render() {
    const { isMobile, bibleMediaLibrary, bibleMediaLibraryLoading, subTitle } = this.props;
    return !bibleMediaLibraryLoading ? (
      <div className="media-chapter">
        {bibleMediaLibrary.length > 0 ? (
          <Fragment>
            <div
              className="bible-content__title"
              style={{ borderBottom: 'none', paddingBottom: '0' }}>
              С этой главой связаны:
            </div>
            <ul>
              {bibleMediaLibrary.map(item => (
                <ChapterMediaLibraryItem
                  key={item.id}
                  isMobile={isMobile}
                  title={item.title}
                  author={item.author.title}
                  description={item.description}
                  img={item.image}
                  audio={item.audio}
                  video={item.video}
                  idMaterial={item.id}
                  book={item.ebook}
                />
              ))}
            </ul>
          </Fragment>
        ) : (
          // style - anitpattern, delete after
          <div
            className="bible-content__title"
            style={{ borderBottom: 'none', paddingBottom: '0', textAlign: 'center' }}>
            {`На ${subTitle} нет материалов медиатеки`}
          </div>
        )}
      </div>
    ) : (
      <Preloader />
    );
  }
}

ChapterMediaLibrary.propTypes = propTypes;
ChapterMediaLibrary.defaultProps = defaultProps;

export default ChapterMediaLibrary;
