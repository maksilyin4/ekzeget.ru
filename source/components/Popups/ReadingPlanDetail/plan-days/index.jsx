import React from 'react';
import cn from 'classnames';
import './index.scss';

export default function PlanDays({
  handleChange,
  stateChange,
  readOnly,
  title,
  variant = 'default',
  isMinCircle,
  isForm,
  className,
}) {
  const daysData = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];
  return (
    <div
      className={cn(
        'rpd_days',
        variant === 'horizontal' && 'horizontal',
        isForm && 'form',
        !readOnly && 'change',
        variant === 'default' && 'default',
        isMinCircle && 'min',
        className,
      )}>
      <div className="rpd__days--title">{title}</div>
      <div className="rpd__days-wrapper">
        {daysData.map(day => (
          <div
            key={day}
            onClick={() => {
              if (!readOnly) {
                handleChange(day);
              }
            }}
            className={cn(
              'rpd__circle-day',
              stateChange.includes(day) && 'rpd__circle-day--active',
            )}>
            <span>{day}</span>
          </div>
        ))}
      </div>
    </div>
  );
}
