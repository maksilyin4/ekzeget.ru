const lib = { Mon: 'пн', Tue: 'вт', Wed: 'ср', Thu: 'чт', Fri: 'пт', Sat: 'сб', Sun: 'вс' };

export function getDaysByObj(obj) {
  const objKeys = Object.keys(lib);
  const days = objKeys.map(key => (obj?.[key] > 0 ? lib?.[key] : '')).filter(Boolean);
  return { days };
}
