import React from 'react';
import './index.scss';
import PlanDays from '../plan-days';
import cn from 'classnames';
import arrow from '../../../../assets/images/plan.png';
import dayjs from 'dayjs';
import { wordEnding } from '~utils/wordEnding';

export default function ReadingPlanPopupHead({ isMin, head, parallelBooks }) {
  const start = new Date(head.startDate);
  const end = new Date(head.endDate);
  const timeDifference = end.getTime() - start.getTime();
  const daysDifference = Math.floor(timeDifference / (1000 * 3600 * 24));

  return (
    <header className={cn('rpd__head', isMin && 'rpd__head-min')}>
      <div className="rpd__history">
        <div className="rpd__title-head">
          <span>{head.chapters}</span> <>{wordEnding(head.chapters, ['глава', 'главы', 'глав'])}</>{' '}
          в день
          {parallelBooks > 0 && (
            <>
              {' '}
              | параллельно
              <span> {parallelBooks} </span>
              {wordEnding(parallelBooks, ['книга', 'книги', 'книг'])}
            </>
          )}
        </div>
        <div className="rpd__arrow-wrapper">
          <div className="rpd__date">
            <span>{dayjs(start).format('DD.MM.YYYY')}</span>
            <span>{dayjs(end).format('DD.MM.YYYY')}</span>
            <img className="rpd__arrow-img" src={arrow} alt="" />
          </div>
          <div className="rpd__blue-arrow">
            <span>
              {head.planLength} дней чтений ({daysDifference} календ)
            </span>
          </div>
        </div>
      </div>
      <PlanDays
        readOnly
        className={'rpd__d'}
        isMin={isMin}
        variant={isMin ? 'horizontal' : 'default'}
        title={'Дни недели'}
        stateChange={head.weekDays}
      />
    </header>
  );
}
