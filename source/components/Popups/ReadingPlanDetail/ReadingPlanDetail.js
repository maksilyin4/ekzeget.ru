import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import readingPlanActions from '../../../dist/actions/ReadingPlan';
import { readingPlanDetailIsLoading, planDetail } from '../../../dist/selectors/ReadingPlan';
import Preloader from '../../Preloader/Preloader';
import './readingPlanDetail.scss';
import ReadingPlanTable from '../../ReadingPlanTable/ReadingPlanTable';
import { getDaysByObj } from './plan-days/getDaysByObj';

const ReadingPlanDetail = ({
  getReadingPlanDetail,
  plan_id,
  group,
  plan_name,
  planDetail,
  readingPlanDetailIsLoading,
}) => {
  useEffect(() => {
    getReadingPlanDetail(plan_id);
  }, []);
  return (
    <div className="popup__content">
      {readingPlanDetailIsLoading ? (
        <Preloader />
      ) : (
        <ReadingPlanTable
          head={
            group
              ? {
                  title: plan_name,
                  chapters: planDetail[1]?.length,
                  startDate: group.start_date,
                  endDate: group.stop_date,
                  weekDays: getDaysByObj(group.schedule).days,
                  planLength: Object.values(planDetail).length,
                  parallel: getDaysByObj(group.schedule).chaptersPerDay,
                }
              : undefined
          }
          plan={planDetail}
        />
      )}
    </div>
  );
};

ReadingPlanDetail.defaultProps = {
  getReadingPlanDetail: () => {},
  plan_id: 1,
  planDetail: {},
  readingPlanDetailIsLoading: true,
};

ReadingPlanDetail.propTypes = {
  getReadingPlanDetail: PropTypes.func,
  plan_id: PropTypes.number,
  readingPlanDetailIsLoading: PropTypes.bool,
  planDetail: PropTypes.shape(
    PropTypes.shape([
      PropTypes.shape({
        book_code: PropTypes.string,
        chapter: PropTypes.number,
        day: PropTypes.number,
        short: PropTypes.string,
        verse: PropTypes.string,
      }),
    ]),
  ),
};

const mapStateToProps = state => ({
  readingPlanDetailIsLoading: readingPlanDetailIsLoading(state),
  planDetail: planDetail(state),
});

const mapDispatchToProps = {
  ...readingPlanActions,
};

export default connect(mapStateToProps, mapDispatchToProps)(ReadingPlanDetail);
