import React from 'react';

import { Popup } from '~dist/browserUtils';

import Auth from '../../Auth/Auth';

export const Warning = ({ text = 'Добавить' }) => {
  const openAuth = () => {
    Popup.close();

    Popup.create(
      {
        title: 'Вход',
        content: <Auth />,
        className: 'popup_auth not-header',
      },
      true,
    );
  };
  return (
    <div className="warning-container">
      <p>
        {`${text} могут только`} <b>аутентифицированные пользователи</b>.
      </p>
      <button className="warning-btn" onClick={openAuth}>
        Войдите или зарегистрируйтесь.
      </button>
    </div>
  );
};
