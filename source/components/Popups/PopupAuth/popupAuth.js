import React from 'react';

import './popupAuth.scss';

import { Popup } from '~dist/browserUtils';
import { Warning } from './Warning/';

export const popupAuth = (text = '') => {
  Popup.close();

  Popup.create(
    {
      content: <Warning text={text} />,
      className: 'popup_warning',
    },
    true,
  );
};
