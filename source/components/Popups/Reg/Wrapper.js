import React, { Component } from 'react';

import { Popup } from '../../../dist/browserUtils';
import RegSuccess from '../RegSuccess/RegSuccess';
import { validateEmail, sendSocialActions } from '../../../dist/utils';
import { preloadLk } from '../../PreloadChunk';
import PropTypes from "prop-types";

function Wrapper(WrappedComponent) {
  return class RegistrationWrapper extends Component {
    state = {
      username: '',
      email: '',
      password: '',
      confirm_password: '',
      verifyCode: '',
      device_info: JSON.stringify({ device_info: typeof navigator !== 'undefined' && navigator.userAgent }),
      message: '',
    };

    static propTypes = {
      onSuccess: PropTypes.func,
    };

    componentWillReceiveProps(nextProps) {
      if (nextProps.error) {
        this.setState({ message: nextProps.error });
      }
    }

    _handleChange = name => ({ currentTarget: { value } }) => {
      this.setState({
        [name]: value,
        message: '',
      });
    };

    _successPopup = () => {
      Popup.close();
      Popup.create(
        {
          title: 'Спасибо за регистрацию!',
          content: <RegSuccess />,
          className: 'popup_reg-success not-header',
        },
        true,
      );

      if (typeof this.props.onSuccess === 'function') {
        this.props.onSuccess();
      }
    };

    _submit = e => {
      e.preventDefault();
      if (this.props.userRegIsLoading) return;
      const { username, email, password, confirm_password, device_info, verifyCode } = this.state;
      if (!username.trim()) {
        this.setState({
          message: 'Проверьте логин',
        });
      } else if (!email || !validateEmail(email)) {
        this.setState({
          message: 'Введён некорректный email',
        });
      } else if (!password || !/^([a-zA-Z0-9]{6,})$/.test(password)) {
        this.setState({
          message:
            'Пароль должен состоять не менее чем из 6 символов и состоять только из латинских букв и цифр',
        });
      } else if (password !== confirm_password) {
        this.setState({
          message: 'Введённые пароли не совпадают',
        });
      } else {
        const data = new FormData();
        data.append('username', username);
        data.append('email', email);
        data.append('password', password);
        data.append('captcha', verifyCode);
        data.append('device_info', device_info);

        this.props
          .regUser(data)
          .then(response => {
            if (response.error) {
              this.setState({
                message: response.error,
              });
            } else {
              preloadLk();

              this._successPopup();
              sendSocialActions({
                ga: [
                  'event',
                  'event_name',
                  { event_category: 'registration', event_action: 'send' },
                ],
                ym: 'registration',
              });
              this.setState({
                isFetching: false,
              });
            }
          })
          .catch(() => this.setState({ message: 'Ошибка сервера, повторите позже' }));
      }
    };

    render() {
      return (
        <WrappedComponent
          {...this.props}
          {...this.state}
          handleChange={this._handleChange}
          submit={this._submit}
        />
      );
    }
  };
}

export default Wrapper;
