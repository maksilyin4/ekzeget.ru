import React from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';
import userActions from '../../../dist/actions/User';
import { userLoggedIn, userRegIsLoading, userError } from '../../../dist/selectors/User';
import Form, { FormGroup, FormSubmit } from '../../Form/Form';
import TextField from '../../TextField/TextField';
import Wrapper from './Wrapper';
import Preloader from '../../Preloader/Preloader';
import ContactCaptcha from '../../Captcha/ContactCaptcha';

const PopupReg = ({
  userLoggedIn,
  userRegIsLoading,
  submit,
  handleChange,
  message,
  username,
  email,
  password,
  confirm_password,
  verifyCode,
}) => (
  <Form name="auth_form" className="auth-form" onSubmit={submit}>
    {userRegIsLoading && <Preloader />}
    <FormGroup>
      <TextField
        type="text"
        name="email"
        placeholder="E-mail *"
        value={email}
        onChange={handleChange('email')}
        required
      />
    </FormGroup>
    <FormGroup>
      <TextField
        type="text"
        name="username"
        placeholder="Имя пользователя *"
        value={username}
        onChange={handleChange('username')}
        required
      />
      </FormGroup>
    <FormGroup>
      <TextField
        type="password"
        name="password"
        placeholder="Пароль *"
        showPasswdControl={false}
        value={password}
        onChange={handleChange('password')}
        required
      />
    </FormGroup>
    <FormGroup>
      <TextField
        type="password"
        name="confirm_password"
        placeholder="Подтверждение пароля *"
        showPasswdControl={false}
        value={confirm_password}
        onChange={handleChange('confirm_password')}
        required
      />
    </FormGroup>
    <ContactCaptcha
      className="textfield__input"
      onChange={handleChange('verifyCode')}
      value={verifyCode}
    />
    {message !== '' && (
      <div
        className={cx('form__notice', {
          form__notice_success: userLoggedIn,
          form__notice_error: !userLoggedIn,
        })}>
        {message}
      </div>
    )}
    <FormSubmit title="Зарегистрироваться" align="center" />
  </Form>
);

const mapStateToProps = state => ({
  userRegIsLoading: userRegIsLoading(state),
  userLoggedIn: userLoggedIn(state),
  error: userError(state),
});
const mapDispatchToProps = {
  ...userActions,
};

export default connect(mapStateToProps, mapDispatchToProps)(Wrapper(PopupReg));
