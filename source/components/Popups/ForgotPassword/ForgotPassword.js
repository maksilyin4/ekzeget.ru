import R from 'ramda';
import React, { PureComponent } from 'react';

import Form, { FormGroup, FormSubmit } from '../../Form/Form';
import TextField from '../../TextField/TextField';
import { validateEmail } from '../../../dist/utils';
import { AXIOS } from '../../../dist/ApiConfig';
import ContactCaptcha from '../../Captcha/ContactCaptcha';

class PopupForgotPassword extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      login: '',
      success: false,
      message: '',
      isFetching: false,
      verifyCode: '',
    };
  }

  _handleChange = field => ({ target: { value } }) => {
    this.setState({
      [field]: value,
      message: '',
    });
  };

  _submit = e => {
    e.preventDefault();
    const { login, verifyCode } = this.state;
    if (!login || (/@/.test(login) && !validateEmail(login))) {
      this.setState({
        message: 'Введён некорректный email',
      });
    } else {
      this.setState({
        isFetching: true,
      });
      const data = new FormData();
      data.append('login', login);
      data.append('captcha', verifyCode);

      AXIOS.post('/user-profile/reset-password/', data)
        .then(({ data }) => {
          if (data.status === 'ok') {
            this.setState({
              success: true,
              isFetching: false,
            });
          } else {
            this.setState({
              message: data.error.message,
              isFetching: false,
            });
          }
        })
        .catch(error => {
          this.setState({
            message: R.pathOr('Ошибка', ['message'], error.response.data.error),
            isFetching: false,
          });
        });
    }
  };

  render() {
    const { success, isFetching, verifyCode } = this.state;
    return !success ? (
      <Form action="" name="forgot_form" className="auth-form" onSubmit={this._submit}>
        <div className="form__decriptor">
          Введите e-mail, который Вы указали при регистрации, и через несколько минут Вам придет
          письмо, содержащее инструкцию для получения нового пароля.
        </div>
        <FormGroup>
          <TextField
            type="text"
            name="email"
            placeholder="E-mail или имя пользователя *"
            required
            onChange={this._handleChange('login')}
          />
        </FormGroup>
        <ContactCaptcha
          className="textfield__input"
          onChange={this._handleChange('verifyCode')}
          value={verifyCode}
        />
        {this.state.message !== '' && (
          <div className="form__notice form__notice_error">{this.state.message}</div>
        )}
        <FormSubmit title="Восстановить" align="center" isPreLoader={isFetching} />
      </Form>
    ) : (
      <div className="form__decriptor">
        На Ваш e-mail отправлено письмо, содержащее инструкцию, для получения нового пароля.
      </div>
    );
  }
}

export default PopupForgotPassword;
