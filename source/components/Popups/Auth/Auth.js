import React, { Component } from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';

import { LS, Popup } from '~dist/browserUtils';
import { popupClose } from '../../../dist/utils';

import userActions from '../../../dist/actions/User';
import {
  userLoggedIn,
  userAuthIsLoading,
  userError,
  userIdentityProviders,
} from '../../../dist/selectors/User';

import Form, { FormGroup, FormSubmit } from '../../Form/Form';
import TextField from '../../TextField/TextField';
import PopupForgotPassword from '../ForgotPassword/ForgotPassword';
import { preloadLk } from '../../PreloadChunk';
import Preloader from '../../Preloader/Preloader';

import './auth.scss';
import { AXIOS } from '~dist/ApiConfig';
import { Link } from 'react-router-dom';

class PopupAuth extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      deviceInfo: JSON.stringify({ device_info: navigator.userAgent }),
      message: '',
    };
  }

  componentDidMount() {
    this.props?.getIdentityProviders();
  }

  componentDidUpdate = prevProps => {
    if (prevProps.userLoggedIn !== this.props.userLoggedIn) {
      this.setState({ message: 'Вы успешно аутентифицированы' }, () => {
        popupClose(1000);
      });
      this.props.getUser();
    }
  };

  _handleChange = field => ({ target: { value } }) => {
    this.setState({
      [field]: value,
      message: '',
    });
  };

  _forgotPopup = () => {
    Popup.close();
    Popup.create(
      {
        title: 'Восстановление пароля',
        content: <PopupForgotPassword />,
        className: 'popup_reg not-header',
      },
      true,
    );
  };

  _submit = e => {
    e.preventDefault();
    if (this.props.userAuthIsLoading) return;
    const { email, password, deviceInfo } = this.state;

    if (!email.trim() || !password.trim() || !/^([a-zA-Z0-9]{6,})$/.test(password)) {
      this.setState({
        message:
          'Пароль должен состоять не менее чем из 6 символов и состоять только из латинских букв и цифр',
      });
    } else {
      const data = new FormData();
      data.append('email', email);
      data.append('password', password);
      data.append('device_info', deviceInfo);

      preloadLk();
      this.props
        .authUser(data)
        .then(response => {
          if (response.error) {
            this.setState({
              message: response.error,
            });
          } else {
            popupClose(1000);
          }
        })
        .catch(() => this.setState({ message: 'Ошибка сервера, повторите позже' }));
    }
  };

  proceedToOAauth = provider => {
    LS.set('oauthReturnUrl', window.location.href);
    window.location.href = `${AXIOS.defaults.baseURL}oauth2/${provider}`;
  };

  render() {
    const { userLoggedIn, userAuthIsLoading, identityProviders } = this.props;

    return (
      <div className="popup__content">
        <Form action="" name="auth_form" className="auth-form" onSubmit={this._submit}>
          {userAuthIsLoading && <Preloader />}
          <FormGroup>
            <TextField
              type="text"
              name="email"
              placeholder="E-mail"
              required
              onChange={this._handleChange('email')}
            />
          </FormGroup>
          <FormGroup>
            <TextField
              type="password"
              name="password"
              showPasswdControl={false}
              placeholder="Пароль"
              rightSlot={
                <div className="auth-form__forgot-password">
                  <Link
                    className="auth-form__forgot-password-link"
                    to="/reset-password"
                    onClick={() => popupClose(0)}>
                    Забыли?
                  </Link>
                </div>
              }
              required
              onChange={this._handleChange('password')}></TextField>
          </FormGroup>
          {this.state.message !== '' && (
            <div
              className={cx(
                'form__notice',
                userLoggedIn ? 'form__notice_success' : 'form__notice_error',
              )}>
              {this.state.message}
            </div>
          )}
          <FormSubmit title="Войти" align="center" />
        </Form>

        {identityProviders.length ? (
          <div className="single-sign-on">
            Войти с помощью
            <div className="single-sign-on__providers">
              {identityProviders.map(provider => (
                <img
                  src={`/frontassets/images/identity-providers/${provider}.svg`}
                  key={provider}
                  className="single-sign-on__provider_logo"
                  alt={`Войти с помощью ${provider}`}
                  title={`Войти с помощью ${provider}`}
                  onClick={() => this.proceedToOAauth(provider)}
                />
              ))}
            </div>
            <div className="popup__content__divider">
              <span className="popup__content__divider__line"></span>
              <span className="popup__content__divider__text ">или</span>
              <span className="popup__content__divider__line"></span>
            </div>
          </div>
        ) : (
          <div />
        )}

        <Link to="/registration" className="auth__reg-button" onClick={() => popupClose(0)}>
          Зарегистрироваться
        </Link>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  userAuthIsLoading: userAuthIsLoading(state),
  userLoggedIn: userLoggedIn(state),
  error: userError(state),
  identityProviders: userIdentityProviders(state),
});
const mapDispatchToProps = { ...userActions };

export default connect(mapStateToProps, mapDispatchToProps)(PopupAuth);
