/* eslint-disable max-len */
import React from 'react';

import './regSuccess.scss';

const RegSuccess = () => (
  <div className="reg-success">
    <div className="reg-success__content">
      <p>На указанный Вами email отправлено письмо о подтверждении регистрации.</p>
      <p>
        После подтверждения Вы можете заполнить информацию о себе и подключить план чтений в личном
        кабинете.
      </p>
    </div>
  </div>
);

export default RegSuccess;
