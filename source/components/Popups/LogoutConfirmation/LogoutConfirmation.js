import React from 'react';
import {Popup} from '~dist/browserUtils';

import './styles.scss';
import {useCookies} from "react-cookie";
import {connect} from "react-redux";
import userActions from "~dist/actions/User";

const LogoutConfirmation = ({logoutUser}) => {
    const [cookies, setCookie, removeCookie] = useCookies(['userId']);

    function logout() {
        logoutUser();
        removeCookie('userId', {path: '/'});
        Popup.close();
    }

    return (
        <div className="logoutConfirmation__btn">
            <button className="btn red" onClick={() => Popup.close()}>
                Остаться
            </button>
            <button className="btn grey" onClick={logout}>
                Выйти
            </button>
        </div>
    );
};


const mapDispatchToProps = {
    ...userActions,
};

export default connect(() => ({}), mapDispatchToProps)(LogoutConfirmation);
