import React from 'react';
import Wrapper from './Wrapper';
import Preloader from '../../Preloader/Preloader';

// TODO: Need parser for verse and think about items.join, maybe it need refactoring
const AddFavorites = ({ isFetching, title, items = [], sendFavorites, closePopUp }) => (
  <div className="addFavorite__verse">
    {isFetching && <Preloader />}
    <header className="addFavorite__verse__header">
      <p>Добавить в избранное</p>
    </header>
    {items.length ? (
      <div className="addFavorite__verse__content">
        <div className="addFavorite__verse__title">
          <p>{`${title} ${items.join(', ')}`}</p>
        </div>
        <div className="addFavorite__verse__btn">
          <button className="btn" onClick={sendFavorites}>
            Добавить
          </button>
          <button className="addFavorite__verse__back" onClick={closePopUp}>
            Отменить
          </button>
        </div>
      </div>
    ) : (
      <div className="addFavorite__verse__title">
        <p>Вы ничего не выбрали</p>
      </div>
    )}
  </div>
);

export default Wrapper(AddFavorites);
