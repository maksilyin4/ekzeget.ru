import React, { Component } from 'react';
import qs from 'qs';

import { Popup } from '../../../dist/browserUtils';
import { _AXIOS } from '../../../dist/ApiConfig';
import { sendSocialActions } from '../../../dist/utils';

import '../AddInterpretation/styles.scss';

export default function withSubscription(WrappedComponent) {
  return class extends Component {
    state = {
      isFetching: false,
    };

    componentDidMount() {
      sendSocialActions({
        ga: ['event', 'event_name', { event_category: 'click_zvezdochka', event_action: 'add' }],
        ym: 'click_zvezdochka',
      });
    }

    closePopUp = () => {
      Popup.close();
    };

    sendFavorites = () => {
      if (this.state.isFetching) return;
      this.setState({ isFetching: true });
      // const { tags, color } = this.state;
      const {
        listIds,
        getUpdateInterpretation,
        handleClickFavInter,
        getFavoriteInter,
        addFavToStore,
        clearSelected,
      } = this.props;

      const data = {
        interpretation_id: listIds,
      };

      _AXIOS({
        method: 'PUT',
        url: 'user/interpretation-fav/',
        isAuth: true,
        data: qs.stringify(data),
      })
        .then(() => {
          getFavoriteInter();
          this.setState({ isFetching: false });
          addFavToStore(data);
          Popup.close();
          Popup.create(
            {
              title: null,
              content: (
                <div className="contact__successfull_popup">
                  <p className="contact__successfull_popup-interpretation">
                    Выбранное толкование добавлено в избранное
                  </p>
                </div>
              ),
              className: 'popup_auth not-header',
            },
            true,
          );
          clearSelected();

          sendSocialActions({
            ga: [
              'event',
              'event_name',
              { event_category: 'Dobavit_izbrannoe', event_action: 'add' },
            ],
            ym: 'Dobavit_izbrannoe',
          });

          handleClickFavInter();
          getUpdateInterpretation && getUpdateInterpretation();
        })
        .catch(() => {
          this.setState(
            { isFetching: false },
            () => getUpdateInterpretation && getUpdateInterpretation(),
          );
        });
    };

    render() {
      const { isFetching } = this.state;
      return (
        <WrappedComponent
          {...this.props}
          isFetching={isFetching}
          sendFavorites={this.sendFavorites}
          closePopUp={this.closePopUp}
        />
      );
    }
  };
}
