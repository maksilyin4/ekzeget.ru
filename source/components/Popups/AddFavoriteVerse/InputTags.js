import React, { Component } from 'react';

export default class InputTags extends Component {
  render() {
    const { tags, tag, handleDelTag, handleInput, handleSubmit } = this.props;
    const filteredTags = tags.filter(i => !!i);
    return (
      <div className="addFavorite__verse__tagForm">
        {filteredTags.length > 0 && (
          <div className="addFavorite__verse__tags">
            {filteredTags.map((filteredTag, index) => (
              // eslint-disable-next-line react/no-array-index-key
              <p key={`${filteredTag}-${index}`} onClick={() => handleDelTag(filteredTag)}>
                {filteredTag}
              </p>
            ))}
          </div>
        )}
        <form className="addFavorite__verse__form" onSubmit={handleSubmit}>
          <input
            className="addFavorite__verse__input"
            type="text"
            name="tag"
            value={tag}
            onChange={handleInput}
            placeholder="Введите теги через запятую"
            autoComplete="off"
          />
          <button type="submit">+</button>
        </form>
      </div>
    );
  }
}
