import R from 'ramda';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Popup } from '../../../dist/browserUtils';
import { requestFavVerse, postFavVerseSuccess } from './serviceAPI';
import { sendSocialActions } from '../../../dist/utils';

export default function withSubscription(WrappedComponent) {
  return class extends Component {
    static propTypes = {
      listIds: PropTypes.array, // ids list
      title: PropTypes.string.isRequired,
      items: PropTypes.array.isRequired, // numbers list
      clearSelected: PropTypes.func,
      addFavToStore: PropTypes.func,
    };

    state = {
      isFetching: false,
      tags: [],
      tag: '',
      color: '#F9D063',
      isColorOpen: false,
      default: 'test',
    };

    componentDidMount() {
      sendSocialActions({
        ga: ['event', 'event_name', { event_category: 'click_zvezdochka', event_action: 'add' }],
        ym: 'click_zvezdochka',
      });
      if (this.props.hasOwnProperty('tags') && this.props.hasOwnProperty('color')) {
        this.setState({
          tags: this.props.tags.split(','),
          color: this.props.color,
        });
      }
    }

    handleInput = event => {
      const { value } = event.currentTarget;
      if (/[а-яА-ЯёЁa-zA-Z1-9!?],$/g.test(value)) {
        this.setState(({ tags }) => ({
          tags: R.uniq([...tags, value.slice(0, -1).trim()]),
          tag: '',
        }));
      } else {
        this.setState({
          tag: value,
        });
      }
    };

    handleTagFromAll = i => {
      this.setState(({ tags }) => ({
        tags: R.uniq([...tags, i]),
      }));
    };

    handleSubmitTag = e => {
      e.preventDefault();
      const tag = this.state.tag.trim();
      if (/[а-яА-ЯёЁa-zA-Z1-9!?]$/g.test(tag)) {
        if (tag.length > 0) {
          this.setState(({ tags }) => ({
            tags: R.uniq([...tags, tag]),
            tag: '',
          }));
        }
      }
    };

    closePopUp = () => {
      Popup.close();
    };

    handleDelTag = tag => {
      this.setState(({ tags }) => ({
        tags: tags.filter(i => i !== tag),
      }));
    };

    sendFavorites = () => {
      const { tag } = this.state;
      if (this.state.isFetching) return;
      this.setState({ isFetching: true });
      const { tags, color } = this.state;
      const { listIds } = this.props;

      const data = {
        tags: tags.length > 0 ? tags.join(',') : tag,
        color,
        verse: listIds,
      };
      requestFavVerse(data, 'POST').then(
        res => {
          if (res.data.status === 'ok') {
            this.setState({ isFetching: false }, () => {
              postFavVerseSuccess(true);
            });
            this.props.clearSelected(); // почистить выборку
            this.props.addFavToStore(data);
            this.props.getFavoriteVerse();

            sendSocialActions({
              ga: [
                'event',
                'event_name',
                { event_category: 'Dobavit_izbrannoe', event_action: 'add' },
              ],
              ym: 'Dobavit_izbrannoe',
            });
          } else {
            this.setState({ isFetching: false });
          }
        },
        () => {
          this.setState({ isFetching: false });
        },
      );
    };

    patchFavorites = () => {
      if (this.state.isFetching) return;
      this.setState({ isFetching: true });
      const { tags, color } = this.state;
      const { id, listIds } = this.props;
      const data = {
        tags: tags.join(','),
        color,
        id,
      };
      requestFavVerse(data, 'PUT').then(
        res => {
          if (res.data.status === 'ok') {
            this.setState({ isFetching: false }, () => {
              postFavVerseSuccess(false);
            });
            this.props.updateFavVerse({
              tags: tags.join(','),
              color,
              verseId: listIds[0],
              id,
            });
          } else {
            this.setState({ isFetching: false });
          }
        },
        () => {
          this.setState({ isFetching: false });
        },
      );
    };

    toggleColor = () => {
      this.setState(({ isColorOpen }) => ({
        isColorOpen: !isColorOpen,
      }));
    };

    handleColor = hex => {
      this.setState({ color: hex.hex }, () => this.toggleColor());
    };

    render() {
      return (
        <WrappedComponent
          {...this.props}
          {...this.state}
          handleDelTag={this.handleDelTag}
          handleInput={this.handleInput}
          handleTagFromAll={this.handleTagFromAll}
          handleSubmitTag={this.handleSubmitTag}
          sendFavorites={this.sendFavorites}
          closePopUp={this.closePopUp}
          handleColor={this.handleColor}
          toggleColor={this.toggleColor}
          patchFavorites={this.patchFavorites}
        />
      );
    }
  };
}
