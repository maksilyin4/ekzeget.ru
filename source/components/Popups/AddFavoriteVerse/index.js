import React, { Component } from 'react';
import { connect } from 'react-redux';

import { CirclePicker } from 'react-color';
import { getAlltags } from '../../../dist/selectors/favorites';

import Wrapper from './Wrapper';
import Preloader from '../../Preloader/Preloader';
import InputTags from './InputTags';
import { defaultColors } from './defaultColors';
import './addFavorites.scss';

// TODO: Need parser for verse and think about items.join, maybe it need refactoring
// при наличии в props id избранного данная форма подстраивается под редактирование

class AddFavorites extends Component {
  render() {
    const {
      tag,
      tags,
      isFetching,
      title,
      items = [],
      handleDelTag,
      handleInput,
      handleSubmitTag,
      sendFavorites,
      patchFavorites,
      closePopUp,
      color,
      handleColor,
      isColorOpen,
      toggleColor,
      allTags,
      handleTagFromAll,
    } = this.props;
    return (
      <div className="addFavorite__verse">
        {isFetching && <Preloader />}
        <header className="addFavorite__verse__header">
          <p>Добавить в избранное</p>
          <div className="addFavorite__verse__color">
            <p>Выберите цвет</p>
            <div
              className="addFavorite__verse__color-btn"
              onClick={toggleColor}
              style={{ backgroundColor: this.props.color }}
            />
            {isColorOpen && (
              <CirclePicker
                width={220}
                color={color}
                onChangeComplete={handleColor}
                colors={defaultColors}
              />
            )}
          </div>
        </header>
        {items.length > 0 ? (
          <div
            ref={el => {
              this.app = el;
            }}
            className="addFavorite__verse__content">
            <div className="addFavorite__verse__title">
              <p>{`${title} ${items.join(', ')}`}</p>
            </div>
            <InputTags
              tags={tags}
              tag={tag}
              handleDelTag={handleDelTag}
              handleInput={handleInput}
              handleSubmit={handleSubmitTag}
            />
            <ul className="addFavorite__verse__allTags">
              {allTags.map(
                i =>
                  i && (
                    <li key={i} className="allTags__item" onClick={() => handleTagFromAll(i)}>
                      {i}
                    </li>
                  ),
              )}
            </ul>
            <div className="addFavorite__verse__btn">
              {this.props.hasOwnProperty('id') ? (
                <button className="btn" onClick={patchFavorites}>
                  Изменить
                </button>
              ) : (
                <button className="btn" onClick={sendFavorites}>
                  Добавить
                </button>
              )}
              <button className="addFavorite__verse__back" onClick={closePopUp}>
                Отменить
              </button>
            </div>
          </div>
        ) : (
          <div className="addFavorite__verse__title">
            <p>Вы ничего не выбрали</p>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  allTags: getAlltags(state),
});

export default connect(mapStateToProps)(Wrapper(AddFavorites));
