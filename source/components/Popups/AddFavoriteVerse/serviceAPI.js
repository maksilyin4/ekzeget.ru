import React from 'react';
import qs from 'qs';

import { Popup, LS } from '../../../dist/browserUtils';
import { AXIOS } from '../../../dist/ApiConfig';

export function requestFavVerse(data, type) {
  return AXIOS({
    method: type,
    url: 'user/verse-fav/',
    headers: {
      'X-Authentication-Token': LS.get('token'),
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    data: qs.stringify(data),
  });
}

export function postFavVerseSuccess(type) {
  Popup.close();
  Popup.create(
    {
      title: null,
      content: (
        <div className="contact__successfull_popup">
          <p className="contact__successfull_popup-interpretation">
            {type
              ? 'Выбранные стихи добавлены в избранное'
              : 'Параметры выбранного стиха были изменены'}
          </p>
        </div>
      ),
      className: 'popup_auth not-header',
    },
    setTimeout(() => {
      Popup.close();
    }, 1500),
    true,
  );
}
