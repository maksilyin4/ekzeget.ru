import R from 'ramda';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Select from 'react-select';
import { toolbarConfig } from './toolbarConfig';

import Wrapper from './Wrapper';
import Preloader from '../../Preloader/Preloader';
import './styles.scss';

// Отдельно от browserUtils, иначе попадет в общий вендор
const RichTextEditor = process.env.BROWSER ? require('react-rte').default : null;

class AddInterpretation extends Component {
  render() {
    const {
      isFetching,
      exegetes,
      title,
      handleSelect,
      ekzeget,
      text,
      handleInput,
      handleSubmit,
      error,
      onClick,
      verseListData,
      handleSelectExtraVerses,
      currentVerse,
      extraVerses,
      onClickDeleteVerse,
    } = this.props;
    return (
      <div className="addInterpretation">
        {isFetching && <Preloader />}
        <header className="addInterpretation__header">
          <span className="addInterpretation__description">Добавить толкование</span>
          <Link
            to="/add-interpretation-rules"
            target="_blank"
            className="addInterpretation__rules-link">
            Правила по добавлению толкований
          </Link>
        </header>
        <div className="addInterpretation__subheader">
          <h3 className="addInterpretation__description">{title}</h3>
          <button className="btn addInterpretation__add" onClick={handleSubmit}>
            Добавить
          </button>
        </div>
        <div className="addInterpretation__actions">
          <div className="addInterpretation__select">
            <Select
              options={R.pathOr([{ id: 0, name: 'Загрузка...' }], ['ekzeget'], exegetes).map(i => ({
                value: i.id,
                label: i.name,
              }))}
              placeholder="Выберите экзегета"
              maxMenuHeight={500}
              clearable={false}
              onChange={handleSelect}
              value={ekzeget}
              toolbarConfig={toolbarConfig}
              classNamePrefix="add-interp"
              isSearchable
            />
          </div>

          <div className="addInterpretation__select">
            <Select
              options={verseListData.map(i => ({
                value: i.id,
                label: `Стих ${i.number}`,
              }))}
              maxMenuHeight={500}
              placeholder="Добавить стих"
              clearable={false}
              onChange={handleSelectExtraVerses}
              value={currentVerse}
              toolbarConfig={toolbarConfig}
              classNamePrefix="add-interp"
              isSearchable
            />
          </div>
          {!!error && <p className="addInterpretation__error">{error}</p>}
        </div>
        {extraVerses.length > 0 && (
          <div className="addInterpretation__stickers">
            {extraVerses.map(i => (
              <span
                key={i.value}
                className="addInterpretation__sticker"
                onClick={() => onClickDeleteVerse(i.value)}>
                {i.label}
              </span>
            ))}
          </div>
        )}

        <div className="addInterpretation__textarea">
          <button onClick={() => onClick(1)} className="addInterpretation__extra-buttons source">
            Источник +++
          </button>
          <button onClick={() => onClick(2)} className="addInterpretation__extra-buttons note">
            Примечание ===
          </button>
          <RichTextEditor
            placeholder="Введите текст толкования..."
            name="text"
            cols="30"
            rows="6"
            value={text}
            onChange={handleInput}
            toolbarConfig={toolbarConfig}
          />
        </div>
      </div>
    );
  }
}

export default Wrapper(AddInterpretation);
