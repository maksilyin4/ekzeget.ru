import R from 'ramda';
import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import qs from 'qs';

import { Popup, LS } from '../../../dist/browserUtils';
import exegetesActions from '../../../dist/actions/Exegetes';
import { getChapter } from '../../../dist/actions/Chapter';

import { getExegetes } from '../../../dist/selectors/Exegetes';
import { getIsMobile } from '../../../dist/selectors/screen';
import { getVerseIDs } from '../../../dist/selectors/Chapter';

import { AXIOS } from '../../../dist/ApiConfig';
import { sendSocialActions } from '../../../dist/utils';
import './styles.scss';

const RichTextEditor = process.env.BROWSER ? require('react-rte').default : null;

function Wrapper(WrappedComponent) {
  return class extends Component {
    constructor(props) {
      super(props);
      this.state = {
        isFetching: false,
        ekzeget: null,
        currentVerse: null,
        text: RichTextEditor.createEmptyValue(),
        error: '',
        textForExtraActions: '',
        extraVerses: [],
      };
    }

    componentDidMount() {
      const { bookCode, chapterId, verseId, verseListFull = [] } = this.props;

      // if (verseId && verseList.length) {
      if (verseId && verseListFull.length) {
        const { number, id } = verseListFull.find(({ id }) => +id === +verseId) || {};

        if (number && id) {
          this.setState({
            extraVerses: [...this.state.extraVerses, { value: id, label: `Стих ${number}` }],
          });
        }
      }

      if (!this.props.exegetes.hasOwnProperty('ekzeget')) {
        this.props.getExegetes();
      }

      sendSocialActions({
        ga: ['event', 'event_name', { event_category: 'click_krestik', event_action: 'click' }],
        ym: 'click_krestik',
      });

      this.props.getChapter(bookCode, chapterId);
    }

    onClick = number => {
      const { textForExtraActions, text } = this.state;
      const extraText = textForExtraActions
        .toString('html')
        .replace(/<\/?[^>]+>/g, '')
        .trim();
      let replacement = '';
      const textContainer = text.toString('html');
      if (number === 1) {
        replacement = textContainer.replace(extraText, `+++${extraText}++`);
      } else {
        replacement = textContainer.replace(extraText, `===${extraText}==`);
      }

      if (extraText.length > 0) {
        this.setState({
          textForExtraActions: '',
          text: RichTextEditor.createValueFromString(replacement, 'html'),
        });
      }
    };

    onClickDeleteVerse = verseId => {
      const { extraVerses, currentVerse } = this.state;
      const filteredVerses = extraVerses.filter(verse => verse.value !== verseId);
      const currentVerseChange = +currentVerse?.value !== +verseId ? currentVerse : null;
      this.setState({
        extraVerses: filteredVerses,
        currentVerse: currentVerseChange,
      });
    };

    handleSelect = e => {
      this.setState({
        ekzeget: e,
        error: '',
      });
    };

    handleSelectExtraVerses = e => {
      if (!this.state.extraVerses.some(i => i.value === e.value)) {
        this.setState(prevState => ({
          extraVerses: [...prevState.extraVerses, e],
          currentVerse: e,
        }));
      }
    };

    closePopUp = () => {
      Popup.close();
    };

    handleSubmit = () => {
      const { isFetching, text, ekzeget, extraVerses } = this.state;
      if (isFetching) return;
      const isEmpty = ['', '\u200b'].includes(text.toString('markdown').trim());
      if (!ekzeget) {
        this.setState({
          error: 'Вы не выбрали толковника',
        });
      } else if (isEmpty) {
        this.setState({
          error: 'Вы не ввели текст толкования',
        });
      } else {
        this.setState({ isFetching: true });
        const data = {
          interpretator: ekzeget.value,
          verse: [...extraVerses.map(i => i.value)],
          text: text.toString('html'),
        };
        AXIOS({
          method: 'POST',
          url: 'user/interpretation-add/',
          headers: {
            'X-Authentication-Token': LS.get('token'),
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          data: qs.stringify(data),
        }).then(
          ({ data }) => {
            if (data.status === 'ok') {
              this.setState({ isFetching: false }, () => {
                Popup.close();
                Popup.create(
                  {
                    title: null,
                    content: (
                      <div className="contact__successfull_popup">
                        <p className="contact__successfull_popup-interpretation">
                          Толкование отправлено модератору
                        </p>
                      </div>
                    ),
                    className: 'popup_auth not-header',
                  },
                  true,
                );
              });
              sendSocialActions({
                ga: [
                  'event',
                  'event_name',
                  { event_category: 'Dobavit_tolkovanie', event_action: 'add' },
                ],
                ym: 'Dobavit_tolkovanie',
              });
            } else {
              this.setState({
                isFetching: false,
                error: R.pathOr(
                  'Толкование уже добавлено',
                  ['response', 'data', 'error', 'name'],
                  'error',
                ),
              });
            }
          },
          error => {
            this.setState({
              isFetching: false,
              error: R.pathOr(
                'Ошибка сервера, попробуйте позже',
                ['response', 'data', 'error', 'name'],
                error,
              ),
            });
          },
        );
      }
    };

    handleInput = value => {
      this.setState({
        text: value,
        error: '',
      });

      let txt = '';
      if (window.getSelection) {
        txt = window.getSelection().toString();
        if (txt.length !== 0) {
          this.setState({
            textForExtraActions: txt,
          });
        }
      } else if (document.selection && document.selection.type !== 'Control') {
        txt = document.selection.createRange().text;
        if (txt.length !== 0) {
          this.setState({
            textForExtraActions: txt,
          });
        }
      }
    };

    render() {
      const { ekzeget, text, error, isFetching, currentVerse, extraVerses } = this.state;

      const verseListData = this.props.verseListFull.filter(({ id }) =>
        extraVerses.every(({ value }) => +value !== +id),
      );

      return (
        <WrappedComponent
          {...this.props}
          isFetching={isFetching}
          closePopUp={this.closePopUp}
          handleSelect={this.handleSelect}
          handleSelectExtraVerses={this.handleSelectExtraVerses}
          ekzeget={ekzeget}
          text={text}
          handleInput={this.handleInput}
          handleSubmit={this.handleSubmit}
          error={error}
          onClick={this.onClick}
          showMoreButtonsMobile={this.showMoreButtonsMobile}
          currentVerse={currentVerse}
          extraVerses={extraVerses}
          verseListData={verseListData}
          onClickDeleteVerse={this.onClickDeleteVerse}
        />
      );
    }
  };
}

const mapStateToProps = (store, ownProps) => ({
  exegetes: getExegetes(store),
  title: ownProps.title,
  isMobile: getIsMobile(store),
  verseList: getVerseIDs(store),
  verseListFull: store?.chapter?.data?.book?.chapters[0]?.verses,
});

const mapDispatchToProps = {
  ...exegetesActions,
  getChapter,
};

const composeHOC = compose(connect(mapStateToProps, mapDispatchToProps), Wrapper);

export default composeHOC;
