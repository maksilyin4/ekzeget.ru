import React from 'react';
import Loadable from 'react-loadable';
import Preloader from '../../Preloader/Preloader';

const LoadableBar = Loadable({
  loader: () => import('./AddInterpretation'),
  loading() {
    return (
      <div className="preloader-relative">
        <Preloader />
      </div>
    );
  },
  render(loaded, props) {
    const Component = loaded.default;
    return <Component {...props} />;
  },
  delay: 200,
});

export default LoadableBar;
