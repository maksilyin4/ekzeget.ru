import React from 'react';
import Preloader from '../../Preloader/Preloader';
import Wrapper from './Wrapper';

import './styles.scss';

const AddNote = ({
  isFetching,
  text,
  title,
  sendNote,
  closePopUp,
  handleInput,
  updateNote,
  noteId,
  error,
}) => (
  <form className="addFavorite__verse addNote" onSubmit={sendNote}>
    {isFetching && <Preloader />}
    <div className="addFavorite__verse__header">
      <p>{noteId ? 'Изменить заметку' : 'Добавить заметку'}</p>
    </div>
    <div className="addFavorite__verse__content">
      <div className="addFavorite__verse__title">
        <p>{title}</p>
      </div>
      <div className="addFavorite__verse__form">
        <textarea
          className="addFavorite__verse__input"
          type="text"
          name="text"
          cols="30"
          rows="5"
          value={text}
          onChange={handleInput}
          placeholder="Введите заметку"
          autoComplete="off"
          required
        />
      </div>
    </div>
    {!!error && <p className="add-error">{error}</p>}
    <div className="addFavorite__verse__btn">
      <button className="btn" onClick={noteId ? updateNote : sendNote} type="submit">
        {noteId ? 'Изменить' : 'Добавить'}
      </button>
      <button className="addFavorite__verse__back" onClick={closePopUp} type="reset">
        Отменить
      </button>
    </div>
  </form>
);

export default Wrapper(AddNote);
