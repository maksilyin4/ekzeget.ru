import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Popup } from '../../../dist/browserUtils';

import { createNote, updateNotes, getNotes } from '../../../dist/actions/favorites';

import { getIsFetching, getNotes as selectorNotes } from '../../../dist/selectors/favorites';
import { userLoggedIn } from '../../../dist/selectors/User';

const mapStateToProps = store => ({
  isFetching: getIsFetching(store),
  noteList: selectorNotes(store),
  userLoggedIn: userLoggedIn(store),
});

const mapDispatchToProps = {
  createNote,
  updateNotes,
  getNotes,
};

function Wrapper(WrappedComponent) {
  return class Wrapper extends Component {
    state = {
      text: '',
      error: '',
      noteId: null,
    };

    componentDidMount() {
      // NOTE: this was created for create notes, because note_id is not defined after creating note
      const { userLoggedIn, getNotes } = this.props;
      !userLoggedIn && getNotes();
    }

    componentDidUpdate(prevProps) {
      if (this.props.noteList.length > 0 && prevProps.isFetching && !this.props.isFetching) {
        const { sendData, noteList } = this.props;
        const isNoteAdded = noteList.find(i => i.short === sendData);
        this.setState({
          text: isNoteAdded ? isNoteAdded.text : '',
          noteId: isNoteAdded ? isNoteAdded.id : null,
        });
      }
    }

    closePopUp = () => {
      Popup.close();
    };

    handleInput = event => {
      const { value } = event.currentTarget;
      this.setState({ text: value, error: '' });
    };

    sendNote = e => {
      e.preventDefault();
      if (this.props.isFetching) return;
      const { text } = this.state;
      if (!text.trim()) {
        this.setState({
          error: 'Вы ничего не написали',
        });
        return;
      }
      const { sendData, createNote } = this.props;
      const data = {
        verse: String(sendData),
        text,
      };

      createNote(data).then(({ error = null }) => {
        if (error) {
          this.setState({ error: 'Такая заметка уже существует' });
        } else {
          this.successPopUp('Заметка сохранена');
        }
      });
    };

    updateNote = e => {
      e.preventDefault();
      if (this.props.isFetching) return;
      const { text, noteId } = this.state;
      if (!text.trim()) {
        this.setState({
          error: 'Вы ничего не написали',
        });
        return;
      }
      const { updateNotes } = this.props;
      const data = {
        note_id: noteId,
        text,
      };

      updateNotes(data).then(({ error = null }) => {
        if (error) {
          this.setState({ error });
        } else {
          this.successPopUp('Заметка сохранена');
        }
      });
    };

    successPopUp = text => {
      Popup.close();
      Popup.create(
        {
          title: null,
          content: (
            <div className="contact__successfull_popup">
              <p>{text}</p>
            </div>
          ),
          className: 'popup_auth not-header',
        },
        true,
      );
    };

    render() {
      return (
        <WrappedComponent
          {...this.props}
          {...this.state}
          closePopUp={this.closePopUp}
          sendNote={this.sendNote}
          handleInput={this.handleInput}
          updateNote={this.updateNote}
        />
      );
    }
  };
}

const composeHOC = compose(connect(mapStateToProps, mapDispatchToProps), Wrapper);

export default composeHOC;
