import R from 'ramda';
import React, { Component } from 'react';
import qs from 'qs';

import { Popup, LS } from '../../../dist/browserUtils';
import { validateEmail, sendSocialActions } from '../../../dist/utils';
import { AXIOS } from '../../../dist/ApiConfig';
import './styles.scss';

export default function(WrappedComponent) {
  return class componentName extends Component {
    state = {
      isFetching: false,
      text: '',
      name: '',
      email: '',
      verifyCode: '',
      error: '',
    };

    handleInput = e => {
      const { name, value } = e.currentTarget;
      this.setState({
        [name]: value,
        error: '',
      });
    };

    handleSubmit = e => {
      e.preventDefault();
      const { name, email, text, isFetching, verifyCode } = this.state;
      if (isFetching) return;
      if (!name.trim()) return this.setState({ error: 'Введите логин' });
      if (!text.trim()) return this.setState({ error: 'Введите текст сообщения' });
      if (!validateEmail(email)) {
        return this.setState({ error: 'Указан некорректный email' });
      }
      this.setState({ isFetching: true });
      const data = qs.stringify({
        body: text,
        sendTo: 'bible-group',
        captcha: verifyCode,
        name,
        email,
      });
      AXIOS.post('site/contact/', data, {
        headers: {
          'X-Authentication-Token': LS.get('token'),
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      }).then(
        ({ data }) => {
          if (data.status === 'ok') {
            this.setState({ isFetching: false }, () => {
              Popup.close();
              Popup.create(
                {
                  title: null,
                  content: (
                    <div className="contact__successfull_popup">
                      <p>Ваше сообщение отправлено</p>
                    </div>
                  ),
                  className: 'popup_auth not-header',
                },
                true,
              );
            });
            sendSocialActions({
              ga: ['event', 'event_name', { event_category: 'Zadat_vopros', event_action: 'send' }],
              ym: 'Zadat_vopros',
            });
          } else {
            this.setState({
              isFetching: false,
              error: R.pathOr('Ошибка при отправке сообщения', ['error', 'message'], data),
            });
          }
        },
        error => {
          this.setState({
            isFetching: false,
            error: R.pathOr(
              'Ошибка сервера, попробуйте позже',
              ['response', 'data', 'error', 'name'],
              error,
            ),
          });
        },
      );
    };

    render() {
      return (
        <WrappedComponent
          {...this.props}
          {...this.state}
          handleInput={this.handleInput}
          handleSubmit={this.handleSubmit}
        />
      );
    }
  };
}
