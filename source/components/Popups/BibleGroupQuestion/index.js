import React, { Component } from 'react';
import Wrapper from './Wrapper';
import Form, { FormGroup } from '../../Form/Form';
import './styles.scss';
import Preloader from '../../Preloader/Preloader';
import ContactCaptcha from '../../Captcha/ContactCaptcha';

class BibleGroupQuestion extends Component {
  render() {
    const {
      isFetching,
      name,
      email,
      text,
      error,
      verifyCode,
      handleSubmit,
      handleInput,
    } = this.props;
    return (
      <div>
        {isFetching && <Preloader />}
        <h1>Задайте ваш вопрос</h1>
        <p>Мы постараемся ответить на него как можно скорее</p>
        <Form className="bible-group__question__form" onSubmit={handleSubmit}>
          <FormGroup className="user_form_input">
            <label htmlFor="bible-group-text-question">
              Ваш вопрос
              <textarea
                name="text"
                id="bible-group-text-question"
                cols="30"
                rows="5"
                autoComplete="off"
                value={text}
                onChange={handleInput}
                required
              />
            </label>
          </FormGroup>
          <FormGroup className="user_form_input">
            <label htmlFor="bible-group-name-question">
              Ваше имя*
              <input
                type="text"
                name="name"
                id="bible-group-name-question"
                autoComplete="off"
                value={name}
                onChange={handleInput}
                required
              />
            </label>
          </FormGroup>
          <FormGroup className="user_form_input">
            <label htmlFor="bible-group-email-question">
              Ваше email*
              <input
                type="email"
                name="email"
                id="bible-group-email-question"
                autoComplete="off"
                value={email}
                onChange={handleInput}
                required
              />
            </label>
          </FormGroup>
          <ContactCaptcha onChange={handleInput} value={verifyCode} />
          {!!error && <p style={{ textAlign: 'center', color: 'red' }}>{error}</p>}
          <button className="btn" type="submit">
            Отправить
          </button>
        </Form>
      </div>
    );
  }
}

export default Wrapper(BibleGroupQuestion);
