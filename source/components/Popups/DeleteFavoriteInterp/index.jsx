import React, { PureComponent } from 'react';

import { Popup } from '../../../dist/browserUtils';

class DeleteFavoriteInterp extends PureComponent {
  closePopUp = () => {
    Popup.close();
  };

  deleteFavorite = () => {
    const { deleteFavorite, currentId, handleClickFavInter } = this.props;
    deleteFavorite(currentId);
    handleClickFavInter();
    Popup.close();
    Popup.create({
      title: null,
      content: (
        <div className="contact__successfull_popup">
          <p>Выбранное толкование удалено из избранного</p>
        </div>
      ),
      className: 'popup_auth not-header',
    });
  };

  render() {
    const { title, items } = this.props;

    return (
      <div className="deleteFavorite__verse addFavorite__verse">
        <header className="deleteFavorite__verse-text addFavorite__verse__header">
          <p>Удалить из избранного?</p>
        </header>
        <div className="addFavorite__verse__content">
          <div className="addFavorite__verse__title">
            <p>{`${title} ${items.join(', ')}`}</p>
          </div>
          <div className="addFavorite__verse__btn">
            <button className="btn" onClick={this.deleteFavorite}>
              Удалить
            </button>
            <button className="addFavorite__verse__back" onClick={this.closePopUp}>
              Отменить
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default DeleteFavoriteInterp;
