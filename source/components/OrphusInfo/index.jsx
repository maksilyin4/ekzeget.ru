import React from 'react';

import './styles.scss';

export default () => (
  <div className="orphus orphus__info">
    <p className="orphus__title">Ошибка в тексте ?</p>
    <div className="orphus__main-container">
      <p className="orphus__main-text">Выделите ее мышкой и нажмите</p>
      <div className="orphus__main-keywords">
        <span className="orphus__keyword"> Ctrl </span>+
        <span className="orphus__keyword"> Enter </span>
      </div>
    </div>
  </div>
);
