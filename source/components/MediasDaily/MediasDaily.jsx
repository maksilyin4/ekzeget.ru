import React from 'react';
import useDailyMediasQuery from '../../apolloClient/query/mediaLibrary/mediasDaily';
import Preloader from '../Preloader/Preloader';

const MediasDaily = () => {
    const { medias, loading } = useDailyMediasQuery();

    if (loading) {
        return (
            <div className="preloader-relative">
                <Preloader />
            </div>
        );
    }

    return (
        <div>
            {medias.map(media => (
                <p key={media.id}>[{media.genre ? media.genre.title : '-'}] {media.title}<br/>{media.description}</p>
            ))}
        </div>
    );
};

export default MediasDaily;
