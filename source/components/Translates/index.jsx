import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import Select from 'react-select';

import {
  getTranslatesForSelect,
  getCurrentTranslate,
  getTranslatesForSelectLk,
  getCurrentTranslateLk,
} from '../../dist/selectors/translates';

import { useTranslate } from '~utils/useTranslate';

import './translates.scss';
import classNames from 'classnames';

const TranslatesSelect = ({ isLk, isPlan }) => {
  const translates = useSelector(getTranslatesForSelect);
  const translatesLk = useSelector(getTranslatesForSelectLk);
  const currentTranslate = useSelector(getCurrentTranslate);
  const currentTranslateLk = useSelector(getCurrentTranslateLk);

  const setTranslate = useTranslate();

  const handleChange = e => {
    setTranslate(e);
  };

  const props = useMemo(
    () =>
      isLk
        ? {
            options: translatesLk,
            defaultValue: translatesLk[0],
            value: currentTranslateLk,
          }
        : { options: translates, value: currentTranslate },

    [isLk, translatesLk, currentTranslateLk, translates, currentTranslate],
  );

  return (
    <div className={classNames('translates-select', isPlan && 'translates-select-plan')}>
      <Select
        isSearchable={false}
        onChange={handleChange}
        classNamePrefix="custom-select"
        {...props}
      />
    </div>
  );
};

TranslatesSelect.propTypes = {
  isLk: PropTypes.bool,
};

export default TranslatesSelect;
