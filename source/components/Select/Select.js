/* eslint-disable max-len */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Icon from '../Icons/Icons';

import './select.scss';

class Select extends Component {
  static propTypes = {
    className: PropTypes.string,
    // defaultText: PropTypes.string,
    options: PropTypes.array.isRequired,
    callBack: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.state = {
      isOpened: false,
      activeItem: this.props.activeItem ? this.props.activeItem : this.props.options[0].value,
    };
  }

  componentWillUnmount() {
    if (typeof document === 'undefined') return;
    document.body.removeEventListener('click', this.closeOptions);
  }

  getDefaultText = () =>
    this.props.activeItem
      ? this.props.options
          .filter(option => option.value === this.props.activeItem)
          .map(option => option.name)
      : this.props.options[0].name;

  _toggleOptions = () => {
    if (typeof document === 'undefined') return;
    this.setState({ isOpened: !this.state.isOpened }, () => {
      if (this.state.isOpened) {
        document.body.addEventListener('click', this.closeOptions);
      } else {
        document.body.removeEventListener('click', this.closeOptions);
      }
    });
  };

  closeOptions = () => {
    this.setState({ isOpened: false });
  };

  // _selectValue = (value, name) => {
  _selectValue = value => {
    this.setState({
      // defaultText: name,
      isOpened: false,
      activeItem: value,
    });
    this.props.callBack(value);
  };

  render() {
    return (
      <div className={cx('select', this.props.className)}>
        <button className="select__value" onClick={this._toggleOptions}>
          {this.getDefaultText()}
          <Icon icon={this.state.isOpened ? 'angle-up' : 'angle-down'} />
        </button>
        <div className={cx('select__options', { active: this.state.isOpened })}>
          {this.props.options
            .filter(option => option.value !== this.state.activeItem)
            .map((option, i) => (
              <div
                /* eslint-disable-next-line react/no-array-index-key */
                key={i}
                className="select__option"
                onClick={() => this._selectValue(option.value, option.name)}>
                {option.name}
              </div>
            ))}
        </div>
      </div>
    );
  }
}

export default Select;
