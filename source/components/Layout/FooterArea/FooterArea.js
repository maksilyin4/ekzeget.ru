import React from 'react';
import { useSelector } from 'react-redux';

import { getIsMobile, getIsTablet } from '../../../dist/selectors/screen';

import { LS } from '../../../dist/browserUtils';
import Social from '../../Social/Social';
import FooterDonations from './FooterDonations';
import FooterPartners from './FooterPartners';
import FooterCopyright from './FooterCopyright';
import FooterNav from './FooterNav';
import ekzegetIcon from './icons/ekzegetIcon.svg';

// import { linkOldSite } from '../../../utils/common';

import './footerArea.scss';

const Footer = () => {
  const isTablet = useSelector(getIsTablet);
  const isPhone = useSelector(getIsMobile);

  const urlFromBible = process.env.BROWSER ? LS.get('urlFromBible') : 'initial';

  return (
    <footer className="footer">
      <div className="footer__middle">
        <div className="footer__row">
          <div className="footer__col">
            <div className="footer__logo">
              <img
                src={ekzegetIcon}
                alt="Экзегет.ру - Библия и толкования"
                className="footer__logo-img"
              />
            </div>
            {!isTablet ? (
              <div className="footer__logo">
                <Social />
              </div>
            ) : (
              <FooterPartners />
            )}

            {/* <!-- Yandex.Metrika informer --> */}
            <a
              href="https://metrika.yandex.ru/stat/?id=23728522&amp;from=informer"
              /* eslint-disable-next-line react/jsx-no-target-blank */
              target="_blank"
              rel="nofollow">
              <img
                src="https://informer.yandex.ru/informer/23728522/3_0_FFFFFFFF_EFEFEFFF_0_pageviews"
                style={{ width: '88px', height: '31px', border: 0 }}
                alt="Яндекс.Метрика"
                title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)"
                className="ym-advanced-informer"
                data-cid="23728522"
                data-lang="ru"
              />
            </a>
            {/* <!-- /Yandex.Metrika informer --> */}
          </div>
          {isPhone ? (
            <div className="footer_nav_mobile">
              <FooterNav urlFromBible={urlFromBible} />
            </div>
          ) : (
            <FooterNav urlFromBible={urlFromBible} />
          )}

          <div className="footer__col">
            <FooterDonations />
            {isTablet ? (
              <div className="footer__logo footer-social-tablet">
                <div className="footer-social-title">Расскажите о нас!</div>
                <Social />
              </div>
            ) : (
              <FooterPartners />
            )}
          </div>
        </div>
      </div>

      <FooterCopyright />
    </footer>
  );
};

export default Footer;
