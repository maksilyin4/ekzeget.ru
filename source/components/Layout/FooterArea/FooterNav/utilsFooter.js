import { bibleyskayaViktorina, allAboutBible, defaultUrlForBible, bibleGroup } from '~utils/common';

const aboutProject = [
  {
    to: '/o-proekte/about/',
    title: 'О проекте',
  },
  {
    to: '/novosti-i-obnovleniya/novosti/',
    title: 'Новости',
  },
  {
    to: '/pozhertvovaniya/',
    title: 'Пожертвования',
  },
  {
    to: '/contacts/',
    title: 'Контакты',
  },
];

const setService = urlFromBible => [
  {
    to: urlFromBible === 'initial' ? defaultUrlForBible : urlFromBible,
    title: 'Библия',
  },
  {
    to: `/reading-plan/`,
    title: 'План чтений',
  },
  {
    to: `/${allAboutBible}/mediateka/`,
    title: 'Медиатека',
  },
  {
    to: `/${allAboutBible}/about-bible/`,
    title: 'Все о Библии',
  },
  // {
  //   to: `/${allAboutBible}/propovedi/`,
  //   title: 'Проповеди',
  // },
  // {
  //   to: `/${allAboutBible}/dictionaries/`,
  //   title: 'Словари',
  // },
  {
    to: `/${bibleGroup}/about/`,
    title: 'Группы',
  },
  // {
  //   to: `/${allAboutBible}/bibleyskaya-karta/`,
  //   title: 'Библейские карты',
  // },
  // {
  //   to: `/${allAboutBible}/about-bible/`,
  //   title: 'Лекторий',
  // },
  {
    to: `/${bibleyskayaViktorina}/`,
    title: 'Викторина',
  },
];

export { aboutProject, setService };
