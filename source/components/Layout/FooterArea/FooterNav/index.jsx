import React, { useCallback } from 'react';
import { Link } from 'react-router-dom';

import { aboutProject, setService } from './utilsFooter';

const FooterNav = ({ urlFromBible }) => {
  const serviceMap = useCallback(() => setService(urlFromBible), [urlFromBible]);
  return (
    <>
      <div className="footer__nav-col">
        <div className="footer__nav-title">О проекте</div>
        <div className="footer__nav-list">
          {aboutProject.map(({ to, title }) => (
            <div key={to} className="footer__nav-item">
              <Link to={to}>{title}</Link>
            </div>
          ))}
        </div>
      </div>
      <div className="footer__nav-col">
        <div className="footer__nav-title">Сервис</div>
        <div className="footer__nav-list">
          {serviceMap().map(({ to, title }) => (
            <div key={to} className="footer__nav-item">
              <Link to={to}>{title}</Link>
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default FooterNav;
