import React from 'react';

const FooterCopyright = () => (
  <div className="footer__bottom">
    <div className="wrapper">
      <div className="footer__copyright">
        <a
          className="footer__copyright-link"
          href="https://www.dobroedelo.ru/"
          target="_blank"
          rel="nofollow noopener noreferrer">
          {`© АНО "Доброе дело", 2017 – ${new Date().getFullYear()}`}
        </a>
      </div>
    </div>
  </div>
);

export default FooterCopyright;
