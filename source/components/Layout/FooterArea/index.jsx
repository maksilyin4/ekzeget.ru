import Loadable from 'react-loadable';

const LoadableBar = Loadable({
  loader: () => import('./FooterArea'),
  loading: () => null,
  delay: 300,
});

export default LoadableBar;
