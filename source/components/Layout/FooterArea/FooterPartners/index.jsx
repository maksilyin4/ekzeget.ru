import React from 'react';
import LazyLoad from 'react-lazyload';

const FooterPartners = () => (
  <div className="footer__partners">
    <div className="footer__partners-list">
      <div className="footer__partners-item">
        <LazyLoad height={120}>
          <a href="https://predanie.ru/" target="blank" alt="предание">
            <img
              src="/frontassets/images/partners/partner1.png"
              alt="Экзегет.ру - Библия и толкования"
            />
          </a>
        </LazyLoad>
      </div>
      <div className="footer__partners-item">
        <LazyLoad height={120}>
          <a href="https://svyatye.com/" target="blank" alt="Цитаты святых">
            Цитаты святых
          </a>
        </LazyLoad>
      </div>

      <div className="footer__partners-item">
        <LazyLoad height={120}>
          <a
            href="https://jesus-portal.ru/"
            target="blank"
            alt="«Иисус». Православный портал о Христе и христианстве">
            <img
              className="footer__partner-svg"
              src="/frontassets/images/partners/js.svg"
              alt="'Иисус'. Православный портал о Христе и христианстве."
              title="'Иисус'. Православный портал о Христе и христианстве."
            />
          </a>
        </LazyLoad>
      </div>
    </div>
  </div>
);

export default FooterPartners;
