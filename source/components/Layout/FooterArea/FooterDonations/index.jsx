import React from 'react';

import Button from '../../../Button/Button';

const FooterDonations = () => (
  <div className="footer__donations">
    <div className="footer__donations-title">Хотите помочь проекту?</div>
    <Button
      className="footer__donations-btn"
      title="Сделать пожертвование"
      href="/pozhertvovaniya/"
    />
  </div>
);

export default FooterDonations;
