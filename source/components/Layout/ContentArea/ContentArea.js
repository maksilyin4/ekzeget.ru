import React, { useEffect, useState } from 'react';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import cx from 'classnames';
import { useCookies } from 'react-cookie';

// actions
import { getTranslates, chooseTranslate } from '~dist/actions/translates';
import { setWidth } from '~dist/actions/screen';
import { getUser } from '~dist/actions/User';
import { setBrowser } from '../../../store/browser/action';

// selectors
import { userLoggedIn, userDataInfo } from '~dist/selectors/User';

// components
import Home from '../../../pages/Home/Home';
// import Book from '../../../pages/Bible/Book';
import Chapter from '../../../pages/Bible/Chapter/Chapter';
import Lk from '../../../pages/Lk';
import Donations from '../../../pages/Donations/DonationsLoadable';
import NotFound from '../../../pages/NotFound';
import News from '../../../pages/News/NewsLoadable';
import AboutUs from '../../../pages/AboutUs/AboutUsLoadable';
import CreatePlan from '../../../pages/CreatePlan';
import CreateGroup from '../../../pages/CreatePlan/CreateGroup';
import Contacts from '../../../pages/Contacts/ContactsLoadable';
import NewsDetails from '../../NewsDetails';
// import Video from '../../../pages/Video';
import BibleGroup from '../../../pages/BibleGroup/BibleIndex';
import GroupItem from '../../../pages/BibleGroup/GroupItem/GroupItemLoadable';
import AddBibleGroup from '../../../pages/BibleGroup/AddGroup/AddGroupLoadable';
import Search from '../../../pages/Search/SearchLoadable';
import AddInterpretationRules from '../../../pages/AddInterpretationRules';
import ForgotPassword from '../../../pages/ForgotPassword/ForgotPassword';
import Interpretation from '../../../pages/Interpretation/InterpretationIndex';
import ConfirmReg from '../../../pages/ConfirmReg';
import BibleMap from '../../../pages/BibleMap/BibleMapLoadable';
import Quiz from '../../../pages/Quiz/QuizLoadable';
import AboutBible from '../../../pages/AboutBible/BibleIndex';
import Mediateka from '../../../pages/Mediateka/Mediateka';
import ReadingPlan from '../../../pages/ReadingPlan';
import PreviewPlan from '../../../pages/PreviewPlan';
import ReadingPlanMonitoring from '../../../pages/ReadingPlanMonitoring';
import CustomPlanDetail from '../../../pages/CustomPlanDetail';
import Registration from '../../../pages/Account/Registration';
import PasswordReset from '../../../pages/Account/PasswordReset';

import { translates } from './translates';

import '../../Popups/styles.scss';
import { useAtom } from '@reatom/react';
import { ssrReAtomAtom } from '../../../model';

const ContentArea = ({
  userLoggedIn,
  directProps = {},
  initialState,
  userInfo,
  location: { pathname },
  getTranslates = f => f,
  setWidth = f => f,
  setBrowser = f => f,
  getUser = f => f,
  chooseTranslate = f => f,
}) => {
  const [isVisible, getVisibleState] = useState(false);
  const [cookies, setCookie] = useCookies(['userId', 'translates']);

  useAtom(ssrReAtomAtom);

  useEffect(() => {
    setWidth();
    setBrowser();
    getTranslates();

    if (Object.keys(userInfo).length === 0 && cookies.userId) {
      getUser();
    }

    setTimeout(() => getVisibleState(true));

    window.addEventListener('resize', setWidth);

    return () => {
      window.removeEventListener('resize', setWidth);
    };
  }, []);

  useEffect(() => {
    if (!cookies.translates) {
      setCookie('translates', translates, { path: '/' });
    } else {
      chooseTranslate(cookies.translates);
    }
  }, [pathname]);

  return (
    <div
      className={cx('wrapper__main', {
        'body-visible': isVisible,
      })}>
      <Switch>
        <Route
          exact
          path="/"
          render={() => <Home directProps={directProps} initialState={initialState} />}
        />
        {/* <Route exact path="/bible/:book_code/" component={Book} /> */}
        <Route
          exact
          path="/bible/:book_code/:chapter_num/"
          render={props => {
            // const {
            //   location: { pathname },
            // } = props;
            // const isBook = pathname.includes('book');
            // return isBook ? (
            //   <Book
            //     directProps={directProps}
            //     initialState={initialState}
            //     {...{ isBook, ...props }}
            //   />
            // ) : (
            return <Chapter directProps={directProps} initialState={initialState} {...props} />;
            // );
          }}
        />
        <Route
          exact
          path={[
            '/bible/:book_code/:chapter_num/stih-:verse_id/',
            '/bible/:book_code/:chapter_num/stih-:verse_id/:ekzeget/',
          ]}
          render={props => (
            <Interpretation directProps={directProps} initialState={initialState} {...props} />
          )}
        />
        <Route
          exact
          path="/bible/:book_code/:chapter_num/:tolkovatel/"
          render={props => (
            <Chapter directProps={directProps} initialState={initialState} {...props} />
          )}
        />

        <Route exact path="/bible-group/add-group/" component={AddBibleGroup} />
        <Route
          exact
          path={[
            '/bible-group/search',
            '/bible-group/about',
            '/bible-group/for-leaders',
            '/bible-group/for-leaders/:id',
            '/bible-group/video',
          ]}
          component={BibleGroup}
        />
        <Route exact path="/bible-group/:id/" component={GroupItem} />
        <Route path="/bibleyskaya-karta/" component={BibleMap} />

        <Route
          path={[
            '/all-about-bible/about-bible',
            '/all-about-bible/ekzegets',
            '/all-about-bible/propovedi',
            '/all-about-bible/dictionaries',
            '/all-about-bible/bibleyskaya-karta',
          ]}
          component={AboutBible}
        />
        <Redirect exact from="/all-about-bible/" to="/all-about-bible/about-bible/" />
        {/*
        <Route path="/video/:testament/" component={Video} />
        <Redirect exact from="/video/" to="/video/video-novogo-zaveta/" /> */}
        <PrivateRoute path="/lk/" permitted={userLoggedIn} component={Lk} />

        <Route
          exact
          path="/o-proekte/:id"
          render={({ match, location }) => (
            <AboutUs
              directProps={directProps}
              initialState={initialState}
              match={match}
              location={location}
              url={'/o-proekte/'}
            />
          )}
        />
        <Redirect exact from="/o-proekte/" to="/o-proekte/about/" />

        <Route exact path="/pozhertvovaniya/" component={Donations} />

        <Route
          exact
          path="/novosti-i-obnovleniya/:id"
          render={props => (
            <News
              directProps={directProps}
              initialState={initialState}
              url={'/novosti-i-obnovleniya/'}
              {...props}
            />
          )}
        />
        <Redirect exact from="/novosti-i-obnovleniya/" to="/novosti-i-obnovleniya/novosti/" />
        <Route
          exact
          path="/novosti-i-obnovleniya/:novosti/:newDetail/"
          render={props => <NewsDetails directProps={directProps} {...props} />}
        />
        <Route exact path="/contacts/" component={Contacts} />
        <Route path="/search" component={Search} />
        <Route path="/add-interpretation-rules/" component={AddInterpretationRules} />

        <Route exact path="/forgot-password/:route/" component={ForgotPassword} />
        <Route exact path="/registration-confirm/:token/" component={ConfirmReg} />
        <Route exact path="/registration/" component={Registration} />
        <Route exact path="/reset-password/" component={PasswordReset} />
        <Route path="/bibleyskaya-viktorina/" component={Quiz} />
        <Route path="/mediateka/" component={Mediateka} />
        <Route exact path={'/reading-plan/group/'} component={ReadingPlanMonitoring} />
        <Route
          path={'/group/join/:iv/:encryptedData'}
          render={() => <ReadingPlanMonitoring isJoin />}
        />
        <Route
          exact
          path={['/group/reading/:plan_id/:day']}
          render={props => {
            return (
              <CustomPlanDetail directProps={directProps} initialState={initialState} {...props} />
            );
          }}
        />
        <Route
          exact
          path={['/group/reading/:plan_id/:day/result']}
          render={props => {
            return (
              <CustomPlanDetail directProps={directProps} initialState={initialState} {...props} />
            );
          }}
        />
        <Route exact path={'/reading-plan/preview-plan'} component={PreviewPlan} />
        <Route exact path={'/reading-plan/preview-plan/:plan_id'} component={PreviewPlan} />
        <Route exact path={'/reading-plan/create-plan'} component={CreatePlan} />
        <Route
          exact
          path={'/reading-plan/edit-plan'}
          render={() => <CreatePlan isEdit directProps={directProps} initialState={initialState} />}
        />
        <Route
          exact
          path={'/reading-plan/edit-plan/:plan_id'}
          render={() => (
            <CreatePlan isEdit isReady directProps={directProps} initialState={initialState} />
          )}
        />
        <Route
          exact
          path={'/reading-plan/create-group/:plan_id'}
          render={() => <CreateGroup directProps={directProps} initialState={initialState} />}
        />
        <Route
          exact
          path={'/reading-plan/edit-group/:plan_id'}
          render={() => (
            <CreateGroup
              isEdit
              isReadyGroup
              directProps={directProps}
              initialState={initialState}
            />
          )}
        />
        <Route path={['/reading-plan/', '/reading-plan/:id']} component={ReadingPlan} />

        <Route component={NotFound} />
      </Switch>
    </div>
  );
};

const PrivateRoute = ({ component: PrivateComponent, permitted }) => (
  <Route
    render={props =>
      permitted ? (
        <PrivateComponent {...props} />
      ) : (
        <Redirect
          to={{
            pathname: '/',
            search: '?auth-need',
          }}
        />
      )
    }
  />
);

const mapStateToProps = store => ({
  userLoggedIn: userLoggedIn(store),
  translatesLength: store.translates.translates.length,
  userInfo: userDataInfo(store),
});

const mapDispatchToProps = {
  getTranslates,
  chooseTranslate,
  setWidth,
  setBrowser,
  getUser,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ContentArea));
