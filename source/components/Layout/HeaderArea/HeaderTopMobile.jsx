import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import HeaderNav from './HeaderNav';
import SiteNavigation from '../../SiteNavigation/SiteNavigation';
import Social from '../../Social/Social';
import AnimateLiftRight from '../../AnimateComponent/SlideLeftRight/index';
import FontSizeControl from '../../FontSizeControl/FontSizeControl';

const menuIcon = require('./images/menu.svg');
const closedIcon = require('./images/closed.svg');

class HeaderTopMobile extends Component {
  state = {
    isOpen: false,
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.location.pathname !== this.props.location.pathname) {
      this.setState({ isOpen: false });
    }
  }

  escPress = e => {
    if (e.keyCode === 27) {
      this.closeMenu();
    }
  };

  openMenu = () => {
    if (typeof document === 'undefined') return;
    this.setState({ isOpen: true }, () => {
      document.addEventListener('keydown', this.escPress);
    });
  };

  closeMenu = e => {
    if (typeof document === 'undefined') return;
    if (e) {
      e.stopPropagation();
    }
    this.setState({ isOpen: false }, () => {
      document.removeEventListener('keydown', this.escPress);
    });
  };

  render() {
    const { isOpen } = this.state;
    return (
      <div className="header__humburger" onClick={this.openMenu}>
        <button>
          <img src={menuIcon} alt="menu" />
        </button>
        {isOpen && <div className="header__humburger__BG" onClick={this.closeMenu} />}
        <AnimateLiftRight show={isOpen} className="header__humburger__sideMenu">
          <div className="header__humburger__close">
            <button onClick={this.closeMenu}>
              <img src={closedIcon} alt="Закрыть" />
            </button>
          </div>
          <FontSizeControl />
          <SiteNavigation isMobile />
          <HeaderNav isMobile />
          <Social isMobile />
        </AnimateLiftRight>
      </div>
    );
  }
}

export default withRouter(HeaderTopMobile);
