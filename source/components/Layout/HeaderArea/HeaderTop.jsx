import React, { PureComponent } from 'react';
import Social from '../../Social/Social';
import HeaderNav from './HeaderNav';
import FontSizeControl from '../../FontSizeControl/FontSizeControl';

import './header.scss';

class HeaderTop extends PureComponent {
  render() {
    return (
      <div className="header__top header__main header__nav">
        <div className="wrapper">
          <HeaderNav />
          <nav className="header__nav">
            <div className="header__nav-item">
              <Social />
            </div>
          </nav>
          <FontSizeControl />
        </div>
      </div>
    );
  }
}

export default HeaderTop;
