import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { NavLink } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
// import { getUserPlanId } from '../../../dist/selectors/ReadingPlan';
import { getUserReadingPlan } from '../../../dist/actions/ReadingPlan';
import { userLoggedIn } from '../../../dist/selectors/User';

const getLinks = () => [
  { to: '/o-proekte/about/', title: 'О проекте' },
  // { to: `/reading-plan/${userReadingPlan}`, title: 'План чтений' },
  { to: '/novosti-i-obnovleniya/novosti/', title: 'Новоe' },
  { to: '/pozhertvovaniya/', title: 'Пожертвования' },
  { to: '/contacts/', title: 'Контакты' },
];

const HeaderNav = React.memo(({ isMobile }) => {
  const dispatch = useDispatch();

  // const userReadingPlan = useSelector(getUserPlanId);
  const logIn = useSelector(userLoggedIn);

  React.useEffect(() => {
    if (logIn) {
      dispatch(getUserReadingPlan());
    }
  }, [logIn]);

  return (
    <nav className="header__nav">
      {getLinks().map(({ to, title }) => {
        return (
          <div key={to} className="header__nav-item">
            <NavLink
              to={to}
              className={cx(isMobile ? 'site-nav__item' : 'header__nav-link')}
              activeClassName={cx({ active: isMobile })}
              title={title}>
              {title}
            </NavLink>
          </div>
        );
      })}

      <a
        className={cx(isMobile ? 'site-nav__item' : 'header__nav-link')}
        href="https://ex.ekzeget.ru">
        Старая версия
      </a>
    </nav>
  );
});

HeaderNav.defaultProps = {
  isMobile: false,
};

HeaderNav.propTypes = {
  isMobile: PropTypes.bool,
};

export default HeaderNav;
