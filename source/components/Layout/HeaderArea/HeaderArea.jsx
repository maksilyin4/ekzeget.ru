import React, { useEffect } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { handleSearch, handleReset } from '../../../dist/actions/search';
import { getIsMobile } from '../../../dist/selectors/screen';
import WidgetUser from '../../WidgetUser/WidgetUser';
import TextField from '../../TextField/TextField';
import HeaderTop from './HeaderTop';
import HeaderBottom from './HeaderBottom';
import HeaderTopMobile from './HeaderTopMobile';

import logotypeIcon from './images/logotype.svg';
import searchIcon from '~assets/images/layout/search.svg';
import { onMouseEnter, onMouseLeave, onMouseObj } from '../../PreloadChunk/';

const mapStateToProps = store => ({
  searchQuery: store.search.searchField,
  isMobile: getIsMobile(store),
});

const mapDispatchToProps = {
  handleSearch,
  handleReset,
};

const DEFAULT_SEARCH_PATH = '/search/mediateka';

const Header = ({
  history,
  searchQuery,
  isMobile,
  handleSearch,
  handleReset,
  location: { pathname },
}) => {
  useEffect(() => {
    if (!pathname.includes('/search/')) {
      handleReset();
    }
  }, [pathname]);

  const handleRedirect = () => {
    const searchQueryStr = searchQuery ? `?search=${searchQuery}` : '';
    history.push(`${DEFAULT_SEARCH_PATH + searchQueryStr}`);
  };

  const handleInput = e => {
    handleSearch(e.currentTarget.value);
  };

  return (
    <header className="header">
      {!isMobile && <HeaderTop />}
      <div className="header__middle">
        <div className="wrapper">
          {isMobile && <HeaderTopMobile />}
          <div className="header__logo">
            <Link
              to="/"
              title="Экзегет.ру"
              onMouseEnter={() => onMouseEnter(onMouseObj.onMouseHome)}
              onMouseLeave={onMouseLeave}
              onFocus={onMouseObj.onMouseHome}>
              <img src={logotypeIcon} alt="Логотип Экзегета" />
            </Link>
          </div>
          {!isMobile ? (
            <>
              <div className="header__search">
                <TextField
                  id="header_search"
                  type="search"
                  placeholder="Поиск по сайту"
                  redirectTo={handleRedirect}
                  onChange={handleInput}
                  value={searchQuery}
                />
              </div>
              <div className="header__auth">
                <WidgetUser />
              </div>
            </>
          ) : (
            <div className="header__mobile-search">
              <Link to={DEFAULT_SEARCH_PATH} className="header__search__link">
                <img className="header__search-icon" src={searchIcon} alt="Поиск" />
              </Link>
              <div className="header__auth">
                <WidgetUser />
              </div>
            </div>
          )}
        </div>
      </div>
      {!isMobile && <HeaderBottom />}
    </header>
  );
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));
