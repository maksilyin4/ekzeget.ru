import React, { Component } from 'react';
import SiteNavigation from '../../SiteNavigation/SiteNavigation';

class HeaderBottom extends Component {
  render() {
    return (
      <div className="header__bottom">
        <div className="wrapper">
          <SiteNavigation />
        </div>
      </div>
    );
  }
}

export default HeaderBottom;
