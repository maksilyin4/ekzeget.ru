import React from 'react';
import Loadable from 'react-loadable';
import Preloader from '../Preloader/Preloader';

const LoadableBar = Loadable({
  loader: () => import('./LatestNews'),
  loading() {
    return (
      <div>
        <Preloader />
      </div>
    );
  },
  render(loaded, props) {
    const Component = loaded.default;
    return <Component latestNewsList={props.latestNewsList} initialState={props.initialState} />;
  },
  delay: 500,
});

export default LoadableBar;
