import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import dayjs from 'dayjs';
import ruLocale from 'dayjs/locale/ru';
import { AXIOS } from '../../dist/ApiConfig';
import Preloader from '../Preloader/Preloader';

import './latestNews.scss';

class LatestNews extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isFetching: false,
      list: this.props.latestNewsList,
    };
  }

  componentDidMount() {
    const { initialState } = this.props;
    if (this.props.latestNewsList > 0) return;

    if ('latestNewsList' in initialState) {
      this.setState({
        list: initialState.latestNewsList,
      });
    } else {
      this.setState({
        isFetching: true,
      });
      AXIOS.get('article/1/?per-page=3').then(
        ({ data, status }) => {
          if (status === 200) {
            this.setState({
              isFetching: false,
              list: data.article,
            });
          } else {
            this.setState({
              isFetching: false,
            });
          }
        },
        () => {
          this.setState({
            isFetching: false,
          });
        },
      );
    }
  }

  render() {
    const { list, isFetching } = this.state;

    return (
      <div className="latest-news">
        <div className="latest-news__head">
          <h2 className="section__title">Библия - новости и статьи</h2>
        </div>
        <div className="latest-news__body">
          {list && list.length > 0 ? (
            <div className="latest-news__list">
              {list.map(({ id, title, body, updated_at, slug = '1' }) => {
                // thumbnail_path
                // RegExp remove all HTML tags and trim spaces
                const partBody = `${body
                  .replace(/<[a-z]+[^>]*>|<\/[a-z]+>/g, '')
                  .replace(/ +/g, ' ')
                  .slice(0, 120)}...`;
                return (
                  <div key={id} className="latest-news__item paper">
                    <div className="latest-news__item-date">
                      {dayjs
                        .unix(updated_at)
                        .locale('ru', ruLocale)
                        .format('DD MMM YYYY')}
                    </div>
                    <Link
                      to={`/novosti-i-obnovleniya/novosti/${id}/`}
                      className="latest-news__item-title">
                      {title}
                    </Link>
                    <div
                      className="latest-news__item-desc"
                      dangerouslySetInnerHTML={{ __html: partBody }}
                    />
                    <div className="latest-news__item-more">
                      <Link to={`/novosti-i-obnovleniya/novosti/${slug}/`}>Подробнее</Link>
                    </div>
                  </div>
                );
              })}
              <div className="latest-news__item latest-news__item_all paper">
                <Link to="/novosti-i-obnovleniya/novosti/">Все новости</Link>
              </div>
            </div>
          ) : (
            <div className="news__preloader">
              {isFetching ? <Preloader /> : <p className="empty-content">Новостей пока ещё нет</p>}
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default LatestNews;
