import React, { useMemo, useState } from 'react';
import remove from '~assets/images/remove-user.png';
import './styles.scss';
import { useMedia } from 'react-use';
import classNames from 'classnames';
import PopoverCustom from '~components/PopoverCustom/PopoverCustom';
import { getName } from '..';
import { kickUser } from '~dist/api/readingPlan/kickUser';

export const percent = num => `${Math.round(Number(num || 0) * 100)}%`;

export default function MonitoringTable({ users: usersData, user: userMe, mentorId }) {
  const isTablet = useMedia('(max-width: 1024px)');
  const isMobile = useMedia('(max-width: 767px)');
  const [isOpen, setIsOpen] = useState(null);

  const [activeSort, setActiveSort] = useState({
    name: 'Имя',
    sort: 'name',
  });

  const variants = [
    {
      name: 'Имя',
      sort: 'name',
    },
    {
      name: 'Отставание в днях',
      sort: 'daysBehind',
    },
    {
      name: 'Ответы верно',
      sort: 'answers',
    },
    {
      name: 'Ответы за 7 дней верно',
      sort: 'answersWeek',
    },
  ];

  const users = useMemo(() => {
    switch (activeSort.sort) {
      case 'name':
        return [...usersData].sort((a, b) => getName(a).localeCompare(getName(b)));
      case 'daysBehind':
        return [...usersData].sort(
          (a, b) => b?.statistics.daysBehindSchedule - a?.statistics.daysBehindSchedule,
        );
      case 'answers':
        return [...usersData].sort((a, b) => b.quizzes?.totalAccuracy - a.quizzes?.totalAccuracy);
      case 'answersWeek':
        return [...usersData].sort(
          (a, b) => b.quizzes?.accuracyLastWeek - a?.quizzes?.accuracyLastWeek,
        );
      default:
        return usersData;
    }
  }, [activeSort]);

  function customSort(a, b) {
    if (a.id === mentorId) {
      return -1; // Перемещаем объект с id 12 в начало
    }
    if (b.id === mentorId) {
      return 1; // Перемещаем объект с id 12 в начало
    }
    return 0;
  }

  return (
    <div className="rp__share-block rp__share-block-users">
      <div className="rp__head-table-main">
        <span className="list-title">Список участников ({users.length})</span>
        {isTablet && (
          <PopoverCustom
            placement="bottom-end"
            className={'rp__pop-btn'}
            innerContent={
              <div className="popover__inner">
                {variants.map((v, i) => (
                  <button key={i} onClick={() => setActiveSort(v)}>
                    {v.name}
                    {activeSort.sort === v.sort && (
                      <svg
                        width="13"
                        height="8"
                        viewBox="0 0 13 8"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path d="M1 1L6.67742 6L12 1" stroke="#929292" strokeWidth="2" />
                      </svg>
                    )}
                  </button>
                ))}
              </div>
            }>
            <div className="rp__head-sort">
              {!isMobile && 'Сортировка '}
              <span>{activeSort.name}</span>
              <svg
                width="9"
                height="6"
                viewBox="0 0 9 6"
                fill="none"
                xmlns="http://www.w3.org/2000/svg">
                <path d="M4.5 6L0.602886 0L8.39711 0L4.5 6Z" fill="black" />
              </svg>
            </div>
          </PopoverCustom>
        )}
      </div>
      <div className="rp__table-users">
        <div
          className={classNames(
            'rp__users-row  rp__head-table',
            Number(mentorId) === userMe.id && 'mentorRows',
          )}>
          <div className="rp__name-head">
            <span>Имя</span>
            {!isTablet && (
              <svg
                onClick={() => setActiveSort({ name: 'Имя', sort: 'name' })}
                xmlns="http://www.w3.org/2000/svg"
                width="12"
                height="6"
                viewBox="0 0 12 6"
                fill="none">
                <path d="M6 0L11.1962 6H0.803848L6 0Z" fill="white" />
              </svg>
            )}
          </div>
          <div className="rp__email-head">E-mail</div>
          <div className="rp__past-head">
            Отставание <br /> в днях
          </div>
          {!isMobile ? (
            <>
              <div>
                {isTablet ? (
                  <>
                    Ответы <br />
                    всего/верно
                  </>
                ) : (
                  <>
                    Ответы всего/
                    <br />
                    из них верных
                  </>
                )}
              </div>
              <div>
                {isTablet ? (
                  <>
                    Ответы <br />7 дней/верно
                  </>
                ) : (
                  <>
                    Ответы 7 дней/
                    <br />
                    из них верных
                  </>
                )}
              </div>
              <div>
                Сообщения
                <br /> за 7 дней
              </div>
              {userMe.id === mentorId && (
                <div className="rp__kick">
                  Исключить <br /> из группы
                </div>
              )}
            </>
          ) : (
            <div />
          )}
        </div>
        <div className="scroll_container">
          {users.sort(customSort).map((user, id) => (
            <Row
              userMe={userMe}
              mentorId={mentorId}
              isOpen={isOpen === id}
              onClick={() => {
                if (isOpen === id) {
                  setIsOpen(null);
                } else {
                  setIsOpen(id);
                }
              }}
              user={user}
            />
          ))}
        </div>
      </div>
    </div>
  );
}

const Row = ({ user, isOpen, userMe, onClick, mentorId }) => {
  const isMobile = useMedia('(max-width: 767px)');
  const stats = user?.statistics;
  const plan = stats.planCompletion;
  const comments = stats.comments;
  const quizzes = stats.quizzes;
  const [isDelete, setDelete] = useState(false);

  const handleClick = async userId => {
    try {
      const { status } = await kickUser(userId);
      if (status < 300) {
        setDelete(true);
      }
    } catch (e) {
      console.error('Произошла ошибка при попытке удаления');
    }
  };

  return (
    !isDelete && (
      <>
        <div
          onClick={onClick}
          className={classNames(
            'rp__users-row',
            isOpen && 'rp__users-row--active',
            Number(mentorId) === user.id && 'mentor',
            Number(mentorId) === userMe.id && 'mentorRows',
          )}>
          <div className="rp__name">{getName(user)}</div>
          <div className="rp__email">
            <span>{user.email}</span>
          </div>
          <div className="rp__past">
            {plan.daysBehindSchedule < 0 ? 0 : plan.daysBehindSchedule}
          </div>
          {isMobile ? (
            <div>
              <svg
                className={classNames('rp__open-arrow', isOpen && 'rp__open-arrow--active')}
                width="12"
                height="8"
                viewBox="0 0 12 8"
                fill="none"
                xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M1 7L6 1L11 7"
                  stroke="#505050"
                  strokeWidth="1.41829"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
              </svg>
            </div>
          ) : (
            <>
              <div>
                {percent(quizzes?.allTime?.completedPercent)}/{percent(quizzes?.allTime?.accuracy)}
              </div>
              <div>
                {percent(quizzes?.lastWeek?.completedPercent)}/
                {percent(quizzes?.lastWeek?.accuracy)}
              </div>
              <div>{comments.lastWeek}</div>
              {mentorId !== user.id && userMe.id === mentorId && (
                <div onClick={() => handleClick(user.id)} className="rp__remove-user--wrapper">
                  <img className="rp__remove-user" src={remove} alt="" />
                </div>
              )}
            </>
          )}
        </div>
        {isOpen && isMobile && (
          <div className="accordion-content">
            {mentorId !== user.id && userMe.id === mentorId && (
              <button onClick={() => handleClick(user.id)} className="rp__remove-user--wrapper">
                <span>Исключить</span>
                <img className="rp__remove-user" src={remove} alt="" />
              </button>
            )}
            <div className="wrapper-content">
              <dl>
                <dd>Ответы всего</dd>
                <dt>{percent(quizzes?.allTime?.completedPercent)}</dt>
              </dl>
              <dl>
                <dd>Верных</dd>
                <dt>{percent(quizzes?.allTime?.accuracy)}</dt>
              </dl>
              <dl>
                <dd>Ответы 7 дней</dd>
                <dt>{percent(quizzes?.lastWeek?.completedPercent)}</dt>
              </dl>
              <dl>
                <dd>Верных</dd>
                <dt>{percent(quizzes?.lastWeek?.accuracy)}</dt>
              </dl>
              <dl>
                <dd>Сообщений всего</dd>
                <dt>{comments.total}</dt>
              </dl>
              <div />
              <dl>
                <dd>Сообщений 7 дней</dd>
                <dt>{comments.lastWeek}</dt>
              </dl>
              <div />
            </div>
          </div>
        )}
      </>
    )
  );
};
