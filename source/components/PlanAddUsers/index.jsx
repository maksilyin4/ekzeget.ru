import React, { useEffect, useState } from 'react';
import MonitoringTable from './table';
import './styles.scss';
import ReadingPlanTable from '~components/ReadingPlanTable/ReadingPlanTable';
import CustomButtonCP from '~components/UICreatePlan/CustomButtonCp';
import readingPlanActions from '../../dist/actions/ReadingPlan';
import { getUser } from '../../dist/actions/User';
import { mentorGroup, userGroup, userGroupError } from '../../store/groups/selectors';
import { getMentorGroup, getUserGroup, getPreviewGroup } from '../../store/groups/actions';
import { userData, userLoggedIn } from '../../dist/selectors/User';
import { getUserPlans, planDetail } from '../../dist/selectors/ReadingPlan';
import { connect, useSelector } from 'react-redux';
import { getDaysByObj } from '~components/Popups/ReadingPlanDetail/plan-days/getDaysByObj';
import ReadingProgressBar from '~components/ReadingProgressBar/ReadingProgressBar';
import ReadingPlanShare from './ReadingPlanShare';
import MyPlanResult from '~components/MyPlanResult';
import { useMedia } from 'react-use';
import { useHistory, useParams } from 'react-router-dom';
import PopupAuth from '../Popups/Auth/Auth';
import { joinGroup } from '~dist/api/readingPlan/createGroup';
import { decrypt } from '~utils/encrypte';
import { Popup } from '~dist/browserUtils';

export const getName = user => {
  const name = user?.firstname;
  const surname = user?.lastname;
  if (name && surname) {
    return `${name} ${surname}`;
  }
  if (name) {
    return name;
  }
  return user?.username;
};

function PlanAddUsers({
  mentorGroup,
  planDetail,
  getDetail,
  userGroup,
  getUserReadingPlan,
  userGroupError,
  user: { user },
  getUser,
  getMentorGroup,
  isJoin,
  getPreviewGroup,
  getUserGroup,
}) {
  const isTablet = useMedia('(max-width: 1024px)');
  const [idDecrypt, setIdDecrypt] = useState(null);
  const loggedIn = useSelector(userLoggedIn);
  const history = useHistory();

  useEffect(() => {
    if (!loggedIn) {
      history.push('/reading-plan');
      Popup.create(
        {
          title: 'Вход',
          content: <PopupAuth />,
          className: 'popup_auth not-header',
        },
        true,
      );
    }
  }, [loggedIn]);

  useEffect(() => {
    if (isJoin) {
      setIdDecrypt(decrypt(iv, encryptedData));
    }
  }, []);

  useEffect(() => {
    if (isJoin) {
      idDecrypt && getPreviewGroup(idDecrypt);
    } else {
      if (!mentorGroup?.id) {
        getMentorGroup();
      }
      if (!userGroup.id) {
        getUserGroup();
      }
    }
    getUserReadingPlan();
  }, [isJoin && idDecrypt]);

  useEffect(() => {
    if (userGroupError?.hasError) {
      history.push('/reading-plan');
      Popup.create(
        {
          title: 'Чтобы подписаться на этот план вы должны выйти из предыдущего плана',
          className: 'not-header popup_auth',
        },
        true,
      );
    }
  }, [userGroupError]);

  const { iv, encryptedData } = useParams();

  const handleJoin = async groupId => {
    try {
      await joinGroup(groupId);
      history.push('/reading-plan/group/');
    } catch (e) {
      console.error('Произошла ошибка при попытке подключиться к группе');
    }
  };

  const handleClick = () => {
    if (isJoin && idDecrypt) {
      handleJoin(idDecrypt);
    }
  };

  useEffect(() => {
    if (!user?.id) {
      getUser();
    }
  }, []);

  useEffect(() => {
    if (userGroup.plan_id) {
      getDetail(userGroup.plan_id);
    }
  }, [userGroup]);

  const userPlans = useSelector(state => getUserPlans(state, userGroup.plan_id));
  const mentor = userGroup.users?.find(u => u.id === userGroup.mentor_id);
  const isMentorGroup = mentorGroup.users?.some(u => u.id === user.id);
  const stats = userGroup.users?.find(u => u.id === user.id)?.statistics;

  return (
    <>
      {isTablet && <ReadingProgressBar withTop userPlans={userPlans} />}

      <div className="rp__wrapper">
        {isJoin && (
          <CustomButtonCP onClick={handleClick} color="blue" className="rpm__btn-join">
            Подписаться
          </CustomButtonCP>
        )}
        <div className="rp__admin-monitoring">Ведущий: {getName(mentor)}</div>
        {userGroup.users?.length && (
          <MonitoringTable user={user} mentorId={userGroup.mentor_id} users={userGroup.users} />
        )}
        <div className="rp__bottom-grid">
          {userGroup.id && (
            <div className="rp__admin-flex">
              {!isTablet && isMentorGroup && (
                <ReadingProgressBar className="monitoring_bar" withTop userPlans={userPlans} />
              )}
              {isMentorGroup ? (
                <ReadingPlanShare id={userGroup.id} />
              ) : (
                <MyPlanResult stats={stats} withTop userPlans={userPlans} />
              )}
            </div>
          )}
          {planDetail[1] && userGroup.schedule && (
            <ReadingPlanTable
              head={{
                title: '',
                chapters: planDetail[1].length,
                startDate: userGroup.start_date,
                endDate: userGroup.stop_date,
                weekDays: getDaysByObj(userGroup.schedule).days,
                planLength: Object.values(planDetail).length,
                parallel: planDetail[1][0].branch === null ? 0 : 1,
              }}
              isMin
              plan={planDetail}
            />
          )}
        </div>
        <CustomButtonCP to={'/reading-plan'} color="gray" className="rpm__btn-back">
          {isJoin ? 'Не вступать' : 'Назад'}
        </CustomButtonCP>
      </div>
    </>
  );
}

const mapStateToProps = state => ({
  mentorGroup: mentorGroup(state),
  userGroup: userGroup(state),
  user: userData(state),
  planDetail: planDetail(state),
  userGroupError: userGroupError(state),
});

const mapDispatchToProps = {
  ...readingPlanActions,
  getDetail: readingPlanActions.getReadingPlanDetail,
  getUser,
  getPreviewGroup,
  getMentorGroup,
  getUserGroup,
};

export default connect(mapStateToProps, mapDispatchToProps)(PlanAddUsers);
