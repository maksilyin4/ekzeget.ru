import React, { useEffect, memo, useCallback } from 'react';
import PropTypes from 'prop-types';

import { Popup } from '../../dist/browserUtils';
import AddFavoritesVerse from '../Popups/AddFavoriteVerse';
import FavoriteVerses from './FavoriteVerses';
import FavoriteVerseTags from './FavoriteVerseTags';
import { favoritesDefault, favoritesTypes } from '../../pages/Lk/Favorites/commonFavoritesTypes';

const FavoriteVerseMap = ({
  getFavoriteVerse,
  sessionTag,
  chooseTag,
  setSessionTag,
  updateFavVerse,
  isFetching,
  favorites,
  deleteFavoriteVerse,
  allTags,
  choosenTag,
  sessionTagArray,
  currentTranslate,
}) => {
  useEffect(() => {
    if (sessionTag.length > 0) {
      getFavoriteVerse(sessionTag);
      chooseTag(sessionTag);
    } else {
      getFavoriteVerse();
      chooseTag('');
    }
  
    return () => {
      setSessionTag('');
    };

  }, []);

  const translateClasses =  {
    csya: currentTranslate?.code === 'csya_old',
    greek: currentTranslate?.code === 'grek',
  };

  const openPopUp = useCallback(
    (id, tags, color, short, verseID, allTags) => {
      Popup.create(
        {
          title: null,
          content: (
            <AddFavoritesVerse
              listIds={[verseID]}
              title="Редактировать стих "
              items={[short]}
              tags={tags}
              color={color}
              updateFavVerse={updateFavVerse}
              id={id}
              allTags={allTags}
            />
          ),
          className: 'popup_add_favorite_verse',
        },
        true,
      );
    },
    [updateFavVerse],
  );
  return (
    <ul className="favorite__verseList">
      <FavoriteVerseTags
        allTags={allTags}
        sessionTagArray={sessionTagArray}
        sessionTag={sessionTag}
        choosenTag={choosenTag}
        getFavoriteVerse={getFavoriteVerse}
        chooseTag={chooseTag}
        setSessionTag={setSessionTag}
      />
      <FavoriteVerses
        translateClasses={translateClasses}
        allTags={allTags}
        isFetching={isFetching}
        favorites={favorites}
        deleteFavoriteVerse={deleteFavoriteVerse}
        updateFavVerse={updateFavVerse}
        openPopUp={openPopUp}
      />
    </ul>
  );
};

FavoriteVerseMap.defaultProps = favoritesDefault;

FavoriteVerseMap.propTypes = {
  sessionTag: PropTypes.object,
  deleteFavoriteVerse: PropTypes.func,
  favorites: PropTypes.arrayOf(PropTypes.object),
  sessionTagArray: PropTypes.array,
  isFetching: PropTypes.bool,
  updateFavVerse: PropTypes.func,
  ...favoritesTypes,
};

export default memo(FavoriteVerseMap);
