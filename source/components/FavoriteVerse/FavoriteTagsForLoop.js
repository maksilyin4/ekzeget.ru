import React from 'react';

import { favoritesDefault, favoritesTypes } from '../../pages/Lk/Favorites/commonFavoritesTypes';

const FavoriteTagsForLoop = ({
  allTags,
  chooseTag,
  getFavoriteVerse,
  choosenTag,
  setSessionTag,
}) => {
  if (!allTags.length) {
    return null;
  }
  return (
    <ul className="favorite__verseList__allTags">
      {!choosenTag ? (
        allTags.map(i =>
          i ? (
            <p
              key={`tag-key-${i}`}
              className="tag"
              onClick={() => {
                chooseTag(i);
                getFavoriteVerse(i);
              }}>
              {i}
            </p>
          ) : null,
        )
      ) : (
        <div className="favorite__verseList-choosen">
          <p
            key={`${choosenTag}-choosenTag`}
            onClick={() => {
              chooseTag('');
              getFavoriteVerse();
            }}
            className="tag">
            {choosenTag}
          </p>
        </div>
      )}
    </ul>
  );
};

FavoriteTagsForLoop.defaultProps = favoritesDefault;

FavoriteTagsForLoop.propTypes = favoritesTypes;

export default FavoriteTagsForLoop;
