import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import cx from 'classnames';
import deleteIcon from '../../assets/images/icons/delete.svg';
import editIcon from '../../assets/images/icons/edit.svg';
import { baseUlr } from '../../getStore';

const FavoriteVerseItem = ({
  translateClasses,
  url,
  color,
  tags,
  text,
  openPopUp,
  deleteFavoriteVerse,
  short,
}) => (
  <li>
    <div className="favorite__verseList__link">
      <Link to={url} className="favorite__verseList-link">
        {short}
      </Link>
      <div className="favorite__verseList__edit">
        <div className="favorite__verseList__edit-icon btn_circle" onClick={openPopUp}>
          <img alt="Редактировать" title="Редактировать" src={editIcon} />
        </div>
        <div className="favorite__verseList__edit-icon btn_circle" onClick={deleteFavoriteVerse}>
          <img alt="Удалить" title="Удалить" src={deleteIcon} />
        </div>
      </div>
    </div>
    <div className="favorite__verseList__text">
      <p
        style={{ backgroundColor: color }}
        className={cx(translateClasses)}
        dangerouslySetInnerHTML={{ __html: text }}
      />
    </div>
    {tags.length > 0 && (
      <div className="favorite__verseList__tags">
        <p>{`Теги: ${tags.replace(/^,/g, '').replace(/,/g, ', ')}`}</p>
      </div>
    )}
  </li>
);

FavoriteVerseItem.defaultProps = {
  url: baseUlr,
  color: '#FFF',
  tags: '',
  openPopUp: () => {},
  deleteFavoriteVerse: () => {},
};

FavoriteVerseItem.propTypes = {
  color: PropTypes.string,
  deleteFavoriteVerse: PropTypes.func,
  openPopUp: PropTypes.func,
  tags: PropTypes.string,
  text: PropTypes.string,
  url: PropTypes.string,
  short: PropTypes.string,
};

export default FavoriteVerseItem;
