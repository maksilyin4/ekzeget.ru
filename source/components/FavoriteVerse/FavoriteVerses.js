import React from 'react';

import FavoriteVerseItem from './FavoriteVerseItem';

const FavoriteVerses = ({ 
  isFetching, 
  favorites, 
  deleteFavoriteVerse, 
  openPopUp, 
  allTags, 
  translateClasses
 }) => (
  <div>
    {favorites.length > 0 ? (
      favorites.map(
        ({ id, verse: { book_code, text, chapter, id: verseId, short, number }, color, tags }) => (
          <FavoriteVerseItem
            key={id}
            url={`/bible/${book_code}/glava-${chapter}/stih-${number}`}
            short={short}
            color={color}
            text={text}
            tags={tags}
            translateClasses={translateClasses}
            openPopUp={() => openPopUp(id, tags, color, short, verseId, allTags)}
            deleteFavoriteVerse={() => deleteFavoriteVerse({ id, verseId })}
          />
        ),
      )
    ) : (
      <div className="empty-content">
        {!isFetching && <p>Вы не добавили ни одного стиха в избранное</p>}
      </div>
    )}
  </div>
);

export default FavoriteVerses;
