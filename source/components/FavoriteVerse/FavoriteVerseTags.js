import React, { useMemo } from 'react';
import PropTypes from 'prop-types';

import FavoriteTagsForLoop from './FavoriteTagsForLoop';
import { favoritesTypes } from '../../pages/Lk/Favorites/commonFavoritesTypes';

const FavoriteVerseTags = ({
  allTags,
  sessionTag,
  choosenTag,
  getFavoriteVerse,
  chooseTag,
  setSessionTag,
}) =>
  useMemo(
    () => (
      <div>
        {allTags.length > 0 && (
          <li key="all-tags" className="favorite__verseTags">
            {sessionTag.length > 0 ? (
              <FavoriteTagsForLoop
                allTags={sessionTag.split()}
                chooseTag={chooseTag}
                getFavoriteVerse={getFavoriteVerse}
                choosenTag={choosenTag}
                setSessionTag={setSessionTag}
              />
            ) : (
              <FavoriteTagsForLoop
                allTags={allTags}
                chooseTag={chooseTag}
                getFavoriteVerse={getFavoriteVerse}
                choosenTag={choosenTag}
                setSessionTag={setSessionTag}
              />
            )}
          </li>
        )}
      </div>
    ),
    [allTags, chooseTag, choosenTag, getFavoriteVerse, sessionTag, setSessionTag],
  );

FavoriteVerseTags.defaultProps = {
  allTags: [],
  sessionTag: [],
};

FavoriteVerseTags.propTypes = {
  sessionTag: PropTypes.string,
  ...favoritesTypes,
};

export default FavoriteVerseTags;
