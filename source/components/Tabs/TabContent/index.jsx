import React from 'react';
import PropTypes from 'prop-types';

export const TabContent = ({ children, content }) =>
  children || <div className="tab-content">{content}</div>;

TabContent.propTypes = {
  content: PropTypes.object,
};
