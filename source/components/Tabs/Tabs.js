import React, { memo } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { areComponentsEqual } from 'react-hot-loader';

import './tabs.scss';

import { Tab } from './Tab';
import { TabActions } from './TabActions/';
import { TabContent } from './TabContent/';

const Tabs = ({
  activeTab,
  callBack,
  className,
  classNameTabs,
  type,
  chapterScroll,
  twoColumn,
  verseTabs,
  activeContent,
  width,
  children,
}) => {
  const getTabProps = (tab, i) => {
    return React.cloneElement(tab, {
      activeTab,
      callBack,
      key: i,
    });
  };

  return (
    <div className={cx('tabs', className)}>
      <div
        className={cx('tabs__head', classNameTabs, {
          tabs__head_justify: type === 'justify',
          tabs__scroll: chapterScroll,
          tabs__column: twoColumn,
          tabs__verseWrapper: verseTabs && activeContent,
        })}
        style={{ width }}>
        {verseTabs && activeContent ? (
          <>
            <div className="tabs__wrapper">
              {twoColumn ? (
                <div className="tabs__list">
                  {children.map(
                    (child, i) => areComponentsEqual(child.type, Tab) && getTabProps(child, i),
                  )}
                </div>
              ) : (
                children.map(
                  (child, i) => areComponentsEqual(child.type, Tab) && getTabProps(child, i),
                )
              )}
              {children.map(child => areComponentsEqual(child.type, TabActions) && child)}
            </div>
          </>
        ) : (
          <>
            {twoColumn ? (
              <div className="tabs__list">
                {children.map(
                  (child, i) => areComponentsEqual(child.type, Tab) && getTabProps(child, i),
                )}
              </div>
            ) : (
              children.map(
                (child, i) => areComponentsEqual(child.type, Tab) && getTabProps(child, i),
              )
            )}
            {children.map(child => areComponentsEqual(child.type, TabActions) && child)}
          </>
        )}
      </div>
      <div className="tabs__content">
        {children.map(child => areComponentsEqual(child.type, TabContent) && child)}
        {children.map(
          child =>
            areComponentsEqual(child.type, Tab) &&
            areComponentsEqual(child.type, TabContent) &&
            areComponentsEqual(child.type, TabActions) &&
            child,
        )}
      </div>
    </div>
  );
};

Tabs.propTypes = {
  callBack: PropTypes.func,
  activeTab: PropTypes.any,
  className: PropTypes.string,
  type: PropTypes.string,
};

export { Tab, TabActions, TabContent };
export default memo(Tabs);
