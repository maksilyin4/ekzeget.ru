import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import cx from 'classnames';

// const viewLabalWithTag = (label, tag) => {
//   const styles = { fontSize: 'inherit', lineHeight: 'inherit', fontWeight: 'inherit' };

//   switch (tag) {
//     case 'h1':
//       return <h1 style={styles}>{label}</h1>;
//     case 'h2':
//       return <h2 style={styles}>{label}</h2>;
//     case 'h3':
//       return <h3 style={styles}>{label}</h3>;
//     case 'h4':
//       return <h4 style={styles}>{label}</h4>;
//     case 'h5':
//       return <h5 style={styles}>{label}</h5>;
//     case 'h6':
//       return <h6 style={styles}>{label}</h6>;
//     default:
//       return label;
//   }
// };

export const Tab = ({
  label,
  subscript,
  value,
  activeTab,
  mod,
  callBack,
  className = '',
  classNameTabItem = '',
}) => (
  <>
    <button
      className={cx('tab', className, {
        active: activeTab === value,
        tab_big: mod === 'big',
      })}
      onClick={() => callBack(value)}
      title={label}>
      {subscript ? (<>{label}<br/> <small>{subscript}</small> </>) : label}
    </button>
    {classNameTabItem && (
      <div
        className={cx('tab-item', classNameTabItem, {
          active: activeTab === value,
        })}
      />
    )}
  </>
);

Tab.propTypes = {
  value: PropTypes.any,
  label: PropTypes.string,
  href: PropTypes.string,
  mod: PropTypes.string,
  className: PropTypes.string,
  classNameTabItem: PropTypes.string,
  callBack: PropTypes.func,
  activeTab: PropTypes.any,
};
