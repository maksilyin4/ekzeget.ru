import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

export const TabActions = ({ className, children }) => (
  <div className={cx('tab__actions', className)}>{children}</div>
);

TabActions.propTypes = {
  className: PropTypes.string,
};
