import React, { useEffect, createContext } from 'react';
import { connect, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import cx from 'classnames';

import { getQuizList, getCurrentQueeze } from '../../dist/actions/Quiz';
import { getCompletedQuestions, getResultsIsLoading } from '../../dist/selectors/Quiz';

import QuizLink from '../QuizUI/QuizLink';
import QuizResultsBody from './QuizResultsBody';

import './styles.scss';
import Preloader from '../Preloader/Preloader';
import { DESC_QUIZ_RESULTS, TITLE_QUIZ_RESULTS } from '../../pages/Quiz/constants';
import { smoothScrollTo } from '../../dist/utils';
import { getIsSafari } from '../../store/browser/selectors';

export const QuizResultsContext = createContext(null);

const QuizResults = ({ getQuizList, completedQuestions, isLoading, getCurrentQueeze }) => {
  const isSafari = useSelector(getIsSafari);

  useEffect(() => {
    getQuizList();

    smoothScrollTo();
  }, []);

  return (
    <QuizResultsContext.Provider value={[getCurrentQueeze]}>
      <Helmet>
        <title> {TITLE_QUIZ_RESULTS} </title>
        <meta name="description" content={DESC_QUIZ_RESULTS} />
      </Helmet>
      <div
        className={cx('quiz-questions__header', {
          'quiz-testament__header-safari-results': isSafari,
        })}>
        <div className="quiz-title">
          <h1 className="page__title">Библейская викторина</h1>
        </div>
        <div className="quiz-results__start-again">
          <QuizLink text="Начать заново" path="/bibleyskaya-viktorina/" />
        </div>
      </div>
      {isLoading ? <Preloader /> : <QuizResultsBody completedQuestions={completedQuestions} />}
    </QuizResultsContext.Provider>
  );
};

const mapStateToProps = state => ({
  completedQuestions: getCompletedQuestions(state),
  isLoading: getResultsIsLoading(state),
});

const mapDispatchToProps = {
  getQuizList,
  getCurrentQueeze,
};

QuizResults.propTypes = {
  getQuizList: PropTypes.func,
  completedQuestions: PropTypes.array,
  isLoading: PropTypes.bool,
  getCurrentQueeze: PropTypes.func,
};

export default connect(mapStateToProps, mapDispatchToProps)(QuizResults);
