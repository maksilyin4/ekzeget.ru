import React, {useState, useContext, useMemo, useCallback, memo} from 'react';
import cx from 'classnames';
import dayjs from 'dayjs';
import ruLocale from 'dayjs/locale/ru';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import QuizNumber from '../QuizUI/QuizNumber/';
import { continueQuiz } from '../../dist/quizAPI';
import { completedQuestionsTypes } from './commonResultsTypes';
import { QuizResultsContext } from './index';
import { DEFAULT_QUIZ_URL, getQuestionParams } from '../../dist/utils';
import {connect} from 'react-redux';
import {getCurrentQuestion} from '~dist/actions/Quiz';
import {getCurrentQuestionData} from '~dist/selectors/Quiz';

import './styles.scss';

const mapStateToProps = (state) => ({
  questionData: getCurrentQuestionData(state)
});
const mapDispatchToProps = { getCurrentQuestion };

const maxOfVisibleBooks = 5;
const hideCountOfWord = 3;

const QuizResult = ({
  questions,
  id,
  created_at,
  status,
  isFirst = false,
  description,
  books,
  meta,
  current_question_code = DEFAULT_QUIZ_URL,
  current_question = 1,
  history: {
    push,
    location: { pathname },
  },
  getCurrentQuestion,
  questionData
}) => {
  const [getCurrentQueeze] = useContext(QuizResultsContext);
  const [isShowBooks, setShowState] = useState(false);
  const [params] = useState(() =>
    getQuestionParams(questions, current_question - 1, current_question),
  );

  const declensionQuiz = useCallback(count => {
    if (count === 1) {
      return 'вопрос';
    }

    if (count > 1 && count <= 4) {
      return 'вопроса';
    }

    return 'вопросов';
  }, []);

  const renderCountCorrectQuestions = useMemo(
    () =>
      `Вы ответили верно на ${meta.correct} ${declensionQuiz(Number(meta.correct))} из ${
        meta.total
      }`,
    [declensionQuiz, meta.correct, meta.total],
  );

  const toggleShowState = useCallback(() => setShowState(!isShowBooks), [isShowBooks]);

  const getContinueData = () =>
    continueQuiz(id, getCurrentQueeze, push, current_question_code, params);

  // Я уже не помню шо это за пиздец, но он работает
  const renderBooks = useMemo(
    () => (
      <ul className="quiz-result__books">
        {!isShowBooks
          ? books.slice(0, maxOfVisibleBooks).map(({ title, code }, index) => {
              // Тк у нас есть одни и те же книги в массивах, id книги
              const linkTo = `/bible/${code}/glava-1/`;
              return (
                <li className="quiz-result__book" key={code}>
                  {books.length > maxOfVisibleBooks ? (
                    books.slice(0, maxOfVisibleBooks).length - 1 === index ? (
                      <span>{`${title.slice(0, -hideCountOfWord)}...`}</span>
                    ) : (
                      <Link to={linkTo} className="quiz-result__book-link">
                        {`${title}, `}
                      </Link>
                    )
                  ) : (
                    <Link to={linkTo} className="quiz-result__book-link">
                      {`${title} `}
                    </Link>
                  )}
                </li>
              );
            })
          : books.map(({ bookId, title, code }, index) => (
              // Тк у нас есть одни и те же книги в массивах, id книги
              <li className="quiz-result__book" key={`${bookId}${code}`}>
                <Link to={`/bible/${code}/glava-1/`} className="quiz-result__book-link">
                  {` ${title}${books.length - 1 !== index ? ', ' : '.'}`}
                </Link>
              </li>
            ))}
      </ul>
    ),
    [books, id, isShowBooks],
  );

  const [currentQuestion, setCurrentQuestion] = useState(null);
  const [currentStatus, setCurrentStatus] = useState(null);

  const chooseHandler = (code, submittedAnswerId, status) => {
    getCurrentQuestion(code);
    setCurrentQuestion(submittedAnswerId)

    switch (status) {
      case 'OK':
        setCurrentStatus(true);
        break;
      case 'NO':
        setCurrentStatus(false);
        break;
      default:
    }
  }

  const currentMessage = useMemo(() => {
    const answers = questionData?.question?.answers || [];
    const question = questionData?.question?.title;
    const id = questionData?.question?.id;
    const link = questionData?.question?.code;

    let rightAnswer;
    let currentAnswer;

    answers.forEach(({ id, is_correct, text }) => {
      if (currentQuestion === id) {
        currentAnswer = text;
        return;
      }

      if (is_correct) {
        rightAnswer = text;
      }
    })

    return {
      link,
      question,
      id,
      rightAnswer,
      currentAnswer
    }
  }, [currentQuestion, questionData.question]);

  return (
    <div className="quiz-result__container">
      <div
        className={cx('quiz-result__border-container', {
          'quiz-result__latest': isFirst,
        })}>
        <div className="quiz-result__desc-main">
          <span className="quiz-result__date">
            {dayjs(created_at)
              .locale('ru', ruLocale)
              .format('DD MMM YYYY')}
          </span>
          {questions.length && (
            <span className="quiz-result__desc">{renderCountCorrectQuestions}</span>
          )}
        </div>
        <div className="quiz-result__text-container">
          <div className="quiz-result__left-side">
            <span className="quiz-result__text"> {description} </span>
            <div className="quiz-result__books-container">
              {books.length && renderBooks}
              {books.length > 5 && (
                <button
                  className="page__desc-toggle text_underline quiz-result__btn-desc"
                  onClick={toggleShowState}>
                  {isShowBooks ? 'Скрыть все книги' : 'Показать все книги'}
                </button>
              )}
            </div>
          </div>
          <div className="quiz-result__desc-container">
            {(status === 'P' || status === 'R') && (
                <button className="btn quiz-ui__link" onClick={getContinueData}>
                  Продолжить
                  <i className="quiz-ui__right" />
                </button>
            )}
          </div>
        </div>
        <ul className="quiz-questions__count">
          {questions.map(({ num, status, code, submittedAnswerId }) => {
            return (
              <QuizNumber
                key={num}
                questionIndex={num - 1}
                status={status}
                onClick={() => chooseHandler(code, submittedAnswerId, status)}
              />
            )
          })}
        </ul>
        {currentMessage?.currentAnswer && (
          <div className="quiz-questions__message">
            <Link
              to={{
                pathname: `/bibleyskaya-viktorina/voprosi/${currentMessage?.link}/`,
                state: {
                  questionId: 1,
                  isUniqQuestion: true,
                  currentId: currentMessage?.id,
                  prevUrlAllQuestions: pathname,
                },
              }}
              className="quiz-questions__message-question"
            >
              {currentMessage?.question}
            </Link>
            {currentStatus ? (
              <div className="quiz-questions__message-right">
                <div className="quiz-questions__message-header">
                  Верно!
                </div>
                <div className="quiz-questions__message-description">
                  {currentMessage?.currentAnswer}
                </div>
              </div>
            ) : (
              <>
                <div className="quiz-questions__message-wrong">
                  <div className="quiz-questions__message-header">
                    Не верно!
                  </div>
                  <div className="quiz-questions__message-description">
                    {currentMessage?.currentAnswer}
                  </div>
                </div>
                <div className="quiz-questions__message-right">
                  <div className="quiz-questions__message-header">
                    Верный ответ:
                  </div>
                  <div className="quiz-questions__message-description">
                    {currentMessage?.rightAnswer}
                  </div>
                </div>
              </>
            )}
          </div>
        )}
      </div>
    </div>
  );
};

QuizResult.propTypes = {
  isFirst: PropTypes.bool,
  history: PropTypes.shape({
    push: PropTypes.func,
  }),
  ...completedQuestionsTypes,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(memo(QuizResult)));
