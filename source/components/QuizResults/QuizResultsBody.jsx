import React, {memo, useMemo} from 'react';
import PropTypes from 'prop-types';

import QuizResult from './QuizResult';
import { completedQuestionsTypes } from './commonResultsTypes';

const QuizResultsBody = ({ completedQuestions = [] }) => {

  const completedQuestionsSort = useMemo(() => {
    return completedQuestions.sort((a, b) => new Date(b.created_at) - new Date(a.created_at))
  }, [completedQuestions]);

  if (!completedQuestions.length) {
    return <p className="quiz__not-found"> У вас отсутствуют начатые или завершённые викторины </p>;
  }

  return (
    <div className="quiz-results__container quiz-testaments">
      <p className="quiz-questions__title quiz-results__title">Результаты</p>
      {/* Возможно стоит проще через slice поделить и isFirst выводить вне цикла */}
      {completedQuestionsSort.map(
        (
          {
            questions,
            created_at,
            status,
            id,
            description,
            books,
            meta,
            current_question_code,
            current_question,
          },
          index,
        ) => (
          <QuizResult
            key={id}
            id={id}
            questions={questions}
            created_at={created_at}
            status={status}
            description={description}
            books={books}
            meta={meta}
            current_question={current_question}
            isFirst={!index}
            current_question_code={current_question_code}
          />
        ),
      )}
    </div>
  );
};

QuizResultsBody.propTypes = {
  completedQuestions: PropTypes.arrayOf(
    PropTypes.shape({
      ...completedQuestionsTypes,
    }),
  ),
};

export default memo(QuizResultsBody);
