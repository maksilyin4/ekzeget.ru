import PropTypes from 'prop-types';

export const completedQuestionsTypes = {
  questions: PropTypes.array,
  created_at: PropTypes.string,
  status: PropTypes.string,
  id: PropTypes.number,
  description: PropTypes.string,
  books: PropTypes.array,
  meta: PropTypes.shape({
    correct: PropTypes.number,
    total: PropTypes.number,
  }),
  current_question: PropTypes.number,
  current_question_code: PropTypes.string,
};
