import React from 'react';
import cx from 'classnames';
import { Link } from 'react-router-dom';
import Icon from '../Icons/Icons';

import './button.scss';

const Button = ({
  alt = '',
  className,
  type = 'default',
  onClick,
  title,
  icon,
  href,
  style,
  size,
}) =>
  href ? (
    <Link
      to={href}
      className={cx('btn', { btn_circle: type === 'circle', [`btn_${size}`]: size }, className)}
      onClick={onClick}
      style={style}>
      {title}
      {icon && <Icon icon={icon} />}
    </Link>
  ) : (
    <button
      className={cx('btn delete', className, {
        btn_circle: type === 'circle',
        [`btn_${size}`]: size,
      })}
      onClick={onClick}
      style={style}>
      {title}
      {icon && <Icon icon={icon} title={alt} />}
    </button>
  );

export default Button;
