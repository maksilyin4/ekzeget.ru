import React, { Component } from 'react';
import cx from 'classnames';

import { declesion } from '../../dist/utils';

class Sticker extends Component {
  render() {
    const { count, isDesktop, isTablet } = this.props;
    return !isDesktop && !isTablet ? (
      <span className={cx('video__item-sticker')}>{count}</span>
    ) : (
      <span className={cx('video__item-sticker video__item-tabs')}>
        {`${count} ${declesion(count)}`}
      </span>
    );
  }
}

export default Sticker;
