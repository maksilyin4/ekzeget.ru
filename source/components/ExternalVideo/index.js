import React, { memo, useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import './style.scss';
import { useOutsideClick } from '../../hooks/useOutsideClick';

const externalVideoClassname = 'external-video';

let script;
const youtubePromise = new Promise(resolve => {
  if (window && window.YT) {
    resolve(window.YT);
  }
  window.onYouTubeIframeAPIReady = () => {
    if (script) {
      script.parentElement.removeChild(script);
      script = null;
    }
    resolve(window && window.YT);
  };
});

let scriptRequested = false;

const loadYoutubePlayer = () => {
  return new Promise(resolve => {
    if (!scriptRequested) {
      scriptRequested = true;

      const urlSearchParams = [['origin', window.location.origin]];
      const url = new URL('https://www.youtube.com/iframe_api');

      urlSearchParams.forEach(([key, value]) => url.searchParams.set(key, value));

      script = document.createElement('script');
      script.src = url;
      document.head.appendChild(script);
      script.onload = () => {
        resolve(true);
      };
    }
    resolve(true);
  });
};

const ExternalVideo = ({ data, className = '' }) => {
  const videoId = data.match(/[^\/\v=]*$/)[0];
  const videoWrapperElementRef = useRef(null);
  const playerElementRef = useRef(null);

  const [isLoadedPlayer, setLoadedPlayer] = useState(false);
  const [isPlaying, setIsPlaying] = useState(false);

  function stopVideo() {
    if (isPlaying) {
      playerElementRef.current.pauseVideo();
      setIsPlaying(false);
      playerElementRef.current.destroy();
    }
  }

  useOutsideClick(videoWrapperElementRef, () => stopVideo(), '.external-video');

  function initVideoPlayer(e, videoId) {
    setIsPlaying(true);
    try {
      youtubePromise.then(YT => {
        const { current: wrapper } = videoWrapperElementRef;
        if (wrapper) {
          // eslint-disable-next-line no-new
          new YT.Player(wrapper, {
            heigth: 'auto',
            width: '100%',
            events: {
              onReady({ target: player }) {
                playerElementRef.current = player;
                playerElementRef.current.playVideo();
              }
            },
            playerVars: {
              autoplay: 1,
              controls: 2,
              enablejsapi: 1,
              fs: 1,
              iv_load_policy: 3,
              modestbranding: 1,
              playsinline: 0,
              rel: 0,
              showinfo: 0
            },
            videoId,
          });
        }
      });
    } catch (e) {
      console.log(e);
    }
  }

  useEffect(() => {
    async function initVideoBlock() {
      try {
        const response = await loadYoutubePlayer();
        setLoadedPlayer(response);
      } catch (e) {
        console.error(e);
      }
    }
    if (!isLoadedPlayer) {
      initVideoBlock();
    }
  }, [isLoadedPlayer, videoId]);

  const [image, setImage] = useState(`url(https://i1.ytimg.com/vi/${videoId}/maxresdefault.jpg)`);
  useEffect(() => {
    const image = document.createElement('img');
    image.src = `https://i1.ytimg.com/vi/${videoId}/maxresdefault.jpg`;
    image.onload = function(e) {
      if (e.path?.[0].height === 90) {
        setImage(`url(https://i1.ytimg.com/vi/${videoId}/hqdefault.jpg)`);
      }
      image.remove();
    };
  }, []);

  return (
    <div className={`${externalVideoClassname} ${className}`}>
      <div
        className={cx(`${externalVideoClassname}__poster`, {
          [`${externalVideoClassname}__poster-opacity`]: isPlaying,
        })}
        style={{ backgroundImage: image }}
        onClick={event => !isPlaying && initVideoPlayer(event, videoId)}
      />
      <div ref={videoWrapperElementRef} className={`${externalVideoClassname}__video`} />
    </div>
  );
};

ExternalVideo.propTypes = {
  data: PropTypes.object.isRequired,
};

ExternalVideo.defaultProps = {
  // eslint-disable-next-line react/default-props-match-prop-types
  data: {},
};

export default memo(ExternalVideo);
