import React from 'react';
import InputMask from 'react-input-mask';

function MaskedInput({ mask, disabled, value, onChange, placeholder, ...props }) {
  return (
    <InputMask
      type="text"
      disabled={disabled}
      mask={mask}
      value={value}
      onChange={e => {
        onChange(e.target.value);
      }}
      placeholder={placeholder}
      {...props}
    />
  );
}

export default MaskedInput;
