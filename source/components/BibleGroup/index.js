import React from 'react';
import PropTypes from 'prop-types';
import dayjs from 'dayjs';

import BibleGroupItem from './BibleGroupItem';
import './biblegroups.scss';

const BibleGroup = ({ bibleGroups }) => (
  <div className="bibleGroups-container">
    {bibleGroups.length ? (
      bibleGroups
        .sort((a, b) => {
          if (process.env.BROWSER && !!window.chrome && !!window.chrome.webstore) {
            return dayjs(b.data).unix() - dayjs(a.data).unix();
          }
          return b.id - a.id;
        })
        .map(item => <BibleGroupItem item={item} key={item.id} idGroup={item.code} />)
    ) : (
      <p>Данные отсутствуют</p>
    )}
  </div>
);

BibleGroup.defaultProps = {
  bibleGroups: [],
};

BibleGroup.propTypes = {
  bibleGroups: PropTypes.arrayOf(
    PropTypes.shape({
      data: PropTypes.string,
    }),
  ),
};

export default BibleGroup;
