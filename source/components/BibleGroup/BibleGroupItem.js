import React, { useMemo } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import shape from '../../assets/images/shape.png';

const BibleItem = ({ item, idGroup }) =>
  useMemo(
    () => (
      <div className="sheet position">
        <div className="header-sheet">
          {item.country ? (
            <Link to={`/bible-group/${idGroup}`} className="header-sheet__update sticker_hover">
              Новые группы
            </Link>
          ) : (
            <Link
              to={`/bible-group/for-leaders/${idGroup}`}
              className="header-sheet__update header_orange sticker_hover">
              Новые материалы
            </Link>
          )}
          <div className="header-sheet__data">{item.data.length < 16 ? '' : item.data}</div>
          {item.region && (
            <div className="header-sheet-location">
              <div className="header-sheet-location__shape">
                <img src={shape} alt={item.name} />
              </div>
              <div className="header-sheet-location__description">{item.region}</div>
            </div>
          )}
        </div>
        <div className="about">
          <div className="text">
            {item.name ? (
              <Link to={`/bible-group/${idGroup}`} className="text__header">
                {item.name}
              </Link>
            ) : (
              <Link to={`/bible-group/for-leaders/${idGroup}`} className="text__header">
                {item.title}
              </Link>
            )}
            <div className="text__description">{item.history}</div>
          </div>
          <div className="more">
            {!item.country ? (
              <Link to={`/bible-group/for-leaders/${idGroup}`}>
                <button className="more__btn">Подробнее</button>
              </Link>
            ) : (
              <Link to={`/bible-group/${idGroup}`}>
                <button className="more__btn">Подробнее</button>
              </Link>
            )}
          </div>
        </div>
      </div>
    ),
    [idGroup, item],
  );

BibleItem.defaultProps = {
  idGroup: '',
  item: {},
};

BibleItem.propTypes = {
  idGroup: PropTypes.string,
  item: PropTypes.arrayOf(
    PropTypes.shape({
      data: PropTypes.string,
      country: PropTypes.string,
      region: PropTypes.string,
      name: PropTypes.string,
      title: PropTypes.string,
      history: PropTypes.string,
    }),
  ),
};

export default BibleItem;
