import React from 'react';
import './styles.scss';
import progressBar from './assets/progress.png';
import progressArrow from './assets/progress-arrow.svg';
import { wordEnding } from '~utils/wordEnding';
import cn from 'classnames';
import R from 'ramda';
import { parseDateFrom } from '~pages/CreatePlan/CreateGroup/model/parseDate';
import { findCurrentReadingDay } from '../CustomPlanModule/utils/getActualCurrentDay';
import useProgressPlan from '../../hooks/useProgressPlan';

export default function ReadingProgressBar({ userPlans, className, withTop }) {
  const [allDays, startTime, stop_date] = React.useMemo(
    () => [
      R.pathOr(0, ['plan', 'lenght'], userPlans),
      R.pathOr('', ['start_date'], userPlans),
      R.pathOr('', ['stop_date'], userPlans),
    ],
    [userPlans],
  );

  const { closedDays } = useProgressPlan();
  const currentDay = closedDays.length;
  const currentPlanDay = findCurrentReadingDay(userPlans);
  const progress = (closedDays.length / allDays) * 100;
  const progressPercent = progress > 100 || !progress ? 0 : progress;
  const lagPercent = (currentPlanDay / allDays) * 100;

  return (
    <div className={className}>
      {withTop && (
        <div className="rpb__progress-text">
          пройдено <span>{progressPercent.toFixed(1)}%</span> плана
        </div>
      )}
      <div className={cn('rpb__wrapper')}>
        <div className="rpb__bar-wrapper">
          <img className="rpb__bar" src={progressBar} alt="" />
        </div>
        <div style={{ width: `${Math.ceil(progressPercent)}%` }} className="rpb__progress">
          <div className="rpb__num">{0}</div>
          <div className="rpb__progress-area" />
          <div
            style={{ translate: progressPercent > 80 ? '-30px' : undefined }}
            className="rpb__progress-area--arrow">
            <img src={progressArrow} alt="" />
            <span>{currentDay}</span>
          </div>
        </div>
        <div style={{ width: `${lagPercent > 100 ? 100 : lagPercent}%` }} className="rpb__lag">
          <div className="rpb__lag-area">
            {lagPercent > progressPercent && lagPercent < 60 && (
              <div style={{ paddingLeft: '30px' }} className="rpb__lag-text">
                Отставание {currentPlanDay - currentDay}{' '}
                {wordEnding(currentPlanDay - currentDay, ['день', 'дня', 'дней'])}
              </div>
            )}
          </div>
        </div>
        <div style={{ zIndex: 9 }} className="rpb__num right">
          {allDays}
        </div>
        {lagPercent > progressPercent && lagPercent > 60 && (
          <div className="rpb__lag-text lag-bottom">
            Отставание {currentPlanDay - currentDay}{' '}
            {wordEnding(currentPlanDay - currentDay, ['день', 'дня', 'дней'])}
          </div>
        )}
      </div>
      <div className="date-wrapper">
        <span>{parseDateFrom(startTime)}</span>
        <span>{parseDateFrom(stop_date)}</span>
      </div>
    </div>
  );
}
