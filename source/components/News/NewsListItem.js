import React, { useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import dayjs from 'dayjs';
import ruLocale from 'dayjs/locale/ru';
import LazyLoad from 'react-lazyload';

import { urlToAPI } from '../../dist/browserUtils';

const defaultIcon = require('./mocks/bitmap.png');

const NewsListItem = ({ title, body, date, id, img, code, optimizedImages, path }) => {
  const [url] = useState(urlToAPI);

  const partBody = `${body
    .replace(/<[a-z]+[^>]*>|<\/[a-z]+>/g, '')
    .replace(/ +/g, ' ')
    .slice(0, 120)}...`;

  const newtItem = useMemo(
    () => (
      <li className="news__list-item">
        <LazyLoad height={180}>
          {optimizedImages && Object.keys(optimizedImages).length ? (
            <picture className="news__list-picture">
              {optimizedImages.news_image.webp && (
                <source
                  media="(min-width: 769px)"
                  srcSet={`${url}${optimizedImages.news_image.webp}`}
                  type="image/webp"
                />
              )}
              {optimizedImages.news_image_mob.webp && (
                <source srcSet={`${url}${optimizedImages.news_image_mob.webp}`} type="image/webp" />
              )}
              <source
                media="(min-width: 769px)"
                srcSet={`${url}${optimizedImages.news_image.original}`}
              />
              <img src={`${url}${optimizedImages.news_image_mob.original}`} alt={title} />
            </picture>
          ) : img ? (
            <img src={`${url}${img}`} alt={title} />
          ) : (
            <img src={defaultIcon} alt={title} />
          )}
        </LazyLoad>
        <div className="news__content-wrapper">
          <div className="news__list-text">
            <div className="news__list-text-link">
              <Link to="/">ekzeget.ru</Link>
              <p>
                {dayjs
                  .unix(date)
                  .locale('ru', ruLocale)
                  .format('DD MMM YYYY')}
              </p>
            </div>
            <Link
              to={{
                pathname: `${path}${code}/`,
                state: { id },
              }}>
              <h2 title={title}>{title}</h2>
            </Link>
            <p
              className="news__list-text__preview"
              dangerouslySetInnerHTML={{ __html: partBody }}
            />
          </div>
          <div className="news__list-btn">
            <Link
              to={{
                pathname: `${path}${code}/`,
                state: { id },
              }}>
              Подробнее
            </Link>
          </div>
        </div>
      </li>
    ),
    [id, title],
  );

  return newtItem;
};

NewsListItem.propTypes = {
  title: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
  date: PropTypes.number.isRequired,
  id: PropTypes.number.isRequired,
  code: PropTypes.string.isRequired,
  path: PropTypes.string.isRequired,
};

export default NewsListItem;
