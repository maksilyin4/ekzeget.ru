import R from 'ramda';
import React, { Component } from 'react';
import { _AXIOS } from '../../dist/ApiConfig';
import NewsListItem from './NewsListItem';
import Preloader from '../Preloader/Preloader';

import '../LatestInterpritations/latestInterpritations.scss';
import '../LatestNews/latestNews.scss';

export default class NewsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: false,
      error: null,
      list: R.pathOr([], ['directProps', 'article'], this.props),
    };

    if (!this.state.list.length) {
      this.state.list = R.pathOr([], ['initialState', 'article'], this.props);
    }
  }

  componentDidMount() {
    const { list } = this.state;
    const { articleType = 1 } = this.props;

    if (!list.length) {
      this.setState({
        isFetching: true,
      });

      this.getArticleRequest(articleType);
    }
  }

  getArticleRequest = articleType =>
    _AXIOS({
      url: `article/${articleType}/?per-page=40&img_sizes[]=news_image&img_sizes[]=news_image_mob`,
    })
      .then(({ article }) =>
        this.setState({
          isFetching: false,
          list: article,
        }),
      )
      .catch(({ data: error }) =>
        this.setState({
          isFetching: false,
          error: R.pathOr('Произошла ошибка', ['error', 'message'], error),
        }),
      );

  render() {
    const { list, isFetching, error } = this.state;
    const {
      match: { url },
    } = this.props;

    return (
      <div className="news__body__list content">
        {isFetching ? (
          <div className="news__preloader">
            <Preloader />
          </div>
        ) : (
          <>
            {error ? (
              <p className="empty-content"> {error} </p>
            ) : list.length ? (
              <ul className="news__list">
                {list.map(newItem => (
                  <NewsListItem
                    key={newItem.id}
                    id={newItem.id}
                    title={newItem.title}
                    body={newItem.body}
                    icon={newItem.thumbnail_path}
                    date={newItem.updated_at}
                    img={newItem.image}
                    optimizedImages={newItem.optimized}
                    code={newItem.slug}
                    path={url}
                  />
                ))}
              </ul>
            ) : (
              <p className="empty-content">Новостей пока ещё нет</p>
            )}
          </>
        )}
      </div>
    );
  }
}
