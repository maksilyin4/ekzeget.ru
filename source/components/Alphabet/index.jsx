import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import './style.scss';

const Alphabet = ({
  alphabet = [],
  activeLetter = 'a',
  regex = '',
  onClick = () => f => f,
  isTest = true,
  className = '',
  popupId = '',
}) => {
  return (
    <div className={cx('alphabet-filter', className, { [`popup_${popupId}`]: popupId })}>
      <div className={cx('alphabet-filter__list', { [`popup_${popupId}`]: popupId })}>
        {alphabet.length > 0 &&
          alphabet.map(letter => {
            if ((regex && regex.test(letter)) || isTest) {
              return (
                <button
                  key={letter}
                  className={cx('alphabet-filter__item', {
                    [`popup_${popupId}`]: popupId,
                    active: activeLetter === letter,
                  })}
                  onClick={onClick(letter)}>
                  {letter}
                </button>
              );
            }
          })}
      </div>
    </div>
  );
};

Alphabet.propTypes = {
  alphabet: PropTypes.array,
  className: PropTypes.string,
  activeLetter: PropTypes.string,
  onClick: PropTypes.func,
  regex: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(RegExp)]),
  isTest: PropTypes.bool,
};

export default Alphabet;
