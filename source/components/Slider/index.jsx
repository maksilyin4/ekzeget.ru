import React from 'react';
import Slider from 'react-slick';

import defaultSettings from './settings';

import './slider.scss';

const S = ({ isDefailtSettings, children, settings, refTo, ...props }) => {
  const initialSettings = isDefailtSettings ? defaultSettings : {};
  return (
    <Slider ref={refTo} {...settings} {...initialSettings} {...props}>
      {children}
    </Slider>
  );
};

export default S;
