import React from 'react';
import Loadable from 'react-loadable';
import Preloader from '../Preloader/Preloader';

const LoadableBar = Loadable({
  loader: () => import('./BooksList'),
  loading() {
    return (
      <div>
        <Preloader />
      </div>
    );
  },
  render(loaded, props) {
    const Component = loaded.default;
    return <Component initialState={props.initialState} />;
  },
});

export default LoadableBar;
