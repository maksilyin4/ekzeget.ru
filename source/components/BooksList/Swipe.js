import React from 'react';
import Slick from 'react-slick';

import '../../assets/sass/slider.scss';

import CollectionList, { CollectionItem } from '../Collections/CollectionList/CollectionList';

const settings = {
  dots: true,
  swipe: true,
  arrows: false,
};

const Swipe = ({ subarray }) => (
  <Slick {...settings}>
    {subarray.map((book, i) => (
      // eslint-disable-next-line react/no-array-index-key
      <div key={i}>
        <CollectionList>
          {book.map(i => (
            <CollectionItem key={i.id} href={`/bible/${i.code}/glava-1/`} title={i.menu} />
          ))}
        </CollectionList>
      </div>
    ))}
  </Slick>
);

export default Swipe;
