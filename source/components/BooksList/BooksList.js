import React, { Component } from 'react';
import { connect } from 'react-redux';

import bibleActions from '../../dist/actions/Bible';

import { getIsMobile, getIsTablet, getIsDesktop } from '../../dist/selectors/screen';

import {
  getTestamentIsLoading,
  getNewTestament,
  getOldTestament,
} from '../../dist/selectors/Bible';
import Preloader from '../Preloader/Preloader';
import Tabs, { Tab, TabContent } from '../Tabs/Tabs';
import CollectionList, { CollectionItem } from '../Collections/CollectionList/CollectionList';
import Swipe from './Swipe';

import './booksList.scss';

class BooksList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTab: 1,
      sizeDesktop: 32,
      sizeMobile: 22,
      width: 0,
    };
  }

  componentDidMount() {
    const { newTestament, oldTestament } = this.props;

    if (!newTestament.length || !oldTestament.length) {
      this.props.getBooks();
    }

    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  updateWindowDimensions = () => {
    if (process.env.BROWSER) {
      this.setState({ width: window.innerWidth });
    }
  };

  handleChange = field => value => {
    this.setState({ [field]: value });
  };

  render() {
    const {
      oldTestament,
      newTestament,
      getTestamentIsLoading,
      isMobile,
      isTablet,
      isDesktop,
      notFoundPage,
    } = this.props;
    const { activeTab, sizeDesktop, sizeMobile } = this.state;
    const subarray = [];
    let size = 0;
    const currentTestament = activeTab === 2 ? newTestament : oldTestament;

    if (isMobile && this.state.width < 767) {
      size = sizeMobile;
    } else {
      size = sizeDesktop;
    }

    for (let i = 0; i < Math.ceil(currentTestament.length / size); i++) {
      subarray[i] = currentTestament.slice(i * size, i * size + size);
    }

    return (
      <Tabs activeTab={this.state.activeTab} callBack={this.handleChange('activeTab')}>
        <Tab label="Новый Завет" value={1} mod="big" />
        <Tab label="Ветхий Завет" value={2} mod="big" />
        <TabContent>
          {(isDesktop || isTablet || this.state.width === 767 || notFoundPage !== undefined) &&
          currentTestament.length <= 32 ? (
            subarray.map((book, i) => (
              <CollectionList key={i}>
                {book.map(i => (
                  <CollectionItem key={i.id} href={`/bible/${i.code}/glava-1/`} title={i.menu} />
                ))}
              </CollectionList>
            ))
          ) : (
            <Swipe subarray={subarray} />
          )}
          {!subarray.length && !getTestamentIsLoading && (
            <p className="empty-content"> Отсутствуют книги по Новому и Ветхому завету </p>
          )}
        </TabContent>
        {getTestamentIsLoading && <Preloader />}
      </Tabs>
    );
  }
}

const mapStateToProps = state => ({
  getTestamentIsLoading: getTestamentIsLoading(state),
  newTestament: getNewTestament(state),
  oldTestament: getOldTestament(state),
  isMobile: getIsMobile(state),
  isTablet: getIsTablet(state),
  isDesktop: getIsDesktop(state),
});
const mapDispatchToProps = { ...bibleActions };

export default connect(mapStateToProps, mapDispatchToProps)(BooksList);
