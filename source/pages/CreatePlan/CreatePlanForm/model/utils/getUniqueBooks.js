export function getUniqueBooks(planDetail) {
  const chapters = planDetail
    ? Object.values(planDetail)
        ?.flat(1)
        .map(({ book_code, branch }) => ({ book_code, branch }))
    : [];
  return Array.from(new Set(chapters?.map(JSON.stringify))).map(JSON.parse);
}
