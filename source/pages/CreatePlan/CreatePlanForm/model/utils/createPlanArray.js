function mergeArrays(...arrays) {
  const maxLength = Math.max(...arrays.map(arr => arr.length));

  return Array.from({ length: maxLength }, (_, index) => {
    return arrays.reduce((acc, arr) => {
      if (index < arr.length) {
        acc.push(arr[index]);
      }
      return acc;
    }, []);
  }).flat();
}

function getChaptersByBooks(branch, num) {
  const chapters = [];
  for (let i = 0; i < branch.length; i++) {
    for (let j = 1; j <= branch[i].parts; j++) {
      chapters.push({
        book_id: branch[i].id,
        chapter: j,
        branch: num,
        day: 0,
      });
    }
  }
  return chapters;
}

export function generateReadingPlan(params) {
  const allBooksNew = params.newBooks.books;
  const allBooksOld = params.oldBooks.books;

  const chaptersBranchNew1 = getChaptersByBooks(allBooksNew.branch1, 1);
  const chaptersBranchNew2 = getChaptersByBooks(allBooksNew.branch2, 2);
  const chaptersBranchNew3 = getChaptersByBooks(allBooksNew.branch3, 3);

  const chaptersBranchOld1 = getChaptersByBooks(allBooksOld.branch1, 1);
  const chaptersBranchOld2 = getChaptersByBooks(allBooksOld.branch2, 2);
  const chaptersBranchOld3 = getChaptersByBooks(allBooksOld.branch3, 3);

  const allChaptersNew = mergeArrays(
    ...[chaptersBranchNew1, chaptersBranchNew2, chaptersBranchNew3],
  );
  const allChaptersOld = mergeArrays(
    ...[chaptersBranchOld1, chaptersBranchOld2, chaptersBranchOld3],
  );

  const daysNew = params.newBooks.chaptersPerDay;
  const daysOld = params.oldBooks.chaptersPerDay;

  const result = [];
  let day = 1;

  while (allChaptersNew.length || allChaptersOld.length) {
    for (let i = 0; i < daysNew; i++) {
      if (allChaptersNew.length) {
        allChaptersNew[0].day = day;
        result.push(allChaptersNew[0]);
        allChaptersNew.shift();
      } else if (allChaptersOld.length) {
        allChaptersOld[0].day = day;
        result.push(allChaptersOld[0]);
        allChaptersOld.shift();
      }
    }

    for (let i = 0; i < daysOld; i++) {
      if (allChaptersOld.length) {
        allChaptersOld[0].day = day;
        result.push(allChaptersOld[0]);
        allChaptersOld.shift();
      } else if (allChaptersNew.length) {
        allChaptersNew[0].day = day;
        result.push(allChaptersNew[0]);
        allChaptersNew.shift();
      }
    }

    day++;
  }
  return result;
}
