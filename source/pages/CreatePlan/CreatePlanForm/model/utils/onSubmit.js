import { generateReadingPlan } from './createPlanArray';

export default function onSubmitPlan(values) {
  const readingPlan = generateReadingPlan({
    newBooks: {
      books: values.new_books,
      chaptersPerDay: values.new_chapters,
    },
    oldBooks: {
      books: values.old_books,
      chaptersPerDay: values.old_chapters,
    },
  });
  const valuesToSubmit = {
    description: values.name,
    lenght: Array.from(new Set(readingPlan.map(d => d.day))).length,
    comment: values.description,
    plan: readingPlan,
  };
  return valuesToSubmit;
}
