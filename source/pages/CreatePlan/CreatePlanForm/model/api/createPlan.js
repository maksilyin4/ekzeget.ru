import { AXIOS } from '~dist/ApiConfig';
import { LS } from '../../../../../dist/browserUtils';

export const createPlanForm = async (values, plan_id) => {
  const { data } = plan_id
    ? await AXIOS.put(`/reading/plan/${plan_id}`, values, {
        headers: {
          'X-Authentication-Token': LS.get('token'),
        },
      })
    : await AXIOS.post('/reading/plan/', values, {
        headers: {
          'X-Authentication-Token': LS.get('token'),
        },
      });
  return data;
};
