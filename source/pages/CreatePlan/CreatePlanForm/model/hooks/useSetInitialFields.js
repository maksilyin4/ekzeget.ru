import { useEffect } from 'react';
import { getName } from '~components/PlanAddUsers';
import { getDaysByObj } from '~components/Popups/ReadingPlanDetail/plan-days/getDaysByObj';
import { parseDateFrom } from '~pages/CreatePlan/CreateGroup/model/parseDate';

export default function useSetInitialFields(
  setFieldValue,
  planShort,
  planDetail,
  user,
  group,
  userPlans,
  plan_id,
) {
  useEffect(() => {
    if (planShort && planDetail && user) {
      setFieldValue('description', planShort.comment);
      setFieldValue('name', planShort.description);
      setFieldValue('all_chapters', planDetail[1]?.length);
    }
  }, [planShort, planDetail, user]);

  useEffect(() => {
    setFieldValue('mentor', getName(user.user));
  }, [user]);
  useEffect(() => {
    if (plan_id) {
      if (group?.schedule && Number(plan_id) === group.plan_id) {
        setFieldValue('week_days', getDaysByObj(group.schedule).days);
        setFieldValue('start_date', parseDateFrom(group.start_date));
        setFieldValue('end_date', parseDateFrom(group.stop_date));
      } else if (Object.keys(userPlans)[0]?.toString() === plan_id) {
        const plan = Object.values(userPlans)[0];
        setFieldValue('week_days', getDaysByObj(plan.schedule).days);
        setFieldValue('start_date', parseDateFrom(plan.start_date));
        setFieldValue('end_date', parseDateFrom(plan.stop_date));
      }
    }
  }, [group, userPlans, plan_id]);
}
