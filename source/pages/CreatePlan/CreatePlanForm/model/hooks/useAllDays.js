import { useMemo } from 'react';
import dayjs from 'dayjs';
import { parseDateFrom } from '~pages/CreatePlan/CreateGroup/model/parseDate';

export default function useAllDays(values, depsEndDate) {
  // Переместим startDate внутрь useMemo, чтобы избежать бесконечных перерендеров
  return useMemo(() => {
    const startDate = dayjs(parseDateFrom(values.start_date, true));
    if (!values.week_days?.length || !depsEndDate.every(Boolean) || !startDate.isValid()) {
      return undefined;
    }

    const arr = Object.values(values.old_books)
      .concat(Object.values(values.new_books))
      .flat(1)
      .map(v => v.parts);
    const totalChapters = arr?.reduce((a, b) => a + b, 0);
    const chaptersPerDay = values.all_chapters;
    const weekDays = values.week_days.map(day =>
      ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'].indexOf(day),
    );

    let dayCount = 0;
    let chaptersRead = 0;
    let currentDate = startDate;

    // Проверяем, является ли сегодняшний день днем чтения
    if (weekDays.includes(startDate.day())) {
      chaptersRead += chaptersPerDay; // Учитываем сегодняшний день, если он является днем чтения
    }

    // Прямой отсчет без поиска первого дня чтения
    while (chaptersRead < totalChapters) {
      if (weekDays.includes(currentDate.day())) {
        chaptersRead += chaptersPerDay;
      }
      dayCount++;
      currentDate = currentDate.add(1, 'day');
    }
    return dayCount;
  }, [depsEndDate, values, values.start_date]); // Указываем values.start_date в зависимостях для пересчета при его изменении
}
