import { useEffect } from 'react';
import { getUser } from '~dist/actions/User';
import { getMentorGroup, getUserGroup } from '../../../../../store/groups/actions';
import { useDispatch } from 'react-redux';
import readingPlanActions from '../../../../../dist/actions/ReadingPlan';

export default function useGetPlanData(plan_id, setLoading) {
  const dispatch = useDispatch();
  const { getReadingPlanDetail, getReadingPlans } = readingPlanActions;
  useEffect(() => {
    (async () => {
      try {
        setLoading(true);
        if (plan_id) {
          await Promise.all([
            dispatch(getReadingPlanDetail(plan_id)),
            dispatch(getReadingPlans()),
            dispatch(getUser()),
            dispatch(getMentorGroup()),
            dispatch(getUserGroup()),
          ]);
        }
        setLoading(false);
      } catch (e) {
        console.error(e);
        setLoading(false);
      }
    })();
  }, [plan_id]);
}
