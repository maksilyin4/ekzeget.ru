import { useFormik } from 'formik';
import { object, string, array } from 'yup';
import onSubmitPlan from '../utils/onSubmit';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { setFormValues, setFormValuesToSend } from '~store/createPlan/actions';
import { editGroup } from '~dist/api/readingPlan/createGroup';
import { createPlanForm } from '../api/createPlan';
import { includeDay } from '~pages/PreviewPlan/PreviewPlan';
import { parseDateFrom } from '../../../CreateGroup/model/parseDate';
import { createDate } from '../../../../PreviewPlan/PreviewPlan';
import { editUserReadingPlan } from '~dist/actions/ReadingPlan';

export default function useFormikPlan(initialValues, plan_id, userGroup) {
  const dispatch = useDispatch();
  const history = useHistory();

  const isEmptyBooks = books => {
    return Boolean(!books?.branch1?.length && !books?.branch2?.length && !books?.branch3?.length);
  };

  const isEdit = Boolean(plan_id);

  const validateDateStart = string()
    .min(10, 'Введите дату начала')
    .test('date-correct', 'Введите корректную дату начала', value => {
      const enteredDate = new Date(createDate(value));
      return !isNaN(enteredDate.getTime());
    })
    .test('date-pass', 'Дата начала не может быть раньше сегодняшнего дня', value => {
      const enteredDate = new Date(createDate(value));
      const today = new Date();
      today.setDate(today.getDate() - 1);
      return enteredDate >= today;
    })
    .test('date-late', `Дата начала не может быть позже чем месяц от сегодняшней`, value => {
      const enteredDate = new Date(createDate(value));
      const oneMonthLater = new Date();
      oneMonthLater.setMonth(oneMonthLater.getMonth() + 1);
      return enteredDate <= oneMonthLater;
    });

  const validateDate = string()
    .min(10, 'Введите дату начала')
    .test('date-correct', 'Введите корректную дату начала', value => {
      const enteredDate = new Date(createDate(value));
      return !isNaN(enteredDate.getTime());
    });
  const validationSchema = object({
    description: string().required('Введите описание'),
    name: string().required('Введите название'),
    mentor: string(),
    week_days: array().min(1, 'Выберите дни недели'),
    start_date: isEdit ? validateDate : validateDateStart,
    all_chapters: string(),
    new_chapters: string().test('newChaptersMin', 'Введите кол-во глав', (val, valParent) => {
      if (Number(valParent.parent.all_chapters) === 0) {
        return false;
      }
      return true;
    }),
    old_chapters: string().test('newChaptersMin', 'Введите кол-во глав', (val, valParent) => {
      if (Number(valParent.parent.all_chapters) === 0) {
        return false;
      }
      return true;
    }),
    new_books: object()
      .shape({
        branch1: array(),
        branch2: array(),
        branch3: array(),
      })
      .test('new-books-validation', 'Выберите книги', function(value) {
        const { old_books } = this.parent;
        return isEmptyBooks(old_books)
          ? Boolean(value.branch1.length || value.branch2.length || value.branch3.length)
          : true; // Пропускаем проверку, если old_books заполнены
      }),
    old_books: object()
      .shape({
        branch1: array(),
        branch2: array(),
        branch3: array(),
      })
      .test('old-books-validation', 'Выберите книги', function(value) {
        const { new_books } = this.parent;
        return isEmptyBooks(new_books)
          ? Boolean(value.branch1.length || value.branch2.length || value.branch3.length)
          : true; // Пропускаем проверку, если new_books заполнены
      }),
  });

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: async values => {
      const valuesToSubmit = onSubmitPlan(values);
      try {
        if (plan_id) {
          await createPlanForm(valuesToSubmit, plan_id);

          if (Number(plan_id) === userGroup.plan_id) {
            await editGroup({
              start_date: parseDateFrom(values.start_date, true),
              stop_date: parseDateFrom(values.end_date, true),
              check_date: parseDateFrom(values.end_date, true),
              schedule: {
                Mon: includeDay(values, 'пн'),
                Tue: includeDay(values, 'вт'),
                Wed: includeDay(values, 'ср'),
                Thu: includeDay(values, 'чт'),
                Fri: includeDay(values, 'пт'),
                Sat: includeDay(values, 'сб'),
                Sun: includeDay(values, 'вс'),
              },
            });
          }
          try {
            await dispatch(
              editUserReadingPlan({
                start_date: parseDateFrom(values.start_date, true),
                stop_date: parseDateFrom(values.end_date, true),
                schedule: {
                  Mon: includeDay(values, 'пн'),
                  Tue: includeDay(values, 'вт'),
                  Wed: includeDay(values, 'ср'),
                  Thu: includeDay(values, 'чт'),
                  Fri: includeDay(values, 'пт'),
                  Sat: includeDay(values, 'сб'),
                  Sun: includeDay(values, 'вс'),
                },
              }),
            );
          } catch (e) {
            console.log(e);
          }

          await dispatch(setFormValuesToSend(valuesToSubmit));
          await dispatch(setFormValues(values));
          history.push(`/reading-plan/preview-plan/${plan_id}`);
        } else {
          await dispatch(setFormValuesToSend(valuesToSubmit));
          await dispatch(setFormValues(values));
          history.push('/reading-plan/preview-plan');
        }
      } catch (e) {
        history.push('/reading-plan');
      }
    },
  });

  return formik;
}
