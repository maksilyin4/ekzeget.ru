import { useMemo } from 'react';
import dayjs from 'dayjs';

export default function useEndDate(start_date, allDays) {
  const endDate = useMemo(() => {
    if (start_date?.length === 10 && (allDays || allDays === 0)) {
      const date = new Date(
        start_date
          .split('.')
          .reverse()
          .join('-'),
      );
      date.setDate(date.getDate() + Number(allDays));
      return dayjs(date).format('DD.MM.YYYY');
    }
    return '';
  }, [start_date, allDays]);
  return endDate;
}
