import React, { useEffect, useMemo, useState } from 'react';
import CustomFieldCreatePlan from '../../../../../components/UICreatePlan/CustomField';
import BooksAccordion from '../../../../../components/UICreatePlan/BooksAccordion';
import AccordionHead from '~components/UICreatePlan/AccrodionHead';
import PlanDays from '~components/Popups/ReadingPlanDetail/plan-days';
import MaskedInput from '~components/MaskedInput';
import './styles.scss';
import '../../ui/styles.scss';
import { useMedia } from 'react-use';
import { getUniqueBooks } from '../utils/getUniqueBooks';
import { useParams } from 'react-router-dom';

export default function SecondStep({
  books,
  isEdit,
  planDetail,
  isOtherPlan,
  startDate,
  isGroup,
  values,
  setFieldValue,
}) {
  const isMobile = useMedia('(max-width: 768px)');
  const oldT = values.parallel_old_chapters;
  const newT = values.parallel_new_chapters;
  const { plan_id } = useParams();
  const uniqueSelectBooks = useMemo(() => {
    if (planDetail) {
      return getUniqueBooks(planDetail);
    }
    return [];
  }, [planDetail]);

  const getBooksChapters = variant => {
    return (
      Object.values(planDetail)[0].filter(b =>
        books[variant]?.books.some(u => b.book_code === u.code),
      ).length || 0
    );
  };

  const filterBooks = (variant, branch) => {
    return books[variant]?.books.filter(b =>
      uniqueSelectBooks.some(u => u.book_code === b.code && u.branch === branch),
    );
  };

  const getDetail = i => Object.values(planDetail)[i];
  const getBranches = variant => {
    const booksBranches = [...getDetail(0), ...getDetail(1), ...getDetail(2), ...getDetail(3)]
      .filter(
        b => books[variant]?.books.some(book => b.book_code === book.code) && b.branch !== null,
      )
      .map(b => b.branch);
    return booksBranches.length > 0 ? Math.max(...booksBranches) : 1;
  };

  useEffect(() => {
    if (
      isEdit &&
      planDetail &&
      plan_id &&
      uniqueSelectBooks.length &&
      (books?.['testament-1']?.books.length || books?.['testament-2']?.books.length)
    ) {
      setFieldValue('old_chapters', getBooksChapters('testament-1'));
      setFieldValue('new_chapters', getBooksChapters('testament-2'));

      (async () => {
        await setFieldValue('parallel_old_chapters', getBranches('testament-1'));
        await setFieldValue('parallel_new_chapters', getBranches('testament-2'));
      })();
      setFieldValue('new_books', {
        branch1: filterBooks('testament-2', 1),
        branch2: filterBooks('testament-2', 2),
        branch3: filterBooks('testament-2', 3),
      });
      setFieldValue('old_books', {
        branch1: filterBooks('testament-1', 1),
        branch2: filterBooks('testament-1', 2),
        branch3: filterBooks('testament-1', 3),
      });
    }
  }, [isEdit, uniqueSelectBooks, books, planDetail]);
  const [isFocus, setFocus] = useState(false);
  const handleChangeOld = v => setFieldValue('parallel_old_chapters', v);
  const fieldsDisabled = startDate && isEdit && new Date(startDate) < new Date();

  return (
    <>
      <div className="cp__block cp__step-two">
        <div className="cp__date-head">
          {!isMobile ? (
            <div className="cp__field-wrapper cp__date-start">
              <span className="cp__label-field">Начать</span>
              <MaskedInput
                placeholder={'__.__.____'}
                onChange={v => {
                  setFieldValue('start_date', v);
                }}
                value={values.start_date}
                className="cp__min-field"
                mask="99.99.9999"
              />
            </div>
          ) : (
            <div style={{ marginBottom: isFocus && '10px' }} className="mobile-top">
              <div className="cp__field-wrapper cp__date-start">
                <span className="cp__label-field">Начать</span>
                <MaskedInput
                  placeholder={'__.__.____'}
                  onChange={v => {
                    setFieldValue('start_date', v);
                  }}
                  value={values.start_date}
                  className="cp__min-field"
                  mask="99.99.9999"
                />
              </div>
              <div className="cp__field-wrapper cp__date-end">
                <span className="cp__label-field">Закончить</span>
                <div style={{ position: 'relative' }}>
                  <MaskedInput
                    onBlur={() => setFocus(false)}
                    onFocus={() => setFocus(true)}
                    placeholder={'__.__.____'}
                    disabled
                    value={values.end_date}
                    className="cp__min-field"
                    mask="99.99.9999"
                  />
                  {isFocus && <div className="auto_error">Рассчет автоматически</div>}
                </div>
              </div>
            </div>
          )}
          <div className="cp__center">
            {!isMobile && (
              <div>
                <div className="cp__field-wrapper cp__date-end">
                  <span className="cp__label-field">Закончить</span>
                  <div style={{ position: 'relative', marginBottom: isFocus && '10px' }}>
                    <MaskedInput
                      disabled
                      onBlur={() => setFocus(false)}
                      onFocus={() => setFocus(true)}
                      placeholder={'__.__.____'}
                      value={values.end_date}
                      className="cp__min-field"
                      mask="99.99.9999"
                    />
                    {isFocus && <div className="auto_error">Рассчет автоматически</div>}
                  </div>
                </div>
              </div>
            )}
            {!isOtherPlan && !isGroup && (
              <div className="cp__field-wrapper cp__all-chapters">
                <span className="cp__label-field">Всего глав в день</span>
                <CustomFieldCreatePlan
                  className="cp__min-field cp__min-field--one cp__min-field--two"
                  value={values.all_chapters}
                  type="min"
                />
              </div>
            )}
          </div>
          <div className="cp__days">
            <PlanDays
              isForm
              variant={'horizontal'}
              title="Выбрать дни недели"
              handleChange={v => {
                const val = values.week_days;
                if (val.includes(v)) {
                  setFieldValue(
                    'week_days',
                    val.filter(d => d !== v),
                  );
                } else {
                  setFieldValue('week_days', [...val, v]);
                }
              }}
              stateChange={values.week_days}
            />
          </div>
        </div>
        {!isOtherPlan && !isGroup && (
          <div className="cp__new-zavet">
            <AccordionHead
              disabled={fieldsDisabled}
              countChapter={values.new_chapters}
              onChange={v => setFieldValue('new_chapters', v)}
              onChangeParallel={v => setFieldValue('parallel_new_chapters', v)}
              countParallelChapter={newT}
              title="Новый завет"
            />
            <BooksAccordion
              disabled={fieldsDisabled}
              setFieldValue={setFieldValue}
              changeName={'new_books'}
              activeBooks={values.new_books}
              countBranches={newT}
              books={books['testament-2']}
            />
          </div>
        )}
        {!isOtherPlan && !isGroup && isMobile && (
          <div className="cp__veth-zavet">
            <AccordionHead
              disabled={fieldsDisabled}
              countChapter={values.old_chapters}
              onChange={v => setFieldValue('old_chapters', v)}
              onChangeParallel={handleChangeOld}
              countParallelChapter={oldT}
              title="Ветхий завет"
            />
            <BooksAccordion
              disabled={fieldsDisabled}
              setFieldValue={setFieldValue}
              changeName={'old_books'}
              activeBooks={values.old_books}
              countBranches={oldT}
              variant="gray"
              books={books['testament-1']}
            />
          </div>
        )}
        {!isOtherPlan && !isGroup && !isMobile && (
          <div className="cp__veth-zavet">
            <AccordionHead
              disabled={fieldsDisabled}
              countChapter={values.old_chapters}
              onChange={v => setFieldValue('old_chapters', v)}
              onChangeParallel={handleChangeOld}
              countParallelChapter={oldT}
              title="Ветхий завет"
            />
            <BooksAccordion
              disabled={fieldsDisabled}
              setFieldValue={setFieldValue}
              changeName={'old_books'}
              activeBooks={values.old_books}
              countBranches={oldT}
              variant="gray"
              books={books['testament-1']}
            />
          </div>
        )}
      </div>
    </>
  );
}
