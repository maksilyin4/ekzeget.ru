import React from 'react';
import CustomFieldCreatePlan from '../../../../../components/UICreatePlan/CustomField';
import '../../ui/styles.scss';
import './styles.scss';

export default function FirstStep({ values, setFieldValue }) {
  return (
    <div className="cp__block cp__top-flex">
      <div className="cp__field-wrapper cp__label-descr">
        <div className="cp__label-field ">Описание</div>
        <div className="cp__block-textarea--wrapper">
          <CustomFieldCreatePlan
            value={values.description}
            onChange={e => {
              if (e.target.value.length < 201) {
                setFieldValue('description', e.target.value);
              }
            }}
            className="cp__block-textarea"
            type="textarea"
          />
          <p className="cp__count-symb">Количество символов {values.description.length}/200</p>
        </div>
      </div>
      <div>
        <div className="cp__field-wrapper cp__name-plan">
          <div className="cp__label-field">Название плана: </div>
          <CustomFieldCreatePlan
            value={values.name}
            onChange={e => {
              if (e.target.value.length < 61) {
                setFieldValue('name', e.target.value);
              }
            }}
            className="cp__block-input"
          />
        </div>
        <div className="cp__field-wrapper">
          <div className="cp__label-field">Ведущий плана: </div>
          <CustomFieldCreatePlan
            disabled
            value={values.mentor}
            onChange={e => {
              setFieldValue('mentor', e.target.value);
            }}
            className="cp__block-input"
          />
        </div>
      </div>
    </div>
  );
}
