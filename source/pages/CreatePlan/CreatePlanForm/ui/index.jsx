import React, { useEffect, useState } from 'react';
import './styles.scss';
import CreateInnerForm from './InnerForm';
import useAllDays from '../model/hooks/useAllDays';
import useEndDate from '../model/hooks/useEndDate';
import useFormikPlan from '../model/hooks/useFormikPlan';
import { connect } from 'react-redux';
import { readingFormValues } from '~store/createPlan/selectors';
import { plans, userPlans, planDetail } from '../../../../dist/selectors/ReadingPlan';
import { userData } from '../../../../dist/selectors/User';
import { mentorGroup, userGroup } from '~store/groups/selectors';
import { useParams } from 'react-router-dom';
import Preloader from '~components/Preloader/Preloader';
import useGetPlanData from '../model/hooks/useGetPlanData';
import useSetInitialFields from '../model/hooks/useSetInitialFields';
import { getUserGroup } from '~store/groups/actions';
import { getUserReadingPlan } from '../../../../dist/actions/ReadingPlan';

function CreatePlanForm({
  reduxValuesForm,
  step,
  onNext,
  getUserGroup,
  userGroup,
  onPrev,
  allPlans,
  getUserReadingPlan,
  isReady,
  myPlans,
  isReadyGroup,
  planDetail,
  mentorGroup,
  user,
}) {
  const { plan_id } = useParams();
  useEffect(() => {
    getUserGroup();
  }, []);

  const planShort = allPlans.length ? allPlans.find(el => el.id === Number(plan_id)) : null;
  const [loading, setLoading] = useState(false);
  const { values, setFieldValue, handleSubmit, submitCount, errors } = useFormikPlan(
    reduxValuesForm,
    plan_id,
    userGroup,
  );
  useGetPlanData(plan_id, setLoading);
  useEffect(() => {
    getUserReadingPlan();
  }, []);

  useSetInitialFields(setFieldValue, planShort, planDetail, user, mentorGroup, myPlans, plan_id);

  const depsEndDate = [
    values.start_date,
    values.week_days,
    values.all_chapters,
    values.new_books,
    values.old_books,
  ];

  const allDays = useAllDays(values, depsEndDate);
  const endDate = useEndDate(values.start_date, allDays);

  useEffect(() => {
    setFieldValue('end_date', endDate);
  }, [endDate]);

  useEffect(() => {
    setFieldValue('all_chapters', Number(values.old_chapters + values.new_chapters));
  }, [values.old_chapters, values.new_chapters]);

  return loading ? (
    <Preloader />
  ) : (
    <CreateInnerForm
      mentorGroup={mentorGroup}
      handleSubmit={handleSubmit}
      isReady={isReady}
      isReadyGroup={isReadyGroup}
      startDate={mentorGroup?.start_date}
      submitCount={submitCount}
      onNext={onNext}
      setFieldValue={setFieldValue}
      onPrev={onPrev}
      planDetail={planDetail}
      values={values}
      step={step}
      isDefaultPlan={false}
      errors={errors}
    />
  );
}
const mapStateToProps = state => ({
  reduxValuesForm: readingFormValues(state),
  allPlans: plans(state),
  mentorGroup: mentorGroup(state),
  userGroup: userGroup(state),
  user: userData(state),
  planDetail: planDetail(state),
  myPlans: userPlans(state),
});

const mapDispatchToProps = {
  getUserGroup,
  getUserReadingPlan,
};

export default connect(mapStateToProps, mapDispatchToProps)(CreatePlanForm);
