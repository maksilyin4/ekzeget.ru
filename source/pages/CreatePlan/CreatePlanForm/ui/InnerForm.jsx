import React, { useEffect } from 'react';
import CustomButtonCP from '~components/UICreatePlan/CustomButtonCp';
import FirstStep from '../model/FirstStep';
import SecondStep from '../model/SecondStep';
import { useMedia } from 'react-use';
import { connect, useDispatch } from 'react-redux';
import { booksReducer } from '../../../../dist/selectors/Bible';
import { getBooks } from '~dist/actions/Bible';
import { useLocation } from 'react-router-dom';

function CreateInnerForm({
  submitCount,
  errors,
  step,
  onNext,
  onPrev,
  books,
  planDetail,
  isGroup,
  mentorGroup,
  startDate,
  setFieldValue,
  isDefaultPlan,
  isReady,
  isReadyGroup,
  values,
  handleSubmit,
}) {
  const isMobile = useMedia('(max-width: 768px)');
  const dispatch = useDispatch();
  const location = useLocation();

  useEffect(() => {
    if (!books?.testament['testament-1']) {
      dispatch(getBooks());
    }
  }, []);
  const isEdit = location.pathname.includes('edit');

  return (
    <div className="cp__wrapper">
      {(!isMobile || step === 1) && !isGroup && (
        <FirstStep setFieldValue={setFieldValue} values={values} />
      )}
      {(!isMobile || step === 2 || isGroup) && books && (
        <SecondStep
          mentorGroup={mentorGroup}
          isEdit={isEdit}
          isReady={isReady}
          isReadyGroup={isReadyGroup}
          startDate={startDate}
          planDetail={planDetail}
          isGroup={isGroup}
          isOtherPlan={isDefaultPlan}
          setFieldValue={setFieldValue}
          values={values}
          books={books.testament}
        />
      )}
      {submitCount > 0 && errors && <span className="cp__error">{Object.values(errors)[0]}</span>}
      <div className="cp__buttons-send">
        {!isMobile ? (
          <>
            <CustomButtonCP
              className={'cp__buttons-btn'}
              size="s"
              to={'/reading-plan'}
              color="gray">
              Назад
            </CustomButtonCP>
            <CustomButtonCP
              type={'submit'}
              onClick={e => {
                e.preventDefault();
                handleSubmit();
              }}
              className={'cp__buttons-btn'}
              size="s"
              color="blue">
              {isEdit ? 'Редактировать план' : 'Создать план'}
            </CustomButtonCP>
          </>
        ) : (
          <>
            {step === 2 ? (
              <CustomButtonCP
                className={'cp__buttons-btn'}
                size="s"
                type={'submit'}
                onClick={e => {
                  e.preventDefault();

                  handleSubmit();
                }}
                color="blue">
                {isEdit ? 'Редактировать план' : 'Создать план'}
              </CustomButtonCP>
            ) : (
              <CustomButtonCP className={'cp__buttons-btn'} size="s" onClick={onNext} color="blue">
                Далее
              </CustomButtonCP>
            )}
            {step === 2 ? (
              <CustomButtonCP className={'cp__buttons-btn'} size="s" onClick={onPrev} color="gray">
                Назад
              </CustomButtonCP>
            ) : (
              <CustomButtonCP
                className={'cp__buttons-btn'}
                size="s"
                to={'/reading-plan'}
                color="gray">
                Назад
              </CustomButtonCP>
            )}
          </>
        )}
      </div>
    </div>
  );
}

const mapStateToProps = state => ({
  books: booksReducer(state),
});

const mapActionsToProps = {};
export default connect(mapStateToProps, mapActionsToProps)(CreateInnerForm);
