import React, { useState } from 'react';
import CreatePlanForm from '../CreatePlanForm/ui';
import BreadHead from '~components/BreadHead';
import { useMedia } from 'react-use';
import ReadingPageTitle from '~components/PageTitle';

const CreatePlanIndex = ({ isEdit, isReady }) => {
  const isMobile = useMedia('(max-width: 768px)');

  const TITLE_READING_PLAN = isEdit
    ? 'Редактирование плана чтений Библии'
    : 'Создание плана чтений Библии';

  const bread = [
    { path: '/', title: 'Главная' },
    { path: '/reading-plan', title: 'План чтений' },
    { path: '', title: isEdit ? 'Редактирование плана чтений' : 'Создание плана чтений' },
  ];
  const breadWithoutR = [
    { path: '/', title: 'Главная' },
    { path: '', title: isEdit ? 'Редактирование плана чтений' : 'Создание плана чтений' },
  ];
  const [step, setStep] = useState(1);
  const mobileTitle = isEdit => {
    if (isEdit) {
      return `Редактирование плана: шаг ${step} из 2`;
    }
    return `Создание плана: шаг ${step}\u00A0из\u00A02`;
  };

  return (
    <div className="page">
      <div className="wrapper">
        <BreadHead bread={isMobile ? breadWithoutR : bread} isDisabledLearnVideo />
        <ReadingPageTitle>{isMobile ? mobileTitle(isEdit) : TITLE_READING_PLAN}</ReadingPageTitle>
        <CreatePlanForm
          step={step}
          isReady={isReady}
          onNext={() => setStep(s => s + 1)}
          onPrev={() => setStep(s => s - 1)}
          isEdit={isEdit}
        />
      </div>
    </div>
  );
};

export default CreatePlanIndex;
