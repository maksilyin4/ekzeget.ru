import React from 'react';
import Loadable from 'react-loadable';
import Preloader from '../../../components/Preloader/Preloader';

const LoadableBar = Loadable({
  loader: () => import('./CreateGroup'),
  loading() {
    return (
      <div>
        <Preloader />
      </div>
    );
  },
  delay: 300,
});

export default LoadableBar;
