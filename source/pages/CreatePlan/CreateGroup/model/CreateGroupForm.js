import React, { useEffect, useMemo, useState } from 'react';
import useGetPlanData from '~pages/CreatePlan/CreatePlanForm/model/hooks/useGetPlanData';
import useFormikGroup from './useFormikGroup';
import useEndDate from '~pages/CreatePlan/CreatePlanForm/model/hooks/useEndDate';
import { useParams } from 'react-router-dom';
import { planDetail, plans, userPlans } from '~dist/selectors/ReadingPlan';
import { mentorGroup } from '~store/groups/selectors';
import { getMentorGroup } from '~store/groups/actions';
import { userData } from '~dist/selectors/User';
import Preloader from '~components/Preloader/Preloader';
import CreateInnerForm from '../../CreatePlanForm/ui/InnerForm';
import { connect } from 'react-redux';
import useSetInitialFields from '../../CreatePlanForm/model/hooks/useSetInitialFields';
import { getUserReadingPlan } from '../../../../dist/actions/ReadingPlan';

function calculateReadingDays(totalDays, readingDaysPerWeek) {
  if (readingDaysPerWeek === 7) {
    return totalDays;
  }
  const lagDaysPerWeek = 7 - readingDaysPerWeek;
  const weeksNeeded = Math.ceil(totalDays / readingDaysPerWeek);
  const totalLagDays = weeksNeeded * lagDaysPerWeek;
  const adjustedTotalDays = totalDays + totalLagDays;
  return adjustedTotalDays;
}
function CreatePlanForm({
  step,
  onNext,
  onPrev,
  allPlans,
  planDetail,
  mentorGroup,
  getMentorGroup,
  userPlans,
  isEdit,
  getUserReadingPlan,
  user,
}) {
  const { plan_id } = useParams();
  const planShort = allPlans.length ? allPlans.find(el => el.id === Number(plan_id)) : null;
  const isUserPlan = planShort ? planShort.creator_id === user?.user?.id : false;
  // TODO:  получить данные о книг (ветхий или новый + получить данные о паралельных ветках)
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (!mentorGroup?.id) {
      getMentorGroup();
    }
  }, []);

  useEffect(() => {
    getUserReadingPlan();
  }, []);

  const { values, setFieldValue, handleSubmit, submitCount, errors } = useFormikGroup(
    Number(plan_id),
    setLoading,
    isEdit,
  );

  useSetInitialFields(setFieldValue, planShort, planDetail, user, mentorGroup, userPlans);

  useGetPlanData(plan_id, setLoading, isEdit);
  const lastDay = Object.keys(planDetail)?.at(-1);

  const allDays = useMemo(() => {
    if (values.week_days.length) {
      return calculateReadingDays(Number(lastDay), values.week_days.length);
    }
  }, [lastDay, values.week_days]);

  const endDate = useEndDate(values.start_date, allDays);

  useEffect(() => {
    if (planDetail) {
      setFieldValue('all_chapters', planDetail['1']?.length);
    }
  }, [planDetail]);

  useEffect(() => {
    setFieldValue('end_date', endDate);
  }, [endDate]);

  return loading ? (
    <Preloader />
  ) : (
    <CreateInnerForm
      handleSubmit={handleSubmit}
      startDate={isEdit ? mentorGroup?.start_date : undefined}
      submitCount={submitCount}
      onNext={onNext}
      isCreate
      isGroup
      setFieldValue={setFieldValue}
      onPrev={onPrev}
      values={values}
      step={step}
      isDefaultPlan={!isUserPlan}
      errors={errors}
    />
  );
}
const mapStateToProps = state => ({
  allPlans: plans(state),
  mentorGroup: mentorGroup(state),
  user: userData(state),
  planDetail: planDetail(state),
  userPlans: userPlans(state),
});

const mapDispatchToProps = {
  getMentorGroup,
  getUserReadingPlan,
};

export default connect(mapStateToProps, mapDispatchToProps)(CreatePlanForm);
