export const parseDateFrom = (date, inverse) => {
  if (inverse) {
    return date
      ?.split('.')
      .reverse()
      .join('-');
  }
  return date
    ?.split('-')
    .reverse()
    .join('.');
};
