import { useFormik } from 'formik';
import { object, string, array } from 'yup';
import { useHistory } from 'react-router-dom';
import { createGroup } from '~dist/api/readingPlan/createGroup';
import { includeDay } from '~pages/PreviewPlan/PreviewPlan';
import { parseDateFrom } from './parseDate';

export default function useFormikGroup(planId, setLoading, isEdit) {
  const history = useHistory();

  const initialValues = {
    all_chapters: '',
    week_days: [],
    start_date: '',
    end_date: '',
  };

  const validationSchema = object({
    week_days: array()
      .required('Выберите дни недели')
      .min(1, 'Выберите дни недели'),
    start_date: string()
      .required('Введите дату начала')
      .min(10, 'Введите дату начала'),
    end_date: string(),
  });

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: async values => {
      await createGroup(
        {
          plan_id: planId,
          start_date: parseDateFrom(values.start_date, true),
          stop_date: parseDateFrom(values.end_date, true),
          check_date: parseDateFrom(values.end_date, true),
          schedule: {
            Mon: includeDay(values, 'пн'),
            Tue: includeDay(values, 'вт'),
            Wed: includeDay(values, 'ср'),
            Thu: includeDay(values, 'чт'),
            Fri: includeDay(values, 'пт'),
            Sat: includeDay(values, 'сб'),
            Sun: includeDay(values, 'вс'),
          },
        },
        isEdit,
      );
      setLoading(false);
      history.push(`/reading-plan/group/`);
    },
  });

  return formik;
}
