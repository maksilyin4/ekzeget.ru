import React, { useState } from 'react';
import BreadHead from '~components/BreadHead';
import { useMedia } from 'react-use';
import ReadingPageTitle from '~components/PageTitle';
import CreateGroupForm from './model/CreateGroupForm';

const CreatePlanIndex = ({ isEdit }) => {
  const isMobile = useMedia('(max-width: 768px)');

  const TITLE_READING_PLAN = isEdit ? 'Редактирование группы' : 'Создание группы';

  const bread = [
    { path: '/', title: 'Главная' },
    { path: '/reading-plan', title: 'План чтений' },
    { path: '', title: isEdit ? 'Редактирование группы' : 'Создание группы' },
  ];
  const breadWithoutR = [
    { path: '/', title: 'Главная' },
    { path: '', title: isEdit ? 'Редактирование группы' : 'Создание группы' },
  ];

  const [step, setStep] = useState(1);
  const mobileTitle = isEdit => {
    if (isEdit) {
      return `Редактирование группы: шаг ${step} из 2`;
    }
    return `Создание группы: шаг ${step}\u00A0из\u00A02`;
  };

  return (
    <div className="page">
      <div className="wrapper">
        <BreadHead bread={isMobile ? breadWithoutR : bread} isDisabledLearnVideo />
        <ReadingPageTitle>{isMobile ? mobileTitle(isEdit) : TITLE_READING_PLAN}</ReadingPageTitle>
        <CreateGroupForm
          step={step}
          onNext={() => setStep(s => s + 1)}
          onPrev={() => setStep(s => s - 1)}
          isEdit={isEdit}
        />
      </div>
    </div>
  );
};

export default CreatePlanIndex;
