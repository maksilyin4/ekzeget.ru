import React, { Component } from 'react';
import Breadcrumbs from '../../components/Breadcrumbs/Breadcrumbs';
import { AXIOS } from '../../dist/ApiConfig';
import Preloader from '../../components/Preloader/Preloader';
import './styles.scss';
import { smoothScrollTo } from '../../dist/utils';

export default class AddInterpretationRules extends Component {
  state = {
    isFetching: false,
    data: '',
  };

  componentDidMount() {
    smoothScrollTo();
    this.setState({
      isFetching: true,
    });
    AXIOS.get('article/detail/interpretation_rules').then(
      ({ data: { article }, status }) => {
        if (status === 200) {
          this.setState({
            isFetching: false,
            data: article.body,
          });
        } else {
          this.setState({
            isFetching: false,
          });
        }
      },
      () => {
        this.setState({
          isFetching: false,
        });
      },
    );
  }

  _breadItems = () => [
    { path: '/', title: 'Главная' },
    { path: '', title: 'Правила добавления толкований' },
  ];

  render() {
    const { isFetching, data } = this.state;
    return (
      <div className="page aboutUs">
        <div className="wrapper">
          <Breadcrumbs data={this._breadItems()} />
          <div className="page__head page__head_two-col">
            <h1 className="page__title">Правила добавления толкований</h1>
          </div>
        </div>
        <div className="page__body aboutUs__body">
          {isFetching ? (
            <Preloader />
          ) : (
            <div className="wrapper wrapper_two-col">
              <div className="interpretationRules" dangerouslySetInnerHTML={{ __html: data }} />
            </div>
          )}
        </div>
      </div>
    );
  }
}
