import { AXIOS } from '../../../dist/ApiConfig';
import { LS } from '../../../dist/browserUtils';

export const getPreviewPlan = async plan => {
  const data = AXIOS.post('/reading/plan-detail/preview/', plan, {
    headers: {
      'X-Authentication-Token': LS.get('token'),
    },
  });
  return data;
};
