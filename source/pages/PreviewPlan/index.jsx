import Loadable from 'react-loadable';

const LoadableBar = Loadable({
  loader: () => import('./PreviewPlan'),
  loading() {
    return null;
  },
  delay: 300,
});

export default LoadableBar;
