import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet/es/Helmet';
import BreadHead from '../../components/BreadHead';
import ReadingPlanTable from '~components/ReadingPlanTable/ReadingPlanTable';
import './styles.scss';
import readingPlanActions from '../../dist/actions/ReadingPlan';
import CustomButtonCP from '~components/UICreatePlan/CustomButtonCp';
import ReadingPageTitle from '~components/PageTitle';
import { useMedia } from 'react-use';
import { connect, useDispatch } from 'react-redux';
import {
  previewPlan,
  previewPlanLoading,
  readingFormValues,
  valuesToSendPlan,
} from '../../store/createPlan/selectors';
import { createPlanForm } from '~pages/CreatePlan/CreatePlanForm/model/api/createPlan';
import {
  getPreviewPlanThunk,
  resetFormValues,
  setFormValues,
  setFormValuesToSend,
} from '../../store/createPlan/actions';
import { useParams, useHistory } from 'react-router-dom';
import { planDetail } from '~dist/selectors/ReadingPlan';
import { useLocation } from 'react-router-dom/cjs/react-router-dom.min';
import { getDaysByObj } from '../../components/Popups/ReadingPlanDetail/plan-days/getDaysByObj';
import { userGroup } from '~store/groups/selectors';
import { getUserGroup } from '../../store/groups/actions';

const TITLE_READING_PLAN = 'Предварительный просмотр';

export const includeDay = (values, str) => {
  if (values.week_days.map(s => s.toLowerCase()).includes(str.toLowerCase())) {
    return 1;
  }
  return 0;
};

export const createDate = str =>
  str
    .split('.')
    .reverse()
    .join('-');

const PreviewPlan = ({
  previewPlan,
  sendValues,
  formValues,
  previewPlanLoading,
  planDetail,
  userGroup,
  // getDetail,
  getPreviewPlanThunk,
}) => {
  const { plan_id } = useParams();
  const dispatch = useDispatch();
  const bread = [
    { path: '/', title: 'Главная' },
    { path: '/reading-plan', title: 'План чтений' },
    { path: '', title: 'Создание плана чтений' },
  ];

  useEffect(() => {
    if (plan_id) {
      dispatch(readingPlanActions.getReadingPlanDetail(plan_id));
    }
  }, [plan_id]);

  const location = useLocation();

  const isMobile = useMedia('(max-width: 768px)');
  const breadMobile = [
    { path: '/', title: 'Главная' },
    { path: '', title: 'Создание плана чтений' },
  ];

  const group = userGroup?.plan_id === Number(plan_id) ? userGroup : undefined;

  const history = useHistory();
  const createPlanHandler = async () => {
    try {
      await createPlanForm(sendValues);
      dispatch(resetFormValues());
      history.push('/reading-plan');
    } catch (e) {
      console.log(e);
    }
  };

  const isEdit = location && location?.pathname.split('/').length === 5;

  useEffect(() => {
    if (!plan_id && !isEdit) {
      if (sendValues.lenght) {
        getPreviewPlanThunk(sendValues);
      } else {
        history.push('/reading-plan/create-plan');
      }
    }
  }, [sendValues, isEdit]);

  useEffect(() => {
    if (!userGroup.id) {
      dispatch(getUserGroup());
    }
  }, []);

  return (
    <div>
      <Helmet title={TITLE_READING_PLAN} description={''} keywords={''} />
      <div className="page">
        <div className="wrapper wrapper-main">
          <BreadHead bread={isMobile ? breadMobile : bread} isDisabledLearnVideo />
          <ReadingPageTitle withMargin>{TITLE_READING_PLAN}</ReadingPageTitle>
          <div className="preview__plan-container">
            {!previewPlanLoading &&
              (!plan_id && !isEdit
                ? formValues && previewPlan.plan
                : Object.keys(planDetail).length > 0) &&
              planDetail && (
                <ReadingPlanTable
                  head={
                    group
                      ? {
                          title: '',
                          chapters: formValues.all_chapters || planDetail[1]?.length,
                          startDate: createDate(formValues.start_date) || group.start_date,
                          endDate: createDate(formValues.end_date) || group.stop_date,
                          weekDays: formValues.week_days?.length
                            ? formValues.week_days
                            : getDaysByObj(group.schedule).days,
                          planLength:
                            sendValues.plan.planLength || Object.values(planDetail).length,
                          parallel:
                            Number(formValues.parallel_old_chapters) +
                              Number(formValues.parallel_new_chapters) ||
                            getDaysByObj(group.schedule).chaptersPerDay,
                        }
                      : formValues.end_date
                      ? {
                          title: formValues.name,
                          chapters: formValues.all_chapters,
                          startDate: createDate(formValues.start_date),
                          endDate: createDate(formValues.end_date),
                          weekDays: formValues.week_days,
                          planLength: sendValues.plan.planLength,
                          parallel:
                            Number(formValues.parallel_old_chapters) +
                            Number(formValues.parallel_new_chapters),
                        }
                      : undefined
                  }
                  plan={plan_id ? planDetail : previewPlan.plan}
                />
              )}
          </div>
          <div className="preview__buttons">
            <CustomButtonCP
              size="s"
              className={'cp__buttons-btn'}
              onClick={() => {
                resetFormValues();
                history.push(
                  plan_id ? `/reading-plan/edit-plan/${plan_id}` : '/reading-plan/edit-plan',
                );
              }}
              color="gray">
              Редактировать план
            </CustomButtonCP>
            <CustomButtonCP
              size="s"
              className={'cp__buttons-btn'}
              onClick={() => {
                if (plan_id) {
                  history.push('/reading-plan');
                } else {
                  createPlanHandler();
                }
              }}
              color="blue">
              {plan_id ? 'Подтвердить' : 'Создать план'}
            </CustomButtonCP>
          </div>
        </div>
      </div>
    </div>
  );
};

const selectorProps = state => ({
  previewPlan: previewPlan(state),
  previewPlanLoading: previewPlanLoading(state),
  sendValues: valuesToSendPlan(state),
  userGroup: userGroup(state),
  planDetail: planDetail(state),
  formValues: readingFormValues(state),
});

const actionProps = dispatch => ({
  getDetail: readingPlanActions.getReadingPlanDetail,
  getUserGroup,
  setFormValuesToSend,
  setFormValues,
  getPreviewPlanThunk: values => dispatch(getPreviewPlanThunk(values)),
});

export default connect(selectorProps, actionProps)(PreviewPlan);
