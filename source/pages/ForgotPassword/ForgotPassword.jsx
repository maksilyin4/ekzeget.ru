import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import cx from 'classnames';
import Breadcrumbs from '../../components/Breadcrumbs/Breadcrumbs';
import Form, { FormGroup, FormSubmit } from '../../components/Form/Form';
import TextField from '../../components/TextField/TextField';
import { AXIOS } from '../../dist/ApiConfig';

import './forgotPassword.scss';

class ForgotPassword extends Component {
  constructor(props) {
    super(props);

    this.state = {
      password: '',
      confirm_password: '',
      secret: props.location.pathname.split('/')[2],
      success: false,
      message: '',
    };
  }

  _handleChange = field => ({ target: { value } }) => {
    this.setState({
      [field]: value,
      message: '',
    });
  };

  _submit = e => {
    e.preventDefault();
    const { password, confirm_password, secret } = this.state;
    if (!password || !/^([a-zA-Z0-9]{6,})$/.test(password)) {
      this.setState({
        message:
          'Пароль должен состоять не менее чем из 6 символов и состоять только из латинских букв и цифр',
      });
    } else if (password !== confirm_password) {
      this.setState({
        message: 'Введённые пароли не совпадают',
      });
    } else {
      const form = new FormData();
      form.append('password', password);
      form.append('secret', secret);

      AXIOS.post('/user-profile/reset-password/', form).then(
        ({ data }) => {
          if (data.status === 'ok') {
            this.setState({
              success: true,
              message:
                'Пароль успешно установлен. Вы будуте перенаправлены на главную страницу для авторизации.',
            });
            setTimeout(() => {
              this.props.history.push('/?auth-need');
            }, 1500);
          } else {
            this.setState({
              message: data.error.message,
            });
          }
        },
        error => {
          this.setState({
            message: error.name,
          });
        },
      );
    }
  };

  _breadItems = () => [
    { path: '/', title: 'Главная' },
    { path: '', title: 'Восстановление пароля' },
  ];

  render() {
    const { success } = this.state;
    return (
      <div className="page">
        <div className="wrapper">
          <Breadcrumbs data={this._breadItems()} />
          <div className="page__head">
            <h1 className="page__title">Восстановление пароля</h1>
          </div>
        </div>

        <div className="page__body">
          <Form
            action=""
            name="forgot_form"
            className="forgot-password__form paper"
            onSubmit={this._submit}>
            <FormGroup>
              <TextField
                type="password"
                name="email"
                label="Введите новый пароль"
                required
                onChange={this._handleChange('password')}
              />
            </FormGroup>
            <FormGroup>
              <TextField
                type="password"
                name="email"
                label="Подтвердите пароль"
                required
                onChange={this._handleChange('confirm_password')}
              />
            </FormGroup>
            {this.state.message !== '' && (
              <div
                className={cx('form__notice', {
                  form__notice_success: success,
                  form__notice_error: !success,
                })}>
                {this.state.message}
              </div>
            )}
            <FormSubmit title="Восстановить" align="center" />
          </Form>
        </div>
      </div>
    );
  }
}

export default withRouter(ForgotPassword);
