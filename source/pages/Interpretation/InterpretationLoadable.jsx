import React from 'react';
import Loadable from 'react-loadable';
import Preloader from '../../components/Preloader/Preloader';

const LoadableBar = Loadable({
  loader: () => import('./InterpretationIndex'),
  loading() {
    return (
      <div className="preloader-relative">
        <Preloader />
      </div>
    );
  },
  render(loaded, props) {
    const Component = loaded.default;
    return <Component directProps={props.directProps} {...props} />;
  },
  delay: 300,
});

export default LoadableBar;
