import R from 'ramda';
import React, { Component } from 'react';
import cx from 'classnames';
import { withCookies } from 'react-cookie';
// css
import './styles.scss';
import '../Bible/bible.scss';
import '../Bible/Chapter/chapter.scss';

// components
import Wrapper from './Wrapper';
import InterpretationHead from './InterpretationHead';
import ToggleLayout from '~components/ToggleLayout';
import BibleActionPanel from '~components/BibleActionPanel/';

import BibleHeader from '../Bible/BibleHeader';
import BibleMenuBooksLeft from '~components/Bible/BibleMenuBooksLeft';
import Helmet from '~components/Helmet';

// utils
import { smoothScrollTo } from '../../dist/utils';
import { getTitle, getMainTitle, breadItems } from './utilsInterp';
import { getIsShowContent } from '../Bible/utilsBible';
import { LS } from '~dist/browserUtils';
import { onMouseEnter, onMouseLeave, onMouseObj } from '~components/PreloadChunk';
import { setStyleHeight, defaultHeightContentBible } from '~utils/common';
import BibleMediaContent from '../../components/Bible/BibleMediaContentRight';

class Interpretation extends Component {
  refContent = React.createRef();

  state = {
    layout: {
      content: false,
      left: false,
      right: false,
    },
  };

  componentDidMount() {
    const { screen, cookies } = this.props;

    smoothScrollTo(800);

    this.setState({
      isAdded: {},
      layout: {
        content: JSON.parse(LS.get('content')) || false,
        left: JSON.parse(LS.get('left')) || false,
        right: JSON.parse(LS.get('right')) || false,
      },
    });

    if (screen.phone) {
      cookies.set('heightLeftMenuBible', 0);
    }
    if (screen.tablet) {
      this.setHeightContentTablet();
    }
  }

  componentDidUpdate(prevProps) {
    const { cookies, bookInfo, screen } = this.props;

    if (prevProps.bookInfo.title !== bookInfo.title) {
      const { title, menu } = bookInfo;
      const bookTitle = cookies.get('bookTitle')?.title ?? '';
      if (title && bookTitle !== title) {
        cookies.set('bookTitle', { title, menu }, { path: '/' });
      }
    }
    if (prevProps.screen.tablet !== screen.tablet && screen.tablet) {
      this.setHeightContentTablet();
    }

    if (prevProps.screen.phone !== screen.phone && screen.phone) {
      cookies.set('heightLeftMenuBible', 0);
    }
  }

  setHeightContentTablet = () => {
    const contentHeight = this.refContent.current?.clientHeight;

    const currentHeight =
      contentHeight > defaultHeightContentBible ? defaultHeightContentBible : contentHeight;

    this.props.cookies.set('heightLeftMenuBible', currentHeight);
  };

  toggleLayout = layoutType => {
    const {
      layout: { [layoutType]: nowLayout, ...lastLayout },
    } = this.state;

    const isLastLayout = !Object.values(lastLayout).every(el => el);

    if (nowLayout || isLastLayout) {
      LS.set([layoutType], !nowLayout);
      this.setState({ ...this.state, layout: { ...this.state.layout, [layoutType]: !nowLayout } });
    }
  };

  getParentInter = isAdded => this.setState({ isAdded });

  render() {
    const {
      isAdded,
      layout: { left, content, right },
    } = this.state;

    const {
      interpretations,
      interpretationsData,
      ekzeget,
      verse: { verse: verseData },
      match: {
        params: {
          book_code: code,
          chapter_num: chapterParams,
          verse_id: verseParams,
          ekzeget: ekzegetParams,
        },
      },
      verseList,
      bookInfo,
      chapterTastement,
      screen,
      chapterLength,
      userLoggedIn,
      location,
      cookies,
      isUpdate,
      getUpdateInterpretation,
      isLoadingVerse,
      isLoadingMeta,
      metaData,
    } = this.props;

    const isContent = !R.isEmpty(interpretationsData);
    const verseCurrent = verseList.find(v => +v.number === +verseParams);

    const bookTitle = verseData?.book?.title ?? cookies.get('bookTitle')?.title;

    const bookMenuKey = screen.desktop ? 'title' : 'menu';
    const bookMenu = verseData?.book?.[bookMenuKey] ?? cookies.get('bookTitle')?.[bookMenuKey];
    const heightLeftMenuBible = cookies.get('heightLeftMenuBible');

    const chapterNum = chapterParams?.replace('glava-', '') ?? '';
    const mainTitle = getMainTitle({ bookInfo, chapterNum, verseParams, ekzeget, ekzegetParams });
    const isShowContent = getIsShowContent(cookies, screen.phone);

    const helmetTitle = getTitle({
      ekzeget,
      number: verseCurrent?.number,
      bookInfo,
      chapter: chapterNum,
      ekzegetParams,
    });

    if (!verseData) {
      return null;
    }

    return (
      <div className="page interpretation verse-page bible-page">
        <>
          <Helmet title={metaData?.title || helmetTitle} />

          {verseParams && (
            <>
              <BibleHeader
                isLoad={isLoadingMeta}
                title={metaData?.h_one || mainTitle}
                breadcrumbs={breadItems({
                  book: bookInfo,
                  bookTitle,
                  onMouseLeave,
                  onMouseEnter,
                  onMouseObj,
                  chapterNum,
                  ekzeget,
                  ekzegetParams,
                  number: verseParams.replace('stih-', ''),
                })}
                isSubTitle={screen.desktop && isContent}
              />

              <div className="wrapper">
                <BibleActionPanel
                  pagesCount={Number(chapterLength)}
                  currentPage={Number(verseParams)}
                  bookCode={code}
                  bookTitle={bookMenu}
                  navText="Стих"
                />
                {!screen.phone && (
                  <ToggleLayout
                    toggleFunc={this.toggleLayout}
                    screen={screen}
                    left={left}
                    content={content}
                    right={right}
                    urlBack={`/bible/${code}/${chapterParams}/`}
                    urlText="Вернуться к главе"
                  />
                )}

                <div className="inner-page__body">
                  <div className="wrapper_multi-col">
                    {(screen.desktop || screen.tabletMedium) && (
                      <BibleMenuBooksLeft
                        left={left}
                        isOtherBlock={content && right}
                        activeBook={code}
                        activeChapter={chapterParams}
                        activeTestament={chapterTastement}
                        isDesktop={screen.desktop}
                      />
                    )}
                    {isShowContent && (
                      <div
                        ref={this.refContent}
                        style={setStyleHeight({
                          height: heightLeftMenuBible,
                          styleName: 'minHeight',
                        })}
                        className={cx('content interpretation_detail', {
                          collapse: content,
                          'bible-not-left': left,
                        })}>
                        <InterpretationHead
                          getParentInter={this.getParentInter}
                          ekzeget={ekzeget}
                          translateEkzeget={ekzegetParams}
                          verseId={verseCurrent}
                          verseContent={verseData}
                          bookInfo={bookInfo}
                          interpretationsData={interpretationsData}
                          interpretations={interpretations}
                          getUpdateInterpretation={getUpdateInterpretation}
                          isUpdate={isUpdate}
                          params={this.props.match.params}
                          activeItem={ekzegetParams}
                          chapterNum={chapterNum}
                          isLoadingVerse={isLoadingVerse}
                          userLoggedIn={userLoggedIn}
                        />
                      </div>
                    )}
                    <BibleMediaContent
                      collapse={right}
                      extend={content}
                      chapter={verseData}
                      bookCode={code}
                      activeItem={ekzegetParams}
                      baseUrl="bible"
                      locationCustom={location}
                      screen={screen}
                      userLoggedIn={userLoggedIn}
                      editFavorite={this.editFavorite}
                      deleteFavorite={this.deleteFavorite}
                      extendLayout={{ content, left, right }}
                      isAddedEkzeget={isAdded}
                      verseId={verseData?.id}
                      verseCurrent={verseCurrent?.number}
                    />
                  </div>
                </div>
              </div>
            </>
          )}
        </>
      </div>
    );
  }
}

export default withCookies(Wrapper(Interpretation));
