import React from 'react';
import cx from 'classnames';

import AddBookmark from '~components/BibleBookmarks/AddBookmarks/AddBookmark';
import { GetBookmark } from '~components/BibleBookmarks/GetBookmarks/GetBookmark';
import BibleTabActionItem from '~components/Bible/BibleTabActionItem';
import BibleManuscriptAction from '~components/Bible/BibleManuscriptAction';
import ShareButton from '~components/ShareButton';

const InterpretationTabActionList = ({
  userLoggedIn = false,
  bookmark,
  short,
  isFavAdded,
  noteList = [],
  deleteInterpretationFavorite,
  addInterpretation,
  addFavorite,
  addNote,
  showManuscript,
}) => {
  return (
    <div className="bible-actions">
      <div className="tab__action-list">
        <BibleTabActionItem
          key="short"
          className="tab__action-item"
          title="Добавить толкование"
          onClick={deleteInterpretationFavorite}>
          {bookmark.hasOwnProperty(short) && userLoggedIn ? (
            <GetBookmark short={short} />
          ) : (
            <AddBookmark short={short} isVerse />
          )}
        </BibleTabActionItem>
        <BibleTabActionItem
          key="plus"
          className="tab__action-item"
          title="Добавить толкование"
          onClick={addInterpretation}
          icon="plus"
        />
        <BibleTabActionItem
          key="favorites"
          className={cx('tab__action-item', {
            'note-added': isFavAdded,
          })}
          title={isFavAdded ? 'Добавлено в избранное' : 'Добавить в избранное'}
          onClick={() => addFavorite(isFavAdded)}
          icon="favorites"
        />
        <BibleTabActionItem
          key="note"
          className={cx('tab__action-item', {
            'note-added': noteList.some(i => i.short === short),
          })}
          title="Добавить заметку на стих"
          onClick={addNote}
          icon="note"
        />

        <BibleManuscriptAction
          classNameIcon="tab-action-icon"
          className="tab__action-item"
          key="manuscript"
          onClick={showManuscript}
          title="Показать манускрипт"
          icon="manuscript"
          type="manuscript"
        />
      </div>
      <div className="chapter__verses-action">
        <ShareButton />
      </div>
    </div>
  );
};

export default InterpretationTabActionList;
