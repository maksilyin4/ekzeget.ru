import R from 'ramda';
import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import qs from 'qs';

// store
import { getVerse as getActionVerse } from '../../dist/actions/Verse';
import { getChapter } from '../../dist/actions/Chapter';
import { getExeget, getExegetClear } from '../../dist/actions/Exeget';

import { getFavoriteVerse, getNotes, getFavoriteInter } from '../../dist/actions/favorites';
import getInterpretationByUrl, {
  clearInterpretationByUrl,
} from '../../dist/actions/Interpretation';

import { getInterpretation } from '../../store/Interpretation/selector';
import {
  getChapter as getChapterSelector,
  getTestament,
  verseListFromChapter,
  getBookInfo,
  getChapterIsLoading,
} from '../../dist/selectors/Chapter';
import { getInterpretators } from '../../dist/selectors/ChapterInterpretators';
import { booksReducer } from '../../dist/selectors/Bible';
import { ekzegetNameId, getErrorExeget } from '../../dist/selectors/Exeget';
import { getVerse, getErrorVerse, getVerseIsLoading } from '~dist/selectors/Verse';
import { userLoggedIn } from '~dist/selectors/User';
import getScreen from '~dist/selectors/screen';
import { chapterVerseLength } from '~dist/selectors/Chapter';

import NotFound from '../NotFound/NotFound';
import { setIsLoadingMeta, setMetaData } from '~store/metaData/selector';
import { getMetaData } from '~store/metaData/actions';

function Wrapper(WrappedComponent) {
  return class GetInterpretation extends Component {
    constructor(props) {
      super(props);
      // For SSR

      const searchedText = R.pathOr(
        '',
        ['search'],
        qs.parse(this.props.location.search.replace('?', '')),
      );

      this.state = {
        searchedText,
        isUpdate: false,
      };
    }

    async componentDidMount() {
      const {
        getFavoriteVerse,
        getNotes,
        getActionVerse,
        match,
        chapter,
        interpretationsData = [],
        getMetaData,
        location: { pathname },
      } = this.props;

      if (!(this.props.errorVerse?.message || this.props.errorExeget?.message)) {
        await getMetaData(pathname);

        getActionVerse(match.params, pathname);

        Object.keys(chapter).length === 0 && this.fetchGetChapter();

        this.fetchGetExeget();

        if (interpretationsData.length === 0) {
          this.fetchInterpretationByUrl();
        }
        getFavoriteVerse();
        getNotes();
      }
    }

    async componentDidUpdate(prevProps) {
      const {
        match: { params },
        userLoggedIn,
        getFavoriteInter,
        errorExeget,
        location: { pathname },
        getMetaData,
      } = this.props;

      if (prevProps.errorExeget?.message !== errorExeget?.message) {
        if (!errorExeget?.message) {
          await this.fetchGetChapter();
        }
      }

      if (prevProps.location.pathname !== pathname) {
        await getMetaData(pathname);

        if (this.props.errorExeget?.message) {
          this.props.getExegetClear();
        }
      }

      if (
        (userLoggedIn !== prevProps.userLoggedIn && prevProps.userLoggedIn) ||
        prevProps.location.pathname !== this.props.location.pathname
      ) {
        this.props.getActionVerse(params, pathname);
      }

      if (prevProps.match.params.verse_id !== params.verse_id) {
        this.props.getActionVerse(params, pathname);
      }

      if (!R.equals(params, prevProps.match.params)) {
        this.fetchInterpretationByUrl();
      }
      if (
        params.book_code !== prevProps.match.params.book_code ||
        params.chapter_num !== prevProps.match.params.chapter_num
      ) {
        await this.fetchGetChapter();
      }
      if (params.ekzeget !== prevProps.match.params.ekzeget) {
        this.fetchGetExeget();
      }
      if (prevProps.match.params.verse_id !== params.verse_id) {
        this.fetchInterpretationByUrl();
      }
      if (prevProps.userLoggedIn !== userLoggedIn) {
        userLoggedIn && (await getFavoriteInter());
      }
    }

    componentWillUnmount = () => {
      this.props.clearInterpretationByUrl();
    };

    fetchGetChapter = () => {
      const {
        getChapter,
        location: { pathname },
        match: {
          params: { book_code, chapter_num },
        },
      } = this.props;

      getChapter(book_code, chapter_num, pathname);
    };

    fetchGetExeget = () => {
      const {
        getExeget,
        match: {
          params: { ekzeget },
        },
      } = this.props;
      if (ekzeget) {
        getExeget({ id: ekzeget });
      }
    };

    fetchInterpretationByUrl = async () => {
      const {
        match: { params },
        getInterpretationByUrl,
      } = this.props;

      await getInterpretationByUrl(params);
    };

    getUpdateInterpretation = () => this.setState({ isUpdate: !this.state.isUpdate });

    render() {
      if (this.props.errorVerse?.message || this.props.errorExeget?.message) {
        return <NotFound />;
      }

      return (
        <WrappedComponent
          {...this.props}
          {...this.state}
          getUpdateInterpretation={this.getUpdateInterpretation}
        />
      );
    }
  };
}

const mapStateToProps = store => ({
  books: booksReducer(store),
  chapterTastement: getTestament(store),
  verseList: verseListFromChapter(store),
  bookInfo: getBookInfo(store),
  ekzeget: ekzegetNameId(store),
  interpretationsData: getInterpretation(store),
  interpretations: getInterpretators(store),
  isLoadingVerse: getVerseIsLoading(store),
  verse: getVerse(store),
  errorVerse: getErrorVerse(store),
  isLoadingChapter: getChapterIsLoading(store),
  userLoggedIn: userLoggedIn(store),
  screen: getScreen(store),
  chapterLength: chapterVerseLength(store),
  chapter: getChapterSelector(store),
  errorExeget: getErrorExeget(store),
  metaData: setMetaData(store),
  isLoadingMeta: setIsLoadingMeta(store),
});

const mapDispatchToProps = {
  getChapter,
  getExeget,
  getFavoriteVerse,
  getFavoriteInter,
  getNotes,
  getInterpretationByUrl,
  clearInterpretationByUrl,
  getActionVerse,
  getExegetClear,
  getMetaData,
};

const composeHOC = compose(connect(mapStateToProps, mapDispatchToProps), Wrapper);
export default composeHOC;
