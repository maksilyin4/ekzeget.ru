import R from 'ramda';
import { AXIOS } from '../../dist/ApiConfig';

export default function getInterpretation(url, searchedText) {
  return AXIOS.get(url).then(
    ({ data }) => {
      if (data.status === 'ok') {
        const interpretationComment = R.pathOr('', ['interpretations', 0, 'comment'], data);
        const regexpSearch = new RegExp(searchedText, 'gi');

        return {
          isFetching: false,
          interpretation: searchedText.trim()
            ? interpretationComment.replace(
                regexpSearch,
                p => `<strong class="yellow__text">${p}</strong>`,
              )
            : interpretationComment,
          interpretationId: R.pathOr(0, ['interpretations', 0, 'id'], data),
          verse: R.pathOr(data, ['interpretations', 0, 'verse', 0], data),
          addedBy: R.pathOr(data, ['interpretations', 0, 'added_by', 'username'], ''),
          editedBy: R.pathOr(data, ['interpretations', 0, 'edited_by', 'username'], ''),
        };
      }
      return { isFetching: false };
    },
    () => ({ isFetching: false }),
  );
}
