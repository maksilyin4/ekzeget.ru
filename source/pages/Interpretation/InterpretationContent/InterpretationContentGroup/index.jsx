import React from 'react';
import { Link } from 'react-router-dom';

import './interContentGroup.scss';

const InterpretationContentGroup = ({ groupInterpretation, params, verse }) => {
  return (
    groupInterpretation && (
      <div className="interpretation_detail__content__group">
        <p>
          Толкование на группу стихов:
          <Link
            to={`/bible/${params.book_code}/${params.chapter_num}?verse=${params.verse_id}:${groupInterpretation[0].number}-${groupInterpretation[groupInterpretation.length - 1].number}/`}
            className="content__group-link">
            {` ${groupInterpretation[0].book.short_title}: ${verse?.chapter}: ${groupInterpretation[0].number}-${groupInterpretation[groupInterpretation.length - 1].number}`}
          </Link>
        </p>
      </div>
    )
  );
};

export default InterpretationContentGroup;
