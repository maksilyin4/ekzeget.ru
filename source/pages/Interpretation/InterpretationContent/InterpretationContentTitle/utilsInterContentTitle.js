export const getContent = ({ isAdded, addFavorite, deleteFavorite, ekzegetId }) => {
  let title = 'Добавить толкование в избранное';
  let func = addFavorite(ekzegetId);
  if (isAdded) {
    title = 'Удалить толкование из избранного';
    func = deleteFavorite(ekzegetId);
  }
  return [title, func];
};
