import React from 'react';
import { Link } from 'react-router-dom';
import cx from 'classnames';
import { useSelector } from 'react-redux';

import { favoritesInterpretationsSelector } from '../../../../dist/selectors/favorites';

import Icon from '~components/Icons/Icons';

import { getContent } from './utilsInterContentTitle';

const InterpretationContentTitle = React.memo(
  ({ userLoggedIn, ekzeget, deleteFavorite, addFavorite, ekzegetId }) => {
    const favoritesInterpretations = useSelector(favoritesInterpretationsSelector);

    const favoriteId = favoritesInterpretations[ekzegetId];
    const isAdded = favoriteId && favoriteId.id === ekzeget.id;

    const [title, onClick] = getContent({
      isAdded,
      addFavorite,
      deleteFavorite,
      ekzegetId,
    });

    return (
      <div className="interpretation_detail__content__leftSide-title">
        <div>
          <Link to={`/all-about-bible/ekzegets/${ekzeget.code}/`}>{ekzeget.name}</Link>
        </div>
        {userLoggedIn && (
          <div
            className={cx('tab__action-item', { 'note-added': isAdded })}
            title={title}
            onClick={onClick}>
            <Icon icon="favorites" />
          </div>
        )}
      </div>
    );
  },
);

export default InterpretationContentTitle;
