import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Redirect } from 'react-router-dom';

import { Popup } from '~dist/browserUtils';
import {
  addFavInterpToStore,
  getFavoriteInter,
  deleteFavoriteInter,
  getFavoriteVerse,
} from '~dist/actions/favorites';
import interpretationsActions from '~dist/actions/ChapterInterpretations';

import { getInterpretators as interpretatorsSelector } from '~dist/selectors/ChapterInterpretators';
import { getIsLoadingInter } from '~store/Interpretation/selector';
import { getCurrentTranslate } from '~dist/selectors/translates';
import { getInterList, favoriteInterListSelector } from '~dist/selectors/favorites';
import { userLoggedIn } from '~dist/selectors/User';

import Preloader from '~components/Preloader/Preloader';
import AddFavoritesInterp from '~components/Popups/AddFavoriteInterp';
import DeleteFavoriteInterp from '~components/Popups/DeleteFavoriteInterp';
import InterpretationContentHead from './InterpretationContentHead';
import InterpretationContentGroup from './InterpretationContentGroup';
import InterpretationContentTitle from './InterpretationContentTitle';
import InterpretationContentText from './InterpretationContentText';

import { getFavoriteListEkzeget } from '../utilsInterp';

class InterpretationContent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isAdded: {},
      locationState: null,
    };
  }

  componentDidMount() {
    const { location, getFavoriteInter, userLoggedIn } = this.props;

    if (userLoggedIn) {
      getFavoriteInter();
    }

    if (location.state) {
      this.setState({ locationState: location.state });
    }
  }

  componentDidUpdate = (prevProps, prevState) => {
    const { interpretations } = this.props;

    if (prevProps.interpretations !== interpretations) {
      const obj = {};
      interpretations.forEach(({ id, in_fav }) => {
        obj[id] = in_fav;
      });

      this.setState({ isAdded: obj });
      this.props.getParentInter(obj);
    }

    if (prevState.isAdded !== this.state.isAdded) {
      this.props.getParentInter(this.state.isAdded);
    }
  };

  getMarkerFromInterpretation = (locationState, interText) => {
    let result = interText;

    if (locationState !== null) {
      const text = `${locationState.bookCode}/${locationState.chapterNum?.replace('glava-', '')}/`;
      const interpretationArr = interText.split(text);

      if (interpretationArr.length > 1) {
        result = `${interpretationArr[0].slice(
          0,
          -15,
        )} verse-link-current${interpretationArr[0].slice(-15)}${text.slice(0, -1)}${
          interpretationArr[1]
        }`;
      }
    }
    return result;
  };

  addFavorite = ekzegetId => () => {
    const {
      verse: { short },
      addFavInterpToStore,
      getFavoriteInter,
      getUpdateInterpretation,
      interpretationsData,
      isUpdate,
    } = this.props;

    const { ekzeget } = interpretationsData.find(({ id }) => +id === +ekzegetId);

    Popup.create(
      {
        title: null,
        content: (
          <AddFavoritesInterp
            title={ekzeget?.name}
            items={[short]}
            listIds={ekzegetId}
            clearSelected={() => {}}
            getFavoriteInter={getFavoriteInter}
            isAddFavorite
            addFavToStore={addFavInterpToStore}
            isUpdate={isUpdate}
            handleClickFavInter={this.handleClickFavInter(ekzeget?.id, 'add')}
            getUpdateInterpretation={getUpdateInterpretation}
          />
        ),
        className: 'popup_add_favorite_verse',
      },
      true,
    );
  };

  handleClickFavInter = (ekzegetId, action = '') => () => {
    this.setState({ isAdded: { ...this.state.isAdded, [ekzegetId]: action.length > 0 } });
  };

  deleteFavorite = ekzegetId => async () => {
    const { getUpdateInterpretation, favoriteInterList } = this.props;

    getUpdateInterpretation();

    const { current, ekzeget } = getFavoriteListEkzeget(favoriteInterList, ekzegetId);

    await this.popUpDelete(ekzeget, current?.id);
  };

  popUpDelete = (ekzeget, currentId) => {
    const {
      deleteFavoriteInter,
      verse: { short },
      getUpdateInterpretation,
    } = this.props;

    Popup.create({
      title: null,
      content: (
        <DeleteFavoriteInterp
          deleteFavorite={deleteFavoriteInter}
          currentId={currentId}
          title={ekzeget?.name}
          handleClickFavInter={this.handleClickFavInter(ekzeget?.id)}
          items={[short]}
        />
      ),
      className: 'popup__delete-favorite',
    });

    getUpdateInterpretation();
  };

  render() {
    const {
      isFetching,
      verse,
      params,
      userLoggedIn,
      currentTranslate,
      interpretationsData = [],
    } = this.props;

    const { locationState } = this.state;

    const verseLink = `/bible/${params.book_code}/${params.chapter_num}/stih-${params.verse_id}`;

    const verseHead =
      verse &&
      verse.translate.find(
        ({ code }) =>
          code.code === currentTranslate?.code || code.title === currentTranslate?.title,
      );

    if (!interpretationsData.length && isFetching) {
      return <Redirect to={verseLink} />;
    }

    return (
      <div className="interpretation_detail__content">
        {this.props.isLoadingInter ? (
          <Preloader className="interpretation_detail__content__leftSide" />
        ) : (
          <div className="interpretation_detail__content__leftSide">
            {interpretationsData.map(
              ({ id, verse: verseD, comment, addedBy, editedBy, ekzeget }) => {
                return (
                  <div className="inter-detail-content-body" key={id}>
                    <InterpretationContentHead
                      verseLink={verseLink}
                      verse={verseHead}
                      currentTranslate={currentTranslate}
                    />
                    <InterpretationContentTitle
                      userLoggedIn={userLoggedIn}
                      ekzeget={ekzeget}
                      ekzegetId={id}
                      deleteFavorite={this.deleteFavorite}
                      addFavorite={this.addFavorite}
                    />
                    <InterpretationContentGroup
                      groupInterpretation={verseD}
                      params={params}
                      verse={verse}
                    />
                    <InterpretationContentText
                      verse={verse}
                      currentTranslate={currentTranslate}
                      contextInterpretation={this.getMarkerFromInterpretation(
                        locationState,
                        comment,
                      )}
                      addedBy={addedBy}
                      editedBy={editedBy}
                    />
                  </div>
                );
              },
            )}
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  favoriteInter: getInterList(state),
  userLoggedIn: userLoggedIn(state),
  currentTranslate: getCurrentTranslate(state),
  favoriteInterList: favoriteInterListSelector(state),
  interpretators: interpretatorsSelector(state),
  isLoadingInter: getIsLoadingInter(state),
});

const mapDispatchToProps = {
  ...interpretationsActions,
  addFavInterpToStore,
  getFavoriteInter,
  deleteFavoriteInter,
  getFavoriteVerse,
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(InterpretationContent));
