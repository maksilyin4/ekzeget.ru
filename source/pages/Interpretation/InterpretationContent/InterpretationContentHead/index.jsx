import React from 'react';
import { Link } from 'react-router-dom';
import cx from 'classnames';

const InterpretationContentHead = React.memo(
    ({ verseLink, verse, currentTranslate }) => {
        return (
            verse?.text && (
                <div className="interpretation_detail__content__leftSide-head">
                    <span
                        //  to={verseLink}
                        className="interpretation_detail__content__leftSide-verse"
                    >
                        <div
                            className={cx({
                                csya: currentTranslate?.code === 'csya_old',
                                greek: currentTranslate?.code === 'grek',
                            })}
                        >
                            <b
                                dangerouslySetInnerHTML={{
                                    __html: verse?.text || '',
                                }}
                            ></b>
                        </div>
                    </span>
                </div>
            )
        );
    }
);

export default InterpretationContentHead;
