export const getContent = ({ isAdded, addFavorite, deleteFavorite }) => {
  let title = 'Добавить толкование в избранное';
  let func = addFavorite;
  if (isAdded) {
    title = 'Удалить толкование из избранного';
    func = deleteFavorite;
  }
  return [title, func];
};
