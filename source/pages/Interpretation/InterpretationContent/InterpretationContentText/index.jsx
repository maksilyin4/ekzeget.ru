import R from 'ramda';
import React from 'react';
import cx from 'classnames';

const InterpretationContentText = React.memo(
  ({ verse, currentTranslate, contextInterpretation, addedBy, editedBy }) => {
    return !R.isEmpty(verse) ? (
      <div className="interpretation_detail__content__leftSide-text">
        <div
          className={cx({
            csya_old__container: currentTranslate?.code === 'csya_old',
            grek__container: currentTranslate?.code === 'grek',
          })}
          dangerouslySetInnerHTML={{ __html: contextInterpretation }}
        />
        {!!addedBy && <p style={{ marginTop: 16, textAlign: 'right' }}>{`Добавил: ${addedBy}`}</p>}
        {!!editedBy && <p style={{ textAlign: 'right' }}>{`Отредактировал: ${editedBy}`}</p>}
      </div>
    ) : (
      <div className="interpretation_detail__content__leftSide-text">
        <p style={{ fontSize: '1.5rem' }}>
          Либо выбранный экзегет не толковал данный стих, либо его толкование в нашей базе
          отсутствует!
        </p>
        <p style={{ fontSize: '1.5rem' }}>
          Но Вы можете выбрать толкование любого другого экзегета из списка справа. Также Вы можете
          воспользоваться списком параллельных стихов, чтобы найти толкования на схожие места
          Писания.
        </p>
        <p style={{ fontSize: '1.5rem' }}>
          Или же Вы можете помочь проекту и добавить отсутствующее толкование, кликнув на
          &laquo;плюсик&raquo; в правом верхнем углу.
        </p>
      </div>
    );
  },
);

export default InterpretationContentText;
