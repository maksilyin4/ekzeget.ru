import R from 'ramda';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { withCookies } from 'react-cookie';

import { Popup } from '../../dist/browserUtils';
import interpretationsActions from '../../dist/actions/ChapterInterpretations';

import exegetesActions from '../../dist/actions/Exeget';
import bookmarksActions from '../../dist/actions/Bookmarks';
import getScreen from '../../dist/selectors/screen';
import { addFavToStore, getFavoriteInter } from '../../dist/actions/favorites';
import { bookmarks } from '../../dist/selectors/Bookmarks';

import { getVerseList, getNotes } from '../../dist/selectors/favorites';
import { getVerse } from '../../dist/selectors/Verse';
import {
  getCurrentTranslate,
  getCurrentTranslateCode,
  getTranslatesForSelect,
} from '../../dist/selectors/translates';
import { getIsLoading } from '../../dist/selectors/Exeget';

// components
import AddFavoritesVerse from '../../components/Popups/AddFavoriteVerse';
import AddInterpretation from '../../components/Popups/AddInterpretation';
import popupShowManuscript from '~pages/Bible/Chapter/popupChapter/popupShowManuscript';
import AddNote from '../../components/Popups/AddNote';
import InterpretationContent from './InterpretationContent';
import Tabs, { Tab, TabActions, TabContent } from '../../components/Tabs/Tabs';
import VerseTranslates from '~components/Bible/VerseTabs/VerseTranslates';
import VersePreaching from '~components/Bible/VerseTabs/VersePreaching';
import VerseParallel from '~components/Bible/VerseTabs/VerseParallel';
import VerseEkzeget from '~components/Bible/VerseTabs/VerseEkzeget';
import BibleMediaMobileInterpretations from '~components/Bible/BibleMediaMobileInterpretations';
import { popupAuth } from '~components/Popups/PopupAuth';

import InterpretationTabActionList from './InterpretationTabActionList';
import { setIsShowMobileInterpretations } from '../Bible/utilsBible';

class InterpretationHead extends Component {
  static propTypes = {
    verse: PropTypes.object,
  };

  state = {
    activeContent: 1,
    isOpen: false,
  };

  componentDidMount() {
    this.fetchGetFavoriteInter();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.location.pathname !== this.props.location.pathname) {
      this.setState({ isOpen: false });
    }

    if (
      prevProps.translateEkzeget !== this.props.translateEkzeget ||
      prevProps.match.params.verse !== this.props.match.params.verse ||
      prevProps.match.params.ekzeget !== this.props.match.params.ekzeget
    ) {
      this.setState({
        activeContent: 1,
      });
    }
  }

  fetchGetFavoriteInter = () => {
    const { userLoggedIn, favoriteInter, getFavoriteInter } = this.props;

    if (userLoggedIn && R.isEmpty(favoriteInter)) {
      getFavoriteInter();
    }
  };

  toggleTranslate = async () => {
    const {
      match: { params },
    } = this.props;

    await this.props.getActionVerse(params);
    await this.props.getInterpretationByUrl(params);
  };

  showManuscript = (book, chapter, verse) => {
    popupShowManuscript({ book, chapter, verse });
  };

  handleChange = field => value => {
    this.setState({ [field]: value });
  };

  addNote = () => {
    const { verseId, bookInfo, userLoggedIn } = this.props;
    if (userLoggedIn) {
      const sendData = `${bookInfo.short_title}. ${verseId.chapter}:${verseId.number}`;

      Popup.create(
        {
          title: null,
          content: (
            <AddNote
              sendData={sendData}
              title={`${bookInfo.title}, Глава ${verseId.chapter}, Стих ${verseId.number}`}
            />
          ),
          className: 'addNote_popup',
        },
        true,
      );
    } else {
      popupAuth('Добавить заметку на стих');
    }
  };

  closeMenu = () => {
    this.setState({
      isOpen: false,
    });
  };

  openMenu = () => {
    this.setState({
      isOpen: true,
    });
  };

  addFavorite = isAdded => {
    if (isAdded) return;
    Popup.close();
    const {
      verseId: { id, number, chapter },
      bookInfo,
      addFavToStore,
      userLoggedIn,
    } = this.props;
    if (userLoggedIn) {
      Popup.create(
        {
          title: null,
          content: (
            <AddFavoritesVerse
              listIds={[id]}
              title={`${R.pathOr('', ['title'], bookInfo)}, ${R.pathOr(
                '',
                ['short_title'],
                bookInfo,
              )}. ${chapter}:`}
              items={[number]}
              clearSelected={() => ({})}
              addFavToStore={addFavToStore}
            />
          ),
          className: 'popup_add_favorite_verse',
        },
        true,
      );
    } else {
      popupAuth('Добавить стих в избранное');
    }
  };

  addInterpretation = () => {
    const {
      verseId,
      bookInfo,
      match: {
        params: { book_code, chapter_num },
      },
      userLoggedIn,
    } = this.props;

    if (userLoggedIn) {
      const bookName = R.pathOr('Евангелие', ['title'], bookInfo);
      const verseNumber = R.pathOr('Стих', ['short'], verseId);

      Popup.create(
        {
          title: null,
          content: (
            <AddInterpretation
              title={`${bookName}, ${verseNumber}`}
              verseId={verseId.id}
              bookCode={book_code}
              chapterId={chapter_num}
            />
          ),
          className: 'addInterpretation_popup',
        },
        true,
      );
    } else {
      popupAuth('Добавить толкование');
    }
  };

  deleteInterpretationFavorite = () => {
    const {
      deleteBookmark,
      bookmarks,
      verse: { verse },
    } = this.props;

    // TODO: Это дичь какая - то, с беком решить, пока он занят
    const currentVerse =
      bookmarks.bookmark[`${verse.book.short_title}. ${verse.chapter}:${verse.number}`];
    const isAdded = !currentVerse;

    if (!isAdded) {
      deleteBookmark(currentVerse.id);
    }
  };

  changeTab = () => {
    if (this.state.activeContent !== 1) {
      this.setState({
        activeContent: 1,
      });
    }
  };

  render() {
    const {
      userLoggedIn,
      favoriteVerse,
      noteList,
      verseId,
      verseContent,
      bookmarks: { bookmark = {} },
      isFetching,
      params,
      ekzeget,
      screen,
      verseTranslate,
      currentTranslate,
      currentCode,
      location,
      interpretationsData,
      activeItem,
      chapterNum,
      getUpdateInterpretation,
      isUpdate,
      getParentInter,
      interpretations,
      bookInfo,
      cookies,
    } = this.props;

    const { activeContent, isOpen } = this.state;
    const isFavAdded = favoriteVerse.hasOwnProperty(verseId && verseId.id);

    const short = `${bookInfo?.short_title}. ${chapterNum}:${verseId?.number}`;

    const isShowMobileInterpretations = setIsShowMobileInterpretations(cookies, screen.phone);

    return (
      <Tabs
        activeTab={activeContent}
        callBack={this.handleChange('activeContent')}
        className="tabs_two-col-content"
        classNameTabs="interpretation-tabs"
        twoColumn
        verseTabs
        screen={screen}
        activeContent={activeContent !== 0}
        verseTranslate={verseTranslate}
        currentTranslate={currentTranslate && currentTranslate.code}
        toggleTranslate={this.toggleTranslate}>
        <Tab label={params?.ekzeget ? 'Толкование' : 'Толкования'} value={1} />
        <Tab label="Переводы" value={2} />
        <Tab label="Проповеди" value={3} />
        <Tab label="Параллельные стихи" value={4} />
        <Tab label="На стих ссылаются" value={5} />
        <TabActions>
          {/* TODO удалить если не будет нужен 03.12.2019 */}
          {/* {!screen.desktop && (
              <div className="exegetes__wrapper-adaptive">
                <BibleManuscriptAction
                  key="ekzegets"
                  onClick={this.openMenu}
                  icon="ekzeget"
                  text={screen.phone || screen.tablet ? 'Толкования' : 'Экзегеты'}
                />
              </div>
            )} */}

          <InterpretationTabActionList
            userLoggedIn={userLoggedIn}
            bookmark={bookmark}
            short={short}
            isFavAdded={isFavAdded}
            noteList={noteList}
            deleteInterpretationFavorite={this.deleteInterpretationFavorite}
            addInterpretation={this.addInterpretation}
            addFavorite={this.addFavorite}
            addNote={this.addNote}
            showManuscript={() =>
              this.showManuscript(
                verseContent?.book.ext_id,
                verseContent?.chapter,
                verseContent?.number,
              )
            }
          />
        </TabActions>

        <TabContent>
          <div className="content_two-col">
            <div className="chapter-content">
              {activeContent === 1 && (
                <>
                  {isShowMobileInterpretations && (
                    <BibleMediaMobileInterpretations
                      activeItem={activeItem}
                      chapter={verseContent}
                      baseUrl="bible"
                      bookCode={params.book_code}
                      verseCurrent={+verseId?.number}
                      title={params.ekzeget && ekzeget.name}
                    />
                  )}
                  <InterpretationContent
                    getParentInter={getParentInter}
                    isFetching={isFetching}
                    interpretationsData={interpretationsData}
                    interpretations={interpretations}
                    ekzeget={ekzeget}
                    verse={verseContent}
                    params={params}
                    screen={screen}
                    isOpen={isOpen}
                    closeMenu={this.closeMenu}
                    location={location}
                    isUpdate={isUpdate}
                    getUpdateInterpretation={getUpdateInterpretation}
                  />
                </>
              )}
              {activeContent === 2 && <VerseTranslates />}
              {activeContent === 3 && <VersePreaching verseId={verseContent?.id} />}
              {activeContent === 4 && (
                <VerseParallel verseId={verseContent?.id} currentCode={currentCode} />
              )}
              {activeContent === 5 && (
                <VerseEkzeget chapterNum={verseContent?.chapter} verseNum={verseContent?.number} />
              )}
            </div>
          </div>
        </TabContent>
      </Tabs>
    );
  }
}

const mapStateToProps = state => ({
  isLoadingExeget: getIsLoading(state),
  favoriteVerse: getVerseList(state),
  noteList: getNotes(state),
  bookmarks: bookmarks(state),
  verse: getVerse(state),
  screen: getScreen(state),
  verseTranslate: getTranslatesForSelect(state),
  currentTranslate: getCurrentTranslate(state),
  currentCode: getCurrentTranslateCode(state),
});

const mapDispatchToProps = {
  addFavToStore,
  getFavoriteInter,
  ...interpretationsActions,
  ...exegetesActions,
  ...bookmarksActions,
};

export default withCookies(
  connect(mapStateToProps, mapDispatchToProps)(withRouter(InterpretationHead)),
);
