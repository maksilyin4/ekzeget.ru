import R from 'ramda';

export const checkVerse = (verse = {}, key = '') => verse && verse.book && verse.book[key];

const getBookTitle = bookInfo => bookInfo?.title || '';

const getEkzegetName = (ekzeget, ekzegetParams) =>
  ekzegetParams ? `${ekzeget?.name || 'экзегет'}` : '';

export const getTitle = ({ ekzeget, number, bookInfo, chapter, ekzegetParams }) => {
  return ekzegetParams
    ? `${getBookTitle(bookInfo)} ${chapter} глава ${number} стих - ${getEkzegetName(
        ekzeget,
        ekzegetParams,
      ) || 'Экзегет'}`
    : `${getBookTitle(bookInfo)} ${chapter} глава ${number} стих - ${
        bookInfo.short_title
      } ${chapter}:${number} - Библия`;
};

export const getMainTitle = ({ bookInfo, chapterNum, verseParams, ekzeget, ekzegetParams }) => {
  if (ekzegetParams) {
    const ekzegetName = getEkzegetName(ekzeget, ekzegetParams);
    return `Толкование ${getBookTitle(
      bookInfo,
    )} ${chapterNum} глава ${verseParams} стих - ${ekzegetName}`;
  }

  return `${getBookTitle(bookInfo)} ${chapterNum} глава ${verseParams} стих`;
};

export const getSubTitle = ({ verse, checkVerse }) => {
  return `Автор ${checkVerse(verse, 'author')}, ${checkVerse(verse, 'year')}, ${checkVerse(
    verse,
    'place',
  )}`;
};

export const breadItems = ({
  bookTitle,
  book,
  onMouseLeave,
  onMouseEnter,
  onMouseObj,
  chapterNum,
  ekzeget,
  ekzegetParams,
  number,
}) => {
  if (!book) {
    return [
      { path: '/', title: 'Главная' },
      { path: '/bible', title: 'Библия' },
    ];
  }
  const bookCode = R.pathOr('mf', ['code'], book);

  const bread = [
    { path: '/', title: 'Главная' },
    {
      path: `/bible/${bookCode}/glava-${chapterNum}/`,
      title: bookTitle,
      onMouseLeave,
      onMouseEnter: () => onMouseEnter(onMouseObj.onMouseBibleBook),
      onFocus: onMouseObj.onMouseBibleBook,
    },
    {
      path: `/bible/${bookCode}/glava-${chapterNum}/`,
      title: `Глава ${chapterNum || ''}`,
      onMouseLeave,
      onMouseEnter: () => onMouseEnter(onMouseObj.onMouseBibleChapter),
      onFocus: onMouseObj.onMouseBibleChapter,
    },
  ];

  const lastBread = ekzegetParams
    ? [
        {
          path: `/bible/${bookCode}/glava-${chapterNum}/stih-${number}/`,
          title: `Стих ${number || ''}`,
        },
        { path: '', title: ekzeget?.name },
      ]
    : [
        {
          path: ``,
          title: `Стих ${number || ''}`,
        },
      ];

  bread.push(...lastBread);

  return bread;
};

export const getFavoriteListEkzeget = (favoriteInterList = [], ekzegetId) => {
  const current = favoriteInterList.find(({ interpretation }) => {
    return +interpretation.id === +ekzegetId;
  });

  const ekzeget = current?.ekzeget;
  return { current, ekzeget };
};
