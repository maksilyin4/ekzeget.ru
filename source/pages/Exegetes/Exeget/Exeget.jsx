import R from 'ramda';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';

import './exeget.scss';

import { maxDefaultDesc } from './utilsExeget';
import { smoothScrollTo } from '../../../dist/utils';

import { getFulfilled } from '../../../store/metaData/actions';
import exegetActions from '../../../dist/actions/Exeget';
import { setMetaData } from '../../../store/metaData/selector';

import { getIsLoading, getExeget, getErrorExeget } from '../../../dist/selectors/Exeget';
import screen from '../../../dist/selectors/screen';

import Breadcrumbs from '../../../components/Breadcrumbs/Breadcrumbs';
import Preloader from '../../../components/Preloader/Preloader';
import ExegetList from '../../../components/ExegetList/ExegetList';
import TextField from '../../../components/TextField/TextField';
import NotFound from '../../NotFound/NotFound';
import Helmet from '~components/Helmet';

class Exeget extends Component {
  static propTypes = {
    location: PropTypes.object,
    exeget: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.state = {
      exegetId: this.props.location.pathname.split('/')[3],
      searchQuery: '',
      showDesc: false,
    };

    this.descRef = React.createRef();
  }

  componentDidMount() {
    smoothScrollTo();
    this.props.getExeget({ info: 1, id: this.state.exegetId });
    this.getMeta();
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      exegetId: nextProps.location.pathname.split('/')[3],
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const { getExeget, exeget } = this.props;
    const { exegetId } = this.state;
    if (prevState.exegetId !== exegetId) {
      getExeget({ info: 1, id: exegetId });
    }

    if (prevProps.exeget?.ekzeget?.name !== exeget?.ekzeget?.name) {
      this.getMeta();
    }
  }

  getMeta = () => {
    const { getFulfilled, exeget } = this.props;

    if (exeget?.ekzeget?.name) {
      const meta = {
        title: `Читать биографию - ${exeget.ekzeget.name}, православный лекторий.`,
        h_one: `${exeget.ekzeget.name}`,
      };

      getFulfilled({ meta });
    }
  };

  _breadItems = props => [
    { path: '/', title: 'Главная' },
    { path: '/all-about-bible/ekzegets/', title: 'Экзегеты' },
    { path: '', title: props },
  ];

  _handleChange = field => e => {
    e.target.value = R.pathOr('', [0], e.target.value.match(/^[а-яё0-9: ].*/i));
    this.setState({ [field]: e.target.value });
  };

  toggleActive = () => {
    const { showDesc } = this.state;

    this.setState({
      showDesc: !showDesc,
    });

    if (showDesc && this.descRef.current.scrollTop !== 0) {
      this.descRef.current.scrollTo({ top: 0, behavior: 'smooth' });
    }
  };

  render() {
    const { exeget, screen, error } = this.props;

    if (Object.values(exeget).length === 0) {
      return null;
    }

    if (error) {
      return <NotFound />;
    }

    const isMaxShowDesc = exeget.ekzeget.info && exeget.ekzeget.info.length > maxDefaultDesc;

    return (
      <div className="exeget__main">
        <div className="page__head page__head_preview page__head_two-col">
          {this.props.isLoading ? (
            <Preloader />
          ) : (
            <div className="wrapper">
              {exeget.ekzeget !== null ? (
                <>
                  <Helmet
                    title={`Читать биографию - ${exeget.ekzeget.name}, православный лекторий.`}
                    description={`${exeget.ekzeget.name} все толкования экзегета на Библию, представленные на сайте.`}
                    keywords={`${exeget.ekzeget.name}, житие, святой, биография, история Церкви, святые отцы, экзегеты, толкователи, богослов.`}
                  />

                  <Breadcrumbs data={this._breadItems(exeget.ekzeget.name)} />
                  <div className="page__preview exeget__preview">
                    <div className="page__preview-content exeget__preview-content">
                      <h1 className="page__title">{exeget.ekzeget.name}</h1>
                      <div className="exeget__century">
                        {exeget.ekzeget.century !== null
                          ? `Период жизни: ${exeget.ekzeget.century} век`
                          : 'Период жизни неизвестен'}
                      </div>
                      <div className="exeget__type">
                        Принадлежность: {exeget.ekzeget.ekzeget_type?.title || null}
                      </div>
                      {(screen.phone || screen.tablet) && (
                        <div className="page__preview-image exeget__adaptive">
                          <img src="/frontassets/images/page-preview/exeget.png" alt="Экзегет" />
                        </div>
                      )}
                      <div className="page__desc">
                        <div
                          className={cx('page__desc-text exeget__desc', {
                            active: this.state.showDesc,
                            isMaxShowDesc: !isMaxShowDesc,
                          })}
                          ref={this.descRef}
                          dangerouslySetInnerHTML={{ __html: exeget.ekzeget.info }}
                        />
                        {isMaxShowDesc && (
                          <button
                            className="page__desc-toggle text_underline"
                            onClick={this.toggleActive}>
                            {this.state.showDesc ? 'Скрыть описание' : 'Читать полностью'}
                          </button>
                        )}
                      </div>
                    </div>
                    {screen.desktop && (
                      <div className="page__preview-image">
                        <img src="/frontassets/images/page-preview/exeget.png" alt="Экзегет" />
                      </div>
                    )}
                  </div>
                </>
              ) : (
                <p> Экзегет не найден или произошла ошибка </p>
              )}
            </div>
          )}
        </div>

        <div className="page__body exeget__body">
          {this.props.isLoading ? (
            <Preloader />
          ) : (
            <div className="wrapper exeget__wrapper">
              <div className="exeget__interpretations">
                <div className="exeget__search">
                  <TextField
                    id="exegetes_search"
                    type="search"
                    placeholder="Поиск по толкованиям экзегета"
                    onChange={this._handleChange('searchQuery')}
                  />
                </div>
                <ExegetList
                  exegetId={this.state.exegetId}
                  code={exeget.ekzeget.code}
                  query={this.state.searchQuery}
                />
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: getIsLoading(state),
  exeget: getExeget(state),
  screen: screen(state),
  error: getErrorExeget(state),
  metaData: setMetaData(state),
});

const mapDispatchToProps = {
  ...exegetActions,
  getFulfilled,
};

export default connect(mapStateToProps, mapDispatchToProps)(Exeget);
