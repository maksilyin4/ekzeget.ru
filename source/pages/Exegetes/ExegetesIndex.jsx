import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Exegetes from './Exegetes';
import Exeget from './Exeget/Exeget';
import NotFound from '../NotFound/NotFound';

const ExegetsIndex = ({ getTitle, match: { path } }) => {
  return (
    <Switch>
      <Route
        exact
        path={[`${path}`, `${path}?=:exegetesId`]}
        render={props => <Exegetes getTitle={getTitle} {...props} />}
      />
      <Route
        exact
        path={`${path}:exeget`}
        render={props => <Exeget getTitle={getTitle} {...props} />}
      />
      <Route component={NotFound} />
    </Switch>
  );
};

export default ExegetsIndex;
