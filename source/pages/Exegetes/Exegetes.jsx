import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import cx from 'classnames';
import Select from 'react-select';

import './exegetes.scss';

import { Popup } from '../../dist/browserUtils';
import { getMetaData } from '../../store/metaData/actions';
import { smoothScrollTo } from '../../dist/utils';
import exegetesActions from '../../dist/actions/Exegetes';
import { getIsDesktop, getIsTablet, getIsTabletMedium } from '../../dist/selectors/screen';
import {
  getExegetesIsLoading,
  getExegetes,
  getEkzegetTypes,
  setExegetIsError,
} from '../../dist/selectors/Exegetes';
import { setMetaData } from '../../store/metaData/selector';

import Preloader from '../../components/Preloader/Preloader';
import TextField from '../../components/TextField/TextField';
import PopupExegetes from '../../components/Popups/Exegetes/Exegetes';
import Icon from '../../components/Icons/Icons';
import Alphabet from '../../components/Alphabet';
import List, { ListItem } from '../../components/List/List';
import Helmet from '~components/Helmet';
import EkzegetsNav from './EkzegetsNav';
import NotFound from '../NotFound/NotFound';

class Exegetes extends Component {
  constructor(props) {
    super(props);

    const {
      location: { search },
    } = props;

    const exegetType = search ? search.split('?')[1].replace('=', '') : '';

    this.state = {
      sort: 'alph',
      localEkzegetes: [],
      searchExegetes: '',
      exegetType,
      activeLetter: 'А',
      century: '1',
      typeInOrder: [
        {
          value: 1,
          label: 'Алфавитный порядок',
        },
        {
          value: 2,
          label: 'Хронологический порядок',
        },
      ],
    };
  }

  componentDidMount = async () => {
    smoothScrollTo();

    await this.getMeta();
    if (this.props.exegetes.ekzeget) return;
    await this.props.getExegetes();
  };

  componentDidUpdate = prevProps => {
    const {
      metaData,
      getTitle,
      location: { pathname },

      isError,
    } = this.props;

    if (prevProps.metaData !== metaData) {
      getTitle(metaData?.h_one || 'Экзегеты');
    }

    if (prevProps.location.pathname !== pathname) {
      getMetaData(pathname);

      if (isError) {
        this.props.getExegetes();
      }
    }
  };

  static getDerivedStateFromProps(props, state) {
    if (props.exegetes && props.exegetes.ekzeget && !state.localEkzegetes.length) {
      return {
        localEkzegetes: props.exegetes.ekzeget,
      };
    }
    return {};
  }

  getMeta = () => {
    const {
      location: { pathname },
      getTitle,
      getMetaData,
      metaData,
    } = this.props;

    getMetaData(pathname);

    getTitle(metaData?.h_one || 'Экзегеты');
  };

  _breadItems = () => [
    { path: '/', title: 'Главная' },
    { path: '', title: 'Экзегеты' },
  ];

  _who = e => {
    e.preventDefault();

    Popup.create(
      {
        title: (
          <div className="page-desc-popup__title">
            <span className="page-desc-popup__title-text exegets__title">Кто такие экзегеты?</span>{' '}
            <div className="page-desc-popup__icon">
              <Icon icon="manuscript_desc" />
            </div>
          </div>
        ),
        content: <PopupExegetes />,
        className: 'popup_exegetes',
      },
      true,
    );
  };

  _sortChrone = () => {
    const { localEkzegetes } = this.state;
    const sortedEkzegetes = [...localEkzegetes];
    sortedEkzegetes.sort((a, b) => a.century - b.century);

    this.setState({
      localEkzegetes: sortedEkzegetes,
      sort: 'chrone',
    });
  };

  _sortAlph = () => {
    const { localEkzegetes } = this.state;
    const sortedAlphEkzegetes = [...localEkzegetes];
    sortedAlphEkzegetes.sort((a, b) => {
      if (a.name > b.name) {
        return 1;
      }
      if (a.name < b.name) {
        return -1;
      }
    });

    this.setState({ sort: 'alph', localEkzegetes: sortedAlphEkzegetes });
  };

  _sortType = type => () => {
    this.setState({ exegetType: type.value });
    if (this.props.isTablet) {
      const search = type.value ? `?=${type.value}` : '';
      this.props.history.push(`/all-about-bible/ekzegets/${search}`);
    }
  };

  _handleChange = field => ({ target: { value } }) => this.setState({ [field]: value });

  selectChange = e => {
    const { typeInOrder } = this.state;

    if (typeInOrder[0].value === e.value) {
      this._sortAlph();
    } else {
      this._sortChrone();
    }
  };

  changeLetter = value => () => {
    if (isNaN(Number(value))) {
      this.setState({ activeLetter: value });
    } else {
      this.setState({ century: value });
    }
  };

  render() {
    const { ekzegetTypes, isDesktop, isTabletMedium, metaData, isError } = this.props;

    const { sort, typeInOrder, localEkzegetes } = this.state;

    const firstLetters = {};
    const centuryes = {};

    function getArrayUniqueEl(obj, el) {
      if (el) {
        const t = el;
        obj[t] = true;
        return obj;
      }
    }

    if (localEkzegetes && localEkzegetes.length > 0) {
      !this.props.isLoading &&
        localEkzegetes.forEach(ekzeget => {
          getArrayUniqueEl(firstLetters, ekzeget.name.substr(0, 1));
          getArrayUniqueEl(centuryes, ekzeget.century);
        });
    }

    const alphabet = Object.keys(firstLetters).sort();
    const centuries = Object.keys(centuryes);

    if (isError) {
      return <NotFound />;
    }

    return (
      <div className="page exegetes-page">
        <Helmet
          title={
            metaData?.title || 'Экзегеты - Библия и ее толкования Святыми Отцами на портале Экзегет'
          }
        />

        {/* оставил не уверен что нужно убирать */}
        {/* <div className="wrapper">
          <Breadcrumbs data={this._breadItems()} />
          <div className="page__head page__head_two-col">
            {isLoad ? (
              <Preloader className="preloader-meta" />
            ) : (
              <h1 className="page__title">{h_one}</h1>
            )}
            <Link to="#" className="page-desc-link" onClick={this._who}>
              Кто такие экзегеты?
            </Link>
          </div>
        </div> */}
        <div className="page__body exegets_body">
          <div className="wrapper wrapper_two-col exegetes not-padding all_about_bible">
            <EkzegetsNav
              isTabletMedium={isTabletMedium}
              isDesktop={isDesktop}
              sort={sort}
              typeInOrder={typeInOrder}
              types={ekzegetTypes}
              sortAlph={this._sortAlph}
              sortChrone={this._sortChrone}
              sortType={this._sortType}
              selectChange={this.selectChange}
            />
            <div className="all-about-bible-content paper exegets_paper">
              <div className="content_two-col ekzeget-body">
                <div className="exegetes_left">
                  <div className="exegetes__search">
                    <TextField
                      id="exegetes_search"
                      type="search"
                      placeholder="Поиск по экзегетам"
                      onChange={this._handleChange('searchExegetes')}
                    />
                  </div>
                  {this.props.isLoading && !firstLetters.length ? (
                    <Preloader />
                  ) : (
                    <>
                      {this.state.sort === 'alph' && (
                        <>
                          <Alphabet
                            alphabet={alphabet}
                            activeLetter={this.state.activeLetter}
                            onClick={this.changeLetter}
                            className="all-about-bible-alphabet"
                          />
                          <div className="all-about-bible-list">
                            {alphabet.map((letter, i) => (
                              <div key={i + letter} className="exegetes__list-group">
                                <div className="exegetes__list-group-content">
                                  {this.state.activeLetter.toLowerCase() ===
                                  letter.toLowerCase() ? (
                                    <div className="exegetes__list-separate">{letter}</div>
                                  ) : (
                                    ''
                                  )}
                                  {localEkzegetes.map(
                                    exeget =>
                                      exeget.ekzeget_type.code
                                        .toLowerCase()
                                        .indexOf(this.state.exegetType.toLowerCase()) !== -1 &&
                                      exeget.name
                                        .toLowerCase()
                                        .indexOf(this.state.searchExegetes.toLowerCase()) !== -1 &&
                                      exeget.name.substr(0, 1) === letter &&
                                      exeget.name.substr(0, 1).toLowerCase() ===
                                        this.state.activeLetter.toLowerCase() && (
                                        <div
                                          className="exegetes__item"
                                          key={exeget.code + exeget.id}>
                                          <Link
                                            to={`/all-about-bible/ekzegets/${exeget.code}/`}
                                            className="exegetes__link"
                                            title={String(exeget.name)}>
                                            {exeget.name}
                                          </Link>
                                        </div>
                                      ),
                                  )}
                                </div>
                              </div>
                            ))}
                          </div>
                        </>
                      )}
                      {this.state.sort === 'chrone' && (
                        <>
                          <Alphabet
                            alphabet={centuries}
                            activeLetter={this.state.century}
                            onClick={this.changeLetter}
                          />
                          <div className="exegetes__list">
                            {centuries.map((century, i) => (
                              // eslint-disable-next-line react/no-array-index-key
                              <div key={i} className="exegetes__list-group">
                                <div className="exegetes__list-group-content">
                                  {localEkzegetes.map(
                                    exeget =>
                                      exeget.ekzeget_type.code
                                        .toLowerCase()
                                        .indexOf(this.state.exegetType.toLowerCase()) !== -1 &&
                                      exeget.name
                                        .toLowerCase()
                                        .indexOf(this.state.searchExegetes.toLowerCase()) !== -1 &&
                                      exeget.century === +century &&
                                      this.state.century === century && (
                                        <div
                                          className="exegetes__item"
                                          key={exeget.code + exeget.id}>
                                          <Link
                                            to={`/all-about-bible/ekzegets/${exeget.code}/`}
                                            className="exegetes__link"
                                            title={String(exeget.name)}>
                                            {exeget.name}
                                          </Link>
                                        </div>
                                      ),
                                  )}
                                </div>
                                {century && (
                                  <div className="exegetes__list-separate">{`${century} век`}</div>
                                )}
                              </div>
                            ))}
                          </div>
                        </>
                      )}
                    </>
                  )}
                </div>

                <div className="side_right list_width all-about-bible-rigth-select">
                  <List className="side__interpretators not-height all-about-bible-right-nav">
                    {!isDesktop ? (
                      <Select
                        isSearchable={false}
                        options={ekzegetTypes}
                        defaultValue={ekzegetTypes[0]}
                        value={ekzegetTypes.find(el => el.value === this.state.exegetType)}
                        onChange={e => this._sortType(e)()}
                        classNamePrefix="custom-select"
                      />
                    ) : (
                      <>
                        <div className="side__interpretators-head lecture__interpretators-head">
                          Все экзегеты
                        </div>
                        <div className="ekzegets-scrollbar">
                          {ekzegetTypes.map(({ value, label }) => (
                            <ListItem
                              key={value}
                              title={label}
                              linkClassName={'list_item_font_size'}
                              onClick={this._sortType({ value })}
                              href={`/all-about-bible/ekzegets/`}
                              className={cx('exzegets__item', {
                                active: value === this.state.exegetType,
                              })}
                            />
                          ))}
                        </div>
                      </>
                    )}
                  </List>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Exegetes.propTypes = {
  isTablet: PropTypes.bool.isRequired,
  isDesktop: PropTypes.bool.isRequired,
  isTabletMedium: PropTypes.bool.isRequired,
  exegetes: PropTypes.object,
};

const mapStateToProps = state => ({
  isLoading: getExegetesIsLoading(state),
  isError: setExegetIsError(state),
  exegetes: getExegetes(state),
  isDesktop: getIsDesktop(state),
  isTabletMedium: getIsTabletMedium(state),
  ekzegetTypes: getEkzegetTypes(state),
  isTablet: getIsTablet(state),
  metaData: setMetaData(state),
});

const mapDispatchToProps = {
  ...exegetesActions,
  getMetaData,
};

export default connect(mapStateToProps, mapDispatchToProps)(Exegetes);
