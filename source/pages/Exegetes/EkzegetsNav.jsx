import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Select from 'react-select';

import OrphusInfo from '../../components/OrphusInfo';

import './ekzeget-nav.scss';

class EkzegetsNav extends Component {
  render() {
    const {
      isTabletMedium,
      isDesktop,
      sort,
      typeInOrder,
      sortAlph,
      sortChrone,
      selectChange,
    } = this.props;
    return (
      <div className="side all-about-bible-side">
        <div className="side__item">
          {isDesktop || isTabletMedium ? (
            <div className="exegetes__selects">
              <div className="side__item nav-side paper-exegetes exegetes__scroll-list">
                <div className="list__item list__item-exegetes">
                  <button
                    className={cx(
                      'nav-side__link list__item-link exegetes-item',
                      sort === 'alph' && 'active',
                    )}
                    onClick={sortAlph}>
                    Алфавитный порядок
                  </button>
                </div>
                <div className="list__item list__item-exegetes">
                  <button
                    className={cx(
                      'nav-side__link list__item-link exegetes-item',
                      sort === 'chrone' && 'active',
                    )}
                    onClick={sortChrone}>
                    Хронологический порядок
                  </button>
                </div>
              </div>
            </div>
          ) : (
            <div className="exegetes__select-adaptive">
              <Select
                isSearchable={false}
                options={typeInOrder}
                defaultValue={typeInOrder[0]}
                onChange={selectChange}
                classNamePrefix="custom-select"
              />
            </div>
          )}
        </div>

        {isDesktop && <OrphusInfo />}
      </div>
    );
  }
}

EkzegetsNav.propTypes = {
  isTabletMedium: PropTypes.bool.isRequired,
  isDesktop: PropTypes.bool.isRequired,
};

export default EkzegetsNav;
