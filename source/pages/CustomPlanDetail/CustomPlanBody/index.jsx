import React, { useCallback, useEffect, useMemo, useState } from 'react';
import CustomPlanBodyLayout from '../Layout/CustomPlanBodyLayout';
import CustomButtonCP from '~components/UICreatePlan/CustomButtonCp';
import CommentsPlan from '~components/CustomPlanModule/Comments';
import R from 'ramda';
import PlanDetailsVerses from '../../Lk/ReadingPlanDetail/PlanDetailsVerses';
import { connect, useDispatch, useSelector } from 'react-redux';
import getScreen, { getIsMobile } from '../../../dist/selectors/screen';
import readingPlanActions, {
  deleteUserReadingPlanSuccess,
  setActiveTab,
} from '../../../dist/actions/ReadingPlan';
import {
  userRPGetIsLoading,
  activeTabUserRP,
  getUserPlans,
} from '../../../dist/selectors/ReadingPlan';
import { getCurrentTranslateCodeLk } from '../../../dist/selectors/translates';
import { _AXIOS } from '../../../dist/ApiConfig';
import { useLocation, useHistory } from 'react-router-dom';
import { planDetailsAtom } from '~pages/Lk/ReadingPlanDetail/model';
import { mentorGroup, userGroup } from '~store/groups/selectors';
import { getMentorGroup, getUserGroup } from '~store/groups/actions';
import { getCommentsPlanByDay } from '~store/commentsPlan/actions';
import { useMedia, useUpdateEffect } from 'react-use';
import { smoothScrollTo } from '../../../dist/utils';
import useGetDayParams from '../../../hooks/useGetDay';
import { getQuizByPlanId, getCurrentQueeze } from '../../../dist/actions/Quiz';
import { completedQueezePlan } from '~dist/selectors/Quiz';
import MainPageGame from '~components/CustomPlanModule/Game/MainPage';
import { createQuizPlan } from '../../../dist/quizAPI';
import ResultPageGame from '~components/CustomPlanModule/Game/ResultPage';
import { getQuizVariant } from '~components/CustomPlanModule/utils/getQuizVariant';
import { useCookies } from 'react-cookie';
import { findCurrentReadingDay } from '../../../components/CustomPlanModule/utils/getActualCurrentDay';
import { deleteGroup } from '~dist/api/readingPlan/createGroup';
import { userData } from '~dist/selectors/User';
import { kickUser } from '~dist/api/readingPlan/kickUser';
import useProgressPlan from '../../../hooks/useProgressPlan';
import { Popup } from '~dist/browserUtils';
import { bibleMediaQuestionsReducer } from '~store/bible/bibleMediaQuestions/selectors';
import {
  getBibleQuestionsData,
  getTypeForQuestions,
} from '../../../components/Bible/BibleMediaContentRight/BibleMedia/BibleMediaContent/BibleMediaBody/BibleMediaQuestions/utilsBibleQuestions';
import { getBibleMediaQuestions } from '~store/bible/bibleMediaQuestions/actions';

function CustomPlanBody({
  userPlans,
  currentTranslate,
  quiz,
  sendCompleteChapter,
  getCurrentQueeze,
  sendResetDays,
  user,
  setActiveTab,
  activeTab,
  group,
  getUserReadingPlan,
  getUserGroup,
  mentorGroup,
  getQuizByPlanId,
  getCommentsPlanByDay,
  planId,
  getMentorGroup,
  deleteUserReadingPlan,
  ...props
}) {
  const [verses, setVerses] = useState([]);
  const [currentDay, setCurrentDay] = useState(null);
  const [chosenDay, setChosenDay] = useState(null);
  const { closedDays } = useProgressPlan();
  const [, setWidthVerser] = useState('');
  const [isFetching, setIsFetching] = useState([]);
  const history = useHistory();
  const dayUrl = useGetDayParams();
  const isMobile = useMedia('(max-width: 768px)');
  const isResult = useLocation()?.pathname.includes('result');
  const [isUnread, setUnread] = useState(false);
  const [isShowAlert, setShowAlert] = useState(false);
  const [isLast, setIsLast] = useState(undefined);

  useUpdateEffect(() => {
    if (activeTab.id && !isResult) {
      getQuizByPlanId(activeTab.id);
    }
  }, [activeTab, isResult]);

  const getVerses = async short => {
    try {
      setIsFetching(true);
      const { verse } = await _AXIOS({ url: `/verse/info/${short}/?per-page=1000&translate=1` });
      setVerses(verse);
      setIsFetching(false);
    } catch (e) {
      setIsFetching(false);
    }
  };

  useUpdateEffect(() => {
    if (activeTab.short) {
      getVerses(activeTab.short);
    }
  }, [activeTab]);

  const getDay = useCallback(
    async day => {
      await getUserReadingPlan(day);
      setChosenDay(day);
    },
    [getUserReadingPlan],
  );

  const [mount, setMount] = useState(false);

  useEffect(() => {
    if (userPlans.id) {
      setCurrentDay(findCurrentReadingDay(userPlans));
    }
  }, [userPlans]);

  useEffect(() => {
    if (userPlans.id && !mount && !isResult) {
      setActiveTab(userPlans.reading.flat()[0]);
      if (activeTab?.day === chosenDay) {
        setMount(true);
      }
    }
  }, [chosenDay, userPlans]);

  useEffect(() => {
    if (!group.id) {
      getUserGroup();
    }
  }, []);

  useEffect(() => {
    if (!mentorGroup?.id) {
      getMentorGroup();
    }
  }, []);
  useEffect(() => {
    if (dayUrl && group.id) {
      getDay(dayUrl);
      setChosenDay(dayUrl);
      setMount(false);
      getCommentsPlanByDay(dayUrl, group.id);
      planDetailsAtom.set.dispatch('planId', planId);
    }
  }, [group, dayUrl]);

  useEffect(() => {
    planDetailsAtom.set.dispatch('currentDay', currentDay);
  }, [currentDay]);

  useEffect(() => {
    planDetailsAtom.set.dispatch('getDay', getDay);
  }, [getDay]);

  // Для prevState.chosenDay !== this.state.chosenDay
  useEffect(() => {
    planDetailsAtom.set.dispatch('chosenDay', chosenDay);
  }, [chosenDay]);

  // Для prevProps.userPlans !== this.props.userPlans
  useEffect(() => {
    planDetailsAtom.set.dispatch('plan', userPlans);
  }, [userPlans]);
  const bibleMediaQuestions = useSelector(bibleMediaQuestionsReducer);
  const [{ interMultiTab }] = useCookies(['interMultiTab', 'heightLeftMenuBible']);
  const type = getTypeForQuestions(interMultiTab);
  const chapterId = verses[0]?.chapter_id;
  const questionId = chapterId + type;

  const [data] = useMemo(() => getBibleQuestionsData(bibleMediaQuestions[questionId]), [
    bibleMediaQuestions,
    questionId,
  ]);

  useEffect(() => {
    dispatch(
      getBibleMediaQuestions({
        book_id: verses[0]?.book_id,
        chapter_id: verses[0]?.chapter_id,
        perPage: 10,
        page: 1,
        type,
      }),
    );
  }, [verses[0]?.chapter_id, verses[0]?.book_id, type]);

  const onPlayGame = async () => {
    await createQuizPlan(
      activeTab.book.id,
      verses[0].chapter_id,
      activeTab.id,
      getCurrentQueeze,
      history.push,
      chosenDay,
      data[0]?.id,
    );
  };

  useEffect(() => {
    if (activeTab.id && userPlans.reading) {
      const plans = userPlans.reading.flat();
      const activeIndex = userPlans.reading.flat().findIndex(plan => plan.id === activeTab.id);
      setIsLast(plans.length - 1 === activeIndex);
    }
  }, [activeTab, userPlans]);

  const correct = quiz?.meta?.correct;
  //
  const plans = userPlans?.reading?.flat();
  const activeIndex = userPlans?.reading?.flat()?.findIndex(plan => plan.id === activeTab.id);
  const otherPlan = plans?.filter((_, i) => i !== activeIndex);

  const completeChapter = async () => {
    if (activeIndex === -1 || !userPlans.reading.flat()[activeIndex + 1]) {
      if (!isUnread && !activeTab.is_closed) {
        await sendCompleteChapter(activeTab.id);
      }
      const url = group.id
        ? `/group/reading/${userPlans.plan.id}/day-${Number(dayUrl) + 1}/`
        : `/reading-plan/${userPlans.plan.id}/day-${Number(dayUrl) + 1}/`;

      await history.push(url);
      await getDay(Number(dayUrl) + 1);
      setChosenDay(Number(dayUrl) + 1);
    } else {
      if (!isUnread && !activeTab.is_closed) {
        await sendCompleteChapter(activeTab.id);
      }
      setActiveTab(userPlans.reading.flat()[activeIndex + 1]);
    }
  };

  const handleNextChapter = async isCorrect => {
    const url = group.id
      ? `/group/reading/${userPlans.plan.id}/day-${dayUrl}/`
      : `/reading-plan/${userPlans.plan.id}/day-${dayUrl}/`;

    if (isResult && correct < 6 && !isCorrect && !isLast) {
      history.push(url);
      return;
    }
    if (isResult) {
      await completeChapter();
      if (!isLast) {
        history.push(url);
      }
      return;
    }
    if (!quiz && !isShowAlert && !isUnread) {
      setShowAlert(true);
    } else {
      setShowAlert(false);
      await completeChapter();
      smoothScrollTo();
    }
  };

  const nameButtonVictorine = useMemo(
    () =>
      getQuizVariant(correct, [
        'Прочитать главу еще раз',
        'Перейти к следующей главе',
        'Перейти к следующей главе',
        'Перейти к следующей главе',
      ]),
    [correct],
  );
  const [cookies] = useCookies(['bibleNavParent']);

  const isVisibleReading = isMobile
    ? cookies?.bibleNavParent?.type
      ? cookies?.bibleNavParent?.type === 'bibleText'
      : true
    : true;

  const isLastDay = userPlans?.plan?.lenght === Number(dayUrl);

  const isReadingDays = closedDays.length + 1 === userPlans?.plan?.lenght;
  const isLastChapter =
    plans?.length === 1 ||
    (otherPlan?.every(p => p.is_closed) &&
      plans?.findIndex(p => p.id === activeTab?.id) === plans?.length - 1);
  const isLastChapterDay =
    plans?.length === 1 || plans?.findIndex(p => p.id === activeTab?.id) === plans?.length - 1;
  const isLastChapterFormPlan = isLastDay && isLastChapter && isReadingDays;
  const isAllRead = plans?.every(p => p.is_closed) && closedDays.length === userPlans?.plan?.lenght;
  const isCheckOpenButton = !(isLastDay && isLastChapterDay);

  const dispatch = useDispatch();
  const handleClosePlan = async () => {
    try {
      if (group?.id && !mentorGroup?.id) {
        await completeChapter();
        Popup.create(
          {
            title: 'Поздравляем, вы завершили план!',
            className: 'popup_auth not-header',
          },
          true,
        );
        return;
      }
      setIsFetching(true);

      if (Number(group.mentor_id) === Number(user.user.id)) {
        await deleteGroup();

        dispatch(deleteUserReadingPlanSuccess());
      } else {
        await deleteUserReadingPlan(planId);
        await kickUser(user.user.id);
      }
      history.push('/reading-plan');
      setIsFetching(false);
    } catch (e) {
      setIsFetching(false);
    }
  };

  return (
    <CustomPlanBodyLayout
      bottomNode={
        isResult ? (
          <></>
        ) : (
          <CommentsPlan
            planId={chosenDay}
            groupId={group.id}
            mentorId={group?.mentor_id}
            chosenDay={chosenDay}
          />
        )
      }
      {...props}>
      {!isResult &&
        (isShowAlert ? (
          <MainPageGame />
        ) : (
          isVisibleReading && (
            <PlanDetailsVerses
              isFetching={isFetching}
              verses={verses}
              code={R.pathOr('', ['book', 'code'], verses[0])}
              legacyCode={R.pathOr('', ['book', 'legacy_code'], verses[0])}
              chapter={R.pathOr('', ['chapter'], verses[0])}
              setWidth={setWidthVerser}
              currentTranslate={currentTranslate}
            />
          )
        ))}
      {isResult && <ResultPageGame isLast={isLast} userPlans={userPlans} />}
      {!isAllRead && (
        <>
          {(isResult ? (
            quiz
          ) : (
            true
          )) ? (
            <div className="buttons-game">
              {(!isLast || !isResult) && isCheckOpenButton ? (
                correct && correct !== 10 && correct > 5 ? (
                  <>
                    <ButtonQuiz onPlayGame={onPlayGame} isResult={isResult} />
                    <div className="game__or">Или</div>
                  </>
                ) : isResult ? (
                  <>
                    <CustomButtonCP
                      onClick={() => handleNextChapter(true)}
                      className={'game__btn'}
                      color={'blue'}>
                      Перейти к следующей главе
                    </CustomButtonCP>
                    <div className="game__or">Или</div>
                  </>
                ) : (
                  <>
                    <ButtonQuiz onPlayGame={onPlayGame} isResult={isResult} />
                    {isCheckOpenButton ? (
                      <div className="game__or">Или</div>
                    ) : (
                      isLastChapterFormPlan && <div className="game__or">Или</div>
                    )}
                  </>
                )
              ) : (
                isLastChapterFormPlan && (
                  <>
                    <ButtonQuiz onPlayGame={onPlayGame} isResult={isResult} />
                    <div className="game__or">Или</div>
                  </>
                )
              )}
              {isCheckOpenButton ? (
                <CustomButtonCP
                  onClick={handleNextChapter}
                  className={'game__btn'}
                  color={(!isResult || correct < 6) && (!isLast || !isResult) ? 'gray' : 'blue'}>
                  {isResult && isLast
                    ? 'Вперед! В новый день'
                    : isShowAlert
                    ? 'Сдаюсь, перейти к следующей главе'
                    : isResult
                    ? nameButtonVictorine
                    : 'Перейти к следующей главе'}
                </CustomButtonCP>
              ) : (
                isLastChapterFormPlan && (
                  <CustomButtonCP
                    onClick={handleClosePlan}
                    className={'game__btn'}
                    color={(!isResult || correct < 6) && (!isLast || !isResult) ? 'gray' : 'blue'}>
                    Завершить план
                  </CustomButtonCP>
                )
              )}
              {!isLastChapterFormPlan && !activeTab?.is_closed && (
                <div className="game-label">
                  <label className="container-label" htmlFor="quiz-chapter">
                    <input
                      onChange={e => setUnread(e.target.checked)}
                      checked={isUnread}
                      type="checkbox"
                      id="quiz-chapter"
                    />
                    <span className="checkmark" />
                    Отметить эту главу, как непрочитанную
                  </label>
                </div>
              )}
            </div>
          ) : null}
        </>
      )}
    </CustomPlanBodyLayout>
  );
}

export const ButtonQuiz = ({ onPlayGame, isResult }) => {
  return (
    <>
      <CustomButtonCP
        onClick={onPlayGame}
        className="game__btn-first"
        color={isResult ? 'gray' : 'blue'}>
        <span>
          {isResult ? (
            <>
              Ответить на 10 вопросов и <br />
              улучшить свои результаты{' '}
            </>
          ) : (
            <>
              Проверьте&nbsp;знания:&nbsp;ответьте <br /> на 10 вопросов по главе
            </>
          )}
        </span>
        <svg
          className="arrow-btn-first"
          width="10"
          height="14"
          viewBox="0 0 10 14"
          fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path d="M0.839844 1L7.83984 6.93939L0.839844 12.8788" stroke="#243737" strokeWidth="2" />
        </svg>
      </CustomButtonCP>
    </>
  );
};

const mapStateToProps = (state, ownProps) => {
  const planId = ownProps.match.params.plan_id;
  return {
    userRPGetIsLoading: userRPGetIsLoading(state),
    user: userData(state),
    activeTab: activeTabUserRP(state),
    quiz: completedQueezePlan(state),
    group: userGroup(state),
    mentorGroup: mentorGroup(state),
    userPlans: getUserPlans(state, planId),
    currentTranslate: getCurrentTranslateCodeLk(state),
    isPhone: getIsMobile(state),
    screen: getScreen(state),
    planId,
  };
};

const mapDispatchToProps = {
  ...readingPlanActions,
  setActiveTab,
  getQuizByPlanId,
  getCommentsPlanByDay,
  getMentorGroup,
  getCurrentQueeze,
  getUserGroup,
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomPlanBody);
