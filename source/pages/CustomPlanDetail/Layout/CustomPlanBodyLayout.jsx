import React, { useEffect } from 'react';
import CustomPlanMain from '~components/CustomPlanModule/Main';
import '../CustomPlanBody/styles.scss';
import Chapters from '~components/CustomPlanModule/Chapters';
import { useMedia } from 'react-use';
import { activeTabUserRP, getUserPlans } from '../../../dist/selectors/ReadingPlan';
import ReadingProgressBar from '~components/ReadingProgressBar/ReadingProgressBar';
import { connect } from 'react-redux';
import readingPlanActions from '../../../dist/actions/ReadingPlan';
import SidebarCustomPlan from '~components/CustomPlanModule/Sidebar';
import { userGroup } from '~store/groups/selectors';
import { getName } from '~components/PlanAddUsers';
import { userLoggedIn } from '~dist/selectors/User';
import BibleMediaContentRight from '~components/Bible/BibleMediaContentRight';
import { getChapter } from '~dist/actions/Chapter';
import { getChapter as chapter } from '~dist/selectors/Chapter';
import getScreen from '../../../dist/selectors/screen';
import { useLocation } from 'react-router-dom';
import { getTogglePlan } from '~dist/selectors/ReadingPlan';

const CustomPlanBodyLayout = ({
  children,
  bottomNode,
  screen,
  toggleOpenPlan,
  path,
  userLoggedIn,
  userPlans: userPlansData,
  userPlansParent,
  getChapter,
  chapter,
  group,
  activeTab,
}) => {
  const isTablet = useMedia('(max-width: 1024px)');
  const location = useLocation();
  useEffect(() => {
    if (activeTab) {
      (async () => {
        await getChapter(activeTab?.book.code, activeTab?.chapter);
      })();
    }
  }, [activeTab]);
  const userPlans = userPlansData?.reading ? userPlansData : userPlansParent;
  const readings = userPlans?.reading?.flat(1);
  const mentor = group?.users?.find(user => user.id === group.mentor_id);

  const isMobile = useMedia('(max-width: 768px)');
  return (
    <div className="cpd__layout">
      {!isTablet && (
        <div className="cpd__aside">
          <SidebarCustomPlan group={group} path={path} userPlans={userPlans} />
        </div>
      )}
      <div className="main__container">
        {getName(mentor) && <div className="cpd__admin">Ведущий: {getName(mentor)}</div>}
        <div className="cpd-wrapper_main">
          {isTablet && !isMobile && <SidebarCustomPlan userPlans={userPlans} />}
          <div className="content__wrapper">
            {isMobile && (
              <>
                <ReadingProgressBar userPlans={userPlans} className={'cpd_progress'} />
              </>
            )}
            {isMobile && readings?.length > 0 && (
              <Chapters className={'chapters__top'} chapters={readings} />
            )}
            {isMobile && (
              <BibleMediaContentRight
                chapter={chapter?.book?.chapters?.[0]}
                bookCode={activeTab?.book?.code}
                activeItem={null}
                baseUrl="bible"
                locationCustom={location}
                withoutScroll
                screen={screen}
                userLoggedIn={userLoggedIn}
                extendLayout={{ content: false, left: false, right: false }}
              />
            )}
            {toggleOpenPlan ? <CustomPlanMain>{children}</CustomPlanMain> : null}
            {bottomNode}
          </div>
        </div>
      </div>
      {!isMobile && (
        <BibleMediaContentRight
          chapter={chapter?.book?.chapters?.[0]}
          bookCode={activeTab?.book?.code}
          withoutScroll
          activeItem={null}
          baseUrl="bible"
          locationCustom={location}
          screen={screen}
          userLoggedIn={userLoggedIn}
          extendLayout={{ content: false, left: false, right: false }}
        />
      )}
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  const planId = ownProps.match?.params?.plan_id;
  return {
    userPlans: getUserPlans(state, planId),
    userLoggedIn: userLoggedIn(state),
    chapter: chapter(state),
    activeTab: activeTabUserRP(state),
    group: userGroup(state),
    toggleOpenPlan: getTogglePlan(state),
    screen: getScreen(state),
  };
};

const mapDispatchToProps = {
  ...readingPlanActions,
  getChapter,
};
export default connect(mapStateToProps, mapDispatchToProps)(CustomPlanBodyLayout);
