import React from 'react';
import { Helmet } from 'react-helmet/es/Helmet';
import BreadHead from '../../components/BreadHead';
import CustomPlanBody from './CustomPlanBody';
import ReadingPageTitle from '~components/PageTitle';
import { useMedia } from 'react-use';
import { useSelector } from 'react-redux';
import { userPlans } from '~dist/selectors/ReadingPlan';

const CustomPlanDetail = ({ ...props }) => {
  const isMobile = useMedia('(max-width: 768px)');
  const userPlan = useSelector(userPlans);

  const TITLE_READING_PLAN = Object.values(userPlan)[0]?.plan?.description;
  const bread = [
    { path: '/', title: 'Главная' },
    { path: '/reading-plan', title: 'План чтений' },
    { path: '', title: TITLE_READING_PLAN },
  ];
  const breadMobile = [
    { path: '/', title: 'Главная' },
    { path: '', title: TITLE_READING_PLAN },
  ];
  return (
    <div>
      <Helmet title={TITLE_READING_PLAN} description={''} keywords={''} />
      <div className="page">
        <div className="wrapper wrapper-main">
          <BreadHead bread={isMobile ? breadMobile : bread} isDisabledLearnVideo />
          <ReadingPageTitle>{TITLE_READING_PLAN}</ReadingPageTitle>
          <CustomPlanBody {...props} />
        </div>
      </div>
    </div>
  );
};

export default CustomPlanDetail;
