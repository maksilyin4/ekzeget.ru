import React from 'react';
import { Helmet } from 'react-helmet/es/Helmet';
import BreadHead from '../../components/BreadHead';
import PlanAddUsers from '../../components/PlanAddUsers';
import './styles.scss';
import ReadingPageTitle from '~components/PageTitle';
import { useMedia } from 'react-use';
import { userGroup, userGroupLoading } from '../../store/groups/selectors';
import { connect, useSelector } from 'react-redux';
import { userPlans } from '~dist/selectors/ReadingPlan';
import Preloader from '~components/Preloader/Preloader';
import { userData } from '~dist/selectors/User';

const ReadingPlanMonitoring = ({ userGroup, userPlans, isJoin }) => {
  const userMe = useSelector(userData);
  const nameTitle =
    userMe?.user?.id === userGroup?.mentor_id ? 'Присоединение участников' : 'Статистика группы';
  const TITLE_READING_PLAN = `${nameTitle}: ${
    isJoin ? userGroup?.plan?.description : Object.values(userPlans)?.[0]?.plan?.description
  }`;
  const bread = [
    { path: '/', title: 'Главная' },
    { path: '/reading-plan', title: 'План чтений' },
    { path: '', title: nameTitle },
  ];
  const isMobile = useMedia('(max-width: 768px)');
  const breadMobile = [
    { path: '/', title: 'Главная' },
    { path: '', title: nameTitle },
  ];

  return (
    <>
      {!userGroup.id && <Preloader />}
      <div>
        <Helmet title={TITLE_READING_PLAN} description={''} keywords={''} />
        <div className="page">
          <div
            style={{ visibility: !userGroup.id ? 'hidden' : 'visible' }}
            className="wrapper wrapper-main">
            <BreadHead bread={isMobile ? breadMobile : bread} isDisabledLearnVideo />
            <ReadingPageTitle>{TITLE_READING_PLAN}</ReadingPageTitle>
            <PlanAddUsers isJoin={isJoin} />
          </div>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = state => ({
  userGroup: userGroup(state),
  userGroupLoading: userGroupLoading(state),
  userPlans: userPlans(state),
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(ReadingPlanMonitoring);
