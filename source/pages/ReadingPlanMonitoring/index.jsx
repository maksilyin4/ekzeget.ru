import Loadable from 'react-loadable';

const LoadableBar = Loadable({
  loader: () => import('./ReadingPlanMonitoring'),
  loading() {
    return null;
  },
  delay: 300,
});

export default LoadableBar;
