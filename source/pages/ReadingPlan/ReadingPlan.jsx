import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet/es/Helmet';
import BreadHead from '../../components/BreadHead';
import { smoothScrollTo } from '../../dist/utils';
import ReadingPlanContent from '../../pages/Lk/ReadingPlan/ReadingPlan';
import ReadingPlanDetail from '../Lk/ReadingPlanDetail/ReadingPlanDetail';
import ReadingPageTitle from '~components/PageTitle';
import { useParams, useHistory, Switch, Route } from 'react-router-dom';
import { decrypt } from '~utils/encrypte';
import { joinGroup } from '~dist/api/readingPlan/createGroup';
import Preloader from '~components/Preloader/Preloader';

const TITLE_READING_PLAN = 'План чтений Библии';
const LINK_READING_PLAN = '/reading-plan/';

const bread = [
  { path: '/', title: 'Главная' },
  {
    path: '',
    title: 'План чтений',
  },
];

const ReadingPlan = ({ isJoin }) => {
  const { iv, encryptedData } = useParams();
  const history = useHistory();
  React.useEffect(() => {
    smoothScrollTo();
  }, []);

  const handleJoin = async groupId => {
    try {
      await joinGroup(groupId);
      history.push('/reading-plan/group/');
    } catch (e) {
      console.error('Произошла ошибка при попытке подключиться к группе');
    }
  };

  useEffect(() => {
    if (isJoin && iv && encryptedData) {
      const idDecrypt = decrypt(iv, encryptedData);
      handleJoin(idDecrypt);
    }
  }, [iv, encryptedData]);

  return (
    <div>
      <Helmet title={TITLE_READING_PLAN} description={''} keywords={''} />
      <div className="page">
        <div className="wrapper">
          <BreadHead bread={bread} isDisabledLearnVideo />
          <ReadingPageTitle withMargin>{TITLE_READING_PLAN}</ReadingPageTitle>
          <Switch>
            <Route exact path={LINK_READING_PLAN} component={ReadingPlanContent} />
            <Route path={`${LINK_READING_PLAN}:plan_id/day-:day`} component={ReadingPlanDetail} />
            {isJoin && <Preloader />}
          </Switch>
        </div>
      </div>
    </div>
  );
};

export default ReadingPlan;
