import React, { Fragment, Component } from 'react';

import HomeSlide from './HomeSlide';

class HomePromoRight extends Component {
  render() {
    const { dataSmallSlider } = this.props;

    return (
      <div className="promo__right">
        <div className="main-page__tiles">
          {dataSmallSlider.map(slider => (
            <Fragment key={slider.id}>
              {slider.type === 'small_lu' && (
                <div className="slider__small_lu right-slider__container">
                  <HomeSlide slider={slider} isSmall isBig={false} />
                </div>
              )}
              {slider.type === 'small_ru' && (
                <div className="slider__small_ru right-slider__container">
                  <HomeSlide slider={slider} isSmall isBig={false} />
                </div>
              )}
              {slider.type === 'small_ld' && (
                <div className="slider__small_ld right-slider__container">
                  <HomeSlide slider={slider} isSmall isBig={false} />
                </div>
              )}
              {slider.type === 'small_rd' && (
                <div className="slider__small_rd right-slider__container">
                  <HomeSlide slider={slider} isSmall isBig={false} />
                </div>
              )}
            </Fragment>
          ))}
        </div>
      </div>
    );
  }
}

export default HomePromoRight;
