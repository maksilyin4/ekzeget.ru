import React from 'react';

import PageHead from '../../../components/PageHead';

import { useAtom } from '@reatom/react';
import { metaHomeAtom } from './model';

export const PageHeadMeta = () => {
  const [{ meta }] = useAtom(metaHomeAtom);

  React.useEffect(() => {
    if (meta && Object.values(meta).length === 0) {
      metaHomeAtom.init();
    }
  }, []);

  return (
    <PageHead
      h_one="БИБЛИЯ ОНЛАЙН"
      meta={meta}
      subTitle="Библия, Евангелие, Новый и Ветхий Завет  с толкованиями Святых отцов. Аудио Библия, Библейская викторина - игра и Библейская медиатека."
    />
  );
};
