import { createAtom } from '@reatom/core';
import { ssrReAtomAtom } from '../../../model';
import { getMetaData } from '../../../dist/utils';

const initStateMeta = { isLoadingMeta: false, meta: null, metaContext: null };

export const metaHomeAtom = createAtom(
  { ssrReAtomAtom, init: () => {}, success: d => d, error: () => null, setMetaHomeSSR: m => m },
  (
    { onAction, get, schedule, create },
    state = get('ssrReAtomAtom')?.metaHomeAtom || initStateMeta,
  ) => {
    onAction('init', () => {
      state = { ...state, isLoadingMeta: true };
      schedule(dispatch => {
        getMetaData('main')
          .then(meta => {
            if (meta || meta !== 'Error') {
              dispatch(create('success', meta));
            }
          })
          .catch(e => dispatch(create('error', e)));
      });
    });

    onAction('success', meta => (state = { ...meta, isLoadingMeta: false }));
    onAction('setMetaHomeSSR', meta => (state = { ...initStateMeta, meta, isLoadingMeta: false }));

    return state;
  },
);
