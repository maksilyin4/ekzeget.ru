import R from 'ramda';
import dayjs from 'dayjs';
import ruLocale from 'dayjs/locale/ru';
import { _AXIOS } from '../../dist/ApiConfig';

export default function getLatestInterpretations() {
  return _AXIOS({ url: 'interpretation/?sort=1&page=1&per-page=10' })
    .then(({ interpretations, pages }) => ({
      isFetching: false,
      total: pages.totalCount,
      latestInterpretationsList: interpretations.map(inter => ({
        id: inter.id,
        username: inter.added_by.username,
        date: dayjs
          .unix(inter.added_at)
          .locale('ru', ruLocale)
          .format('DD MMM YYYY'),
        verse: inter.verse[0],
        author: R.pathOr('', ['ekzeget', 'name'], inter),
        authorId: R.pathOr(0, ['ekzeget', 'id'], inter),
        investigated: inter.investigated,
      })),
    }))
    .catch(() => ({
      isFetching: false,
    }));
}
