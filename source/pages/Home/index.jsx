import React from 'react';
import Loadable from 'react-loadable';

const LoadableBar = Loadable({
  loader: () => import('./Home'),
  loading: () => null,
  render(loaded, props) {
    const Component = loaded.default;
    return <Component directProps={props.directProps} initialState={props.initialState} />;
  },
  delay: 300,
});

export default LoadableBar;
