import React from 'react';
import Button from '../../components/Button/Button';
import { useSelector } from 'react-redux';
import { getUserPlanId } from '../../dist/selectors/ReadingPlan';

export const SubscribeReadingPlan = () => {
  const userReadingPlan = useSelector(getUserPlanId);
  // запрос делается в HeaderNav
  return (
    <Button
      className="subscribe-reading-plan"
      title="Прочитать Библию за год - подпишись на рассылку"
      href={`/reading-plan/${userReadingPlan}`}>
      Прочитать Библию за год - подпишись на рассылку
    </Button>
  );
};
