import R from 'ramda';
import LazyLoad from 'react-lazyload';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { _AXIOS } from '../../dist/ApiConfig';
import { smoothScrollTo } from '../../dist/utils';
import BooksList from '../../components/BooksList';
import LectionInDay from '../../components/LectionInDay/LectionInDay';
// import ReadingGospelAudio from '../../components/ReadingGospelAudio/ReadingGospelAudio';
import LatestNews from '../../components/LatestNews/LatestNews';
import LatestInterpritations from '../../components/LatestInterpritations/LatestInterpritations';
import HomeSwiper from './HomeSwiper';
import TotalInterpretations from './TotalInterpretations';

import { HomeText } from '../../components/HomeText';
import { setClearMeta } from '../../store/metaData/actions';
// import { SubscribeReadingPlan } from './subscribe-reading-plan';
import { PageHeadMeta } from './page-head-meta/page-head-meta';

import './home.scss';
import HomePromoSlider from '~pages/Home/HomePromoSlider';

// directProps - SSR Parse, initialState - render oO
// TODO Переделать

class Home extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      // isFetching: false,
      latestNewsList: R.pathOr([], ['directProps', 'latestNewsList'], this.props),
      sliderBigData: [],
      isLoading: true,
      homeText: this.props.initialState?.text ? this.props.initialState?.text?.text : null,
    };

    if (this.props && this.props.directProps) {
      if ('latestSlider' in this.props.directProps) {
        this.state.isLoading = false;
        this.state.sliderBigData = this.props.directProps.latestSlider;
      }
    } else if (this.props && this.props.initialState) {
      if ('latestSlider' in this.props.initialState) {
        this.state.isLoading = false;
      }
    }
  }

  componentDidMount() {
    const { initialState, directProps } = this.props;

    smoothScrollTo();

    this.getSliderData(initialState, directProps);
    this.getHomeText();
  }

  getSliderData = (data, propData) => {
    if ('latestSlider' in data) {
      this.getStateSlider(data.latestSlider);
    } else if ('latestSlider' in propData) {
      return true;
    } else {
      _AXIOS({
        url:
          '/slider',
      })
        .then(data => this.getStateSlider(data.sliders))
        .catch(() => {
          this.setState({
            isLoading: true,
          });
        });
    }
  };

  getHomeText = () => {
    if (!this.state.homeText) {
      _AXIOS({
        url:
          '/textBlock/main/',
      })
        .then(data => this.getStateText(data))
        .catch(() => {
          this.setState({
            isLoading: true,
          });
        });
    }
  }

  getStateSlider = data => {
    this.setState({
      sliderBigData: data,
      isLoading: false,
    });
  };

  getStateText = data => {
    this.setState({
      homeText: data.text ? data.text.text : null,
      isLoading: false,
    });
  }

  render() {
    const { latestNewsList, isLoading } = this.state;
    const { initialState } = this.props;

    return (
      <div className="main-section">
        <div className="wrapper home-wrapper">
          <div style={{ borderBottom: '1px solid #F0F0F0' }} className="promo">
            {isLoading ? null : (
              <>
                <HomeSwiper
                  dataBigSlider={this.state.sliderBigData}
                />
                <HomePromoSlider />
              </>
            )}
          </div>

          <div style={{ width: '100%' }}>
            <PageHeadMeta />
            <div className="main-page__bible">
              <BooksList initialState={initialState} />
            </div>
          </div>

          <div className="main-page__rga">
            <HomeText text={this.state.homeText}/>
          </div>

          <div className="main-page__lid">
            <LectionInDay />
          </div>

          <LazyLoad height={300}>
            <div className="main-page__news">
              <LatestNews latestNewsList={latestNewsList} initialState={initialState} />
            </div>
          </LazyLoad>

          <div className="main-page__total-interp">
            <TotalInterpretations total={initialState?.latestInterpretationsTotal} />
          </div>

          <div className="main-page__updates">
            <LatestInterpritations
              latestInterpretationsList={initialState?.latestInterpretationsList}
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = () => ({});
const mapDispatchToProps = {
  setClearMeta,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
