import React, { memo, useState } from 'react';
import Slick from 'react-slick';
import loadable from '@loadable/component';

import '../../assets/sass/slider.scss';
import useMainMediasQueryVideo from '~apollo/query/mediaLibrary/mediasMainVideo';

import arrowLeft from '../../assets/icons/arrow_left.svg';
import arrowRight from '../../assets/icons/arrow_right.svg';

const sliderSettings = {
  infinite: false,
  speed: 500,
  slidesToShow: 5,
  slidesToScroll: 5,
  prevArrow: <PrevArrow />,
  nextArrow: <NextArrow />,
  responsive: [
    {
      breakpoint: 1023,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        swipeToSlide: true,
        arrows: true,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        swipeToSlide: true,
        arrows: true,
      },
    },
    {
      breakpoint: 425,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        swipeToSlide: true,
        arrows: true,
      },
    },
  ],
};

function PrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <img src={arrowLeft} className={className} alt="" style={{ ...style }} onClick={onClick} />
  );
}

function NextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <img src={arrowRight} className={className} alt="" style={{ ...style }} onClick={onClick} />
  );
}

const ExternalVideo = loadable(() => import('~components/ExternalVideo'), { ssr: false })

const HomePromoSlider = () => {
  const [offset, setOffset] = useState(0);
  const [limit, setLimit] = useState(30);
  const [isInitialized, setInitialized] = useState(false);

  const { medias = [], refetch } = useMainMediasQueryVideo({
    offset,
    limit,
    category_code: 'biblia-otvecaet',
    'orderBy': 'created_at'
  });

  function fetchVideo(current, next) {
    const countSlides = next - current;
    if (isInitialized && countSlides > 0 && current + countSlides * 3 > limit) {
      const offsetSlides = offset + countSlides;
      const limitSlides = Math.abs(limit + countSlides * 3);
      refetch({
        offset: offsetSlides,
        limit: limitSlides,
        category_code: 'video',
        'orderBy': 'created_at'
      });
      setOffset(offsetSlides);
      setLimit(limitSlides);
    } else {
      setInitialized(true);
    }
  }

  return (
    <div className="promo promo__slider">
      <div className="main-page__h3">Библия отвечает</div>
      <Slick
        className="main-page__slider"
        beforeChange={(current, next) => fetchVideo(current, next)}
        {...sliderSettings}>
        {medias.map(slides => {
          return (
            <div key={slides.video_href} className={'main-page__slide-video'}>
              <ExternalVideo data={slides.video_href} />
            </div>
          );
        })}
      </Slick>
    </div>
  );
};

export default memo(HomePromoSlider);
