import React from 'react';
import cx from 'classnames';
import dayjs from 'dayjs';
import LazyLoad from 'react-lazyload';

const HomeSliderVideo = ({ isBig, isSmall, isVideoActive, getActiveVideo, slider }) => (
  <div
    className={cx({
      'video-newSite': isBig,
      'video-newSite-right': isSmall,
      'video-activated': isVideoActive,
    })}
    itemScope
    itemType="http://schema.org/VideoObject"
    onClick={getActiveVideo}>
    <div className="video-schema-meta">
      <meta
        itemProp="description"
        content={
          slider.description.length
            ? slider.description
            : `Библейское видео ${slider.title} на Экзегет.ру`
        }
      />
      <link itemProp="url" href={slider.video_url} />
      <link
        itemProp="thumbnailUrl"
        href={`https://i.ytimg.com/vi/${slider.video_url.split('/')[4]}/mqdefault.jpg`}
      />
      <meta
        itemProp="name"
        content={slider.title.length ? slider.title : 'Библейское видео на Экзегет.ру'}
      />
      <meta
        itemProp="uploadDate"
        content={dayjs.unix(slider.created_at).format('YYYY-MM-DDTHH:mm:ss')}
      />
      <meta itemProp="isFamilyFriendly" content="true" />
      <span
        className="video-schema__hidden"
        itemProp="thumbnail"
        itemScope
        itemType="http://schema.org/ImageObject">
        <img
          itemProp="contentUrl"
          src={`https://i.ytimg.com/vi/${slider.video_url.split('/')[4]}/mqdefault.jpg`}
          alt={slider.title}
        />
        <meta itemProp="width" content="300px" />
        <meta itemProp="height" content="300px" />
      </span>
    </div>
    <LazyLoad height={120}>
      {isVideoActive ? (
        <iframe
          title="Видео"
          width="560"
          height="340"
          src={`${slider.video_url}?rel=0&showinfo=0&autoplay=1`}
          frameBorder="0"
          allow="autoplay; encrypted-media"
          allowFullScreen="allowFullScreen"
        />
      ) : (
        <div className="video-container" id={slider.video_url.split('/')[4]}>
          {isBig ? (
            <picture>
              <source
                srcSet={`https://i.ytimg.com/vi_webp/${
                  slider.video_url.split('/')[4]
                }/sddefault.webp`}
                type="image/webp"
              />
              <img
                className="video__media"
                src={`https://i.ytimg.com/vi/${slider.video_url.split('/')[4]}/sddefault.jpg`}
                alt="Экзегет Видео"
              />
            </picture>
          ) : (
            <picture>
              <source
                media="(min-width: 769px)"
                srcSet={`https://i.ytimg.com/vi_webp/${
                  slider.video_url.split('/')[4]
                }/sddefault.webp`}
                type="image/webp"
              />
              <source
                srcSet={`https://i.ytimg.com/vi_webp/${
                  slider.video_url.split('/')[4]
                }/mqdefault.webp`}
                type="image/webp"
              />
              <source
                media="(min-width: 769px)"
                srcSet={`https://i.ytimg.com/vi/${slider.video_url.split('/')[4]}/sddefault.jpg`}
              />
              <img
                className="video__media"
                src={`https://i.ytimg.com/vi/${slider.video_url.split('/')[4]}/mqdefault.jpg`}
                alt="Экзегет Видео"
              />
            </picture>
          )}

          <button className="video__button" type="button" aria-label="Запустить видео">
            <svg width="68" height="48" viewBox="0 0 68 48">
              <path
                className="video__button-shape"
                d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z"
              />
              <path className="video__button-icon" d="M 45,24 27,14 27,34" />
            </svg>
          </button>
        </div>
      )}
    </LazyLoad>
  </div>
);

export default HomeSliderVideo;
