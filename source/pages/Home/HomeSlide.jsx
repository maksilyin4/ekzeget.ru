import React, { PureComponent } from 'react';
import cx from 'classnames';

import { urlToAPI } from '../../dist/browserUtils';

import HomeSliderVideo from './HomeSliderVideo';
import HomeSlidePicture from './HomeSlidePicture';

class HomeSlide extends PureComponent {
  constructor(props) {
    super(props);
    const { slider } = this.props;

    this.state = {
      url: urlToAPI,
      imgPath: null,
      currentUrl: '',
      isVideoActive: false,
    };

    if (slider.image && slider.image.length) {
      this.state.imgPath =
        slider.image_optimized && Object.keys(slider.image_optimized).length
          ? slider.image_optimized
          : slider.image;
    } else if (slider.article_image && slider.article_image.length) {
      this.state.imgPath =
        slider.article_image_optimized && Object.keys(slider.article_image_optimized).length
          ? slider.article_image_optimized
          : slider.article_image;
    }

    if (slider.url && slider.url.length) {
      this.state.currentUrl = slider.url;
    } else if (slider.article_id) {
      this.state.currentUrl = `${this.state.url}/novosti-i-obnovleniya/novosti/${slider.article_id}`;
    }
  }

  componentDidUpdate(prevProps) {
    const { isNextVideo } = this.props;

    if (prevProps.isNextVideo !== isNextVideo && isNextVideo) {
      this.setState({ isVideoActive: false });
    }
  }

  getActiveVideo = () => {
    this.setState({ isVideoActive: true });
    this.props.getNextVideo(false);
  };

  render() {
    const { slider, isBig, isSmall } = this.props;
    const { url, imgPath = {}, currentUrl, isVideoActive } = this.state;

    return slider.video_url.length ? (
      <div
        className={cx({
          'main-page__tile main-page-video': isSmall,
          'main-page__slide': isBig,
          'main-page__slide-video': isBig && slider.video_url.length,
        })}>
        <HomeSliderVideo
          isBig={isBig}
          isSmall={isSmall}
          isVideoActive={isVideoActive}
          getActiveVideo={this.getActiveVideo}
          slider={slider}
        />
      </div>
    ) : currentUrl.length ? (
      <a
        className={cx({
          'main-page__tile': isSmall,
          'main-page__slide': isBig,
        })}
        href={currentUrl}
        title={String(slider.title)}>
        <HomeSlidePicture
          imgPath={imgPath}
          url={url}
          slider={slider}
          isSmall={isSmall}
          isBig={isBig}
          defaultTypeSlider={imgPath && (isBig ? imgPath.big_slider : imgPath.small_slider)}
          mobTypeSlider={imgPath && (isBig ? imgPath.big_slider_mob : imgPath.small_slider_mob)}
        />
      </a>
    ) : (
      <div
        className={cx({
          'main-page__tile': isSmall,
          'main-page__slide': isBig,
        })}>
        <HomeSlidePicture
          imgPath={imgPath}
          url={url}
          slider={slider}
          isSmall={isSmall}
          isBig={isBig}
          defaultTypeSlider={imgPath && (isBig ? imgPath.big_slider : imgPath.small_slider)}
          mobTypeSlider={imgPath && (isBig ? imgPath.big_slider_mob : imgPath.small_slider_mob)}
        />
      </div>
    );
  }
}

export default HomeSlide;
