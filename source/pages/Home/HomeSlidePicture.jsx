import React from 'react';
import cx from 'classnames';
import LazyLoad from 'react-lazyload';

const HomeSlidePicture = ({
  imgPath,
  url,
  slider,
  isSmall,
  isBig,
  defaultTypeSlider,
  mobTypeSlider,
}) => (
  <>
    {imgPath &&
      (typeof imgPath === 'object' && Object.keys(imgPath).length > 0 ? (
        <LazyLoad height={180}>
          <picture className="slider__picture">
            {defaultTypeSlider.webp && (
              <>
                <source
                  media="(min-width: 769px)"
                  srcSet={`${url}${defaultTypeSlider.webp}`}
                  type="image/webp"
                />
                <source srcSet={`${url}${mobTypeSlider.webp}`} type="image/webp" />
              </>
            )}
            <source media="(min-width: 769px)" srcSet={`${url}${defaultTypeSlider.original}`} />
            <img
              srcSet={`${url}${mobTypeSlider.original}`}
              alt={String(slider.title)}
              title={String(slider.title)}
              className="slider__responsive-img"
            />
          </picture>
        </LazyLoad>
      ) : (
        <LazyLoad height={180}>
          <img
            srcSet={`${url}${imgPath}`}
            alt={String(slider.title)}
            title={String(slider.title)}
            className="slider__responsive-img"
          />
        </LazyLoad>
      ))}
    <div
      className={cx({
        'main-page__container': isSmall,
        'main-page__text-container': isBig,
        'main-page__container-empty': !slider.title.length && !slider.description.length,
      })}>
      {slider.title.length > 0 && (
        <p
          className={cx({
            'main-page__tile-title': isSmall,
            'main-page__slide-title': isBig,
          })}>
          {slider.title}
        </p>
      )}
      {slider.description.length > 0 && (
        <p
          className={cx({
            'main-page__tile-description': isSmall,
            'main-page__slide-desc': isBig,
          })}
          dangerouslySetInnerHTML={{ __html: slider.description }}
        />
      )}
    </div>
  </>
);

export default HomeSlidePicture;
