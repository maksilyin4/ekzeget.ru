import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { declOfNum } from '../../dist/utils';
import './totalInterpretations.scss';

class TotalInterpretations extends Component {
  render() {
    const { total = 0 } = this.props;

    return (
      <div className="latest-interpritations__totalCount">
        <div className="latest-interpritations__totalCount__text">
          <p>Сейчас на сайте размещено: </p>
        </div>
        <div className="latest-interpritations__totalCount__numbers">
          <div>
            <p>{String(total).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')}</p>
            <span>
              {`${declOfNum(total, [
                'толкование',
                'толкования',
                'толкований',
              ])} на Священное Писание`}
            </span>
          </div>
        </div>
      </div>
    );
  }
}

TotalInterpretations.propTypes = {
  total: PropTypes.number.isRequired,
};

export default TotalInterpretations;
