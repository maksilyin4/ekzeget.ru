import React from 'react';
import Loadable from 'react-loadable';
import Preloader from '../../components/Preloader/Preloader';

const LoadableBar = Loadable({
  loader: () => import('./TotalInterpretations'),
  loading() {
    return (
      <div>
        <Preloader />
      </div>
    );
  },
  render(loaded, props) {
    const Component = loaded.default;
    return <Component total={props.total} />;
  },
  delay: 300,
});

export default LoadableBar;
