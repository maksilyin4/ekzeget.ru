import React, { memo } from 'react';
import Slick from 'react-slick';
import loadable from '@loadable/component';

import '../../assets/sass/slider.scss';

import arrowLeft from '../../assets/icons/arrow_left.svg';
import arrowRight from '../../assets/icons/arrow_right.svg';

const ExternalVideo = loadable(() => import('~components/ExternalVideo'), { ssr: false })

const sliderSettings = {
  infinite: false,
  speed: 500,
  slidesToShow: 2,
  prevArrow: <PrevArrow />,
  nextArrow: <NextArrow />,
  responsive: [
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        swipeToSlide: true,
        arrows: true,
      },
    },
  ],
};

function PrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <img src={arrowLeft} className={className} alt="" style={{ ...style }} onClick={onClick} />
  );
}

function NextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <img src={arrowRight} className={className} alt="" style={{ ...style }} onClick={onClick} />
  );
}

const HomeSwiper = ({ dataBigSlider }) => {
  return (
    <>
      <Slick className="main-page__slider" {...sliderSettings}>
        {dataBigSlider.sort((a, b) => {
          if (a?.type[0] < b?.type[0]) return -1;
          if (a?.type[0] > b?.type[0]) return 1;
          return 0;
        }).map(slider => (
          <div key={slider.id} className={'main-page__slide-video'}>
            <ExternalVideo data={slider.video_url} />
          </div>
        ))}
      </Slick>
    </>
  );
};

export default memo(HomeSwiper);
