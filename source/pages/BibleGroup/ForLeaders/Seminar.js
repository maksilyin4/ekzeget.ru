import React, { useState, useEffect, useMemo } from 'react';
import { Link } from 'react-router-dom';

import { forLeadersDefault, forLeadersTypes } from './commonForLeadersTypes';

const Seminar = ({ list }) => {
  const [sortedList, setSortedList] = useState([]);

  useEffect(() => {
    setSortedList(
      [...list].sort((a, b) => Number(a.title.slice(0, 3)) - Number(b.title.slice(0, 3))),
    );
  }, [list]);

  return useMemo(
    () => (
      <div className="bible-group__content">
        <div className="bible-content__title">
          <h3>Материалы семинара для ведущих</h3>
          <p>Семинар под эгидой Миссионерско-катехизаторской комиссии г. Москвы</p>
        </div>
        <ul className="bible-group__links">
          {sortedList.map(({ code, title }) => (
            <li key={code}>
              <Link to={`/bible-group/for-leaders/${code}/`}>{title}</Link>
            </li>
          ))}
        </ul>
      </div>
    ),
    [sortedList],
  );
};

Seminar.defaultProps = forLeadersDefault;

Seminar.propTypes = forLeadersTypes;

export default Seminar;
