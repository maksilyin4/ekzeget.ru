import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { forLeadersDefault, forLeadersTypes } from './commonForLeadersTypes';

export default class Experience extends Component {
  render() {
    const { list } = this.props;

    return (
      <div className="bible-group__content">
        <div className="bible-content__title">
          <h3>Опыт и вопросы</h3>
        </div>
        <ul className="bible-group__links">
          {list.map(item => (
            <li key={item.code}>
              <Link to={`/bible-group/for-leaders/${item.code}/`}>{item.title}</Link>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

Experience.defaultProps = forLeadersDefault;

Experience.propTypes = forLeadersTypes;
