import PropTypes from 'prop-types';

export const forLeadersTypes = {
  list: PropTypes.arrayOf(
    PropTypes.shape({
      code: PropTypes.string,
      title: PropTypes.string,
    }),
  ),
};

export const forLeadersDefault = {
  list: [],
};
