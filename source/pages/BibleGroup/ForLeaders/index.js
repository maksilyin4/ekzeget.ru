import React, { useMemo, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';

import { smoothScrollTo } from '../../../dist/utils';

import Wrapper from './Wrapper';
import Seminar from './Seminar';
import Beginners from './Beginners';
import Chavo from './Chavo';
import Experience from './Experience';
import Preloader from '../../../components/Preloader/Preloader';

const GroupLeaders = ({ location: { search }, isFetch, beginners, seminar, experience, chavo }) => {
  const [fromChavo] = useState(/from_chavo/g.test(search));

  useEffect(() => {
    if (!/from_chavo/g.test(search)) {
      smoothScrollTo();
    }
  }, [search]);

  return useMemo(
    () => (
      <div className="content bible-group content__paper container">
        <Helmet title="Материалы для ведущих || Экзегет.ру" />
        {isFetch && <Preloader />}
        <Seminar list={seminar} />
        <Beginners list={beginners} />
        <Experience list={experience} />
        <Chavo list={chavo} fromChavo={fromChavo} />
      </div>
    ),
    [beginners, chavo, experience, fromChavo, isFetch, seminar],
  );
};

export default Wrapper(GroupLeaders);
