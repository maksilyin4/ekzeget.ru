import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { _AXIOS } from '../../../dist/ApiConfig';
import { alphSort } from '../../../dist/utils';
import { bibleGroupNotes } from '../../../dist/selectors/BibleGroupUpdates';

const leadersWrapper = WrappedComponent =>
  class WrapperForLeaders extends Component {
    constructor(props) {
      super(props);

      this.state = {
        isFetch: false,
        beginners: [],
        seminar: [],
        experience: [],
        chavo: [],
      };

      // SSR
      if (props.groupNotes.length) {
        this.state.beginners = alphSort(props.groupNotes.filter(n => n.type === 1));
        props.groupNotes.forEach(note => {
          switch (note.type) {
            case 2:
              this.state.seminar.push(note);
              break;
            case 3:
              this.state.experience.push(note);
              break;
            case 4:
              this.state.chavo.push(note);
              break;
            default:
              break;
          }
        });
      }
    }

    componentDidMount() {
      if (!this.props.groupNotes.length) {
        this.setState({ isFetch: true });
        _AXIOS({ url: 'bible-group/note/' })
          .then(({ note }) => {
            this.setState({
              isFetch: false,
              beginners: alphSort(this.getFilteredNote(note, 1)),
              seminar: this.getFilteredNote(note, 2),
              experience: this.getFilteredNote(note, 3),
              chavo: this.getFilteredNote(note, 4),
            });
          })
          .catch(() => this.setState({ isFetch: false }));
      }
    }

    getFilteredNote = (note, type) => note.filter(n => n.type === type);

    render() {
      const { isFetch, beginners, seminar, experience, chavo } = this.state;
      return (
        <WrappedComponent
          isFetch={isFetch}
          beginners={beginners}
          seminar={seminar}
          experience={experience}
          chavo={chavo}
          {...this.props}
        />
      );
    }
  };

const mapStateToProps = state => ({
  groupNotes: bibleGroupNotes(state),
});

const forLeadersCompose = compose(connect(mapStateToProps, null), leadersWrapper);

export default forLeadersCompose;
