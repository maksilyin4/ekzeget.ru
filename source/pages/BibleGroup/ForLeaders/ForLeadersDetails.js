import R from 'ramda';
import React, { useEffect, useReducer, useMemo } from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';

import { smoothScrollTo } from '../../../dist/utils';
import { _AXIOS } from '../../../dist/ApiConfig';

import { getBibleGroupNotesUpdatesClear } from '../../../dist/actions/BibleGroupUpdates';
import { bibleGroupNoteInfo } from '../../../dist/selectors/BibleGroupUpdates';
import Preloader from '../../../components/Preloader/Preloader';
import { types, reducer } from './reducer';

import './forLeadersDetails.scss';

const ForLeadersDetails = ({ match: { params }, location }) => {
  const bibleNoteInfo = useSelector(bibleGroupNoteInfo);
  const dispatchRedux = useDispatch();

  const [state, dispatch] = useReducer(reducer, {
    isFetching: false,
    content: (!R.isEmpty(bibleNoteInfo) && bibleNoteInfo) || {},
  });

  useEffect(() => {
    smoothScrollTo();
    if (R.isEmpty(bibleNoteInfo)) {
      dispatch({ type: types.IS_FETCHING, isFetching: true });
      _AXIOS({ url: `bible-group/note-detail/${params.id}` })
        .then(({ note }) => {
          dispatch({
            type: types.UPDATE_ALL,
            completed: {
              isFetching: false,
              content: note,
            },
          });
        })
        .catch(() => dispatch({ type: types.IS_FETCHING, isFetching: false }));
    }
    return () => dispatchRedux(getBibleGroupNotesUpdatesClear());
  }, []);

  return useMemo(
    () => (
      <div className="content bible-group__details content__paper container">
        {!R.isEmpty(state.content) && (
          <div>
            <Helmet title={`${state.content.title} - Библейские группы || Экзегет.ру`} />
            <h3 className="bible-group__details__title">{state.content.title}</h3>
            <div
              className="bible-group__details__content"
              dangerouslySetInnerHTML={{ __html: state.content.text }}
            />
          </div>
        )}

        {state.isFetching && <Preloader />}

        {!!R.isEmpty(state.content) && !state.isFetching && (
          <div>
            <Helmet title="Такого материала нет в базе - Библейские группы || Экзегет.ру" />
            <p>Такого материала нет в базе</p>
          </div>
        )}

        <div className="bible-group__details__back">
          <Link
            to={
              /chavo/.test(location.search)
                ? '/bible-group/for-leaders?from_chavo'
                : '/bible-group/for-leaders'
            }>
            Вернуться к списку
          </Link>
        </div>
      </div>
    ),
    [state.content, state.isFetching, params.id],
  );
};

ForLeadersDetails.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }),
  }),
  location: PropTypes.shape({
    search: PropTypes.string,
  }),
};

export default ForLeadersDetails;
