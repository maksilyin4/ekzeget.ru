export const types = {
  UPDATE_ALL: 'UPDATE_ALL',
  IS_FETCHING: 'IS_FETCHING',
};

export const reducer = (state, action) => {
  const { isFetching, content, completed } = action;

  switch (action.type) {
    case types.UPDATE_ALL:
      return { ...state, ...completed };
    case types.IS_FETCHING:
      return { ...state, isFetching };
    case types.CONTENT:
      return { ...state, content };
    default:
      return state;
  }
};
