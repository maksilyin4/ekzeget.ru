import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { forLeadersDefault, forLeadersTypes } from './commonForLeadersTypes';

const question = require('../images/question.svg');
const arrow = require('../images/next.svg');

export default class Chavo extends Component {
  state = {
    isOpen: this.props.fromChavo,
  };

  handleOpen = () => {
    this.setState(({ isOpen }) => ({
      isOpen: !isOpen,
    }));
  };

  render() {
    const { list } = this.props;
    const { isOpen } = this.state;

    return (
      <div className="bible-group__chavo">
        <div className="bible-group__chavo__head">
          <div className="bible-group__chavo__head-img">
            <img src={question} alt="Question" />
          </div>
          <button className="bible-group__chavo__head-title" onClick={this.handleOpen}>
            <h3>Вопросы и ответы по ведению кружков</h3>
            <div className="bible-group-arrow">
              <img
                src={arrow}
                alt="arrow"
                style={{
                  transform: isOpen ? 'rotateZ(180deg)' : 'rotateZ(0)',
                }}
              />
            </div>
          </button>
        </div>
        {isOpen && (
          <ul className="bible-group__links">
            {list.map(item => (
              <li key={item.id}>
                <Link to={`/bible-group/for-leaders/${item.code}/?chavo`}>{item.title}</Link>
              </li>
            ))}
          </ul>
        )}
      </div>
    );
  }
}

Chavo.defaultProps = {
  ...forLeadersDefault,
  fromChavo: false,
};

Chavo.propTypes = {
  ...forLeadersTypes,
  fromChavo: PropTypes.bool,
};
