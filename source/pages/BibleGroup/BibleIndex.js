import React, { PureComponent } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { Popup } from '../../dist/browserUtils';
import { getMetaData, isMetaData, smoothScrollTo } from '../../dist/utils';

import BreadHead from '~components/BreadHead';
import PageHead from '~components/PageHead';
import BibleGroupNav from './BibleGroupNav';
import BibleGroupAbout from './About';
import ForLeaders from './ForLeaders';
import ForLeadersDetails from './ForLeaders/ForLeadersDetails';
import SearchGroup from './SearchGroup';
import VideoBibleGroup from './Video';
import BibleGroupQuestion from '../../components/Popups/BibleGroupQuestion';
import './styles.scss';
import { setClearMeta } from '../../store/metaData/actions';

class BibleGroup extends PureComponent {
  state = {
    meta: {
      h_one: 'Библейские группы',
    },
    h_one: 'Библейские группы',
    isLoad: true,
  };

  componentDidMount() {
    this.props.setClearMeta();
    smoothScrollTo();
    getMetaData('/bible-group/').then(meta => {
      if (meta || meta !== 'Error') {
        this.setState({
          meta,
          h_one: isMetaData('h_one', meta, 'Библейские группы'),
        });
      }
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const { meta } = this.state;
    const isData = isMetaData('h_one', meta, 'Библейские группы');
    if (isData !== prevState.h_one) {
      this.setState({ h_one: isData, isLoad: false });
    } else if (prevState.isLoad) this.setState({ isLoad: false });
  }

  _breadItems = () => [
    { path: '/', title: 'Главная' },
    { path: '', title: 'Библейские группы' },
  ];

  openPopUp = () => {
    Popup.create(
      {
        title: null,
        content: <BibleGroupQuestion />,
        className: 'chapter__bible-group__popup',
      },
      true,
    );
  };

  render() {
    const { history } = this.props;
    const { meta, isLoad } = this.state;

    return (
      <div className="page page__bible-group">
        <div className="wrapper">
          <BreadHead bread={this._breadItems()} />
          <PageHead
            meta={meta}
            h_one={'Библейские группы'}
            isLoad={isLoad}
            onClick={this.openPopUp}
          />
        </div>

        <div className="wrapper wrapper_two-col">
          <BibleGroupNav history={history} />
          <Switch>
            <Route exact path="/bible-group/about/" component={BibleGroupAbout} />
            <Route exact path="/bible-group/for-leaders/" component={ForLeaders} />
            <Route exact path="/bible-group/for-leaders/:id/" component={ForLeadersDetails} />
            <Route exact path="/bible-group/video/" component={VideoBibleGroup} />
            <Route exact path="/bible-group/search/" component={SearchGroup} />
            <Redirect to="/bible-group/about/" />
          </Switch>
        </div>
      </div>
    );
  }
}

const mapStateToProps = () => ({});
const mapDispatchToProps = {
  setClearMeta,
};

export default connect(mapStateToProps, mapDispatchToProps)(BibleGroup);
