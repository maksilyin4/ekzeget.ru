import R from 'ramda';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { getAllBibleGroups } from '../../dist/selectors/BibleGroupUpdates';
import { AXIOS } from '../../dist/ApiConfig';

const searchWrapper = WrappedComponent =>
  class WrapperForLeaders extends Component {
    constructor(props) {
      super(props);
      this.state = {
        isFetching: false,
        data: [],
        filter: '',
      };

      // SSR
      if (props.bibleGroups.length && props.bibleGroups.length > 4) {
        const filteredData = this.getData(props.bibleGroups);
        this.state.data = filteredData;
        this.state.countries = this.getFilteredData(filteredData);
      }
    }

    componentDidMount() {
      const { bibleGroups } = this.props;

      if (!bibleGroups.length || bibleGroups.length <= 4) {
        this.setState({ isFetching: true }, () =>
          AXIOS.get('bible-group').then(
            ({ data }) => {
              if (data.status === 'ok') {
                this.updateState(data.group);
              } else {
                this.setState({ isFetching: false });
              }
            },
            () => {
              this.setState({ isFetching: false });
            },
          ),
        );
      }
    }

    updateState = data => {
      const filteredData = this.getData(data);
      this.setState({
        isFetching: false,
        data: filteredData,
        countries: this.getFilteredData(filteredData),
      });
    };

    getData = data =>
      data.reduce((sum, g) => {
        let coordinates = g.coords;
        if (g.coords) {
          const splitedCoords = g.coords.split(',');
          coordinates = {
            latitude: splitedCoords[0],
            longitude: splitedCoords[1],
          };
        }
        return g.active === 1 ? [...sum, { ...g, coords: coordinates }] : sum;
      }, []);

    getFilteredData = data =>
      data.reduce(
        (sum, i) =>
          i.country === 'Россия'
            ? {
                ...sum,
                [i.country]: sum.hasOwnProperty(i.country)
                  ? R.uniq(['Москва', 'Санкт-Петербург', ...[...sum[i.country], i.region].sort()])
                  : [i.region],
              }
            : {
                ...sum,
                [i.country]: sum.hasOwnProperty(i.country)
                  ? R.uniq([...sum[i.country], i.region]).sort()
                  : [i.region],
              },
        {},
      );

    handleInput = e => {
      const { value } = e.currentTarget;
      this.setState({ filter: value });
    };

    handleClickRegion = filter => {
      this.setState({
        filter,
      });
    };

    render() {
      const { isFetching, data, filter, countries } = this.state;
      const filteredData = filter
        ? data.filter(i => i.location.toLowerCase().includes(filter.toLowerCase()))
        : data;
      return (
        <WrappedComponent
          {...this.props}
          isFetching={isFetching}
          data={filteredData}
          filter={filter}
          handleInput={this.handleInput}
          handleClickRegion={this.handleClickRegion}
          countries={countries}
        />
      );
    }
  };

const mapStateToProps = state => ({
  bibleGroups: getAllBibleGroups(state),
});

const searchComposeWrapper = compose(connect(mapStateToProps, null), searchWrapper);

export default searchComposeWrapper;
