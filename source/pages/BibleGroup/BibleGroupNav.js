import React, { Component } from 'react';
import Select from 'react-select';
import { Link, NavLink, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { getIsTabletPhone } from '../../dist/selectors/screen';
import OrphusInfo from '../../components/OrphusInfo';

// TODO: create order component for nav menu
class LKNav extends Component {
  static propTypes = {
    isTabletPhone: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      bibleSelect: [
        {
          value: 1,
          label: 'О разделе',
        },
        {
          value: 2,
          label: 'Материалы для ведущих',
        },
        {
          value: 3,
          label: 'Видео',
        },
        {
          value: 4,
          label: 'Поиск групп',
        },
      ],
      bibleUrls: [
        {
          value: 1,
          label: 'about',
        },
        {
          value: 2,
          label: 'for-leaders',
        },
        {
          value: 3,
          label: 'video',
        },
        {
          value: 4,
          label: 'search',
        },
      ],
      currentBible: {
        value: 1,
        label: 'О разделе',
      },
    };
  }

  toggleSelect = e => {
    const { history } = this.props;
    const { bibleUrls } = this.state;

    history.push(`/bible-group/${bibleUrls[e.value - 1].label}`);
    this.setState({ currentBible: bibleUrls[e.value - 1] });
  };

  render() {
    const { isTabletPhone } = this.props;
    const { bibleSelect, currentBible } = this.state;

    return (
      <div className="side bible-group__wrapper">
        <div className="side__item nav-side nav-side__top">
          {isTabletPhone ? (
            <div className="select-adaptive bible-group-select">
              <Select
                isSearchable={false}
                options={bibleSelect}
                defaultValue={currentBible}
                onChange={this.toggleSelect}
                classNamePrefix="custom-select"
              />
            </div>
          ) : (
            <>
              <div className="list__item">
                <NavLink
                  to="/bible-group/about/"
                  className="nav-side__link list__item-link"
                  activeClassName="active">
                  О разделе
                </NavLink>
              </div>
              <div className="list__item">
                <NavLink
                  to="/bible-group/for-leaders/"
                  className="nav-side__link list__item-link"
                  activeClassName="active">
                  Материалы для ведущих
                </NavLink>
              </div>
              <div className="list__item">
                <NavLink
                  to="/bible-group/video/"
                  className="nav-side__link list__item-link"
                  activeClassName="active">
                  Видео
                </NavLink>
              </div>
              <div className="list__item">
                <NavLink
                  to="/bible-group/search/"
                  className="nav-side__link list__item-link"
                  activeClassName="active">
                  Поиск групп
                </NavLink>
              </div>
            </>
          )}
        </div>
        <OrphusInfo />
        <div className="side__item nav-side bible__btn-wrapper">
          <Link className="btn" to="/bible-group/add-group/">
            Добавить свою группу
          </Link>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isTabletPhone: getIsTabletPhone(state),
});

export default withRouter(connect(mapStateToProps)(LKNav));
