import R from 'ramda';
import React, { Component } from 'react';

import { Popup } from '../../../dist/browserUtils';
import { sendNewGroupRequest, getGroupTypes } from './serviceAPI';
import { initState } from './placeholderData';
import { sendSocialActions } from '../../../dist/utils';

export default function(WrappedComponent) {
  return class WrapperAddGroup extends Component {
    state = {
      ...initState,
      groupTypes: [
        {
          value: 0,
          label: 'Загрузка...',
        },
      ],
    };

    componentDidMount() {
      getGroupTypes().then(({ data }) => {
        if (data.status === 'ok') {
          this.setState({
            groupTypes: data.type.map(i => ({
              value: i.id,
              label: i.title,
            })),
          });
        }
      });
    }

    handleInput = e => {
      const { name, value } = e.currentTarget;
      if (value.length < 500) {
        this.setState(({ data, errors }) => ({
          data: {
            ...data,
            [name]: value,
          },
          errors: {
            ...errors,
            [name]: '',
            global: '',
          },
        }));
      }
    };

    handleLocation = e => {
      if (R.isEmpty(e)) {
        this.setState(({ errors }) => ({
          errors: {
            ...errors,
            location: 'Введите адрес группы',
          },
        }));
      } else if (
        (e[0] !== undefined && e[0].hasOwnProperty('address_components')) ||
        e.address_components !== undefined
      ) {
        const city = e[0].address_components.find(i =>
          i.types.includes('administrative_area_level_2'),
        );
        let region = e[0].address_components.find(i =>
          i.types.includes('administrative_area_level_1'),
        );
        let lat;
        let lng;
        const location = e[0].formatted_address;

        if (region !== undefined) {
          region = region.long_name;
        } else if (city !== undefined) {
          region = city.long_name;
        } else {
          region = '';
        }

        const country = e[0].address_components.find(i => i.types.includes('country')).long_name;
        const getCoords = R.pathOr('', [0, 'geometry', 'location'], e);

        if (Number(getCoords.lat)) {
          lat = getCoords.lat;
        } else {
          lat = getCoords.hasOwnProperty('lat') && getCoords.lat();
        }

        if (Number(getCoords.lng)) {
          lng = getCoords.lng;
        } else {
          lng = getCoords.hasOwnProperty('lng') && getCoords.lng();
        }

        this.setState(({ data, errors }) => ({
          data: {
            ...data,
            location,
            coords: `${lat}, ${lng}`,
            region,
            country,
          },
          errors: {
            ...errors,
            location: '',
            global: '',
          },
        }));
      } else {
        this.setState(({ errors }) => ({
          errors: {
            ...errors,
            location: 'Вы ввели недействительный адрес',
          },
        }));
      }
    };

    handleDrop = e => {
      this.setState(({ data, errors }) => ({
        data: {
          ...data,
          foto: e,
        },
        errors: {
          ...errors,
          foto: '',
          global: '',
        },
      }));
    };

    handleDropError = e => {
      this.setState(({ errors }) => ({
        errors: {
          ...errors,
          foto: `${R.pathOr('добавленный файл', [0, 'name'], e)} не подходит по требованиям`,
        },
      }));
    };

    handleSelect = (e, name) => {
      this.setState(({ data, errors }) => ({
        data: {
          ...data,
          [name]: e,
        },
        errors: {
          ...errors,
          [name]: '',
          global: '',
        },
      }));
    };

    handleSubmit = e => {
      e.preventDefault();
      const {
        isFetching,
        data: { name, location, days, times, email, type /* verifyCode, */ },
      } = this.state;
      if (isFetching) return;
      const validate = {
        name: !name.trim(),
        location: !location.trim(),
        days: !days.length,
        times: !times.trim(),
        email: !email.trim(),
        type: !type.length,
      };
      if (Object.values(validate).some(i => i)) {
        this.setState({
          errors: {
            ...validate,
            global: 'Проверьте поля, где-то ошибка',
          },
        });
      } else {
        this.setState({ isFetching: true });
        const { data } = this.state;
        sendNewGroupRequest({ ...data, captcha: data.verifyCode }).then(
          res => {
            if (res.status === 'ok') {
              this.setState({ ...initState }, () => {
                Popup.create(
                  {
                    title: null,
                    content: (
                      <div className="contact__successfull_popup bible-group__success">
                        <p>Ваша группа отправлена модератору</p>
                      </div>
                    ),
                    className: 'popup_auth not-header',
                  },
                  true,
                );
              });
              sendSocialActions({
                ga: [
                  'event',
                  'event_name',
                  { event_category: 'dobavit_gruppu', event_action: 'add' },
                ],
                ym: 'dobavit_gruppu',
              });
            } else {
              this.setState({
                isFetching: false,
                errors: {
                  global: R.pathOr('Ошибка сервера, попробуйте позже', ['error', 'message'], res),
                },
              });
            }
          },
          err => {
            this.setState({
              isFetching: false,
              errors: {
                global: R.pathOr(
                  'Ошибка сервера, попробуйте позже',
                  ['data', 'error', 'message'],
                  err,
                ),
              },
            });
          },
        );
      }
    };

    render() {
      return (
        <WrappedComponent
          {...this.props}
          {...this.state}
          handleInput={this.handleInput}
          handleDrop={this.handleDrop}
          handleDropError={this.handleDropError}
          handleSelect={this.handleSelect}
          handleSubmit={this.handleSubmit}
          handleLocation={this.handleLocation}
        />
      );
    }
  };
}
