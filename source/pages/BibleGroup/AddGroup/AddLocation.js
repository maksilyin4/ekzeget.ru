import R from 'ramda';
import React, { Component } from 'react';
import { withScriptjs } from 'react-google-maps';
import MapAddLocation from './MapAddLocation';

const {
  StandaloneSearchBox,
} = require('react-google-maps/lib/components/places/StandaloneSearchBox');

class AddLocation extends Component {
  marker = {};

  state = {
    places: [],
    currentValue: '',
  };

  componentDidUpdate(prevProps) {
    if (prevProps.location !== this.props.location) {
      this.setState({ currentValue: this.props.location });
    }
  }

  onPlacesChanged = () => {
    const places = this.searchBox.getPlaces();
    this.props.handleLocation(places);
    this.setState({
      places,
    });
  };

  onChangeInput = e => this.setState({ currentValue: e.currentTarget.value });

  render() {
    const { places, currentValue } = this.state;
    const { error, coords } = this.props;
    const getCoords = R.pathOr({}, [0, 'geometry', 'location'], places);
    const coordsArr = coords?.split(',');
    const hasLocationHelpers = getCoords.lat && getCoords.lng;

    // Если кликнули и получили данные, а не через селект

    if (coordsArr?.length > 1) {
      this.marker = {
        lat: Number(coordsArr[0]),
        lng: Number(coordsArr[1]),
      };
    } else if (hasLocationHelpers) {
      // Через селект
      this.marker = {
        lat: getCoords.lat(),
        lng: getCoords.lng(),
      };
    }

    return (
      <div data-standalone-searchbox="">
        <p>
          Начните вводить адрес или название храма и выберите из предложенного списка, или кликните
          мышью нужное место на карте
        </p>
        <StandaloneSearchBox
          ref={el => {
            this.searchBox = el;
          }}
          bounds={this.props.bounds}
          onPlacesChanged={this.onPlacesChanged}>
          <input
            type="text"
            id="bible-group-location"
            placeholder="Введите адрес"
            onChange={this.onChangeInput}
            value={currentValue}
            style={{
              boxSizing: 'border-box',
              // border: '1px solid transparent',
              width: '100%',
              height: '36px',
              padding: '9px 12px',
              borderRadius: 2,
              border: '1px solid #e8e8e8',
              fontSize: 15,
              outline: 'none',
              textOverflow: 'ellipses',
            }}
          />
        </StandaloneSearchBox>
        {!!error && <p className="err-message-bible-group">Укажите адрес</p>}
        <MapAddLocation
          {...this.props}
          mapElement={<div style={{ height: 230 }} />}
          marker={this.marker}
        />
      </div>
    );
  }
}

export default withScriptjs(AddLocation);
