import R from 'ramda';
import React, { Component } from 'react';
import { withGoogleMap, GoogleMap, Marker /* withScriptjs */ } from 'react-google-maps';
import Geocode from 'react-geocode';

import { googleGEOAPIKey } from '../../../dist/utils';

Geocode.setApiKey(googleGEOAPIKey);

class MapAddLocation extends Component {
  defaultCoords = {
    lat: 55.7558,
    lng: 37.6173,
  };

  clickTest = e => {
    const lat = e.latLng.lat();
    const lng = e.latLng.lng();

    // В массив тк данные из Селекта поступают обернутые в массив!!!

    Geocode.fromLatLng(lat, lng).then(
      response => this.props.handleLocation([response.results[0]]),
      error => {
        console.error(error);
      },
    );
  };

  render() {
    const { /* center, */ marker } = this.props;

    return (
      <GoogleMap
        ref={el => {
          this.map = el;
        }}
        defaultZoom={15}
        defaultCenter={this.defaultCoords}
        onClick={this.clickTest}
        center={!R.isEmpty(marker) ? { lat: marker.lat, lng: marker.lng } : this.defaultCoords}>
        {!R.isEmpty(marker) && <Marker position={{ lat: marker.lat, lng: marker.lng }} />}
      </GoogleMap>
    );
  }
}

export default withGoogleMap(MapAddLocation);
