export const daysWeek = [
  {
    value: 'Понедельник',
    label: 'Понедельник',
  },
  {
    value: 'Вторник',
    label: 'Вторник',
  },
  {
    value: 'Среда',
    label: 'Среда',
  },
  {
    value: 'Четверг',
    label: 'Четверг',
  },
  {
    value: 'Пятница',
    label: 'Пятница',
  },
  {
    value: 'Суббота',
    label: 'Суббота',
  },
  {
    value: 'Воскресенье',
    label: 'Воскресенье',
  },
];

export const groupType = [
  {
    value: 1,
    label: 'Учебная',
  },
  {
    value: 3,
    label: 'Общение',
  },
  {
    value: 4,
    label: 'Дискусионная',
  },
  {
    value: 5,
    label: 'свободная',
  },
  {
    value: 6,
    label: 'молитвенная',
  },
];

export const initState = {
  isFetching: false,
  data: {
    name: '',
    foto: [],
    location: '',
    coords: '',
    days: [],
    times: '',
    vedet: '',
    priester: '',
    phone: '',
    email: '',
    web: '',
    we_reads: '',
    kak_prohodit: '',
    history: '',
    type: [],
    region: '',
    country: '',
    verifyCode: '',
  },
  errors: {
    name: '',
    location: '',
    days: '',
    times: '',
    email: '',
    type: '',
    verifyCode: '',
    global: '',
  },
};
