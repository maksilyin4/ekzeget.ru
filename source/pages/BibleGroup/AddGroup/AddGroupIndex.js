import React, { Component } from 'react';
import { connect } from 'react-redux';

import Select from 'react-select';

import BreadHead from '~components/BreadHead';
import PageHead from '~components/PageHead';
import Form, { FormGroup /* , FormSubmit */ } from '../../../components/Form/Form';
import BibleGroupNav from '../BibleGroupNav';
import { daysWeek } from './placeholderData';
import { InputAddGroup, TextareaAddGroup } from './AddInput';
import Wrapper from './Wrapper';
import Preloader from '../../../components/Preloader/Preloader';
import ContactCaptcha from '../../../components/Captcha/ContactCaptcha';
import MapAddLocation from './AddLocation';
import '../styles.scss';
import { smoothScrollTo, googleAPIKey, getMetaData } from '../../../dist/utils';
import { setClearMeta } from '../../../store/metaData/actions';
import ContactsForm from '~pages/Contacts/ContactForm';

class AddBibleGroup extends Component {
  state = {
    meta: {
      h_one: 'Добавление Библейской группы',
      title: 'Добавление Библейской группы',
    },
  };

  componentDidMount() {
    this.props.setClearMeta();
    smoothScrollTo();
    getMetaData('/bible-group/add-group/').then(meta => {
      if (meta || meta !== 'Error') {
        this.setState({
          meta,
        });
      }
    });
  }

  _breadItems = () => [
    { path: '/', title: 'Главная' },
    { path: '/bible-group/search', title: 'Библейские группы' },
    { path: '', title: 'Добавление группы' },
  ];

  render() {
    const { meta } = this.state;
    const {
      isFetching,
      data,
      errors,
      handleInput,
      handleSelect,
      handleLocation,
      handleSubmit,
      groupTypes,
    } = this.props;

    return (
      <div className="page page__bible-group">
        <div className="wrapper">
          <BreadHead bread={this._breadItems()} />
          <PageHead meta={meta} h_one={'Добавить группу'} />
        </div>

        <div className="wrapper wrapper_two-col">
          <BibleGroupNav />
          <div className="content bible-group__content">
            <div className="bible-group__add-group">
              {isFetching && <Preloader />}
              <div className="content__paper container">
                <Form className="bible-group__form" onSubmit={handleSubmit}>
                  <InputAddGroup
                    label="Ваш храм или название группы"
                    valueName="name"
                    type="text"
                    handleInput={handleInput}
                    value={data?.name}
                    errors={errors?.name}
                    required
                  />
                  <FormGroup>
                    {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
                    <label htmlFor="bible-group-location">Адрес *</label>
                    <MapAddLocation
                      googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${googleAPIKey}&v=3.exp&libraries=geometry,drawing,places`}
                      loadingElement={<div style={{ height: 230 }} />}
                      containerElement={<div style={{ height: 230 }} />}
                      handleLocation={handleLocation}
                      error={errors?.location}
                      location={data?.location}
                      coords={data?.coords}
                    />
                  </FormGroup>
                  <FormGroup>
                    <label htmlFor="days">
                      Занятия проходят *
                      <Select
                        isSearchable={false}
                        options={daysWeek}
                        id="days"
                        name="days"
                        placeholder="Выберите дни недели"
                        isMulti
                        onChange={e => handleSelect(e, 'days')}
                        value={data?.days}
                        classNamePrefix="custom-select"
                        required
                      />
                      {!!errors?.days && (
                        <p className="err-message-bible-group">Выберите дни проведения</p>
                      )}
                    </label>
                  </FormGroup>
                  <InputAddGroup
                    label="Время начала"
                    type="time"
                    valueName="times"
                    handleInput={handleInput}
                    value={data?.times}
                    errors={errors?.times}
                    required
                  />
                  <InputAddGroup
                    type="text"
                    label="Ведущий"
                    valueName="vedet"
                    handleInput={handleInput}
                    value={data?.vedet}
                  />
                  <InputAddGroup
                    type="text"
                    label="Духовник группы (или кто благословил)"
                    valueName="priester"
                    handleInput={handleInput}
                    value={data?.priester}
                  />
                  <InputAddGroup
                    type="number"
                    label="Телефон для связи"
                    valueName="phone"
                    handleInput={handleInput}
                    value={data?.phone}
                  />
                  <InputAddGroup
                    type="email"
                    label="Email для связи"
                    valueName="email"
                    handleInput={handleInput}
                    value={data?.email}
                    errors={errors?.email}
                    required
                  />
                  <InputAddGroup
                    type="url"
                    label="Соц. сети или сайт группы"
                    valueName="web"
                    handleInput={handleInput}
                    value={data?.web}
                  />
                  <TextareaAddGroup
                    label="Что вы изучаете на занятиях"
                    valueName="we_reads"
                    handleInput={handleInput}
                    value={data?.we_reads}
                  />
                  <TextareaAddGroup
                    label="Опишите, как проходят занятия"
                    valueName="kak_prohodit"
                    handleInput={handleInput}
                    value={data?.kak_prohodit}
                  />
                  <TextareaAddGroup
                    label="Расскажите немного о возникновении группы. Как
                                        появилась, как возникла идея?"
                    valueName="history"
                    handleInput={handleInput}
                    value={data?.history}
                  />
                  <FormGroup>
                    <label htmlFor="type">
                      Тип группы *
                      <Select
                        isSearchable={false}
                        options={groupTypes}
                        id="type"
                        name="type"
                        placeholder="Выберите тип группы"
                        onChange={e => handleSelect(e, 'type')}
                        value={data?.type}
                        classNamePrefix="custom-select"
                        isMulti
                      />
                      {!!errors?.type && (
                        <p className="err-message-bible-group">Выберите тип группы</p>
                      )}
                    </label>
                  </FormGroup>
                  <div className="bible-group__add-group__other-questions">
                    <p>
                      Дополнительные вопросы (эти ответы не будут опубликованы, но помогут нам
                      развивать раздел)
                    </p>
                  </div>
                  <TextareaAddGroup
                    label="Какие материалы по теме работы библейских
                                        групп вам были бы интересны?"
                    valueName="materials"
                    handleInput={handleInput}
                    value={data?.materials}
                  />
                  <TextareaAddGroup
                    label="С какими трудностями вы сталкивались в работе
                                        группы?"
                    valueName="hardness"
                    handleInput={handleInput}
                    value={data?.hardness}
                  />
                  <ContactCaptcha onChange={handleInput} value={data?.verifyCode} />
                  {!!errors?.global && (
                    <p style={{ textAlign: 'center' }} className="err-message-bible-group">
                      {errors?.global}
                    </p>
                  )}
                  <button type="submit" className="btn add-group-btn">
                    Добавить группу
                  </button>
                </Form>
              </div>
              <div className="bible-group__add-group__contacts content__paper container">
                <p className="form__notice">
                  Если у вас возникли какие-то вопросы, Вы можете связаться с нами через эту форму
                </p>
                <div>
                  <ContactsForm />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = () => ({});
const mapDispatchToProps = {
  setClearMeta,
};

export default connect(mapStateToProps, mapDispatchToProps)(Wrapper(AddBibleGroup));
