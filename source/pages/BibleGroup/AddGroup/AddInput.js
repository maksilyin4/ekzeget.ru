import React from 'react';

export function InputAddGroup({ label, valueName, handleInput, required, value, errors, type }) {
  return (
    <fieldset className="form__fieldset">
      <label htmlFor={valueName}>{`${label} ${required ? '*' : ''}`}</label>
      <input
        className="textfield__input"
        type={type || 'text'}
        name={valueName}
        id={valueName}
        autoComplete="off"
        onChange={handleInput}
        value={value}
        required={required}
      />
      {!!errors && <p className="err-message-bible-group">Введены некорректные данные</p>}
    </fieldset>
  );
}

export function TextareaAddGroup({ label, valueName, handleInput, required, value }) {
  return (
    <fieldset className="form__fieldset">
      <label htmlFor={valueName}>{label}</label>
      <textarea
        className="textfield__input"
        name={valueName}
        id={valueName}
        cols="30"
        rows="10"
        autoComplete="off"
        onChange={handleInput}
        value={value}
        required={required}
      />
    </fieldset>
  );
}
