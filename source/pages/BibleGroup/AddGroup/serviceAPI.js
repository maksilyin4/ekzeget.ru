// import qs from 'qs';
import { AXIOS } from '../../../dist/ApiConfig';

export function sendNewGroupRequest(body) {
  const data = new FormData();
  if (body.foto.length > 0) {
    data.append('foto', body.foto[0]);
  }
  const sendData = {
    ...body,
    type: body.type.map(i => i.value),
    days: body.days.map(i => i.label).join(', '),
  };

  Object.keys(sendData).forEach(i => {
    if (i !== 'foto' && i !== 'type') {
      data.append(i, sendData[i]);
    } else if (i === 'type') {
      data.append('type[]', sendData[i]);
    }
  });

  return AXIOS({
    method: 'POST',
    url: 'bible-group/add/',
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    data,
  }).then(
    res => res.data,
    err => err,
  );
}

export function getGroupTypes() {
  return AXIOS.get('bible-group/type/');
}
