import React from 'react';
import Loadable from 'react-loadable';
import Preloader from '../../../components/Preloader/Preloader';

const LoadableBar = Loadable({
  loader: () => import('./AddGroupIndex'),
  loading() {
    return (
      <div className="preloader-relative">
        <Preloader />
      </div>
    );
  },
  delay: 300,
});

export default LoadableBar;
