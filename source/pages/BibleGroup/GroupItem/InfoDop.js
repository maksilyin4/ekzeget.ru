import React, { PureComponent } from 'react';

export default class InfoDop extends PureComponent {
  render() {
    const { kak_prohodit, history, type, we_reads } = this.props;
    return (
      <div className="bible-group__item__dopInfo content__paper container">
        {!!we_reads && (
          <div>
            <p>
              <strong>Что мы читаем на занятиях:</strong>
            </p>
            <p>{we_reads}</p>
          </div>
        )}
        {!!kak_prohodit && (
          <div>
            <p>
              <strong>Как проходят встречи:</strong>
            </p>
            <p>{kak_prohodit}</p>
          </div>
        )}
        {!!history && (
          <div>
            <p>
              <strong>История группы:</strong>
            </p>
            <p>{history}</p>
          </div>
        )}
        {!!type && type.length > 0 && (
          <div>
            <p>
              <strong>Тип группы:</strong>
            </p>
            <p>{type.map(i => i.title).join(', ')}</p>
          </div>
        )}
      </div>
    );
  }
}
