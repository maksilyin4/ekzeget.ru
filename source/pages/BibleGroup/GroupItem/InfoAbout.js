import React, { PureComponent } from 'react';

export default class InfoAbout extends PureComponent {
  render() {
    const { location, days, times, vedet } = this.props;
    return (
      <div className="bible-group__item__mainInfo__about">
        <p>
          <strong>Адрес:</strong>
          {` ${location}`}
        </p>
        <p>
          <strong>Занятия:</strong>
          {` ${days}`}
        </p>
        <p>
          <strong>Время начала:</strong>
          {` ${times}`}
        </p>
        <p>
          <strong>Ведущий:</strong>
          {` ${vedet}`}
        </p>
      </div>
    );
  }
}
