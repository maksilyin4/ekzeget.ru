import R from 'ramda';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { Popup, urlToAPI } from '../../../dist/browserUtils';
import Preloader from '../../../components/Preloader/Preloader';
import BreadHead from '~components/BreadHead';
import PageHead from '~components/PageHead';
import BibleGroupNav from '../BibleGroupNav';
import BibleGroupMap from '../SearchGroup/BibleGroupMap';
import InfoAbout from './InfoAbout';
import InfoContacts from './InfoContacts';
import InfoDop from './InfoDop';
import BibleGroupQuestion from '../../../components/Popups/BibleGroupQuestion';
import Wrapper from '../SearchWrapper';

import '../styles.scss';
import { smoothScrollTo, googleAPIKey } from '../../../dist/utils';

class GroupItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      choosenCoord: { lat: 62.333256, lng: 84.705232 },
      zoom: 2,
      chosenData: {},
      url: urlToAPI,
    };

    // SSR
    if (this.props.data.length) {
      const [chosenData] = this.props.data.filter(i => i.code === this.props.match.params.id);
      if (chosenData && chosenData.coords) {
        this.state.choosenCoord = {
          lat: Number(chosenData.coords.latitude),
          lng: Number(chosenData.coords.longitude),
        };
        this.state.zoom = 15;
      }
      this.state.chosenData = chosenData || {};
    }
  }

  componentDidMount() {
    smoothScrollTo();
  }

  componentDidUpdate(prevProps) {
    if (
      (prevProps.isFetching && !this.state.isFetching) ||
      prevProps.match.params.id !== this.props.match.params.id
    ) {
      this.getCurrentCoords();
    }
  }

  getCurrentCoords = () => {
    smoothScrollTo(500);
    const {
      data,
      match: { params },
    } = this.props;
    const [chosenData] = data.filter(i => i.code === params.id);
    if (chosenData) {
      this.setState(() =>
        chosenData.coords
          ? {
              choosenCoord: {
                lat: Number(chosenData.coords.latitude),
                lng: Number(chosenData.coords.longitude),
              },
              zoom: 15,
              chosenData,
            }
          : {
              chosenData,
            },
      );
    }
  };

  handleClick = coords => {
    if (coords) {
      this.setState({
        choosenCoord: {
          lat: Number(coords.latitude),
          lng: Number(coords.longitude),
        },
        zoom: 15,
      });
    } else {
      this.setState({
        choosenCoord: { lat: 62.333256, lng: 84.705232 },
        zoom: 2,
      });
    }
  };

  _breadItems = () => [
    { path: '/', title: 'Главная' },
    { path: '', title: 'Библейские группы' },
  ];

  openPopUp = () => {
    Popup.create(
      {
        title: null,
        content: <BibleGroupQuestion />,
        className: 'chapter__bible-group__popup',
      },
      true,
    );
  };

  render() {
    const { isFetching, data } = this.props;
    const { chosenData, choosenCoord, zoom, url } = this.state;
    const isContent = !R.isEmpty(chosenData);
    return (
      <div className="page page__bible-group">
        <div className="wrapper">
          <BreadHead bread={this._breadItems()} />
          <PageHead h_one="Библейские группы" onClick={this.openPopUp} />
        </div>
        <div className="wrapper wrapper_two-col">
          <BibleGroupNav />
          <div className="content bible-group__content bible-group__item">
            {isContent ? (
              <>
                <div className="bible-content__title">
                  <h3>{R.pathOr(chosenData, ['name'], chosenData)}</h3>
                </div>
                <div className="bible-group__item__content">
                  <div className="bible-group__item__mainInfo content__paper-border">
                    {chosenData.foto && (
                      <img
                        className="bible-group__img"
                        src={`${url}${chosenData.foto}`}
                        alt={`${R.pathOr(chosenData, ['name'], chosenData)}`}
                      />
                    )}
                    <InfoAbout
                      location={chosenData.location}
                      days={chosenData.days}
                      times={chosenData.times}
                      vedet={chosenData.vedet}
                      photo={chosenData.foto}
                    />
                    <InfoContacts
                      phone={chosenData.phone}
                      email={chosenData.email}
                      web={chosenData.web}
                    />
                  </div>
                  <InfoDop
                    we_reads={chosenData.we_reads}
                    kak_prohodit={chosenData.kak_prohodit}
                    history={chosenData.history}
                    type={chosenData.type}
                  />
                  {typeof chosenData.coords === 'object' && (
                    <div className="bible-group__item__map">
                      <div className="bible-content__title">
                        <h3>Мы на карте</h3>
                      </div>
                      <BibleGroupMap
                        isMarkerShown
                        choosenCoord={choosenCoord}
                        zoom={zoom}
                        markerClick={this.handleClick}
                        markers={[
                          {
                            id: chosenData.code,
                            latitude: Number(chosenData.coords.latitude),
                            longitude: Number(chosenData.coords.longitude),
                            name: chosenData.name,
                            location: chosenData.location,
                            days: chosenData.days,
                            times: chosenData.times,
                            phone: chosenData.phone,
                            photo: chosenData.foto,
                          },
                        ]}
                        googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${googleAPIKey}&v=3.exp&libraries=geometry,drawing,places`}
                        loadingElement={<div style={{ height: 530 }} />}
                        containerElement={<div style={{ height: 530 }} />}
                        mapElement={<div style={{ height: 530 }} />}
                      />
                    </div>
                  )}
                  <div className="bible-group__other-groups">
                    <div className="bible-content__title">
                      <h3>Другие группы в этом регионе</h3>
                    </div>
                    <nav>
                      {data
                        .filter(
                          i => i.location.includes(chosenData.region) && i.code !== chosenData.code,
                        )
                        .map(j => (
                          <Link key={j.id} to={`/bible-group/${j.code}`}>
                            {j.name}
                          </Link>
                        ))}
                    </nav>
                  </div>
                </div>
              </>
            ) : isFetching ? (
              <Preloader />
            ) : (
              <div>Этой группы нет в базе</div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Wrapper(GroupItem);
