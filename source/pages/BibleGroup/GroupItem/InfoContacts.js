import React, { PureComponent } from 'react';

const phoneIcon = require('../images/phone.svg');
const linkIcon = require('../images/link.svg');
const mailIcon = require('../images/mail.svg');

export default class InfoContacts extends PureComponent {
  render() {
    const { phone, web, email } = this.props;
    return (
      <div className="bible-group__item__mainInfo__contacts">
        {!!phone && (
          <div>
            <img src={phoneIcon} alt="phoneIcon" />
            <p>{phone}</p>
          </div>
        )}
        {!!web && (
          <div>
            <img src={linkIcon} alt="linkIcon" />
            <a href={web} target="_blank" rel="nofollow noopener noreferrer">
              {web}
            </a>
          </div>
        )}
        {!!email && (
          <div>
            <img src={mailIcon} alt="mailIcon" />
            <a href={`mailto:${email}`}>{email}</a>
          </div>
        )}
      </div>
    );
  }
}
