import React, { useMemo, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import {
  getBibleGroupUpdates,
  getBibleGroupNotesUpdates,
} from '../../../dist/actions/BibleGroupUpdates';
import { getBibleGroups, isFetching } from '../../../dist/selectors/BibleGroupUpdates';
import BibleGroups from '../../../components/BibleGroup';
import Preloader from '../../../components/Preloader/Preloader';

import './about.scss';

const BibleGroupAbout = ({
  getBibleGroupUpdates,
  getBibleGroupNotesUpdates,
  isFetching,
  bibleGroups,
}) => {
  useEffect(() => {
    if (!bibleGroups.length) {
      getBibleGroupUpdates(4);
    }
    
    getBibleGroupNotesUpdates();
  }, []);

  return useMemo(
    () => (
      <div className="content bible-group">
        <div className="content__paper container">
          <div className="bible-content__title">О разделе</div>
          <p style={{ textAlign: 'justify' }}>
            Уже не первый год в России и других странах развивается движение православных библейских
            групп: верующие собираются, чтобы читать и изучать вместе Священное Писание. Подобные
            кружки не только помогают лучше узнать Библию с разных сторон, но и дают возможность
            общения с теми, кто близок по духу. Формы и практики групп многообразны, с их ведением
            связаны различные дискуссии и вопросы, относящиеся к сферам экзегетки, библеистики,
            педагогики, психологии и другим. Здесь мы постарась собрать наиболее полную информацию о
            библейских группах на сегодняшний день.
            <br />
            <br />В этом разделе вы найдете каталог и карту действующих групп, методические указания
            в помощь начинающим, советы и опыт ведущих с многолетним стажем, а также отзывы
            священников и участников кружков.
          </p>
        </div>
        <div className="bible-group__news">
          <div className="bible-content__title content__paper container">Обновления в разделе</div>
          {isFetching ? <Preloader /> : <BibleGroups bibleGroups={bibleGroups} />}
        </div>
      </div>
    ),
    [isFetching, bibleGroups],
  );
};

BibleGroupAbout.defaultProps = {
  isFetching: true,
  bibleGroups: [],
  getBibleGroupUpdates: () => {},
};

BibleGroupAbout.propTypes = {
  isFetching: PropTypes.bool,
  bibleGroups: PropTypes.arrayOf(PropTypes.object),
  getBibleGroupUpdates: PropTypes.func,
};

const mapDispatchToProps = {
  getBibleGroupUpdates,
  getBibleGroupNotesUpdates,
};

const mapStateToProps = store => ({
  bibleGroups: getBibleGroups(store),
  isFetching: isFetching(store),
});

export default connect(mapStateToProps, mapDispatchToProps)(BibleGroupAbout);
