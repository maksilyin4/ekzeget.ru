import React, { useMemo } from 'react';
import { Helmet } from 'react-helmet';

import listOfVideo from './listOfVideo';

const VideoBibleGroup = () =>
  useMemo(
    () => (
      <div className="content bible-group content__paper container">
        <Helmet title="Видео Библейские группы || Экзегет.ру" />
        {listOfVideo.map(({ description, youTubeURL, subTitle }, i) => {
          return subTitle ? (
            // TODO extract to scss
            <p key={i} style={{ paddingBottom: 10 }}>
              {subTitle}
            </p>
          ) : (
            <div className="bible-group__video" key={i}>
              <iframe
                title={description}
                width={560}
                height={315}
                src={youTubeURL}
                frameBorder="0"
                allow="autoplay; encrypted-media"
                allowFullScreen
              />
              <p style={{ paddingTop: 10 }}>{description}</p>
            </div>
          );
        })}
      </div>
    ),
    [listOfVideo],
  );

export default VideoBibleGroup;
