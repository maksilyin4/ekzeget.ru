import React, { useState, useRef, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';

import Wrapper from '../SearchWrapper';
import Preloader from '../../../components/Preloader/Preloader';
import SearchGroupCities from './SearchGroupCities';
import SearchGroupMapContainer from './SearchGroupMapContainer';

import './searchGroup.scss';

const SearchGroup = ({ isFetching, data, filter, handleInput, countries, handleClickRegion }) => {
  const [state, setState] = useState({
    choosenCoord: { lat: 62.333256, lng: 84.705232 },
    zoom: 2,
  });
  const mapSearch = useRef(null);

  useEffect(() => {
    if (state.zoom !== 2) {
      setState({
        ...state,
        zoom: 2,
      });
    }
  }, [filter]);

  const handleClick = coords => {
    if (coords) {
      setState({
        choosenCoord: {
          lat: Number(coords.latitude),
          lng: Number(coords.longitude),
        },
        zoom: 12,
      });
    } else {
      setState({
        choosenCoord: { lat: 62.333256, lng: 84.705232 },
        zoom: 2,
      });
    }
  };

  return (
    <div className="content bible-group bible-group__search content__paper container">
      <Helmet title="Поиск групп Библейские группы || Экзегет.ру" />
      {isFetching && <Preloader />}
      <SearchGroupCities
        countries={countries}
        handleClickRegion={handleClickRegion}
        mapSearch={mapSearch}
      />
      <SearchGroupMapContainer
        filter={filter}
        handleInput={handleInput}
        data={data}
        handleClick={handleClick}
        mapSearch={mapSearch}
        state={state}
      />
    </div>
  );
};

SearchGroup.defaultProps = {
  isFetching: true,
};

SearchGroup.propTypes = {
  isFetching: PropTypes.bool,
  data: PropTypes.array,
  filter: PropTypes.string,
  handleInput: PropTypes.func,
  countries: PropTypes.object,
  handleClickRegion: PropTypes.func,
};

export default Wrapper(SearchGroup);
