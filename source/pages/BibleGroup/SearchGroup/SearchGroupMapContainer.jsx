import React, { useMemo } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { googleAPIKey } from '../../../dist/utils';
import BibleGroupMap from './BibleGroupMap';

const geo = require('../images/geo.svg');
const searchIcon = require('../images/find.png');

const SearchGroupMapContainer = ({ filter, handleInput, data, handleClick, mapSearch, state }) =>
  useMemo(
    () => (
      <div className="bible-group__search-tools">
        <div className="bible-group__search-input">
          <div className="bible-group__search-input-text">
            <input
              type="text"
              name=""
              autoComplete="off"
              value={filter}
              onChange={handleInput}
              placeholder="Поиск по адресу"
            />
            <img src={searchIcon} alt="search" />
          </div>
          <ul>
            {data.map(({ id, name, code, coords, location }, index) => (
              <li key={id}>
                <Link to={`/bible-group/${code}/`}>
                  <h4 tabIndex={index}>{name}</h4>
                </Link>
                <div onClick={() => handleClick(coords)}>
                  <img className="bible-group__img-map" src={geo} alt="geo" />
                  <p>{location}</p>
                </div>
              </li>
            ))}
          </ul>
        </div>
        <div ref={mapSearch} className="bible-group__search-map">
          <BibleGroupMap
            isMarkerShown
            filter={filter}
            choosenCoord={state.choosenCoord}
            zoom={state.zoom}
            markerClick={handleClick}
            markers={data.reduce(
              (sum, d) =>
                d.coords
                  ? [
                      ...sum,
                      {
                        id: d.code,
                        latitude: Number(d.coords.latitude),
                        longitude: Number(d.coords.longitude),
                        name: d.name,
                        location: d.location,
                        days: d.days,
                        times: d.times,
                        phone: d.phone,
                        photo: d.foto,
                      },
                    ]
                  : sum,
              [],
            )}
            googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${googleAPIKey}&v=3.exp&libraries=geometry,drawing,places`}
            loadingElement={<div style={{ height: 530 }} />}
            containerElement={<div style={{ height: 530 }} />}
            mapElement={<div style={{ height: 530 }} />}
          />
          <ul className="bible-group__hidden">
            {data.map(item => (
              <li key={item.id}>
                <span> {item.kak_prohodit} </span>
                <span> {item.location} </span>
                <span> {item.name} </span>
                <span> {item.priester} </span>
              </li>
            ))}
          </ul>
        </div>
      </div>
    ),
    [data, filter, mapSearch, state.choosenCoord, state.zoom],
  );

SearchGroupMapContainer.defaultProps = {
  filter: '',
  handleInput: () => {},
  data: [],
  handleClick: () => {},
  mapSearch: { current: null },
  state: {
    choosenCoord: { lat: 62.333256, lng: 84.705232 },
    zoom: 2,
  },
};

SearchGroupMapContainer.propTypes = {
  filter: PropTypes.string,
  handleInput: PropTypes.func,
  data: PropTypes.shape({
    id: PropTypes.number,
    coords: PropTypes.shape({
      latitude: PropTypes.string,
      longitude: PropTypes.string,
    }),
    name: PropTypes.string,
    location: PropTypes.string,
    days: PropTypes.string,
    times: PropTypes.string,
    phone: PropTypes.string,
    foto: PropTypes.string,
    kak_prohodit: PropTypes.string,
    priester: PropTypes.string,
  }),
  handleClick: PropTypes.func,
  mapSearch: PropTypes.shape({
    current: PropTypes.object,
  }),
  state: PropTypes.shape({
    choosenCoord: PropTypes.shape({
      lat: PropTypes.number,
      lng: PropTypes.number,
    }),
    zoom: PropTypes.number,
  }),
};

export default SearchGroupMapContainer;
