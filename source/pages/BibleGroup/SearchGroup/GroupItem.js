import R from 'ramda';
import React, { Component } from 'react';

import Preloader from '../../../components/Preloader/Preloader';
import BreadHead from '~components/BreadHead';
import PageHead from '~components/PageHead';
import BibleGroupNav from '../BibleGroupNav';

import { smoothScrollTo } from '../../../dist/utils';

const phoneIcon = require('../images/phone.svg');
const linkIcon = require('../images/link.svg');
const mailIcon = require('../images/mail.svg');

class GroupItem extends Component {
  componentDidMount() {
    smoothScrollTo();
  }

  _breadItems = props => [
    { path: '/', title: 'Главная' },
    { path: '/bible-group/search', title: 'Библейские группы' },
    { path: '', title: R.pathOr('Группа', ['name'], props) },
  ];

  render() {
    const {
      isFetch,
      data,
      match: { params },
    } = this.props;
    const [chosenData] = data.filter(i => i.id === Number(params.id));
    const isContent = !!chosenData;

    return (
      <div className="page page__bible-group">
        <div className="wrapper">
          <BreadHead bread={this._breadItems(chosenData)} />
          <PageHead h_one={R.pathOr('Библейская группа', ['name'], chosenData)} />
        </div>

        <div className="wrapper wrapper_two-col">
          <BibleGroupNav />
          <div className="content bible-group__content bible-group__item">
            <div className="bible-content__title">
              <h3>Подробная информация о группе</h3>
            </div>
            {isContent ? (
              <div className="bible-group__item__content">
                <div className="bible-group__item__mainInfo">
                  <div className="bible-group__item__mainInfo__about">
                    <p>
                      <strong>Адрес:</strong>
                      {` ${chosenData.location}`}
                    </p>
                    <p>
                      <strong>Занятия:</strong>
                      {` ${chosenData.days}`}
                    </p>
                    <p>
                      <strong>Время начала:</strong>
                      {` ${chosenData.times}`}
                    </p>
                    <p>
                      <strong>Ведущий:</strong>
                      {` ${chosenData.vedet}`}
                    </p>
                  </div>
                  <div className="bible-group__item__mainInfo__contacts">
                    {!!chosenData.phone && (
                      <div>
                        <img src={phoneIcon} alt="phoneIcon" />
                        <p>{chosenData.phone}</p>
                      </div>
                    )}
                    {!!chosenData.web && (
                      <div>
                        <img src={linkIcon} alt="linkIcon" />
                        <a href={chosenData.web} target="_blank" rel="nofollow noopener noreferrer">
                          {chosenData.web}
                        </a>
                      </div>
                    )}
                    {!!chosenData.email && (
                      <div>
                        <img src={mailIcon} alt="mailIcon" />
                        <a href={`mailto:${chosenData.email}`}>{chosenData.email}</a>
                      </div>
                    )}
                  </div>
                </div>
                <div>чем занимаемся</div>
                <div>как проходят встречи</div>
                <div>тип группы</div>
                <div>мы на карте</div>
                <div>другие группы в этой области</div>
              </div>
            ) : isFetch ? (
              <Preloader />
            ) : (
              <div>Этой группы нет в базе</div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default GroupItem;
