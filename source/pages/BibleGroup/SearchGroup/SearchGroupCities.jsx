import React, { useMemo, useCallback } from 'react';
import PropTypes from 'prop-types';

const SearchGroupCities = ({ countries, handleClickRegion }) => {
  const clickRegion = useCallback(
    region => {
      handleClickRegion(region);
    },
    [handleClickRegion],
  );

  return useMemo(
    () => (
      <div className="bible-group__search__city-list">
        <ul className="bible-group__search__city-list-russian">
          {!!Object.keys(countries).length &&
            countries['Россия'].map((i, index) => (
              // eslint-disable-next-line react/no-array-index-key
              <li key={`${i}${index}`} onClick={() => clickRegion(i)}>
                <button className="bible-group__search-btn">{i}</button>
              </li>
            ))}
        </ul>
        <ul className="bible-group__search__city-list-others">
          {!!countries &&
            Object.keys(countries).map(i =>
              i !== 'Россия' ? (
                <li key={i}>
                  <h4>{i}</h4>
                  <div>
                    {countries[i].map((j, index) => (
                      <button
                        className="bible-group__search-btn"
                        /* eslint-disable-next-line react/no-array-index-key */
                        key={`${index}:${j}`}
                        onClick={() => clickRegion(j)}>
                        {j}
                      </button>
                    ))}
                  </div>
                </li>
              ) : null,
            )}
        </ul>
      </div>
    ),
    [clickRegion, countries],
  );
};

SearchGroupCities.defaultProps = {
  countries: {},
  handleClickRegion: () => {},
  mapSearch: { current: null },
};

SearchGroupCities.propTypes = {
  countries: PropTypes.objectOf(PropTypes.arrayOf(PropTypes.string)),
  mapSearch: PropTypes.shape({
    current: PropTypes.object,
  }),
  handleClickRegion: PropTypes.func,
};

export default SearchGroupCities;
