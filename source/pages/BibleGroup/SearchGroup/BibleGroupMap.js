import R from 'ramda';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow } from 'react-google-maps';
import { urlToAPI } from '../../../dist/browserUtils';

const { MarkerClusterer } = require('react-google-maps/lib/components/addons/MarkerClusterer');

class BibleGroupMap extends Component {
  state = {
    isOpen: null,
    url: urlToAPI,
  };

  componentDidUpdate = prevProps => {
    if (prevProps.filter !== this.props.filter && this.state.isOpen) {
      this.setState({ isOpen: null });
    }
  };

  handleClick = id => {
    this.setState({
      isOpen: id,
    });
  };

  render() {
    const { isMarkerShown, markers, choosenCoord, zoom, markerClick } = this.props;

    const { isOpen, url } = this.state;

    return (
      <GoogleMap defaultZoom={2} center={choosenCoord} zoom={zoom}>
        {isMarkerShown && (
          <MarkerClusterer averageCenter enableRetinaIcons gridSize={60}>
            {markers.map((marker, i) => (
              <Marker
                key={marker.i || i}
                position={{ lat: marker.latitude, lng: marker.longitude }}
                onClick={() => {
                  this.handleClick(marker.id);
                  if (markerClick) {
                    markerClick({
                      latitude: marker.latitude,
                      longitude: marker.longitude,
                    });
                  }
                }}>
                {isOpen === marker.id && (
                  <InfoWindow onCloseClick={() => this.handleClick(marker.id)}>
                    <div className="google-maps-infoWindow">
                      {!R.isEmpty(marker.photo) && (
                        <img
                          className="bible-group__map-photo"
                          src={`${url}${marker.photo}`}
                          alt="Фото группы"
                        />
                      )}
                      <div className="bible-group__map-container">
                        <Link to={`/bible-group/${marker.id}/`}>
                          <h3>{marker.name}</h3>
                        </Link>
                        <p className="bible-group__map-location">
                          <strong>Адрес:</strong>
                          {` ${marker.location}`}
                        </p>
                        {marker.phone.length > 0 && (
                          <p className="bible-group__map-phone">
                            <strong>Тел:</strong>
                            {` ${marker.phone}`}
                          </p>
                        )}
                        <div className="google-maps-infoWindow-dateTime">
                          <p className="bible-group__map-lessons-title">Занятия проводятся:</p>
                          <p className="bible-group__map-lessons-desc">
                            {` ${marker.days} (${marker.times})`}
                          </p>
                        </div>
                      </div>
                    </div>
                  </InfoWindow>
                )}
              </Marker>
            ))}
          </MarkerClusterer>
        )}
      </GoogleMap>
    );
  }
}

export default withScriptjs(withGoogleMap(BibleGroupMap));
