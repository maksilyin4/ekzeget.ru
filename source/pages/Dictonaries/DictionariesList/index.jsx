import React, { memo, useMemo } from 'react';

import CollectionList, {
  CollectionItem,
} from '../../../components/Collections/CollectionList/CollectionList';

const DictionariesList = ({
  count,
  isDictonariesWordLoading,
  changePage,
  words: {
    dictionary = [],
    pages: { totalCount = 0 },
  },
  dictionaryName,
  path,
}) => {
  const isDictionary = useMemo(() => dictionary.length > 0, [dictionary]);

  if (!isDictonariesWordLoading && !isDictionary) {
    return (
      <div className="all-about-bible-list">
        <p className="empty-content">Библейский контент не обнаружен</p>
      </div>
    );
  }
  return (
    <div className="all-about-bible-list dictionary-list">
      <>
        <CollectionList>
          {dictionary.map(({ id, code, word }, index) => (
            <CollectionItem
              key={id + code + index}
              href={`${path}${dictionaryName}/${code}/`}
              title={word}
              mod="underline"
            />
          ))}
        </CollectionList>

        {totalCount / 40 > count ? (
          <p className="seeAll" onClick={changePage}>
            Показать еще
          </p>
        ) : (
          ''
        )}
      </>
    </div>
  );
};

export default memo(DictionariesList);
