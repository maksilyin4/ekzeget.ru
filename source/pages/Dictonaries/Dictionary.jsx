import R from 'ramda';
import React, { PureComponent } from 'react';
import cx from 'classnames';
import { Link } from 'react-router-dom';
import { withCookies } from 'react-cookie';

import Preloader from '../../components/Preloader/Preloader';
import { ListItem } from '../../components/List/List';
import DictionaryMobile from './DictionaryMobile';
import { smoothScrollTo } from '../../dist/utils';

import { setTranslate } from '../../utils/setTranslate';

class Dictionary extends PureComponent {
  componentDidMount() {
    smoothScrollTo();

    const {
      match: { params },
    } = this.props;

    this.getDataWord(params.id);
  }

  componentDidUpdate(prevProps) {
    const {
      getMeaningWord,
      match: {
        params: { id },
      },
    } = this.props;

    if (R.isEmpty(this.scrollBar) && R.isEmpty(this.scrollToElement)) {
      this.scrollBar.scrollTop(this.scrollToElement.offsetTop);
    }
    if (prevProps.match.params.id !== id) {
      getMeaningWord(id);
    }
  }

  componentWillUnmount = () => this.props.getMeanibgWordClear();

  getDataWord = async id => {
    const {
      getMeaningWord,
      word: { word },
    } = this.props;

    !word?.word && (await getMeaningWord(id));
  };

  getTranslateToPush = (defaultWord = null) => {
    const {
      chooseTranslate,
      history,
      word: { word },
      stTranslate,
      location: { pathname },
      greekTranslate,
      cookies,
    } = this.props;

    const currentWord = defaultWord || word?.word;

    const translate = pathname.includes('grechesko-russkiy-bibleyskiy-slovar')
      ? greekTranslate
      : stTranslate;

    if (translate !== null && translate.length > 0) {
      setTranslate({ chooseTranslate, translate: translate[0], cookies });
    }

    history.push(`/search/bible/?search=${currentWord}`);
  };

  scrollBarRef = element => {
    this.scrollBar = element;
  };

  scrollToActive = element => {
    this.scrollToElement = element;
  };

  renderView = ({ style }) => {
    const customStyle = {
      maxHeight: 'calc(100% + 5px)',
      overflow: 'hidden',
      overflowY: 'scroll',
    };
    return <div style={{ ...style, ...customStyle }} />;
  };

  render() {
    const {
      word: { word },
      dictionary = [],
      currentTranslate,
      dictionaryName,
      isMeaningWordLoading,
      match: { params },
      isDesktop,
      changeType,
      location: { pathname },
      path,
    } = this.props;

    // Проблема такой проверки в том, что на 200 статус нам приходит word, даже если null
    // Так же может придти пустой объект word
    // ¯\_(ツ)_/¯

    return (
      <>
        {!isDesktop && (
          <DictionaryMobile
            letter={word?.letter}
            dictionaryName={dictionaryName}
            sameWords={word?.same_words}
            activeWord={params.id}
            path={path}
          />
        )}
        <div className="content dictionary">
          <div className="content_two-col dictionary__container font-zoomable">
            <div className="dictionary__head">
              <h2 className="dictionary__title">{word?.word}</h2>
              {!pathname.includes('grechesko-russkiy-biblejskiy-slovar') && (
                <button
                  onClick={() => this.getTranslateToPush()}
                  className="dictionary__link-translate dictionary__link">
                  Упоминания в Библии
                </button>
              )}
            </div>
            {!isMeaningWordLoading ? (
              <>
                {word?.other_dictionaries.length > 0 && (
                  <div className="dictionary__others">
                    <p className="dictionary__others-title"> Также встречается в словарях: </p>
                    <ul className="dictionary__others-list">
                      {word.other_dictionaries.map(other => {
                        const otherDictionary = dictionary.find(el => el.id === other.type_id);
                        return (
                          <li className="dictionary__other-item" key={other.id}>
                            <Link
                              to={`${path}${otherDictionary?.code}/${other.id}/`}
                              className="dictionary__other-link dictionary__link"
                              onClick={() =>
                                changeType({
                                  name: otherDictionary?.code,
                                  type: other.type_id,
                                  linkToWord: other.id,
                                })
                              }>
                              {otherDictionary?.title}
                            </Link>
                          </li>
                        );
                      })}
                    </ul>
                  </div>
                )}

                <div
                  className={cx('dictionary__content', {
                    csya_old__container: currentTranslate?.code === 'csya_old',
                    grek__container: currentTranslate?.code === 'grek',
                  })}
                  dangerouslySetInnerHTML={{ __html: word?.description }}
                />

                {word?.variant.length > 0 && (
                  <div className="dictionary__variables">
                    <p className="dictionary__others-title"> Варианты, встречающиеся в Писании: </p>
                    <ul>
                      {/* No ID and HTML from Backend */}
                      {word.variant.map((item, index) => (
                        // eslint-disable-next-line react/no-array-index-key
                        <li key={index}>
                          <div dangerouslySetInnerHTML={{ __html: item }} />
                        </li>
                      ))}
                    </ul>
                  </div>
                )}
                {/* 
                {!word?.description.length && !word?.other_dictionaries.length && (
                  <h3 className="dictionary__error"> Такое определение отсутствует </h3>
                )} */}
              </>
            ) : (
              <Preloader className="dictionary" />
            )}
          </div>
          {isDesktop && (
            <div className="side side_right">
              <div className="list dictionary-right">
                <div className="side__interpretators-head">
                  <p className="dictionary__word"> {word?.letter} </p>
                  <Link
                    to={{ pathname: `${path}${dictionaryName}/`, letter: word?.letter }}
                    className="dictionary__link-back dictionary__link">
                    Выбрать другую букву
                  </Link>
                </div>
                <div className="dictionary__scrollbar-wrapper">
                  {word?.same_words.map(same_word => {
                    return (
                      <ListItem
                        toRef={params.id === same_word.code && this.scrollToActive}
                        key={same_word.code}
                        title={same_word.word}
                        href={`${path}${dictionaryName}/${same_word.code}/`}
                        className={cx('dictionary__item', {
                          active: params.id === same_word.code,
                        })}
                      />
                    );
                  })}
                </div>
              </div>
            </div>
          )}
        </div>
      </>
    );
  }
}

Dictionary.defaultProps = {
  alphabet: [],
  otherVariables: [],
  word: {},
};

export default withCookies(Dictionary);
