import R from 'ramda';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavLink, Route, Switch } from 'react-router-dom';
import Select from 'react-select';

import './dictonaries.scss';

import { Popup } from '../../dist/browserUtils';
import { getMetaData, setClearMeta } from '../../store/metaData/actions';
import { smoothScrollTo } from '../../dist/utils';
import dictonariesActions from '../../dist/actions/Dictonaries';
import { chooseTranslate } from '../../dist/actions/translates';
import {
  getDictonaries,
  getDictonariesIsLoading,
  getDictonariesWord,
  getWordsIsLoading,
  getMeaningWord,
  getMeaningIsLoading,
  getDictonariesSelect,
  setIsDictonariesWordFailed,
} from '../../dist/selectors/Dictonaries';
import { getIsDesktop, getIsTabletMedium } from '../../dist/selectors/screen';
import {
  getCurrentTranslate,
  getStTextTranslate,
  getGreekTranslate,
} from '../../dist/selectors/translates';
import { setMetaData } from '../../store/metaData/selector';

import DictionariesList from './DictionariesList';
import Preloader from '../../components/Preloader/Preloader';
import Icon from '../../components/Icons/Icons';
import Dictionary from './Dictionary';
import OrphusInfo from '../../components/OrphusInfo';
import DictionariesSearch from './DictionariesSearch';
import Alphabet from '../../components/Alphabet';
import NotFound from '../NotFound/NotFound';
import getTitleMeta from './utilsDictonaries';
import Helmet from '~components/Helmet';

class Dictonaries extends Component {
  static propTypes = {
    location: PropTypes.object,
    isDesktop: PropTypes.bool.isRequired,
    isTabletMedium: PropTypes.bool.isRequired,
    dictonariesSelect: PropTypes.array,
    dictionaries: PropTypes.object,
    words: PropTypes.object,
  };

  constructor(props) {
    super(props);

    this.perPage = 40;

    // Узрите ужас и пиздец
    // На беке было проблематично, хардкод
    this.state = {
      dictonaryType: 1,
      activeLetter: '',
      dictionaryName: '',

      listForCheckRedirect: {
        'Библейская энциклопедия Брокгауза': 'enciklopediya-brokgauza',
        'Библейская энциклопедия архимандрита Никифора': 'enciklopediya-arhimandrita-nikifora',
        'Греческо-русский Библейский словарь': 'grechesko-russkiy-biblejskiy-slovar',
      },

      count: 1,
    };
  }

  async componentDidMount() {
    smoothScrollTo();
    const {
      metaData,
      getTitle,
      location: { pathname },
      dictionaries,
      word,
    } = this.props;

    await this.getMeta(pathname);

    const dictionaryId = pathname.split('/').filter(el => el)[2];

    await Promise.all([
      this.getDictonariesState(),
      this.getDictonariesWord(),
      metaData?.title && dictionaryId && (await this.getMeta(pathname)),
    ]);

    getTitle(
      Object.values(word).length
        ? `${word?.word?.word || ''} - ${word?.word?.dictionary?.title || ''}`
        : getTitleMeta({ pathname, metaData, dictionaries }),
    );
  }

  componentWillReceiveProps(nextProps) {
    // Проблема такой проверки в том, что на 200 статус нам приходит word, даже если null
    // Так же может придти пустой объект word
    // ¯\_(ツ)_/¯
    const arrayOfPathname = nextProps.location.pathname.split('/');
    const id = arrayOfPathname[4];

    if (
      id !== undefined &&
      id.length > 0 &&
      nextProps.word &&
      !R.isEmpty(nextProps.word) &&
      nextProps.word.word !== null
    ) {
      this.setState({
        activeLetter: nextProps.word.word.letter,
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { activeLetter } = this.state;
    const {
      location: { pathname },
      word,
      metaData,
      getTitle,
      dictionaries,
      isDictonariesWordFailed,
      setDictonariesWordClearError,
    } = this.props;

    if (prevState.activeLetter !== activeLetter || prevProps.location.pathname !== pathname) {
      this.changeDictonariesWord(pathname);
    }

    if (prevProps.location.pathname !== pathname) {
      if (isDictonariesWordFailed) {
        setDictonariesWordClearError();
      }
      this.getDictonariesWord();
      this.getDictonariesState();
      this.getMeta(pathname);
    }

    if (
      prevProps.metaData !== metaData ||
      prevProps.location.pathname !== pathname ||
      prevProps.word !== word
    ) {
      getTitle(
        Object.values(word).length
          ? `${word?.word?.word || ''} - ${word?.word?.dictionary?.title || ''}`
          : getTitleMeta({ pathname, metaData, dictionaries }),
      );
    }
  }

  componentWillUnmount = () => {
    // for seo
    this.props.getDictonariesWordClear();
    this.props.setClearMeta();
  };

  getMeta = name => this.props.getMetaData(name);

  changeDictonariesWord = pathname => {
    const { dictonaryType, activeLetter } = this.state;

    if (pathname.split('/')[3] && pathname.split('/')[3].length) {
      if (R.isEmpty(pathname.split('/')[4]) && activeLetter.length) {
        this.props.getDictonariesWordClear();
        this.props.getDictonariesWord(dictonaryType, activeLetter, `?per-page=${this.perPage}`);
      }
    }
  };

  getDictonariesWord = () => {
    const {
      dictionaries: { dictionary },
      location: { pathname },
      getDictonariesWord,
      words,
    } = this.props;

    if (dictionary) {
      const { activeLetter } = this.state;
      const dictionaryName = pathname.split('/')[3];

      const dictonaryTypes = dictionaryName
        ? dictionary.find(el => el.code === dictionaryName)
        : dictionary[0];
      const letter = !activeLetter.length ? dictonaryTypes.letters[0] : activeLetter;

      if (R.isEmpty(pathname.split('/')[4]) && words.dictionary.length === 0) {
        getDictonariesWord(dictonaryTypes.id, letter, `?per-page=${this.perPage}`);
      }
      this.setState({
        dictonaryType: dictonaryTypes.id,
        dictionaryName,
      });
    }
  };

  getDictonariesState = async () => {
    const {
      // dictionaries,
      getDictonaries,
      location: { pathname },
      history,
    } = this.props;

    const { dictionaryName } = this.state;

    // if (!dictionaries.dictionary) {
    await getDictonaries(pathname);
    // }

    const { dictionary } = this.props.dictionaries;
    if (dictionary) {
      const arrayOfPathname = pathname.split('/');
      const dictonaryTypes = arrayOfPathname[3]
        ? dictionary.find(el => el.code === arrayOfPathname[3])
        : dictionary[0];
      if (!arrayOfPathname[3]) {
        history.replace(`${pathname}${dictionaryName || dictonaryTypes.code}/`);
      }
    }
  };

  _breadItems = () => [
    { path: '/', title: 'Главная' },
    { path: '', title: 'Словари' },
  ];

  _who = e => {
    e.preventDefault();

    Popup.create(
      {
        title: (
          <div className="page-desc-popup__title">
            <span className="page-desc-popup__title-text">Словари</span>{' '}
            <div className="page-desc-popup__icon">
              <Icon icon="manuscript_desc" />
            </div>
          </div>
        ),
        content: (
          <p className="page__desc">
            В энциклопедию включено около 7500 объяснений различных понятий из книг Ветхого и Нового
            Заветов. При этом богословские термины, за исключением терминов строго библейского
            значения, опущены. Целью издания являлось дать ответы «почти на большую часть вопросов
            библейской археологии, архитектуры, астрономии, географии, биографии библейских
            деятелей, ботаники, священной библиографии, военной науки, зоологии, земледелия,
            искусств, минералогии, метеорологии, медицины, математики, нумизматики, педагогики,
            физики, этнографии и др».
          </p>
        ),
        className: 'popup_exegetes',
      },
      true,
    );
  };

  changePage = () => {
    const {
      dictionaries: { dictionary },
      location: { pathname },
      words,
      getDictonariesWord,
    } = this.props;
    if (dictionary) {
      const { activeLetter, dictonaryType } = this.state;
      const dictionaryName = pathname.split('/')[3];

      const dictonaryTypes = dictionaryName
        ? dictionary.find(el => el.code === dictionaryName)
        : dictionary[0];
      const letter = !activeLetter.length ? dictonaryTypes.letters[0] : activeLetter;

      const count = this.state.count + 1;
      if (Math.ceil(words.pages.totalCount / 40) >= count) {
        this.setState({ count });
        getDictonariesWord(dictonaryType, letter, `?page=${count}&per-page=${this.perPage}`);
      }
    }
  };

  onChangeSelect = e => this._changeType({ name: e.name, type: e.value, letter: e.letter });

  _changeType = ({ name, letter = '', type = '', linkToWord = null }) => {
    const { history } = this.props;

    this.setState({
      dictonaryType: type,
      dictionaryName: name,
      activeLetter: letter,
    });

    if (linkToWord) {
      history.push(`/all-about-bible/dictionaries/${name}/${linkToWord}/`);
    } else {
      history.push(`/all-about-bible/dictionaries/${name}/`);
    }
  };

  _changeLetter = value => () => {
    if (this.state.activeLetter !== value) {
      this.props.getDictonariesWordClear();
      this.setState({ activeLetter: value, count: 1 });
    }
  };

  render() {
    const {
      isDictonariesLoading,
      dictionaries,
      metaData,

      words,
      dictonariesSelect,
      isTabletMedium,
      isDesktop,
      history,
      word,
      location: { pathname },
      getMeaningWord,
      currentTranslate,
      stTranslate,
      chooseTranslate,
      greekTranslate,
      match: { path },
      isDictonariesWordLoading,
      getMeanibgWordClear,
      isMeaningWordLoading,
      isDictonariesWordFailed,
    } = this.props;

    const { dictonaryType, dictionaryName, listForCheckRedirect, activeLetter, count } = this.state;

    if (isDictonariesWordFailed) {
      return <NotFound />;
    }

    const title = Object.values(word).length
      ? `${word?.word?.word || ''} - ${word?.word?.dictionary?.title || ''} БИБЛИЯ онлайн.`
      : dictonariesSelect.find(dictonary => pathname.includes(dictonary.name))?.label || '';

    return (
      <>
        <Helmet
          title={title || 'Словари'}
          description={metaData?.description || ''}
          keywords={metaData?.keywords || ''}
        />
        <div className="page dictonaries-page">
          <div className="page__body">
            <div className="wrapper wrapper_two-col dictonaries__wrapper not_padding all_about_bible">
              <div className="side all-about-bible-side">
                {isDictonariesLoading ? (
                  <Preloader />
                ) : (
                  <>
                    <div className="side__item nav-side dictonaries__side">
                      {!isDesktop && !isTabletMedium ? (
                        <div className="dictonaries-side-select">
                          <Select
                            isSearchable={false}
                            options={dictonariesSelect}
                            value={dictonariesSelect[dictonaryType - 1]}
                            onChange={this.onChangeSelect}
                            classNamePrefix="dictonaries-select custom-select"
                          />
                        </div>
                      ) : (
                        <div className="dictonaries__list-wrapper">
                          {dictionaries.dictionary ? (
                            dictionaries.dictionary.map(dictionary => {
                              const letter =
                                dictionary.letters.find(
                                  letter => letter === this.state.activeLetter,
                                ) || dictionary.letters[0];
                              return (
                                <div key={dictionary.id} className="list__item">
                                  <NavLink
                                    className="nav-side__link  list__item-link"
                                    activeClassName={
                                      listForCheckRedirect[dictionary.title] && 'active'
                                    }
                                    onClick={() =>
                                      this._changeType({
                                        name: dictionary.code,
                                        letter,
                                        type: dictionary.id,
                                      })
                                    }
                                    to={`${path}${dictionary.code}/`}>
                                    {dictionary.title}
                                  </NavLink>
                                </div>
                              );
                            })
                          ) : (
                            <p className="empty-content"> Информация не обнаружена </p>
                          )}
                        </div>
                      )}
                    </div>
                    {isDesktop && (
                      <div className="side__item">
                        <OrphusInfo />
                      </div>
                    )}
                  </>
                )}
              </div>
              <div className="all-about-bible-content dictonaries-content-body">
                <Switch>
                  <Route
                    exact
                    path={[`${path}`, `${path}:dictonaries/`]}
                    render={() => (
                      <>
                        <DictionariesSearch history={history} />
                        {!isDictonariesLoading ? (
                          <>
                            {dictionaries &&
                              dictionaries.dictionary &&
                              dictionaries.dictionary.map(dictionary => {
                                const regex = new RegExp(`[${dictionary.letters.join('')}]`);
                                const letter = !activeLetter.length
                                  ? dictionary.letters[0]
                                  : activeLetter;
                                return (
                                  dictionary.id === this.state.dictonaryType && (
                                    <Alphabet
                                      key={dictionary.id}
                                      alphabet={dictionary.letters}
                                      activeLetter={letter}
                                      onClick={this._changeLetter}
                                      regex={regex}
                                      className="all-about-bible-alphabet"
                                      isTest={false}
                                    />
                                  )
                                );
                              })}
                          </>
                        ) : (
                          <Preloader />
                        )}
                        <DictionariesList
                          count={count}
                          changePage={this.changePage}
                          words={words}
                          dictionaryName={dictionaryName}
                          path={path}
                          isDictonariesWordLoading={isDictonariesWordLoading}
                        />
                      </>
                    )}
                  />
                  <Route
                    exact
                    path={`${path}:dictonaries/:id/`}
                    render={props => (
                      <Dictionary
                        match={props.match}
                        history={props.history}
                        location={props.location}
                        chooseTranslate={chooseTranslate}
                        word={word}
                        isMeaningWordLoading={isMeaningWordLoading}
                        dictionaryName={dictionaryName}
                        getMeaningWord={getMeaningWord}
                        currentTranslate={currentTranslate}
                        stTranslate={stTranslate}
                        greekTranslate={greekTranslate}
                        isDesktop={isDesktop}
                        changeType={this._changeType}
                        path={path}
                        dictionary={dictionaries.dictionary}
                        getMeanibgWordClear={getMeanibgWordClear}
                      />
                    )}
                  />
                  <Route component={NotFound} />
                </Switch>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  isDictonariesLoading: getDictonariesIsLoading(state),
  dictionaries: getDictonaries(state),
  dictonariesSelect: getDictonariesSelect(state),
  isDictonariesWordLoading: getWordsIsLoading(state),
  isDictonariesWordFailed: setIsDictonariesWordFailed(state),
  words: getDictonariesWord(state),
  word: getMeaningWord(state),
  isMeaningWordLoading: getMeaningIsLoading(state),
  isDesktop: getIsDesktop(state),
  isTabletMedium: getIsTabletMedium(state),
  currentTranslate: getCurrentTranslate(state),
  stTranslate: getStTextTranslate(state),
  greekTranslate: getGreekTranslate(state),
  metaData: setMetaData(state),
});

const mapDispatchToProps = {
  ...dictonariesActions,
  chooseTranslate,
  getMetaData,
  setClearMeta,
};

export default connect(mapStateToProps, mapDispatchToProps)(Dictonaries);
