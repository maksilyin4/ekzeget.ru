import React from 'react';
import Breadcrumbs from '../../components/Breadcrumbs/Breadcrumbs';

const DictionaryHeader = ({ breadArr, h_one }) => (
  <div className="wrapper">
    <Breadcrumbs data={breadArr} />
    <div className="page__head page__head_two-col dictionaries-title">
      <h1 className="page__title">{h_one}</h1>
    </div>
  </div>
);

export default DictionaryHeader;
