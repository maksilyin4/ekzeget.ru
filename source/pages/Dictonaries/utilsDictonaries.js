export default function getTitleMeta({ pathname, metaData, dictionaries }) {
  const dictionaryName = pathname.split('/')[3];
  let titleMeta = 'Словари';
  if (metaData && metaData.meta && metaData.meta.h_one) {
    titleMeta = metaData.meta.h_one;
  } else {
    const dictionary =
      dictionaries &&
      dictionaries.dictionary &&
      dictionaries.dictionary.find(({ code }) => code === dictionaryName);
    titleMeta = dictionary ? dictionary.title : 'Словари';
  }

  return titleMeta;
}
