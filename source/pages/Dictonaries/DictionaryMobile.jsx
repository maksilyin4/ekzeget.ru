import React, { PureComponent } from 'react';
import cx from 'classnames';
import { Link } from 'react-router-dom';

import AnimateComponent from '../../components/AnimateComponent/SlideLeftRight';
import Scrollbar from '../../components/Scrollbar';
import { ListItem } from '../../components/List/List';

const backArrow = require('../../components/NavTestament/images/backArrow.svg');

class DictionaryMobile extends PureComponent {
  state = {
    isOpen: false,
  };

  componentDidUpdate() {
    if (this.scrollAction && this.scrollActiveItem) {
      this.scrollAction.scrollTop(this.scrollActiveItem.getBoundingClientRect().y - 150);
    }
  }

  toggleOpenState = bool => this.setState({ isOpen: bool });

  scrollBarRef = element => {
    this.scrollAction = element;
  };

  scrollToActive = element => {
    this.scrollActiveItem = element;
  };

  // странно, но если общий не ставить hidden, то появляется горизонтальный скролл
  renderView = ({ style }) => (
    <div
      style={{
        ...style,
        overflow: 'hidden',
        overflowX: 'hidden',
        overflowY: 'scroll',
      }}
    />
  );

  render() {
    const { letter, dictionaryName, sameWords, activeWord, path } = this.props;

    const sameWordsData = sameWords || [];
    const { isOpen } = this.state;

    return (
      <div className="dictionary_mobile">
        <div className="dictionary__other-words">
          <Link to={`${path}${dictionaryName}/`} className="dictionary__link-back">
            <p className="dictionary__word">{letter}</p>
          </Link>

          <p className="dictionary__word-desc" onClick={() => this.toggleOpenState(true)}>
            Другие слова на эту букву
          </p>
        </div>

        <AnimateComponent show={isOpen} className="dictionary__mobile-popup">
          <div className="dictionary__back">
            <button onClick={() => this.toggleOpenState(false)} className="dictionary__mobile-btn">
              <img src={backArrow} alt="menu" />
            </button>
          </div>
          <div className="dictionary__animate">
            <div className="side__interpretators-head">
              <p className="dictionary__word"> {letter} </p>
              <Link
                to={`${path}${dictionaryName}/`}
                className="dictionary__link-back dictionary__link">
                Выбрать другую букву
              </Link>
            </div>
            <div className="dictionary__scrollbar-wrapper">
              <Scrollbar toRef={this.scrollBarRef} renderView={this.renderView}>
                {sameWordsData.map(same_word => (
                  <ListItem
                    toRef={activeWord === same_word.code && this.scrollToActive}
                    key={same_word.id}
                    title={same_word.word}
                    href={`${path}${dictionaryName}/${same_word.code}/`}
                    onClick={() => this.toggleOpenState(false)}
                    className={cx('dictionary__item', {
                      active: activeWord === same_word.code,
                    })}
                  />
                ))}
              </Scrollbar>
            </div>
          </div>
        </AnimateComponent>
      </div>
    );
  }
}

export default DictionaryMobile;
