import React, { PureComponent } from 'react';

import searchIcon from '~assets/images/layout/search.svg';

class DictionariesSearch extends PureComponent {
  state = {
    value: '',
  };

  onKeyDown = e => {
    if (e.keyCode === 13) {
      this.pushToSearch();
    }
  };

  getNewValue = e => this.setState({ value: e.target.value });

  pushToSearch = () => {
    const { history } = this.props;
    const { value } = this.state;

    history.push(`/search/dictionaries/?search=${value}`);
  };

  render() {
    return (
      <div className="dictionary__search">
        <input
          className="dictionary__search-input"
          onChange={this.getNewValue}
          onKeyDown={this.onKeyDown}
          type="text"
          placeholder="Поиск по словарям"
        />
        <img
          className="dictionary__search-icon"
          onClick={this.pushToSearch}
          src={searchIcon}
          alt="Поиск"
        />
      </div>
    );
  }
}

export default DictionariesSearch;
