import React, { Component } from 'react';
import cx from 'classnames';
import { Link } from 'react-router-dom';

import closeIcon from './images/close.svg';
import bookIcon from './images/book.svg';
import searchIcon from '~assets/images/layout/search.svg';
import noImage from './images/noImage.png';
import { urlToAPI } from '~dist/browserUtils';

class BibleMapSearch extends Component {
  state = {
    showText: false,
    activeOld: true,
    activeNew: false,
    activeResult: false,
    valueInput: '',
    resultMarkers: [],
    isActiveSearch: false,
    testamentsNew: [],
    testamentsOld: [],
  };

  componentDidMount() {
    const { isOpen } = this.props;

    document.addEventListener('mousedown', this.handleClickOutside);
    this.setState({
      activeResult: isOpen,
    });
  }

  componentWillReceiveProps(prevProps) {
    const { isOpen, typeMarkers } = this.props;
    const { valueInput } = this.state;

    if (prevProps.isOpen !== isOpen) {
      this.setState({
        activeResult: prevProps.isOpen,
      });
    }

    // Если мы выбрали другой завет или "Показать все"
    if (typeMarkers !== prevProps.typeMarkers) {
      this.getListMarkers(valueInput, prevProps.localMarkers);
    }

    if (prevProps.data !== null) {
      this.getDataTestament(prevProps.data);
    }
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  getDataTestament = testaments => {
    const newData = [];
    const oldData = [];

    testaments.verses.forEach(testament => {
      if (testament.book.testament_id === 2) {
        newData.push(testament.book);
      } else {
        oldData.push(testament.book);
      }
    });

    this.setState({
      testamentsNew: newData,
      testamentsOld: oldData,
    });
  };

  getListMarkers = (value, markers) => {
    const currentMarkers = [];
    const regExp = new RegExp(value.trim(), 'i');

    markers.forEach(marker => {
      if (regExp.test(marker.title)) {
        currentMarkers.push(marker);
      }
    });

    this.setState({ resultMarkers: currentMarkers });
  };

  toggleActive = () => {
    const { showText } = this.state;

    this.setState({
      showText: !showText,
    });
  };

  handleInput = e => {
    const { isMarkersLocal, markers, localMarkers } = this.props;

    this.setState({ valueInput: e.target.value, isActiveSearch: true });

    if (isMarkersLocal) {
      this.getListMarkers(e.target.value, localMarkers);
    } else {
      this.getListMarkers(e.target.value, markers.markersData);
    }
  };

  activeOld = () => this.setState({ activeOld: true, activeNew: false });

  activeNew = () => this.setState({ activeOld: false, activeNew: true });

  closeResult = () => this.props.closeMarker();

  clickResult = marker => {
    const { openMarker } = this.props;

    openMarker(marker);
    this.setState({ isActiveSearch: false, valueInput: marker.title });
  };

  inputClick = () => {
    const { isMarkersLocal, markers, localMarkers } = this.props;
    const { valueInput } = this.state;

    if (isMarkersLocal) {
      this.getListMarkers(valueInput, localMarkers);
    } else {
      this.getListMarkers(valueInput, markers.markersData);
    }

    this.setState({ isActiveSearch: true });
  };

  setWrapperRef = node => {
    this.wrapperRef = node;
  };

  setMarkers = testament => {
    const { setFilterMarkers, screen, closeMarker } = this.props;

    if (!screen.desktop) {
      this.setState({ activeResult: false });
      closeMarker();
    }

    setFilterMarkers(testament);
  };

  handleClickOutside = event => {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({ isActiveSearch: false });
    }
  };

  render() {
    const {
      showText,
      activeNew,
      activeOld,
      activeResult,
      valueInput,
      resultMarkers,
      isActiveSearch,
      testamentsNew,
      testamentsOld,
    } = this.state;
    const { currentTranslate, data } = this.props;

    return (
      <div className="bible-map__search-input">
        <div className="bible-map__search-input-container" ref={this.setWrapperRef}>
          <div className="bible-map__search-input-text bible-group__search-input-text">
            <input
              type="text"
              name=""
              ref="searchInput"
              value={valueInput}
              autoComplete="off"
              onChange={this.handleInput}
              onClick={this.inputClick}
              placeholder="Найти на карте"
            />
            <img className="bible-map__search-icon" src={searchIcon} alt="search" />
          </div>
          {/* Выподающий список при поиске */}
          {valueInput.length >= 2 && isActiveSearch && (
            <ul className="bible-map__search-list">
              {resultMarkers.length > 0 ? (
                resultMarkers.slice(0, 10).map(marker => (
                  <li
                    key={marker.id}
                    onClick={() => this.clickResult(marker)}
                    className="bible-map__search-item">
                    {marker.title}
                  </li>
                ))
              ) : (
                <p className="bible-map__search-nothing"> По вашему запросу ничего не найдено </p>
              )}
            </ul>
          )}
        </div>
        {/* Выбраный из списка в поиске */}
        {activeResult && data !== null && (
          <div className="bible-map__search-result">
            <div className="bible-map__target">
              <div className="bible-map__target-head">
                <h3 className="bible-map__target-title">{data.title}</h3>
                <button className="bible-map__btn-close" onClick={this.closeResult}>
                  <img className="btn-close__icon" src={closeIcon} alt="Закрыть" />
                </button>
              </div>
              <div className="bible-map__target-content">
                {data.image.base_url !== undefined ? (
                  <img
                    className="bible-map__target-img"
                    src={`${urlToAPI}${data.image.base_url}/${data.image.path}`}
                    alt={String(data.title)}
                  />
                ) : (
                  <img
                    className="bible-map__target-img bible-map__no-image"
                    src={noImage}
                    alt="Изображение отсутствует"
                  />
                )}
                {data.description.length > 0 ? (
                  <div
                    className={cx('bible-map__target-text', { targetActive: showText })}
                    dangerouslySetInnerHTML={{ __html: data.description }}
                  />
                ) : (
                  <p className="bible-map__target-text">Описание отсутствует</p>
                )}
                {data.description.length > 120 && (
                  <button className="page__desc-toggle text_underline" onClick={this.toggleActive}>
                    {showText ? 'Скрыть описание' : 'Читать полностью'}
                  </button>
                )}
                {data.verses.length > 0 ? (
                  <>
                    <ul className="bible-map__list">
                      {data.verses.map(item =>
                        item.verse.map(verse => (
                          <li className="bible-map__item" key={verse.id}>
                            <div className="bible-map__item-head">
                              <Link
                                className="bible-map__item-link"
                                to={`/bible/${verse.book_code}/glava-${verse.chapter}?verse=${verse.chapter}:${verse.number}`}>
                                {verse.short}
                              </Link>
                            </div>
                            <div
                              className={cx('bible-map__links-text', {
                                csya: currentTranslate.code === 'csya_old',
                                greek: currentTranslate.code === 'grek',
                              })}
                              dangerouslySetInnerHTML={{ __html: verse.text }}
                            />
                          </li>
                        )),
                      )}
                    </ul>
                    <div className="bible-map__inBooks">
                      <h3 className="bible-map__inBooks-title">Также упоминается в книгах</h3>
                      <div className="bible-map__inBooks-btns">
                        <button
                          className={cx('btn bible-map__inBooks-btn', { activeBtn: activeOld })}
                          onClick={this.activeOld}>
                          Ветхий завет
                        </button>
                        <button
                          className={cx('btn bible-map__inBooks-btn', { activeBtn: activeNew })}
                          onClick={this.activeNew}>
                          Новый завет
                        </button>
                      </div>
                      {/* testament_id - Ветхий (1) и Новый (2) завет */}
                      <ul className="bible-map__inBooks-list">
                        {activeOld &&
                          (testamentsOld.length > 0 ? (
                            testamentsOld.map(testament => (
                              <li className="bible-map__inBooks-item" key={testament.id}>
                                <p
                                  className="bible-map__inBooks-desc"
                                  onClick={() => this.setMarkers(testament)}>
                                  {testament.title}
                                </p>
                                <img
                                  src={bookIcon}
                                  className="bible-map__inBook-icon"
                                  alt={testament.title}
                                />
                              </li>
                            ))
                          ) : (
                            <p> Нет упоминаний в Ветхом завете </p>
                          ))}
                        {activeNew &&
                          (testamentsNew.length > 0 ? (
                            testamentsNew.map(testament => (
                              <li className="bible-map__inBooks-item" key={testament.id}>
                                <p
                                  className="bible-map__inBooks-desc"
                                  onClick={() => this.setMarkers(testament)}>
                                  {testament.title}
                                </p>
                                <img
                                  src={bookIcon}
                                  className="bible-map__inBook-icon"
                                  alt={testament.title}
                                />
                              </li>
                            ))
                          ) : (
                            <p> Нет упоминаний в Новом завете </p>
                          ))}
                      </ul>
                    </div>
                  </>
                ) : (
                  <p className="bible-map__empty-data"> Упоминания отсутствуют </p>
                )}
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default BibleMapSearch;
