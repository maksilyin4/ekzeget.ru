import React, { Component } from 'react';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps';

import markerIcon from './images/marker.svg';

const { MarkerClusterer } = require('react-google-maps/lib/components/addons/MarkerClusterer');

class BibleMapMain extends Component {
  state = {
    choosenCoord: { lat: 49.128722, lng: 67.067935 },
    zoom: this.props.zoomFilter || 3,
  };

  componentDidMount() {
    const { markers, pointId, openPointIdMarker } = this.props;
    if (pointId && markers && markers.length) {
      const coorEl = markers.find(el => el.id === pointId);
      openPointIdMarker(coorEl);

      if (coorEl) {
        const { lat, lon } = coorEl;
        this.setState({ choosenCoord: { lat, lng: lon }, zoom: 7 });
      }
    }
  }

  componentWillReceiveProps(prevProps) {
    const { centerMarker } = this.props;
    if (prevProps.centerMarker !== centerMarker) {
      this.setState({
        choosenCoord: prevProps.centerMarker,
        zoom: 7,
      });
    }

    if (prevProps.isZoomFilter) {
      this.setState({ zoom: prevProps.zoomFilter });
    }
  }

  getMarker = (markers, pointId) => {
    const { openMarker } = this.props;
    if (pointId) {
      return markers.map(
        marker =>
          marker.id === pointId && (
            <Marker
              key={marker.id}
              position={{ lat: marker.lat, lng: marker.lon }}
              options={{ icon: markerIcon }}
              onClick={() => openMarker(marker)}
            />
          ),
      );
    }
    return markers.map(marker => (
      <Marker
        key={marker.id}
        position={{ lat: marker.lat, lng: marker.lon }}
        options={{ icon: markerIcon }}
        onClick={() => openMarker(marker)}
      />
    ));
  };

  handleClick = coords => {
    if (coords) {
      this.setState({
        choosenCoord: {
          lat: Number(coords.latitude),
          lng: Number(coords.longitude),
        },
        zoom: 13,
      });
    } else {
      this.setState({
        choosenCoord: { lat: 49.128722, lng: 67.067935 },
        zoom: 2,
      });
    }
  };

  render() {
    const { choosenCoord, zoom } = this.state;

    const {
      isMarkerShown,
      markers,
      openMarker,
      isMarkersLocal,
      localMarkers,
      pointId,
    } = this.props;

    return (
      <GoogleMap
        defaultZoom={3}
        center={choosenCoord}
        zoom={zoom}
        // Отключаем кнопки Спутник, Карты и возможность во весь экран
        defaultOptions={{ mapTypeControl: false, fullscreenControl: false }}>
        {isMarkerShown && (
          <MarkerClusterer averageCenter enableRetinaIcons gridSize={60}>
            {isMarkersLocal
              ? localMarkers.map(marker => (
                  <Marker
                    key={marker.id}
                    position={{ lat: marker.lat, lng: marker.lon }}
                    options={{ icon: markerIcon }}
                    onClick={() => openMarker(marker)}
                  />
                ))
              : this.getMarker(markers, pointId)}
          </MarkerClusterer>
        )}
      </GoogleMap>
    );
  }
}

export default withScriptjs(withGoogleMap(BibleMapMain));
