import React from 'react';
import { Switch, Route } from 'react-router-dom';
import NotFound from '../NotFound/NotFound';
import BibleMapIndex from './BibleMapIndex';

export default ({ location, ...rest }) => {
  return (
    <Switch>
      <Route exact path={location.pathname} render={props => <BibleMapIndex {...props} {...rest} />} />
      <Route component={NotFound} />
    </Switch>
  );
};
