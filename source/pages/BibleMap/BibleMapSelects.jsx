import React, { Component, Fragment } from 'react';
import Select from 'react-select';
import cx from 'classnames';

class BibleMapSelects extends Component {
  state = {
    typeTestament: [
      {
        value: 1,
        label: 'Новый завет',
      },
      {
        value: 2,
        label: 'Ветхий завет',
      },
    ],
    activeTestament: null,
    chosenTestament: {
      value: 1,
      label: 'Новый завет',
    },
    newTestamentLocalActive: null,
    oldTestamentLocalActive: null,
    isDisabled: true,
    isNothingCurrent: false,
    isNothingAll: false,
    isNothingBook: false,
  };

  componentDidMount() {
    const {
      isPopUp,
      activeTestament,
      newTestamentLocalActive,
      oldTestamentLocalActive,
      isDisabledSelects,
      isNothingSelect,
      dataFilter,
    } = this.props;

    const { chosenTestament } = this.state;
    let currentActiveTestament;

    // Для адаптива
    if (isPopUp) {
      if (activeTestament !== null) {
        currentActiveTestament = activeTestament;
      } else {
        currentActiveTestament = chosenTestament;
      }

      if (dataFilter.testament_id >= 1) {
        this.changeDataMarkers(dataFilter.menu, dataFilter.testament_id);
      }

      this.setState({
        chosenTestament: currentActiveTestament,
        activeTestament,
        newTestamentLocalActive,
        oldTestamentLocalActive,
        isDisabled: isDisabledSelects,
        isNothingCurrent: isNothingSelect,
      });
    }
  }

  // Для адаптива, при закрытии попапа

  componentWillReceiveProps(prevProps) {
    const { dataFilter } = this.props;

    if (dataFilter.title !== prevProps.dataFilter.title) {
      this.changeSettingsFilter(prevProps.dataFilter);
    }
  }

  componentWillUnmount() {
    const {
      activeTestament,
      newTestamentLocalActive,
      oldTestamentLocalActive,
      isDisabled,
      isNothingCurrent,
    } = this.state;
    const { saveAdaptiveParams } = this.props;

    if (saveAdaptiveParams !== undefined) {
      saveAdaptiveParams(
        activeTestament,
        newTestamentLocalActive,
        oldTestamentLocalActive,
        isDisabled,
        isNothingCurrent,
      );
    }
  }

  getAllMarkers = () => {
    const { markers, getDataMarkers, bookId } = this.props;
    if (bookId) {
      window.history.pushState(null, '', '/bibleyskaya-karta/');
    }
    this.setState({
      oldTestamentLocalActive: null,
      newTestamentLocalActive: null,
      activeTestament: null,
      isDisabled: true,
    });

    const isNothing = !markers.markersData.length;

    this.setState({
      isNothingAll: isNothing,
      isNothingBook: false,
      isNothingCurrent: false,
    });
    getDataMarkers(markers.markersData, 3, true);
  };

  changeSettingsFilter = dataFilter => {
    const { typeTestament } = this.state;

    if (dataFilter.testament_id === 2) {
      this.setState({
        activeTestament: typeTestament[0],
        chosenTestament: typeTestament[0],
        newTestamentLocalActive: { value: dataFilter.id, label: dataFilter.menu },
        isDisabled: false,
      });
      this.changeDataMarkers(dataFilter.menu, dataFilter.testament_id);
    } else if (dataFilter.testament_id === 1) {
      this.setState({
        activeTestament: typeTestament[1],
        chosenTestament: typeTestament[1],
        oldTestamentLocalActive: { value: dataFilter.id, label: dataFilter.menu },
        isDisabled: false,
      });
      this.changeDataMarkers(dataFilter.menu, dataFilter.testament_id);
    }
  };

  selectChangeNew = e => {
    const { closeMarker } = this.props;

    this.setState({ newTestamentLocalActive: e });
    this.changeDataMarkers(e.label, e.value);
    closeMarker();
  };

  selectChangeOld = e => {
    const { closeMarker } = this.props;

    this.setState({ oldTestamentLocalActive: e });
    this.changeDataMarkers(e.label, e.value);
    closeMarker();
  };

  selectChange = e => {
    const { closeMarker } = this.props;

    this.setState({ activeTestament: e, chosenTestament: e, isDisabled: false });

    if (e.label === 'Новый завет') {
      this.changeTestament(2);
    } else {
      this.changeTestament(1);
    }

    closeMarker();
  };

  changeDataMarkers = (menuName, typeTestament) => {
    const { markers, getDataMarkers } = this.props;
    const chosenMarkers = [];

    // MarkersData - array, элемент содержит verses
    // В каждом элемент массива verses маркер
    // Добавляем marker, а не verse, потому что в нем координаты

    markers.markersData.forEach(marker => {
      marker.verses.forEach(verse => {
        if (verse.book.menu === menuName) {
          chosenMarkers.push(marker);
        }
      });
    });

    const isNothing = !chosenMarkers.length;

    this.setState({
      isNothingCurrent: isNothing,
      isNothingBook: false,
      isNothingAll: false,
    });
    getDataMarkers(chosenMarkers, typeTestament, true);
  };

  changeTestament = testament => {
    const { markers, getDataMarkers } = this.props;
    const chosenMarkers = [];

    // Если в массиве marker есть Новый/Ветхий завет - пушим

    markers.markersData.forEach(marker => {
      if (marker.verses.some(verse => verse.book.testament_id === testament)) {
        chosenMarkers.push(marker);
      }
    });

    const isNothing = !chosenMarkers.length;

    this.setState({
      isNothingBook: isNothing,
      isNothingCurrent: false,
      isNothingAll: false,
    });
    getDataMarkers(chosenMarkers, testament, true);
  };

  submitAdaptiveForm = () => {
    const { screen, closeMenu } = this.props;

    !screen.desktop && closeMenu();
  };

  render() {
    const { isPopUp, screen } = this.props;
    const {
      typeTestament,
      activeTestament,
      newTestamentLocalActive,
      oldTestamentLocalActive,
      isDisabled,
      chosenTestament,
      isNothingCurrent,
      isNothingAll,
      isNothingBook,
    } = this.state;

    const { newTestament, oldTestament } = this.props;

    const newZavet = 'Новый завет';
    return (
      <div className={cx('bible-map__btns-wrapper', { biblePopUp: isPopUp })}>
        <div className="bible-map__btns-container">
          <div className="bible-map__select-type bible-map__select">
            <Select
              isSearchable={false}
              options={typeTestament}
              value={activeTestament}
              placeholder="Выберите завет"
              onChange={this.selectChange}
              classNamePrefix="custom-select"
            />
            {isNothingBook && (
              <div className="bible-map__select-nothing bible-map__nothing-book">
                <p className="bible-map__nothing-text">По вашему запросу ничего не найдено</p>
              </div>
            )}
          </div>
          <div className="bible-map__select-testament bible-map__select">
            {chosenTestament.label === newZavet ? (
              <Select
                isSearchable={false}
                options={oldTestament}
                placeholder="Выберите книгу"
                onChange={this.selectChangeNew}
                value={newTestamentLocalActive}
                classNamePrefix="custom-select"
                isDisabled={isDisabled}
              />
            ) : (
              <Select
                isSearchable={false}
                placeholder="Выберите книгу"
                options={newTestament}
                onChange={this.selectChangeOld}
                value={oldTestamentLocalActive}
                classNamePrefix="custom-select"
                isDisabled={isDisabled}
              />
            )}
            {isNothingCurrent && (
              <div className="bible-map__select-nothing">
                <p className="bible-map__nothing-text">По вашему запросу ничего не найдено</p>
              </div>
            )}
          </div>
        </div>
        <Fragment>
          <div className="bible-map__btn-adaptive-container">
            <div className="bible-map__btn-wrapper">
              {screen.desktop || screen.tabletMedium ? (
                <button className="btn btn-showAll" onClick={this.getAllMarkers}>
                  Показать все
                </button>
              ) : (
                <div className="bible-map__btn-adaptive">
                  <button
                    className="btn btn-showAll bible-map__btn-reset"
                    onClick={this.getAllMarkers}>
                    Сбросить фильтры
                  </button>
                  <button
                    className="btn-showAll-adaptive"
                    disabled={activeTestament === null}
                    onClick={this.submitAdaptiveForm}>
                    Показать
                  </button>
                </div>
              )}
            </div>
            {isNothingAll && (
              <div className="bible-map__select-nothing bible-map__nothing-all">
                <p className="bible-map__nothing-text">По вашему запросу ничего не найдено</p>
              </div>
            )}
          </div>
        </Fragment>
      </div>
    );
  }
}

export default BibleMapSelects;
