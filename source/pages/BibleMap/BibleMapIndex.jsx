import React, { PureComponent } from 'react';
// import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import cx from 'classnames';

import './styles.scss';
import '../../components/NavTestament/navTestament.scss';

import { googleAPIKey, setAnimatedScrollTo } from '../../dist/utils';

import {
  getNewTestamentSelect,
  getOldTestamentSelect,
  getTestamentIsLoading,
} from '../../dist/selectors/Bible';
import getScreen from '../../dist/selectors/screen';
import { markers, getPointsByBookSelector } from '../../dist/selectors/BibleMap';
import bibleActions from '../../dist/actions/Bible';
import { getAllMarkers } from '../../dist/actions/BibleMap';
import { getMetaData, setClearMeta } from '../../store/metaData/actions';

import BibleMapMain from './BibleMapMain';
import Preloader from '../../components/Preloader/Preloader';
import BibleMapSearch from './BibleMapSearch';
import AnimateComponent from '../../components/AnimateComponent/SlideLeftRight';
import BibleMapSelects from './BibleMapSelects';
import bookPopupIcon from './images/bookPopup.svg';
import { getCurrentTranslate } from '../../dist/selectors/translates';
import { setMetaData } from '../../store/metaData/selector';

const backArrow = require('../../components/NavTestament/images/backArrow.svg');

class BibleMap extends PureComponent {
  static propTypes = {
    newTestament: PropTypes.array.isRequired,
    oldTestament: PropTypes.array.isRequired,
    markers: PropTypes.object.isRequired,
  };

  state = {
    typeTestament: [
      {
        value: 1,
        label: 'Ветхий завет',
      },
      {
        value: 2,
        label: 'Новый завет',
      },
    ],
    isOpen: false,
    isOpenMarker: false,
    dataMarkers: null,
    localMarkers: [],
    isMarkers: false,
    typeMarkers: 3,
    activeTestament: null,
    newTestamentLocalActive: null,
    oldTestamentLocalActive: null,
    isDisabledSelects: true,
    centerMarker: null,
    dataFilter: {
      title: 'Не выбрано',
    },
    zoom: 3,
    isZoom: false,
    isChangedData: false,
    isNothingSelect: false,
    pointId: null,
  };

  componentDidMount() {
    setAnimatedScrollTo(document.querySelector('.bible-map'));

    const {
      oldTestament,
      newTestament,
      getBooks,
      getAllMarkers,
      getTitle,
      location,
      getMetaData,
      metaData,
      location: { pathname },
    } = this.props;

    if (metaData?.title) {
      getMetaData(pathname);
    }

    getAllMarkers();

    const [, , pointId] = location.pathname.split('/');

    getTitle && getTitle('Библейские карты');

    if (!oldTestament.length || !newTestament.length) {
      getBooks();
    }

    if (pointId) {
      this.setState({
        pointId: Number(pointId),
      });
    }
  }

  componentWillUnmount = () => {
    this.props.setClearMeta();
  };

  // typeMarkers (1 - Ветхий, 2 - Новый, 3 - Все)

  getDataMarkers = (data, type, isZoom) => {
    this.setState({
      isChangedData: true,
    });

    if (isZoom) {
      this.setState({ isZoom: true, zoom: 2 });
    }

    setTimeout(() => {
      this.setState({
        isChangedData: false,
        isMarkers: true,
        typeMarkers: type,
        localMarkers: data,
      });
    }, 500);
  };

  breadItems = () => [
    { path: '/', title: 'Главная' },
    { path: '', title: 'Библейские карты' },
  ];

  saveAdaptiveParams = (mainTestament, newTestament, oldTestament, isDisabled, isNothing) => {
    this.setState({
      activeTestament: mainTestament,
      newTestamentLocalActive: newTestament,
      oldTestamentLocalActive: oldTestament,
      isDisabledSelects: isDisabled,
      isNothingSelect: isNothing,
    });
  };

  openMenu = () => {
    this.setState({
      isOpen: true,
    });
  };

  closeMenu = () => {
    this.setState({
      isOpen: false,
    });
  };

  openMarker = data => {
    this.setState({
      isOpenMarker: true,
      dataMarkers: data,
      centerMarker: { lat: data.lat, lng: data.lon },
      zoom: 7,
      isZoom: false,
    });
  };

  openPointIdMarker = data => {
    this.setState({
      isOpenMarker: true,
      dataMarkers: data,
    });
  };

  closeMarker = () => this.setState({ isOpenMarker: false });

  setFilterMarkers = markerData => {
    this.getCurrentSelects(markerData);
    this.setState({ dataFilter: markerData, isZoom: true, zoom: 4 });
  };

  // Костыль для адаптива :(

  getCurrentSelects = data => {
    const { typeTestament } = this.state;

    if (data.testament_id === 1) {
      this.setState({
        activeTestament: typeTestament[0],
        oldTestamentLocalActive: { value: data.id, label: data.menu },
        isDisabledSelects: false,
      });
      this.changeDataMarkers(data.menu, data.testament_id);
    } else {
      this.setState({
        activeTestament: typeTestament[1],
        newTestamentLocalActive: { value: data.id, label: data.menu },
        isDisabledSelects: false,
      });
      this.changeDataMarkers(data.menu, data.testament_id);
    }
  };

  changeDataMarkers = (markerName, testament) => {
    const { markers } = this.props;
    const chosenMarkers = [];

    markers.markersData.forEach(marker => {
      marker.verses.forEach(verse => {
        if (verse.book.menu === markerName) {
          chosenMarkers.push(marker);
        }
      });
    });

    this.getDataMarkers(chosenMarkers, testament, true);
  };

  render() {
    const {
      oldTestament,
      newTestament,
      screen,
      markers,
      getAllMarkers,
      getTestamentIsLoading,
      currentTranslate,
    } = this.props;

    const {
      isOpen,
      isOpenMarker,
      dataMarkers,
      localMarkers,
      isMarkers,
      typeMarkers,
      activeTestament,
      newTestamentLocalActive,
      oldTestamentLocalActive,
      isDisabledSelects,
      centerMarker,
      dataFilter,
      zoom,
      isZoom,
      isChangedData,
      isNothingSelect,
      pointId,
    } = this.state;

    return (
      <div className="page bible-map">
        <div className="wrapper wrapper__header">{/* <Helmet title="Библейские карты" /> */}</div>
        <div className="page__body">
          <div className="wrapper wrapper_two-col not_padding">
            {!newTestament.length ||
            !oldTestament.length ||
            getTestamentIsLoading ||
            markers.isFetching ? (
              <Preloader />
            ) : (
              <>
                <BibleMapSearch
                  isOpen={isOpenMarker}
                  closeMarker={this.closeMarker}
                  data={dataMarkers}
                  markers={markers}
                  isMarkersLocal={isMarkers}
                  localMarkers={localMarkers}
                  openMarker={this.openMarker}
                  typeMarkers={typeMarkers}
                  setFilterMarkers={this.setFilterMarkers}
                  screen={screen}
                  currentTranslate={currentTranslate}
                />
                {(screen.desktop || screen.tabletMedium) && (
                  <BibleMapSelects
                    newTestament={newTestament}
                    oldTestament={oldTestament}
                    closeMarker={this.closeMarker}
                    getAllMarkers={getAllMarkers}
                    markers={markers}
                    isPopUp={false}
                    getDataMarkers={this.getDataMarkers}
                    dataFilter={dataFilter}
                    screen={screen}
                    closeMenu={this.closeMenu}
                    changeBookId={this.changeBookId}
                  />
                )}
                {!screen.desktop && !screen.tabletMedium && (
                  <>
                    <button className="bible-map__btn-popup" onClick={this.openMenu}>
                      <img
                        src={bookPopupIcon}
                        className="bible-map__btn-popup-icon"
                        alt="Открыть"
                      />
                    </button>
                    <AnimateComponent show={isOpen} className="testament__book-wrapper paper">
                      <div className="testament__back">
                        <button onClick={this.closeMenu}>
                          <img src={backArrow} alt="menu" />
                        </button>
                      </div>
                      <div className="bible-map__select-popup">
                        <BibleMapSelects
                          newTestament={newTestament}
                          oldTestament={oldTestament}
                          closeMarker={this.closeMarker}
                          getAllMarkers={getAllMarkers}
                          markers={markers}
                          isPopUp
                          closeMenu={this.closeMenu}
                          getDataMarkers={this.getDataMarkers}
                          activeTestament={activeTestament}
                          newTestamentLocalActive={newTestamentLocalActive}
                          oldTestamentLocalActive={oldTestamentLocalActive}
                          isNothingSelect={isNothingSelect}
                          isDisabledSelects={isDisabledSelects}
                          saveAdaptiveParams={this.saveAdaptiveParams}
                          dataFilter={dataFilter}
                          screen={screen}
                          changeStateMarkers={this.changeStateMarkers}
                        />
                      </div>
                    </AnimateComponent>
                  </>
                )}
                <div
                  className={cx('bible-map__container', {
                    'bible-map__container-animation': isChangedData,
                  })}>
                  <BibleMapMain
                    isMarkerShown
                    openPointIdMarker={this.openPointIdMarker}
                    googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${googleAPIKey}&v=3.exp&libraries=geometry,drawing,places`}
                    loadingElement={<div style={{ height: 530 }} />}
                    containerElement={<div style={{ height: 530 }} />}
                    mapElement={<div style={{ height: 530 }} />}
                    markers={markers.markersData}
                    isMarkersLocal={isMarkers}
                    localMarkers={localMarkers}
                    centerMarker={centerMarker}
                    openMarker={this.openMarker}
                    zoomFilter={zoom}
                    isZoomFilter={isZoom}
                    pointId={pointId}
                  />
                </div>
              </>
            )}
            <div className="bible-map__hidden">
              {markers.markersData.map(({ title, description, id }) => (
                <React.Fragment key={id}>
                  <span> {title} </span>
                  <div dangerouslySetInnerHTML={{ __html: description }} />
                </React.Fragment>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  getTestamentIsLoading: getTestamentIsLoading(state),
  newTestament: getNewTestamentSelect(state),
  oldTestament: getOldTestamentSelect(state),
  screen: getScreen(state),
  markers: markers(state),
  currentTranslate: getCurrentTranslate(state),
  pointsByBookSelector: getPointsByBookSelector(state),
  metaData: setMetaData(state),
});

const mapDispatchToProps = {
  ...bibleActions,
  getAllMarkers,
  getMetaData,
  setClearMeta,
};

export default connect(mapStateToProps, mapDispatchToProps)(BibleMap);
