export default function getMetaTitle(props, activeGroup) {
  let titleMeta = 'О Библии';
  const { lectureGroup } = props;
  if (lectureGroup && lectureGroup.categories && activeGroup) {
    const metaDefault = lectureGroup.categories.find(({ code }) => code === activeGroup);
    if (metaDefault) {
      const { title = 'О Библии' } = metaDefault;

      titleMeta = title;
    }
  }

  return titleMeta;
}
