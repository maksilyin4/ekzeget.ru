import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Select from 'react-select';
import { Switch, Route, Link } from 'react-router-dom';
import cx from 'classnames';

import lectureActions from '../../dist/actions/Lecture';
import {
  getLectureGroupLoading,
  lectureGroup,
  getLectureGroupListLoading,
  lectureGroupList,
  alphSortLectureList,
  lectureSelect,
  setIsErrorLectureDetail,
  setIsErrorLectureGroupList,
} from '../../dist/selectors/Lecture';
import { getIsTabletMedium, getIsDesktop } from '../../dist/selectors/screen';
import { getMetaData, setClearMeta } from '../../store/metaData/actions';
import { setMetaData } from '../../store/metaData/selector';

import Preloader from '../../components/Preloader/Preloader';
import { smoothScrollTo } from '../../dist/utils';
import NotFound from '../NotFound/NotFound';
import LectureDetail from './LectureDetail';
import LectureEaster from './LectureEaster';
import OrphusInfo from '../../components/OrphusInfo';

import getMetaTitle from './utilsLecture';

import './lecture.scss';

// TODO CurrentSelect

class Lecture extends Component {
  static propTypes = {
    location: PropTypes.object,
    getLectureGroupLoading: PropTypes.bool,
    lectureGroup: PropTypes.object,
    getLectureGroupListLoading: PropTypes.bool,
    lectureGroupList: PropTypes.object,
    isTabletMedium: PropTypes.bool.isRequired,
    isDesktop: PropTypes.bool.isRequired,
  };

  state = {
    activeGroup: this.props.location.pathname.split('/')[3] || '',
    currentSelect: null,
  };

  componentDidMount = async () => {
    smoothScrollTo();
    const {
      location: { pathname },
      metaData,
      getTitle,
      isIsErrorLectureGroupList,
      isErrorLectureDetail,
    } = this.props;

    if (!(isIsErrorLectureGroupList || isErrorLectureDetail)) {
      const groupId = this.props.location.pathname.split('/')[3] || 'o-biblii';

      await Promise.all([
        this.getLectureGroup(),
        this.getLectureGroupList(groupId),
        this.setMetaData({ pathname, metaData, groupId, getTitle }),
      ]);

      this.setActiveGroup({ groupId, pathname });
    }
  };

  componentDidUpdate(prevProps, prevState) {
    const {
      location: { pathname },
      lectureGroupList,
      lectureSelectType,
      getLectureGroupList,
      metaData,
      isErrorLectureDetail,
      lectureDetailClearError,
      cleartLectureGroupList,
      isIsErrorLectureGroupList,
    } = this.props;

    const { activeGroup } = this.state;

    if (pathname !== prevProps.location.pathname) {
      if (isErrorLectureDetail) {
        this.getLectureGroup();
        lectureDetailClearError();
      }
      if (isIsErrorLectureGroupList) {
        this.getLectureGroup();
        cleartLectureGroupList();
      }

      this.setMeta(pathname);
    }

    if (lectureGroupList?.lecture !== prevProps.lectureGroupList?.lecture) {
      const lecturePath = pathname.split('/')[4];

      if (!lecturePath && activeGroup !== 'easter') {
        this.props.getLectureDetail(lectureGroupList?.lecture?.[0]?.code);
      }
    }

    if (prevProps.lectureSelectType !== lectureSelectType) {
      this.setState({
        activeGroup: lectureSelectType[0].value,
        currentSelect: lectureSelectType[0],
      });
    }

    if (prevProps.metaData !== metaData || pathname !== prevProps.location.pathname) {
      const title =
        metaData && metaData.h_one ? metaData.h_one : getMetaTitle(this.props, activeGroup);
      this.props.getTitle(title);
    }

    if (prevState.activeGroup !== activeGroup) {
      if (activeGroup !== 'easter') {
        getLectureGroupList(activeGroup);
      }
    }
  }

  componentWillUnmount = () => {
    this.props.setClearMeta();
  };

  getLectureGroupList = groupId => {
    const { lectureGroupList, getLectureGroupList } = this.props;
    if (
      groupId &&
      groupId !== 'easter' &&
      (!lectureGroupList?.lecture || lectureGroupList?.lecture.length === 0)
    ) {
      getLectureGroupList(groupId);
    }
  };

  getLectureGroup = () => {
    const { lectureGroup, getLectureGroup } = this.props;
    if (!lectureGroup.categories || lectureGroup.categories.length === 0) {
      return getLectureGroup();
    }
  };

  setMetaData = ({ pathname, metaData, groupId, getTitle }) => {
    // if (!metaData?.h_one) {
    this.setMeta(pathname);
    // }
    const title = metaData?.h_one || getMetaTitle(this.props, groupId);
    getTitle(title);
  };

  setActiveGroup = ({ groupId, pathname }) => {
    const { lectureSelectType, lectureGroup } = this.props;

    if (!pathname.includes('easter')) {
      const activeGroup = groupId || lectureGroup.categories[0].code;
      const currentSelect = groupId
        ? lectureSelectType.filter(i => i.value === groupId)
        : lectureSelectType[0];

      this.setState({
        activeGroup,
        currentSelect,
      });
    } else {
      this.setState({
        activeGroup: 'easter',
      });
    }
  };

  _handleChange = (field, value) => {
    this.setState({
      [field]: value,
    });
  };

  toggleSelect = e => {
    const {
      history,
      match: { path },
    } = this.props;

    if (e.value === 1) {
      history.push(`${path}easter/`);
      this.setState({ currentSelect: e, activeGroup: '' });
    } else {
      history.push(`${path}${e.value}/`);
      this.setState({ currentSelect: e, activeGroup: e.value });
    }
  };

  setMeta = pathname => {
    this.props.getMetaData(pathname);
  };

  render() {
    const {
      isTabletMedium,
      getLectureGroupLoading,
      lectureGroup,
      lectureSelectType,
      isDesktop,
      match: { path },
      metaData,
      isErrorLectureDetail,
      isIsErrorLectureGroupList,
    } = this.props;

    if (isErrorLectureDetail || isIsErrorLectureGroupList) {
      return <NotFound />;
    }

    const { activeGroup, currentSelect } = this.state;

    return (
      <div className="page lecture-page">
        <>
          <div className="page__body lecture">
            <div className="wrapper wrapper_two-col not_padding all_about_bible">
              <div className="side all-about-bible-side">
                {getLectureGroupLoading ? (
                  <Preloader />
                ) : (
                  <>
                    <div className="side__item nav-side">
                      {!isDesktop && !isTabletMedium ? (
                        <div className="select-adaptive">
                          <Select
                            isSearchable={false}
                            options={lectureSelectType}
                            value={currentSelect}
                            classNamePrefix="custom-select"
                            onChange={this.toggleSelect}
                          />
                        </div>
                      ) : (
                        <>
                          {lectureGroup.categories ? (
                            lectureGroup.categories.map((group, i) => (
                              <div key={group.id} className="list__item">
                                <Link
                                  className={cx('nav-side__link list__item-link', {
                                    active: activeGroup === group.code,
                                  })}
                                  onClick={() => this._handleChange('activeGroup', group.code)}
                                  to={`${path}${i !== 0 ? `${group.code}/` : ''}`}>
                                  {group.title}
                                </Link>
                              </div>
                            ))
                          ) : (
                            <p className="empty-content"> Отсутствуют данные </p>
                          )}
                          <div className="list__item">
                            <Link
                              className={cx('nav-side__link list__item-link', {
                                active: this.props.location.pathname.split('/')[3] === 'easter',
                              })}
                              onClick={() => this._handleChange('activeGroup', 'easter')}
                              to={`${path}easter/`}>
                              Вечная пасхалия
                            </Link>
                          </div>
                        </>
                      )}
                    </div>
                    {isDesktop && (
                      <div className="side__item">
                        <OrphusInfo />
                      </div>
                    )}
                  </>
                )}
              </div>
              <Switch>
                <Route
                  exact
                  path={`${path}`}
                  render={props =>
                    props.match.params.groupId !== 'easter' && (
                      <LectureDetail
                        h_one={metaData ? metaData.h_one : 'Православный лекторий'}
                        changeMetaData={this.getMeta}
                        activeGroup={activeGroup}
                        path={path}
                        {...props}
                      />
                    )
                  }
                />
                <Route
                  exact
                  path={[`${path}:groupId/`, `${path}:groupId/:lectureId/`]}
                  render={props =>
                    props.match.params.groupId !== 'easter' ? (
                      <LectureDetail
                        h_one={metaData ? metaData.h_one : 'Православный лекторий'}
                        changeMetaData={this.getMeta}
                        activeGroup={activeGroup}
                        path={path}
                        {...props}
                      />
                    ) : (
                      <LectureEaster
                        history={props.history}
                        location={props.location}
                        changeMetaData={this.setMeta}
                      />
                    )
                  }
                />
                <Route component={NotFound} />
              </Switch>
            </div>
          </div>
        </>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  getLectureGroupLoading: getLectureGroupLoading(state),
  lectureGroup: lectureGroup(state),
  getLectureGroupListLoading: getLectureGroupListLoading(state),
  lectureGroupList: lectureGroupList(state),
  alphSortLectureList: alphSortLectureList(state),
  isTabletMedium: getIsTabletMedium(state),
  isDesktop: getIsDesktop(state),
  lectureSelectType: lectureSelect(state),
  isErrorLectureDetail: setIsErrorLectureDetail(state),
  isIsErrorLectureGroupList: setIsErrorLectureGroupList(state),
  metaData: setMetaData(state),
});
const mapDispatchToProps = {
  ...lectureActions,
  getMetaData,
  setClearMeta,
};

export default connect(mapStateToProps, mapDispatchToProps)(Lecture);
