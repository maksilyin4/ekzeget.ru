import React, { Component } from 'react';
import dayjs from 'dayjs';

import Preloader from '../../components/Preloader/Preloader';
import TextField from '../../components/TextField/TextField';
import { AXIOS } from '../../dist/ApiConfig';

class LectureEaster extends Component {
  state = {
    isFetching: false,
    isCalculate: false,
    year: dayjs().format('YYYY'),
    celebrations: [
      {
        date: 'Отсутствует дата',
        title: 'Отсутствует описание',
      },
    ],
    calendar: '',
    isError: false,
  };

  componentDidMount() {
    const { changeMetaData } = this.props;
    changeMetaData('lecture-easter');
  }

  _handleChange = field => ({ target: { value } }) => {
    this.setState({ [field]: value < 10000 ? value : 9999 });
  };

  _easterCalc = async () => {
    const { year } = this.state;

    this.setState({
      isFetching: true,
      isCalculate: true,
    });

    if (year <= 0) {
      this.setState({
        isError: true,
        isFetching: false,
      });
    } else {
      this.setState({ isError: false });
      await AXIOS.post('/site/easter/', { year: +year }).then(
        ({ data: { calendar_type, celebrates, status } }) => {
          if (status === 'ok') {
            this.setState({
              isFetching: false,
              celebrations: celebrates,
              calendar: calendar_type,
            });
          }
        },
        () => {
          this.setState({
            isFetching: false,
          });
        },
      );
    }
  };

  render() {
    const { isFetching, isCalculate, year, celebrations, calendar, isError } = this.state;

    return (
      <div className="content easter">
        <div className="easter-content">
          <div className="easter__form">
            <TextField
              type="number"
              name="date"
              label="Введите год в промежутке с 1 по 9999"
              required
              onChange={this._handleChange('year')}
              defaultValue={year}
              value={String(this.state.year)}
              max={9999}
              min={1}
              maxLength={4}
            />
            <button className="learn-btn" onClick={this._easterCalc}>
              Рассчитать
            </button>
          </div>
          {!isCalculate ? (
            <div className="easter__message">
              Пасха празднуется в первое воскресенье после первого весеннего полнолуния,
              наступившего не ранее дня весеннего равноденствия.
            </div>
          ) : isFetching ? (
            <Preloader />
          ) : !isError ? (
            <div className="easter__celebrations">
              <div className="easter__celebrations-calendar">{calendar}</div>
              {celebrations.map((celebration, i) => (
                // eslint-disable-next-line react/no-array-index-key
                <div key={i} className="easter__celebration">
                  <span className="easter__celebration-title">{celebration.title} - </span>
                  <span className="easter__celebration-value">{celebration.date}</span>
                </div>
              ))}
            </div>
          ) : (
            <div className="easter__celebrations">
              <p> Введите корректный год </p>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default LectureEaster;
