import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import cx from 'classnames';
import Select from 'react-select';

import lectureActions from '../../dist/actions/Lecture';
import {
  getLectureDetailLoading,
  lectureDetail,
  getLectureGroupListLoading,
  lectureGroupList,
  lectureGroupSelect,
  setIsErrorLectureDetail,
} from '../../dist/selectors/Lecture';
import { getIsDesktop } from '../../dist/selectors/screen';
import { getCurrentTranslate } from '../../dist/selectors/translates';
import { smoothScrollTo } from '../../dist/utils';

import List, { ListItem } from '../../components/List/List';
import Preloader from '../../components/Preloader/Preloader';
import Scrollbar from '../../components/Scrollbar';
import Helmet from '~components/Helmet';

class LectureDetail extends Component {
  static propTypes = {
    getLectureDetailLoading: PropTypes.bool,
    lectureDetail: PropTypes.object,
    getLectureGroupListLoading: PropTypes.bool,
    lectureGroupList: PropTypes.object,
    match: PropTypes.shape({
      params: PropTypes.shape({
        lectureId: PropTypes.string,
      }),
    }),
    lectureGroupSelect: PropTypes.shape({
      value: PropTypes.string,
      label: PropTypes.string,
    }),
    getLectureDetail: PropTypes.func,
    history: PropTypes.object,
    isDesktop: PropTypes.bool,
    currentTranslate: PropTypes.shape({
      code: PropTypes.string,
    }),
    h_one: PropTypes.string,
    path: PropTypes.string,
    activeGroup: PropTypes.string,
  };

  constructor(props) {
    super(props);

    this.state = {
      currentLectureGroup: null,
    };
  }

  componentDidMount() {
    const {
      lectureGroupSelect,
      getLectureDetail,
      lectureDetail,
      match: {
        params: { lectureId },
      },
      isErrorLectureDetail,
      activeGroup,
      lectureGroupList,
      getLectureGroupList,
    } = this.props;

    smoothScrollTo();

    if (!lectureGroupList?.lecture && activeGroup) {
      getLectureGroupList(activeGroup);
    }

    if (!isErrorLectureDetail) {
      if (lectureId) {
        (!lectureDetail.lecture || Object.keys(lectureDetail.lecture).length === 0) &&
          getLectureDetail(lectureId);
      }

      this.setState({
        currentLectureGroup:
          lectureGroupSelect.find(el => el.value === lectureId) || lectureGroupSelect[0],
      });
    }
  }

  componentDidUpdate = prevProps => {
    const {
      lectureGroupSelect,
      match: { params },
    } = this.props;

    const lectureId = params.lectureId || lectureGroupSelect[0]?.value;

    if (prevProps.match.params.lectureId !== params.lectureId) {
      if (params.lectureId) {
        this.props.getLectureDetail(lectureId);
      }
    }
    if (prevProps.lectureGroupSelect !== lectureGroupSelect) {
      //    this.props.getLectureDetail(lectureGroupSelect[0]?.value);
      this.setState({
        currentLectureGroup: lectureGroupSelect[0],
      });
    }

    if (prevProps.match.params.lectureId !== params.lectureId) {
      if (params.lectureId) {
        this.setState({
          currentLectureGroup: lectureGroupSelect.find(el => el.value === lectureId),
        });
      }
    }
  };

  componentWillUnmount = () => this.props.lectureDetailClear();

  toggleSelect = e => {
    const { history, match } = this.props;
    const {
      params: { groupId },
    } = match;
    history.push(`/all-about-bible/about-bible/${groupId}/${e.value}/`);
    this.setState({ currentLectureGroup: e });
  };

  renderView = ({ style }) => {
    const customStyle = {
      maxHeight: 'calc(100% + 5px)',
      overflow: 'hidden',
      overflowY: 'scroll',
    };
    return <div style={{ ...style, ...customStyle }} />;
  };

  render() {
    const {
      lectureDetail,
      getLectureDetailLoading,
      getLectureGroupListLoading,
      lectureGroupList,
      lectureGroupSelect,
      isDesktop,
      currentTranslate,
      h_one,
      path,
      activeGroup,
      match: { params },
    } = this.props;

    const { currentLectureGroup } = this.state;

    const lectureId = params.lectureId || lectureGroupSelect[0]?.value;

    return (
      <div className="all-about-bible-content paper">
        <div className="content_two-col lecture-content">
          {getLectureDetailLoading ? (
            <Preloader />
          ) : (
            <>
              <Helmet
                title={h_one}
                description={
                  'Православные просветительские лектории на библейские темы. Читать онлайн на сайте Экзегет.'
                }
                keywords={''}
              />
              <div className="all-about-bible-list">
                <div
                  className={cx('lecture__content-text text_justify', {
                    csya_old__container: currentTranslate?.code === 'csya_old',
                    grek__container: currentTranslate?.code === 'grek',
                  })}
                  dangerouslySetInnerHTML={{
                    __html: lectureDetail?.lecture.text,
                  }}
                />
              </div>
            </>
          )}

          <div className="side side_right lecture_list_width all-about-bible-rigth-select">
            {!getLectureGroupListLoading ? (
              <List className="side__interpretators lecture__interpretators all-about-bible-right-nav">
                {!isDesktop ? (
                  <Select
                    isSearchable={false}
                    options={lectureGroupSelect}
                    value={currentLectureGroup}
                    classNamePrefix="custom-select"
                    onChange={this.toggleSelect}
                  />
                ) : (
                  <>
                    <div className="side__interpretators-head lecture__interpretators-head">
                      {h_one || ''}
                    </div>
                    <div className="lecture__scrollbar-wrapper">
                      <Scrollbar renderView={this.renderView}>
                        {lectureGroupList.lecture &&
                          lectureGroupList.lecture.map((lecture, i) => {
                            const group = `${activeGroup}/`;
                            const activeDetail = lectureId ? lectureId === lecture?.code : i === 0;
                            return (
                              <ListItem
                                key={lecture.id}
                                title={lecture.title}
                                href={`${path}${group}${lecture?.code}/`}
                                className={cx('lecture__item', {
                                  active: activeDetail,
                                })}
                              />
                            );
                          })}
                      </Scrollbar>
                    </div>
                  </>
                )}
              </List>
            ) : (
              <Preloader />
            )}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  getLectureDetailLoading: getLectureDetailLoading(state),
  lectureDetail: lectureDetail(state),
  getLectureGroupListLoading: getLectureGroupListLoading(state),
  lectureGroupList: lectureGroupList(state),
  lectureGroupSelect: lectureGroupSelect(state),
  isDesktop: getIsDesktop(state),
  currentTranslate: getCurrentTranslate(state),
  isErrorLectureDetail: setIsErrorLectureDetail(state),
});

const mapDispatchToProps = {
  ...lectureActions,
};

export default connect(mapStateToProps, mapDispatchToProps)(LectureDetail);
