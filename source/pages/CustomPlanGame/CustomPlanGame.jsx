import React from 'react';
import MainPageGame from '~components/CustomPlanModule/Game/MainPage';
import CustomPlanBodyLayout from '~pages/CustomPlanDetail/Layout/CustomPlanBodyLayout';
import { Helmet } from 'react-helmet/es/Helmet';
import BreadHead from '../../components/BreadHead';
import ReadingPageTitle from '~components/PageTitle';
import { useMedia } from 'react-use';

const TITLE_READING_PLAN = 'План на 2023 год';

const CustomPlanGameResult = () => {
  const isMobile = useMedia('(max-width: 768px)');
  const bread = [
    { path: '/', title: 'Главная' },
    { path: '/reading-plan', title: 'План чтений' },
    { path: '', title: 'План на 2023 год' },
  ];

  const breadMobile = [
    { path: '/', title: 'Главная' },
    { path: '', title: 'План на 2023 год' },
  ];
  return (
    <div>
      <Helmet title={TITLE_READING_PLAN} description={''} keywords={''} />
      <div className="page">
        <div className="wrapper wrapper-main">
          <BreadHead bread={isMobile ? breadMobile : bread} isDisabledLearnVideo />
          <ReadingPageTitle>{TITLE_READING_PLAN}</ReadingPageTitle>
          <CustomPlanBodyLayout>
            <MainPageGame />
          </CustomPlanBodyLayout>
        </div>
      </div>
    </div>
  );
};

export default CustomPlanGameResult;
