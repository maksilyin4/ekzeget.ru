import Loadable from 'react-loadable';

const LoadableBar = Loadable({
  loader: () => import('./CustomPlanGameResult'),
  loading() {
    return null;
  },
  delay: 300,
});

export default LoadableBar;
