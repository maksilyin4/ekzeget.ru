import Loadable from 'react-loadable';

const LoadableBar = Loadable({
  loader: () => import('./CustomPlanGame'),
  loading() {
    return null;
  },
  delay: 300,
});

export default LoadableBar;
