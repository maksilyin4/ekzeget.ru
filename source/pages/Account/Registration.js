import React, {Component, memo} from 'react';
import PopupReg from "~components/Popups/Reg/Reg";
import BreadHead from "~components/BreadHead";
import PageHead from "~components/PageHead";

import './styles.scss';

class Registration extends Component {
    _breadItems() {
        return [
            {path: '/', title: 'Главная'},
            {path: '', title: 'Регистрация'},
        ];
    }

    render() {
        return (
            <div className="page">
              <div className="wrapper">
                  <BreadHead bread={this._breadItems()} />
                  <PageHead meta={{title: 'Регистрация || Экзегет.ру', h_one: 'Регистрация'}} h_one={'Регистрация'} />

                  <div className="form-wrapper">
                      <h1 className="mm-popup__box__header__title">Регистрация</h1>
                      <PopupReg onSuccess={() => this.props.history.replace('/')}/>
                  </div>
              </div>
            </div>
        );
  }
}

export default memo(Registration);
