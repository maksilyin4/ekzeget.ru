import React, {Component, memo} from 'react';
import PopupForgotPassword from "~components/Popups/ForgotPassword/ForgotPassword";
import BreadHead from "~components/BreadHead";
import PageHead from "~components/PageHead";

import './styles.scss';

class Registration extends Component {
    _breadItems() {
        return [
            {path: '/', title: 'Главная'},
            {path: '', title: 'Восстановление пароля'},
        ];
    }

    render() {
        return (
            <div className="page">
                <div className="wrapper">
                    <BreadHead bread={this._breadItems()} />
                    <PageHead meta={{title: 'Восстановление пароля || Экзегет.ру', h_one: 'Восстановление пароля'}} h_one={'Восстановление пароля'} />

                    <div className="form-wrapper">
                        <h1 className="mm-popup__box__header__title">Восстановить пароль</h1>
                        <PopupForgotPassword />
                    </div>
                </div>
            </div>
        );
    }
}

export default memo(Registration);
