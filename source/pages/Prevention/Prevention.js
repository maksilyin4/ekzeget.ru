import React from 'react';
import './prevention.scss';

const Prevention = () => (
  <div className="page">
    <div className="wrapper">
      <div className="prevention">
        <div className="prevention__icon">
          <img
            src="/assets/images/technical/prevention.png"
            alt="Экзегет"
            className="prevention__icon-img"
          />
        </div>
        <div className="prevention__title">На сайте ведутся профилактические работы</div>
        <div className="prevention__subtitle">
          Просим прощения за причиненные неудобства.
          <br />
          Работа сайта будет восстановлена в ближайшее время
        </div>
      </div>
    </div>
  </div>
);

export default Prevention;
