import React, { Component } from 'react';
import Select from 'react-select';

import AnimateComponent from '../../../components/AnimateComponent/SlideLeftRight';
import Button from '../../../components/Button/Button';
import Preloader from '../../../components/Preloader/Preloader';

const backArrow = require('../mocks/backArrow.svg');

class SubcribePlanMobile extends Component {
  state = {
    isOpen: false,
  };

  openMenu = () => {
    this.setState({
      isOpen: true,
    });
  };

  closeMenu = () => {
    this.setState({
      isOpen: false,
    });
  };

  render() {
    const { isOpen } = this.state;
    const {
      readingPlanSubscribe,
      translates,
      getCurrentTranslate,
      choseTranslate,
      disableSubscribe,
      enableSubscribe,
      userRPGetIsLoading,
    } = this.props;

    return (
      <div className="reading-plan-mobile__wrapper subscribe-plan-mobile__wrapper">
        <button onClick={this.openMenu} className="reading-plan-mobile__select">
          Подписаться на чтение
        </button>
        <AnimateComponent show={isOpen} className="reading-plan-mobile__select-wrapper paper">
          {!!userRPGetIsLoading && <Preloader className="preloader__whiteBG" />}
          <div className="reading-plan-mobile__back">
            <button onClick={this.closeMenu}>
              <img src={backArrow} alt="menu" />
            </button>
          </div>
          <div className="reading-plan__subscribe">
            <p className="reading-plan__subscribe-text">Подписаться на чтение</p>
            <p className="reading-plan__subscribe-adaptive-text">
              {readingPlanSubscribe === 1
                ? 'Вы подписаны на рассылку'
                : 'Я хочу ежедневно получать чтение на мой email'}
            </p>
            <div className="reading-plan__subscribe-action">
              {readingPlanSubscribe === 1 ? (
                <Button className="btn_complete" title="Отписаться" onClick={disableSubscribe} />
              ) : (
                <Button title="Подписаться" onClick={enableSubscribe} />
              )}
            </div>
            <div className="lk__rp__translate">
              <p className="lk__rp__text-translate">Перевод, который будет приходить на почту</p>
              <Select
                isSearchable={false}
                classNamePrefix="custom-select"
                options={translates}
                value={getCurrentTranslate()}
                onChange={choseTranslate}
              />
            </div>
          </div>
        </AnimateComponent>
      </div>
    );
  }
}

export default SubcribePlanMobile;
