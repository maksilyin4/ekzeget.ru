import React, { Component } from 'react';
import AnimateComponent from '../../../components/AnimateComponent/SlideLeftRight';
import ReadingPlanDays from './ReadingPlanDays';

const backArrow = require('../mocks/backArrow.svg');

class NavTestamentMobile extends Component {
  state = {
    isOpen: false,
  };

  openMenu = () => {
    this.setState({
      isOpen: true,
    });
  };

  closeMenu = () => {
    this.setState({
      isOpen: false,
    });
  };

  saveSubscribeSettings = value => {
    this.props.saveSubscribeSettings(value);
    this.closeMenu();
  };

  render() {
    const { isOpen } = this.state;
    const { selectedDays, handleCheckbox, selectedDaysIsFetching, userRPGetIsLoading } = this.props;

    return (
      <div className="reading-plan-mobile__wrapper">
        <button onClick={this.openMenu} className="reading-plan-mobile__select">
          Когда читать
        </button>
        <AnimateComponent show={isOpen} className="reading-plan-mobile__select-wrapper paper">
          <div className="reading-plan-mobile__back">
            <button onClick={this.closeMenu}>
              <img src={backArrow} alt="menu" />
            </button>
          </div>
          <ReadingPlanDays
            selectedDays={selectedDays}
            handleCheckbox={handleCheckbox}
            saveSubscribeSettings={this.saveSubscribeSettings}
            selectedDaysIsFetching={selectedDaysIsFetching}
            userRPGetIsLoading={userRPGetIsLoading}
          />
        </AnimateComponent>
      </div>
    );
  }
}

export default NavTestamentMobile;
