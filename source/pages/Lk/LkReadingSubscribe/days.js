export default [
  { value: 'Mon', title: 'Понедельник' },
  { value: 'Tue', title: 'Вторник' },
  { value: 'Wed', title: 'Среда' },
  { value: 'Thu', title: 'Четверг' },
  { value: 'Fri', title: 'Пятница' },
  { value: 'Sat', title: 'Суббота' },
  { value: 'Sun', title: 'Воскресение' },
];
