import R from 'ramda';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Select from 'react-select';

import Button from '../../../components/Button/Button';
import Preloader from '../../../components/Preloader/Preloader';
import readingPlanActions from '../../../dist/actions/ReadingPlan';
import { userRPGetIsLoading, getUserPlans } from '../../../dist/selectors/ReadingPlan';
import {
  getTranslatesForSelect,
  getTranslatesForSelectLk,
} from '../../../dist/selectors/translates';
import getScreen from '../../../dist/selectors/screen';
import ReadingPlanDays from './ReadingPlanDays';
import ReadingPlanDaysMobile from './ReadingPlanDaysMobile';
// import { sendSocialActions } from '../../../dist/utils';

import days from './days';
import SubcribePlanMobile from './SubcribePlanMobile';

class LkReadingSubscribe extends Component {
  state = {
    translate_id: 1,
    selectedDays: R.pathOr({}, ['schedule'], this.props.userPlans),
    selectedDaysIsFetching: false,
  };

  componentWillReceiveProps(nextProps) {
    if (R.isEmpty(this.props.userPlans) && !R.isEmpty(nextProps.userPlans)) {
      this.setState({
        translate_id: R.pathOr(1, ['translate_id'], nextProps.userPlans),
        selectedDays: R.pathOr({}, ['schedule'], nextProps.userPlans),
      });
    } else {
      this.setState({
        translate_id: R.pathOr(this.props.userPlans, ['translate_id'], nextProps.userPlans),
        selectedDays: R.pathOr({}, ['schedule'], nextProps.userPlans),
      });
    }
  }

  // TODO translates был. Править фильтрацию.
  getCurrentTranslate = () => this.props.translatesLk.find(i => i.id === this.state.translate_id);

  enableSubscribe = () => {
    this._readingPlanSubscribe(1);
    // sendSocialActions({
    //   ga: [
    //     'event',
    //     'event_name',
    //     { event_category: 'reading_plan', event_action: 'on' }],
    //   ym: 'reading_plan',
    // });
  };

  disableSubscribe = () => {
    this._readingPlanSubscribe(0);
    // sendSocialActions({
    //   ga: [
    //     'event',
    //     'event_name',
    //     { event_category: 'disable_plan', event_action: 'off' }],
    //   ym: 'disable_plan',
    // });
  };

  saveSubscribeSettings = () => {
    const id = R.pathOr('', ['id'], this.props.userPlans);
    this.setState({ selectedDaysIsFetching: true });
    this.props.saveSelectedDays(this.state.selectedDays, id).then(() => {
      this.setState({ selectedDaysIsFetching: false });
    });
  };

  _readingPlanSubscribe = async subscribe => {
    const { plan_id } = this.props;
    const { translate_id = 1 } = this.state;
    const data = {
      subscribe,
      plan_id,
      translate_id,
    };

    await this.props.addUserReadingPlan(data);
    await this.props.getUserReadingPlan();
  };

  handleCheckbox = (day, active) => {
    const selectedDays = {
      ...this.state.selectedDays,
      [day]: active,
    };
    if (days.some(({ value }) => selectedDays[value])) {
      this.setState({
        selectedDays,
      });
    }
  };

  choseTranslate = e => {
    this.setState(
      {
        translate_id: e.id,
      },
      () => {
        // если пользователь подписан, то обновляем подписку
        if (R.pathOr(0, ['subscribe'], this.props.userPlans)) {
          this.enableSubscribe();
        }
      },
    );
  };

  render() {
    const { userRPGetIsLoading, userPlans, screen, translatesLk } = this.props;

    const { selectedDays, selectedDaysIsFetching } = this.state;
    const readingPlanSubscribe = R.pathOr(0, ['subscribe'], userPlans);
    const screenDesktop = screen.tabletMedium || screen.desktop;
    return (
      <>
        <div className="side__item nav-side paper lk__side-two">
          {!!userRPGetIsLoading && <Preloader className="preloader__whiteBG" />}
          {/* TODO remove false after build API */}
          {(screen.phone || screen.tablet) && (
            <>
              <ReadingPlanDaysMobile
                selectedDays={selectedDays}
                handleCheckbox={this.handleCheckbox}
                saveSubscribeSettings={this.saveSubscribeSettings}
                selectedDaysIsFetching={selectedDaysIsFetching}
                userRPGetIsLoading={userRPGetIsLoading}
              />
              {/* TODO translates был! Править фильтрацию */}
              <SubcribePlanMobile
                readingPlanSubscribe={readingPlanSubscribe}
                translates={translatesLk}
                getCurrentTranslate={this.getCurrentTranslate}
                choseTranslate={this.choseTranslate}
                disableSubscribe={this.disableSubscribe}
                enableSubscribe={this.enableSubscribe}
                userRPGetIsLoading={userRPGetIsLoading}
              />
            </>
          )}
          {screenDesktop && (
            <div className="reading-plan__subscribe">
              <p className="reading-plan__subscribe-text">
                {readingPlanSubscribe === 1
                  ? 'Вы подписаны на рассылку'
                  : 'Я хочу ежедневно получать чтение на мой email'}
              </p>
              <div className="reading-plan__subscribe-action">
                {readingPlanSubscribe === 1 ? (
                  <Button
                    className="btn_complete"
                    title="Отписаться"
                    onClick={this.disableSubscribe}
                  />
                ) : (
                  <Button title="Подписаться" onClick={this.enableSubscribe} />
                )}
              </div>
              <div className="lk__rp__translate">
                <p className="lk__rp__text-translate">Перевод, который будет приходить на почту</p>
                {/* TODO translates был! Править фильтрацию */}
                <Select
                  isSearchable={false}
                  classNamePrefix="custom-select"
                  options={translatesLk}
                  value={this.getCurrentTranslate()}
                  onChange={this.choseTranslate}
                />
              </div>
            </div>
          )}
          {/* TODO remove false after build API */}
        </div>
        {screenDesktop && (
          <ReadingPlanDays
            selectedDays={selectedDays}
            handleCheckbox={this.handleCheckbox}
            saveSubscribeSettings={this.saveSubscribeSettings}
            selectedDaysIsFetching={selectedDaysIsFetching}
            userRPGetIsLoading={userRPGetIsLoading}
          />
        )}
      </>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const { plan_id } = ownProps?.match?.params || {};
  return {
    userPlans: getUserPlans(state, plan_id),
    userRPGetIsLoading: userRPGetIsLoading(state),
    translates: getTranslatesForSelect(state),
    translatesLk: getTranslatesForSelectLk(state),
    screen: getScreen(state),
    plan_id,
  };
};

const mapDispatchToProps = {
  ...readingPlanActions,
};

export default connect(mapStateToProps, mapDispatchToProps)(LkReadingSubscribe);
