import React, { Component } from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';
import Button from '../../../components/Button/Button';
import days from './days';

class ReadingPlanDays extends Component {
  static propTypes = {
    selectedDays: PropTypes.object,
    handleCheckbox: PropTypes.func,
    saveSubscribeSettings: PropTypes.func,
  };

  render() {
    const {
      selectedDays,
      handleCheckbox,
      saveSubscribeSettings,
      selectedDaysIsFetching,
      userRPGetIsLoading,
    } = this.props;

    return (
      <div className="reading-plan__days">
        <p className="reading-plan__days-header">Когда читать?</p>
        <div className="reading-plan__days-select">
          {days.map(({ value, title }) => (
            <div
              key={value}
              className="reading-plan__days-select-item"
              onClick={() => {
                handleCheckbox(value, selectedDays[value] ? 0 : 1);
              }}>
              <input type="checkbox" />
              <span
                className={cx('reading-plan__days-select-checkbox', {
                  active: !!selectedDays[value],
                })}>
                {title}
              </span>
            </div>
          ))}
        </div>
        <div className="reading-plan__days-button-wrapper">
          <Button title="Сохранить" onClick={saveSubscribeSettings} />
        </div>
        {(selectedDaysIsFetching || userRPGetIsLoading === 1) && (
          <div className="reading-plan__days-preload-wrapper" />
        )}
      </div>
    );
  }
}

export default ReadingPlanDays;
