import React, { useRef, useState } from 'react';
import cx from 'classnames';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import Preloader from '../../../components/Preloader/Preloader';
import { sendSocialActions } from '../../../dist/utils';
import { userData, userLoggedIn } from '../../../dist/selectors/User';
import { Popup } from '../../../dist/browserUtils';
import PopupAuth from '../../../components/Popups/Auth/Auth';
import { kickUser } from '~dist/api/readingPlan/kickUser';
import deleteUser from '~assets/images/remove-plan.png';
import { deleteUserReadingPlanSuccess } from '../../../dist/actions/ReadingPlan';

const ReadinPlanItem = React.memo(
  ({
    deleteUserReadingPlan,
    addUserReadingPlan,
    planId,
    getPlanDetail,
    isFetching,
    deleteGroupThunk,
    myPlans,
    groups,
    description,
    deleteReadingPlan,
    isCreatedPlan,
    isMyPlan,
    handleWithGroupClick,
    closedDaysLength,
    comment,
  }) => {
    const logIn = useSelector(userLoggedIn);
    const [deletedGroup, setDeletedGroup] = useState(false);
    const userGroup = groups?.userGroup;
    const dispatch = useDispatch();
    const mentorGroup = groups?.mentorGroup;

    const group = userGroup?.plan_id === planId ? userGroup : null;
    const isUserGroup = userGroup.plan_id === planId && !deletedGroup;
    const isMentorGroup = mentorGroup.plan_id === planId && !deletedGroup;
    const hasGroup = Boolean(group?.id) && !deletedGroup;
    const [kicked, setKicked] = useState(false);
    const userMe = useSelector(userData);

    const addRP = e => {
      if (logIn) {
        if (userGroup.id && !hasGroup) {
          e.preventDefault();
          handleWithGroupClick();
          return;
        }
        if (Object.values(myPlans)[0]?.id) {
          e.preventDefault();
          handleWithGroupClick();
          return;
        }
        addUserReadingPlan({ plan_id: planId });
        sendSocialActions({
          ga: ['event', 'event_name', { event_category: 'reading_plan', event_action: 'on' }],
          ym: 'reading_plan',
        });
      } else if (Popup) {
        Popup.create(
          {
            title: 'Вход',
            content: <PopupAuth />,
            className: 'popup_auth not-header',
          },
          true,
        );
      }
    };

    const deleteRP = () => {
      deleteUserReadingPlan(planId);
      sendSocialActions({
        ga: ['event', 'event_name', { event_category: 'disable_plan', event_action: 'off' }],
        ym: 'disable_plan',
      });
    };

    const receivePlanDetail = () => {
      getPlanDetail(description, planId, group);
    };

    const ref = useRef(null);

    const handleDeletePlan = async () => {
      if (isMyPlan) {
        await deleteRP();
      }
      if (group || mentorGroup) {
        await deleteGroupThunk();
      }
      await deleteReadingPlan(planId);
    };

    const isBlockingSubscribe =
      (userGroup.id && !hasGroup) || (!isMyPlan && Object.values(myPlans).length);

    const canDelete = group ? group?.users?.length < 2 && isMentorGroup : false;

    return (
      <div className={cx('lk__rp-item-wrapper', isCreatedPlan && 'lk__my-created-plan')}>
        {((isCreatedPlan && !isUserGroup) || isMentorGroup) && (
          <div className={cx('lk__head-my-plan', isMentorGroup && 'lk__head-my-plan-mentor')}>
            {isMentorGroup ? '' : 'Мой план'}
            {isCreatedPlan && (group ? canDelete < 2 : true) && (
              <img onClick={handleDeletePlan} className="delete-plan" src={deleteUser} alt="" />
            )}
          </div>
        )}
        {isUserGroup && !isMentorGroup && !kicked && (
          <div className="lk__head-my-plan">Подписан</div>
        )}
        <div className={cx('lk__rp-item', { active: isMyPlan && logIn })}>
          {isFetching === planId && <Preloader className="preloader__whiteBG" />}

          <div>
            <div className="lk__rp-item-head">
              <div className="lk__rp-item-title">{description}</div>
              <div className="lk__rp-item-subtitle" />
            </div>

            <div className="lk__rp-item-desc">{comment}</div>
          </div>
          <div className="lk__rp-item-content">
            <div className="lk__btn-wrapper">
              {(!hasGroup || kicked) &&
                (isMyPlan && logIn ? (
                  <Link
                    to={`/reading-plan/${planId}/day-${closedDaysLength || 1}`}
                    className="btn btn_complete lk__rp-item-plug">
                    Перейти к плану
                  </Link>
                ) : (
                  <button className="btn lk__rp-item-plug" onClick={addRP}>
                    Подключить
                  </button>
                ))}
              {hasGroup && !kicked && (
                <Link
                  to={`/group/reading/${planId}/day-${closedDaysLength || 1}`}
                  className="btn lk__rp-item-plug login">
                  Войти
                </Link>
              )}
            </div>
            <div
              style={{
                justifyContent:
                  isCreatedPlan || isMentorGroup || (isUserGroup && !kicked)
                    ? 'space-between'
                    : 'center',
              }}
              ref={ref}
              className="lk__rp-item-more">
              {!canDelete ? (
                <button className="lk__rp-item-more-link" onClick={receivePlanDetail}>
                  Посмотреть план
                </button>
              ) : (
                <button
                  className="lk__rp-item-more-link"
                  onClick={async () => {
                    dispatch(deleteUserReadingPlanSuccess(planId));
                    await deleteGroupThunk();
                    setDeletedGroup(true);
                  }}>
                  Распустить группу
                </button>
              )}
              {(isCreatedPlan || isMentorGroup) && (
                <Link
                  className="lk__rp-item-more-link"
                  to={`/reading-plan/${isCreatedPlan ? 'edit-plan' : 'edit-group'}/${planId}`}>
                  Редактировать план
                </Link>
              )}
              {isUserGroup && !isMentorGroup && !kicked && (
                <button
                  onClick={async () => {
                    if (userMe?.user?.id) {
                      await kickUser(userMe?.user?.id);
                      setKicked(true);
                    }
                  }}
                  className="lk__rp-item-more-link">
                  Покинуть группу
                </button>
              )}
            </div>
          </div>
        </div>

        {hasGroup && (
          <Link to="/reading-plan/group" className="lk__rp-turn-on-group">
            Статистика группы
          </Link>
        )}

        {isMyPlan && logIn && !hasGroup && (
          <button className="lk__rp-turn-on-group" onClick={deleteRP}>
            Отключить план
          </button>
        )}

        {!hasGroup && !isMyPlan && !isMentorGroup && (
          <Link
            onClick={e => {
              if (!logIn) {
                e.preventDefault();
                Popup.create(
                  {
                    title: 'Вход',
                    content: <PopupAuth />,
                    className: 'popup_auth not-header',
                  },
                  true,
                );
              }
              if (isBlockingSubscribe) {
                e.preventDefault();
                handleWithGroupClick();
              }
            }}
            to={`/reading-plan/create-group/${planId}`}
            className="lk__rp-turn-on-group">
            Подключить группу
          </Link>
        )}
      </div>
    );
  },
);

ReadinPlanItem.defaultProps = {
  comment:
    'Данный план подходит тем, кто только начинает свое знакомство с христианством. За год Вы освоите самую важную часть Библии - Новый Завет. Последовательность чтения та же, что и в Библии.',
};

ReadinPlanItem.propTypes = {
  isFetching: PropTypes.number.isRequired,
  isMyPlan: PropTypes.bool.isRequired,
  description: PropTypes.string.isRequired,
  planId: PropTypes.number.isRequired,
  addUserReadingPlan: PropTypes.func.isRequired,
  deleteUserReadingPlan: PropTypes.func.isRequired,
  getPlanDetail: PropTypes.func.isRequired,
  comment: PropTypes.string,
};

export default withRouter(ReadinPlanItem);
