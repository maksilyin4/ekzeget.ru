export const getUrlReadingPlant = (url = '') => {
  return url
    .split('/')
    .filter(f => f)
    .filter((_, i, arr) => i !== arr.length - 1)
    .join('/');
};
