import React, { useEffect, useCallback, useState, useMemo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Popup } from '../../../dist/browserUtils';
import readingPlanActions from '../../../dist/actions/ReadingPlan';
import { getUser } from '~dist/actions/User';
import {
  readingPlansIsLoading,
  plans,
  userRPGetIsLoading,
  userPlans,
} from '../../../dist/selectors/ReadingPlan';
import ReadingPlanDetail from '../../../components/Popups/ReadingPlanDetail/ReadingPlanDetail';
import Preloader from '../../../components/Preloader/Preloader';
import ReadinPlanItem from './ReadingPlanItem';

import './styles.scss';
import { userData, userLoggedIn } from '../../../dist/selectors/User';
import CustomButtonCP from '~components/UICreatePlan/CustomButtonCp';
import PopupAuth from '../../../components/Popups/Auth/Auth';
import { mentorGroup, userGroup } from '~store/groups/selectors';
import { deleteGroupThunk, getMentorGroup, getUserGroup } from '~store/groups/actions';
import { findCurrentReadingDay } from '~components/CustomPlanModule/utils/getActualCurrentDay';
import useProgressPlan from '../../../hooks/useProgressPlan';
import { setFormValues } from '~store/createPlan/actions';
import { resetFormValues } from '../../../store/createPlan/actions';
import { useHistory } from 'react-router-dom';

const ReadingPlan = React.memo(
  ({
    getReadingPlans,
    getMentorGroup,
    getUserGroup,
    userGroup,
    mentorGroup,
    user,
    getUser,
    deleteGroupThunk,
    getUserReadingPlan,
    rpIsLoading,
    userRPIsLoading,
    allPlans,
    deleteReadingPlan,
    myPlans,
    addUserReadingPlan,
    deleteUserReadingPlan,
    resetFormValues,
    logIn,
  }) => {
    const history = useHistory();
    useEffect(() => {
      getReadingPlans();
      getUser();
      getMentorGroup();
      getUserGroup();
    }, []);

    useEffect(() => {
      if (logIn && myPlans.length === 0) {
        getUserReadingPlan();
      }
    }, [logIn, myPlans]);

    const getPlanDetail = useCallback((plan_name, plan_id, group) => {
      Popup.create(
        {
          title: plan_name,
          content: <ReadingPlanDetail plan_name={plan_name} group={group} plan_id={plan_id} />,
          className: 'popup_rp-detail not-header',
        },
        true,
      );
    }, []);

    const handleNoAuthClick = () => {
      Popup.create(
        {
          title: 'Вход',
          content: <PopupAuth />,
          className: 'popup_auth not-header',
        },
        true,
      );
    };
    const handleWithGroupClick = () => {
      Popup.create(
        {
          title: 'Чтобы подписаться на этот план вы должны выйти из предыдущего плана',
          className: 'popup_auth not-header',
        },
        true,
      );
    };

    const sortPlan = (a, b) => {
      const isACreator = user?.user?.id === a.creator_id ? -1 : 1;
      const isBCreator = user?.user?.id === b.creator_id ? -1 : 1;

      if (isACreator !== isBCreator) {
        return isACreator - isBCreator;
      }

      return 0;
    };

    const [currentDay, setCurrentDay] = useState(null);
    useEffect(() => {
      if (Object.values(myPlans)?.[0]?.id) {
        const d = findCurrentReadingDay(myPlans);
        setCurrentDay(d > 0 ? d : 1);
      }
    }, [myPlans]);
    const { closedDays } = useProgressPlan();
    const lastUnreadDay = useMemo(() => {
      if (closedDays.length) {
        let day = 1;
        closedDays.every((el, i, arr) => {
          if (!arr[i + 1]) {
            day = el + 1;
            return false;
          }
          if (el + 1 === arr[i + 1]) {
            return true;
          }
          day = el + 1;
          return false;
        });
        return day;
      }
      return 1;
    }, [closedDays]);

    return (
      <div className="lk__rp wrapper__container">
        {logIn ? (
          <CustomButtonCP
            onClick={() => {
              resetFormValues();
              history.push('/reading-plan/create-plan');
            }}
            color="blue"
            className={'lk__rp-create-plan'}>
            Создать свой план чтений
          </CustomButtonCP>
        ) : (
          <CustomButtonCP onClick={handleNoAuthClick} color="blue" className={'lk__rp-create-plan'}>
            Создать свой план чтений
          </CustomButtonCP>
        )}

        {rpIsLoading && <Preloader />}
        <div className="lk__rp-list">
          {[...allPlans].sort(sortPlan).map(plan => (
            <ReadinPlanItem
              key={plan.id}
              currentDay={currentDay}
              groups={{
                mentorGroup,
                userGroup,
              }}
              closedDaysLength={lastUnreadDay}
              deleteGroupThunk={deleteGroupThunk}
              handleWithGroupClick={handleWithGroupClick}
              myPlans={myPlans}
              deleteReadingPlan={deleteReadingPlan}
              isCreatedPlan={user?.user?.id === plan.creator_id}
              isFetching={userRPIsLoading}
              isMyPlan={`${plan.id}` in myPlans}
              planId={plan.id}
              description={plan.description}
              comment={plan.comment}
              addUserReadingPlan={addUserReadingPlan}
              deleteUserReadingPlan={deleteUserReadingPlan}
              getPlanDetail={getPlanDetail}
            />
          ))}
        </div>
      </div>
    );
  },
);

ReadingPlan.propTypes = {
  rpIsLoading: PropTypes.bool.isRequired,
  userRPIsLoading: PropTypes.number.isRequired,
  allPlans: PropTypes.array.isRequired,
  myPlans: PropTypes.object.isRequired,
  addUserReadingPlan: PropTypes.func.isRequired,
  deleteUserReadingPlan: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  rpIsLoading: readingPlansIsLoading(state),
  userRPIsLoading: userRPGetIsLoading(state),
  logIn: userLoggedIn(state),
  allPlans: plans(state),
  mentorGroup: mentorGroup(state),
  userGroup: userGroup(state),
  user: userData(state),
  myPlans: userPlans(state),
});

const mapDispatchToProps = {
  ...readingPlanActions,
  getUser,
  setFormValues,
  getMentorGroup,
  resetFormValues,
  deleteGroupThunk,
  getUserGroup,
};

export default connect(mapStateToProps, mapDispatchToProps)(ReadingPlan);
