import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import userActions from '../../../dist/actions/User';
import {
  userDataIsLoading,
  userData,
  userUpdateIsLoading,
  userError,
} from '../../../dist/selectors/User';
import Form, { FormGroup, FormSubmit } from '../../../components/Form/Form';
import TextField from '../../../components/TextField/TextField';
import Preloader from '../../../components/Preloader/Preloader';
import { validateEmail } from '../../../dist/utils';

class Personal extends Component {
  static propTypes = {
    userData: PropTypes.object,
  };

  constructor(props) {
    super(props);

    this.state = {
      username: '',
      email: '',
      firstName: '',
      lastName: '',
      country: '',
      city: '',
      about: '',
      message: '',
      error: '',
    };
  }

  componentDidMount() {
    const {
      userData: { user },
    } = this.props;

    if (Object.keys(user).length === 0) {
      this.props.getUser();
    } else {
      this.setState({
        username: user.username,
        email: user.email,
        firstName: user.firstname,
        lastName: user.lastname,
        country: user.country,
        city: user.city,
        about: user.about || '',
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.userData !== this.props.userData) {
      this.setState({
        username: nextProps.userData.user.username,
        email: nextProps.userData.user.email,
        firstName: nextProps.userData.user.firstname,
        lastName: nextProps.userData.user.lastname,
        country: nextProps.userData.user.country,
        city: nextProps.userData.user.city,
        about: nextProps.userData.user.about || '',
      });
    }
    if (nextProps.error) {
      this.setState({
        error: nextProps.error,
      });
    }
  }

  _submit = e => {
    e.preventDefault();
    const { username, email, firstName, lastName, country, city, about } = this.state;

    if (!username) {
      this.setState({
        error: 'Проверьте логин пользователя',
      });
    } else if (!email || !validateEmail(email)) {
      this.setState({
        error: 'Введён некорректный email',
      });
    } else if (!firstName) {
      this.setState({
        error: 'Проверьте Имя',
      });
    } else if (!lastName) {
      this.setState({
        error: 'Проверьте фамилию',
      });
    } else if (!country) {
      this.setState({
        error: 'Проверьте страну',
      });
    } else if (!city) {
      this.setState({
        error: 'Проверьте город',
      });
    } else {
      const data = new FormData();
      data.append('email', email);
      data.append('username', username);
      data.append('firstname', firstName);
      data.append('lastname', lastName);
      data.append('country', country);
      data.append('city', city);
      data.append('about', about);

      this.props.updateUser(data).then(() => {
        if (!this.props.error && this.props.userData.status === 'ok') {
          this.setState({ message: 'Изменения сохранены' });
          setTimeout(() => this.setState({ message: '' }), 1000);
        }
      });
    }
  };

  _handleChange = field => ({ target: { value } }) => {
    this.setState({
      [field]:
        field === 'email'
          ? value.replace(new RegExp('[#$%^&*)(+=]'), '')
          : value.replace(new RegExp('(?![ .,!a-zа-я0-9ё-]).', 'igm'), ''),
      message: '',
      error: '',
    });
  };

  render() {
    const { userDataIsLoading } = this.props;
    const { message, error } = this.state;

    return userDataIsLoading ? (
      <Preloader />
    ) : (
      <div className="lk-content content paper">
        <div className="lk__personal content-area-for-side">
          <Form
            action=""
            name="lk__personal-form"
            className="lk__personal-form"
            onSubmit={this._submit}>
            <FormGroup>
              <TextField
                type="text"
                name="username"
                label="Логин"
                required
                value={this.state.username}
                onChange={this._handleChange('username')}
              />
            </FormGroup>
            <FormGroup>
              <TextField
                type="text"
                name="firstName"
                label="Имя"
                required
                value={this.state.firstName || ''}
                onChange={this._handleChange('firstName')}
              />
            </FormGroup>
            <FormGroup>
              <TextField
                type="text"
                name="lastName"
                label="Фамилия"
                required
                value={this.state.lastName || ''}
                onChange={this._handleChange('lastName')}
              />
            </FormGroup>
            <FormGroup>
              <TextField
                type="text"
                name="email"
                label="Email"
                required
                value={this.state.email}
                onChange={this._handleChange('email')}
              />
            </FormGroup>
            <FormGroup>
              <TextField
                type="text"
                name="country"
                label="Страна"
                required
                value={this.state.country || ''}
                onChange={this._handleChange('country')}
              />
            </FormGroup>
            <FormGroup>
              <TextField
                type="text"
                name="city"
                label="Город"
                required
                value={this.state.city || ''}
                onChange={this._handleChange('city')}
              />
            </FormGroup>
            <FormGroup>
              <TextField
                type="textarea"
                name="about"
                label="О себе"
                value={this.state.about || ''}
                onChange={this._handleChange('about')}
              />
            </FormGroup>
            {message && <div className="form__notice form__notice_success">{message}</div>}
            {error && <div className="form__notice form__notice_error">{error}</div>}
            <FormSubmit title="Сохранить изменения" />
          </Form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  userDataIsLoading: userDataIsLoading(state),
  userData: userData(state),
  userUpdateIsLoading: userUpdateIsLoading(state),
  error: userError(state),
});

const mapDispatchToProps = {
  ...userActions,
};

export default connect(mapStateToProps, mapDispatchToProps)(Personal);
