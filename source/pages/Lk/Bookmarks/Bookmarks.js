import React, { useMemo, useCallback } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { bookmarks, getIsLoading } from '../../../dist/selectors/Bookmarks';
import { deleteBookmark } from '../../../dist/actions/Bookmarks';

import List, { ListItem } from '../../../components/List/List';
import Preloader from '../../../components/Preloader/Preloader';
import Icon from '../../../components/Icons/Icons';
import Button from '../../../components/Button/Button';

import './bookmarks.scss';

const Bookmarks = ({ getIsLoading, bookmarks: { bookmark }, deleteBookmark }) => {
  const deleteBookmarkWithId = useCallback(deleteBookmark, [deleteBookmark]);

  const renderBookmarks = useMemo(() => {
    const items = Object.values(bookmark);

    return (
      <List className="lk__bookmarks">
        {items.length > 0 ? (
          items.map(({ id, color, info, bookmark }) => {
            return (
              !Array.isArray(info) && (
                <ListItem
                  key={id}
                  title={
                    <span className="lk__bookmark-item">
                      <Icon type="circle" icon="bookmark" style={{ backgroundColor: color }} />
                      {info.book && info.book.title}, Глава{' '}
                      {info.chapter === 0 && bookmark.match(/glava-\d+/g)
                        ? bookmark
                            .match(/glava-\d+/g)
                            .join('')
                            .replace('glava-', '')
                        : info.chapter}
                      {info.verse ? ` стих ${info.number}` : ''}
                    </span>
                  }
                  href={`/bible/${info.book && info.book.code}/glava-${
                    info.chapter === 0 && bookmark.match(/glava-\d+/g)
                      ? bookmark
                          .match(/glava-\d+/g)
                          .join('')
                          .replace('glava-', '')
                      : info.chapter
                  }/${info.number ? `stih-${info.number}` : ''}`}
                  actions={
                    <Button type="circle" icon="delete" onClick={() => deleteBookmarkWithId(id)} />
                  }
                />
              )
            );
          })
        ) : (
          <div className="empty-content">Вы еще не добавляли закладок</div>
        )}
      </List>
    );
  }, [deleteBookmarkWithId, bookmark]);

  return (
    <div className="lk-content content paper">{getIsLoading ? <Preloader /> : renderBookmarks}</div>
  );
};

Bookmarks.defaultProps = {
  getIsLoading: true,
  bookmarks: {},
  deleteBookmark: () => {},
};

Bookmarks.propTypes = {
  getIsLoading: PropTypes.string,
  bookmarks: PropTypes.shape({
    bookmark: PropTypes.shape({
      bookmark: PropTypes.string.isRequired,
      color: PropTypes.string.isRequired,
      created_at: PropTypes.number,
      id: PropTypes.number.isRequired,
      info: PropTypes.shape({
        title: PropTypes.string,
        code: PropTypes.string,
        book: PropTypes.shape({
          title: PropTypes.string.isRequired,
          code: PropTypes.string.isRequired,
        }),
        chapter: PropTypes.number.isRequired,
        verse: PropTypes.object.isRequired,
      }),
      chapter: PropTypes.number,
      number: PropTypes.number,
      verse: PropTypes.string,
    }),
  }),
  deleteBookmark: PropTypes.func,
};

const mapStateToProps = state => ({
  getIsLoading: getIsLoading(state),
  bookmarks: bookmarks(state),
});

const mapDispatchToProps = {
  deleteBookmark,
};

export default connect(mapStateToProps, mapDispatchToProps)(Bookmarks);
