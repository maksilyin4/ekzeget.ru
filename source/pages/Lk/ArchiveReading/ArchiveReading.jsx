import React from 'react';
import './styles.scss';
import ArchiveTables from '~components/ArchiveTables';

export default function ArchiveReading() {
  return (
    <div className="lk-content content archive-content">
      <div className="lk__personal content-area-for-side">
        <ArchiveTables />
      </div>
    </div>
  );
}
