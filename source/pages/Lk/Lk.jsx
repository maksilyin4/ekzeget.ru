import React, { Component } from 'react';
// import { Helmet } from 'react-helmet';
import { Switch, Route, Redirect } from 'react-router-dom';

import BreadHead from '~components/BreadHead';
import PageHead from '~components/PageHead';
import Personal from './Personal/Personal';
import MyInterpretations from './Interpretations';
import Favorites from './Favorites/Favorites';
import Bookmarks from './Bookmarks/Bookmarks';
import ReadingPlan from './ReadingPlan/ReadingPlan';
import ReadingPlanDetail from './ReadingPlanDetail/ReadingPlanDetail';
import LKNav from './LKNav';
import './lk.scss';
import { smoothScrollTo } from '../../dist/utils';
import ArchiveReading from './ArchiveReading/ArchiveReading';

class Lk extends Component {
  componentDidMount() {
    smoothScrollTo();
  }

  _breadItems = () => [
    { path: '/', title: 'Главная' },
    { path: '', title: 'Личный кабинет' },
  ];

  render() {
    const meta = {
      h_one: 'Личный кабинет',
      title: 'Личный кабинет',
    };

    return (
      <div className="page lk-page">
        <div className="wrapper wrapper__header">
          {/* <Helmet title="Личный кабинет" /> */}
          <BreadHead bread={this._breadItems()} />
          <PageHead meta={meta} />
        </div>

        <div className="wrapper wrapper_two-col wrapper__lk">
          <LKNav />
          <Switch>
            <Route path="/lk/personal/" component={Personal} />
            <Route path="/lk/interpretations/" component={MyInterpretations} />
            <Route path="/lk/favorites/" component={Favorites} />
            <Route path="/lk/bookmarks/" component={Bookmarks} />
            <Route exact path="/lk/reading-plan/" component={ReadingPlan} />
            <Route path="/lk/reading-plan/:plan_id/" component={ReadingPlanDetail} />

            <Route path="/lk/archive-reading-plan/" component={ArchiveReading} />
            <Redirect to="/lk/personal/" />
          </Switch>
        </div>
      </div>
    );
  }
}

export default Lk;
