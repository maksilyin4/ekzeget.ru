import React, { Component } from 'react';
import { Route, NavLink, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { getUserReadingPlan } from '../../dist/actions/ReadingPlan';
import { getUserPlanId } from '../../dist/selectors/ReadingPlan';
import LkReadingSubscribe from './LkReadingSubscribe';
import OrphusInfo from '../../components/OrphusInfo';
import PlanDetailsDays from './ReadingPlanDetail/plan-details-days';

// TODO: create order component for nav menu
class LKNav extends Component {
  componentDidMount() {
    const {
      location: { pathname },
    } = this.props;
    if (!pathname.includes('reading-plan')) {
      this.props.getUserReadingPlan();
    }
  }

  render() {
    return (
      <div className="side lk-side">
        <div className="side__item nav-side">
          <div className="list__item">
            <NavLink
              to="/lk/personal/"
              className="nav-side__link list__item-link"
              activeClassName="active">
              Персональные данные
            </NavLink>
          </div>
          <div className="list__item">
            <NavLink
              to="/lk/interpretations/"
              className="nav-side__link list__item-link"
              activeClassName="active">
              Мои толкования
            </NavLink>
          </div>
          <div className="list__item">
            <NavLink
              to="/lk/favorites/"
              className="nav-side__link list__item-link"
              activeClassName="active">
              Избранное
            </NavLink>
          </div>
          <div className="list__item">
            <NavLink
              to="/lk/bookmarks/"
              className="nav-side__link list__item-link"
              activeClassName="active">
              Закладки
            </NavLink>
          </div>
          <div className="list__item">
            <NavLink
              to={`/reading-plan/`}
              className="nav-side__link list__item-link"
              activeClassName="active">
              План чтений
            </NavLink>
          </div>
          <div className="list__item">
            <NavLink
              to={`/lk/archive-reading-plan`}
              className="nav-side__link list__item-link"
              activeClassName="active">
              Архив планов чтений
            </NavLink>
          </div>
        </div>
        <PlanDetailsDays />
        <Route path="/lk/reading-plan/:plan_id/" component={LkReadingSubscribe} />
        <OrphusInfo />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  userReadingPlan: getUserPlanId(state),
});

const mapDispatchToProps = {
  getUserReadingPlan,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LKNav));
