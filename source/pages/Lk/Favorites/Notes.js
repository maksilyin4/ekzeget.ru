import React, { Component } from 'react';
import NoteItem from './NotesItem';

export default class LKNote extends Component {
  state = {
    opened: null,
  };

  componentDidMount() {
    this.props.getNotes();
  }

  openEdit = id => {
    this.setState(({ opened }) => ({
      opened: opened !== id ? id : null,
    }));
  };

  handleUpdate = (id, text) => {
    if (text.trim()) {
      this.props.updateNotes({ note_id: id, text }).then(res => {
        if (res === 'ok') {
          this.openEdit(null);
        }
      });
    } else {
      this.openEdit(null);
    }
  };

  render() {
    const { list, isFetching, deleteNote } = this.props;
    const { opened } = this.state;

    return (
      <div className="favorite__interpretation notes">
        {list.length > 0 ? (
          <ul>
            {list.map(note => {
              const linkUrl = `/bible/${note.book_code}/glava-${note.chapter}/${
                note.number !== 0 ? `stih-${note.number}/` : ''
              }`;
              return (
                <NoteItem
                  key={note.id}
                  linkUrl={linkUrl}
                  short={note.short}
                  text={note.text}
                  deleteNote={() => deleteNote(note.id)}
                  opened={opened === note.id}
                  openEdit={() => this.openEdit(note.id)}
                  updateNotes={text => this.handleUpdate(note.id, text)}
                />
              );
            })}
          </ul>
        ) : (
          <div className="empty-content">
            {!isFetching && <p>Вы не создали ни одной заметки</p>}
          </div>
        )}
      </div>
    );
  }
}
