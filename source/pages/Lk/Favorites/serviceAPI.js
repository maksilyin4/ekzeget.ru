import qs from 'qs';
import { _AXIOS } from '../../../dist/ApiConfig';

const url = 'user/verse-fav/';

export function getFavoriteVerse() {
  return _AXIOS({ url, isAuth: true })
    .then(({ verse }) => ({
      isFetching: false,
      favorites: verse,
    }))
    .catch(() => ({
      isFetching: false,
      favorites: [],
    }));
}

export function deleteFavoriteVerse(id) {
  _AXIOS({
    url,
    method: 'DELETE',
    isAuth: true,
    data: qs.stringify({ id }),
  }).then(
    ({ status }) => status,
    () => 'error',
  );
}
