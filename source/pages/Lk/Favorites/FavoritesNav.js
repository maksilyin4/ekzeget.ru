import React, { memo } from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';

const FavoritesNav = ({ handleChose, favoritesType }) => (
  <div className="favorites__main">
    <div className="favorites__nav">
      <div
        className={cx('tab', { active: favoritesType === 'verse' })}
        onClick={() => handleChose('verse')}>
        <p className="desc">Стихи</p>
      </div>
      <div
        className={cx('tab', { active: favoritesType === 'interpretation' })}
        onClick={() => handleChose('interpretation')}>
        <p className="desc">Толкования</p>
      </div>
      <div
        className={cx('tab', { active: favoritesType === 'notes' })}
        onClick={() => handleChose('notes')}>
        <p className="desc">Заметки</p>
      </div>
    </div>
  </div>
);

FavoritesNav.defaultProps = {
  handleChose: () => {},
  favoritesType: 'verse',
};

FavoritesNav.propTypes = {
  handleChose: PropTypes.func,
  favoritesType: PropTypes.string,
};

export default memo(FavoritesNav);
