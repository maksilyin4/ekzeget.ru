import PropTypes from 'prop-types';

export const favoritesTypes = {
  allTags: PropTypes.arrayOf(PropTypes.string),
  choosenTag: PropTypes.string,
  getFavoriteVerse: PropTypes.func,
  chooseTag: PropTypes.func,
  setSessionTag: PropTypes.func,
};

export const favoritesDefault = {
  getFavoriteVerse: () => {},
  chooseTag: () => {},
  setSessionTag: () => {},
};
