import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';

export default class FavoriteInterpretation extends PureComponent {
  componentDidMount() {
    this.props.getFavoriteInter();
  }

  render() {
    const { isFetching, list, deleteFavorite } = this.props;

    return (
      <div className="favorite__interpretation">
        {list.length > 0 ? (
          <ul>
            {list.map(inter => (
              <li key={inter.id}>
                <div className="favorite__interpretation__head">
                  <h3>
                    <Link to={inter.verse.verseLink}>{inter.verse.short}</Link>
                    {' - '}
                    <Link to={inter.ekzeget.interpretationLink}>{inter.ekzeget.name}</Link>
                  </h3>
                  <span onClick={() => deleteFavorite(inter.id)}>X</span>
                </div>
              </li>
            ))}
          </ul>
        ) : (
          <div className="empty-content">
            {!isFetching && <p>Вы не добавили ни одно толкование</p>}
          </div>
        )}
      </div>
    );
  }
}
