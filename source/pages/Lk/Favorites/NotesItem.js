import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import deleteIcon from '../../../assets/images/icons/delete.svg';
import editIcon from '../../../assets/images/icons/edit.svg';

class NoteItem extends Component {
  state = {
    value: '',
  };

  componentDidMount() {
    this.setState({
      value: this.props.text,
    });
  }

  handleInput = e => {
    const { value } = e.currentTarget;
    this.setState({
      value,
    });
  };

  render() {
    const { linkUrl, short, text, deleteNote, opened, openEdit, updateNotes } = this.props;
    const { value } = this.state;
    return (
      <li>
        <div className="notes__title">
          <Link to={linkUrl}>{short}</Link>
          <div className="favorite__verseList__edit">
            <div className="favorite__verseList__edit-icon btn_circle" onClick={openEdit}>
              <img alt="Редактировать" title="Редактировать" src={editIcon} />
            </div>
            <div className="favorite__verseList__edit-icon btn_circle" onClick={deleteNote}>
              <img alt="Удалить" title="Удалить" src={deleteIcon} />
            </div>
          </div>
        </div>
        <div className="notes__content">
          <p>Заметка</p>
          {opened ? (
            <textarea
              name=""
              id=""
              cols="30"
              rows="6"
              onBlur={e => updateNotes(e.currentTarget.value)}
              onChange={this.handleInput}
              value={value}
            />
          ) : (
            <p className="notes__text">{text}</p>
          )}
        </div>
      </li>
    );
  }
}

export default NoteItem;
