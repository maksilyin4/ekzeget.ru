import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  getFavoriteVerse,
  getFavoriteInter,
  deleteFavoriteVerse,
  deleteFavoriteInter,
  getNotes,
  updateNotes,
  deleteNote,
  chooseTag,
  updateFavVerse,
  setSessionTag,
} from '../../../dist/actions/favorites';
import * as S from '../../../dist/selectors/favorites';
import Preloader from '../../../components/Preloader/Preloader';
import FavoritesNav from './FavoritesNav';
import FavoriteVerseMap from '../../../components/FavoriteVerse/FavoriteVerseMap';
import FavoriteInterpretation from './Favoriteinterpretation';
import LKNote from './Notes';
import './styles.scss';
import '../../../components/Tabs/tabs.scss';
import { getCurrentTranslate } from '../../../dist/selectors/translates';

class Favorites extends Component {
  state = {
    favoritesType: 'verse',
  };

  handleChose = favoritesType => {
    this.setState({
      favoritesType,
    });
  };

  render() {
    const { favoritesType } = this.state;
    const {
      isFetching,
      favoritesVerse,
      getFavoriteVerse,
      getFavoriteInter,
      deleteFavoriteVerse,
      deleteFavoriteInter,
      favoritesInter,
      getNotes,
      notesList,
      updateNotes,
      deleteNote,
      allTags,
      choosenTag,
      chooseTag,
      updateFavVerse,
      sessionTag,
      sessionTagArray,
      currentTranslate,
      setSessionTag,
    } = this.props;

    return (
      <div className="page content favorites">
        <FavoritesNav handleChose={this.handleChose} favoritesType={favoritesType} />
        {isFetching && <Preloader />}
        {favoritesType === 'verse' && (
          <FavoriteVerseMap
            isFetching={isFetching}
            favorites={favoritesVerse}
            getFavoriteVerse={getFavoriteVerse}
            deleteFavoriteVerse={deleteFavoriteVerse}
            updateFavVerse={updateFavVerse}
            allTags={allTags}
            chooseTag={chooseTag}
            choosenTag={choosenTag}
            sessionTag={sessionTag}
            sessionTagArray={sessionTagArray}
            setSessionTag={setSessionTag}
            currentTranslate={currentTranslate}
          />
        )}
        {favoritesType === 'interpretation' && (
          <FavoriteInterpretation
            isFetching={isFetching}
            getFavoriteInter={getFavoriteInter}
            list={favoritesInter}
            deleteFavorite={deleteFavoriteInter}
          />
        )}
        {favoritesType === 'notes' && (
          <LKNote
            isFetching={isFetching}
            getNotes={getNotes}
            list={notesList}
            updateNotes={updateNotes}
            deleteNote={deleteNote}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = store => ({
  isFetching: S.getIsFetching(store),
  favoritesVerse: S.getFavoriteList(store),
  favoritesInter: S.favoriteInterListSelector(store),
  notesList: S.getNotes(store),
  allTags: S.getAlltags(store),
  choosenTag: S.getChosenTag(store),
  sessionTag: S.getSessionTag(store),
  sessionTagArray: S.getSessionTagArray(store),
  currentTranslate: getCurrentTranslate(store),
});

const mapDispatchToProps = {
  getFavoriteVerse,
  deleteFavoriteVerse,
  getFavoriteInter,
  deleteFavoriteInter,
  updateFavVerse,
  getNotes,
  updateNotes,
  deleteNote,
  chooseTag,
  setSessionTag,
};

export default connect(mapStateToProps, mapDispatchToProps)(Favorites);
