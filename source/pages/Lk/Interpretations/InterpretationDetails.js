import R from 'ramda';
import React, { useMemo } from 'react';
import { Link } from 'react-router-dom';
import dayjs from 'dayjs';
import ruLocale from 'dayjs/locale/ru';
import PropTypes from 'prop-types';

import Preloader from '../../../components/Preloader/Preloader';
import { interpretationTypes } from './commonInterpretationTypes';

const InterpDetails = ({ allMyInterp, isFetching, match: { params } }) =>
  useMemo(() => {
    const currentInterp = R.pathOr({}, [params.id], allMyInterp);
    const data = dayjs
      .unix(R.pathOr(dayjs().unix(), ['added_at'], currentInterp))
      .locale('ru', ruLocale)
      .format('DD MMM YYYY');
    const verse = R.pathOr({}, ['verse', 0], currentInterp);
    const verseLink = `/bible/${R.pathOr('', ['book', 'code'], verse)}/glava-${
      verse.chapter
    }/stih-${verse.number}/`;

    return (
      <div className="LK__interpretations lk-content content">
        {allMyInterp.hasOwnProperty(params.id) ? (
          <div className="LK__interpretations__details">
            <div className="LK__interpretations__details-head">
              <Link to="/lk/interpretations/" className="LK__interpretations__details-back" />
              <Link to={verseLink}>{verse.short}</Link>
              <Link
                to={`/all-about-bible/ekzegets/${R.pathOr(
                  '',
                  ['ekzeget', 'code'],
                  currentInterp,
                )}/`}>
                {R.pathOr('Экзегет', ['ekzeget', 'name'], currentInterp)}
              </Link>
              <p>{`добавлено ${data}`}</p>
            </div>
            <div className="LK__interpretations__details-content">
              <p>Текст толкования</p>
              <div
                className="LK__interpretations__details-comment"
                dangerouslySetInnerHTML={{
                  __html: R.pathOr('', ['comment'], currentInterp),
                }}
              />
            </div>
          </div>
        ) : (
          <div>
            {isFetching ? (
              <Preloader />
            ) : (
              <div>
                <p>Данного толкования нет в базе, попробуйте выбрать из списка</p>
                <Link to="/lk/interpretations/">Вернуться к списку</Link>
              </div>
            )}
          </div>
        )}
      </div>
    );
  }, [allMyInterp, isFetching]);

InterpDetails.propTypes = {
  isFetching: PropTypes.bool,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.number,
    }),
  }),
  ...interpretationTypes,
};

export default InterpDetails;
