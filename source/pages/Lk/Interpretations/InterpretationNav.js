import React, { useMemo } from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';

const InterpretationNav = ({ handleChose, favoritesType }) =>
  useMemo(
    () => (
      <div className="favorites__nav">
        <div
          className={cx('tab', { active: favoritesType === 'all' })}
          onClick={() => handleChose('all')}>
          <p>Все толкования</p>
        </div>
        <div
          className={cx('tab', { active: favoritesType === 'inVacation' })}
          onClick={() => handleChose('inVacation')}>
          <p>На проверке</p>
        </div>
      </div>
    ),
    [handleChose, favoritesType],
  );

InterpretationNav.defaultProps = {
  handleChose: () => {},
  favoritesType: 'all',
};

InterpretationNav.propTypes = {
  handleChose: PropTypes.func,
  favoritesType: PropTypes.string,
};

export default InterpretationNav;
