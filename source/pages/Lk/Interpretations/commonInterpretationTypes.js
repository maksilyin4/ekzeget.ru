import PropTypes from 'prop-types';

export const interpretationTypes = {
  myInterpList: PropTypes.arrayOf(
    PropTypes.shape({
      added_at: PropTypes.number,
      id: PropTypes.number,
      ekzeget: PropTypes.shape({
        name: PropTypes.string,
      }),
      editable: PropTypes.bool,
      verse: PropTypes.arrayOf(
        PropTypes.shape({
          short: PropTypes.string,
        }),
      ),
    }),
  ),
};
