import R from 'ramda';
import React, { useMemo, useState, useCallback } from 'react';
import dayjs from 'dayjs';
import ruLocale from 'dayjs/locale/ru';
import PropTypes from 'prop-types';

import InterpretationNav from './InterpretationNav';
import MyInterpItem from './myInterpretationsItem';
import Preloader from '../../../components/Preloader/Preloader';
import { interpretationTypes } from './commonInterpretationTypes';

const MyInterpList = ({ myInterpList, isFetching }) => {
  const [favoritesType, setFavoritesType] = useState('all');

  const handleChose = useCallback(favoritesType => setFavoritesType(favoritesType), []);

  const renderInterpList = useMemo(() => {
    const sortedList =
      favoritesType === 'all' ? myInterpList : myInterpList.filter(i => i.editable);

    return myInterpList.length ? (
      <ol className="LK__interpretations__list">
        {sortedList.map(interp => {
          const verse = R.pathOr({}, ['verse', 0], interp);
          const url = `/lk/interpretations/${interp.id}`;
          const data = dayjs
            .unix(interp.added_at)
            .locale('ru', ruLocale)
            .format('DD MMM YYYY');
          return (
            <MyInterpItem
              key={interp.id}
              url={url}
              name={interp.ekzeget.name}
              verse={verse.short}
              confermed={!interp.editable}
              data={data}
            />
          );
        })}
      </ol>
    ) : (
      <div className="LK__interpretations__empty">
        {isFetching ? <Preloader /> : <p>Вы ещё не добавляли толкований</p>}
      </div>
    );
  }, [favoritesType, isFetching, myInterpList]);

  return (
    <div className="LK__interpretations lk-content content">
      <InterpretationNav handleChose={handleChose} favoritesType={favoritesType} />
      {renderInterpList}
    </div>
  );
};

MyInterpList.defaultProps = {
  isFetching: false,
};

MyInterpList.propTypes = {
  ...interpretationTypes,
  isFetching: PropTypes.bool,
};

export default MyInterpList;
