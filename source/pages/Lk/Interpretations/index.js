import React, { useEffect, useMemo } from 'react';
import { Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { getMyInterp } from '../../../dist/actions/myInterpretations';
import { getMyInterpList, getIsFetch, byIDs } from '../../../dist/selectors/myInterpretations';

import MyInterpList from './myInterpretationsList';
import InterpDetails from './InterpretationDetails';

import './styles.scss';

const mapStateToProps = store => ({
  myInterpList: getMyInterpList(store),
  isFetching: getIsFetch(store),
  allMyInterp: byIDs(store),
});

const mapDispatchToProps = {
  getMyInterp,
};

const MyInterpretations = ({ myInterpList, isFetching, allMyInterp, getMyInterp }) => {
  useEffect(() => getMyInterp(), []);

  return useMemo(
    () => (
      <Switch>
        <Route
          exact
          path="/lk/interpretations/"
          render={props => (
            <MyInterpList {...props} myInterpList={myInterpList} isFetching={isFetching} />
          )}
        />
        <Route
          path="/lk/interpretations/:id/"
          render={props => (
            <InterpDetails {...props} isFetching={isFetching} allMyInterp={allMyInterp} />
          )}
        />
      </Switch>
    ),
    [allMyInterp, isFetching, myInterpList],
  );
};

MyInterpretations.defaultProps = {
  myInterpList: [],
  isFetching: false,
  allMyInterp: {},
};

MyInterpretations.propTypes = {
  myInterpList: PropTypes.arrayOf(PropTypes.object),
  isFetching: PropTypes.bool,
  allMyInterp: PropTypes.objectOf(PropTypes.object),
};

export default connect(mapStateToProps, mapDispatchToProps)(MyInterpretations);
