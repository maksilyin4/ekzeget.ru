import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import Icon from '../../../components/Icons/Icons';
import { baseUlr } from '../../../getStore';

const MyInterpItem = ({ url, name, confermed, verse, data }) => (
  <li className="myInterp__item">
    <Link to={url}>
      <p>{verse}</p>
      <p>{name}</p>
      <p className="myInterp__item__data">{`добавлено ${data}`}</p>
      {confermed && <Icon icon="confermed-interpretation" title="подтверждено" />}
    </Link>
  </li>
);

MyInterpItem.defaultProps = {
  url: baseUlr,
  name: 'default name',
  confermed: false,
  verse: 'default verse',
  data: 'default date',
};

MyInterpItem.propTypes = {
  url: PropTypes.string,
  name: PropTypes.string,
  confermed: PropTypes.bool,
  verse: PropTypes.string,
  data: PropTypes.string,
};

export default MyInterpItem;
