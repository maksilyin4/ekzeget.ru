import { createAtom } from '@reatom/core';

const initialState = {
  planId: '',
  currentDay: null,
  chosenDay: null,
  getDay: null,
  plan: null,
};

export const planDetailsAtom = createAtom(
  {
    set: (name, value) => [name, value],
    setGetDay: v => v,
  },
  ({ onAction }, state = initialState) => {
    onAction('set', ([name, value]) => {
      return (state = { ...state, [name]: value });
    });

    onAction('setGetDay', getDay => {
      return (state = { ...state, getDay });
    });

    return state;
  },
);
