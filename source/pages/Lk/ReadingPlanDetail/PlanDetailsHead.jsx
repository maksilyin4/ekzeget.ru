import React, { useMemo } from 'react';
import PropTypes from 'prop-types';

import PlanVerseItem from './PlanVerseItem';
import Translates from '../../../components/Translates';

const PlanDetailsHead = ({
  readings,
  isUserPlans,
  activeContent,
  // isRemainderDays,
  changeTab,
  // isPhone,
}) => {
  // const renderRemainderDays = useMemo(
  //   () =>
  //     isRemainderDays && !isPhone ? (
  //       <div className="urp__remainder urp__notEmpty">
  //         <p className="urp__remainder-notice">У вас есть пропущенные дни</p>
  //       </div>
  //     ) : (
  //       <div className="urp__remainder emptyRemainder" />
  //     ),
  //   [isPhone, isRemainderDays],
  // );

  const renderVerses = useMemo(
    () => (
      <div className="tabs__plan-scroll">
        {readings.map(({ short }) => (
          <PlanVerseItem
            key={short}
            short={short}
            changeTab={changeTab}
            isActiveContent={activeContent === short}
          />
        ))}
      </div>
    ),
    [activeContent, changeTab, readings],
  );

  return (
    <div className="tabs__head">
      {isUserPlans && <div>Выбранный план не подключен</div>}
      <div className={'tabs__chapters'}>{renderVerses}</div>
      <Translates isLk />
    </div>
  );
};

PlanDetailsHead.propTypes = {
  readings: PropTypes.array.isRequired,
  isUserPlans: PropTypes.bool.isRequired,
  activeContent: PropTypes.string.isRequired,
  // isRemainderDays: PropTypes.bool.isRequired,
  changeTab: PropTypes.func.isRequired,
  // isPhone: PropTypes.bool.isRequired,
};

export default PlanDetailsHead;
