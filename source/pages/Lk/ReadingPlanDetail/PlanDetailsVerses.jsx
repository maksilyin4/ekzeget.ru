import React, { useMemo, useEffect, useRef } from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Preloader from '../../../components/Preloader/Preloader';
import PlanDetailsAudio from './PlanDetailsAudio';

const PlanDetailsVerses = ({
  setWidth,
  isFetching,
  verses,
  currentTranslate,
  code,
  legacyCode,
  chapter,
}) => {
  const verRef = useRef(null);

  useEffect(() => {
    setWidth(verRef.current?.clientWidth);
  }, [setWidth]);

  const renderDetailsVerses = useMemo(() => {
    const currentTranslateVerses = verses.reduce(
      (prev, { translate, text, ...otherVerseProps }) => {
        if (currentTranslate === 'st_text') prev.push({ ...otherVerseProps, text });
        else {
          const translatedVerse = translate.find(
            item =>
              currentTranslate === 'st_text' ||
              (item.code.code === currentTranslate && item.text !== ''),
          );
          if (translatedVerse) prev.push({ ...otherVerseProps, text: translatedVerse.text });
        }
        return prev;
      },
      [],
    );
    const translateExist = currentTranslateVerses.length > 0;

    const translateStyle = {
      csya: currentTranslate === 'csya_old',
      greek: currentTranslate === 'grek',
    };

    const src = `${currentTranslate}/${legacyCode}/${legacyCode}-${chapter}-${currentTranslate}.mp3`;
    const srcEkzeget = `${currentTranslate}/${code}/${code}-${chapter}-${currentTranslate}.mp3`;

    return (
      <div className="content-area-for-side text_justify" ref={verRef}>
        {!isFetching && code && chapter && (
          <PlanDetailsAudio
            src={src}
            srcEkzeget={srcEkzeget}
            code={code}
            chapter={chapter}
            currentTranslate={currentTranslate}
          />
        )}
        <div className="chapter__verses">
          {translateExist ? (
            currentTranslateVerses.map(item => (
              <div key={item.id} className={cx('chapter__verse', translateStyle)}>
                <Link to={`/bible/${item.book.code}/glava-${item.chapter}/stih-${item.number}/`}>
                  <sup>{item.number}</sup>
                  <span dangerouslySetInnerHTML={{ __html: item.text }} />
                </Link>
              </div>
            ))
          ) : (
            <p style={{ fontSize: '1.8rem', textAlign: 'center' }}>
              Выбранный Вами перевод на данную главу отсутствует
            </p>
          )}
        </div>
      </div>
    );
  }, [chapter, code, currentTranslate, isFetching, legacyCode, verses]);

  return isFetching ? <Preloader className="preloader__whiteBG" /> : renderDetailsVerses;
};

PlanDetailsVerses.defaultProps = {
  verses: [],
  currentTranslate: 'st_text',
  setWidth: () => {},
};

PlanDetailsVerses.propTypes = {
  isFetching: PropTypes.bool.isRequired,
  verses: PropTypes.array,
  currentTranslate: PropTypes.string,
  code: PropTypes.string.isRequired,
  chapter: PropTypes.number.isRequired,
  setWidth: PropTypes.func,
  legacyCode: PropTypes.string,
};

export default PlanDetailsVerses;
