import React, { useEffect, useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import qs from 'qs';

import { _AXIOS } from '../../../dist/ApiConfig';
import AudioPlayer from '../../../components/AudioPlayer/AudioPlayer';
import { bibleCDN } from '~utils/common';

const PlanDetailsAudio = ({ src }) => {
  const [isExist, setExistStatus] = useState(false);

  const isFileExist = useCallback(() => {
    const data = qs.stringify({ file: src });

    _AXIOS({ url: '/site/file-exist/', method: 'POST', data })
      .then(({ file_exist }) => setExistStatus(file_exist))
      .catch(() => setExistStatus(false));
  }, [src]);

  useEffect(() => {
    isFileExist();
  }, []);

  useEffect(() => {
    setExistStatus(false);
    isFileExist();
  }, [src]);

  return (
    <div className="lk__rp__audio">
      {isExist && <AudioPlayer src={`${bibleCDN}/${src}`} srcToDownload={`${bibleCDN}/${src}`} />}
    </div>
  );
};

PlanDetailsAudio.propTypes = {
  src: PropTypes.string.isRequired,
};

export default PlanDetailsAudio;
