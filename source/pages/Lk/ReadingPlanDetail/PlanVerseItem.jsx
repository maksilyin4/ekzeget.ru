import React, { useCallback, useMemo } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const PlanVerseItem = ({ short, changeTab, isActiveContent }) => {
  const getNewTab = useCallback(() => {
    changeTab(short);
  }, [changeTab, short]);

  return useMemo(
    () => (
      <div className={cx('tab', { active: isActiveContent })} onClick={getNewTab}>
        {short}
      </div>
    ),
    [getNewTab, isActiveContent, short],
  );
};

PlanVerseItem.propTypes = {
  changeTab: PropTypes.func.isRequired,
  short: PropTypes.string.isRequired,
  isActiveContent: PropTypes.bool.isRequired,
  animatedScroll: PropTypes.func.isRequired,
};

export default PlanVerseItem;
