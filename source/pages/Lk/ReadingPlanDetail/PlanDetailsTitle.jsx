import React from 'react';
import PropTypes from 'prop-types';
import { Link, useRouteMatch } from 'react-router-dom';
import { getUrlReadingPlant } from '../ReadingPlan/utils';

const PlanDetailsTitle = React.memo(({ title }) => {
  const { url } = useRouteMatch();

  return (
    <div className="urp__head">
      <div className="urp__head__leftpart">
        <div className="urp__head__leftpart__title">
          <h3 className="urp__title">{title}</h3>
          <Link to={`/${getUrlReadingPlant(url)}/`} className="urp__back" title="Изменить план">
            Изменить
          </Link>
        </div>
      </div>
    </div>
  );
});

PlanDetailsTitle.propTypes = {
  title: PropTypes.string.isRequired,
};

export default PlanDetailsTitle;
