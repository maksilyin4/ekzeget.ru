import React from 'react';

import './styles.scss';
import { useAtom } from '@reatom/react';
import { planDetailsAtom } from '../model';
import { getDateFull } from '../../../../utils/date';

export const PlanDetailsInfo = () => {
  const [{ currentDay, chosenDay, plan }] = useAtom(planDetailsAtom);

  const renderItem = (label, value) => {
    if (!value) {
      return null;
    }
    return (
      <div>
        <b>{label}</b>
        <b>{value}</b>
      </div>
    );
  };

  return (
    <div className="plan-details-info-content">
      <div className="plan-details-info-item yellow-text">
        {renderItem('Прочитано:', chosenDay)}
        {renderItem('Сегодня:', `${currentDay} день`)}
      </div>
      <div className="plan-details-info-item blue-text">
        {renderItem('Закончите:', getDateFull(plan?.stop_date))}
        {renderItem('План закончится:', getDateFull(plan?.stop_date))}
        {renderItem('Отставание:', `${plan?.remainder_days.length}`)}
      </div>
    </div>
  );
};
