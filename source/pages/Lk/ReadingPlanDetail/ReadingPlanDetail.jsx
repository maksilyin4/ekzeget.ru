import React, { useCallback, useEffect, useMemo, useState } from 'react';
import CustomButtonCP from '~components/UICreatePlan/CustomButtonCp';
import R from 'ramda';
import PlanDetailsVerses from '../../Lk/ReadingPlanDetail/PlanDetailsVerses';
import { connect, useDispatch, useSelector } from 'react-redux';
import getScreen, { getIsMobile } from '../../../dist/selectors/screen';
import readingPlanActions, { setActiveTab } from '../../../dist/actions/ReadingPlan';
import {
  userRPGetIsLoading,
  activeTabUserRP,
  getUserPlans,
} from '../../../dist/selectors/ReadingPlan';
import { getCurrentTranslateCodeLk } from '../../../dist/selectors/translates';
import { _AXIOS } from '../../../dist/ApiConfig';
import { getQuizVariant } from '~components/CustomPlanModule/utils/getQuizVariant';
import { useLocation, useHistory } from 'react-router-dom';
import { planDetailsAtom } from '~pages/Lk/ReadingPlanDetail/model';
import { useMedia, useUpdateEffect } from 'react-use';
import { smoothScrollTo } from '../../../dist/utils';
import { getCurrentQueeze, getQuizByPlanId } from '../../../dist/actions/Quiz';
import { useCookies } from 'react-cookie';
import { findCurrentReadingDay } from '../../../components/CustomPlanModule/utils/getActualCurrentDay';
import { useParams } from 'react-router-dom/cjs/react-router-dom.min';
import CustomPlanBodyLayout from '~pages/CustomPlanDetail/Layout/CustomPlanBodyLayout';
import { ButtonQuiz } from '~pages/CustomPlanDetail/CustomPlanBody';
import { createQuizPlan } from '~dist/quizAPI';
import useProgressPlan from '../../../hooks/useProgressPlan';
import MainPageGame from '~components/CustomPlanModule/Game/MainPage';
import { completedQueezePlan } from '~dist/selectors/Quiz';
import { bibleMediaQuestionsReducer } from '~store/bible/bibleMediaQuestions/selectors';
import {
  getBibleQuestionsData,
  getTypeForQuestions,
} from '../../../components/Bible/BibleMediaContentRight/BibleMedia/BibleMediaContent/BibleMediaBody/BibleMediaQuestions/utilsBibleQuestions';
import { getBibleMediaQuestions } from '~store/bible/bibleMediaQuestions/actions';

function ReadingPlanDetail({
  userPlans,
  currentTranslate,
  sendCompleteChapter,
  getCurrentQueeze,
  sendResetDays,
  setActiveTab,
  activeTab,
  quiz,
  getUserReadingPlan,
  getQuizByPlanId,
  getCommentsPlanByDay,
  deleteUserReadingPlan,
  planId,
  ...props
}) {
  const [verses, setVerses] = useState([]);
  const [currentDay, setCurrentDay] = useState(null);
  const [chosenDay, setChosenDay] = useState(null);
  const [, setWidthVerser] = useState('');
  const [isFetching, setIsFetching] = useState([]);
  const history = useHistory();
  const { closedDays } = useProgressPlan();
  const plans = userPlans?.reading?.flat();
  const { day: dayUrl } = useParams();
  const isMobile = useMedia('(max-width: 768px)');
  const [isLast, setIsLast] = useState(false);
  const [isShowAlert, setShowAlert] = useState(false);
  const isResult = useLocation()?.pathname.includes('result');
  const [isUnread, setUnread] = useState(false);
  const activeIndex = userPlans?.reading?.flat()?.findIndex(plan => plan.id === activeTab.id);
  const correct = quiz?.meta?.correct;

  useUpdateEffect(() => {
    if (activeTab.id && !isResult) {
      getQuizByPlanId(activeTab.id);
    }
  }, [activeTab, isResult]);

  useEffect(() => {
    if (activeTab.id && userPlans.reading) {
      setIsLast(plans.length - 1 === activeIndex);
    }
  }, [activeTab, userPlans]);

  const getVerses = async short => {
    try {
      setIsFetching(true);
      const { verse } = await _AXIOS({ url: `/verse/info/${short}/?per-page=1000&translate=1` });
      setVerses(verse);
      setIsFetching(false);
    } catch (e) {
      setIsFetching(false);
    }
  };

  useUpdateEffect(() => {
    if (activeTab.short) {
      getVerses(activeTab.short);
    }
  }, [activeTab]);

  const getDay = useCallback(
    async day => {
      await getUserReadingPlan(day);
      setChosenDay(day);
    },
    [getUserReadingPlan],
  );

  const [mount, setMount] = useState(false);

  useEffect(() => {
    if (userPlans.id) {
      setCurrentDay(findCurrentReadingDay(userPlans));
    }
  }, [userPlans]);

  useEffect(() => {
    if (userPlans.id && !mount && !isResult) {
      setActiveTab(userPlans.reading.flat()[0]);
      if (activeTab?.day === chosenDay) {
        setMount(true);
      }
    }
  }, [chosenDay, userPlans]);

  useEffect(() => {
    if (dayUrl) {
      getDay(dayUrl);
      setChosenDay(dayUrl);
      setMount(false);
      planDetailsAtom.set.dispatch('planId', planId);
    }
  }, [dayUrl]);

  useEffect(() => {
    planDetailsAtom.set.dispatch('currentDay', currentDay);
  }, [currentDay]);

  useEffect(() => {
    planDetailsAtom.set.dispatch('getDay', getDay);
  }, [getDay]);

  // Для prevState.chosenDay !== this.state.chosenDay
  useEffect(() => {
    planDetailsAtom.set.dispatch('chosenDay', chosenDay);
  }, [chosenDay]);

  // Для prevProps.userPlans !== this.props.userPlans
  useEffect(() => {
    planDetailsAtom.set.dispatch('plan', userPlans);
  }, [userPlans]);

  const completeChapter = async () => {
    const activeIndex = userPlans.reading.flat().findIndex(plan => plan.id === activeTab.id);
    if (activeIndex === -1 || !userPlans.reading.flat()[activeIndex + 1]) {
      if (!isUnread && !activeTab.is_closed) {
        await sendCompleteChapter(activeTab.id);
      }

      await history.push(`/reading-plan/${userPlans.plan.id}/day-${Number(chosenDay) + 1}`);
      await getDay(Number(chosenDay) + 1);
      setChosenDay(Number(chosenDay) + 1);
    } else {
      if (!isUnread && !activeTab.is_closed) {
        await sendCompleteChapter(activeTab.id);
      }
      setActiveTab(userPlans.reading.flat()[activeIndex + 1]);
    }
  };

  const handleNextChapter = async isCorrect => {
    const url = `/reading-plan/${userPlans.plan.id}/day-${dayUrl}/`;
    if (isResult && correct < 6 && !isCorrect) {
      history.push(url);
      return;
    }
    if (isResult) {
      history.push(url);
      await completeChapter();
      return;
    }
    if (!quiz && !isShowAlert && !isUnread) {
      setShowAlert(true);
    } else {
      setShowAlert(false);
      await completeChapter();
      smoothScrollTo();
    }
  };

  const [cookies] = useCookies(['bibleNavParent']);

  const isVisibleReading = isMobile
    ? cookies?.bibleNavParent?.type
      ? cookies?.bibleNavParent?.type === 'bibleText'
      : true
    : true;

  const handleClosePlan = async () => {
    try {
      setIsFetching(true);
      await deleteUserReadingPlan(planId);
      history.push('/reading-plan');
      setIsFetching(false);
    } catch (e) {
      setIsFetching(false);
    }
  };

  const bibleMediaQuestions = useSelector(bibleMediaQuestionsReducer);
  const [{ interMultiTab }] = useCookies(['interMultiTab', 'heightLeftMenuBible']);
  const type = getTypeForQuestions(interMultiTab);
  const chapterId = verses[0]?.chapter_id;
  const questionId = chapterId + type;

  const [data] = useMemo(() => getBibleQuestionsData(bibleMediaQuestions[questionId]), [
    bibleMediaQuestions,
    questionId,
  ]);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(
      getBibleMediaQuestions({
        book_id: verses[0]?.book_id,
        perPage: 10,
        page: 1,
        chapter_id: verses[0]?.chapter_id,
        type,
      }),
    );
  }, [verses[0]?.chapter_id, verses[0]?.book_id, type]);

  const onPlayGame = async () => {
    await createQuizPlan(
      activeTab.book.id,
      verses[0].chapter_id,
      activeTab.id,
      getCurrentQueeze,
      history.push,
      chosenDay,
      data[0]?.id,
    );
  };

  const nameButtonVictorine = useMemo(
    () =>
      getQuizVariant(correct, [
        'Прочитать главу еще раз',
        'Перейти к следующей главе',
        'Перейти к следующей главе',
        'Перейти к следующей главе',
      ]),
    [correct],
  );

  const isLastDay = userPlans?.plan?.lenght === Number(dayUrl);
  const otherPlan = plans?.filter((_, i) => i !== activeIndex);
  const isReadingDays = closedDays.length + 1 === userPlans?.plan?.lenght;
  const isLastChapter =
    plans?.length === 1 ||
    (otherPlan?.every(p => p.is_closed) &&
      plans?.findIndex(p => p.id === activeTab?.id) === plans?.length - 1);
  const isLastChapterDay =
    plans?.length === 1 || plans?.findIndex(p => p.id === activeTab?.id) === plans?.length - 1;
  const isLastChapterFormPlan = isLastDay && isLastChapter && isReadingDays;
  const isCheckOpenButton = !(isLastDay && isLastChapterDay);

  return (
    <CustomPlanBodyLayout path="/reading-plan" {...props}>
      {isShowAlert ? (
        <MainPageGame />
      ) : (
        isVisibleReading && (
          <PlanDetailsVerses
            isFetching={isFetching}
            verses={verses}
            code={R.pathOr('', ['book', 'code'], verses[0])}
            legacyCode={R.pathOr('', ['book', 'legacy_code'], verses[0])}
            chapter={R.pathOr('', ['chapter'], verses[0])}
            setWidth={setWidthVerser}
            currentTranslate={currentTranslate}
          />
        )
      )}
      <div className="buttons-game">
        <>
          <ButtonQuiz onPlayGame={onPlayGame} isResult={isResult} />
          {isCheckOpenButton ? (
            <div className="game__or">Или</div>
          ) : (
            isLastChapterFormPlan && <div className="game__or">Или</div>
          )}
        </>
        {isCheckOpenButton ? (
          <CustomButtonCP
            onClick={handleNextChapter}
            className={'game__btn'}
            color={(!isResult || correct < 6) && (!isLast || !isResult) ? 'gray' : 'blue'}>
            {isResult && isLast
              ? 'Вперед! В новый день'
              : isShowAlert
              ? 'Сдаюсь, перейти к следующей главе'
              : isResult
              ? nameButtonVictorine
              : 'Перейти к следующей главе'}
          </CustomButtonCP>
        ) : (
          isLastChapterFormPlan && (
            <CustomButtonCP
              onClick={handleClosePlan}
              className={'game__btn'}
              color={(!isResult || correct < 6) && (!isLast || !isResult) ? 'gray' : 'blue'}>
              Завершить план
            </CustomButtonCP>
          )
        )}
        {!isCheckOpenButton && !activeTab?.is_closed && (
          <div className="game-label">
            <label className="container-label" htmlFor="quiz-chapter">
              <input
                onChange={e => setUnread(e.target.checked)}
                checked={isUnread}
                type="checkbox"
                id="quiz-chapter"
              />
              <span className="checkmark" />
              Отметить эту главу, как непрочитанную
            </label>
          </div>
        )}
      </div>
    </CustomPlanBodyLayout>
  );
}

const mapStateToProps = (state, ownProps) => {
  const planId = ownProps.match.params.plan_id;
  return {
    userRPGetIsLoading: userRPGetIsLoading(state),
    activeTab: activeTabUserRP(state),
    userPlans: getUserPlans(state, planId),
    currentTranslate: getCurrentTranslateCodeLk(state),
    quiz: completedQueezePlan(state),
    isPhone: getIsMobile(state),
    screen: getScreen(state),
    planId,
  };
};

const mapDispatchToProps = {
  ...readingPlanActions,
  setActiveTab,
  getQuizByPlanId,
  getCurrentQueeze,
};

export default connect(mapStateToProps, mapDispatchToProps)(ReadingPlanDetail);
