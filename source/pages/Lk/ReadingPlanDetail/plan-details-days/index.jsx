import R from 'ramda';
import React from 'react';
import cx from 'classnames';
import Select from 'react-select';
import List, { ListItem } from '../../../../components/List/List';
// import Preloader from '../../../components/Preloader/Preloader';
import { withRouter, useLocation } from 'react-router-dom';

import { useSelector } from 'react-redux';
import { getUserPlans } from '../../../../dist/selectors/ReadingPlan';

import { useAtom } from '@reatom/react';
import getScreen from '../../../../dist/selectors/screen';

import '../styles.scss';
import './styles.scss';
import { planDetailsAtom } from '../model';

const PlanDetailsDays = () => {
  const { pathname } = useLocation();
  const [{ planId, currentDay, chosenDay, getDay }] = useAtom(planDetailsAtom);

  const { desktop, tabletMedium } = useSelector(getScreen);
  const userPlans = useSelector(state => getUserPlans(state, planId));

  const _renderDays = (length, chosenDay, remainderDays, closedDays, url) => {
    const days = [];

    let day = 1;

    while (day <= length) {
      const thisDay = day;

      days.push(
        <ListItem
          className={cx(
            remainderDays.includes(day) && 'remainder',
            closedDays.includes(day) && 'complete',
            +day === +chosenDay && 'active',
          )}
          key={day}
          title={`${day} день`}
          href={`/${url}/${planId}/?day=${day}`}
          onClick={() => getDay(thisDay)}
        />,
      );
      day += 1;
    }

    return days;
  };

  const renderSelectDays = (days, chosenDay, remainderDays, closedDays, url) => {
    let daysSelect = [];
    let chosenDayArr = [];
    const daysArr = Array(days)
      .join()
      .split(',')
      .map(
        function() {
          return this.i++;
        },
        { i: 1 },
      );

    daysSelect = daysArr.map(day => {
      return {
        value: day,
        label: (
          <ListItem
            className={cx(
              remainderDays.includes(day) && 'remainder',
              closedDays.includes(day) && 'complete',
              {
                active: +day === +chosenDay,
              },
            )}
            key={day}
            title={`${day} день`}
            href={`/${url}/${planId}/?day=${day}`}
            onClick={() => getDay(day)}
          />
        ),
      };
    });

    chosenDayArr = {
      value: chosenDay,
      label: (
        <ListItem
          key={chosenDay}
          title={`${chosenDay} день`}
          href={`/${url}/${planId}/?day=${chosenDay}`}
          onClick={() => getDay(chosenDay)}
        />
      ),
    };

    return (
      <Select
        isSearchable={false}
        classNamePrefix="custom-select"
        options={daysSelect}
        value={chosenDayArr}
      />
    );
  };

  const urlReadingPlant = 'lk/reading-plan';

  // if (false) {
  //   return <Preloader />;
  // }

  const [userPlan, remainderDays, closedDays] = React.useMemo(
    () => [
      R.pathOr(0, ['plan', 'lenght'], userPlans),
      R.pathOr('', ['remainder_days'], userPlans),
      R.pathOr('', ['closed_days'], userPlans),
    ],
    [userPlans],
  );

  if (planId?.length === 0 || (!pathname.includes(planId) && pathname !== '/group/reading')) {
    return null;
  }

  return (
    <div className="side plan-details-days">
      {!!currentDay && (
        <p className="side_right-today today">{`Сегодня ${currentDay}-й день чтения`}</p>
      )}
      {!desktop && !tabletMedium ? (
        <div className="urp__days">
          {renderSelectDays(userPlan, chosenDay, remainderDays, closedDays, urlReadingPlant)}
        </div>
      ) : (
        <List className="urp__days">
          {_renderDays(userPlan, chosenDay, remainderDays, closedDays, urlReadingPlant)}
        </List>
      )}
    </div>
  );
};

export default withRouter(PlanDetailsDays);
