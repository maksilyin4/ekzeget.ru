import React, { useMemo } from 'react';
import PropTypes from 'prop-types';

/* 1 проверка - если приходит не пустой объект, дабы без ошибки.
    2 проверка -  если у нас уже есть изначально закрытые дни, то по условию задачи, нужно проверять
    если у нас <= 3 пропущенных дней
    3 проверка - если у нас пользователь только начал читать и нет прочитанных дней, но есть пропущенные
*/
const PlanDetailsUpdate = ({ userPlans, resetDays }) =>
  useMemo(() => {
    const maxClosedDays = userPlans.closed_days[userPlans.closed_days.length - 1] + 3;
    const minRemainderDays = userPlans.remainder_days[userPlans.remainder_days.length - 1];
    const closedDaysLength = userPlans.closed_days.length;
    const remainderDaysLength = userPlans.remainder_days.length;

    return (
      (maxClosedDays <= minRemainderDays || (!closedDaysLength && remainderDaysLength >= 3)) && (
        <div className="lk__resetDays-wrapper">
          <h3 className="lk__resetDays-title">У Вас большое отставание от плана чтения!</h3>
          <p className="lk__resetDays-desc">
            Вы можете наверстать чтение, сделав так, что последний день, когда вы читали по плану,
            станет вчерашним в календаре.
          </p>
          <button className="btn lk__resetDays-btn" onClick={resetDays}>
            Наверстать
          </button>
        </div>
      )
    );
  }, [resetDays, userPlans]);

PlanDetailsUpdate.defaultProps = {
  userPlans: () => {},
};

PlanDetailsUpdate.propTypes = {
  resetDays: PropTypes.shape({
    closed_days: PropTypes.arrayOf(PropTypes.number),
    remainder_days: PropTypes.arrayOf(PropTypes.number),
  }),
};

export default PlanDetailsUpdate;
