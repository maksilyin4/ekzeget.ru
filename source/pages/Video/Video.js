import R from 'ramda';
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';

import { isMetaData } from '../../dist/utils';
import { getMetaData } from '../../store/metaData/actions';
import videoActions from '../../dist/actions/Video';
import getLastVideos from '../../dist/actions/LastVideos';

import { getIsDesktop, getIsTablet, getIsMobile } from '../../dist/selectors/screen';
import {
  videoPlaylistLoading,
  videoPlaylist,
  getLastVideosDataFiltered,
  videoDetail,
} from '../../dist/selectors/Video';

import PageHead from '~components/PageHead';
import BreadHead from '~components/BreadHead';
import VideoDetail from './indexDetail';
import VideoList from './VideoList';
import VideoMenu from './VideoMenu';
import Preloader from '../../components/Preloader/Preloader';

import './video.scss';

const Video = props => {
  const { isDesktop, isTablet, isPhone, metaData, videoPlaylist, newVideo, videoDetail } = props;
  const testament = props.location.pathname.split('/')[2];
  const isTestament = testament === 'video-novogo-zaveta'; // For SSR

  const obj = {
    testament,
    videoId: props.location.pathname.split('/')[4],
    oldTestamentBooks: isTestament ? getTestamentBooksFromTestament(props.videoPlaylist.video) : [],
    newTestamentBooks: !isTestament
      ? getTestamentBooksFromTestament(props.videoPlaylist.video)
      : [],
    h_one: 'Видео',
    isLoad: true,
    bread: [
      { path: '/', title: 'Главная' },
      { path: '', title: 'Видео' },
    ],
  };
  const [state, setState] = useState(obj);

  useEffect(() => {
    if (!Object.keys(videoPlaylist).length || !videoPlaylist.video.length) {
      props.getVideoPlaylist('video-novogo-zaveta');
    }
    props.getLastVideos('video-vethogo-zaveta');
    props.getLastVideos('video-novogo-zaveta');
    getMeta(state.testament);
  }, []);

  useEffect(() => {
    const video_id = props.location.pathname.split('/')[4];
    const isTitle = video_id && videoDetail.video;
    const title = isTitle ? videoDetail.video.find(el => el.code === video_id) : '';

    setState({
      ...state,
      isLoad: false,
      h_one: isMetaData('h_one', metaData, 'Видео'),
      bread: isTitle
        ? [
            { path: '/', title: 'Главная' },
            { path: '/video/', title: 'Видео' },
            {
              path: '',
              title: title ? title.title : '',
            },
          ]
        : [
            { path: '/', title: 'Главная' },
            { path: '', title: 'Видео' },
          ],
    });
  }, [props.location.pathname, metaData.h_one, videoDetail.video]);

  function getMeta(value) {
    const param = value === 'video-novogo-zaveta' ? 2 : 1;
    props.getMetaData(`video-${param}`);
  }

  function getTestamentBooksFromTestament(testament) {
    return testament
      ? R.uniqBy(R.prop('id'), [
          ...testament.map(({ book_id, title, author }) => ({
            id: book_id,
            title,
            author,
          })),
        ])
      : [];
  }

  const _toggleTestament = value => {
    getMeta(value);
    props.getVideoPlaylist(value);
    setState({
      ...state,
      testament: value,
      bread: [
        { path: '/', title: 'Главная' },
        { path: '', title: 'Видео' },
      ],
    });
  };

  const selectChange = ({ value }) => {
    getMeta(value);
    const { history } = props;
    props.getVideoPlaylist(value);
    setState({ ...state, testament: value });
    history.push(`/video/${value}/`);
  };

  const getDetailOther = playlist_id => () => {
    props.getVideoDetail(playlist_id);
  };

  return (
    <div className="page video-page">
      <div className="wrapper">
        <BreadHead bread={state.bread} />
        <PageHead h_one={state.h_one} isLoad={state.isLoad} />
      </div>
      <div className="video__wrapper">
        <div className="wrapper wrapper_two-col">
          <VideoMenu
            {...{
              _toggleTestament,
              selectChange,
              testament: state.testament,
              newVideo,
              isDesktop,
              isTablet,
              isPhone,
            }}
          />
          <div className="content video-content">
            {!Object.keys(videoPlaylist).length ? (
              <Preloader />
            ) : (
              <div className="video__list">
                <Helmet>
                  <title>
                    {isMetaData(
                      'title',
                      metaData,
                      'Видео о Новом завете и Ветхом завете с толкованием и лекциями по Библии на Экзегет',
                    )}
                  </title>
                  <meta
                    name="description"
                    content={isMetaData(
                      'description',
                      metaData,
                      'Смотреть библейские видео о Новом завете и Ветхом завете на портале Экзегет. Исторические видео-толкования и видеолекции Библии.',
                    )}
                  />
                </Helmet>

                <Switch>
                  <Route
                    exact
                    path="/video/:testament/:playlist_id/:videoId"
                    render={props => (
                      <VideoDetail
                        {...{
                          videoPlaylist: videoPlaylist.video,
                          videoDetail,
                          getDetailOther,
                          ...state,
                          ...props,
                        }}
                      />
                    )}
                  />
                  <Route
                    path="/video/:testament/"
                    render={() => (
                      <VideoList
                        bookList={getTestamentBooksFromTestament(videoPlaylist.video)}
                        testament={state.testament}
                        videoPlaylist={videoPlaylist.video}
                        lastVideoTestment={props.newVideo.data}
                      />
                    )}
                  />
                </Switch>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  videoPlaylistLoading: videoPlaylistLoading(state),
  videoPlaylist: videoPlaylist(state),
  videoDetail: videoDetail(state),
  isDesktop: getIsDesktop(state),
  isTablet: getIsTablet(state),
  isPhone: getIsMobile(state),
  newVideo: getLastVideosDataFiltered(state),
  metaData: state.metaData,
});

const mapDispatchToProps = {
  ...videoActions,
  getLastVideos,
  getMetaData,
};

Video.propTypes = {
  location: PropTypes.object,
  videoPlaylistLoading: PropTypes.bool,
  videoPlaylist: PropTypes.object,
  isDesktop: PropTypes.bool.isRequired,
};
export default connect(mapStateToProps, mapDispatchToProps)(Video);
