import React from 'react';

const VideDetailActive = ({ currentVideo, preview, uploadTime, length }) => (
  <div className="video-detail-container">
    <div itemScope itemType="http://schema.org/VideoObject">
      <div className="video-schema-meta">
        <meta itemProp="description" content={currentVideo.description} />
        <meta itemProp="duration" content={length} />
        <link itemProp="url" href={currentVideo.path} />
        <link itemProp="thumbnailUrl" href={preview} />
        <meta itemProp="name" content={`${currentVideo.author} ${currentVideo.title}`} />
        <meta itemProp="uploadDate" content={uploadTime} />
        <meta itemProp="isFamilyFriendly" content="true" />
        <span
          className="video-schema__hidden"
          itemProp="thumbnail"
          itemScope
          itemType="http://schema.org/ImageObject">
          <img
            itemProp="contentUrl"
            src={preview}
            alt={`${currentVideo.author} ${currentVideo.title}`}
          />
          <meta itemProp="width" content="300px" />
          <meta itemProp="height" content="300px" />
        </span>
      </div>
      <iframe
        title={currentVideo.title}
        className="video-detail-iframe"
        src={currentVideo.path}
        allowFullScreen
        frameBorder="0"
      />
    </div>
  </div>
);

export default VideDetailActive;
