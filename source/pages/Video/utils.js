const optionsTab = [
  {
    value: 'video-novogo-zaveta',
    label: 'Новый завет',
  },
  {
    value: 'video-vethogo-zaveta',
    label: 'Ветхий завет',
  },
];

export default optionsTab;
