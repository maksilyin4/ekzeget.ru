import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import LazyLoad from 'react-lazyload';

class OtherPlaylist extends PureComponent {
  render() {
    const { otherPlaylist, video, testament, getDetailOther } = this.props;
    return (
      <div className="video-detail__description">
        <div className="video-detail__title">Эту книгу также читают</div>
        <div className="video-detail__other-playlist video__list-content">
          {otherPlaylist.map(
            otherVideo =>
              otherVideo.book_id === video.book_id &&
              otherVideo.author !== video.author && (
                <Link
                  key={otherVideo.id}
                  className="video__item"
                  to={`/video/${testament}/${otherVideo.playlist_code}/${otherVideo.code}/`}
                  onClick={getDetailOther(otherVideo.playlist_code)}>
                  <div className="video__item-preview">
                    <LazyLoad height={112}>
                      <picture>
                        <source
                          srcSet={`//img.youtube.com/vi_webp/${
                            otherVideo.path.split('/')[4]
                          }/mqdefault.webp`}
                          type="image/webp"
                        />
                        <img
                          src={`//img.youtube.com/vi/${
                            otherVideo.path.split('/')[4]
                          }/mqdefault.jpg`}
                          alt={otherVideo.title}
                          width="100%"
                        />
                      </picture>
                    </LazyLoad>
                  </div>
                  <div className="video__item-author">{otherVideo.author}</div>
                </Link>
              ),
          )}
        </div>
      </div>
    );
  }
}

export default OtherPlaylist;
