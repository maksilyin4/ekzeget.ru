import React from 'react';
import Select from 'react-select';
import VideoTabs from './VideoTabs';
import optionsTab from '../utils';

const VideoMenu = ({
  _toggleTestament,
  selectChange,
  testament,
  newVideo,
  isDesktop,
  isPhone,
  isTablet,
}) => {
  const valueOption = optionsTab.length ? optionsTab.filter(el => el.value === testament) : '';
  return isPhone || isTablet ? (
    <div className="select-adaptive">
      <Select
        isSearchable={false}
        options={optionsTab}
        value={valueOption}
        classNamePrefix="custom-select"
        onChange={selectChange}
      />
    </div>
  ) : (
    <div className="side video">
      <VideoTabs
        testament={testament}
        lastVideosNewTestament={newVideo && newVideo.lastVideosNewTestament}
        lastVideosOldTestament={newVideo && newVideo.lastVideosOldTestament}
        isDesktop={isDesktop}
        isTablet={isTablet}
        _toggleTestament={_toggleTestament}
      />
    </div>
  );
};

export default VideoMenu;
