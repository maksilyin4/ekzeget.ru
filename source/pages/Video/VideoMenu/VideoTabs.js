import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import cx from 'classnames';

import Sticker from '../../../components/Sticker/Sticker';
import OrphusInfo from '../../../components/OrphusInfo';

import '../video.scss';

class VideoTabs extends Component {
  render() {
    const {
      testament,
      lastVideosNewTestament,
      lastVideosOldTestament,
      _toggleTestament,
      isTablet,
      isDesktop,
    } = this.props;
    return (
      <>
        <div className="side__item nav-side video-select__wrapper">
          <div className="list__item">
            <Link
              className={cx('nav-side__link list__item-link', {
                active: testament === 'video-novogo-zaveta',
              })}
              onClick={() => _toggleTestament('video-novogo-zaveta')}
              to="/video/video-novogo-zaveta/">
              Новый завет
            </Link>
            {lastVideosNewTestament.length > 0 && (
              <Sticker
                count={lastVideosNewTestament.length}
                isDesktop={isDesktop}
                isTablet={isTablet}
              />
            )}
          </div>
          <div className="list__item">
            <Link
              className={cx('nav-side__link list__item-link', {
                active: testament === 'video-vethogo-zaveta',
              })}
              onClick={() => _toggleTestament('video-vethogo-zaveta')}
              to="/video/video-vethogo-zaveta/">
              Ветхий завет
            </Link>
            {lastVideosOldTestament.length > 0 && (
              <Sticker
                count={lastVideosOldTestament.length}
                isDesktop={isDesktop}
                isTablet={isTablet}
              />
            )}
          </div>
        </div>
        {isDesktop && <OrphusInfo />}
      </>
    );
  }
}

export default VideoTabs;
