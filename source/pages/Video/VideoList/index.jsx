import React from 'react';
import PropTypes from 'prop-types';
import LazyLoad from 'react-lazyload';
import { Link } from 'react-router-dom';
import StickerVideo from '../StickerVideo';

const VideoList = ({ bookList, testament, videoPlaylist, lastVideoTestment = [] }) => {
  return (
    <>
      {bookList.map((book, i) => (
        <div key={i} className="video__list-group">
          <h3 className="video__list-separate">{book.title.split(/[\.|\,]/gm)[0]}</h3>
          <div className="video__list-content">
            {videoPlaylist
              .filter(video => video.book_id === book.id)
              .map((video, i) => (
                <Link
                  key={i}
                  className="video__item"
                  // Пока такой маршрут, не пришли к решению
                  to={`/video/${testament}/${video.playlist_code}/${video.code}`}>
                  <div className="video__item-preview">
                    <LazyLoad height={120}>
                      <picture>
                        <source
                          srcSet={`//img.youtube.com/vi_webp/${
                            video.path.split('/')[4]
                          }/mqdefault.webp`}
                          type="image/webp"
                        />
                        <img
                          src={`//img.youtube.com/vi/${video.path.split('/')[4]}/mqdefault.jpg`}
                          alt={`Изображение автора ${video.author || ''}`}
                          width="100%"
                        />
                      </picture>
                    </LazyLoad>
                    {lastVideoTestment.filter(
                      i => i.book_id === video.book_id && i.author === video.author,
                    ).length > 0 && (
                      <StickerVideo lastVideosTestament={lastVideoTestment} video={video} />
                    )}
                  </div>
                  <p className="video__item-author">{video.author}</p>
                </Link>
              ))}
          </div>
        </div>
      ))}
    </>
  );
};

VideoList.propTypes = {
  bookList: PropTypes.array.isRequired,
  videoPlaylist: PropTypes.array.isRequired,
  lastVideoTestment: PropTypes.array.isRequired,
  testament: PropTypes.string.isRequired,
};
export default VideoList;
