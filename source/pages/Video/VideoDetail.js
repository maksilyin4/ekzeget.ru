import R from 'ramda';
import React, { useState, useEffect } from 'react';
import dayjs from 'dayjs';
import { connect } from 'react-redux';

import Preloader from '../../components/Preloader/Preloader';
import OtherPlaylist from './OtherPlaylist';
import videoActions from '../../dist/actions/Video';

import VideDetailActive from './VideoDetailActive';
import VideoDetailList from './VideoDetailList';

const VideoDetail = props => {
  const {
    isPhone,
    videoDetail,
    match: {
      params: { testament, playlist_id, videoId },
    },
    videoPlaylist = [],
    getVideoDetail,
    getVideoDetailClear,
    getDetailOther,
  } = props;

  const obj = {
    video: [],
    otherVideo: [],
  };
  const [state, setState] = useState(obj);

  useEffect(() => {
    !Object.keys(videoDetail).length && getVideoDetail(playlist_id);
    return () => getVideoDetailClear();
  }, []);

  useEffect(() => {
    const video = [];
    const otherVideo = [];
    if (Object.keys(videoDetail).length) {
      videoDetail.video.forEach(el => {
        if (el.code === videoId) {
          video.push(el);
        } else {
          otherVideo.push(el);
        }
      });
      setState({ ...state, video, otherVideo });
    }
  }, [videoDetail.video, props.location.pathname, playlist_id]);

  const [currentVideo] = state.video;

  // eslint-disable-next-line no-unused-vars
  let length = 0;
  let preview = null;
  let uploadTime = 0;

  if (currentVideo) {
    length = currentVideo.length.split(':').reduce((acc, time) => 60 * acc + +time);
    const hrs = Math.floor(length / 3600);
    const mins = Math.floor((length % 3600) / 60);
    const secs = Math.floor(length % 60);
    // Seconds video duration to ISO format
    length = 'PT';

    if (hrs) {
      length = `${length}${hrs}H`;
    }

    if (mins) {
      length = `${length}${mins}M`;
    }

    if (secs) {
      length = `${length}${secs}S`;
    }

    uploadTime = dayjs.unix(currentVideo.created_at).format('YYYY-MM-DDTHH:mm:ss');
    preview = `https://i.ytimg.com/vi/${currentVideo.path.split('/')[4]}/mqdefault.jpg`;
  }

  return (
    <div className="page__body video__body">
      <div className="paper">
        {!currentVideo ? (
          <Preloader />
        ) : (
          <div className="video__content">
            <h2 className="video-detail__title video-detail__title_main">
              {R.isEmpty(currentVideo?.title) ? '' : currentVideo?.title}
            </h2>
            <div className="content_two-col video_two-col">
              <div className="video-detail content-area-for-side">
                <VideDetailActive
                  {...{
                    currentVideo,
                    preview,
                    uploadTime,
                    videoPlaylist,
                    testament,
                    length,
                  }}
                />
                {!R.isEmpty(currentVideo?.description) && (
                  <p className="video-detail__description">{currentVideo?.description}</p>
                )}
                {videoDetail.video.filter(i => i.id === currentVideo.id).length && !isPhone ? (
                  <OtherPlaylist
                    otherPlaylist={videoPlaylist}
                    video={currentVideo}
                    testament={testament}
                    getDetailOther={getDetailOther}
                  />
                ) : (
                  ''
                )}
              </div>
              <VideoDetailList {...{ otherVideo: state.otherVideo, isPhone, testament }} />
              {videoDetail.video.filter(i => i.book_id === state.video.book_id).length > 1 &&
              isPhone ? (
                <OtherPlaylist
                  otherPlaylist={videoPlaylist}
                  video={currentVideo}
                  testament={testament}
                />
              ) : (
                ''
              )}
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

// eslint-disable-next-line no-unused-vars
const mapStateToProps = state => ({});
export default connect(mapStateToProps, { ...videoActions })(VideoDetail);
