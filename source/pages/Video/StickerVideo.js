import React, { Component } from 'react';
import cx from 'classnames';

class StickerVideo extends Component {
  render() {
    const { lastVideosTestament, video } = this.props;
    const count = lastVideosTestament.filter(
      i => i.book_id === video.book_id && i.author === video.author,
    ).length;
    return (
      <span
        className={cx('video__item-sticker', {
          'video__item-sticker-absolute': video,
        })}>
        {count > 1 ? `${count} новых` : `${count} новое`}
      </span>
    );
  }
}

export default StickerVideo;
