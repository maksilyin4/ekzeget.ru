import R from 'ramda';
import React from 'react';
import { Link } from 'react-router-dom';
import LazyLoad from 'react-lazyload';
import Scrollbar from '../../../components/Scrollbar';

const VideoDetailList = ({ otherVideo, isPhone, testament }) => {
  return (
    <div className="side side_right video__right" style={{ border: 'none' }}>
      <div className="other-video">
        <div className="other-video__title">Следующее видео</div>
        <div className="other-video__list">
          <Scrollbar autoHide={!isPhone}>
            {otherVideo.map(video => (
              <Link
                key={video.id}
                className="other-video__item"
                to={`/video/${testament}/${video.playlist_code}/${video.code}`}>
                <div className="other-video__item-preview">
                  <LazyLoad height={60}>
                    <picture>
                      <source
                        srcSet={`//img.youtube.com/vi_webp/${
                          R.pathOr('', ['path'], video).split('/')[4]
                        }/mqdefault.webp`}
                        type="image/webp"
                      />
                      <img
                        src={`//img.youtube.com/vi/${
                          R.pathOr('', ['path'], video).split('/')[4]
                        }/mqdefault.jpg`}
                        alt={`${video.author} ${video.title}`}
                        width="100%"
                      />
                    </picture>
                  </LazyLoad>
                </div>
                <div className="other-video__item-info">
                  <div className="other-video__item-title">
                    {video.title}. ({video.length})
                  </div>
                  <div className="other-video__item-author">{video.author}</div>
                </div>
              </Link>
            ))}
          </Scrollbar>
        </div>
      </div>
    </div>
  );
};

export default VideoDetailList;
