import React, { Component } from 'react';
import { connect } from 'react-redux';

import BreadHead from '~components/BreadHead';
import PageHead from '~components/PageHead';
import ContactsForm from './ContactForm';
import { getMetaData, smoothScrollTo } from '../../dist/utils';
import { setClearMeta } from '../../store/metaData/actions';

class Contacts extends Component {
  constructor(props) {
    super(props);

    this.state = {
      meta: {},
    };
  }

  componentDidMount() {
    this.props.setClearMeta();
    smoothScrollTo();
    getMetaData('/contacts/').then(meta => {
      if (meta || meta !== 'Error') {
        this.setState({ meta });
      }
    });
  }

  _breadItems = () => [
    { path: '/', title: 'Главная' },
    { path: '', title: 'Контакты' },
  ];

  render() {
    return (
      <div className="page contacts">
        <div className="wrapper">
          <BreadHead bread={this._breadItems()} />
          <PageHead meta={this.state.meta} h_one={'Контакты'} />

          <div className="page__head__description">
            <p>Здесь Вы можете оставить свои замечания и пожелания, касающиеся работы сайта</p>
          </div>
        </div>
        <div className="page__body aboutUs__body">
          <div className="wrapper wrapper_two-col">
            <ContactsForm />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = () => ({});
const mapDispatchToProps = {
  setClearMeta,
};

export default connect(mapStateToProps, mapDispatchToProps)(Contacts);
