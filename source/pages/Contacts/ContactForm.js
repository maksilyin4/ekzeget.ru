import React, { Component } from 'react';
import qs from 'qs';

import { Popup } from '../../dist/browserUtils';
import { _AXIOS } from '../../dist/ApiConfig';
import { sendSocialActions } from '../../dist/utils';
import Preloader from '../../components/Preloader/Preloader';
import Form, { FormGroup, FormSubmit } from '../../components/Form/Form';
import ContactCaptcha from '../../components/Captcha/ContactCaptcha';
import './styles.scss';

export default class ContactsForm extends Component {
  state = {
    isFetching: false,
    name: '',
    email: '',
    body: '',
    verifyCode: '',
    message: '',
  };

  handleSubmit = event => {
    event.preventDefault();
    if (this.state.isFetching) {
      return;
    }
    this.setState({
      isFetching: true,
    });

    const { name, email, body, verifyCode } = this.state;
    const form = qs.stringify({
      name,
      email,
      body,
      captcha: verifyCode,
    });

    _AXIOS({ url: '/site/contact/', data: form, method: 'POST' })
      .then(() => {
        Popup.close();
        Popup.create(
          {
            title: null,
            content: (
              <div className="contact__successfull_popup">
                <p>Ваше сообщение отправлено успешно</p>
              </div>
            ),
            className: 'popup_auth not-header',
          },
          true,
        );
        this.setState({
          name: '',
          email: '',
          body: '',
          verifyCode: '',
          isFetching: false,
          message: '',
        });
        sendSocialActions({
          ga: [
            'event',
            'event_name',
            { event_category: 'Otpravit_soobshchenie', event_action: 'send' },
          ],
          ym: 'Otpravit_soobshchenie',
        });
      })
      .catch(
        ({
          data: {
            error: { message, name },
          },
        }) => {
          this.setState({
            isFetching: false,
            message: `${message}, ${name}`,
          });
        },
      );
  };

  handleInput = event => {
    const { name, value } = event.currentTarget;
    this.setState({
      [name]: value,
      message: '',
    });
  };

  render() {
    const { name, email, body, isFetching, message, verifyCode } = this.state;
    return (
      <div className="contacts__leftSide">
        {isFetching && <Preloader />}
        <Form className="contacts__form" onSubmit={this.handleSubmit} action="">
          <FormGroup className="user_form_input">
            <label htmlFor="contacts-name">
              Ваше имя*
              <input
                type="text"
                name="name"
                id="contacts-name"
                onChange={this.handleInput}
                value={name}
                autoComplete="off"
                required
              />
            </label>
          </FormGroup>
          <FormGroup className="user_form_input">
            <label htmlFor="contacts-email">
              Ваш email*
              <input
                type="email"
                name="email"
                id="contacts-email"
                onChange={this.handleInput}
                value={email}
                autoComplete="off"
                required
              />
            </label>
          </FormGroup>
          <FormGroup className="user_form_input">
            <label htmlFor="contacts-body">
              Ваше сообщение*
              <textarea
                name="body"
                id="contacts-body"
                cols="30"
                rows="5"
                onChange={this.handleInput}
                value={body}
                autoComplete="off"
                required
              />
            </label>
          </FormGroup>
          <ContactCaptcha onChange={this.handleInput} value={verifyCode} />
          {message && <div className="form__notice form__notice_error">{message}</div>}
          <FormSubmit title="Отправить" align="center" />
        </Form>
      </div>
    );
  }
}
