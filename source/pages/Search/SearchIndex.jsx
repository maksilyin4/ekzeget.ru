import React, { memo } from 'react';

import PageHead from '~components/PageHead/';
import BreadHead from '~components/BreadHead';
import SearchBody from './SearchBody/';

const bread = [
  { path: '/', title: 'Главная' },
  { path: '', title: 'Поиск' },
];

const SearchIndex = () => {
  return (
    <div className="page search">
      <div className="wrapper">
        <BreadHead bread={bread} />
        <PageHead h_one="Поиск" />
        <SearchBody />
      </div>
    </div>
  );
};

export default memo(SearchIndex);
