export const getNavRouter = path => [
  {
    to: `${path}/verse/`,
    title: 'Поиск по стихам',
  },
  {
    to: `${path}/dictionary/`,
    title: 'Поиск по словарям',
  },
  {
    to: `${path}/interpretation/`,
    title: 'Поиск по толкованиям',
  },
  {
    to: `${path}/propovedi/`,
    title: 'Поиск по проповедям',
  },
  {
    to: `${path}/mediateka/`,
    title: 'Поиск по медиатеке',
  },
];

export function parseTextSearch(text, regexpSearch) {
  return text ? text.replace(regexpSearch, p => `<strong class="yellow__text">${p}</strong>`) : '';
}
