import React from 'react';

import { SearchResultContent } from '../SearchResultContent/';
// utils
import { allAboutBible } from '~utils/common';

export const SearchDictionaries = ({ searchData = [], searchField }) => {
  return searchData.map(({ id, word = '', code, description = '', type = {} }) => (
    <SearchResultContent
      key={id}
      linkTo={`/${allAboutBible}/dictionaries/${type?.code}/${code}/`}
      textTitle={word}
      textAuthor={type?.title}
      description={description}
      searchField={searchField}
    />
  ));
};
