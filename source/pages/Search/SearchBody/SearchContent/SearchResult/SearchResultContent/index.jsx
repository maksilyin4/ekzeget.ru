import React from 'react';
import cx from 'classnames';

import MediaResultItem from './MediaResultItem/';

import { findSearchText } from '../../utilsMediaContent';

export const SearchResultContent = ({
  linkTo = '',
  textTitle = 'Нет наименования',
  textInfoBook = '',
  textAuthor = 'Без автора',
  description = '',
  searchField = '',
  translate,
}) => {
  return (
    <a href={linkTo} target="blank" className="search-content-body-item">
      {textTitle && (
        <MediaResultItem
          className="search-item-author"
          text={findSearchText(textTitle, searchField)}
        />
      )}
      {textInfoBook && (
        <MediaResultItem
          className="search-item-info"
          text={findSearchText(textInfoBook, searchField)}
        />
      )}
      {textAuthor && (
        <MediaResultItem
          className="search-item-span"
          text={findSearchText(textAuthor, searchField)}
        />
      )}
      <MediaResultItem
        className={cx('search-item-desc', {
          'big-desc': description?.length > 350,
          [translate]: translate,
        })}
        text={findSearchText(description, searchField)}
      />
    </a>
  );
};
