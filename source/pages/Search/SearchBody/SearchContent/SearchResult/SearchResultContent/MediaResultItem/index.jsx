import React from 'react';

const MediaResultItem = ({ className, text }) => (
  <p className={className} dangerouslySetInnerHTML={text} />
);

export default MediaResultItem;
