import React from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { useLocation } from 'react-router-dom';

import './mediaResultSearch.scss';

import { getQueryUrl, queryFilter } from '~utils/common';

import MediaResultHead from './MediaResultHead';
import { SearchMediateka } from './SearchMediateka/';
import { SearchBible } from './SearchBible/';
import { SearchInterpretations } from './SearchInterpretations';
import { SearchQuiz } from './SearchQuiz';
import { SearchDictionaries } from './SearchDictionaries/';
import { SearchPreaching } from './SearchPreaching/';
import Preloader from '~components/Preloader/Preloader';

const setContent = {
  mediateka: SearchMediateka,
  bible: SearchBible,
  interpretations: SearchInterpretations,
  viktorina: SearchQuiz,
  dictionaries: SearchDictionaries,
  propovedi: SearchPreaching,
};

const MediaResultSearch = ({
  searchData,
  searchField,
  pathname,
  searchId,
  total = 0,
  handleScroll = f => f,
  loading,
  variables,
}) => {
  const { connected_to_book_id, connected_to_chapter_id, connected_to_verse_id } = variables;
  const Component = setContent[searchId];

  const { search } = useLocation();

  const translate = getQueryUrl({ search, tag: queryFilter.translate });

  return (
    <div className="search-content-results">
      <MediaResultHead total={total} searchField={searchField} />
      <InfiniteScroll
        dataLength={searchData.length}
        next={handleScroll}
        scrollThreshold="50%"
        hasMore>
        <div className="search-content-body">
          <Component
            pathname={pathname}
            searchData={searchData}
            searchField={searchField}
            translate={translate}
            bookId={connected_to_book_id}
            chapterId={connected_to_chapter_id}
            versesId={connected_to_verse_id}
          />
          {loading && <Preloader className="search-content-preloader" />}
        </div>
      </InfiniteScroll>
    </div>
  );
};

export default React.memo(MediaResultSearch);
