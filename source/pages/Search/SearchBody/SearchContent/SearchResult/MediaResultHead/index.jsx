import React from 'react';

function getWordLoad(count) {
  let lastLetter = 'ов';
  const countBool = count % 100 < 10 || count % 100 > 20;

  if (countBool && count % 10 === 1) {
    lastLetter = '';
  } else if (countBool && (count % 10 === 2 || count % 10 === 3)) {
    lastLetter = 'а';
  }

  return `материал${lastLetter}`;
}

const MediaResultHead = ({ searchField = '', total = 0 }) => (
  <div className="search-content-head">
    <p>
      Результаты поиска: <span className="search-text-result">{searchField}</span>
    </p>
    <p>
      Найдено {total} {getWordLoad(total)}
    </p>
  </div>
);

export default MediaResultHead;
