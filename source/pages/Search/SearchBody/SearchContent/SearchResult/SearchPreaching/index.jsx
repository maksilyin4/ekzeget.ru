import React from 'react';

import { SearchResultContent } from '../SearchResultContent/';
// utils
import { allAboutBible } from '~utils/common';

export const SearchPreaching = ({ searchData = [], searchField }) => {
  return searchData.map(({ id, theme = '', text = '', preacher = {}, excuse = {} }) => (
    <SearchResultContent
      key={id}
      linkTo={`/${allAboutBible}/propovedi/${excuse?.parent?.code}/${excuse?.code}/${preacher?.code}/`}
      textTitle={theme}
      textAuthor={preacher?.name}
      description={text}
      searchField={searchField}
    />
  ));
};
