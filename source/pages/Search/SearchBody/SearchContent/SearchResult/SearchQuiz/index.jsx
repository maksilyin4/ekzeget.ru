import React from 'react';

import { SearchResultContent } from '../SearchResultContent/';
// utils
import { bibleyskayaViktorinaVoprosi } from '~utils/common';

export const SearchQuiz = ({ searchData = [], searchField }) => {
  return searchData.map(({ id, title = '', description = '', code = '' }) => (
    <SearchResultContent
      key={id}
      linkTo={`${bibleyskayaViktorinaVoprosi}${code}`}
      textTitle={title}
      textAuthor={''}
      description={description}
      searchField={searchField}
    />
  ));
};
