import React from 'react';

import { SearchResultContent } from '../SearchResultContent/';
// utils
import { getLinkForMediateka, setInfoForMedia, mediaBibleFilter } from '../../utilsMediaContent';
import { parsePathname } from '~utils/parsePathname';

const setUrl = (pathname = '') => parsePathname({ pathname, index: 1 });

export const SearchMediateka = ({
  searchData = [],
  searchField,
  pathname,
  bookId,
  chapterId,
  versesId,
}) => {
  return searchData.map(
    ({ author, id, text = '', categories = '', template, title, code, media_bible }) => {
      const mediaBible = bookId
        ? media_bible.filter(mediaBibleFilter({ versesId, chapterId, bookId }))
        : media_bible;
      return (
        <SearchResultContent
          key={id}
          linkTo={getLinkForMediateka({
            template,
            categories,
            code,
            id,
            url: setUrl(pathname),
          })}
          textTitle={title}
          textInfoBook={setInfoForMedia(mediaBible)}
          textAuthor={author?.title}
          description={text}
          searchField={searchField}
        />
      );
    },
  );
};
