import React from 'react';
import { useLocation } from 'react-router-dom';

import { SearchResultContent } from '../SearchResultContent/';

import { setLinkForBible, setTextForTitle, mediaBibleFilter } from '../../utilsMediaContent';
import { getQueryUrl, queryFilter, bible } from '~utils/common';

export const SearchInterpretations = ({
  searchData = [],
  searchField,
  bookId,
  chapterId,
  versesId,
}) => {
  const { search } = useLocation();
  const translate = getQueryUrl({ search, tag: queryFilter.translate });

  return searchData.map(({ id, verses = [] }) => {
    const verseInterp = bookId
      ? verses.filter(mediaBibleFilter({ versesId, chapterId, bookId, setName: 'id' }))
      : verses;

    return (
      <React.Fragment key={id}>
        {verseInterp.map(({ id, number, text = '', book, chapter, verse_translate = [] }) => {
          const description =
            translate && verse_translate?.length
              ? verse_translate.find(({ code }) => code === translate)?.text ||
                'Перевод отсутствует'
              : text;

          const chapterNumber = chapter?.number ?? 1;
          const bookText = setTextForTitle({ text: book?.short_title });
          const chapterText = setTextForTitle({ text: chapterNumber });
          const verseText = setTextForTitle({ setName: ':', text: number });

          const textInfoBook = `${bookText} ${chapterText}${verseText}`;

          return (
            <SearchResultContent
              key={id}
              linkTo={setLinkForBible({
                url: bible,
                bookName: book?.code,
                chapterNumber: chapter?.number ?? 1,
                verseNumber: number,
              })}
              textTitle={book?.title}
              textInfoBook={textInfoBook}
              textAuthor={book?.author}
              description={description}
              searchField={searchField}
            />
          );
        })}
      </React.Fragment>
    );
  });
};
