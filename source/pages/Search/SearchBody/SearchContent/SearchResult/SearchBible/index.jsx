import React from 'react';

import { SearchResultContent } from '../SearchResultContent/';

import { setLinkForBible, setTextForTitle } from '../../utilsMediaContent';

import { parsePathname } from '~utils/parsePathname';

const setUrl = (pathname = '') => parsePathname({ pathname, index: 1 });

export const SearchBible = ({ searchData = [], searchField, pathname, translate = '' }) => {
  const url = setUrl(pathname);

  return searchData.map(({ id, number, text = '', book, chapter, verse_translate = [] }) => {
    const description =
      translate && verse_translate?.length
        ? verse_translate.find(({ code }) => code === translate)?.text || 'Перевод отсутствует'
        : text;

    const chapterNumber = chapter?.number ?? 1;
    const bookText = setTextForTitle({ text: book?.short_title });
    const chapterText = setTextForTitle({ text: chapterNumber });
    const verseText = setTextForTitle({ setName: ':', text: number });

    const textInfoBook = `${bookText} ${chapterText}${verseText}`;

    return (
      <SearchResultContent
        key={id}
        linkTo={setLinkForBible({
          url,
          bookName: book?.code,
          chapterNumber,
          verseNumber: number,
        })}
        textTitle={`${book?.title} `}
        textInfoBook={textInfoBook}
        textAuthor={book?.author}
        description={description}
        searchField={searchField}
        translate={translate}
      />
    );
  });
};
