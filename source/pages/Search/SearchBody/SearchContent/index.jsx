import React, { useCallback, memo, useMemo } from 'react';
import { useLocation, useRouteMatch } from 'react-router-dom';

import useMediaSearchQuery from '~apollo/query/mediaLibrary/search/';

import SearchResult from './SearchResult/';
import MediaNotContent from '~pages/Mediateka/MediaBody/MediaContent/MediaNotContent/';
import Preloader from '~components/Preloader/Preloader';

import { getQueryUrl, queryFilter, fetchInfiniteScroll } from '~utils/common';
import { setVariables } from '~pages/Mediateka/MediaBody/MediaContent/utils';
import { setNameQuery } from './utilsMediaContent';
import { searchIds } from '~pages/Search/SearchBody/SearchContent/utilsMediaContent';

const SearchContent = () => {
  const { pathname, search: searchLocation } = useLocation();
  const {
    params: { searchId, subSearchId },
  } = useRouteMatch();

  const searchField = getQueryUrl({ search: searchLocation, tag: queryFilter.search }) || '';

  const categoryId = subSearchId || (searchId === searchIds.mediateka ? '' : searchId);
  const isSearch = searchId === searchIds.bible;

  const variables = setVariables({ search: searchLocation, categoryId, isSearch });

  const {
    item: { searchData = [], total, loading, isLoading },
    fetchMore = f => f,
    error = '',
  } = useMediaSearchQuery(searchId, variables, searchField);

  const handleScroll = useCallback(() => {
    if (total !== searchData.length) {
      const setName = setNameQuery(searchId);
      fetchInfiniteScroll({ data: searchData, fetchMore, variables, setName });
    }
  }, [searchData, searchId]);

  const renderContent = useMemo(() => {
    return searchData.length && searchField && !error ? (
      <SearchResult
        handleScroll={handleScroll}
        searchId={searchId}
        searchData={searchData}
        searchField={searchField}
        pathname={pathname}
        total={total}
        loading={loading}
        variables={variables}
      />
    ) : (
      <MediaNotContent />
    );
  }, [searchData, searchField, pathname, searchId, total, error, loading, searchLocation]);

  return <div className="search-content-page">{isLoading ? <Preloader /> : renderContent}</div>;
};

export default memo(SearchContent);
