import { defaultTemplate } from '~pages/Mediateka/MediaBody/utilsMediaBody';

function findSearchIndex(text, searchText) {
  return text?.toLowerCase().indexOf(searchText.toLowerCase());
}

function sliceText(text, searchText) {
  return text?.substr(findSearchIndex(text, searchText), searchText.length);
}

function parseText(text, searchText) {
  const regex = new RegExp(searchText, 'gi');
  const textStr = Array.isArray(text) ? text[0] : text;

  searchText = `<span class="replace-text">${sliceText(textStr, searchText)}</span>`;
  return textStr?.replace(regex, searchText);
}

export function findSearchText(text = '', searchText = '') {
  text = parseText(text, searchText);

  return { __html: text };
}

export function getLinkForMediateka({ template, categories, code, id, url }) {
  if (categories.length) {
    const categoryCode = categories[0].code;
    const lastUrl =
      defaultTemplate.motivator !== template
        ? `detail-${code}/`
        : `${categoryCode}/?motivator=${id}`;

    return `/${url}/${lastUrl}`;
  }
  return `/${url}/`;
}

export const setLinkForBible = ({
  url = '',
  bookName = '',
  chapterNumber = 1,
  verseNumber = 1,
}) => {
  if (url) {
    return `/${url}/${bookName}/glava-${chapterNumber}/stih-${verseNumber}`;
  }
  return '/';
};
export const searchIds = {
  bible: 'bible',
  mediateka: 'mediateka',
  interpretations: 'interpretations',
  dictionaries: 'dictionaries',
  quiz: 'viktorina',
  preacher: 'propovedi',
};

export const setTextForTitle = ({ setName = '', text = '' }) => {
  return text ? `${setName}${text}` : text;
};

export const setNameQuery = (category = '') => {
  switch (category) {
    case searchIds.mediateka:
      return 'medias';
    case searchIds.bible:
      return 'verses';
    case searchIds.interpretations:
      return 'interpretations';
    case searchIds.dictionaries:
      return 'dictionaries';
    case searchIds.quiz:
      return 'queeze_questions_list';
    case searchIds.preacher:
      return 'preaching_list';
    default:
      return '';
  }
};

export const setInfoForMedia = media_bible => {
  const mediaBible = media_bible || [];
  return mediaBible.map(({ book, chapter, verse }, i) => {
    const bookText = setTextForTitle({ text: book.short_title });
    const chapterText = setTextForTitle({
      text: chapter?.number,
    });
    const verseText = setTextForTitle({
      text: verse?.number,
      setName: ':',
    });

    const comma = mediaBible.length - 1 !== i ? ', ' : '';

    return `${bookText} ${chapterText}${verseText}${comma}
      `;
  });
};

export const mediaBibleFilter = ({ versesId, chapterId, bookId, setName = '' }) => ({
  book,
  chapter,
  ...rest
}) => {
  const verseResultId = setName ? rest[setName] : rest.verse?.id;
  const isVerse = versesId
    ? versesId.some(vId => +vId === +verseResultId) && +chapterId === +chapter?.id
    : +chapterId === +chapter?.id;
  return chapterId ? +book?.id === +bookId && isVerse : +book?.id === +bookId;
};
