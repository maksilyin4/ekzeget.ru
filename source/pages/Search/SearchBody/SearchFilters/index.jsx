import React, { useMemo } from 'react';
import { useLocation } from 'react-router-dom';

import MediaHeader from '~components/Mediateka/MediaHeader/';
import MediaFilters from '~components/Mediateka/MediaFilters/';

import { setIsShowFilter } from './utilsSearchFilters';

const SearchFilters = () => {
  const { pathname } = useLocation();
  const isShowFilter = useMemo(() => setIsShowFilter(pathname), [pathname]);

  return (
    <div className="search-filters">
      <MediaHeader />
      {isShowFilter && <MediaFilters />}
    </div>
  );
};

export default SearchFilters;
