import React, { useState, useEffect } from 'react';

import MediaNavParent from '../../../../Mediateka/MediaBody/MediaBodyLeftBlock/MediaGroupNav/MediaNavCategory/MediaNavParent/';

export const SearchNavigationDesktop = ({
  searchNavigation = [],
  categoryId,
  categorySub,
  categorySubChild,
  search,
}) => {
  const [state, setState] = useState({
    isCategoryId: categorySub.length > 0,
    isCategoryIdSub: categorySubChild.length > 0,
  });

  useEffect(() => {
    const setName = 'isCategoryId';
    setState({ ...state, [setName]: true });
  }, [categoryId]);

  const activeParent = (children = []) => {
    return children.length && categorySub?.length ? 'active__parent' : 'active';
  };

  const handleShowCategory = setName => () => {
    setState({ ...state, [setName]: !state[setName] });
  };

  const checkIsChild = (children = [], value, id) => {
    return children.length > 0 && id === value;
  };

  return searchNavigation.map(({ id, label, children, value }) => {
    const isChildCategory = checkIsChild(children, value, categoryId);
    return (
      <React.Fragment key={id}>
        <MediaNavParent
          title={label}
          path="/search/"
          code={value}
          search={search}
          activeParent={activeParent(children)}
          isShowParent={state.isCategoryId}
          onClick={handleShowCategory('isCategoryId')}
          child={children}
          isChild={isChildCategory}
        />
        {state.isCategoryId &&
          isChildCategory &&
          children.map(sub => {
            const isChildCategorySub = checkIsChild(sub.children, sub.value, categorySub);
            return (
              <MediaNavParent
                key={sub.id}
                title={sub.label}
                path={`/search/${value}/`}
                search={search}
                code={sub.value}
                className="search-children"
                activeParent={activeParent(sub.children, sub.value)}
                isShowParent={state.isCategoryIdSub}
                isChild={isChildCategorySub}
              />
            );
          })

        // <MediaNavChild path={`/search/${code}/`} child={children} isChild />
        }
      </React.Fragment>
    );
  });
};
