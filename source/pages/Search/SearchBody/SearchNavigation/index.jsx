// eslint-disable-next-line no-unused-vars
import React, { useMemo } from 'react';
import { useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';
// css
import './searchNavigation.scss';
import '../../../Mediateka/MediaBody/MediaBodyLeftBlock/MediaGroupNav/mediaNav.scss';
import '../../../Mediateka/MediaBody/MediaBodyLeftBlock/mediaBodyLeftBlock.scss';
// redux
import { getIsTabletPhone } from '../../../../dist/selectors/screen';
// fetch
import useMediaCategoriesQuery from '../../../../apolloClient/query/mediaLibrary/categories';
// components
import { SearchNavigationDesktop } from './SearchNavigationDesktop/';
import { SearchNavigationMobile } from './SearchNavigationMobile/';
// utils
import { parsePathname } from '~utils/parsePathname';
import { setSearchNavigation } from './utilsSearchNavigation';

const SearchNavigation = () => {
  const { categories, loading } = useMediaCategoriesQuery();
  const { pathname, search } = useLocation();

  const isAdaptive = useSelector(getIsTabletPhone);

  const categoryId = parsePathname({ pathname, index: 1, defaultReturn: '' });
  const categorySub = parsePathname({ pathname, index: 2, defaultReturn: '' });
  const categorySubChild = parsePathname({ pathname, index: 3, defaultReturn: '' });

  const searchData = useMemo(() => setSearchNavigation({ isAdaptive, categories }), [
    categories,
    isAdaptive,
  ]);

  if (loading) {
    return null;
  }

  return (
    <div className="media-library__wrapper search-navigation media-library ">
      <div className="nav-side nav-side__top media-library-left-nav">
        {isAdaptive ? (
          <SearchNavigationMobile
            searchNavigation={searchData}
            categoryId={categoryId}
            categorySub={categorySub}
            search={search}
          />
        ) : (
          <SearchNavigationDesktop
            searchNavigation={searchData}
            categoryId={categoryId}
            categorySub={categorySub}
            categorySubChild={categorySubChild}
            search={search}
          />
        )}
      </div>
    </div>
  );
};

export default SearchNavigation;
