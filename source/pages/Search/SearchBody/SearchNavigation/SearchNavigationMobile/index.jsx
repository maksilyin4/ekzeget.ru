import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import { MediaMenuMobile } from '~components/Mediateka/MediaMenuMobile';

export const SearchNavigationMobile = ({
  searchNavigation = [],
  categoryId = '',
  categorySub = '',
  search,
}) => {
  const history = useHistory();
  const [state, setState] = useState({
    categoryValue: null,
    subCategoryValue: null,
    selectCategoriesChild: [],
  });

  useEffect(() => {
    const categoryValue = searchNavigation.find(({ value }) => value === categoryId);
    const selectCategoriesChild = categoryValue?.children ?? [];
    const subCategoryValue = selectCategoriesChild.length
      ? selectCategoriesChild.find(({ value }) => value === categorySub)
      : null;
    if (categoryValue) {
      setState({
        ...state,
        categoryValue: [categoryValue],
        selectCategoriesChild,
        subCategoryValue,
      });
    }
  }, []);

  const handleChange = select => {
    const selectCategoriesChild = select.children || [];
    setState({ ...state, categoryValue: select, selectCategoriesChild });
    history.push({ pathname: `/search/${select.value}/`, search });
  };

  const handleChangeChildren = select => {
    setState({ ...state, subCategoryValue: select || null });
    const linkChild = select ? (select.value ? `${select.value}/` : '') : '';
    history.push({ pathname: `/search/${categoryId}/${linkChild}`, search });
  };

  return (
    <MediaMenuMobile
      options={searchNavigation}
      optionsChildren={state.selectCategoriesChild}
      value={state.categoryValue}
      valueChild={state.subCategoryValue}
      handleChange={handleChange}
      handleChangeChildren={handleChangeChildren}
    />
  );
};
