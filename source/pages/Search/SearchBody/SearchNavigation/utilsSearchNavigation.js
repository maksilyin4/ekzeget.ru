const searchNavigation = [
  {
    id: 'bible',
    label: 'Поиск по Библии',
    value: 'bible',
  },
  {
    id: 'mediateka',
    label: 'Поиск по медиатеке',
    value: 'mediateka',
  },
  {
    id: 'interpretations',
    label: 'Поиск по толкованиям',
    value: 'interpretations',
  },
  {
    id: 'dictionaries',
    label: 'Поиск по словарям',
    value: 'dictionaries',
  },
  {
    id: 'propovedi',
    label: 'Поиск по проповедям',
    value: 'propovedi',
  },
  {
    id: 'viktorina',
    label: 'Поиск по  викторине',
    value: 'viktorina',
  },
];

const setSearchNavigation = ({ isAdaptive, categories = [] }) => {
  return searchNavigation.map(({ id, ...rest }) => {
    if (id === 'mediateka' && categories.length) {
      const childrenAll = isAdaptive ? [{ id: 'all', label: 'Все разделы', value: '' }] : [];
      return {
        children: [
          ...childrenAll,
          ...categories.map(({ id, title, code }) => ({
            id,
            label: title,
            value: code,
          })),
        ],
        id,
        ...rest,
      };
    }
    return {
      children: [],
      id,
      ...rest,
    };
  });
};

export { setSearchNavigation, searchNavigation };
