import React from 'react';
import { Route, Switch } from 'react-router-dom';

import SearchBody from './SearchBody';
import NotFound from '../../NotFound';

const SearchContent = () => {
  return (
    <Switch>
      <Route
        exact
        path={['/search/:searchId/', '/search/:searchId/:subSearchId/']}
        component={SearchBody}
      />
      <Route component={NotFound} />
    </Switch>
  );
};

export default SearchContent;
