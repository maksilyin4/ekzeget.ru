import React from 'react';

import './searchBody.scss';

import SearchNavigation from './SearchNavigation/';
import SearchFilters from './SearchFilters/';
import SearchContent from './SearchContent/';

const SearchBody = () => {
  return (
    <div className="search-container">
      <SearchNavigation />
      <div className="search-content">
        <SearchFilters />
        <SearchContent />
      </div>
    </div>
  );
};

export default SearchBody;
