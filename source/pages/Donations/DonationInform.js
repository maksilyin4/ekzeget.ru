import React, { Component } from 'react';
import './styles.scss';

export default class DonationInform extends Component {
  render() {
    return (
      <div className="donation__content">
        <div className="donation__content_sberbankInfo">
          <h2>
            Также можно сделать банковский перевод
            <br />
            по реквизитам на счет Сбербанка:
          </h2>
          <div>
            <p>Получатель платежа: АНО «Доброе дело»</p>
            <p>Банк: ПАО Сбербанк</p>
            <p>ИНН 7731329442, КПП 773101001</p>
            <p>БИК 044525225</p>
            <p>к/с 30101810400000000225</p>
            <p>р/с 40703810738000005564</p>
            <p>С пометкой «на сайт Экзегет.ру»</p>
          </div>
        </div>
      </div>
    );
  }
}
