import React, { Component } from 'react';
// import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';

import PageHead from '~components/PageHead';
import BreadHead from '~components/BreadHead';
import DonationForm from './DonationForm';
import DonationInform from './DonationInform';

import { sendSocialActions, getMetaData, isMetaData, smoothScrollTo } from '../../dist/utils';

import './styles.scss';
import { setClearMeta } from '../../store/metaData/actions';

class Donations extends Component {
  state = {
    meta: {
      h_one: 'пожертвования',
    },
    h_one: 'Пожертвования',
    isLoad: true,
  };

  componentDidMount() {
    this.props.setClearMeta();

    smoothScrollTo();

    getMetaData('/pozhertvovaniya/').then(meta => {
      if (meta || meta !== 'Error') {
        this.setState({
          meta,
          // h_one: isMetaData('h_one', meta, 'Пожертвования'),
        });
      }
    });

    sendSocialActions({
      ga: ['event', 'event_name', { event_category: 'pozhertvovanie', event_action: 'click' }],
      ym: 'pozhertvovanie',
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const { meta } = this.state;
    const isData = isMetaData('h_one', meta, 'Пожертвования');
    if (isData !== prevState.h_one) {
      this.setState({ h_one: isData, isLoad: false });
    } else if (prevState.isLoad) this.setState({ isLoad: false });
  }

  _breadItems = () => [
    { path: '/', title: 'Главная' },
    { path: '', title: 'Пожертвования' },
  ];

  render() {
    const { meta, isLoad } = this.state;

    return (
      <div className="page donation">
        <div className="wrapper donation__header">
          <BreadHead bread={this._breadItems()} />
          <PageHead meta={meta} h_one={'Пожертвования'} isLoad={isLoad} />

          <div className="page__head__description">
            <p>
              Наш интернет-проект ekzeget.ru не имеет какого-то постоянного источника доходов,
              существует исключительно на пожертвования тех, кто им пользуется и кому он помогает.
            </p>
            <br />
            <p>
              Если Вы хотите помочь нашему проекту, чтобы он продолжал существовать и развиваться и
              помогал изучающим Слово Божие, то можете выбрать любой из предложенных способов
              пожертвования.
            </p>
          </div>
        </div>
        <div className="page__body donation__body">
          <div className="wrapper wrapper_two-col">
            <DonationForm />
            <DonationInform />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = () => ({});
const mapDispatchToProps = {
  setClearMeta,
};

export default connect(mapStateToProps, mapDispatchToProps)(Donations);
