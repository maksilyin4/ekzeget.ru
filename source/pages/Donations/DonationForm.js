import React, { Component } from 'react';
import Form, { FormGroup, FormSubmit } from '../../components/Form/Form';
import { sendSocialActions } from '../../dist/utils';
import './styles.scss';

export default class DonationForm extends Component {
  state = {
    custName: '',
    sum: '',
    orderDetails: '',
  };

  getFormRef = el => {
    this.formRef = el;
  };

  handleInput = event => {
    const { name, value } = event.currentTarget;
    if (name === 'sum') {
      this.setState({
        [name]: value.replace(/\D/g, ''),
      });
    } else {
      this.setState({
        [name]: value,
      });
    }
  };

  sendAction = e => {
    e.preventDefault();
    sendSocialActions({
      ga: [
        'event',
        'event_name',
        { event_category: 'donation', event_action: 'click', value: this.state.sum },
      ],
      ym: 'donation',
    });
    this.formRef.submit();
  };

  render() {
    const { custName, sum, orderDetails } = this.state;
    return (
      <div className="donation__content">
        <Form
          formRef={this.getFormRef}
          className="donation_form"
          action="https://yookassa.ru/integration/simplepay/payment"
          method="post"
          target="_blank"
          onSubmit={this.sendAction}
          accept-charset="utf-8">
          <input name="shopId" type="hidden" value="973264" />
          <FormGroup className="user_form_input">
            <label htmlFor="custName">
              Ваше имя*
              <input
                type="text"
                name="custName"
                autoComplete="off"
                onChange={this.handleInput}
                value={custName}
                required
              />
            </label>
          </FormGroup>
          <FormGroup className="user_form_input">
            <label htmlFor="sum">
              Сумма пожертвования*
              <input
                type="number"
                min="50"
                name="sum"
                autoComplete="off"
                onChange={this.handleInput}
                value={sum}
                required
              />
            </label>
          </FormGroup>
          <FormGroup className="user_form_input">
            <label htmlFor="orderDetails">
              Комментарий
              <textarea
                name="orderDetails"
                cols="30"
                rows="5"
                autoComplete="off"
                onChange={this.handleInput}
                value={orderDetails}
              />
            </label>
          </FormGroup>
          <FormSubmit title="Пожертвовать" align="center" />
          <p className="donation_form_note">
            После нажатия на кнопку, Вам будет предложено сделать пожертвование любым удобным для
            Вас способом
          </p>
        </Form>
      </div>
    );
  }
}
