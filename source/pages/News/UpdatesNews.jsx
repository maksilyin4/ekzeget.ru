import R from 'ramda';
import React, { Component } from 'react';
import dayjs from 'dayjs';
import ruLocale from 'dayjs/locale/ru';
import { AXIOS } from '../../dist/ApiConfig';
import LIBody from '../../components/LatestInterpritations/LIBody';
import './styles.scss';

export default class UpdatesNews extends Component {
  state = {
    isFetching: false,
    list: [],
  };

  componentDidMount() {
    this.setState({
      isFetching: true,
    });
    AXIOS.get('interpretation/?sort=1&per-page=50').then(
      ({ data: { interpretations, status } }) => {
        if (status === 'ok') {
          this.setState({
            isFetching: false,
            list: interpretations.map(inter => ({
              id: inter.id,
              username: inter.added_by.username,
              date: dayjs
                .unix(inter.added_at)
                .locale('ru', ruLocale)
                .format('DD MMM YYYY'),
              verse: inter.verse[0],
              author: R.pathOr('Экзегет', ['ekzeget', 'name'], inter),
              authorId: R.pathOr(0, ['ekzeget', 'id'], inter),
              investigated: inter.investigated,
              code: inter.ekzeget.code,
            })),
          });
        }
      },
      () => {
        this.setState({
          isFetching: false,
        });
      },
    );
  }

  render() {
    const { list, isFetching } = this.state;
    return (
      <div className="news__interpretations content">
        <LIBody list={list} isFetching={isFetching} />
      </div>
    );
  }
}
