import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import cx from 'classnames';

import OrphusInfo from '../../components/OrphusInfo';

import './styles.scss';

export default class NavNews extends PureComponent {
  render() {
    const { contentType, selectTypeNews, url } = this.props;

    return (
      <div className="side news__wrapper">
        <div className="side__item nav-side">
          <div className="list__item" onClick={() => selectTypeNews(true)}>
            <NavLink
              to={`${url}novosti/`}
              className={cx('nav-side__link list__item-link news__item', {
                active: contentType,
              })}>
              Новости
            </NavLink>
          </div>
          <div className="list__item" onClick={() => selectTypeNews(false)}>
            <NavLink
              to={`${url}obnovleniya/`}
              className={cx('nav-side__link list__item-link news__item', {
                active: !contentType,
              })}>
              Обновления
            </NavLink>
          </div>
        </div>
        <OrphusInfo />
      </div>
    );
  }
}

NavNews.propTypes = {
  contentType: PropTypes.bool.isRequired,
  selectTypeNews: PropTypes.func.isRequired,
  url: PropTypes.string,
};
