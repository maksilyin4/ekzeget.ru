import React from 'react';
import Loadable from 'react-loadable';
import Preloader from '../../components/Preloader/Preloader';

const LoadableBar = Loadable({
  loader: () => import('./NewsIndex'),
  loading() {
    return (
      <div className="preloader-relative">
        <Preloader />
      </div>
    );
  },
  render(loaded, props) {
    const Component = loaded.default;
    return <Component {...props} />;
  },
  delay: 300,
});

export default LoadableBar;
