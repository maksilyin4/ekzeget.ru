import React, { Component } from 'react';
// import { Helmet } from 'react-helmet';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';

import { getMetaData, isMetaData, smoothScrollTo } from '../../dist/utils';
import BreadHead from '~components/BreadHead';
import PageHead from '~components/PageHead';
import NavNews from './NavNews';
import NewsList from '../../components/News';
import UpdatesNews from './UpdatesNews';
import { setClearMeta } from '../../store/metaData/actions';

class News extends Component {
  state = {
    contentType: true,
    meta: {
      h_one: 'Новости и обновления',
      title: 'Новости и обновления',
    },
    h_one: 'Новости и обновления',
    isLoad: true,
  };

  componentDidMount() {
    this.props.setClearMeta();
    smoothScrollTo();
    getMetaData('/novosti-i-obnovleniya/').then(meta => {
      if (meta || meta !== 'Error') {
        this.setState({
          meta,
          h_one: isMetaData('h_one', meta, 'Новости и обновления'),
        });
      }
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const { meta } = this.state;
    const isData = isMetaData('h_one', meta, 'Новости и обновления');
    if (isData !== prevState.h_one) {
      this.setState({ h_one: isData, isLoad: false });
    } else if (prevState.isLoad) this.setState({ isLoad: false });
  }

  _breadItems = () => [
    { path: '/', title: 'Главная' },
    { path: '', title: 'Новости и обновления' },
  ];

  selectTypeNews = contentType => {
    this.setState({
      contentType,
    });
  };

  render() {
    const { url } = this.props;
    const { contentType, meta, isLoad } = this.state;

    return (
      <div className="page news">
        <div className="side wrapper">
          <BreadHead bread={this._breadItems()} />
          {/* <PageHead h_one={h_one} isLoad={isLoad} /> */}
          <PageHead meta={meta} h_one={'Новости и обновления'} isLoad={isLoad} />

          <div className="news__body">
            <div className="wrapper_two-col">
              <NavNews contentType={contentType} selectTypeNews={this.selectTypeNews} url={url} />
              <Route
                path={`${url}:id`}
                render={props => {
                  return contentType ? (
                    <NewsList
                      directProps={props.directProps}
                      initialState={props.initialState}
                      articleType={1}
                      {...props}
                    />
                  ) : (
                    <UpdatesNews />
                  );
                }}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = () => ({});
const mapDispatchToProps = {
  setClearMeta,
};

export default connect(mapStateToProps, mapDispatchToProps)(News);
