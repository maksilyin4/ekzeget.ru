import React from 'react';

import './notFound.scss';
import Button from '../../components/Button/Button';
import BooksList from '../../components/BooksList/BooksList';

const NotFound = () => (
  <div className="page">
    <div className="wrapper">
      <div className="not-fount">
        <h2 className="not-fount__404">404</h2>
        <h3 className="not-fount__title">Извините, такой страницы не существует</h3>
        <p className="not-fount__subtitle">
          Страница, на которую вы пытались перейти, не существует, либо ещё не создана. Вы можете
          перейти на главную страницу или выбрать книгу.
        </p>
        <p className="not-fount__subtitle">
          Если же вы перешли на данную страницу с нашего сайта Экзегет, то отправьте нам ваш порядок
          действий, который привел на данную страницу.
        </p>
        <div className="not-fount__to-home">
          <Button href="/" title="На главную" />
        </div>
      </div>
      <section className="main-page__bible">
        <BooksList notFoundPage />
      </section>
    </div>
  </div>
);

export default NotFound;
