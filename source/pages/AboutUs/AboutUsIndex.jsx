import React, { useState, useEffect, useReducer } from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';

import { smoothScrollTo } from '../../dist/utils';
import { fetchMetaData, fetchArticule } from './fetch';

import BreadHead from '~components/BreadHead';
import PageHead from '~components/PageHead';
import AboutUsNav from './AboutUsNav';
import AboutContent from './AboutContent/AboutContent';

import { reducer, initialState } from './reducer';
import { titleForMenu } from './utilsAboutUs';

import './styles.scss';

const AboutUsPage = ({
  location: { pathname },
  url,
  match: {
    params: { id },
  },
}) => {
  const [state, dispatch] = useReducer(reducer, { ...initialState, id });

  const [metaData, setMeta] = useState({
    meta: { h_one: 'О сайте Экзегет.ру' },
    h_one: 'О сайте Экзегет.ру',
    isLoad: true,
  });

  useEffect(() => {
    smoothScrollTo();
    fetchMetaData(setMeta);
  }, []);

  useEffect(() => {
    fetchArticule(state, dispatch, id);
  }, [pathname]);

  const _breadItems = title => [
    { path: '/', title: 'Главная' },
    { path: '', title },
  ];

  const { meta } = metaData;
  const breadTitle = state.content?.[id]?.title ?? titleForMenu[id];

  return (
    <div className="page aboutUs">
      <Helmet>
        <title>Портал Экзегет ру - объединения известных толкования Священного Писания</title>
      </Helmet>
      <div className="wrapper">
        <BreadHead bread={_breadItems(breadTitle)} />
        <PageHead meta={meta} h_one={'О сайте Экзегет.ру'} />
      </div>
      <div className="aboutUs__body">
        <div className="wrapper wrapper_two-col">
          <AboutUsNav path={url} />
          <AboutContent path={url} state={state} />
        </div>
      </div>
    </div>
  );
};

AboutUsPage.propTypes = {
  url: PropTypes.string,
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }),
  initialState: PropTypes.shape({
    about: PropTypes.shape({
      content: PropTypes.string,
      title: PropTypes.string,
    }),
    howuse: PropTypes.shape({
      content: PropTypes.string,
      title: PropTypes.string,
    }),
  }),
  match: PropTypes.shape({
    params: PropTypes.shape({ id: PropTypes.string }),
  }),
};

export default AboutUsPage;
