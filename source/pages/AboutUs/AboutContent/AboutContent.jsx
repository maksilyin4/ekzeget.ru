import React from 'react';
import { Route } from 'react-router-dom';
import PropTypes from 'prop-types';

import AboutProject from './AboutProject';

const AboutContent = ({ path, state }) => {
  return <Route path={`${path}:id`} render={props => <AboutProject state={state} {...props} />} />;
};

AboutContent.propTypes = {
  content: PropTypes.shape({
    about: PropTypes.shape({
      content: PropTypes.string,
      title: PropTypes.string,
    }),
    howuse: PropTypes.shape({
      content: PropTypes.string,
      title: PropTypes.string,
    }),
  }),
  path: PropTypes.string,
  isError: PropTypes.bool,
};

export default AboutContent;
