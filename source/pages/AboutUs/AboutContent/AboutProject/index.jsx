import React from 'react';

import PropTypes from 'prop-types';
import Preloader from '~components/Preloader/Preloader';

const AboutProject = ({
  state = {},
  match: {
    params: { id },
  },
}) => {
  const { content, isLoaded } = state;

  return (
    <div className="content container-body ">
      {isLoaded && <Preloader />}
      <div
        className="aboutUs__content"
        dangerouslySetInnerHTML={{ __html: content?.[id]?.content || 'Нет информации' }}
      />
    </div>
  );
};

AboutProject.propTypes = {
  content: PropTypes.shape({
    about: PropTypes.shape({
      content: PropTypes.string,
      title: PropTypes.string,
    }),
    howuse: PropTypes.shape({
      content: PropTypes.string,
      title: PropTypes.string,
    }),
  }),
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }),
  }),
};

export default AboutProject;
