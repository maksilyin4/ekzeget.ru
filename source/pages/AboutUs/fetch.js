import { types } from './reducer';
import { getMetaData } from '../../dist/utils';
import { AXIOS } from '../../dist/ApiConfig';

export const fetchArticule = (state, dispatch, id) => {
  const { content } = state;

  if (!content?.[id]) {
    dispatch({ type: types.FETCH });
    AXIOS.get(`article/detail/${id}`)
      .then(({ data }) => {
        const {
          article: { body, title },
        } = data;

        dispatch({
          type: types.SUCCESS,
          payload: { content: { ...content, [id]: { content: body, title } }, id },
        });
      })
      .catch(e => {
        dispatch({
          type: types.ERROR,
        });
        console.error(e.message);
      });
  }
};

export const fetchMetaData = setMeta => {
  getMetaData('/o-proekte/')
    .then(meta =>
      setMeta({
        meta,
        h_one: meta.h_one,
        isLoad: false,
      }),
    )
    .catch(() => setMeta({ isLoad: true }));
};
