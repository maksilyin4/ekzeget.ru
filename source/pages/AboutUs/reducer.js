const initialState = {
  id: '',
  content: {},
  isLoaded: false,
};

const types = {
  FETCH: 'FETCH',
  SUCCESS: 'SUCCESS',
  ERROR: 'ERROR',
};

const reducer = (state, action) => {
  switch (action.type) {
    case types.FETCH:
      return { ...state, isLoaded: true };
    case types.SUCCESS:
      return { ...state, isLoaded: false, ...action.payload };
    case types.ERROR:
      return { ...state, isLoaded: false };
    default:
      return state;
  }
};

export { initialState, reducer, types };
