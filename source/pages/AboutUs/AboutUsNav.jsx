import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

import OrphusInfo from '../../components/OrphusInfo';

import { titleForMenu } from './utilsAboutUs';

const AboutUsNav = ({ path }) => {
  return (
    <div className="side side__wrapper about-us__wrapper">
      <div className="side__item nav-side">
        {Object.keys(titleForMenu).map(el => (
          <NavLink
            key={el}
            to={`${path}${el}`}
            className="list__item nav-side__link list__item-link about-us__link">
            {titleForMenu[el]}
          </NavLink>
        ))}
      </div>
      <OrphusInfo />
    </div>
  );
};

AboutUsNav.propTypes = {
  path: PropTypes.string,
};

export default AboutUsNav;
