import { addQueryUrl } from '~utils/addQueryUrl';
import {
  queryFilter,
  getQueryUrl,
  deleteSearchUrl,
  setSearch,
  fetchLoadScroll,
} from '~utils/common';
import { deleteQueryUrl } from '~utils/deleteQueryUrl';

export const limit = 15;

export const defaultTemplate = {
  main: 'M',
  img: 'I2',
  motivator: 'I1',
  video: 'V1',
};

export const relatedObj = {
  C: 'book',
  B: 'book',
  A: 'listen',
  V: 'play',
};

export const addSearchUrl = ({ search, name = queryFilter.tags, state }) => {
  const searchUrl = search.split('&');

  const stateStr = Array.isArray(state) ? state.join(',') : state;
  if (search) {
    const inx = searchUrl.findIndex(el => el.includes(name));

    if (inx !== -1) {
      searchUrl.splice(inx, 1, `${name}=${stateStr}`);

      const searchNew = searchUrl.join('&');

      const query = searchNew.length
        ? `${searchNew.includes('?') ? '' : '?'}${searchNew}`
        : `?${name}=${stateStr}`;
      return query;
    }

    return `${search}&${name}=${stateStr}`;
  }
  return `?{name}=${stateStr}`;
};

export const deleteQueryUrlFilter = ({
  search,
  history,
  name,
  del = [queryFilter.chapter, queryFilter.verse, queryFilter.book],
}) => {
  const searchUrl = search.split('&');
  let te;
  switch (name) {
    case queryFilter.book: {
      te = searchUrl.filter(el => {
        return del.findIndex(e => el.includes(e)) === -1;
      });
      break;
    }
    case queryFilter.chapter: {
      te = searchUrl.filter(el => {
        return del.findIndex(e => el.includes(e)) === -1;
      });
      break;
    }
    default: {
      te = searchUrl.filter(el => {
        return del.findIndex(e => el.includes(e)) === -1;
      });
      break;
    }
  }

  const str = te.join('&');

  const query = str.includes('?') ? '' : '?';

  history.push(`${query}${str}`);
};

export function deleteSearchQueryPopup({ search, history, name, stateHistory = '' }) {
  const searchUrl = search.split('&');
  const inx = searchUrl.findIndex(el => el.includes(name));
  if (inx !== -1) {
    searchUrl.splice(inx, 1);
    let searchNew = '';

    if (searchUrl.length > 1) {
      searchNew = searchUrl.join('&');
    } else {
      searchNew = searchUrl.join('');
    }

    const query = searchNew.includes('?') ? '' : '?';

    history.push(`${query}${searchNew}`, stateHistory);
  } else {
    const query = search.includes('?') ? '' : '?';
    history.push(`${query}${search}`, stateHistory);
  }
}

export function getQuery(queryValue) {
  let query;
  if (queryValue) {
    query = queryValue || '';
  }
  return query;
}

export function getUrlParams(search, queryFilter) {
  if (search && search.length) {
    const urlParams = new URLSearchParams(search);
    const queryTags = urlParams.get(queryFilter.tags);
    const queryBook = +urlParams.get(queryFilter.book);
    const queryChapter = +urlParams.get(queryFilter.chapter);
    const queryVerse = urlParams.get(queryFilter.verse);
    const queryCat = urlParams.get(queryFilter.cat);
    const querySort = urlParams.get(queryFilter.sort);
    const queryAuthor = +urlParams.get(queryFilter.author);
    const queryMotivator = +urlParams.get(queryFilter.motivator);

    return {
      queryTags,
      queryBook,
      queryChapter,
      queryVerse,
      queryCat,
      querySort,
      queryAuthor,
      queryMotivator,
    };
  }
  return {};
}

export const getArrSplitOrToNumber = (arr, isNumber) => {
  if (arr) {
    const newArr = arr?.split(',');
    return isNumber ? newArr.map(el => +el) : newArr;
  }
  return arr;
};

export function defineTemplate({ state, template, category_id }) {
  if (state) {
    return state;
  }
  if (!category_id) {
    return 'M';
  }

  const tempQueryCode = template && template.length && template[0].template_code;
  return tempQueryCode || 'M';
}

export function getToForLink({ i, url, related }) {
  const urlUpdate = url.endsWith('/') ? url : `${url}/`;
  return i === 0 ? urlUpdate : `${urlUpdate}${related}/`;
}

export {
  deleteQueryUrl,
  addQueryUrl,
  queryFilter,
  getQueryUrl,
  deleteSearchUrl,
  setSearch,
  fetchLoadScroll,
};
