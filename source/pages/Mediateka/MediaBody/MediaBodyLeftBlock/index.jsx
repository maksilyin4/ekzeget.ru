import React, { useContext, useMemo } from 'react';
import PropTypes from 'prop-types';
import { useLocation } from 'react-router-dom';

import { MediaContext } from '~context/MediaContext';

import MediaGroupNav from './MediaGroupNav/';
import MediaTags from './MediaTags';
import OrphusInfo from '~components/OrphusInfo';

import { parsePathname } from '~utils/parsePathname';

import './mediaBodyLeftBlock.scss';

const MediaBodyLeftBlock = () => {
  const { pathname, search } = useLocation();
  const { tabletMedium, desktop } = useContext(MediaContext);

  const mediaId = useMemo(() => parsePathname({ pathname, index: 2 }), [pathname]);

  const mediaBodyLeft = React.useMemo(
    () => (
      <div className="media-library__left-wrapper">
        <>
          <MediaGroupNav />
          {(tabletMedium || desktop) && !mediaId && <MediaTags />}
          <OrphusInfo />
        </>
      </div>
    ),
    [pathname, search, tabletMedium, desktop],
  );

  return mediaBodyLeft;
};

MediaBodyLeftBlock.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }),
};

export default MediaBodyLeftBlock;
