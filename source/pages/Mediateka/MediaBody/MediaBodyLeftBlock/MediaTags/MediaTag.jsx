import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const MediaTag = ({ title = '', tagCode = '', isActive = false, toggleTags = f => f }) => (
  <li className="tag__item">
    <button
      className={cx('tag__btn', {
        'tag__btn-active': isActive,
      })}
      onClick={toggleTags(tagCode)}>
      {title}
    </button>
  </li>
);

MediaTag.propTypes = {
  title: PropTypes.string.isRequired,
  tagCode: PropTypes.string.isRequired,
  isActive: PropTypes.bool.isRequired,
  toggleTags: PropTypes.func.isRequired,
};
export default MediaTag;
