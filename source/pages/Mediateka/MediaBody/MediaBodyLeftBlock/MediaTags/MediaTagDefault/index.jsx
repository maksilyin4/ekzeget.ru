import React from 'react';

import MediaTag from '../MediaTag';

import Preloader from '~components/Preloader/Preloader';

const MediaTagDefault = ({ resetTags, tags, toggleTags, activeTagIds, loading }) => {
  if (loading) {
    return (
      <div className="media-tags-container">
        <Preloader />
      </div>
    );
  }
  return (
    <div className="media-tags-container">
      <div className="media-tag">
        <p className="tags__desc">Теги</p>
        <button className="tags__reset-btn" onClick={resetTags}>
          Сбросить
        </button>
      </div>
      <div className="tags__container">
        <ul className="tags__list">
          {tags.map(tag => (
            <MediaTag
              key={tag.id}
              title={tag.title}
              isActive={activeTagIds.includes(tag.id)}
              tagCode={tag.id}
              toggleTags={toggleTags}
            />
          ))}
        </ul>
      </div>
    </div>
  );
};

export default React.memo(MediaTagDefault);
