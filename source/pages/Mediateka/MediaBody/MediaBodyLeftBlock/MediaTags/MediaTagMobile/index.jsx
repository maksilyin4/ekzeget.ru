import React, { useState, useCallback } from 'react';
import cx from 'classnames';

import MediaTag from '../MediaTag';
import MediaBtn from '~components/Mediateka/BtnMedia/';

import Preloader from '~components/Preloader/Preloader';

import './mediaTagMobile.scss';

const MediaTagMobile = ({ resetTags, tags, toggleTags, activeTagIds, loading }) => {
  const [isOpen, setIsOpen] = useState(false);

  const handleOpen = useCallback(() => setIsOpen(!isOpen), [isOpen]);

  if (loading) {
    return (
      <div className="media-tag-mobile">
        <Preloader />
      </div>
    );
  }

  return (
    <div
      className={cx('media-tag-mobile', {
        'tags-btn-open': isOpen,
      })}>
      <MediaBtn text="Теги" onClick={handleOpen} styleName="tags-btn" />

      {isOpen && (
        <div className="media-tag-mobile-list">
          <div className="media-tag">
            <button className="tags__reset-btn" onClick={resetTags}>
              Сбросить
            </button>
          </div>
          <div className="tags__container">
            <ul className="tags__list">
              {tags.map(tag => (
                <MediaTag
                  key={tag.id}
                  title={tag.title}
                  isActive={activeTagIds.includes(tag.id)}
                  tagCode={tag.id}
                  toggleTags={toggleTags}
                />
              ))}
            </ul>
          </div>
        </div>
      )}
    </div>
  );
};

export default React.memo(MediaTagMobile);
