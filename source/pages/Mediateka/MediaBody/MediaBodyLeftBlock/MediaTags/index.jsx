import React, { useEffect, useContext, useMemo, useCallback } from 'react';
import PropTypes from 'prop-types';
import { useLocation, useHistory } from 'react-router-dom';

import { MediaContext } from '~context/MediaContext';
import useTagsQuery from '~apollo/query/mediaLibrary/tags';

import MediaTagDefault from './MediaTagDefault';
import MediaTagMobile from './MediaTagMobile';

import { deleteQueryUrl, addQueryUrl, queryFilter, getQueryUrl } from '../../utilsMediaBody';

import './mediaTags.scss';

const MediaTags = () => {
  const { search } = useLocation();
  const history = useHistory();

  const { tablet, phone } = useContext(MediaContext);
  const { tags, loading } = useTagsQuery();

  const urlParamsTags = useMemo(() => getQueryUrl({ search, tag: queryFilter.tags }), [search]);
  const activeTagIds = useMemo(() => (search && urlParamsTags ? urlParamsTags.split(',') : []), [
    search,
    urlParamsTags,
  ]);

  useEffect(() => {
    if (!urlParamsTags) {
      deleteQueryUrl({ search, history, name: queryFilter.tags });
    }
  }, [search]);

  const toggleTags = useCallback(
    tagId => () => {
      const index = activeTagIds.indexOf(tagId);
      if (index === -1) {
        activeTagIds.push(tagId);
      } else {
        activeTagIds.splice(index, 1);
      }
      addQueryUrl({ search, history, name: queryFilter.tags, state: activeTagIds });
    },
    [activeTagIds],
  );

  const resetTags = () => {
    if (search) {
      deleteQueryUrl({ search, history, name: queryFilter.tags });
    }
  };

  return (
    <>
      {!tablet && !phone && !loading ? (
        <MediaTagDefault
          resetTags={resetTags}
          tags={tags}
          toggleTags={toggleTags}
          activeTagIds={activeTagIds}
          loading={loading}
        />
      ) : (
        <MediaTagMobile
          resetTags={resetTags}
          tags={tags}
          toggleTags={toggleTags}
          activeTagIds={activeTagIds}
          loading={loading}
        />
      )}
    </>
  );
};

MediaTags.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string,
    search: PropTypes.string,
  }),
};

export default MediaTags;
