import React, { memo } from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

const MediaNavChild = ({
  path = '',
  search = '',
  child = [],
  onClick = f => f,
  oddEvent = () => f => f,
  isChild = false,
  parentCode = '',
  templateCode = '',
  parentInfo,
}) => {
  return (
    <div className={cx('media-library-link-child', { active: isChild })}>
      {child.map(({ id, title, code }) => (
        <NavLink
          key={id}
          to={{
            pathname: `${path}${parentCode}/${code}/`,
            search,
            state: templateCode,
          }}
          onClick={onClick({ parent: parentInfo, children: { title, code } })}
          className="list__item nav-side__link list__item-link media-library-link-child__link"
          isActive={oddEvent && oddEvent(code)}>
          {title}
        </NavLink>
      ))}
    </div>
  );
};

MediaNavChild.propTypes = {
  path: PropTypes.string,
  search: PropTypes.string,
  child: PropTypes.array,
  oddEvent: PropTypes.func,
  isChild: PropTypes.bool,
  parentCode: PropTypes.string,
  templateCode: PropTypes.string,
  onClick: PropTypes.func,
};

export default memo(MediaNavChild);
