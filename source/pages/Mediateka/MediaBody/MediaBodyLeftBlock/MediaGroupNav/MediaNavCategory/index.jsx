import React, { memo, useState, useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';

import { setMediatekaBreadcrumbs } from '~store/mediateka/action';
import { getMediatekaBreadcrumbs } from '~store/mediateka/selectors';

import MediaNavParent from './MediaNavParent';
import MediaNavChild from './MediaNavChild';

import Preloader from '~components/Preloader/Preloader';

import { deleteSearchUrl, queryFilter } from '../../../utilsMediaBody';

const MediaNavCategory = ({
  category = [],
  category_id = '',
  activeParent = f => f,
  search = '',
  path = '',
  oddEvent = f => f,
  loading = false,
  subCategory = '',
}) => {
  const { parent: activeCategoryParent } = useSelector(getMediatekaBreadcrumbs);

  const dispatchRedux = useDispatch();
  const [isShowParent, setIsShowParent] = useState(subCategory.length > 0);

  useEffect(() => {
    setIsShowParent(true);
  }, [category_id]);

  const handleClickParent = categoryInfo => () => {
    dispatchRedux(setMediatekaBreadcrumbs({ parent: categoryInfo }));
    setIsShowParent(!isShowParent);
  };

  const handleClickChild = categoryInfo => () => {
    dispatchRedux(setMediatekaBreadcrumbs(categoryInfo));
  };

  if (loading) {
    return (
      <div className="media-preloader-nav">
        <Preloader />
      </div>
    );
  }

  return category.map(({ id, title, template_code, children, code }) => {
    const isChild = children.length > 0 && activeCategoryParent?.code === code;

    const parentInfo = { title, code };

    return (
      <React.Fragment key={id}>
        <MediaNavParent
          title={title}
          id={id}
          code={code}
          customStyle={activeCategoryParent?.code === code ? activeParent(children) : ''}
          search={deleteSearchUrl({ search, name: queryFilter.cat })}
          path={path}
          templateCode={template_code}
          child={children}
          isChild={isChild}
          isShowParent={isShowParent}
          onClick={handleClickParent(parentInfo)}
        />
        {isShowParent && (
          <MediaNavChild
            path={path}
            parentCode={code}
            templateCode={template_code}
            search={search}
            child={children}
            oddEvent={oddEvent}
            isChild={isChild}
            parentInfo={parentInfo}
            onClick={handleClickChild}
          />
        )}
      </React.Fragment>
    );
  });
};

export default memo(MediaNavCategory);
