import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import cx from 'classnames';

const MediaNavParent = ({
  path = '',
  isChild = false,
  child = [],
  activeParent = '',
  title = '',
  templateCode = '',
  code = '',
  onClick = f => f,
  isShowParent = false,
  className = '',
  search = '',
  customStyle = '',
}) => {
  const childShow = isChild && isShowParent ? 'open_child' : 'close_child';
  return (
    <div className="list__item media-library-link">
      <NavLink
        to={{
          pathname: `${path}${code}/`,
          search,
          state: templateCode,
        }}
        className={cx('nav-side__link list__item-link', className, customStyle, {
          [childShow]: child.length,
        })}
        activeClassName={activeParent}
        onClick={onClick}>
        {title}
      </NavLink>
    </div>
  );
};

MediaNavParent.propTypes = {
  path: PropTypes.string,
  search: PropTypes.string,
  onClick: PropTypes.func,
  title: PropTypes.string,
  templateCode: PropTypes.string,
  child: PropTypes.array,
  activeParent: PropTypes.func,
  isChild: PropTypes.bool,
  code: PropTypes.string,
  isShowParent: PropTypes.bool,
  className: PropTypes.string,
};

export default memo(MediaNavParent);
