export function getDefaultMetaMediateka({ metaMediateka, setTitleMeta, fetchMetaData }) {
  const { h_one, description } = metaMediateka.meta;
  const isMetaMediateka = JSON.stringify(metaMediateka.meta).includes('mediateka');
  if (isMetaMediateka) {
    setTitleMeta(h_one);
    return description;
  }
  fetchMetaData();
}
