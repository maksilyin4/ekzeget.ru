import React, { useEffect, useContext, memo } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import './mediaNav.scss';

import { MediaContext } from '~context/MediaContext';

import MediaNavCategory from './MediaNavCategory';
import MediaNavHead from './MediaNavHead';
import MediaNavMobile from './MediaNavMobile';

import useCategoriesQuery from '~apollo/query/mediaLibrary/categories';

import { getMediatekaBreadcrumbs } from '~store/mediateka/selectors';
import { setMediatekaBreadcrumbs } from '~store/mediateka/action';
import { mediateka, prefixForDetailCode } from '~utils/common';

const mediatekaLink = `/${mediateka}/`;

const MediaGroupNav = ({ history, location: { search }, match: { params } }) => {
  const { category_id: catId, routing = '' } = params;

  const { tabletMedium, desktop, changeIs404, setMeta } = useContext(MediaContext);
  const dispatchRedux = useDispatch();
  const { parent: activeCategoryParent, children: activeCategoryChild } = useSelector(
    getMediatekaBreadcrumbs,
  );

  const subCategory = !routing.includes(prefixForDetailCode) && routing;

  const { categories, loading } = useCategoriesQuery();

  const category_id = catId?.includes('detail-') ? activeCategoryParent?.code : catId;

  useEffect(() => {
    try {
      if (category_id || (subCategory && categories.length)) {
        const parent = categories.find(({ code }) => code === category_id);
        const children = parent?.children.find(({ code }) => code === subCategory);

        if (parent && !catId?.includes('detail-')) {
          setMeta(parent.meta);
        }

        if (children && !catId?.includes('detail-')) {
          setMeta(children.meta);
        }

        if ((parent || children) && !catId?.includes('detail-')) {
          dispatchRedux(setMediatekaBreadcrumbs({ parent, children, detailTitle: '' }));
        }
      } else {
        dispatchRedux(setMediatekaBreadcrumbs({}));
      }
    } catch (error) {
      console.log('error', error);
    }
  }, [subCategory, category_id, categories, catId]);

  useEffect(() => {
    if (category_id && !category_id.includes('detail-') && categories.length) {
      const currentCategory = categories.find(({ code }) => code === category_id);
      if (!currentCategory) {
        changeIs404(true);
      }
    }
  }, [categories, category_id]);

  const oddEvent = code => () => {
    return activeCategoryChild?.code === code;
  };

  const activeParent = children => {
    return children.length && activeCategoryChild ? 'active__parent' : 'active';
  };

  return (
    <div className="media-library__wrapper">
      <div className="nav-side nav-side__top media-library-left-nav">
        {(desktop || tabletMedium) && !loading ? (
          <>
            <MediaNavHead search={search} category_id={category_id} path={mediatekaLink} />
            <MediaNavCategory
              category={categories}
              category_id={category_id}
              activeParent={activeParent}
              search={search}
              path={mediatekaLink}
              oddEvent={oddEvent}
              loading={loading}
              subCategory={subCategory}
            />
          </>
        ) : (
          <MediaNavMobile
            category={categories}
            loading={loading}
            path={mediatekaLink}
            search={search}
            history={history}
            category_id={category_id}
            subCategory={subCategory}
          />
        )}
      </div>
    </div>
  );
};

MediaGroupNav.propTypes = {
  match: PropTypes.shape({
    path: PropTypes.string.isRequired,
  }),
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
    search: PropTypes.string,
  }),
};

export default memo(withRouter(MediaGroupNav));
