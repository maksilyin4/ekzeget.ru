import React, { memo, useMemo, useState, useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { setMediatekaBreadcrumbs } from '~store/mediateka/action';
import { MediaMenuMobile } from '~components/Mediateka/MediaMenuMobile/';

import { deleteQueryUrl, queryFilter } from '../../../utilsMediaBody';
import {
  updateValue,
  updateValueChild,
  getOptions,
  changeQueryAndValue,
  defaultValueMenu,
} from './utilsMediaNavMobile';

const MediaNavMobile = ({
  category = [],
  loading = false,
  path = '',
  search = '',
  history = '',
  category_id = '',
  subCategory = '',
}) => {
  const dispatchRedux = useDispatch();

  const [state, setState] = useState({
    value: [defaultValueMenu],
    optionsChildren: [],
  });

  const [valueChild, setValueChild] = useState([]);

  const options = useMemo(() => getOptions(category), [category, search]);

  useEffect(() => {
    changeQueryAndValue({
      state,
      category_id,
      options,
      search,
      history,
      queryFilter,
      setState,
      setValueChild,
      deleteQueryUrl,
    });
  }, [search, options, category_id]);

  useEffect(() => {
    const { optionsChildren } = state;

    const value = subCategory ? optionsChildren.filter(({ value }) => value === subCategory) : [];
    setValueChild(value);
  }, [search, subCategory, state.optionsChildren]);

  const handleChange = useCallback(
    select => {
      dispatchRedux(
        setMediatekaBreadcrumbs({ parent: { title: select.label, code: select.value } }),
      );

      updateValue({ select, path, search, setState, state, history });
    },
    [state.value],
  );

  const handleChangeChildren = useCallback(
    valueChild => {
      dispatchRedux(
        setMediatekaBreadcrumbs({
          parent: valueChild.parent,
          children: { title: valueChild.label, code: valueChild.value },
        }),
      );
      updateValueChild({
        valueChild,
        path,
        history,
        search,
        category_id,
      });
    },
    [valueChild, category_id, search],
  );

  if (loading && options.length <= 1) return null;
  const { value, optionsChildren } = state;

  return (
    <MediaMenuMobile
      options={options}
      optionsChildren={optionsChildren}
      value={value}
      valueChild={valueChild}
      handleChange={handleChange}
      handleChangeChildren={handleChangeChildren}
      isClearable
    />
  );
};

export default memo(MediaNavMobile);
