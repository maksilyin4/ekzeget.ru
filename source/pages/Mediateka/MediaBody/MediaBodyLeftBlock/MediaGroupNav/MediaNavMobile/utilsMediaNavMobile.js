export const defaultValueMenu = { label: 'Все разделы', value: '' };
export const updateValue = ({ select, path, search, setState, state, history }) => {
  const { children, value, label, template_code } = select;

  const catId = !value ? '' : `${value}/`;

  history.push(`${path}${catId}${search}`, template_code);

  const optionsChildren = children.map(({ title, template_code, code }) => ({
    value: code,
    label: title,
    template_code,
    parent: { title: label, code: value },
  }));
  setState({ ...state, value: [select], optionsChildren });
};

export const updateValueChild = ({ valueChild, path, history, search, category_id }) => {
  let templateCode = '';
  if (valueChild) {
    const { value, template_code } = valueChild;

    templateCode = template_code;
    history.push(`${path}${category_id}/${value ? `${value}/` : ''}${search}`, templateCode);
  } else {
    history.push(`${path}${category_id}/${search}`, templateCode);
  }
};

export const getOptions = category => {
  const options = [{ label: 'Все разделы', value: '', children: [], template_code: 'M' }];
  category.forEach(({ title, children, template_code, code }) =>
    options.push({ label: title, value: code, children, template_code }),
  );
  return options;
};

export const changeQueryAndValue = ({
  state,
  category_id,
  options,
  search,
  history,
  queryFilter,
  setState,
  setValueChild,
  deleteQueryUrl,
}) => {
  const { value: stateValue } = state;
  const { value } = stateValue[0];
  const catId = category_id === '' ? '' : category_id;

  if (value !== catId) {
    const valueOptions = options.filter(({ value }) => value === catId);

    if (valueOptions.length) {
      const { children, label: titleParent, value: codeParent } = valueOptions[0];
      const optionsChildren = children.map(({ title, template_code, code }) => ({
        value: code,
        label: title,
        template_code,
        search,
        parent: { title: titleParent, code: codeParent },
      }));

      setState({ ...state, value: valueOptions, optionsChildren });
      if (optionsChildren.length === 0) {
        deleteQueryUrl({ search, history, name: queryFilter.cat });
        setValueChild([]);
      }
    }
  }

  if (!catId) {
    setState({ ...state, value: [defaultValueMenu], optionsChildren: [] });
  }
};
