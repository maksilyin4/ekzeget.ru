import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

import { deleteSearchUrl, queryFilter } from '../../../utilsMediaBody';

const MediaNavHead = ({ category_id, path, search }) => {
  return (
    <div className="list__item">
      <NavLink
        to={{
          pathname: path,
          search: deleteSearchUrl({ search, name: queryFilter.cat }),
          state: 'M',
        }}
        className="nav-side__link list__item-link"
        isActive={() => !category_id || category_id.includes('detail-')}>
        Все разделы
      </NavLink>
    </div>
  );
};

MediaNavHead.propTypes = {
  path: PropTypes.string,
  search: PropTypes.string,
  category_id: PropTypes.number,
};

export default memo(MediaNavHead);
