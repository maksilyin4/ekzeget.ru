import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { MediaRouting } from './MediaRouting';
import NotFound from '../../NotFound/NotFound';

export const MediaBody = () => {
  const { path } = useRouteMatch();

  return (
    <Switch>
      <Route
        exact
        path={[
          `${path}`,
          `${path}:category_id/`,
          `${path}:category_id/:routing/`,
          `${path}:category_id/:routing/:detail_id/`,
          `${path}:category_id/:routing/:detail_id/:active/`,
        ]}
        component={MediaRouting}
      />
      <Route path="*" component={NotFound} />
    </Switch>
  );
};
