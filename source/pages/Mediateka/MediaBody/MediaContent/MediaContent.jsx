import React, { useMemo, memo, useCallback, useEffect, useContext } from 'react';
import { useLocation, useRouteMatch } from 'react-router-dom';

import './mediaContentStyles.scss';

import useMediaQuery from '~apollo/query/mediaLibrary/media';
import useTemplateQuery from '~apollo/query/mediaLibrary/templateCategories';

import { MediaBodyContext, MediaContext } from '~context/MediaContext';

import MediaHeader from '~components/Mediateka/MediaHeader/';
import MediaContentList from './MediaContentList';
import MediaFilters from '~components/Mediateka/MediaFilters';

import { setVariables } from './utils';
import { defineTemplate } from '../utilsMediaBody';
import { fetchInfiniteScroll } from '~utils/common';

const MediaContentCommon = () => {
  const { changeIs404 } = useContext(MediaContext);
  const { search, state } = useLocation();
  const {
    params: { category_id, routing },
  } = useRouteMatch();

  const variables = useMemo(() => setVariables({ search, categoryId: category_id, routing }), [
    search,
    category_id,
    routing,
  ]);

  const { medias, loading, fetchMore, isLoading, total, error } = useMediaQuery(variables);

  const { templateData } = useTemplateQuery({
    variables: { code: category_id },
  });

  const templateCode = useMemo(
    () =>
      defineTemplate({
        state,
        template: templateData,
        category_id,
      }),
    [category_id, templateData, state],
  );

  useEffect(() => {
    changeIs404(error);
  }, [error]);

  // scroll loading media
  const onScrollContent = useCallback(
    () => medias.length !== total && fetchInfiniteScroll({ data: medias, fetchMore, variables }),
    [medias],
  );

  if (error) {
    return null;
  }

  return (
    <div className="media-content-common">
      <MediaHeader isShowTags />
      <MediaFilters />
      <MediaBodyContext.Provider
        value={{
          template: templateCode,
          medias,
          onScrollMethod: onScrollContent,
          isLoadingMediaContent: loading,
          isLoadingMedia: isLoading,
        }}>
        <MediaContentList main={medias} />
      </MediaBodyContext.Provider>
    </div>
  );
};

export default memo(MediaContentCommon);
