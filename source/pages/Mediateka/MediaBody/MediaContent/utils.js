import {
  queryFilter,
  getQuery,
  getQueryUrl,
  getUrlParams,
  getArrSplitOrToNumber,
  limit,
} from '../utilsMediaBody';
import { prefixForDetailCode } from '~utils/common';

export const setVariables = ({ search = '', categoryId = '', isSearch = false, routing = '' }) => {
  const { queryTags, queryBook, queryChapter, queryVerse, querySort, queryAuthor } = getUrlParams(
    search,
    queryFilter,
  );

  const querySearchMedia = getQueryUrl({ search, tag: queryFilter.search });
  const queryTranslate = getQueryUrl({ search, tag: queryFilter.translate });
  const queryPreacher = +getQueryUrl({ search, tag: queryFilter.preacher });
  const queryEkzeget = +getQueryUrl({ search, tag: queryFilter.ekzeget });

  const tag_ids = getQuery(queryTags);
  const categorySub = !routing.includes(prefixForDetailCode) && routing;
  const category_code = categorySub || categoryId;
  const connected_to_book_id = getQuery(queryBook);
  const connected_to_chapter_id = getQuery(queryChapter);
  const verse_id = getQuery(queryVerse);
  const sorts = getQuery(querySort);
  const author_id = getQuery(queryAuthor);
  const preacher_id = getQuery(queryPreacher);
  const ekzeget_id = getQuery(queryEkzeget);
  const translate = getQuery(queryTranslate);
  const searchMedia = getQuery(querySearchMedia);

  const [orderBy, order] = getArrSplitOrToNumber(sorts) || [];

  const orderByQuery = getQuery(orderBy);
  const orderQuery = getQuery(+order);

  const searchObj = isSearch ? { code: [translate] } : {};

  const setAuthor = () => {
    if (category_code !== 'propovedi') {
      return { author_id };
    }
    if (category_code !== 'interpretations') {
      return { ekzeget_id };
    }
    return { preacher_id };
  };

  const searchCommon =
    category_code !== 'dictionaries'
      ? {
          category_code,
          tag_ids: getArrSplitOrToNumber(tag_ids, true),
          connected_to_book_id,
          connected_to_chapter_id,
          connected_to_verse_id: getArrSplitOrToNumber(verse_id, true),
          ...setAuthor(),
          ...searchObj,
        }
      : {};

  return {
    offset: 0,
    limit,
    orderBy: orderByQuery,
    order: orderQuery,
    search_query: searchMedia,
    ...searchCommon,
  };
};
