import React, { useMemo } from 'react';
import { useLocation, useHistory } from 'react-router-dom';

import MediaBtn from '~components/Mediateka/BtnMedia';

import './mediaNotContent.scss';

const MediaNotContent = () => {
  const { search, pathname } = useLocation();
  const history = useHistory();

  const resetFilter = () => {
    if (search) {
      history.push('?');
    }
  };

  const isResetBtn = useMemo(() => {
    const searchArr = search.split('&');
    const everyQueryReset = searchArr.some(
      el => el.includes('book') || el.includes('chapter') || el.includes('chapter'),
    );

    let isSearchReset = searchArr.some(el => el.includes('author'));

    if (pathname.includes('/search/bible')) {
      isSearchReset = searchArr.some(el => el.includes('translate'));
    }
    if (pathname.includes('/search/interpretations')) {
      isSearchReset = searchArr.some(el => el.includes('ekzeget'));
    }
    if (pathname.includes('/search/propovedi')) {
      isSearchReset = searchArr.some(el => el.includes('preacher'));
    }
    if (pathname.includes('search/dictionaries')) {
      isSearchReset = false;
    }

    return searchArr.length > 1 && (everyQueryReset || isSearchReset);
  }, [search, pathname]);

  return (
    <div className="media_not_content">
      <p>Материалы не найдены</p>
      {isResetBtn && (
        <MediaBtn styleName="btn_reset" onClick={resetFilter} text="Сбросить все фильтры" />
      )}
    </div>
  );
};

export default MediaNotContent;
