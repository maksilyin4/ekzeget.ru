export const types = {
  TYPE_IMAGE: 'I',
  TYPE_VIDEO: 'V',
  TYPE_AUDIO: 'A',
  TYPE_BOOK: 'B',
};

export const getType = type => {
  switch (type) {
    case 'I':
      return 'image';
    case 'V':
      return 'video';
    case 'A':
      return 'audio';
    case 'B':
      return 'book';
    default:
      return 'book';
  }
};

export function getSmallText(value, count = 184) {
  return value.length <= 180 ? value : `${value.slice(0, count)}...`;
}

export function getMotivators({ commonMain, defaultTemplate }) {
  const motivators = [];
  const uniqueMediaIds = [];
  commonMain.forEach(({ id, template, ...items }) => {
    if (template === defaultTemplate.motivator && !uniqueMediaIds.includes(id)) {
      uniqueMediaIds.push(id);
      motivators.push({ id, template, ...items });
    }
  });
  return motivators;
}

export const getTypesForImgVideoImage = (book, types, defaultTemplate) => {
  return book.type === types.TYPE_VIDEO || book.template === defaultTemplate.img;
};

export const getTypeThisMedia = (type, book) => {
  let typeHave = '';
  const { TYPE_VIDEO, TYPE_AUDIO, TYPE_BOOK } = types;
  if (
    (TYPE_AUDIO === type && book.audio_href) ||
    (TYPE_VIDEO === type && book.video_href) ||
    (TYPE_BOOK === type && (book.epub || book.fb2 || book.pdf))
  ) {
    typeHave = type;
  }
  return typeHave;
};

export const setCommonMain = ({ searchMotivator, main, motivator }) => {
  const isCheckMotivator = searchMotivator && main.some(({ id }) => +id !== +searchMotivator);
  const motivatorContent = motivator.id ? [motivator] : [];

  const commonMain = isCheckMotivator ? [...main, ...motivatorContent] : main;

  return [commonMain, isCheckMotivator];
};
