const initialState = {
  isOpenPortal: false,
  currentMediaId: '',
};

const types = {
  SET_IS_OPEN_PORTAL: 'SET_IS_OPEN_PORTAL',
  SET_CHANGE_ALL: 'SET_CHANGE_ALL',
};

const reducer = (state, action) => {
  switch (action.type) {
    case types.SET_IS_OPEN_PORTAL:
      return { ...state, isOpenPortal: action.payload };
    case types.SET_CHANGE_ALL:
      return { ...state, ...action.payload };

    default:
      return state;
  }
};

export { initialState, reducer, types };
