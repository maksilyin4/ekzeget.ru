import React from 'react';

import MediaMotivatorTemplate from './MediaMotivatorTemplate/';

import './mediaMotivatorTempale.scss';

const MediaMotivator = ({ motivators = [], handleOpenPopup = f => f }) => {
  return (
    <div className="materials__list media-motivator">
      {motivators.map(book => (
        <MediaMotivatorTemplate key={book.id} book={book} openPopup={handleOpenPopup} />
      ))}
    </div>
  );
};

export default MediaMotivator;
