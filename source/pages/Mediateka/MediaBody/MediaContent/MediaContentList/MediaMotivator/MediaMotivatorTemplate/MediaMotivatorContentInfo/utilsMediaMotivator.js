import { getQueryUrl, queryFilter } from '../../../../../utilsMediaBody';

const parent = { type: 'mediaCat_motivatory', title: 'Мотиваторы' };
const child = { type: 'mediaCatChild_all', title: 'Все произведения' };

const getText = (chapter, verse) => {
  let chapterText = '';
  let verseText = '';
  if (chapter) {
    chapterText = `Гл. ${chapter}`;
  }
  if (verse) {
    verseText = `Ст. ${verse}`;
  }

  return `${chapterText} ${verseText}`;
};

const getChapterOrVerse = (number, text = 'glava-') => (number ? `${text}${number}` : '');

const setInfoMediaBible = (book, chapter, verse) => {
  const chapterNumber = chapter?.number;
  const verseNumber = verse?.number;
  const bookTitle = book?.title;

  const url = `/bible/${book?.code}/${getChapterOrVerse(chapterNumber)}/${getChapterOrVerse(
    verseNumber,
    'stih-',
  )}/`;

  return [bookTitle, chapterNumber, verseNumber, url];
};

const setVerseQuery = search => {
  const query = getQueryUrl({ search, tag: queryFilter.verse }) || '';
  return query
    .split(',')
    .filter(el => el)
    .map(el => +el);
};

export { getText, parent, child, setInfoMediaBible, setVerseQuery };
