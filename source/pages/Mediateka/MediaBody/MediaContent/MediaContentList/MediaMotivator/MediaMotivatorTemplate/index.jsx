import React from 'react';

import MediaMotivatorContentImg from '~components/Mediateka/MediaMotivatorContentImg';
import MediaMotivatorContentInfo from './MediaMotivatorContentInfo';

const MediaMotivatorTemplate = ({ book, openPopup = f => f }) => {
  return (
    <div className="media-motivator__item">
      <MediaMotivatorContentImg book={book} openPopup={openPopup(book?.id)} isShowIcons />
      <MediaMotivatorContentInfo book={book} />
    </div>
  );
};

export default MediaMotivatorTemplate;
