import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { Link, useLocation } from 'react-router-dom';
import { useCookies } from 'react-cookie';
import { useSelector } from 'react-redux';

import { getIsMobile } from '~dist/selectors/screen';

import MediaContentInfoItem from '~components/Mediateka/MediaContentInfoItem';

import { getText, parent, child, setInfoMediaBible, setVerseQuery } from './utilsMediaMotivator';

import './mediaContentInfo.scss';

const MediaContentInfo = ({ book }) => {
  const { media_bible = [], title = '' } = book;

  const { search } = useLocation();
  const verseQuery = setVerseQuery(search);

  const isMobile = useSelector(getIsMobile);

  const [cookies, setCookies] = useCookies(['bibleNavParent', 'bibleNavChild']);
  const { bibleNavParent, bibleNavChild } = cookies;

  const mediaBible = useMemo(() => {
    if (verseQuery.length) {
      return media_bible.filter(({ verse_id }) => verseQuery.includes(verse_id));
    }
    return media_bible;
  }, [media_bible, verseQuery]);

  const handleClick = () => {
    if (parent?.type !== bibleNavParent?.type) {
      setCookies('bibleNavParent', parent, { path: '/' });
    }
    if (child?.type !== bibleNavChild?.type) {
      setCookies('bibleNavChild', child, { path: '/' });
    }
  };

  return (
    <div className="media-motivator-content-info">
      {mediaBible.length ? (
        mediaBible.map(({ id, book, verse, chapter }) => {
          const [bookText, chapterNumber, verseNumber, url] = setInfoMediaBible(
            book,
            chapter,
            verse,
            isMobile,
          );

          return (
            <Link key={id} to={url} className="media-motivator-item" onClick={handleClick}>
              <MediaContentInfoItem
                styleName="media-content-info-item"
                text={`${bookText}. ${getText(chapterNumber, verseNumber)}.`}
                isOtherTemplate
              />

              {/* TODO если будет не нужно удалить 20.03.20 */}
              {/* {!isMobile && (
                <MediaContentInfoItem
                  styleName="media-content-info-item"
                  text={getText(chapterNumber, verseNumber)}
                  isOtherTemplate
                />
              )} */}
            </Link>
          );
        })
      ) : (
        <MediaContentInfoItem
          styleName="media-content-info-item media-motivator-title"
          text={title}
          isOtherTemplate
        />
      )}
    </div>
  );
};

MediaContentInfo.propTypes = {
  book: PropTypes.share({
    description: PropTypes.string,
    type: PropTypes.string,
    year: PropTypes.string,
    genre: PropTypes.share({
      title: PropTypes.string,
    }),
  }),
};

export default MediaContentInfo;
