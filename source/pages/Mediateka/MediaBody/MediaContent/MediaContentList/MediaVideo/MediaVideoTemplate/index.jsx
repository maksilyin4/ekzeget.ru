import React from 'react';
import './mediaVideoTemplate.scss';

import MediaContentImg from '~components/Mediateka/MediaContentImg/';
import MediaVideoContentInfo from './MediaVideoContentInfo';

import { urlToAPI } from '~dist/browserUtils';
import { mediateka } from '~utils/common';

const MediaVideoTemplate = ({ book, search }) => {
  const { video_href, image = {}, code } = book;
  return (
    <div className="media-video__item">
      <MediaContentImg
        book={book}
        url={urlToAPI}
        imageUrl={image?.url}
        optimizedImg={image?.optimized ?? []}
        videoHref={video_href}
        urlLink={`/${mediateka}/detail-${code}/${search}`}
        styleName={'media-video-link-img'}
      />
      <MediaVideoContentInfo book={book} />
    </div>
  );
};

export default MediaVideoTemplate;
