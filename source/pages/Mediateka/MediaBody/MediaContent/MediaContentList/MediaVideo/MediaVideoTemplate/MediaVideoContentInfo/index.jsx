import React from 'react';
import PropTypes from 'prop-types';

import MediaContentInfoItem from '~components/Mediateka/MediaContentInfoItem';

import './mediaContentInfo.scss';

const MediaContentInfo = ({ book }) => {
  const { author, title } = book;

  return (
    <div className="media-content-info">
      <MediaContentInfoItem styleName="media-content-title" text={title} />
      <MediaContentInfoItem styleName="media-content-author" text={author?.title} />
    </div>
  );
};

MediaContentInfo.propTypes = {
  book: PropTypes.share({
    description: PropTypes.string,
    type: PropTypes.string,
    year: PropTypes.string,
    genre: PropTypes.share({
      title: PropTypes.string,
    }),
  }),
};

export default MediaContentInfo;
