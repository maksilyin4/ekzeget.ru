import React from 'react';
import { useLocation } from 'react-router-dom';

import MediaVideoTemplate from './MediaVideoTemplate/';

import './mediaVideoTempale.scss';

const MediaVideo = ({ content = [] }) => {
  const { search } = useLocation();
  if (content.length === 0) {
    return null;
  }

  return (
    <div className="materials__list media-video">
      {content.map(book => (
        <MediaVideoTemplate key={book.id} book={book} search={search} />
      ))}
    </div>
  );
};

export default MediaVideo;
