import React from 'react';
import PropTypes from 'prop-types';

import MediaTemplate from './MediaTemplate';
import { mediaContentTypes } from '../../../../../../types/media';

import './mediaMainTemplate.scss';

const MediaMain = ({ content = [], openPopup = f => f }) => (
  <div className="materials__list media-main">
    {content.map(book => (
      <MediaTemplate
        key={book.id}
        viewsStatisticCount={book.statistics_views_count}
        book={book}
        imageUrl={book.image && book.image.url}
        optimizedImg={book?.image?.optimized ?? []}
        type={book.type}
        related={book.related_media_types}
        openPopup={openPopup}
      />
    ))}
  </div>
);

MediaMain.propTypes = {
  content: PropTypes.arrayOf(PropTypes.shape(mediaContentTypes)),
  openPopup: PropTypes.func,
};

export default MediaMain;
