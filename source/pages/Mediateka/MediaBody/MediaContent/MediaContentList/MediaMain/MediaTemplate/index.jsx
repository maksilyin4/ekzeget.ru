import React, { useMemo, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { useLocation, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import cx from 'classnames';

import { getMediatekaStatisticMediaId } from '~store/mediateka/selectors';
import { getMediaViewsSuccess } from '~store/mediateka/action';

import MediaContentHead from './MediaContentHead/';
import MediaContentInfoRight from './MediaContentInfoRight';
import MediaContentInfo from './MediaContentInfo';
import MediaContentImg from '~components/Mediateka/MediaContentImg/';
import MediaMotivatorContentImg from '~components/Mediateka/MediaMotivatorContentImg/';

import { types, getTypesForImgVideoImage, getTypeThisMedia } from '../../utilsMediaContentList';
import { defaultTemplate, relatedObj } from '../../../../utilsMediaBody';
import { getRelatedTypes } from '../../../../MediaDetailContent/utilsMediaDetail';
import { mediateka } from '~utils/common';

import './mediaTemplate.scss';

const MediaTemplate = ({
  book,
  type,
  imageUrl,
  optimizedImg = [],
  openPopup,
  related,
  viewsStatisticCount,
}) => {
  const { search } = useLocation();
  const history = useHistory();

  const imgRef = useRef(null);

  const dispatch = useDispatch();
  const { viewCount } = useSelector(getMediatekaStatisticMediaId(book.id));

  useEffect(() => {
    const view_count = viewsStatisticCount < viewCount ? viewCount : viewsStatisticCount;
    dispatch(getMediaViewsSuccess({ id: book.id, data: { view_count } }));
  }, [viewCount, viewsStatisticCount]);

  const urlLink = `/${mediateka}/detail-${book.code}/${search}`;
  const urlLinkHead = `/${mediateka}/detail-${book.code}/`;

  const isTemplate =
    book.template === defaultTemplate.main || book.template === defaultTemplate.img;

  const templateImg = useMemo(() => {
    switch (book.template) {
      case defaultTemplate.motivator:
        return (
          <MediaMotivatorContentImg
            className="book-info_img_link book-info-motivator"
            book={book}
            imageUrl={imageUrl}
            openPopup={openPopup(book?.id)}
          />
        );
      case defaultTemplate.video:
        return (
          <MediaContentImg
            type={book.type}
            title={book.title}
            imageUrl={imageUrl}
            optimizedImg={optimizedImg}
            videoHref={book.video_href}
            urlLink={urlLink}
            styleName="book-info_img_link book-info-video"
          />
        );
      default:
        return (
          <div className="material__book-info book-info">
            <MediaContentImg
              imgRef={imgRef}
              type={book.type}
              title={book.title}
              imageUrl={imageUrl}
              optimizedImg={optimizedImg}
              urlLink={urlLink}
              styleName={'book-info_img_link'}
              styleNameVideo={cx({
                'book-info__img_video': getTypesForImgVideoImage(book, types, defaultTemplate),
                isBigImg: imgRef.current?.width > 320,
                // 'book-info-video': book.template === defaultTemplate.img,
                'book-info-main': book.template !== defaultTemplate.img,
              })}
            />
            {book.type !== types.TYPE_VIDEO && book.template !== defaultTemplate.img && (
              <MediaContentInfoRight book={book} urlLink={urlLink} />
            )}
          </div>
        );
    }
  }, [imgRef.current?.width]);

  const relatedData = useMemo(
    () => getRelatedTypes([...related, getTypeThisMedia(type, book)], relatedObj),
    [related, type, book],
  );

  return (
    <div className="material__item material">
      {isTemplate && (
        <MediaContentHead
          relatedData={relatedData}
          urlLink={urlLinkHead}
          search={search}
          viewsCount={viewCount}
        />
      )}
      <div className="material__body">
        {templateImg}
        <MediaContentInfo
          book={book}
          urlLink={urlLink}
          openPopup={openPopup}
          search={search}
          history={history}
        />
      </div>
    </div>
  );
};

MediaTemplate.propTypes = {
  type: PropTypes.string,
  imageUrl: PropTypes.string,
  book: PropTypes.object,
  openPopup: PropTypes.func,
  related: PropTypes.array,
  viewsStatisticCount: PropTypes.number,
  optimizedImg: PropTypes.array,
};

export default React.memo(MediaTemplate);
