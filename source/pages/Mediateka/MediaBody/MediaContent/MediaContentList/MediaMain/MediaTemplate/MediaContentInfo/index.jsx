import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import MediaContentInfoRight from '../MediaContentInfoRight';
import MediaBtn from '~components/Mediateka/BtnMedia';
import MediaMotivatorTemplateInfo from '../../../MediaMotivator/MediaMotivatorTemplate/MediaMotivatorContentInfo';
import MediaVideoTemplateInfo from '../../../MediaVideo/MediaVideoTemplate/MediaVideoContentInfo/';

import { types } from '../../../utilsMediaContentList';
import { defaultTemplate } from '../../../../../utilsMediaBody';

import './mediaContentInfo.scss';

const getTypesVideoImage = (type, types, template) =>
  type === types.TYPE_VIDEO || template === defaultTemplate.img;

const MediaContentInfo = ({ book, urlLink, openPopup = f => f, ...props }) => {
  const { type, template, id } = book;

  function getTemplate(template) {
    switch (template) {
      case defaultTemplate.motivator:
        return <MediaMotivatorTemplateInfo book={book} openPopup={openPopup} index={id} />;
      case defaultTemplate.video:
        return <MediaVideoTemplateInfo book={book} />;

      default:
        return (
          <div className="material_book_info_content">
            <div className="material__book-desc book-desc">
              {getTypesVideoImage(type, types, template) && (
                <MediaContentInfoRight book={book} urlLink={urlLink} {...props} />
              )}
            </div>

            <Link to={urlLink} className="media-content-info-btn">
              <MediaBtn text="Полное описание" />
            </Link>
          </div>
        );
    }
  }

  return getTemplate(template);
};

MediaContentInfo.propTypes = {
  screen: PropTypes.share({
    desktop: PropTypes.string,
  }),
  book: PropTypes.share({
    description: PropTypes.string,
    type: PropTypes.string,
    year: PropTypes.string,
    genre: PropTypes.share({
      title: PropTypes.string,
    }),
  }),
  urlLink: PropTypes.string,
};

export default memo(MediaContentInfo);
