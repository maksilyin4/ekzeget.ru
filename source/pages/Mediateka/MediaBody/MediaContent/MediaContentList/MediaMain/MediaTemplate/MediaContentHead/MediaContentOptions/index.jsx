import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Icon from '~components/Icons/Icons';

import { getToForLink } from '../../../../../../utilsMediaBody';

import './mediaContentOptions.scss';

const MediaContentOptions = ({ relatedData = [], urlLink, search }) => {
  return (
    <div className="material__options">
      {relatedData.length === 0 ? (
        <div className="material__btn-option--empty" />
      ) : (
        <>
          {relatedData.map(({ relatedTitle }, i) => (
            <Link
              key={relatedTitle}
              className="material__btn-option media_icon_item"
              to={{
                pathname: getToForLink({
                  i,
                  url: urlLink,
                  related: relatedTitle,
                }),
                search,
              }}>
              <Icon icon={relatedTitle} />
            </Link>
          ))}
        </>
      )}
    </div>
  );
};

MediaContentOptions.propTypes = {
  urlLink: PropTypes.string,
  search: PropTypes.string,
  relatedData: PropTypes.array,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }),
  }),
};

export default React.memo(MediaContentOptions);
