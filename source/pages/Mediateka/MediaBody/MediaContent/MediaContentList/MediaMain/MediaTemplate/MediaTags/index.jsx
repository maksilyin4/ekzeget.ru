import React, { memo, useCallback, useMemo } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import cx from 'classnames';

import { addQueryUrl, queryFilter } from '../../../../../utilsMediaBody';

const MediaContentTags = ({ tags = [] }) => {
  const { search } = useLocation();
  const history = useHistory();

  const searchTags = useMemo(() => new URLSearchParams(search).get(queryFilter.tags), [search]);

  const activeTagIds = useMemo(() => (searchTags ? searchTags.split(',') : []), [searchTags]);

  const toggleTag = tagId =>
    useCallback(() => {
      const index = activeTagIds.indexOf(tagId);
      if (index === -1) {
        activeTagIds.push(tagId);
      } else {
        activeTagIds.splice(index, 1);
      }

      addQueryUrl({ search, history, name: queryFilter.tags, state: activeTagIds });
    }, [activeTagIds]);

  if (tags.length === 0) {
    return null;
  }
  return (
    <ul className="tags__desc book-info__tags">
      {tags.map(tag => (
        <li className="tags__item book-info__tag" key={tag.code}>
          <button
            className={cx('tag__btn book-info__tag-btn', {
              'tag__btn-active': activeTagIds.includes(tag.id),
            })}
            onClick={toggleTag(tag.id)}>
            {tag.title}
          </button>
        </li>
      ))}
    </ul>
  );
};

export default memo(MediaContentTags);
