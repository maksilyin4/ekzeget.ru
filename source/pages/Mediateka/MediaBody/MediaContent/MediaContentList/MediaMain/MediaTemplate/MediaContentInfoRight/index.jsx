import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import MediaTags from '../MediaTags';
import MediaContentInfoItem from '~components/Mediateka/MediaContentInfoItem';
import MediaContentInfoBible from '~components/Mediateka/MediaContentInfoBible';

import { getTitleAuthor, getArtTitle, getEra, getArtCreatedDate } from '../utilsMediaTemplate';

const MediaContentInfoRight = ({
  book: {
    title,
    author,
    location,
    categories,
    art_director,
    art_title,
    art_created_date,
    tags,
    genre,
    year,
    era,
    media_bible,
  },
  urlLink,
}) => (
  <div className="book-info__main">
    <h4 className="book-info__name">
      <Link to={urlLink}>{title}</Link>
    </h4>
    <div>
      <MediaContentInfoItem
        styleName="book-info__author"
        contentTitle={getTitleAuthor(categories)}
        text={author?.title}
      />
      <MediaContentInfoItem
        styleName="book-info__author"
        contentTitle="Настоятель: "
        text={art_director}
      />
      <MediaContentInfoItem contentTitle="Местонахождение: " text={location} />
      <MediaContentInfoItem contentTitle={getArtTitle(categories)} text={art_title} />
      <MediaContentInfoItem contentTitle="Жанр: " text={genre?.title} />
      <MediaContentInfoItem contentTitle="Год создания: " text={year} />
      <MediaContentInfoItem contentTitle={getEra(categories)} text={era} />
      <MediaContentInfoItem contentTitle={getArtCreatedDate(categories)} text={art_created_date} />

      {media_bible.length > 0 && (
        <MediaContentInfoItem
          contentTitle="Эпизод в Библии: "
          text={<MediaContentInfoBible media_bible={media_bible} />}
        />
      )}

      <MediaTags tags={tags} />
    </div>
  </div>
);

MediaContentInfoRight.propTypes = {
  book: PropTypes.share({
    title: PropTypes.string,
    author: PropTypes.string,
    id: PropTypes.string,
  }),
  urlLink: PropTypes.string,
};

export default MediaContentInfoRight;
