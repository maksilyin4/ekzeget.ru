import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { getDeclination } from './utilsContentHead';

import MediaContentOptions from './MediaContentOptions/';

const MediaContentHead = ({ viewsCount, ...props }) => {
  return (
    <div className="material__head">
      <p className="material__count-download">{getDeclination(viewsCount)}</p>
      <MediaContentOptions {...props} />
    </div>
  );
};

MediaContentHead.propTypes = {
  viewsCount: PropTypes.number.isRequired,
};

export default memo(MediaContentHead);
