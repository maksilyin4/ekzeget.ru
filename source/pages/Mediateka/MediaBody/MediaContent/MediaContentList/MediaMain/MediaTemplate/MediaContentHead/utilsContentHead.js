export const getDeclination = count => {
  let end = 'ов';
  if (count === 1) {
    end = '';
  } else if (count > 1 && count <= 4) {
    end = 'а';
  }
  return `${count} просмотр${end}`;
};
