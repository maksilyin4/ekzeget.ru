const arhitektura = 'arhitektura';
const ikonografia = 'ikonografia';
const zivopis = 'zivopis';
const muzika = 'muzyka';
const literatura = 'literatura';

const getCategory = categories => {
  let code;
  if (categories.length) {
    const category = categories[categories.length - 1];
    code = category.code;
  }
  return code;
};

const getTitleAuthor = (categories = []) => {
  let authorTitle = 'Автор: ';
  const code = getCategory(categories);
  if (ikonografia === code) {
    authorTitle = 'Иконописная школа/ автор: ';
  }
  return authorTitle;
};

const getArtTitle = (categories = []) => {
  let artTitle = 'Произведение: ';
  const code = getCategory(categories);
  switch (code) {
    case ikonografia:
      artTitle = 'Название иконы: ';
      break;
    case arhitektura:
      artTitle = 'Название: ';
      break;
    default:
      break;
  }
  return artTitle;
};

const getArtCreatedDate = (categories = []) => {
  let artCreatedDate = 'Дата создания: ';
  const code = getCategory(categories);
  if (code === ikonografia) {
    artCreatedDate = 'Время создания: ';
  }
  return artCreatedDate;
};

const getEra = (categories = []) => {
  let era = 'Эпоха: ';
  const code = getCategory(categories);

  if (code === ikonografia || code === zivopis) {
    era = 'Эпоха/течение: ';
  }
  if (code === muzika) {
    era = 'Стиль:';
  }
  if (code === literatura) {
    era = 'Течение: ';
  }
  return era;
};

export { getTitleAuthor, getArtTitle, getArtCreatedDate, getEra };
