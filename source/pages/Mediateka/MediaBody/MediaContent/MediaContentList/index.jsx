import React, { useCallback, useContext, useMemo, useReducer, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useLocation, useHistory, useRouteMatch } from 'react-router-dom';
import InfiniteScroll from 'react-infinite-scroll-component';

import useMediaDetailMotivatorLazy from '~apollo/query/mediaLibrary/useMediaDetailMotivatorLazy';

import { MediaBodyContext } from '~context/MediaContext';

import MediaMain from './MediaMain';
import MediaVideo from './MediaVideo';
import MediaMotivator from './MediaMotivator/';
import MediaNotContent from '../MediaNotContent';
import Preloader from '~components/Preloader/Preloader';
import MediaMotivatorPopup from '~components/Mediateka/MediaMotivatorPopup';

import { defaultTemplate, queryFilter, deleteSearchQueryPopup } from '../../utilsMediaBody';
import { getMotivators, setCommonMain } from './utilsMediaContentList';

import { reducer, initialState, types } from './reducer';

import './mediaContent.scss';

const MediaContentList = ({ main = [] }) => {
  const { search, pathname } = useLocation();
  const match = useRouteMatch();
  const history = useHistory();

  const { onScrollMethod, template, isLoadingMedia, isLoadingMediaContent } = useContext(
    MediaBodyContext,
  );
  const [state, dispatch] = useReducer(reducer, initialState);

  const [getMotivator, motivator] = useMediaDetailMotivatorLazy();

  const searchMotivator = useMemo(() => new URLSearchParams(search).get(queryFilter.motivator), [
    search,
    pathname,
  ]);

  const [commonMain, isCheckMotivator] = useMemo(
    () => setCommonMain({ searchMotivator, main, motivator }),
    [main, motivator.id, searchMotivator],
  );

  const motivators = useMemo(() => getMotivators({ commonMain, defaultTemplate }), [commonMain]);

  const currentMotivator = useMemo(
    () => motivators.find(({ id }) => +id === +searchMotivator) || {},
    [searchMotivator, motivators],
  );

  useEffect(() => {
    if (searchMotivator && !isCheckMotivator) {
      getMotivator({ variables: { id: +searchMotivator } });
    }
  }, []);

  useEffect(() => {
    if (searchMotivator && currentMotivator.id) {
      dispatch({
        type: types.SET_CHANGE_ALL,
        payload: { isOpenPortal: true, currentMediaId: searchMotivator },
      });
    }
    if (!searchMotivator) {
      dispatch({
        type: types.SET_IS_OPEN_PORTAL,
        payload: false,
      });
    }
  }, [currentMotivator.id, searchMotivator]);

  const closePortal = () => {
    dispatch({
      type: types.SET_IS_OPEN_PORTAL,
      payload: false,
    });

    deleteSearchQueryPopup({
      search,
      history,
      name: queryFilter.motivator,
      stateHistory: template,
    });
  };

  const handleOpenPopup = index => () => {
    dispatch({
      type: types.SET_CHANGE_ALL,
      payload: { isOpenPortal: true, currentMediaId: index },
    });
  };

  const getTemplateMedia = useCallback(() => {
    switch (template) {
      case defaultTemplate.video:
        return <MediaVideo content={main} match={match} search={search} />;
      case defaultTemplate.motivator:
        return (
          motivators.length > 0 && (
            <MediaMotivator motivators={motivators} handleOpenPopup={handleOpenPopup} />
          )
        );
      default:
        return <MediaMain content={main} openPopup={handleOpenPopup} />;
    }
  }, [template, main, search, match, motivators]);

  if (isLoadingMedia && process.env.BROWSER) {
    return (
      <div className="media-content-preloader">
        <Preloader />
      </div>
    );
  }

  if (!isLoadingMedia && main.length === 0) {
    return <MediaNotContent />;
  }

  const { isOpenPortal, currentMediaId } = state;

  return (
    <InfiniteScroll dataLength={main.length} next={onScrollMethod} scrollThreshold="50%" hasMore>
      <div className="media-content">
        {getTemplateMedia()}
        {isLoadingMediaContent && (
          <div className="media-content-preloader">
            <Preloader />
          </div>
        )}
        {isOpenPortal && (
          <MediaMotivatorPopup
            content={motivators}
            currentContent={currentMotivator}
            template={template}
            currentMediaId={currentMediaId}
            closePortal={closePortal}
          />
        )}
      </div>
    </InfiniteScroll>
  );
};

MediaContentList.propTypes = {
  main: PropTypes.array,
};

export default MediaContentList;
