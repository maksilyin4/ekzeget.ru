import React, { useContext, useMemo, useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { MediaContext } from '~context/MediaContext';

import './mediaDetailVideo.scss';

import MediaDetailVideoList from './MediaDetailVideoList';
import MediaDescription from '~components/Mediateka/MediaDescription';
import { MediaDetailVideoReady } from './MediaDetailVideoReady';
import { VideoPlay } from '~components/VideoPlay';
import Preloader from '~components/Preloader/Preloader';
import ShareButton from '~components/ShareButton';
import MediaContentInfoItem from '~components/Mediateka/MediaContentInfoItem';
import MediaContentInfoBible from '~components/Mediateka/MediaContentInfoBible';

const MediaDetailVideo = ({
  categoryId,
  mediaId,
  text,
  description,
  video_href,
  title,
  book = {},
  author = {},
  media_bible = [],
  image,
}) => {
  const { phone } = useContext(MediaContext);
  const [state, setState] = useState({ styleHeight: 0, styleMaxHeight: 0 });

  const [isLoadingVideoList, setIsLoadingVideoList] = useState(false);
  const videoBodyRef = useRef(null);
  const videoReadyRef = useRef(null);

  useEffect(() => {
    if (!book) {
      setIsLoadingVideoList(true);
    }
  }, []);

  useEffect(() => {
    if (!phone) {
      if (book && isLoadingVideoList && videoReadyRef.current) {
        setState({
          ...state,
          styleHeight: videoBodyRef.current.clientHeight,
          styleMaxHeight: videoReadyRef.current?.clientHeight ?? 0,
        });
      }
    } else {
      setState({
        ...state,
        styleHeight: videoBodyRef.current.clientHeight,
      });
    }
  }, [
    videoBodyRef.current?.clientHeight,
    videoReadyRef.current?.clientHeight,
    isLoadingVideoList,
    book,
    phone,
  ]);

  const mediaDetailVideoReadyRender = useMemo(
    () =>
      book && (
        <MediaDetailVideoReady
          setIsLoadingVideoList={setIsLoadingVideoList}
          book={book}
          phone={phone}
          author_id={+author.id}
          videoReadyRef={videoReadyRef}
        />
      ),
    [book, videoReadyRef.current?.clientHeight, phone],
  );

  const { styleHeight, styleMaxHeight } = state;

  const locationHref = (process.env.BROWSER && window.location.href) || '';

  return (
    <div className="media-detail-video">
      <div className="media-detail-video-head">
        <h3 className="media-detail-video__title">{title}</h3>
        <ShareButton className="media-icons-item" urlShare={video_href} text={[locationHref]} />
      </div>
      <div className="media-detail-video-body">
        <div ref={videoBodyRef} className="media-detail-video-body-left">
          {video_href && (
            <VideoPlay
              path={video_href}
              preview={image?.url}
              author={author.title}
              title={title}
              isLiteShow
            />
          )}

          {media_bible.length > 0 && (
            <MediaContentInfoItem
              styleName="media-detail-video-episode"
              contentTitle="Эпизод в Библии: "
              text={<MediaContentInfoBible media_bible={media_bible} />}
            />
          )}
          <MediaDescription
            description={text || description}
            defaultCount={500}
            className="media-detail-video-body-left-desc"
          />

          {!phone && mediaDetailVideoReadyRender}
        </div>

        {isLoadingVideoList || phone ? (
          <MediaDetailVideoList
            isloadingVideoList={isLoadingVideoList && book}
            styleHeight={styleHeight}
            styleMaxHeight={styleMaxHeight}
            categoryId={categoryId}
            mediaId={mediaId}
            bookId={book?.id}
            phone={phone}
            author_id={+author?.id}
          />
        ) : (
          <div className="media-detail-video-content">
            <Preloader />
          </div>
        )}

        {phone && mediaDetailVideoReadyRender}
      </div>
    </div>
  );
};

MediaDetailVideo.propTypes = {
  categoryId: PropTypes.string,
  mediaId: PropTypes.string,
  description: PropTypes.string,
  text: PropTypes.string,
  video_href: PropTypes.string,
  book: PropTypes.objectOf(PropTypes.string),
};
export default MediaDetailVideo;
