import React, { useContext, useMemo, useCallback, useRef, useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import cx from 'classnames';

import './mediaDetailVideoList.scss';

import { MediaBodyContext } from '~context/MediaContext/';

import useMediaVideoListQuery from '~apollo/query/mediaLibrary/videoList';

import MediaDetailVideoItem from './MediaDetailVideoItem';
import MediaDetailVideoNext from './MediaDetailVideoNext/';
import Preloader from '~components/Preloader/Preloader';

import { setStyleVideoContent, setStyleVideoList } from './utilsMediaDetailVideoList';
import { fetchLoadScroll, defaultTemplate } from '../../../../utilsMediaBody';
import { mediateka } from '~utils/common';

const MediaDetailVideoList = ({
  categoryId,
  mediaId,
  bookId,
  author_id,
  styleHeight = 500,
  phone,
}) => {
  const { search } = useLocation();

  const { template, subCategory } = useContext(MediaBodyContext);

  const [heightVideoNext, setHeightVideoNext] = useState(60);
  const refNextVideo = useRef(null);

  const refNextVideoHeight = refNextVideo.current?.clientHeight;

  const limit = useMemo(() => (phone ? 5 : Math.round(styleHeight / 60)), [styleHeight, phone]);

  const variables = useMemo(() => {
    const category_id = categoryId === 'all' ? '' : categoryId;
    const category_code = subCategory || category_id;

    return {
      category_code,
      template: defaultTemplate.video,
      limit,
      offset: 0,
      author_id,
      connected_to_book_id: +bookId,
    };
  }, [categoryId, template, subCategory, bookId, author_id, limit]);

  const { mediaList, loading, fetchMore, isLoading, total } = useMediaVideoListQuery(variables);

  const mediaListData = useMemo(() => {
    return mediaList.filter(({ id }) => +id !== +mediaId);
  }, [mediaList, mediaId]);

  const onScrollContent = useCallback(
    event => {
      if (!loading && mediaList.length < total)
        fetchLoadScroll({ event, data: mediaList, fetchMore, variables });
    },
    [mediaList, loading, fetchMore, variables, total],
  );

  useEffect(() => {
    if (refNextVideo.current) {
      setHeightVideoNext(refNextVideoHeight);
    }
  }, [refNextVideoHeight]);

  if (isLoading) {
    return (
      <div className="media-detail-video-content">
        <Preloader />
      </div>
    );
  }

  if (mediaList?.length <= 1) {
    return <div className="media-detail-video-content" />;
  }

  const isShowVideoList = mediaListData.length > 2;

  return (
    <div className="media-detail-video-content" style={setStyleVideoContent(phone, styleHeight)}>
      {author_id && (
        <MediaDetailVideoNext
          refNextVideo={refNextVideo}
          heightVideoNext={heightVideoNext}
          path={mediateka}
          mediaId={+mediaId}
          categoryId={categoryId}
          search={search}
          variables={variables}
          author_id={author_id}
        />
      )}

      {isShowVideoList && (
        <div
          style={setStyleVideoList(styleHeight, heightVideoNext)}
          className={cx('media-detail-video-list')}
          onScroll={onScrollContent}>
          {mediaListData.map(({ title, author, id, length, code, video_href, image }) => (
            <MediaDetailVideoItem
              key={id}
              to={`/${mediateka}/detail-${code}/${search}`}
              videoHref={video_href}
              title={title}
              imageUrl={image?.url}
              optimizedImg={image?.optimized ?? []}
              author={author?.title}
              duration={length}
            />
          ))}
          {loading && (
            <div className="media-detail-video-content">
              <Preloader />
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default MediaDetailVideoList;
