import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import MediaDetailVideoImg from '~components/Mediateka/MediaContentImg';
// import fancyTimeFormat from '~utils/fancyTimeFormat';

import './mediaDetailVideoItem.scss';

const MediaDetailVideoItem = ({
  videoHref = '',
  imageUrl = '',
  title = '',
  author = '',
  duration = '',
  to = '',
  optimizedImg = [],
}) => {
  return (
    <Link to={to} className="media-detail-video-list-item">
      <MediaDetailVideoImg
        styleName="media-detail-video-list-item-img"
        videoHref={videoHref}
        imageUrl={imageUrl}
        optimizedImg={optimizedImg}
        isLink
      />
      <div>
        <p className="media-detail-video-list-item-title">
          {title}
          {duration
            ? // TODO удалить если будет не нужно 10.02.20
              //  `(${fancyTimeFormat(duration)})`
              ` (${duration})`
            : ''}
        </p>
        <p className="media-detail-video-list-item-author">{author}</p>
      </div>
    </Link>
  );
};

MediaDetailVideoItem.propTypes = {
  videoHref: PropTypes.string,
  imageUrl: PropTypes.string,
  title: PropTypes.string,
  author: PropTypes.string,
  duration: PropTypes.string,
  to: PropTypes.string,
  optimizedImg: PropTypes.array,
};

export default MediaDetailVideoItem;
