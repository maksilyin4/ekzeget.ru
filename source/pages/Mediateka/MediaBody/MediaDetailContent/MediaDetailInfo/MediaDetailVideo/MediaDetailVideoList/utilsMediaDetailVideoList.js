const setStyleVideoContent = (phone = false, styleHeight = 500) => {
  return phone ? { maxHeight: `${styleHeight}px` } : { height: `${styleHeight}px` };
};

const setStyleVideoList = (styleHeight = 500, heightVideoNext = 60) => {
  return { height: `${styleHeight - heightVideoNext}px` };
};

export { setStyleVideoContent, setStyleVideoList };
