import React from 'react';

import MediaDetailVideoItem from '../MediaDetailVideoItem/';
import Preloader from '~components/Preloader/Preloader';

import useNextVideoMediaQuery from '~apollo/query/mediaLibrary/nextVideoMedia';

const MediaDetailVideoNext = ({
  path,
  categoryId,
  search,
  variables,
  author_id,
  mediaId,
  refNextVideo,
  heightVideoNext,
}) => {
  const { mediaNext, loading } = useNextVideoMediaQuery({
    author_id,
    current_item_id: +mediaId,
    ...variables,
  });

  const { id, image, title, author, code, video_href, length } = mediaNext[0] || [];

  return (
    mediaId !== id && (
      <>
        <p>Следующее видео</p>
        <div
          className="media-detail-video-next"
          ref={refNextVideo}
          style={loading ? { height: `${heightVideoNext}px` } : {}}>
          {loading ? (
            <Preloader />
          ) : (
            <MediaDetailVideoItem
              key={id}
              imageUrl={image?.url}
              videoHref={video_href}
              to={`/${path}/${categoryId}detail-${code}/${search}`}
              title={title}
              author={author?.title ?? ''}
              duration={length}
            />
          )}
        </div>
      </>
    )
  );
};

export default MediaDetailVideoNext;
