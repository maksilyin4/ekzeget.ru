import React, { memo, useMemo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import useMediaDetailBookReady from '~apollo/query/mediaLibrary/bookReady';

import Preloader from '~components/Preloader/Preloader';

import { setUrlForImage } from './utilsMediaDetailReady';

import './mediaDetailVideoReady.scss';

export const MediaDetailVideoReady = memo(
  ({ book: { id }, author_id, videoReadyRef, setIsLoadingVideoList, phone }) => {
    const { data, loading } = useMediaDetailBookReady({
      variables: { id: +id, type: 'V', bookId: +id },
    });

    useEffect(() => {
      if (!phone) {
        setIsLoadingVideoList(!loading);
      }
    }, [loading, setIsLoadingVideoList, phone]);

    const dataReady = useMemo(() => data.filter(({ id }) => +id !== author_id), [data, author_id]);

    return (
      <div ref={videoReadyRef} className="media-detail-video-ready">
        {!loading ? (
          <>
            {dataReady.length ? (
              <>
                <div className="media-detail-video-ready-head">
                  <p>Эту книгу также читают</p>
                </div>
                <div className="media-detail-video-ready-content">
                  {dataReady.map(({ id, title, image, book_media = [] }) => {
                    const url = `/mediateka/video/detail-${book_media[0]?.code ?? '/'}`;
                    return (
                      <div className="media-detail-video-ready-item" key={id}>
                        <Link
                          className="media-detail-video-ready-content-img"
                          to={url}
                          alt={title}
                          title={title}>
                          <div
                            className="media-detail-ready-authors"
                            style={{
                              backgroundImage: `url(${setUrlForImage(image)})`,
                            }}
                          />
                        </Link>
                        <Link to={url}>{title}</Link>
                      </div>
                    );
                  })}
                </div>
              </>
            ) : null}
          </>
        ) : (
          <Preloader />
        )}
      </div>
    );
  },
);

MediaDetailVideoReady.propTypes = {
  book: PropTypes.shape({
    id: PropTypes.string,
  }),
};
