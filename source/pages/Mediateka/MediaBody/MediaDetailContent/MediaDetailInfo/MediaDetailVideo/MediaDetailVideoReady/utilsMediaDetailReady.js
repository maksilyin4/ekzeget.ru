import { urlToAPI } from '~dist/browserUtils';

export const setUrlForImage = urlImage =>
  urlImage ? `${urlToAPI}${urlImage}` : `/frontassets/images/mediaImg/media-book.png`;
