export const getWidthForImg = imgWidth => {
  const maxWidthImg = 350;
  const minWidthImg = 50;

  return imgWidth < maxWidthImg && imgWidth > minWidthImg ? `${imgWidth}px` : '';
};
