import React, { memo, useEffect, useCallback, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import cx from 'classnames';

import MediaDetailIcons from './MediaDetailIcons';
import MediaContentImg from '~components/Mediateka/MediaContentImg';
import MediaContentInfoItem from '~components/Mediateka/MediaContentInfoItem';
import MediaDescription from '~components/Mediateka/MediaDescription';
import MediaContentInfoBible from '~components/Mediateka/MediaContentInfoBible';

import screenReducer from '~dist/selectors/screen';

import { Popup } from '~dist/browserUtils';
import { getWidthForImg } from './utilsMediaDetailMain.js';
import {
  getTitleAuthor,
  getArtTitle,
  getArtCreatedDate,
  getEra,
} from '../../../MediaContent/MediaContentList/MediaMain/MediaTemplate/utilsMediaTemplate';
import { defaultTemplate } from '../../../utilsMediaBody';

import './mediaDetailMain.scss';

const MediaDetailMain = ({
  title = '',
  author,
  location = '',
  genre,
  era = '',
  year = '',
  description = '',
  text = '',
  image,
  type,
  fb2,
  epub,
  pdf,
  audio_href,
  media_bible,
  art_title,
  art_created_date,
  art_director,
  categories,
  template,
  mediaId,
}) => {
  const [imgWidth, setImgWidth] = useState({});
  const imgRef = useRef(null);

  const screen = useSelector(screenReducer);

  useEffect(() => {
    if (!screen.phone) {
      const width = getWidthForImg(imgRef.current?.width);
      setImgWidth({ width });
    }
  }, [imgRef.current?.width, screen.phone, title]);

  const handlePopup = useCallback(() => {
    Popup.create({
      content: (
        <MediaContentImg
          styleName="media-detail-img-popup-item"
          imageUrl={image?.url}
          type={type}
          title={title}
          isLink
        />
      ),
      className: 'media-detail-img-popup',
    });
  }, [title, type, image]);

  return (
    <div className="media-detail-main">
      <div className="media-detail-main-header">
        <MediaDetailIcons
          className="media-detail-main-icons"
          audio_href={audio_href}
          epub={epub}
          fb2={fb2}
          pdf={pdf}
          mediaId={mediaId}
        />

        <MediaContentImg
          styleName={cx('media-detail-main-header__img', {
            'media-detail-img-book': template !== defaultTemplate.img,
          })}
          style={imgWidth}
          imageUrl={image?.url}
          type={type}
          title={title}
          onClick={handlePopup}
          imgRef={imgRef}
          isLink
        />

        <div className="media-detail-main-header__info">
          <MediaContentInfoItem styleName="media-detail-title" text={title} />
          <MediaContentInfoItem
            styleName="media-detail-author"
            contentTitle={getTitleAuthor(categories)}
            text={author?.title}
          />
          <MediaContentInfoItem
            styleName="media-detail-author"
            contentTitle="Настоятель: "
            text={art_director}
          />

          <MediaContentInfoItem contentTitle="Местонахождение: " text={location} />
          <MediaContentInfoItem contentTitle={getArtTitle(categories)} text={art_title} />
          <MediaContentInfoItem contentTitle="Жанр: " text={genre?.title} />
          <MediaContentInfoItem contentTitle="Год создания: " text={year} />
          <MediaContentInfoItem contentTitle={getEra(categories)} text={era} />
          <MediaContentInfoItem
            contentTitle={getArtCreatedDate(categories)}
            text={art_created_date}
          />

          {media_bible.length > 0 && (
            <MediaContentInfoItem
              contentTitle="Эпизод в Библии: "
              text={<MediaContentInfoBible media_bible={media_bible} />}
            />
          )}
        </div>
      </div>

      <div className="media-detail-main-desc">
        <MediaDescription description={text || description} screen={screen} defaultShowBtn={400} />
      </div>
    </div>
  );
};

MediaDetailMain.propTypes = {
  title: PropTypes.string,
  author: PropTypes.share({
    title: PropTypes.string,
  }),
  location: PropTypes.string,
  genre: PropTypes.share({
    title: PropTypes.string,
  }),
  era: PropTypes.string,
  year: PropTypes.string,
  description: PropTypes.string,
  text: PropTypes.string,
  image: PropTypes.share({
    url: PropTypes.string,
  }),
  type: PropTypes.string,
  fb2: PropTypes.string,
  epub: PropTypes.string,
  pdf: PropTypes.string,
  audio_href: PropTypes.string,
  media_bible: PropTypes.array,
};

export default memo(MediaDetailMain);
