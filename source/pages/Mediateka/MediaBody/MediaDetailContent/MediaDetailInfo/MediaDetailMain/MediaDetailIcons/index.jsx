import React, { memo, useCallback } from 'react';
import { useDispatch } from 'react-redux';

import { getMediatekaDownloadsIncrease } from '~store/mediateka/action';

import MediaDetailMultiReadyLink from '../../../MediaDetailMultiContent/MediaDetailMultiBody/MediaDetailMultiReady/MediaDetailMultiReadyLink';
import Icon from '~components/Icons/Icons';
import ShareButton from '~components/ShareButton';

import './mediaDetailIcons.scss';

import { typeDownload } from '../../../utilsMediaDetail';

const MediaDetailIcons = ({ audio_href, epub, fb2, pdf, className = '', mediaId }) => {
  const isMediaDownload = audio_href || epub || fb2 || pdf;

  const dispatch = useDispatch();

  const handleDownload = useCallback(
    setName => () => {
      dispatch(getMediatekaDownloadsIncrease(mediaId, setName));
    },
    [mediaId],
  );

  const { read, listen } = typeDownload;

  return (
    <div className={`${className} media-icons`}>
      {isMediaDownload && (
        <div className="media-icons-item">
          <Icon icon="download_media" type="download" />

          <p>Cкачать</p>
          <div className="media-icons-download">
            <div className="media-icons-download-content">
              <MediaDetailMultiReadyLink
                text="fb2"
                href={fb2}
                onClick={handleDownload(read)}
                download
              />
              <MediaDetailMultiReadyLink
                text="mp3"
                href={audio_href}
                onClick={handleDownload(listen)}
                download
              />
              <MediaDetailMultiReadyLink
                text="epub"
                href={epub}
                onClick={handleDownload(read)}
                download
              />
              <MediaDetailMultiReadyLink
                  text="pdf"
                  href={pdf}
                  onClick={handleDownload(read)}
                  download
              />
            </div>
          </div>
        </div>
      )}
      <ShareButton className="media-icons-item" />
    </div>
  );
};

export default memo(MediaDetailIcons);
