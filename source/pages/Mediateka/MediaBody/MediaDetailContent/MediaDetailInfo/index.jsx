import React from 'react';

import MediaDetailMain from './MediaDetailMain/';
import MediaDetailVideo from './MediaDetailVideo/';
import { defaultTemplate } from '../../utilsMediaBody';

const MediaDetailInfo = ({ media, categoryId }) => {
  const { template, id, ...items } = media;
  switch (template) {
    case defaultTemplate.video:
      return <MediaDetailVideo mediaId={id} categoryId={categoryId} {...items} />;
    default:
      return <MediaDetailMain mediaId={id} {...items} />;
  }
};

export default MediaDetailInfo;
