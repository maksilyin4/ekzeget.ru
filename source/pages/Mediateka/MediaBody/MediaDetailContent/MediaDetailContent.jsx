import React, { useState, useEffect, useContext, memo, useMemo } from 'react';
import { useLocation, useHistory, useRouteMatch } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import './mediaDetail.scss';

import { MediaContext } from '~context/MediaContext';

import useMediaDetailQuery from '~apollo/query/mediaLibrary/mediaDetail';

import MediaDetailInfo from './MediaDetailInfo';
import MediaDetailMultiContent from './MediaDetailMultiContent';
import MediaComments from './MediaComments';
import Preloader from '~components/Preloader/Preloader';

import { defaultTemplate, deleteQueryUrlFilter, relatedObj } from '../utilsMediaBody';
import { getTypeThisMedia } from '../MediaContent/MediaContentList/utilsMediaContentList';
import { getRelatedTypes, setDetailCode } from './utilsMediaDetail';
import { useMediaViews } from '../../../../hooks/useMediaViews';
import {
  getMediatekaDownloadsSuccess,
  setMediatekaBreadcrumbs,
  clearBread,
} from '~store/mediateka/action';
import { prefixForDetailCode } from '~utils/common';

const MediaDetailContent = () => {
  const history = useHistory();
  const { search, pathname } = useLocation();
  const {
    path,
    url,
    params: { category_id = '', routing = '', detail_id = '' },
  } = useRouteMatch();

  const categoryID = category_id.includes(prefixForDetailCode) ? '' : category_id;

  const detailCode = setDetailCode({ categoryID, routing, detail_id, category_id });

  const subCategory = !routing.includes(prefixForDetailCode) && routing;

  const { screen, titleMeta, changeIs404, setMeta } = useContext(MediaContext);

  const [defaultAudio, setDefaultAudio] = useState([]);
  const [defaultVideo, setDefaultVideo] = useState([]);

  const dispatch = useDispatch();

  const { media, loading, refetch, error } = useMediaDetailQuery({
    variables: { code: detailCode },
  });

  const [mediaItem] = media;

  const {
    comments_count,
    allow_comment,
    related_media_types,
    audio_href,
    video_href,
    epub,
    fb2,
    pdf,
    type,
    id,
    statistics_downloads_read_count: read_cnt,
    statistics_downloads_listen_count: listen_cnt,
    template,
    main_category,
  } = mediaItem || {};

  useMediaViews(id);

  const relatedTypes = useMemo(() => {
    if (related_media_types) {
      const related = [
        ...related_media_types,
        getTypeThisMedia(type, { audio_href, video_href, epub, fb2, pdf }),
      ];

      return getRelatedTypes(related, relatedObj);
    }
    return [];
  }, [related_media_types, type, audio_href, video_href, epub, fb2, pdf]);

  useEffect(() => {
    if (error) {
      changeIs404(error);
    }
  }, [error]);

  useEffect(() => {
    if (categoryID) {
      refetch();
    }

    return () => {
      // удаляем квери после просмотра детальной страницы мультиконтента
      deleteQueryUrlFilter({ search: location.search, history, del: ['online=', 'glava='] });
    };
  }, [subCategory]);

  useEffect(() => {
    if (media.length) {
      const { type, title: mediaTitle, author } = media[0] || {};
      setMeta(
        type === 'V'
          ? {
              title: `БИБЛИЯ онлайн: ${mediaTitle} - ${author.title}`,
              h_one: `${mediaTitle} - ${author.title}`,
            }
          : media[0].meta,
      );
      const { title, code, parent: parentCat } = main_category || {};

      if (title) {
        const mainCat = { title, code };
        const parent = parentCat || mainCat;
        const children = parentCat ? mainCat : undefined;

        dispatch(setMediatekaBreadcrumbs({ parent, children, detailTitle: media[0].title }));
      }
    }
    return () => dispatch(clearBread());
  }, [media, pathname, titleMeta, search, url, categoryID]);

  useEffect(() => {
    if (audio_href) {
      setDefaultAudio([
        { id: 'defaultAudio', audio_href, title: mediaItem.title, duration: mediaItem.length },
      ]);
    }
    if (video_href) {
      setDefaultVideo([
        {
          id: 'defaultVideo',
          video_href,
          title: mediaItem.title,
          description: mediaItem.description,
        },
      ]);
    }
  }, [audio_href, video_href]);

  useEffect(() => {
    dispatch(getMediatekaDownloadsSuccess({ id, data: { read_cnt, listen_cnt } }));
  }, [id, read_cnt, listen_cnt, refetch]);

  if (loading) {
    return <Preloader className="media-detail-info-preloader" />;
  }

  if (error) {
    return null;
  }

  const multiLink = `/mediateka/${categoryID ? `${categoryID}/` : ''}${
    categoryID && !routing.includes(prefixForDetailCode) ? `${routing}/` : ''
  }${detailCode ? `${prefixForDetailCode}${detailCode}/` : ''}`;

  return (
    <>
      <div className="media-detail-info">
        <MediaDetailInfo media={mediaItem} categoryId={categoryID} />
        {(template === defaultTemplate.main || template === defaultTemplate.img) && (
          <MediaDetailMultiContent
            url={multiLink}
            path={path}
            defaultVideo={defaultVideo}
            defaultAudio={defaultAudio}
            defaultBook={{ epub, fb2, pdf, mediaId: +id }}
            media_id={+id}
            relatedTypes={relatedTypes}
          />
        )}
      </div>
      <MediaComments
        count={comments_count}
        screen={screen}
        media_id={+id}
        allow_comment={allow_comment}
      />
    </>
  );
};

export default memo(MediaDetailContent);
