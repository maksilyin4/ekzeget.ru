import React from 'react';
import { useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import { getMediatekaDownloadMediaId } from '~store/mediateka/selectors';

import { MediaDetailLink } from './MediaDetailLink';

import { getTitle, setLabel } from '../utilsMediaMultiLink';
import { getToForLink } from '../../../utilsMediaBody';

import './mediaDetailMultiLink.scss';

const MediaDetailMultiLink = ({ relatedTypes, url, mediaId }) => {
  const { search } = useLocation();

  const mediatekaDownload = useSelector(getMediatekaDownloadMediaId(mediaId));

  return (
    <div className="media-detail-multi-link">
      {relatedTypes.map(
        ({ relatedTitle }, i) =>
          relatedTitle && (
            <MediaDetailLink
              key={relatedTitle}
              pathname={getToForLink({ i, url, related: relatedTitle })}
              label={setLabel(mediatekaDownload, relatedTitle)}
              icon={relatedTitle}
              title={getTitle(relatedTitle)}
              search={search}
            />
          ),
      )}
    </div>
  );
};

MediaDetailMultiLink.propTypes = {
  relatedTypes: PropTypes.array,
  url: PropTypes.string,
};

export default MediaDetailMultiLink;
