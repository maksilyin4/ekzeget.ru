import React, { memo, useMemo } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';
import cx from 'classnames';

import Icon from '~components/Icons/Icons';
import { getIsMobile } from '~dist/selectors/screen';

const MOBILE_COUNT_SIZE = 4;
const MAX_MOBILE_COUNT_SIZE = 6;

export const MediaDetailLink = memo(({ pathname, search, label = '0 скачиваний', icon, title }) => {
  const isMobile = useSelector(getIsMobile);

  const oddEvent = (match, location) => {
    return match && match.url === location.pathname;
  };

  const adaptiveCountClasses = useMemo(() => {
    let adaptiveClass = '';

    if (isMobile) {
      const labelLength = label.split(' ')[0].length;

      if (labelLength >= MOBILE_COUNT_SIZE) {
        adaptiveClass = 'media-detail__download-count-adaptive';
      } else if (labelLength > MAX_MOBILE_COUNT_SIZE) {
        adaptiveClass = 'media-detail__download-max-count-adaptive';
      }
    }

    return adaptiveClass;
  }, [isMobile, label]);

  return (
    <NavLink to={{ pathname, search }} isActive={oddEvent} className="media-detail-multi-link-item">
      <div className="media-detail-multi-link-item-content">
        <p
          className={cx(
            'media-detail__content media-detail__download-count',
            adaptiveCountClasses,
          )}>
          {label}
        </p>
        <div className="media-detail-title-container">
          <Icon icon={icon} />
          <p className="media-detail__content media-detail__download-title">{title}</p>
        </div>
      </div>
    </NavLink>
  );
});

MediaDetailLink.propTypes = {
  pathname: PropTypes.string,
  label: PropTypes.string,
  icon: PropTypes.string,
  title: PropTypes.string,
  search: PropTypes.string,
};
