import React, { useMemo, memo, useCallback } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

import './mediaDetailMultiReady.scss';

import { getMediatekaDownloadsIncrease } from '~store/mediateka/action';

import MediaDetailMultiReadyLink from './MediaDetailMultiReadyLink';
import MediaDetailMultiReadyOnline from './MediaDetailMultiReadyOnline';
import MediaBtn from '~components/Mediateka/BtnMedia/';

import {
  addQueryUrl,
  queryFilter,
  deleteQueryUrlFilter,
  getQueryUrl,
} from '../../../../utilsMediaBody';
import { typeDownload } from '../../../utilsMediaDetail';

const MediaDetailMultiReady = ({ relatedData, defaultBook }) => {
  const { search } = useLocation();
  const history = useHistory();

  const dispatch = useDispatch();

  const { fb2, epub, pdf, mediaId } = defaultBook;

  const searchReadyOnline = useMemo(() => getQueryUrl({ tag: queryFilter.online, search }), [
    search,
  ]);

  const activeReady = useMemo(() => {
    return search && searchReadyOnline ? [searchReadyOnline.split(',')] : ['ready'];
  }, [search, searchReadyOnline]);

  // Читать онлайн
  const handleClick = useCallback(() => {
    if (!searchReadyOnline) {
      addQueryUrl({ search, history, name: queryFilter.online, state: activeReady });
    } else {
      deleteQueryUrlFilter({ search, history, del: [queryFilter.online] });
    }
  }, [search]);

  const handleDownload = useCallback(() => {
    dispatch(getMediatekaDownloadsIncrease(mediaId, typeDownload.read));
  }, [mediaId]);

  return (
    <>
      <div className="media-multi-ready-controls">
        {relatedData.length > 0 && (
          <MediaBtn text="Читать онлайн" styleName="media-controls-item" onClick={handleClick} />
        )}
        <MediaDetailMultiReadyLink text="FB2" href={fb2} onClick={handleDownload} download />
        <MediaDetailMultiReadyLink text="EPUB" href={epub} onClick={handleDownload} download />
        <MediaDetailMultiReadyLink text="PDF" href={pdf} onClick={handleDownload} download />
      </div>

      {relatedData.length > 0 && (
        <MediaDetailMultiReadyOnline relatedData={relatedData} search={search} />
      )}
    </>
  );
};

MediaDetailMultiReady.propTypes = {
  relatedData: PropTypes.array,
  defaultBook: PropTypes.shape({
    fb2: PropTypes.string,
    epub: PropTypes.string,
    pdf: PropTypes.string,
  }),
};

export default memo(MediaDetailMultiReady);
