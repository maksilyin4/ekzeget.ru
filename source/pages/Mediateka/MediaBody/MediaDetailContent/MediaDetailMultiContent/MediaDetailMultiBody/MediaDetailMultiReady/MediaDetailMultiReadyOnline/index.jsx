import React, { useMemo, useCallback, useRef } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { useSelector } from 'react-redux';

import cx from 'classnames';
import { getIsMobile } from '~dist/selectors/screen';

import { addQueryUrl, queryFilter, deleteQueryUrlFilter } from '../../../../../utilsMediaBody';
import { getActiveNav, maxHeightHeader } from './utilsMediaReadyOnline';

import './mediaDetailMultiReadyOnline.scss';

const MediaDetailMultiReadyOnline = ({ relatedData = [], search, history }) => {
  const isMobile = useSelector(getIsMobile);

  const searchReadyOnlineGlava = useMemo(() => new URLSearchParams(search).get(queryFilter.glava), [
    search,
  ]);

  const refGlava = useRef([]);

  const handleClick = useCallback(
    (code, i) => () => {
      if (searchReadyOnlineGlava !== code) {
        if (!isMobile) {
          refGlava.current[i].scrollIntoView();
        } else {
          setTimeout(() => {
            refGlava.current[i].scrollIntoView();
            window.scrollBy(0, maxHeightHeader - 5);
          }, 10);
        }
        addQueryUrl({ search, history, name: queryFilter.glava, state: code });
      } else {
        deleteQueryUrlFilter({ search, history, del: [queryFilter.glava] });
      }
    },
    [search, history],
  );

  const renderContent = useMemo(() => {
    if (searchReadyOnlineGlava && relatedData.length) {
      return relatedData.filter(({ code }) => code === searchReadyOnlineGlava);
    }
    return relatedData.length ? [relatedData[0]] : [];
  }, [searchReadyOnlineGlava, relatedData]);

  return (
    <div className="media-ready-online-container">
      <ul className="media-ready-online-nav">
        {relatedData.map(({ id, title, code }, i) => {
          const isActive = getActiveNav(searchReadyOnlineGlava, code);
          return (
            <li key={id}>
              <button
                ref={ref => refGlava.current.push(ref)}
                className={cx('media-ready-online-nav-item', {
                  active: isActive,
                })}
                onClick={handleClick(code, i)}>
                {title}
              </button>
              <div className={cx('media-ready-online-content', { active: isActive })}>
                {renderContent.map(({ id, text }) => (
                  <div key={id} dangerouslySetInnerHTML={{ __html: text }} />
                ))}
              </div>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

MediaDetailMultiReadyOnline.propTypes = {
  relatedData: PropTypes.array,
  search: PropTypes.string,
  history: PropTypes.object,
};

export default withRouter(MediaDetailMultiReadyOnline);
