import React from 'react';

import Icon from '~components/Icons/Icons';

import './mediaDetailMultiReadyLink.scss';

const MediaDetailMultiReadyLink = ({ text = '', href = '', onClick = f => f }) => {
  if (!href) {
    return null;
  }
  return (
    <a href={href} onClick={onClick} className="media-controls-item" download target="blank">
      <Icon className="multi-icon" icon="download_book" />
      <p>{text}</p>
    </a>
  );
};

export default MediaDetailMultiReadyLink;
