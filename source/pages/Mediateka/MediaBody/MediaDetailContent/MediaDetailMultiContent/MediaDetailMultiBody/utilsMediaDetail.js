export const templateTypes = {
  audio: 'A',
  video: 'V',
  book: 'B',
  chapterBook: 'C',
};
