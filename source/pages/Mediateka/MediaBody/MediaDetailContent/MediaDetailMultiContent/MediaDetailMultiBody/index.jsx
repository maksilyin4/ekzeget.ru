import React from 'react';
import cx from 'classnames';

import MediaDetailMultiAudio from './MediaDetailMultiAudio';
import MediaDetailMultiVideo from './MediaDetailMultiVideo';
import MediaDetailMultiReady from './MediaDetailMultiReady';

import useMediaRelatedQuery from '~apollo/query/mediaLibrary/related';

import { templateTypes } from './utilsMediaDetail';

import './mediaDetailMultiBody.scss';

const MediaDetailMultiBody = ({ related, media_id, defaultAudio, defaultBook, defaultVideo }) => {
  const { relatedTitle, relatedOrigin } = related;

  const { relatedData } = useMediaRelatedQuery({
    media_id,
    type: relatedOrigin,
  });

  function getTemplate(relatedOrigin) {
    switch (relatedOrigin) {
      case templateTypes.audio:
        return (
          <MediaDetailMultiAudio
            media_id={media_id}
            relatedData={[...relatedData, ...defaultAudio]}
          />
        );
      case templateTypes.video:
        return (
          <MediaDetailMultiVideo
            relatedData={[...relatedData, ...defaultVideo]}
            relatedTitle={relatedTitle}
          />
        );
      case templateTypes.chapterBook:
      case templateTypes.book:
        return (
          <MediaDetailMultiReady
            relatedData={relatedData}
            defaultBook={defaultBook}
            relatedTitle={relatedTitle}
          />
        );
      default:
        return null;
    }
  }

  return (
    <div
      className={cx(`media-multi-body__${relatedTitle}`, {
        'media-video-heigtht': relatedTitle === 'V' && relatedData.length,
      })}>
      {getTemplate(relatedOrigin)}
    </div>
  );
};

export default MediaDetailMultiBody;
