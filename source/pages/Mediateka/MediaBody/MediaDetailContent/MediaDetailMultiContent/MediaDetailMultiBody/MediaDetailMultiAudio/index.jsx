import React, { useCallback, useState } from 'react';
import { useDispatch } from 'react-redux';

import './mediaDetailMultiAudio.scss';

import { getMediatekaDownloadsIncrease } from '~store/mediateka/action';

import MediaDetailMultiItem from './MediaDetailMultiItem';
import MediaDetailDownload from './MediaDetailDownload';

import { typeDownload } from '../../../utilsMediaDetail';

const getDownloadHref = audio_href => `${audio_href}?filename=${audio_href}`;

const getDownloadAllHref = mediaId =>
  `/api/v1/media/download-media-audio-archive/?mediaId=${mediaId}`;

const MediaDetailMultiAudio = ({ relatedData = [], media_id = '' }) => {
  const dispatch = useDispatch();
  const [nextSong, setNextSong] = useState('');

  const handleDownload = useCallback(() => {
    dispatch(getMediatekaDownloadsIncrease(media_id, typeDownload.listen));
  }, [media_id]);

  const callBack = value => {
    setNextSong(value);
  };

  // сортируем массив, пришедший с сервера, по именам аудио
  relatedData.sort((a, b) => a?.sort - b?.sort);

  // массив для данных после группировки по главам (если есть имена)
  let groupRelatedData = [];

  // группируем отсортированный массив по именам глав (если они есть)
  if (relatedData?.length && relatedData[0]?.grouping_name) {
    const newArr = [];

    relatedData.forEach(item => {
      // выделим в отдельный массив элемент (массив) с группировкой по главам
      const arrItem = newArr.length ? newArr[newArr?.length - 1] : [];
      // если после первого аудио файла нет группировки, то сгруппируем сами
      // но если в первом файле нет группировки, а дальше она идет - группировки не будет и все файлы просто пойдут списком
      if (!item.grouping_name) {
        item.grouping_name = 'Глава без имени';
      }
      // проверяем, сходится ли имя из полученных данных с именем последнего
      // элемента из последнего массива с группировкой
      item.grouping_name === arrItem[arrItem?.length - 1]?.grouping_name
        ? // имя сходится - пушим в массив с группировкой
          newArr[newArr.length - 1].push(item)
        : // имя не сходится - пушим как новый массив для группировки
          // и сразу пишим в этот новый массив элемент с новым именем
          newArr.push([item]);

      groupRelatedData = newArr;
    });
  }

  // состояние для определения, показываем аудио или нет
  const [show, setShow] = useState({});

  // показать / скрыть аудио в главе
  const openAudio = i => {
    setShow({ ...show, [i]: !show[i] });
  };

  const renderAudio = (item, i) => {
    return show[i] ? (
      item.map(itemObject => {
        return (
          <MediaDetailMultiItem
            key={itemObject.id}
            bookNext={itemObject.audio_href}
            callBack={callBack}
            audio_href={itemObject.audio_href}
            play={nextSong === itemObject.audio_href}
            relatedBible={itemObject.media_bible?.[0] || null}
            // chaptersCounter={restRelated?.book?.chapters.length}
            chaptersCounter={itemObject.book?.chapters.length}
            {...itemObject}
            download={
              <MediaDetailDownload
                href={getDownloadHref(itemObject.audio_href)}
                handleDownload={handleDownload}
              />
            }
          />
        );
      })
    ) : (
      <div />
    );
  };

  return (
    <div className="media-multi-audio">
      <div className="media-multi-audio-download-all">
        <MediaDetailDownload
          text="Скачать все"
          href={getDownloadAllHref(media_id)}
          handleDownload={handleDownload}
        />
      </div>
      <div className="media-multi-audio-list">
        {/* если есть имена группировки, то группируем, иначе все аудио выдаем списком */}
        {relatedData[0]?.grouping_name ? (
          <div className={'media-multi-body__book'}>
            {groupRelatedData.map((item, i) => {
              return (
                <>
                  <button
                    key={i}
                    className={'media-ready-online-nav-item margin_top'}
                    onClick={() => openAudio(i)}>
                    {item[0].grouping_name}
                  </button>
                  <div>{renderAudio(item, i)}</div>
                </>
              );
            })}
          </div>
        ) : (
          relatedData.map((item, i) => {
            if (item?.audio_href.length) {
              return (
                <MediaDetailMultiItem
                  key={item?.id}
                  bookNext={relatedData[i + 1]?.audio_href}
                  callBack={callBack}
                  audio_href={item?.audio_href}
                  play={nextSong === item?.audio_href}
                  relatedBible={item?.media_bible?.[0] || null}
                  // chaptersCounter={restRelated?.book?.chapters.length}
                  chaptersCounter={item?.book?.chapters.length}
                  {...item}
                  download={
                    <MediaDetailDownload
                      href={getDownloadHref(item?.audio_href)}
                      handleDownload={handleDownload}
                    />
                  }
                />
              );
            }
          })
        )}
      </div>
    </div>
  );
};

export default MediaDetailMultiAudio;
