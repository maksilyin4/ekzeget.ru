import { bible } from '~utils/common';

export const getUrlToBible = relatedBible => {
  const chapter = relatedBible?.chapter?.number;
  const bookCode = relatedBible?.book?.code;
  const chapterNumber = chapter ? `glava-${chapter}/` : '';
  return bookCode ? `/${bible}/${bookCode}/${chapterNumber}` : '';
};
