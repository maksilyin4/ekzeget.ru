import React from 'react';
import PropTypes from 'prop-types';

import Icon from '~components/Icons/Icons';

const MediaDetailDownload = ({
  className = 'media-download-all',
  href = '',
  text = '',
  handleDownload = f => f,
}) => (
  <a className={className} href={href} onClick={handleDownload} target="blank" download>
    <Icon icon="download_media" />
    {text && <p>{text}</p>}
  </a>
);

MediaDetailDownload.propTypes = {
  className: PropTypes.string,
  href: PropTypes.string,
  text: PropTypes.string,
  handleDownload: PropTypes.func,
};

export default MediaDetailDownload;
