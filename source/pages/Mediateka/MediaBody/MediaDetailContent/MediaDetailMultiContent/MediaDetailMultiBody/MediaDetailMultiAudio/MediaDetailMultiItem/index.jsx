import React from 'react';

import AudioPlayer from '~components/AudioPlayer/AudioPlayer';

import './mediaDetailMultiItem.scss';

const MediaDetailMultiItem = ({
  title = '',
  audio_href,
  download,
  bookNext,
  callBack,
  nextSong,
  play,
  length,
  // chaptersCounter,
}) => {
  return (
    <div className="media-multi-audio-list-item">
      <div className="media-multi-audio-player">
        <AudioPlayer
          src={audio_href}
          autoPlay={play}
          bookNext={bookNext}
          callBack={callBack}
          nextSong={nextSong}
          title={title}
          downloadIcon={download}
          duration={length}
          preload="none"
        />
      </div>
    </div>
  );
};

export default MediaDetailMultiItem;
