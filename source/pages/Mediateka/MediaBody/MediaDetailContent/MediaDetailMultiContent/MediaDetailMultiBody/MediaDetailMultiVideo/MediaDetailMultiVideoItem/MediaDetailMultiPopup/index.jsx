import React, { useEffect } from 'react';

import { VideoPlay } from '~components/VideoPlay';
import MediaDetailMultiLinkBible from '~components/Mediateka/MediaDetailMultiLinkBible';

import { addQueryUrl, queryFilter, deleteSearchQueryPopup } from '../../../../../../utilsMediaBody';

import './mediaDetailMultiPopup.scss';

const MediaDetailMultiPopup = ({
  id,
  search,
  title,
  description,
  history,
  relatedTitle,
  setIsOpenPopup,
  video_href,
  chaptersCounter,
  relatedBibleLink,
}) => {
  useEffect(() => {
    addQueryUrl({
      search,
      history,
      name: queryFilter[relatedTitle],
      state: id,
    });
    return () => {
      setIsOpenPopup(false);
      deleteSearchQueryPopup({ search, history, name: queryFilter[relatedTitle] });
    };
  }, [id]);

  return (
    <div className="media-detail-multi-popup">
      <div className="media-detail-multi-popup-head">
        <p className="media-detail-multi-popup-head-title">
          {title}. <span>{chaptersCounter ? `(${chaptersCounter} стихов)` : ''}</span>
        </p>
      </div>
      <VideoPlay path={video_href} isLiteShow />
      <div className="media-detail-multi-popup-body">
        <div className="media-detail-multi-popup-body-head">
          <MediaDetailMultiLinkBible linkTo={relatedBibleLink} />
        </div>
        <div className="media-detail-multi-popup-body-desc">
          <p>{description}</p>
        </div>
      </div>
    </div>
  );
};

export default MediaDetailMultiPopup;
