import React, { memo, useCallback } from 'react';

import MediaContentImg from '~components/Mediateka/MediaContentImg';
import MediaDetailMultiVideoDesc from '~components/Mediateka/MediaDescription';
import MediaDetailMultiVideoLinkBible from '~components/Mediateka/MediaDetailMultiLinkBible';
import MediaDetailMultiPopup from './MediaDetailMultiPopup';

import { Popup } from '~dist/browserUtils';
import { getUrlToBible } from '../../MediaDetailMultiAudio/MediaDetailMultiItem/utilsMediaDetailMultiItem';
// import fancyTimeFormat from '~utils/fancyTimeFormat';

import './mediaDetailMultiVideoItem.scss';

const MediaDetailMultiVideoItem = ({
  id,
  title,
  video_href,
  image,
  description = '',
  relatedTitle,
  history,
  search,
  setIsOpenPopup,
  chaptersCounter,
  relatedBible,
  duration,
}) => {
  const relatedBibleLink = getUrlToBible(relatedBible);

  const handleOpenPopup = useCallback(() => {
    setIsOpenPopup(true);
    Popup.create({
      content: (
        <MediaDetailMultiPopup
          title={title}
          video_href={video_href}
          description={description}
          id={id}
          search={search}
          history={history}
          relatedTitle={relatedTitle}
          setIsOpenPopup={setIsOpenPopup}
          chaptersCounter={chaptersCounter}
          relatedBibleLink={relatedBibleLink}
        />
      ),
      className: 'media-popup-multi-video',
    });
  }, [search]);

  return (
    <div className="media-detail-multi-video-item">
      <div className="media-detail-multi-video-img" onClick={handleOpenPopup}>
        <MediaContentImg
          styleName="media-detail-multi-video-img-item"
          imageUrl={image?.url}
          title={title}
          search={search}
          videoHref={video_href}
          isLink
        />
        {duration && (
          <div className="media-detail-multi-video-duration">
            {
              // TODO удалить если будет не нужно 10.02.20
              // fancyTimeFormat(duration)
              duration
            }
          </div>
        )}
      </div>

      <div className="media-detail-multi-video-head">
        <p className="bold-video-title" onClick={handleOpenPopup}>
          {title}
        </p>
      </div>

      <MediaDetailMultiVideoLinkBible linkTo={relatedBibleLink} />
      <MediaDetailMultiVideoDesc description={description} />
    </div>
  );
};

export default memo(MediaDetailMultiVideoItem);
