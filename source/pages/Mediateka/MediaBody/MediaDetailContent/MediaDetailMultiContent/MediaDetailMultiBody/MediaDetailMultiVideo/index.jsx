import React, { memo, useEffect, useState } from 'react';
import LazyLoad from 'react-lazyload';
import { useLocation, useHistory } from 'react-router-dom';

import MediaDetailMultiVideoItem from './MediaDetailMultiVideoItem';
import MediaDetailMultiPopup from './MediaDetailMultiVideoItem/MediaDetailMultiPopup';

import { queryFilter } from '../../../../utilsMediaBody';
import { Popup } from '~dist/browserUtils';

import './mediaDetailMultiVideo.scss';

const MediaDetailMultiVideo = ({ relatedData = [], relatedTitle }) => {
  const { search } = useLocation();
  const history = useHistory();

  const [isOpenPopup, setIsOpenPopup] = useState(false);

  useEffect(() => {
    const searchPlay = new URLSearchParams(search).get(queryFilter[relatedTitle]);

    if (searchPlay && relatedData.length && !isOpenPopup) {
      const { title, video_href, description, id } = relatedData.find(
        ({ id }) => id === searchPlay,
      );

      Popup.create({
        content: (
          <MediaDetailMultiPopup
            title={title}
            video_href={video_href}
            description={description}
            id={id}
            search={search}
            history={history}
            relatedTitle={relatedTitle}
            setIsOpenPopup={setIsOpenPopup}
          />
        ),
        className: 'media-popup-multi-video',
      });
    }
  }, [search, relatedData]);

  return (
    <div className="media-multi-video">
      {relatedData.map(({ id, video_href, title, description, image, media_bible, length }) => {
        if (video_href.length) {
          return (
            <LazyLoad key={id} height={130}>
              <MediaDetailMultiVideoItem
                id={id}
                title={title}
                image={image}
                video_href={video_href}
                relatedTitle={relatedTitle}
                relatedBible={media_bible?.[0] || null}
                search={search}
                history={history}
                description={description}
                setIsOpenPopup={setIsOpenPopup}
                chaptersCounter={media_bible?.[0]?.chapter?.verses.length}
                duration={length}
              />
            </LazyLoad>
          );
        }
      })}
    </div>
  );
};

export default memo(MediaDetailMultiVideo);
