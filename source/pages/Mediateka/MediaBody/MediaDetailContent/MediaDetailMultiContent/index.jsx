import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';

import './mediaDetailMultiContent.scss';

import { getToForLink } from '../../utilsMediaBody';

import MediaDetailMultiLink from './MediaDetailMultiLink';
import MediaDetailMultiBody from './MediaDetailMultiBody';
import NotFound from '../../../../NotFound/NotFound';

const MediaDetailMultiContent = ({
  url,
  media_id,
  relatedTypes,
  defaultAudio,
  defaultBook,
  defaultVideo,
}) => {
  if (relatedTypes.length === 0) {
    return null;
  }
  return (
    <div className="media-detail-multi">
      <MediaDetailMultiLink relatedTypes={relatedTypes} url={url} mediaId={media_id} />
      <Switch>
        {relatedTypes.map(({ relatedTitle, relatedOrigin }, i) => {
          return (
            <Route
              exact
              key={relatedTitle}
              path={getToForLink({ i, url, related: relatedTitle })}
              render={() => (
                <MediaDetailMultiBody
                  media_id={media_id}
                  defaultVideo={defaultVideo}
                  defaultAudio={defaultAudio}
                  defaultBook={defaultBook}
                  related={{ relatedTitle, relatedOrigin }}
                />
              )}
            />
          );
        })}

        <Route path="*" component={NotFound} />
      </Switch>
    </div>
  );
};

MediaDetailMultiContent.propTypes = {
  url: PropTypes.string,
  media_id: PropTypes.number,
  relatedTypes: PropTypes.array,
};

export default MediaDetailMultiContent;
