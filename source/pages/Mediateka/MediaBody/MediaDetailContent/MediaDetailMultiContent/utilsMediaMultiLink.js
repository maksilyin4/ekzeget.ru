export function getTitle(related) {
  switch (related) {
    case 'listen':
      return 'Слушать';
    case 'play':
      return 'Смотреть';
    default:
      return 'Читать';
  }
}

function getWordLoad(count) {
  let lastLetter = 'й';
  const countBool = count % 100 < 10 || count % 100 > 20;

  if (countBool && count % 10 === 1) {
    lastLetter = 'e';
  } else if (countBool && (count % 10 === 2 || count % 10 === 3)) {
    lastLetter = 'я';
  }

  return `${count} скачивани${lastLetter}`;
}

export const setLabel = (mediatekaDownload = {}, relatedTitle = '') =>
  relatedTitle in mediatekaDownload ? getWordLoad(mediatekaDownload[relatedTitle] || 0) : '';
