import { prefixForDetailCode } from '~utils/common';

export const typeDownload = {
  read: 'read',
  listen: 'listen',
};

export function addFetchComment({ fetchMore, variablesComment, comments }) {
  return fetchMore({
    variables: { ...variablesComment, offset: comments.length },
    updateQuery: (prev, { fetchMoreResult }) => {
      if (!fetchMoreResult) return prev;
      return {
        ...prev,
        comment: [...prev.comment, ...fetchMoreResult.comment],
      };
    },
  });
}

export function getRelatedTypes(related, relatedObj) {
  try {
    const relatedTypes = [...related.filter(el => el.length)];

    const sortingTypes = Object.keys(relatedObj);
    const sortFunc = (a, b) => sortingTypes.indexOf(a) - sortingTypes.indexOf(b);
    const getIndex = title => ({ relatedTitle }) => relatedTitle === title;

    relatedTypes.sort(sortFunc);

    const t = relatedTypes
      .map(related => ({
        relatedTitle: relatedObj[related],
        relatedOrigin: related,
      }))
      .filter(
        ({ relatedTitle }, i, arr) => relatedTitle && arr.findIndex(getIndex(relatedTitle)) === i,
      );

    return t;
  } catch (error) {
    console.error('TCL: error', error);
  }
}

export const checkErrorMultiLink = (detail_id = '', active = '', relatedTypes = []) => {
  const titleMultiContent = detail_id.includes(prefixForDetailCode) ? active : detail_id;

  return (
    !titleMultiContent ||
    relatedTypes.some(({ relatedTitle }) =>
      relatedTitle.toLowerCase().includes(titleMultiContent.toLowerCase()),
    )
  );
};

export const setDetailCode = ({ categoryID, routing, detail_id, category_id }) => {
  if (categoryID) {
    return routing.includes(prefixForDetailCode)
      ? routing.replace(prefixForDetailCode, '')
      : detail_id.replace(prefixForDetailCode, '');
  }

  return category_id.replace(prefixForDetailCode, '');
};
