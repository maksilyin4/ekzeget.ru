import React from 'react';

import Icon from '~components/Icons/Icons';

const ContolsArrow = ({ className, icon, onClick }) => (
  <Icon className={className} icon={icon} onClick={onClick} />
);

export default ContolsArrow;
