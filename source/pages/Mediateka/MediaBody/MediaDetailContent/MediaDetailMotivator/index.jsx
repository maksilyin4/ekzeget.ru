import React, { useMemo, useEffect, useCallback } from 'react';
import { useLocation, useHistory, useRouteMatch } from 'react-router-dom';

import './mediaDetailMotivator.scss';

import Slider from '~components/Slider/';
import MediaMotivatorContentImg from '~components/Mediateka/MediaMotivatorContentImg';

import ContolsArrow from './ContolsArrow';

import { addQueryUrl, queryFilter } from '../../utilsMediaBody';

const getCurrentSlide = ({ content, getIndex }) => currentSlide => {
  const index = content[currentSlide].id;
  getIndex(index);
};

const getSettings = (index, content, getIndex) => {
  const obj = { content, getIndex };
  return {
    dots: false,
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: +index,
    afterChange: getCurrentSlide(obj),
    nextArrow: <ContolsArrow icon="angle-right-slide" />,
    prevArrow: <ContolsArrow icon="angle-left-slide" />,
  };
};

const MediaDetailMotivator = ({ index, content = [], template }) => {
  const { search } = useLocation();
  const history = useHistory();
  const match = useRouteMatch();

  const searchMotivator = useMemo(() => new URLSearchParams(search).get(queryFilter.motivator), [
    search,
  ]);

  const initialSlide = content.findIndex(el => el.id === index);

  const componentSlide = useMemo(
    () =>
      content.map(book => (
        <MediaMotivatorContentImg
          key={index}
          book={book}
          imageUrl={book?.image?.url ?? ''}
          match={match}
        />
      )),
    [content],
  );

  const addQuery = useCallback(
    nextMotivatorId =>
      addQueryUrl({
        search,
        history,
        name: queryFilter.motivator,
        state: nextMotivatorId,
        stateHistory: template,
      }),
    [],
  );

  useEffect(() => {
    if (!searchMotivator) {
      addQuery(index);
    }
  }, []);

  const getIndex = index => {
    addQuery(index);
  };
  return (
    <Slider
      className="media-slider-motivator"
      settings={getSettings(initialSlide, content, getIndex)}>
      {componentSlide}
    </Slider>
  );
};

export default MediaDetailMotivator;
