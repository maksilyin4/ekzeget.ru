import React, { useState, useMemo, useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useCookies } from 'react-cookie';

import useMediaCommentsQuery from '~apollo/query/mediaLibrary/comments';

import { Popup } from '~dist/browserUtils';

import MediaDescription from '~components/Mediateka/MediaDescription';
import IconClose from './IconClose';
import CommentInfo from './CommentInfo';
import CommentHead from './CommentHead';
import CommentForm from './CommentForm';
import CommentBtn from '~components/Mediateka/BtnMedia';
import PopupAuth from '~components/Popups/Auth/Auth';

import { addFetchComment } from '../utilsMediaDetail';

import './comments.scss';

const MediaComments = ({ screen, count, media_id, allow_comment }) => {
  const [deleteIdComments, setDeleteId] = useState([]);
  const [countCommonComment, setCountCommonComment] = useState();
  const [cookies, setCookie] = useCookies(['mediatekaComments', 'userId']);

  const { mediatekaComments, userId } = cookies;

  const isMediatekaComments =
    mediatekaComments && mediatekaComments[userId] && mediatekaComments[userId][media_id];

  const commentsCookie = useMemo(
    () => (userId && isMediatekaComments ? mediatekaComments[userId][media_id] : []),
    [userId, isMediatekaComments, media_id, mediatekaComments],
  );

  const countComments = useMemo(() => count - commentsCookie.length, [commentsCookie]);

  const variablesComment = useMemo(
    () => ({ media_id, limit: 3, offset: 0, excludeIds: commentsCookie }),
    [media_id, userId],
  );

  const { comments, fetchMore } = useMediaCommentsQuery({
    variables: variablesComment,
  });

  useEffect(() => {
    setCountCommonComment(countComments);
  }, []);

  useEffect(() => {
    setDeleteId(commentsCookie);
  }, [commentsCookie]);

  const authPopup = () => {
    if (Popup) {
      Popup.create(
        {
          title: 'Вход',
          content: <PopupAuth />,
          className: 'popup_auth not-header',
        },
        true,
      );
    }
  };

  const deleteComment = useCallback(
    id => () => {
      if (!userId) {
        authPopup();
      } else {
        const commonDeleteId = [...deleteIdComments, id];

        setCookie(
          'mediatekaComments',
          { ...mediatekaComments, [userId]: { [media_id]: commonDeleteId } },
          { path: '/' },
        );
        setDeleteId(commonDeleteId);
      }
    },
    [media_id, deleteIdComments, userId],
  );

  const addComment = () => {
    addFetchComment({ fetchMore, variablesComment, comments });
  };

  return (
    <div className="comments">
      {countComments !== 0 && (
        <>
          <CommentHead count={countComments} media_id={media_id} />
          {comments.map(({ comment, id, username, date }, index) => {
            const deleteId = deleteIdComments.find(del => +del === +id);
            if (!deleteId || deleteId === -1) {
              return (
                <div className="comment" key={id}>
                  <IconClose onClick={deleteComment(+id, index)} />
                  <CommentInfo username={username} date={date} />
                  <div className="comment-desc">
                    <MediaDescription
                      description={comment}
                      screen={screen}
                      defaultShowBtn={500}
                      textBtnClose="Читать полностью"
                      textBtnOpen="Скрыть полное описание"
                      classBtnContent="comment-desc-btn"
                    />
                  </div>
                </div>
              );
            }
          })}
          {comments.length !== countCommonComment && count !== 0 && (
            <div className="comment-btn">
              <CommentBtn text="Ещё комментарии" onClick={addComment} />
            </div>
          )}
        </>
      )}
      {allow_comment && <CommentForm media_id={media_id} />}
    </div>
  );
};

MediaComments.propTypes = {
  screen: PropTypes.object,
  count: PropTypes.number,
  media_id: PropTypes.string,
  allow_comment: PropTypes.bool,
};

export default MediaComments;
