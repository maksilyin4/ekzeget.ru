import React, { useState, useCallback, useEffect } from 'react';

import { useSelector } from 'react-redux';
import { useCookies } from 'react-cookie';

import Form, { FormSubmit } from '~components/Form/Form';
import TextField from '~components/TextField/TextField';
import useMediaSubscriberMutation from '~apollo/mutation/mediaLibrary/subscriber';

import { userDataInfo } from '~dist/selectors/User';

import './commentFormFollow.scss';

const CommentFormFollow = ({ media_id, unSubscriberRefetch }) => {
  const [email, setEmail] = useState('');
  const [cookies] = useCookies(['userId']);
  const userInfo = useSelector(userDataInfo);

  const { userId } = cookies;

  const [addSubscriber, { data, error, loading, called }] = useMediaSubscriberMutation();

  useEffect(() => {
    if (userInfo && userInfo.email) {
      setEmail(userInfo.email);
    }
  }, [userInfo && userInfo.id]);

  useEffect(() => {
    if (!error && called) {
      unSubscriberRefetch({ subscribe_id: +userId, id: +media_id });
    }
  }, [error, loading, called]);

  const handleChange = useCallback(
    e => {
      setEmail(e.target.value);
    },
    [email],
  );

  const handleSubmit = e => {
    e.preventDefault();

    if (email) {
      const back_url = process.env.BROWSER && window.location.href;
      addSubscriber({
        variables: {
          user_email: email,
          media_id: +media_id,
          user_id: +userId,
          username: null,
          back_url,
        },
      });
    }
  };

  return (
    <Form onSubmit={handleSubmit} className="comment-form comment-form-follow">
      {data && data.new_media_subscriber.id ? (
        <div className="media-subscriber-success">Спасибо за подписку</div>
      ) : (
        <>
          <p>
            Подписаться на новые <br />
            комментарии
          </p>
          <div className="comment-input">
            <TextField
              label="Ваш e-mail"
              required
              type="email"
              onChange={handleChange}
              value={email}
              isReq
            />
          </div>

          <FormSubmit title="Подписаться" isPreLoader={false} align="comment-btn" />
        </>
      )}
    </Form>
  );
};

export default CommentFormFollow;
