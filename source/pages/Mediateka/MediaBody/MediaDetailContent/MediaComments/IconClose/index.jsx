import React, { memo } from 'react';

const IconClose = ({ onClick }) => (
  <button className="comment-close" onClick={onClick}>
    <div className="close-icon">
      <span />
      <span />
    </div>
  </button>
);

export default memo(IconClose);
