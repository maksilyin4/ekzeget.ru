import React, { memo } from 'react';
import PropTypes from 'prop-types';

import { getDateFull } from '~utils/date';

const CommentInfo = ({ username, date = '29 января 2018' }) => (
  <div className="comment-info">
    <p>{username}</p>
    <span>{getDateFull(date)}</span>
  </div>
);

CommentInfo.propTypes = {
  username: PropTypes.string,
  date: PropTypes.string,
};
export default memo(CommentInfo);
