import React, { useState, memo, useCallback } from 'react';

import Form, { FormSubmit } from '~components/Form/Form';
import TextField from '~components/TextField/TextField';
import MediaFormAnswer from '~components/Mediateka/MediaFormAnswer/';

import useMutationComments from '~apollo/mutation/mediaLibrary/comments';
import { Popup } from '~dist/browserUtils';

import './commentForm.scss';

const CommentForm = ({ media_id }) => {
  const [state, setState] = useState({ username: '', user_email: '', textarea: '' });
  const [addComment] = useMutationComments();

  const handleChange = useCallback(
    setName => e => {
      setState({ ...state, [setName]: e.target.value });
    },
    [state],
  );

  const handleSubmit = e => {
    e.preventDefault();
    const { username, user_email, textarea } = state;
    if (username && user_email && textarea) {
      addComment({ variables: { username, user_email, media_id: +media_id, comment: textarea } });

      Popup.create(
        {
          title: null,
          content: <MediaFormAnswer />,
          className: 'media-popup-comment-answer',
        },
        true,
      );
    }
  };

  const { username, user_email, textarea } = state;
  return (
    <Form onSubmit={handleSubmit} className="comment-form comment-form-send">
      <p>Оставить комментарий</p>
      <div className="comment-input">
        <TextField
          label="Ваше имя"
          type="text"
          required
          onChange={handleChange('username')}
          value={username}
          isReq
        />
        <TextField
          label="Ваша почта"
          required
          type="email"
          onChange={handleChange('user_email')}
          value={user_email}
          isReq
        />
      </div>
      <TextField
        label="Комментарий"
        type="textarea"
        required
        onChange={handleChange('textarea')}
        value={textarea}
        isReq
      />
      <FormSubmit title="Оставить комментарий" isPreLoader={false} align="comment-btn" />
    </Form>
  );
};

export default memo(CommentForm);
