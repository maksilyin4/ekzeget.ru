import React, { memo, useEffect } from 'react';
import { useCookies } from 'react-cookie';

import useMediaSubscriberQuery from '~apollo/query/mediaLibrary/subscriber';
import useMediaUnSubscriberMutation from '~apollo/mutation/mediaLibrary/unsubscriber';

import MediaBtn from '~components/Mediateka/BtnMedia';
import PopupAuth from '~components/Popups/Auth/Auth';
import CommentFormFollow from '../CommentFormFollow';

import { Popup } from '~dist/browserUtils';

const commentPopup = props => {
  Popup.create(
    {
      title: null,
      content: <CommentFormFollow {...props} />,
      className: 'media-popup-comment-follow',
    },
    true,
  );
};

const authPopup = () => {
  if (Popup) {
    Popup.create(
      {
        title: 'Вход',
        content: <PopupAuth />,
        className: 'popup_auth not-header',
      },
      true,
    );
  }
};

const CommentHead = ({ count = 5, media_id }) => {
  const [cookies] = useCookies(['userId']);
  const { userId } = cookies;

  const [addUnSubscriber, { error, loading }] = useMediaUnSubscriberMutation();

  const [
    getUnSubscriber,
    unSubscriber,
    { refetch: unSubscriberRefetch },
  ] = useMediaSubscriberQuery();

  useEffect(() => {
    if (userId) {
      getUnSubscriber({
        variables: { subscribe_id: +userId, id: +media_id },
      });
    }
  }, [userId]);

  const handleClick = async () => {
    if (userId && unSubscriber && unSubscriber.subscription) {
      await addUnSubscriber({
        variables: {
          subscriber_id: +unSubscriber.subscription.id,
          checksum: unSubscriber.subscription.checksum,
        },
      });

      if (unSubscriberRefetch) {
        return unSubscriberRefetch({ subscribe_id: +userId, id: +media_id });
      }
    }
    return userId ? commentPopup({ media_id, unSubscriberRefetch }) : authPopup();
  };

  const text =
    userId && unSubscriber && unSubscriber.subscription ? 'Отписаться' : 'Следить за комментариями';

  return (
    <div className="comments-head">
      <div className="comments-head-item">
        <p>Комментарии</p>
        <span>({count})</span>
      </div>
      <div className="comments-btn">
        <MediaBtn text={text} onClick={handleClick} loading={loading} error={error} />
      </div>
    </div>
  );
};

export default memo(CommentHead);
