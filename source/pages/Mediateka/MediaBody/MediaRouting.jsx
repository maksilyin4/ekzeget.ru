import React from 'react';

import { prefixForDetailCode } from '~utils/common';

import MediaBodyLeftBlock from './MediaBodyLeftBlock/';
import MediaContent from './MediaContent/MediaContent';
import MediaDetailContent from './MediaDetailContent/MediaDetailContent';

import './mediaBody.scss';

export const MediaRouting = ({
  match: {
    params: { category_id = '', routing = '', detail_id },
  },
}) => {
  return (
    <div className="wrapper wrapper_two-col media_wrapper">
      <MediaBodyLeftBlock />
      <div className="media-library__materials">
        {category_id.includes(prefixForDetailCode) ||
        routing.includes(prefixForDetailCode) ||
        detail_id ? (
          <MediaDetailContent />
        ) : (
          <MediaContent />
        )}
      </div>
    </div>
  );
};
