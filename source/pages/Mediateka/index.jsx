import React from 'react';
import Loadable from 'react-loadable';
import Preloader from '../../components/Preloader/Preloader';

const LoadableBar = Loadable({
  loader: () => import('./Mediateka'),
  loading() {
    return (
      <div className="preloader-relative">
        <Preloader />
      </div>
    );
  },
});

export default LoadableBar;
