import { mediateka } from '../../utils/common';

export const getBread = ({ parentCat, childrenCat, detailTitle }) => {
  const defaultBread = [{ path: '/', title: 'Главная' }];

  if (parentCat || childrenCat) {
    [{ parentCat, childrenCat, detailTitle }].forEach(el => {
      const pathParent =
        el.childrenCat || el.detailTitle ? `/${mediateka}/${el.parentCat?.code}/` : '';
      const pathChild =
        el.childrenCat && el.detailTitle
          ? `/${mediateka}/${el.parentCat?.code}/${el.childrenCat?.code}/`
          : '';
      const child = el.childrenCat ? [{ path: pathChild, title: el.childrenCat?.title }] : [];

      const detail = el.detailTitle ? [{ path: '', title: el.detailTitle }] : [];
      defaultBread.push(
        ...[
          { path: '/mediateka/', title: 'Медиатека' },
          { path: pathParent, title: el.parentCat?.title },
          ...child,
          ...detail,
        ],
      );
    });
  } else {
    defaultBread.push(...[{ path: '', title: 'Медиатека' }]);
  }

  return defaultBread;
};

const initialState = {
  bread: [
    { path: '/', title: 'Главная' },
    { path: '', title: 'Медиатека' },
  ],
  meta: {
    title: 'БИБЛИЯ онлайн - Медиатека: видео, мотиваторы, Библия в искусстве, книги, иконография',
    description:
      'Раздел Медиатека к Библии включает в себя множество гениальных произведений искусства, видео к каждой главе Библии, мотиваторы к цитатам из Библии и цитатам святых отцов, книги к Библии, Библия в иконографии.',
    keywords: 'Медиатека к Библии, читать Библию, Библия онлайн, слушать Библию',
    h_one: 'Медиатека к Библии',
  },
};

export default initialState;
