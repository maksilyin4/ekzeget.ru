import React, { useState, useEffect, memo } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';

import { getMediatekaBreadcrumbs } from '~store/mediateka/selectors';

import './styles.scss';

import getScreen from '../../dist/selectors/screen';

import initialState, { getBread } from './utilsMediateka';

import { MediaContext } from '~context/MediaContext';

import { MediaBody } from './MediaBody';
import PageHead from '~components/PageHead/';
import BreadHead from '~components/BreadHead';
import Helmet from '~components/Helmet';
import NotFound from '../NotFound/NotFound';
// import { setMetaData } from '~store/metaData/selector';
import { setClearMeta } from '~store/metaData/actions';

const Mediateka = () => {
  const dispatch = useDispatch();
  const { pathname } = useLocation();
  const { phone, tablet, desktop, tabletMedium } = useSelector(getScreen);
  const defaultMeta = initialState.meta;

  const [meta, setMeta] = useState(defaultMeta);

  useEffect(() => {
    if (pathname.split('/').length === 3) {
      setMeta(defaultMeta);
    }
  }, []);

  const { parent: parentCat, children: childrenCat, detailTitle } = useSelector(
    getMediatekaBreadcrumbs,
  );

  const [searchMedia, setSearchMedia] = useState('');
  const [state, setState] = useState(initialState);
  const [is404, setIs404] = useState('');

  useEffect(() => {
    const defaultBread = getBread({ parentCat, childrenCat, detailTitle });
    setState({ ...state, h_one: defaultBread[defaultBread.length - 1].title, bread: defaultBread });
  }, [parentCat, childrenCat, detailTitle]);

  useEffect(() => {
    dispatch(setClearMeta());
  }, []);

  useEffect(() => {
    if (pathname.split('/').filter(el => el).length === 1) {
      setMeta(defaultMeta);
    }

    if (is404) {
      changeIs404('');
    }
  }, [pathname, defaultMeta.h_one]);

  const changeIs404 = error => {
    setIs404(error);
  };

  const { bread } = state;

  if (is404) {
    return <NotFound />;
  }

  return (
    <MediaContext.Provider
      value={{
        bread: [],
        phone,
        tablet,
        desktop,
        tabletMedium,
        handleSearch: setSearchMedia,
        searchMedia,
        meta,
        setMeta,
        changeIs404,
      }}>
      <Helmet title={meta?.title} description={meta?.description} keywords={meta?.keywords} />
      <div className="page media-library media-library__mateiral-container">
        <div className="wrapper wrapper__header">
          <BreadHead bread={bread} query="mediateka_tutorial_video_url" />
          <PageHead h_one={meta?.h_one || meta?.title} />
          <MediaBody />
        </div>
      </div>
    </MediaContext.Provider>
  );
};

export default memo(Mediateka);
