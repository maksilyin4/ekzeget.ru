import React, { Component } from 'react';
import Select from 'react-select';
import { Link } from 'react-router-dom';
import cx from 'classnames';
import PropTypes from 'prop-types';
import OrphusInfo from '../../components/OrphusInfo';

class PreachingNav extends Component {
  render() {
    const {
      excuses,
      excusesSelect,
      activeExcuse,
      celebrations,
      onChange,
      onChangeSelect,
      url,
    } = this.props;

    const urlBasic = url.endsWith('/') ? url : `${url}/`;

    return (
      <>
        <div className="side preaching-side__wrapper">
          <div className="side__item nav-side">
            {excuses.map(excuse => {
              const endUrl = celebrations.length
                ? celebrations.find(el => el.parent.id === excuse.id)
                : '';
              return (
                <div key={excuse.id} className="list__item">
                  <Link
                    className={cx('nav-side__link list__item-link', {
                      active: (activeExcuse || excuses[0].code) === excuse.code,
                    })}
                    onClick={onChange(excuse.code, excuse.id)}
                    to={{
                      pathname: `${urlBasic}${excuse.code}/`,
                      state: { preacherWeek: endUrl.code },
                    }}>
                    {excuse.title}
                  </Link>
                </div>
              );
            })}
          </div>
          <OrphusInfo />
        </div>

        <div className="preaching-side">
          <Select
            isSearchable={false}
            options={excusesSelect}
            value={excusesSelect.find(el => el.value === activeExcuse)}
            onChange={onChangeSelect}
            classNamePrefix="custom-select"
          />
        </div>
      </>
    );
  }
}

PreachingNav.propTypes = {
  excuses: PropTypes.array,
  excusesSelect: PropTypes.object,
  activeExcuse: PropTypes.string,
  celebrations: PropTypes.array,
  onChange: PropTypes.func,
  onChangeSelect: PropTypes.func,
  url: PropTypes.string,
};

export default PreachingNav;
