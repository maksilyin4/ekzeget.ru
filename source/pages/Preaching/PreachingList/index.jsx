import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const PreachingList = ({ celebrations = [], excuse_id, excuses, basicUrl, preachers }) => {
  const activeExcuse = excuses.find(el => el.code === excuse_id);

  return (
    <div className="all-about-bible-list preaching_list_content">
      {celebrations
        .filter(celebration => celebration.parent.id === (activeExcuse ? activeExcuse.id : 1))
        .map((celebration, i) => {
          return (
            <div className="content" key={celebration.id + celebration.code}>
              <div className="exegetes__item">
                <Link
                  to={`${basicUrl}${excuse_id}/${celebration.code}/${
                    i === 0 && preachers.length ? `${preachers[0].code}/` : ''
                  }`}
                  className="exegetes__link"
                  title={String(celebration.title)}>
                  {celebration.title}
                </Link>
              </div>
            </div>
          );
        })}
    </div>
  );
};

PreachingList.propTypes = {
  basicUrl: PropTypes.string,
  celebrations: PropTypes.array,
  excuses: PropTypes.array,
  preachers: PropTypes.array,
  excuse_id: PropTypes.string,
};

export default PreachingList;
