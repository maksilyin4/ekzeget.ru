import { parsePathname } from '../../utils/parsePathname';

export async function fetchData({
  getPreachingExcuse,
  getPreachingCelebration,
  setIsLoading,
  excuses,
  celebrations = [],
}) {
  setIsLoading(true);
  try {
    excuses.length === 0 && (await getPreachingExcuse());
    celebrations.length === 0 && (await getPreachingCelebration('?per-page=1000'));
  } catch (error) {
    console.log(error);
  }

  setIsLoading(false);
}

export function setPreacherId({ pathname, locationState, celebrations, excuses, excuse_id }) {
  if (excuse_id && celebrations.length) {
    const excuse = excuses.find(el => el.code === excuse_id);
    return (
      parsePathname({ pathname, index: 3, defaultReturn: '' }) ||
      (locationState && locationState.preacherWeek) ||
      celebrations.find(el => el.parent.id === excuse?.id)?.code ||
      ''
    );
  }
}
