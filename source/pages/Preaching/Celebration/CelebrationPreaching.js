import R from 'ramda';
import React, { memo, useMemo } from 'react';
import { Link } from 'react-router-dom';

import VersePreacherLink from './VersePreacherLink';
import Preloader from '../../../components/Preloader/Preloader';

import { parseTextPunctuation } from '../../../dist/utils';

const parsePreachText = (text, activePreacher) => {
  if (activePreacher) {
    const regexpSearch = new RegExp(activePreacher + /&quot?;/, 'gi');
    return text.replace(regexpSearch, p => `<strong class="yellow__text">${p}</strong>`);
  }
  return text;
};

const CelebrationPreaching = ({
  preaching,
  activePreacher,
  basicUrl,
  match: {
    params: { excuseId, preacherWeekId, id },
  },
  isPreaching,
}) => {
  const preach = useMemo(() => R.pathOr([], ['preaching'], preaching), [preaching]);

  const preachingList = useMemo(() => {
    if (id) {
      return preach.filter(p => p.code === id);
    }
    return preach;
  }, [id, preach]);

  if (isPreaching) {
    return <Preloader />;
  }

  return (
    <div className="content-area-for-side celebration__about">
      {preach.length === 1 || !!id ? (
        preachingList.map(preach => (
          <div key={preach.id} className="verse__preaching">
            <div className="verse__preacher-links">
              <VersePreacherLink
                key="apostols"
                title="Апостол:"
                list={R.pathOr([], ['excuse', 'apostolic'], preaching)}
              />
              <VersePreacherLink
                key="gospel"
                title="Евангелие:"
                list={R.pathOr([], ['excuse', 'gospel'], preaching)}
              />
            </div>
            <p className="verse__preacher celebration__desc">
              {R.pathOr('', ['preacher', 'name'], preach)}
            </p>
            <p className="verse__preaching-occasion">{parseTextPunctuation(preach.theme)}</p>
            <div
              key={`preaching-text-${preach.id}`}
              className="verse__preaching-text"
              dangerouslySetInnerHTML={{
                __html: parsePreachText(preach.text, activePreacher),
              }}
            />
          </div>
        ))
      ) : (
        <div className="celebration__list_content">
          {preach.map(preaching => (
            <div className="exegetes__item" key={preaching.id}>
              <Link
                to={`${basicUrl}${excuseId}/${preacherWeekId}/${activePreacher}/${preaching.code}/`}
                className="exegetes__link">
                {parseTextPunctuation(preaching.theme)}
              </Link>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default memo(CelebrationPreaching);
