import R from 'ramda';
import React, { PureComponent } from 'react';
import cx from 'classnames';
import VersePreacherLink from './VersePreacherLink';

import Preloader from '../../../components/Preloader/Preloader';

class CelebrationBible extends PureComponent {
  render() {
    const { preaching, currentTranslate, isPreaching } = this.props;

    const apostolLinks = R.pathOr('', ['excuse', 'apostolic'], preaching);
    const gospelLinks = R.pathOr('', ['excuse', 'gospel'], preaching);
    return (
      <div className="content-area-for-side preaching__verses celebration__about">
        {!isPreaching ? (
          <>
            {!!apostolLinks && (
              <div className="preaching__verses-group">
                <div className="preaching__verses-type">Апостол</div>
                <VersePreacherLink key="apostols" title="" list={apostolLinks} />
                <div className="preaching__verses-list">
                  {R.pathOr([], ['apostolic'], preaching).map(verse => (
                    <span key={verse.id} className="preaching__verses-item">
                      <sup>{verse.number}</sup>
                      <span
                        className={cx({
                          csya: currentTranslate && currentTranslate.code === 'csya_old',
                          greek: currentTranslate && currentTranslate.code === 'grek',
                        })}
                        dangerouslySetInnerHTML={{
                          __html: verse.text,
                        }}
                      />
                    </span>
                  ))}
                </div>
              </div>
            )}
            {!!gospelLinks && (
              <div className="preaching__verses-group">
                <div className="preaching__verses-type">Евангелие</div>
                <VersePreacherLink key="apostols" title="" list={gospelLinks} />
                <div className="preaching__verses-list">
                  {R.pathOr([], ['gospel'], preaching).map(verse => (
                    <span
                      key={verse.id}
                      className={cx('preaching__verses-item', {
                        // csya:
                        //   currentTranslate !== undefined && currentTranslate.code === 'csya_old',
                        greek: currentTranslate && currentTranslate.code === 'grek',
                      })}>
                      <sup>{verse.number}</sup>
                      <span
                        dangerouslySetInnerHTML={{
                          __html: verse.text,
                        }}
                      />
                    </span>
                  ))}
                </div>
              </div>
            )}
            {Object.values(preaching).length && (
              <>
                {!apostolLinks && !gospelLinks && (
                  <div className="preaching__verses-group">
                    <p>Нет проповедей</p>
                  </div>
                )}
              </>
            )}
          </>
        ) : (
          <Preloader />
        )}
      </div>
    );
  }
}

export default CelebrationBible;
