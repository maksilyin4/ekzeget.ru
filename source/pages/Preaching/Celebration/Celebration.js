import React, { useState, useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import Select from 'react-select';
import cx from 'classnames';

import preachingActions from '../../../dist/actions/Preaching';
import { getCurrentTranslate } from '../../../dist/selectors/translates';
import {
  getPreachingIsLoading,
  getPreaching,
  getPreachersNames,
} from '../../../dist/selectors/Preaching';
import { getIsDesktop } from '../../../dist/selectors/screen';
import List, { ListItem } from '../../../components/List/List';
import Tabs, { Tab, TabContent } from '../../../components/Tabs/Tabs';
import CelebrationBible from './CelebrationBible';
import CelebrationPreaching from './CelebrationPreaching';

import PreachingList from '../PreachingList';

const Celebration = ({
  match,
  history,
  celebrations,
  excuses,
  preachers,
  preaching,
  isDesktop,
  preachersSelect,
  currentTranslate,
  activeExcuse,
  basicUrl,
  getPreaching,
  preacherWeekState,
  isPreaching,
}) => {
  const {
    url,
    params: { excuseId, preacherWeekId, preacher_id },
  } = match;

  const obj = {
    excuseId,
    activeContent: preacherWeekId ? 2 : 1, // for CEO
    activePreacher: '',
  };
  const [state, setState] = useState(obj);

  useEffect(() => {
    if (preachers.length && preacherWeekId && !preacher_id) {
      history.push(`${basicUrl}${excuseId}/${preacherWeekId}/${preachers[0].code}/`);
    }
  }, [preachers]);

  useEffect(() => {
    if (preacher_id) getPreaching(`?preacher_id=${preacher_id}&excuse_id=${preacherWeekId}`);
  }, [preacherWeekId, preacher_id]);

  useEffect(() => {
    setState({
      ...state,
      activePreacher: preacher_id,
    });
  }, [preacher_id]);

  useEffect(() => {
    if (!preacherWeekId) {
      setState({ ...state, activeContent: preacherWeekId ? 2 : 1, activePreacher: '' });
    }
  }, [preacherWeekId]);

  const handleChange = field => value => {
    if (Number(value) === 2) {
      const preacher = preacher_id || preachers[0].code;

      const { id } = excuses.find(el => el.code === excuseId) || {};
      const preacherWeek =
        preacherWeekId || celebrations.find(el => el.parent.id === id)?.code || '';

      if (!preacher_id) {
        const urls = url.endsWith('/') ? url : `${url}/`;
        history.push(`${urls}${preacherWeek}/${preacher}/`);
      }
    }
    setState({ ...state, [field]: value });
  };

  const selectPreaching = preacher_id => {
    setState({ ...state, activeContent: 2, activePreacher: preacher_id });
  };

  const changePreachingMobile = e => {
    if (e.label === 'Проповедники') {
      history.push(`${basicUrl}${excuseId}/`);
    } else {
      history.push(`${basicUrl}${excuseId}/${preacherWeekState}/${e.value}/`);
    }
    selectPreaching(e.value);
  };

  const { activePreacher } = state;

  return (
    <>
      <div className="all-about-bible-content paper content-item">
        <div className="content_two-col preaching_content_two-col all_about_bible">
          {preaching.excuse && (
            <Helmet>
              <title>
                {`${preaching.excuse.title} - чтение проповеди онлайн на портале Экзегет`}
              </title>
              <meta
                name="description"
                content={`Библейская проповедь ${preaching.excuse.title}. Читать онлайн христианские проповеди на портале Экзегет.`}
              />
            </Helmet>
          )}
          <Tabs
            activeTab={state.activeContent}
            callBack={handleChange('activeContent')}
            className="tabs_in-paper tabs_two-col-content">
            <Tab label="Библия" value={1} className="tab_content" />
            <Tab label="Проповеди" value={2} className="tab_content" />
            <TabContent>
              <div className="content_two-col celebration">
                {state.activeContent === 1 && (
                  <>
                    {!preacher_id ? (
                      <PreachingList
                        celebrations={celebrations}
                        excuses={excuses}
                        excuse_id={activeExcuse}
                        basicUrl={basicUrl}
                        preachers={preachers}
                      />
                    ) : (
                      <CelebrationBible
                        preaching={preaching}
                        currentTranslate={currentTranslate}
                        isPreaching={isPreaching}
                      />
                    )}
                  </>
                )}
                {state.activeContent === 2 && (
                  <CelebrationPreaching
                    preaching={preaching}
                    excuseId={state.excuseId}
                    activePreacher={state.activePreacher}
                    match={match}
                    basicUrl={basicUrl}
                    isPreaching={isPreaching}
                  />
                )}
              </div>
            </TabContent>
          </Tabs>
          <div className="side side_right celebration__right cerebration__rigth__menu all-about-bible-rigth-select">
            <List className="side__interpretators all-about-bible-right-nav">
              {!isDesktop ? (
                <Select
                  isSearchable={false}
                  options={preachersSelect}
                  defaultValue={preachersSelect[0]}
                  value={
                    activePreacher
                      ? preachersSelect.find(el => el.value === activePreacher)
                      : preachersSelect[0]
                  }
                  onChange={changePreachingMobile}
                  classNamePrefix="custom-select"
                />
              ) : (
                <>
                  <div className="side__interpretators-head cerebration__head">Проповедники</div>
                  {preachers.map((preacher, i) => (
                    <ListItem
                      key={preacher.id}
                      title={preacher.name}
                      linkClassName={'list_item_font_size'}
                      href={`${basicUrl}${excuseId}/${preacherWeekId || preacherWeekState}/${
                        preacher.code
                      }/`}
                      className={cx({
                        active: !state.activePreacher
                          ? Number(i) === 0
                          : state.activePreacher === preacher.code,
                      })}
                      onClick={() => selectPreaching(preacher.code)}
                    />
                  ))}
                </>
              )}
            </List>
          </div>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = state => ({
  preachingIsLoading: getPreachingIsLoading(state),
  isPreaching: state.preaching.isPreaching,
  preaching: getPreaching(state),
  preachersSelect: getPreachersNames(state),
  isDesktop: getIsDesktop(state),
  currentTranslate: getCurrentTranslate(state),
});
const mapDispatchToProps = {
  ...preachingActions,
};

Celebration.propTypes = {
  isDesktop: PropTypes.bool.isRequired,
  preaching: PropTypes.object,
  preachers: PropTypes.array,
  history: PropTypes.object,
  currentTranslate: PropTypes.object,
  preachersSelect: PropTypes.object,
  excuses: PropTypes.array,
  basicUrl: PropTypes.string,
  celebrations: PropTypes.array,
  preacherWeekState: PropTypes.string,
  activeExcuse: PropTypes.string,
  getPreaching: PropTypes.func,
  match: PropTypes.shape({
    url: PropTypes.string,
    params: PropTypes.shape({
      excuseId: PropTypes.string,
      preacherId: PropTypes.string,
      preacher_id: PropTypes.string,
      preacherWeekId: PropTypes.string,
    }),
  }),
};
export default connect(mapStateToProps, mapDispatchToProps)(memo(Celebration));
