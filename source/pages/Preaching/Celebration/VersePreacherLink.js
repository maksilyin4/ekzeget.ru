import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import parseBookLink from '../../../dist/books_short';

class VersePreacherLink extends PureComponent {
  render() {
    const { list, title } = this.props;

    return (
      <div className="verse__preacher-link">
        <span>{title}</span>
        {parseBookLink(list).map(link => (
          <Link
            key={link.bookCode + link.chapter + link.verses}
            to={`/bible/${link.bookCode}/glava-${link.chapter}/?verse=${link.verses}`}
            className="preaching__verses-short">
            {link.title}
          </Link>
        ))}
      </div>
    );
  }
}

export default VersePreacherLink;
