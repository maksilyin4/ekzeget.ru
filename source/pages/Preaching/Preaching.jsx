import React, { useEffect, useMemo, memo, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { Switch, Route } from 'react-router-dom';

import './preaching.scss';

import preachingActions from '../../dist/actions/Preaching';
import { getMetaData } from '../../store/metaData/actions';
import {
  getPreachingExcuse,
  getPreachingCelebration,
  getPreacherTypes,
  getPreacher,
  getPreaching,
  getPreachingIsError,
} from '../../dist/selectors/Preaching';
import { setMetaData } from '../../store/metaData/selector';

import Preloader from '../../components/Preloader/Preloader';
import Celebration from './Celebration/Celebration';
import PreachingNav from './PreachingNav';
import NotFound from '../NotFound/NotFound';

import { smoothScrollTo } from '../../dist/utils';
import { setPreacherId, fetchData } from './preachingUtils';
import { getIsDesktop } from '../../dist/selectors/screen';
import { parsePathname } from '../../utils/parsePathname';
import Helmet from '~components/Helmet';

const Preaching = ({
  location: { pathname, state: locationState },
  excuses,
  preachers,
  excusesSelect,
  celebrations = [],
  metaData,
  match: { path },

  history,
  getPreacher,
  getPreachingExcuse,
  getPreachingCelebration,
  getMetaData,
  getTitle,
  isDesktop,
  isError,
  getClearPreachingIsError,
  setPreachingIsError,
}) => {
  const [isLoading, setIsLoading] = useState(false);
  const [isErrorPage, setIsErrorPage] = useState(false);
  useEffect(() => {
    smoothScrollTo();

    // for 404
    if (!isError) {
      fetchData({
        getPreachingExcuse,
        getPreachingCelebration,
        setIsLoading,
        excuses,
        celebrations,
      });
    } else {
      setIsErrorPage(isError);
    }
  }, [isError]);

  const excuse_id = parsePathname({ pathname, index: 2 });

  const preacherId = useMemo(
    () =>
      celebrations.length > 0 &&
      setPreacherId({ pathname, locationState, celebrations, excuses, excuse_id }),
    [celebrations, pathname, excuse_id],
  );

  useEffect(() => {
    if (!excuse_id && excuses.length) {
      history.replace(`${path}${excuses[0].code}/`);
    }
  }, [excuses, pathname]);

  // for SEO 404
  useEffect(() => {
    if (!preacherId && isError) {
      setPreachingIsError();
    }
  }, [preacherId]);

  useEffect(() => {
    if (excuses.length && preacherId) {
      getPreacher(`?excuse_id=${preacherId}`);
    }
  }, [excuse_id, preacherId]);

  useEffect(() => {
    const title = metaData?.h_one ?? 'Проповеди';
    getTitle(title);
  }, [metaData?.h_one]);

  useEffect(() => {
    if (excuse_id) {
      getMetaData(pathname);
    }

    if (isError && isErrorPage) {
      getClearPreachingIsError();
    }
  }, [pathname, excuse_id]);

  const handleChange = value => () => {
    history.replace(`${path}${value}/`);
  };

  const handleSelect = e => {
    history.replace(`${path}${e.value}/`);
  };

  if (isError) {
    return <NotFound />;
  }

  if (isLoading) {
    return <Preloader />;
  }

  return (
    <div className="page preaching-page">
      <div className="page__body">
        <div className="wrapper wrapper_two-col preaching all_about_bible">
          <Helmet
            title={
              // metaData?.title ||
              'Проповеди - читать православные проповеди онлайн на портале Экзегет'
            }
            description={
              metaData?.description ||
              'Христианские проповеди в текстовом варианте. Читать библейские проповеди онлайн на сайте Экзегет.'
            }
          />

          <PreachingNav
            isDesktop={isDesktop}
            excuses={excuses}
            excusesSelect={excusesSelect}
            preachers={preachers}
            activeExcuse={excuse_id}
            onChange={handleChange}
            onChangeSelect={handleSelect}
            celebrations={celebrations}
            url={path}
          />
          <Switch>
            <Route
              exact
              path={[
                `${path}`,
                `${path}:excuseId`,
                `${path}:excuseId/:preacherWeekId`,
                `${path}:excuseId/:preacherWeekId/:preacher_id`,
                `${path}:excuseId/:preacherWeekId/:preacher_id/:id`,
              ]}
              render={props =>
                excuses.length ? (
                  <Celebration
                    activeExcuse={excuse_id}
                    basicUrl={path}
                    preacherWeekState={preacherId}
                    preachers={preachers}
                    excuses={excuses}
                    celebrations={celebrations}
                    {...props}
                  />
                ) : (
                  <Preloader />
                )
              }
            />
            <Route component={NotFound} />
          </Switch>
        </div>
      </div>
    </div>
  );
};

Preaching.propTypes = {
  excuses: PropTypes.array,
  preachers: PropTypes.array,
  excusesSelect: PropTypes.array,
  celebrations: PropTypes.array,
  getPreachingExcuse: PropTypes.func,
  getPreachingCelebration: PropTypes.func,
  getMetaData: PropTypes.func,
  getPreacher: PropTypes.func,
  getTitle: PropTypes.func,
  history: PropTypes.object,
  metaData: PropTypes.object,
  match: PropTypes.shape({
    url: PropTypes.string,
  }),
  location: PropTypes.shape({
    pathname: PropTypes.string,
    state: PropTypes.shape({
      preacherWeek: PropTypes.string,
    }),
  }),
};

const mapStateToProps = state => ({
  preaching: getPreaching(state),
  preachers: getPreacher(state),
  excuses: getPreachingExcuse(state),
  celebrations: getPreachingCelebration(state),
  excusesSelect: getPreacherTypes(state),
  metaData: setMetaData(state),
  isDesktop: getIsDesktop(state),
  isError: getPreachingIsError(state),
});
const mapDispatchToProps = {
  ...preachingActions,
  getMetaData,
};

export default connect(mapStateToProps, mapDispatchToProps)(memo(Preaching));
