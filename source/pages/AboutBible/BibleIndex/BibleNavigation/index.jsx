import React, { memo, useCallback } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { NavLink } from 'react-router-dom';

import { onMouseEnter, onMouseLeave, onMouseObj } from '../../../../components/PreloadChunk';

import './style.scss';

function getRouters(path) {
  return [
    {
      path: `/${path}/about-bible/`,
      title: 'О Библии',
      getMouseObj: onMouseObj.onMouseLecture,
    },
    {
      path: `/${path}/ekzegets/`,
      title: 'Экзегеты',
      getMouseObj: onMouseObj.onMouseExegetes,
    },
    {
      path: `/${path}/propovedi/`,
      title: 'Проповеди',
      getMouseObj: onMouseObj.onMousePreaching,
    },
    {
      path: `/${path}/dictionaries/`,
      title: 'Словари',
      getMouseObj: onMouseObj.onMouseDictonaries,
    },
    {
      path: `/${path}/bibleyskaya-karta/`,
      title: 'Библейские карты',
      getMouseObj: onMouseObj.onMouseBiblemaps,
    },
  ];
}

const Navigation = ({ path, isAdaptive }) => {
  const getMouseObjFunc = obj => () => onMouseEnter(obj);

  const getOnMouseEnter = useCallback(getMouseObjFunc, []);

  return (
    <div
      className={cx('navigation', {
        'navigation-adaptive': isAdaptive,
      })}>
      {getRouters(path).map(({ path, title, getMouseObj }) => (
        <NavLink
          key={path}
          to={path}
          className="navigation__item"
          activeClassName="active"
          title={title}
          onMouseEnter={getOnMouseEnter(getMouseObj)}
          onMouseLeave={onMouseLeave}
          onFocus={getMouseObj}>
          {title}
        </NavLink>
      ))}
    </div>
  );
};

Navigation.propTypes = {
  path: PropTypes.string,
  isAdaptive: PropTypes.bool,
};

export default memo(Navigation);
