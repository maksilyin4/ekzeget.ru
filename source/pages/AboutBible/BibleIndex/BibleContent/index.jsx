import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';

import Exegetes from '../../../../pages/Exegetes';
import Lecture from '../../../../pages/Lecture';
import BibleMap from '../../../../pages/BibleMap/BibleMapLoadable';
import Preaching from '../../../../pages/Preaching';
import Dictionaries from '../../../../pages/Dictonaries';
import NotFound from '../../../NotFound/NotFound';

const setRouters = path => [
  {
    to: `/${path}/about-bible/`,
    C: Lecture,
  },
  {
    to: `/${path}/ekzegets/`,
    C: Exegetes,
  },
  {
    to: `/${path}/bibleyskaya-karta/`,
    C: BibleMap,
  },
  {
    to: `/${path}/propovedi/`,
    C: Preaching,
  },
  {
    to: `/${path}/dictionaries/`,
    C: Dictionaries,
  },
];

const BiblioContent = ({ path, getTitle }) => {
  return (
    <Switch>
      {setRouters(path).map(({ to, C }, i) => (
        <Route key={i} path={to} render={props => <C getTitle={getTitle} {...props} />} />
      ))}
      <Route components={NotFound} />
    </Switch>
  );
};

BiblioContent.propTypes = {
  path: PropTypes.string,
  getTitle: PropTypes.func,
};

export default BiblioContent;
