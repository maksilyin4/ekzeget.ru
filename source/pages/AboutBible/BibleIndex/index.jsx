import React, { useState, useMemo, memo } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import './style.scss';

import { getIsMobile, getIsAdaptive } from '../../../dist/selectors/screen';
import { setMetaData } from '../../../store/metaData/selector';

import PageHead from '~components/PageHead';
import BibleNavigation from './BibleNavigation';
import BibleContent from './BibleContent';
import LearnVideo from '~components/LearnVideo/LearnVideo';
import BreadHead from '~components/BreadHead';

const bread = [
  { path: '/', title: 'Главная' },
  { path: '', title: 'Все о Библии' },
];

const BiblioIndex = ({ match: { url } }) => {
  const metaData = useSelector(setMetaData);

  const isPhone = useSelector(getIsMobile);
  const isAdaptive = useSelector(getIsAdaptive);

  const urlBasic = useMemo(
    () =>
      url.split('/').filter(el => {
        return el.length;
      })[0],
    [],
  );

  const [title, setTitle] = useState('');

  function getTitle(titlePage) {
    if (titlePage) {
      setTitle(titlePage);
    }
  }

  const titlePage = metaData?.h_one || title;

  return (
    <div className="page">
      <div className="wrapper">
        <BreadHead bread={bread} query="bible_tutorial_video_url" isPhone={isPhone} />
        <div className="about_bible_h_one">
          <PageHead h_one={titlePage} />
          {isPhone && <LearnVideo query="bible_tutorial_video_url" />}
        </div>

        <BibleNavigation path={urlBasic} isAdaptive={isAdaptive} />
        <BibleContent path={urlBasic} getTitle={getTitle} />
      </div>
    </div>
  );
};

BiblioIndex.propTypes = {
  match: PropTypes.object,
};

export default memo(BiblioIndex);
