import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import qs from 'qs';
import { userRegIsLoading, userLoggedIn, getUserError } from '../../dist/selectors/User';
import { regConfirm } from '../../dist/actions/User';
import Preloader from '../../components/Preloader/Preloader';

import './styles.scss';

class ConfirmReg extends Component {
  componentDidMount() {
    const {
      isUserLoggin,
      regConfirm,
      match: { params },
    } = this.props;
    if (params.hasOwnProperty('token') && !!params.token && !isUserLoggin) {
      regConfirm(qs.stringify({ secret: params.token }));
    }
  }

  render() {
    const { isUserRegLoading, userError } = this.props;
    return (
      <div className="page">
        <div className="wrapper">
          {isUserRegLoading ? (
            <Preloader />
          ) : !userError ? (
            <div className="confirm_reg">
              <div className="confirm_reg__title">Ваш Email подтверждён</div>
              <div className="confirm_reg__text">
                Теперь Вам доступен расширенный функционал работы с сайтом.
                <br />
                Вы можете добавлять стихи, толкования в избранное.
                <br />
                Создавать заметки и закладки на главу или стих.
                <br />
                Добавлять толкования на выбранный стих.
              </div>
              <div className="confirm_reg__links">
                <Link to="/">На главную</Link>
                <Link to="/lk/">Перейти в Личный Кабинет</Link>
              </div>
            </div>
          ) : (
            <div className="confirm_reg">
              <div className="confirm_reg__title">Ваш Email не подтверждён</div>
              <div className="confirm_reg__text">{userError}</div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isUserRegLoading: userRegIsLoading(state),
  isUserLoggin: userLoggedIn(state),
  userError: getUserError(state),
});

const mapDispatchToProps = {
  regConfirm,
};

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmReg);
