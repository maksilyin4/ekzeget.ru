import React, { useMemo } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import cx from 'classnames';

import QuizTestaments from '../../components/QuizTestaments';
import QuizQuestions from '../../components/QuizQuestions/QuizQuestionsLoadable';
import QuizResults from '../../components/QuizResults/QuizResultsLoadable';
import QuizHeader from '../../components/QuizHeader';
import QuizAllQuestions from '../../components/QuizAllQuestions';

import './styles.scss';

const Quiz = ({ location: { state = {}, pathname } }) =>
  useMemo(
    () => (
      <div className="page quiz-page">
        <QuizHeader locationState={state} pathname={pathname} />
        <div className="page__body">
          <div
            className={cx('wrapper wrapper_two-col quiz__wrapper', {
              'quiz__wrapper-questions': pathname.includes('voprosi-'),
            })}>
            <div className="content">
              <Switch>
                <Route exact path="/bibleyskaya-viktorina/" component={QuizTestaments} />
                <Route
                  exact
                  path="/bibleyskaya-viktorina/voprosi/:questionId/"
                  component={QuizQuestions}
                />
                <Route
                  exact
                  path="/bibleyskaya-viktorina/voprosi/day-:day/:plan_id/:questionId/"
                  component={QuizQuestions}
                />
                <Route exact path="/bibleyskaya-viktorina/resultati/" component={QuizResults} />
                <Route
                  exact
                  path="/bibleyskaya-viktorina/voprosi-:id/"
                  component={QuizAllQuestions}
                />
                <Redirect to="/bibleyskaya-viktorina/" />
              </Switch>
            </div>
          </div>
        </div>
      </div>
    ),
    [pathname, state],
  );

export default Quiz;
