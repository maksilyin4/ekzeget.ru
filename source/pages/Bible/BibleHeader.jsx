import React from 'react';
import PropTypes from 'prop-types';

import BreadHead from '~components/BreadHead';
import PageHead from '~components/PageHead/';

const BibleHeader = ({ title = '', breadcrumbs, subTitle = '', isSubTitle }) => {
  return (
    <div className="wrapper">
      <BreadHead bread={breadcrumbs} />
      <PageHead h_one={title} subTitle={subTitle} isSubTitle={isSubTitle} />
    </div>
  );
};

BibleHeader.propTypes = {
  title: PropTypes.string,
  subTitle: PropTypes.string,
  breadcrumbs: PropTypes.array,
  isSubTitle: PropTypes.bool,
};
export default BibleHeader;
