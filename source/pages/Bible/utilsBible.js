import R from 'ramda';

export const getHref = ({ excuse, preacher }) => {
  const parentExcuseCode = excuse.parent && excuse.parent.code ? excuse.parent.code : '';
  const excuseCode = excuse.code || '';
  const preacherCode = preacher.code || '';
  let href = '/all-about-bible/propovedi/';
  if (parentExcuseCode && excuseCode && preacherCode) {
    href = `/all-about-bible/propovedi/${parentExcuseCode}/${excuseCode}/${preacherCode}/`;
  }
  return href;
};

export const parsePreaching = preachingForVerse => {
  const preaching = R.pathOr([], ['gp'], preachingForVerse);
  const preachingByPage = R.pathOr({}, ['gbs'], preachingForVerse);
  return { preaching, preachingByPage };
};

export const getIsShowContent = (cookies, isPhone) => {
  if (isPhone) {
    const category = cookies.get('bibleNavParent') || {};
    const { type } = category;
    return type === 'bibleText' || type === 'interpretations';
  }
  return !isPhone;
};

export const setIsShowMobileInterpretations = (cookies, isPhone) => {
  if (isPhone) {
    const category = cookies.get('bibleNavParent') || {};
    const { type } = category;
    return type !== 'bibleText';
  }
  return false;
};
