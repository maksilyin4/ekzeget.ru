import React, { memo } from 'react';
import cx from 'classnames';
import { useSelector } from 'react-redux';

import { getBookIsLoading } from '~dist/selectors/Book';

import BiblePreloader from '~components/Bible/BiblePreloader';
import List, { ListItem } from '~components/List/List';
import BookAudio from './BookAudio';

const BookChapters = ({
  book,
  userLoggedIn,
  getIsLoading,
  bookmarks,
  screen,
  declOfNumStr,
  nextId,
  callBack,
}) => {
  const isBookIsLoading = useSelector(getBookIsLoading);
  if (isBookIsLoading) {
    return <BiblePreloader />;
  }

  return (
    <List>
      {book.chapters.map(
        ({ id, number, title, verse_count, audio }, index) =>
          audio && (
            <ListItem
              key={id}
              className={cx({
                activeBookmark:
                  userLoggedIn && !getIsLoading && bookmarks.bookmark[`${book.short_title}. ${id}`],
                list__item_column: screen.phone,
              })}
              title={
                <div>
                  <span>{`${book.title}. ${title} `}</span>
                  <span style={{ color: '#777777' }}>
                    {`(${declOfNumStr(verse_count, ['стих', 'стиха', 'стихов'])})`}
                  </span>
                </div>
              }
              href={`/bible/${book.code}/glava-${number}/`}
              actions={
                <BookAudio
                  audio={audio}
                  id={id}
                  book={book}
                  number={number}
                  callBack={callBack}
                  nextId={nextId}
                  userLoggedIn={userLoggedIn}
                  getIsLoading={getIsLoading}
                  bookmarks={bookmarks}
                  index={index}
                />
              }
            />
          ),
      )}
    </List>
  );
};

export default memo(BookChapters);
