import React, { memo, useMemo } from 'react';

import AudioPlayer from '../../../../../components/AudioPlayer/AudioPlayer';
import AddBookmark from '../../../../../components/BibleBookmarks/AddBookmarks/AddBookmark';
import { GetBookmark } from '../../../../../components/BibleBookmarks/GetBookmarks/GetBookmark';
import { bibleCDN } from '~utils/common';

const BookAudio = ({
  audio,
  // id,
  book,
  number,
  callBack,
  nextId,
  userLoggedIn,
  getIsLoading,
  bookmarks,
  index,
}) => {
  const audioSrc = useMemo(
    () =>
      audio
        ? audio.src
        : `${bibleCDN}/st_text/${book.legacy_code}/${book.legacy_code}-${number}-st_text.mp3`,
    [audio.src, book.legacy_code],
  );

  const short = `${book.short_title || ''}. ${number}`;

  return (
    <div className="book__chapter-actions">
      <AudioPlayer
        id={+audio && audio.id}
        src={audioSrc}
        srcToDownload={audioSrc}
        callBack={callBack}
        autoPlay={audio ? nextId === +audio.chapter_id : 0}
        bookNext={book.chapters[index + 1]}
      />

      {userLoggedIn && !getIsLoading && (
        <div className="book__add-favorite">
          {!bookmarks.bookmark[short] ? (
            <AddBookmark
              className="book__chapter-action book__chapter-action_bookmark"
              short={short}
              type="circle"
            />
          ) : (
            <GetBookmark
              className="book__chapter-action book__chapter-action_bookmark active-bible-bookmark"
              short={short}
              type="circle"
            />
          )}
        </div>
      )}
    </div>
  );
};

export default memo(BookAudio);
