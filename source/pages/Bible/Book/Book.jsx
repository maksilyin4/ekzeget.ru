import R from 'ramda';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
// import { Helmet } from 'react-helmet';

// store
import bookActions from '../../../dist/actions/Book';
import { bookmarks, getIsLoading } from '../../../dist/selectors/Bookmarks';
import { getBook, getBookInfo } from '../../../dist/selectors/Book';
import { userLoggedIn } from '~dist/selectors/User';
import { declOfNumStr, smoothScrollTo } from '../../../dist/utils';
import getScreen from '../../../dist/selectors/screen';
import { getCurrentTranslate } from '../../../dist/selectors/translates';

// components
import NotFound from '../../NotFound/NotFound';
import Tabs, { Tab, TabContent } from '../../../components/Tabs/Tabs';
import BibleMenuBooksLeft from '~components/Bible/BibleMenuBooksLeft';
import BookChapters from './BookChapters';
import BookInfo from './BookInfo';
import BibleActionPanel from '../../../components/BibleActionPanel/';
import BibleHeader from '../BibleHeader';

import './book.scss';
import '../bible.scss';

class Book extends Component {
  static propTypes = {
    location: PropTypes.object,
    book: PropTypes.object,
  };

  constructor(props) {
    super(props);

    this.state = {
      bookCode: this.props.location.pathname.split('/')[2],
      activeContent: 1,
      getParams:
        (this.props.location.state && this.props.location.state.stateId) ||
        this.props.match.params.chapter_num,
      nextId: '',
    };
  }

  async componentDidMount() {
    const { book, getBook, getBookInfo, bookInfo } = this.props;

    const { getParams, bookCode } = this.state;

    smoothScrollTo();

    if (getParams) this.setState({ activeContent: 2 });

    if (!book.chapters.length || bookCode !== R.pathOr('', ['book', 'code'], this.props)) {
      await getBook(bookCode);
    }

    if (!bookInfo || !bookInfo.length) {
      getBookInfo(bookCode);
    }

    if (this.props.book) {
      const { cookies } = this.props;
      cookies.set('bookTitle', this.props.book.title, { path: '/' });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      location: { pathname, state },
      location,
      book,
    } = this.props;

    if (state !== prevProps.location.state || pathname !== prevProps.location.pathname) {
      this.setState({
        bookCode: location.pathname.split('/')[2],
      });

      smoothScrollTo();
    }

    if (prevState.bookCode !== this.state.bookCode) {
      this.props.getBook(this.state.bookCode);
      this.props.getBookInfo(this.state.bookCode);
    }
    if (prevProps.book !== book) {
      const { cookies } = this.props;
      cookies.set('bookTitle', this.props.book.title, { path: '/' });
    }
  }

  handleChange = field => value => {
    this.setState({ [field]: value });
  };

  callBack = value => {
    if (value) {
      this.setState({ nextId: Number(value) });
    }
  };

  breadItems = title => [
    { path: '/', title: 'Главная' },
    { path: '', title },
  ];

  render() {
    const {
      book,
      bookInfo,
      getIsLoading,
      bookmarks,
      userLoggedIn,
      screen,
      isBook,
      location: { pathname, state },
    } = this.props;

    let verses_count = 0;

    if (book !== null) {
      verses_count = book.chapters.reduce((sum, item) => Number(sum) + Number(item.verse_count), 0);
    }

    return (
      <div className="page bible-page book-page">
        {book === null ? (
          <NotFound />
        ) : (
          <>
            {/* {Object.keys(book).length > 1 && ( */}
            {/*  <Helmet> */}
            {/*    <title> */}
            {/*      {`${book.title || 'Библия'} - читать толкование ${ */}
            {/*        +book.testament.id === 2 ? 'Нового завета' : 'Ветхого завета' */}
            {/*      } онлайн на портале Экзегет.ру`} */}
            {/*    </title> */}
            {/*    <meta */}
            {/*      name="description" */}
            {/*      content={`Толкование ${book.title || 'Библия'} на сайте Экзегет. */}
            {/*      Читать онлайн главы Библии ${ */}
            {/*        +book.testament.id === 2 ? 'Нового завета' : 'Ветхого завета' */}
            {/*      } в разных переводах`} */}
            {/*    /> */}
            {/*  </Helmet> */}
            {/* )} */}
            <BibleHeader
              title={book.title}
              breadcrumbs={this.breadItems(book.title)}
              screen={screen}
            />
            <div className="wrapper">
              <BibleActionPanel
                bookCode={book.code}
                bookTitle={book.menu}
                navText="Глава"
                pagesCount={Number(book.parts)}
                currentPage={null}
              />
            </div>

            <div className="inner-page__body book">
              <div className="wrapper wrapper_two-col">
                {(screen.desktop || screen.tabletMedium) && (
                  <BibleMenuBooksLeft
                    activeTestament={state?.bookTestamentId || book?.testament_id}
                    activeBook={book.code}
                    isDesktop={screen.desktop}
                  />
                )}
                <div className="content paper">
                  <div className="content__head">
                    <Tabs
                      activeTab={this.state.activeContent}
                      callBack={this.handleChange('activeContent')}
                      className="tabs_in-paper book-tabs">
                      <Tab label="Содержание" value={1} />
                      <Tab label="Сведения о книге" value={2} />
                    </Tabs>
                    {!screen.phone && (
                      <p className="content__head-actions">
                        {`В книге ${book.chapters.length} глав, ${declOfNumStr(verses_count, [
                          'стих',
                          'стиха',
                          'стихов',
                        ])}`}
                      </p>
                    )}
                  </div>
                  <TabContent>
                    <div className="bible-book-content">
                      {this.state.activeContent === 1 && (
                        <BookChapters
                          book={book}
                          userLoggedIn={userLoggedIn}
                          getIsLoading={getIsLoading}
                          bookmarks={bookmarks}
                          screen={screen}
                          declOfNumStr={declOfNumStr}
                          nextId={this.state.nextId}
                          callBack={this.callBack}
                        />
                      )}
                      {this.state.activeContent === 2 && Array.isArray(bookInfo) && (
                        <BookInfo
                          isBook={isBook}
                          bookInfo={bookInfo}
                          bookCode={book.code}
                          pathname={pathname}
                        />
                      )}
                    </div>
                  </TabContent>
                </div>
              </div>
            </div>
          </>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  book: getBook(state),
  bookInfo: getBookInfo(state),
  userLoggedIn: userLoggedIn(state),
  getIsLoading: getIsLoading(state),
  bookmarks: bookmarks(state),
  screen: getScreen(state),
  currentTranslate: getCurrentTranslate(state),
});
const mapDispatchToProps = {
  ...bookActions,
};

export default withCookies(connect(mapStateToProps, mapDispatchToProps)(Book));
