import React, { memo } from 'react';

import List, { ListItem } from '../../../../components/List/List';
import Button from '../../../../components/Button/Button';

const BookInfo = ({ isBook, bookInfo, bookCode, pathname }) => (
  <>
    {isBook ? (
      <div className="bible-tabs__content content-area-for-side">
        {/* eslint-disable-next-line array-callback-return */}
        {bookInfo.map(({ descriptor: { title, code }, id, text }) => {
          if (pathname.includes(code)) {
            return (
              <div className="bible__info" key={id}>
                <div className="bible__info-head">
                  <h2 className="bible__info-preacher">{title}</h2>
                  <Button
                    href={`/bible/${bookCode}/`}
                    title="Вернуться к списку сведений"
                    size="small"
                  />
                </div>
                <div
                  className="bible__info-preaching-text"
                  dangerouslySetInnerHTML={{
                    __html: text,
                  }}
                />
              </div>
            );
          }
        })}
      </div>
    ) : (
      <List>
        {bookInfo.map(({ id, descriptor: { title, code } }) => (
          <ListItem key={id} title={title} href={`/bible/${bookCode}/book-${code}`} />
        ))}
      </List>
    )}
  </>
);

export default memo(BookInfo);
