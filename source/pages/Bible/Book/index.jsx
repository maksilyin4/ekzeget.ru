import React from 'react';
import Loadable from 'react-loadable';
import Preloader from '../../../components/Preloader/Preloader';

const LoadableBar = Loadable({
  loader: () => import('./Book'),
  loading() {
    return (
      <div className="preloader-relative">
        <Preloader />
      </div>
    );
  },
  delay: 200,
});

export default LoadableBar;
