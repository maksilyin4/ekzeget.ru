import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import cx from 'classnames';

import { parseVerseSelect } from '../../../dist/utils';

import ChapterVerseParallel from './ChapterExtraContent/ChapterVerseParallel';
import ChapterVerseExtraInfo from './ChapterExtraContent/ChapterVerseExtraInfo';

import Icon from '../../../components/Icons/Icons';

export default class VerseItem extends PureComponent {
  state = {
    isOpen: false,
    verseId: null,
  };

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  setContainerRef = node => {
    this.containerRef = node;
  };

  handleClickOutside = event => {
    if (this.containerRef && !this.containerRef.contains(event.target)) {
      this.setState({ isOpen: false });
    }
  };

  toggleParallel = id => {
    const { verseId } = this.state;

    if (verseId === id) {
      this.setState(prevState => ({ isOpen: !prevState.isOpen }));
    } else {
      this.setState({ isOpen: true, verseId: id });
    }
  };

  render() {
    const {
      verseInline,
      verse,
      code,
      showParallel,
      currentTranslate,
      selected,
      isCheckboxVisible,
      selectableKey,
      inFavList,
      userLoggedIn,
      screen,
      localToken,
      bold,
      showMapPoints,
      showDictionaries,
      handleClickItem,
    } = this.props;

    const { isOpen, verseId } = this.state;
    let fontWeight = '';
    if (bold) {
      parseVerseSelect(bold).map(el => {
        if (verse.number >= el.start && verse.number <= el.end) {
          fontWeight = 'bold';
        }
      });
    }
    const classes = {
      csya: currentTranslate === 'csya_old',
      greek: currentTranslate === 'grek',
    };

    return (
      <div
        className={cx('chapter__verse not-translated', { 'chapter-verse-padding': userLoggedIn })}
        id={verse.number}>
        {!verseInline && (
          <button
            onClick={handleClickItem(selectableKey, {
              verseText: verse.text,
              verseNumber: verse.number,
            })}
            className={cx('chapter__verse__checkbox', {
              selectedVerse: selected,
              selectingVerse: selected,
              verseVisible: isCheckboxVisible,
            })}>
            {selected ? <Icon icon="checkboxFavorite" /> : null}
          </button>
        )}
        <Link
          className={cx('not-selectable', {
            checkbox__visible: !verseInline,
            checkbox__hidden: verseInline || !localToken,
          })}
          to={`/bible/${code}/glava-${verse.chapter}/stih-${verse.number}/`}>
          <sub style={{ backgroundColor: inFavList || '' }}>{verse.number}</sub>
          <span
            key={verse.id}
            className={cx(classes, 'chapter__verse-description')}
            style={{ backgroundColor: inFavList || '', fontWeight }}
            dangerouslySetInnerHTML={{ __html: verse.text }}
          />
        </Link>
        {showParallel && verse.parallels !== null && (
          <ChapterVerseParallel
            verseInline={verseInline}
            localToken={localToken}
            verse={verse}
            isOpen={isOpen}
            toggleParallel={this.toggleParallel}
            verseId={verseId}
            classes={classes}
            setContainerRef={this.setContainerRef}
            screen={screen}
            icon="list"
          />
        )}
        {showMapPoints && verse.bibleMapsPoints.length > 0 && (
          <ChapterVerseExtraInfo extraInfo={verse.bibleMapsPoints} icon="map" />
        )}

        {showDictionaries && verse.dictionaries.length > 0 && (
          <ChapterVerseExtraInfo extraInfo={verse.dictionaries} icon="dictionary" />
        )}
      </div>
    );
  }
}
