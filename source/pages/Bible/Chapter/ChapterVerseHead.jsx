import React, { useEffect, useState, useMemo } from 'react';

import ChapterVerseAction from '~components/Bible/BibleManuscriptAction';
import AudioPlayer from '../../../components/AudioPlayer/AudioPlayer';
import isFileExist from './serviceAudioApi';
import ChapterTabActions from './ChapterTabActions';
import ShareButton from '../../../components/ShareButton';
import MediaDetailDownload from '~pages/Mediateka/MediaBody/MediaDetailContent/MediaDetailMultiContent/MediaDetailMultiBody/MediaDetailMultiAudio/MediaDetailDownload';

import { getIsMap, setAudioSrc } from './utilsChapter';
import { bibleCDN } from '~utils/common';

const ChapterVerseHead = props => {
  const {
    parallelToggle,
    audioPlayerToggle,
    mapPointsToggle,
    showManuscript,
    showPlayer,
    showDictionaries,
    chapter,
    showParallel,
    // TODO пока не понятно нужно будет или нет 10.12.2019
    // screen,
    // openMenu,
    showMapPoints,
    addFavorite,
    addNote,
    short,
    deleteFavoriteChapter,
    isNoteAdd,
    selectedItems,
    dictionariesVerseToggle,
    favoriteVerse,
    favoritesVerse,
    updateFavVerse,
    editFavorite,
    arrayOfIds,
    currentTranslate,
    verseTextForShare,
    verseInfo,
  } = props;

  const [isExist, setIsExist] = useState(false);

  const [pathToAudio, setPathToAudio] = useState('');

  // TODO пока не понятно нужно будет или нет 10.12.2019
  // const [showMore, setShowMore] = useState(false);

  useEffect(() => {
    let isSubscribed = true;

    const audioTranslate = currentTranslate?.code || 'st_text';

    const audioSrc = setAudioSrc({ audioTranslate, chapter });

    isFileExist(audioSrc).then(res => {
      if (isSubscribed) {
        setIsExist(res);
        setPathToAudio(audioSrc);
      }
    });

    return () => {
      isSubscribed = false;
    };
  }, [currentTranslate, chapter]);

  // TODO пока не понятно нужно будет или нет 10.12.2019
  // const _showMoreToggle = () => {
  //   setShowMore(!showMore);
  // };

  const isMap = useMemo(() => chapter && getIsMap(chapter), [chapter?.chapters]);

  const getDownloadHref = audio_href => `${audio_href}?filename=${audio_href}`;

  return (
    <div className="chapter__action-panel">
      <div className="chapter__verses-actions">
        {/* TODO пока не понятно нужно будет или нет 10.12.2019 */}
        {/* <div className="chapter__verses-action">
          {screen.phone && (
            <ChapterVerseAction
              key="ekzegets"
              onClick={openMenu}
              icon="ekzeget"
              text={screen.phone || screen.tablet ? 'Толкования' : 'Экзегеты'}
            />
          )}
        </div> */}
        <div className="chapter__verse-actions">
          <div className="chapter-verses-left">
            <ChapterVerseAction
              key="play"
              onClick={audioPlayerToggle}
              icon={showPlayer && isExist ? 'pause' : 'play'}
              text="Прослушать главу"
              active={showPlayer}
              type="play"
              disabled={!isExist}
            />

            <ChapterVerseAction
              key="parallel"
              onClick={parallelToggle}
              icon={showParallel ? 'parallel' : 'parallelOpen'}
              text={'Параллельные стихи'}
              active={showParallel}
            />
            <ChapterVerseAction
              key="dictionaries"
              onClick={dictionariesVerseToggle}
              icon={'dictionary'}
              text={'Словари'}
              active={showDictionaries}
            />
            {/* TODO пока не понятно нужно будет или нет 10.12.2019 */}
            {/* {screen.phone && (
              <div>
                <ChapterVerseAction
                  key="more"
                  onClick={_showMoreToggle}
                  icon="more"
                  screen={screen}
                  text={screen.phone ? '' : 'Больше'}
                  active={showMore}
                  disabled={false}
                />
                {showMore && (
                  <div className="chapter__verses-action_mobile">
                    <ChapterVerseAction
                      key="map-mobile"
                      onClick={mapPointsToggle}
                      icon="map-mobile"
                      text={'Библейские карты'}
                      active={showMapPoints}
                      disabled={!isMap}
                    />
                  </div>
                )}
              </div>
            )} */}
            <ChapterVerseAction
              key="map"
              onClick={mapPointsToggle}
              icon="map"
              text={'Библейские карты'}
              active={showMapPoints}
              disabled={!isMap}
            />
          </div>

          <div className="chapter-verses-middle">
            <ChapterTabActions
              addFavorite={addFavorite}
              addNote={addNote}
              short={short}
              showManuscript={showManuscript}
              deleteFavoriteChapter={deleteFavoriteChapter}
              isNoteAdd={isNoteAdd}
              selectedItems={selectedItems}
              favoriteVerse={favoriteVerse}
              favoritesVerse={favoritesVerse}
              updateFavVerse={updateFavVerse}
              editFavorite={editFavorite}
              arrayOfIds={arrayOfIds}
            />
            <div className="chapter__verses-action">
              <ShareButton text={verseTextForShare} verseInfo={verseInfo} />
            </div>
          </div>
        </div>
      </div>
      {showPlayer && isExist && (
        <div className="chapter__audio">
          <AudioPlayer
            src={`${bibleCDN}/${pathToAudio}`}
            srcToDownload={`${bibleCDN}/${pathToAudio}`}
            numberOfChapters={+chapter.parts}
            withSwitchForChapter
            title={currentTranslate.title || ''}
            downloadIcon={
              <MediaDetailDownload href={getDownloadHref(`${bibleCDN}/${pathToAudio}`)} />
            }
          />
        </div>
      )}
    </div>
  );
};

export default ChapterVerseHead;
