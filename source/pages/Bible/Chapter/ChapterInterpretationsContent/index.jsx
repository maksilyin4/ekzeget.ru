import React from 'react';
import cx from 'classnames';

import InterpretationItem from './InterpretationItem';

import { findTranslateChapterInterpretations } from '../utilsChapter';

const ChapterInterpretationsContent = ({
  verseWithInterpretations,
  currentTranslate,
  selected,
  handleSelect,
  addedToFavorite,
  linkText,
  ekzegetCode,
  isVisibleCheckBox,
}) => {
  return verseWithInterpretations.verses.map(({ id, verses, interpretation }) => {
    const verseTranslate = findTranslateChapterInterpretations({
      verses,
      currentTranslate,
    });
    const keyId = id;
    return verseTranslate.length && interpretation ? (
      <InterpretationItem
        key={keyId}
        verses={verseTranslate}
        interpretation={interpretation}
        currentTranslate={currentTranslate}
        selected={interpretation.id === selected}
        handleSelect={handleSelect}
        addedToFavorite={addedToFavorite.hasOwnProperty(interpretation.id)}
        linkText={linkText}
        ekzegetId={ekzegetCode}
        isVisibleCheckBox={isVisibleCheckBox}
      />
    ) : (
      <div key={keyId} className="verse__preaching-verse--empty">
        <sup>{verseTranslate[0]?.number}</sup>
        <span
          className={cx({
            csya: currentTranslate === 'csya_old',
            greek: currentTranslate === 'grek',
          })}
          dangerouslySetInnerHTML={{
            __html: verseTranslate[0]?.text,
          }}
        />
      </div>
    );
  });
};

export default ChapterInterpretationsContent;
