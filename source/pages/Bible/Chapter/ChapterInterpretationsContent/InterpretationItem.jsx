import React from 'react';
import { Link } from 'react-router-dom';
import cx from 'classnames';

import Icon from '~components/Icons/Icons';

const InterpretationItem = props => {
  const {
    verses = [],
    interpretation = {},
    selected,
    handleSelect,
    addedToFavorite,
    linkText,
    ekzegetId,
    currentTranslate,
  } = props;

  return (
    <div className="verse__preaching">
      {!addedToFavorite && (
        <div
          onClick={() => {
            handleSelect(interpretation.id, verses[0]);
          }}
          className={cx('chapter__verse__checkbox', {
            selectedVerse: selected,
            verseVisible: selected,
          })}>
          {selected ? <Icon icon="checkboxFavorite" /> : null}
        </div>
      )}
      {verses.map(({ id, number, text }) => {
        return (
          <div className={cx('chapter-verse-title', 'chapter-verse-padding')}>
            <sup>{number}</sup>
            <Link
              key={id}
              to={`${linkText}/stih-${number}/tolkovatel-${ekzegetId}/`}
              className="verse__preaching-verse">
              <span
                className={cx({
                  csya: currentTranslate === 'csya_old',
                  greek: currentTranslate === 'grek',
                })}
                dangerouslySetInnerHTML={{ __html: text }}
                style={{ backgroundColor: addedToFavorite ? '#F9D063' : '' }}
              />
            </Link>
          </div>
        );
      })}
      <div
        className={cx('verse__preaching-text', {
          csya_old__container: currentTranslate === 'csya_old',
          grek__container: currentTranslate === 'grek',
        })}
        dangerouslySetInnerHTML={{ __html: interpretation.comment }}
      />
    </div>
  );
};

export default InterpretationItem;
