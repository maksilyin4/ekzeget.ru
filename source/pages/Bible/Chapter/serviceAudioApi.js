import qs from 'qs';

import { _AXIOS } from '../../../dist/ApiConfig';

export default function isFileExist(audioSrc) {
  const data = qs.stringify({ file: audioSrc });

  return _AXIOS({ url: '/site/file-exist/', method: 'POST', data })
    .then(({ file_exist }) => file_exist)
    .catch(() => false);
}
