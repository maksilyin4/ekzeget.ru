import R from 'ramda';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import cx from 'classnames';
import { withCookies } from 'react-cookie';
// css
import './chapter.scss';
import '../bible.scss';
// redux
import { getMetaData, setClearMeta } from '~store/metaData/actions';
import { LS } from '../../../dist/browserUtils';
import chapterActions from '../../../dist/actions/Chapter';
import interpretationsActions from '../../../dist/actions/ChapterInterpretations';
import {
  getFavoriteVerse,
  addFavToStore,
  getFavoriteInter,
  addFavInterpToStore,
  getNotes,
  setSessionTag,
  updateFavVerse,
  deleteFavoriteVerse,
} from '../../../dist/actions/favorites';
import exegetesActions from '../../../dist/actions/Exeget';
import bookmarksActions from '../../../dist/actions/Bookmarks';
import { getChapterIsLoading, getChapter, getErrorChapter } from '../../../dist/selectors/Chapter';
import {
  getChapterInterpretationsIsLoading,
  getChapterInterpretations,
  getVerseWithInterpretations,
} from '../../../dist/selectors/ChapterInterpretations';
import {
  getFavoriteList,
  getChosenTag,
  getFavTagsWithId,
  getArrayOfIds,
  getVerseList,
  getInterList,
  getNotes as selectorNotes,
} from '../../../dist/selectors/favorites';
import {
  parseGetParams,
  setAnimatedScrollTo,
  getVerseBold,
  smoothScrollTo,
} from '../../../dist/utils';
import { getCurrentTranslate } from '../../../dist/selectors/translates';
import getScreen from '../../../dist/selectors/screen';
import { userLoggedIn } from '~dist/selectors/User';
import { getErrorExeget } from '~dist/selectors/Exeget';
import { setMetaData, setIsLoadingMeta } from '~store/metaData/selector';
import { onMouseEnter, onMouseLeave, onMouseObj } from '~components/PreloadChunk';
import BiblePreloader from '~components/Bible/BiblePreloader';
import VerseList from './VerseList';
import BibleHeader from '../BibleHeader';
import ChapterInterpretations from './ChapterInterpretations';
import ChapterVerseHead from './ChapterVerseHead';
import '../../../components/PullDown/pullDown.scss';
import BibleActionPanel from '../../../components/BibleActionPanel/';
import ToggleLayout from '../../../components/ToggleLayout';
import BibleMediaContentRight from '~components/Bible/BibleMediaContentRight';
import BibleMenuBooksLeft from '~components/Bible/BibleMenuBooksLeft';
import BibleMediaMobileInterpretations from '~components/Bible/BibleMediaMobileInterpretations';
import popupShowManuscript from './popupChapter/popupShowManuscript';
import popupAddFavorite from './popupChapter/popupAddFavorite';
import popupEditFavorite from './popupChapter/popupEditFavorite';
import popupAddNote from './popupChapter/popupAddNote';
import { popupAuth } from '~components/Popups/PopupAuth';
import NotFound from '../../NotFound/NotFound';

// import Helmet from '~components/Helmet';
// utils
import { parsePathname } from '~utils/parsePathname';
import { getLocalStorage } from '~utils/localStorage';
import { getIsShowContent, setIsShowMobileInterpretations } from '../utilsBible';
import { setStyleHeight, defaultHeightContentBible, setItemsCheckbox } from '~utils/common';
import { changeVerseTextForShare } from './utilsChapter';

const PHONE_ADDITIONAL = -180;
const ADAPTIVE_ADITITONAL = -120;

class Chapter extends Component {
  static propTypes = {
    location: PropTypes.object,
    chapter: PropTypes.object,
  };

  constructor(props) {
    super(props);
    const getParams = parseGetParams(props.location.search);

    const chapterId = props.match.params.chapter_num;
    const bookCode = this.props.location.pathname.split('/')[2];

    let tab;
    switch (getParams.tab) {
      case 'video':
        tab = 2;
        break;
      case 'media-library':
        tab = 4;
        break;
      default:
        tab = 1;
    }

    this.refContent = React.createRef();

    this.state = {
      bookCode,
      chapterId,
      activeContent: tab,
      activePreacher: '',
      showParallel: false,
      showMapPoints: false,
      showDictionaries: false,
      showPlayer: false,
      verseInline: false,
      verseBold: getVerseBold(props.location.search, chapterId),
      getParams,
      selectedItems: [],
      verseInfo: [],
      verseTextForShare: [],
      selectedInter: null,
      isReload: false,
      isLoadTranslate: true,
      isPrevOpen: false,
      layout: {
        content: false,
        left: false,
        right: false,
      },
      isRedirect: false,
    };
  }

  async componentDidMount() {
    const { bookCode, chapterId, getParams } = this.state;

    const {
      getChapter,
      getChapterInterpretations,
      userLoggedIn,
      getExeget,
      match: { params },
      screen,
      cookies,
      getMetaData,
      location: { pathname },
      errorExeget,
      errorChapter,
    } = this.props;

    if (!(errorChapter?.message || errorExeget?.message)) {
      await getMetaData(pathname);

      let activePreacher =
        this.getCheckInterpretator(this.props.match.params.tolkovatel) ||
        getParams.interpretator_id ||
        getParams.investigator_id ||
        null;

      let activeContent =
        activePreacher && !activePreacher?.includes('verse') ? 3 : this.state.activeContent;

      smoothScrollTo();

      if (screen.phone) {
        cookies.set('heightLeftMenuBible', 0);
      }
      if (screen.tablet) {
        this.setHeightContentTablet();
      }

      if (
        !this.props.chapterInterpretations.interpretations &&
        (!!getParams.interpretator_id || !!getParams.investigator_id || !!params.tolkovatel)
      ) {
        if (params.tolkovatel) {
          await getChapterInterpretations(
            bookCode,
            chapterId,
            this.getCheckInterpretator(params.tolkovatel),
          );
        }
        if (getParams.interpretator_id) {
          await getChapterInterpretations(bookCode, chapterId, getParams.interpretator_id);
        }

        activeContent = 3;
        activePreacher =
          this.getCheckInterpretator(params.tolkovatel) ||
          getParams.interpretator_id ||
          getParams.investigator_id ||
          null;
      }

      if (userLoggedIn) {
        await this.props.getFavoriteVerse();
        await this.props.getFavoriteInter();
        await this.props.getNotes();
      }

      if (getParams.investigator_id || getParams.interpretator_id || params.tolkovatel) {
        const ekzeget =
          getParams.investigator_id ||
          getParams.interpretator_id ||
          this.getCheckInterpretator(params.tolkovatel);

        if (ekzeget && !ekzeget.includes('verse')) {
          await getExeget({ id: ekzeget });
        }
      }

      if (
        !this.props.chapter?.book ||
        this.state.bookCode !== R.pathOr('', ['chapter', 'book', 'code'], this.props) ||
        this.state.chapterId !==
          R.pathOr(0, ['chapter', 'book', 'chapter', 'number'], this.props).toString() ||
        this.props.currentTranslate !== 'st_text'
      ) {
        await getChapter(bookCode, chapterId, pathname).then(() => {
          if (
            !this.props.isLoadingChapter &&
            this.state.verseBold &&
            typeof document !== 'undefined'
          ) {
            const additionalOffset = this.props.screen.phone
              ? PHONE_ADDITIONAL
              : ADAPTIVE_ADITITONAL;
            setAnimatedScrollTo(
              document.getElementById(this.state.verseBold.match(/\d{1,2}/)[0]),
              additionalOffset,
            );
          }
        });

        this.setState({
          activeContent,
          activePreacher: activePreacher?.includes('verse') ? undefined : activePreacher,
          layout: {
            content: getLocalStorage('content') || false,
            left: getLocalStorage('left') || false,
            right: getLocalStorage('right') || false,
          },
        });
      }
    }
  }

  componentWillReceiveProps(nextProps, nextState) {
    const chapterId = nextProps.match.params?.chapter_num;
    const bookCode = nextProps.location.pathname.split('/')[2];
    // TODO переделать
    if (this.state.activeContent !== nextState.activeContent) {
      this.setState({
        bookCode,
        chapterId,
        getParams: parseGetParams(nextProps.location.search),
        verseBold: getVerseBold(this.props.location.search, chapterId),
        // TODO Зачем? Был баг
        // selectedItems: [],
      });
    } else {
      this.setState({
        bookCode,
        chapterId,
        getParams: parseGetParams(nextProps.location.search),
      });
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    const { getParams, bookCode, chapterId, isRedirect } = this.state;

    const {
      getChapterInterpretations,
      getFavoriteVerse,
      location: { pathname, search },
      match: { params },
      chapter,
      cookies,
      screen,
      userLoggedIn,
      getFavoriteInter,
      getNotes,
      getMetaData,
      errorChapter,
      errorExeget,
    } = this.props;

    if (prevProps.errorExeget?.message !== errorExeget?.message) {
      if (!errorExeget?.message) {
        await this.fetchChapter();
      }
    }

    if (prevProps.errorChapter?.message !== errorChapter?.message) {
      if (!errorChapter?.message) {
        await this.fetchChapter();
      }
    }
    if (prevProps.chapter?.book !== chapter?.book) {
      // для редиректа. Если выбранная глава(предыдущей книги),
      // при переключении на след. книгу больше, чем глав в книге
      const chapterNumber = parsePathname({
        pathname: params.chapter_num,
        index: 1,
        separator: '-',
      });

      if (chapterNumber > +chapter?.book?.parts && !location.search.length) {
        this.setState({ isRedirect: true });
      }
    }

    if (search !== prevProps.location.search || pathname !== prevProps.location.pathname) {
      const { verseInfo } = this.state;
      let verseTextForShareCurrent = [];
      if (verseInfo && !params.tolkovatel) {
        const [, text] = changeVerseTextForShare(verseInfo);
        verseTextForShareCurrent = text;
      }

      this.setState({ verseTextForShare: verseTextForShareCurrent });
    }

    if (params.tolkovatel && prevProps.match.params.tolkovatel !== params.tolkovatel) {
      smoothScrollTo();
    }

    if (prevProps.location.pathname !== pathname) {
      if (this.props.errorChapter?.message) {
        this.props.getChapterClear();
      }
      if (this.props.errorExeget?.message) {
        this.props.getExegetClear();
      }

      getMetaData(pathname);

      if (!this.props.errorChapter?.message) {
        this.fetchChapter();

        if (isRedirect) {
          this.setState({ isRedirect: false });
        }
      }
    }
    if (
      getParams.investigator_id &&
      (prevState.getParams.investigator_id !== getParams.investigator_id ||
        prevState.bookCode !== bookCode ||
        prevState.chapterId !== chapterId)
    ) {
      getChapterInterpretations(bookCode, chapterId, getParams.investigator_id);
    }
    if (
      getParams.interpretator_id &&
      (prevState.getParams.interpretator_id !== getParams.interpretator_id ||
        prevState.bookCode !== bookCode ||
        prevState.chapterId !== chapterId)
    ) {
      getChapterInterpretations(bookCode, chapterId, getParams.interpretator_id);
    }
    if (
      params.tolkovatel &&
      (prevProps.match.params.tolkovatel !== params.tolkovatel ||
        prevState.bookCode !== bookCode ||
        prevState.chapterId !== chapterId)
    ) {
      getChapterInterpretations(bookCode, chapterId, this.getCheckInterpretator(params.tolkovatel));
    }
    if (
      getParams.investigator_id !== prevState.getParams.investigator_id ||
      getParams.interpretator_id !== prevState.getParams.interpretator_id ||
      prevProps.match.params.tolkovatel !== params.tolkovatel
    ) {
      const ekzeget =
        getParams.investigator_id ||
        getParams.interpretator_id ||
        this.getCheckInterpretator(params.tolkovatel);
      if (ekzeget) {
        this.props.getExeget({ id: ekzeget });
      }
    }

    if (
      search !== prevProps.location.search ||
      prevProps.match.params.tolkovatel !== params.tolkovatel
    ) {
      if (
        getParams.interpretator_id ||
        getParams.investigator_id ||
        (params.tolkovatel && !params.tolkovatel?.includes('verse'))
      ) {
        LS.set(['content'], false);
        const activePreacher =
          this.getCheckInterpretator(params.tolkovatel) ||
          getParams.interpretator_id ||
          getParams.investigator_id ||
          null;
        this.setState({
          layout: { ...this.state.layout, content: false },
          activeContent: 3,
          activePreacher: activePreacher?.includes('verse') ? undefined : activePreacher,
        });
      } else {
        this.setState({
          activeContent: getParams.tab === 'video' ? 2 : getParams.tab === 'media-library' ? 4 : 1,
          activePreacher: null,
        });
      }
    }

    if (prevProps.chapter !== chapter) {
      const { title, menu } = chapter?.book ?? {};

      const bookTitle = cookies.get('bookTitle')?.title ?? '';
      if (title && (!bookTitle || bookTitle !== title)) {
        cookies.set('bookTitle', { title, menu }, { path: '/' });
      }
    }

    if (prevProps.userLoggedIn !== userLoggedIn && userLoggedIn) {
      getFavoriteVerse();
      getFavoriteInter();
      getNotes();
    }

    if (prevProps.screen.tablet !== screen.tablet && screen.tablet) {
      this.setHeightContentTablet();
    }

    if (prevProps.screen.phone !== screen.phone && screen.phone) {
      cookies.set('heightLeftMenuBible', 0);
    }
  }

  componentWillUnmount = async () => {
    this.props.getExegetClear();
    this.props.setClearMeta();
    this.setState({ isRedirect: false, activePreacher: '' });
  };

  fetchChapter = () => {
    const { bookCode, chapterId } = this.state;

    const {
      isLoadingChapter,
      search,
      screen,
      getChapter,
      location: { pathname },
    } = this.props;
    try {
      getChapter(bookCode, chapterId, pathname).then(() => {
        if (!isLoadingChapter && search && typeof document !== 'undefined') {
          const additionalOffset = screen.phone ? PHONE_ADDITIONAL : ADAPTIVE_ADITITONAL;
          setAnimatedScrollTo(
            document.getElementById(search.match(/\d{1,2}/)[0]),
            additionalOffset,
          );
        }
      });
    } catch (error) {
      console.error('Chapter -> fetchChapter -> error', error);
    }
  };

  setHeightContentTablet = () => {
    const contentHeight = this.refContent.current?.clientHeight;

    const currentHeight =
      contentHeight > defaultHeightContentBible ? defaultHeightContentBible : contentHeight;

    this.props.cookies.set('heightLeftMenuBible', currentHeight);
  };

  getCheckInterpretator = param => param && param.replace('tolkovatel-', '');

  setActiveContent = active => {
    this.setState({ activeContent: active });
  };

  _parallelToggle = () => {
    this.setState({ showParallel: !this.state.showParallel });
  };

  _mapPointsToggle = () => {
    this.setState({
      showMapPoints: !this.state.showMapPoints,
    });
  };

  _dictionariesVerseToggle = () => {
    this.setState({
      showDictionaries: !this.state.showDictionaries,
    });
  };

  _showMoreToggle = () => {
    this.setState({
      showMore: !this.state.showMore,
    });
  };

  _audioPlayerToggle = () => {
    this.setState({
      showPlayer: !this.state.showPlayer,
    });
  };

  _toggleInline = () => {
    this.setState({ verseInline: !this.state.verseInline });
  };

  _showManuscript = (book, chapter) =>
    popupShowManuscript({ screen: this.props.screen, chapter, book });

  tabRef = element => {
    this.tabAction = element;
  };

  addFavorite = () => {
    if (this.props.userLoggedIn) {
      popupAddFavorite({
        props: { ...this.props },
        state: { ...this.state },
        clearSelected: this.clearSelected,
        setState: this.setState,
      });
    } else {
      popupAuth('Добавлять стихи в избранное');
    }
  };

  editFavorite = favoriteId => {
    const { updateFavVerse, favoritesVerse } = this.props;
    popupEditFavorite({ favoriteId, updateFavVerse, favoritesVerse });
  };

  deleteFavorite = favoriteId => this.props.deleteFavoriteVerse(favoriteId);

  addNote = () => {
    if (this.props.userLoggedIn) {
      const {
        chapter: { book },
      } = this.props;
      popupAddNote({ book });
    } else {
      popupAuth('Добавить заметку');
    }
  };

  handleSelectionFinish = (id, infoVerse) => () => {
    const { selectedItems, verseInfo } = this.state;

    if (!this.props.userLoggedIn) {
      this.clearSelected();
    }

    const selectedVerseId = setItemsCheckbox({
      stateItems: selectedItems,
      key: id,
      item: id,
    });
    const verseInfoState = setItemsCheckbox({
      stateItems: verseInfo,
      key: infoVerse.verseNumber,
      item: infoVerse,
    });

    const [verseInfos, verseTextForShare] = changeVerseTextForShare(verseInfoState);

    this.setState({
      selectedItems: [...selectedVerseId],
      verseInfo: verseInfos,
      verseTextForShare,
    });
  };

  clearSelected = () => {
    this.setState({
      selectedItems: [],
    });
  };

  selectInter = (id, { text, number }) => {
    this.setState(({ selectedInter }) => ({
      selectedInter: selectedInter === id ? null : id,
      verseTextForShare: [`Ст.${number}. ${text}`],
    }));
  };

  handleChange = field => value => {
    this.setState({ [field]: value });
  };

  deleteFavoriteChapter = (isAdded, currentBook) => {
    const { deleteBookmark } = this.props;
    if (!isAdded) {
      deleteBookmark(currentBook.id);
    }
  };

  toggleLayout = layoutType => {
    const {
      layout: { [layoutType]: nowLayout, ...lastLayout },
    } = this.state;

    const isLastLayout = !Object.values(lastLayout).every(el => el);

    if (nowLayout || isLastLayout) {
      LS.set([layoutType], !nowLayout);
      this.setState({ ...this.state, layout: { ...this.state.layout, [layoutType]: !nowLayout } });
    }
  };

  breadItems = (chapter_num, bookTitle, book_code) => [
    { path: '/', title: 'Главная' },
    {
      path: `/bible/${book_code}/glava-${chapter_num}/`,
      title: bookTitle || 'Евангелие от Матфея',
      onMouseLeave,
      onMouseEnter: () => onMouseEnter(onMouseObj.onMouseBibleBook),
      onFocus: onMouseObj.onMouseBibleBook,
    },
    { path: '', title: `Глава ${chapter_num}` },
  ];

  _getChapterId = chapter => {
    if (!chapter || !chapter.chapters.length) return '';

    return chapter.chapters[0].number;
  };

  render() {
    const chapter = this.props.chapter?.book;

    const {
      chapterId,
      bookCode,
      activeContent,
      showPlayer,
      verseInline,
      showParallel,
      showMapPoints,
      showDictionaries,
      activePreacher,
      selectedItems,
      selectedInter,
      isReload,
      isLoadTranslate,
      isPrevOpen,
      layout: { left, content, right },
      isRedirect,
      verseTextForShare,
      verseInfo,
    } = this.state;

    const {
      favoriteVerse,
      isLoadingChapterInterpretations,
      userLoggedIn,
      favoriteInter,
      verseWithInterpretations,
      notesList,
      currentTranslate,
      screen,
      favoritesVerse,
      updateFavVerse,
      arrayOfIds,
      location,
      filteredMapPoints,
      match: {
        params: { tolkovatel: tolkovatelParams, chapter_num, book_code },
      },
      isLoadingChapter,
      cookies,
      // isLoadingMeta,
      metaData,
    } = this.props;

    const tolkovatel = tolkovatelParams?.includes('verse') ? undefined : tolkovatelParams;

    const chapterNumber = parsePathname({
      pathname: chapter_num,
      index: 1,
      separator: '-',
    });

    if (isRedirect) {
      return (
        <Redirect
          to={`/bible/${book_code}/${parsePathname({
            pathname: chapter_num,
            index: 0,
            separator: '-',
          })}-${chapter.parts}/`}
        />
      );
    }

    const bookTitle = chapter?.title ?? (cookies.get('bookTitle')?.title || '');

    const chapterKey = screen.desktop ? 'title' : 'menu';
    const bookMenu = chapter?.[chapterKey] ?? cookies.get('bookTitle')?.[chapterKey];

    const heightLeftMenuBible = cookies.get('heightLeftMenuBible');

    const isShowContent = getIsShowContent(cookies, screen.phone);
    const isShowMobileInterpretations = setIsShowMobileInterpretations(cookies, screen.phone);

    const shortChapterCode = `${R.pathOr('', ['short_title'], chapter)}. ${this._getChapterId(
      chapter,
    )}`;
    let title = '';
    if (
      (tolkovatel && tolkovatel.includes('tolkovatel')) ||
      (location.search.includes('interpretator_id') && verseWithInterpretations.ekzeget.length > 1)
    ) {
      title = `, ${verseWithInterpretations.ekzeget}`;
    }

    const { ekzeget } = verseWithInterpretations;
    const ekzegetTitle = verseWithInterpretations.verses.length ? `${ekzeget}` : '';

    // for SEO 404

    if (this.props.errorChapter?.message || this.props.errorExeget?.message) {
      return <NotFound />;
    }

    // if (!chapter) return null;

    return (
      <div className="page chapter bible-page">
        <>
          {/* {!isLoadingMeta && ( */}
          {/*  <Helmet */}
          {/*    title={ */}
          {/*      metaData?.title || */}
          {/*      (title.length > 0 */}
          {/*        ? `${bookTitle} ${chapterNumber} глава - ${chapter?.short_title} ${chapterNumber} | ${ekzegetTitle}` */}
          {/*        : `БИБЛИЯ – ${bookTitle} ${chapterNumber} глава`) */}
          {/*    } */}
          {/*    description={ */}
          {/*      metaData?.description || */}
          {/*      (title.length > 0 */}
          {/*        ? `БИБЛИЯ с толкованием, ${ekzegetTitle} - ${bookTitle} ${chapterNumber} глава. Читать, слушать и изучать️.` */}
          {/*        : `БИБЛИЯ - читать и слушать ${chapter && */}
          {/*            chapter.title} ${chapterNumber} глава. Все к Библии: видео, аудио, книги, литература, музыка, картины, вопросы. Толкование Святых отцов.`) */}
          {/*    } */}
          {/*    keywords={ */}
          {/*      metaData?.keywords || */}
          {/*      (title.length > 0 */}
          {/*        ? `${ekzegetTitle}, ${bookTitle} ${chapterNumber} глава, ${chapter?.short_title} ${chapterNumber}, Библия, Толкование Библии, Свящённое писание, Святые отцы, Новый Завет, Ветхий Завет, Иисус Христос` */}
          {/*        : `${bookTitle}️ ${chapterNumber}, читать Библию, Священное Писание, Новый Завет, Ветхий Завет, Иисус Христос, Толкование`) */}
          {/*    } */}
          {/*  /> */}
          {/* )} */}

          <BibleHeader
            title={
              metaData?.h_one ||
              (title.length > 0
                ? `Толкование ${bookTitle} ${chapterNumber} глава - ${ekzegetTitle}`
                : `${bookTitle} ${chapterNumber} глава`)
            }
            breadcrumbs={this.breadItems(chapterNumber, bookTitle, book_code)}
            screen={screen}
          />

          <div className="wrapper">
            <BibleActionPanel
              pagesCount={Number(chapter && chapter.parts)}
              currentPage={Number(chapterNumber)}
              bookCode={book_code}
              bookTitle={bookMenu}
              navText="Глава"
            />

            {!screen.phone && (
              <ToggleLayout
                toggleFunc={this.toggleLayout}
                screen={screen}
                left={left}
                content={content}
                right={right}
                urlBack={tolkovatel ? `/bible/${book_code}/${chapter_num}/` : ''}
                urlText="Вернуться к тексту"
              />
            )}

            <div className="inner-page__body">
              <div className="wrapper_multi-col">
                {(screen.desktop || screen.tabletMedium) && (
                  <BibleMenuBooksLeft
                    left={left}
                    isOtherBlock={content && right}
                    activeTestament={location.state?.bookTestamentId || chapter?.testament_id}
                    activeBook={bookCode}
                    activeChapter={chapterId}
                    isDesktop={screen.desktop}
                  />
                )}

                {isShowContent && (
                  <div
                    ref={this.refContent}
                    style={setStyleHeight({
                      height: heightLeftMenuBible,
                      styleName: 'minHeight',
                    })}
                    className={cx('bible-content', {
                      collapse: content && !screen.phone,
                      'bible-not-left': left,
                    })}>
                    <div className="paper" ref={this.tabRef}>
                      {(activeContent === 1 || activeContent === 3) && (
                        <ChapterVerseHead
                          verseTextForShare={verseTextForShare}
                          screen={screen}
                          activeContent={activeContent}
                          currentTranslate={currentTranslate}
                          parallelToggle={this._parallelToggle}
                          mapPointsToggle={this._mapPointsToggle}
                          audioPlayerToggle={this._audioPlayerToggle}
                          dictionariesVerseToggle={this._dictionariesVerseToggle}
                          showManuscript={() =>
                            this._showManuscript(chapter.ext_id, chapter.chapters[0].number)
                          }
                          toggleInline={this._toggleInline}
                          showPlayer={showPlayer}
                          chapter={chapter}
                          verseInline={verseInline}
                          showParallel={showParallel}
                          showMapPoints={showMapPoints}
                          showDictionaries={showDictionaries}
                          openMenu={this.openMenu}
                          isPrevOpen={isPrevOpen}
                          addFavorite={this.addFavorite}
                          addNote={this.addNote}
                          short={shortChapterCode}
                          deleteFavoriteChapter={this.deleteFavoriteChapter}
                          isNoteAdd={notesList.some(i => i.short === shortChapterCode)}
                          selectedItems={selectedItems}
                          favoriteVerse={favoriteVerse}
                          favoritesVerse={favoritesVerse}
                          updateFavVerse={updateFavVerse}
                          editFavorite={this.editFavorite}
                          arrayOfIds={arrayOfIds}
                          verseInfo={verseInfo}
                        />
                      )}

                      {isReload ? (
                        <BiblePreloader />
                      ) : (
                        <div className="chapter-content">
                          {isShowMobileInterpretations && (
                            <BibleMediaMobileInterpretations
                              activeItem={activePreacher}
                              chapter={chapter?.chapters[0]}
                              bookCode={bookCode}
                              baseUrl="bible"
                              title={tolkovatel && verseWithInterpretations?.ekzeget}
                            />
                          )}
                          {activeContent === 1 && !tolkovatel && !tolkovatel?.includes('verse') && (
                            <>
                              {!isLoadingChapter ? (
                                <VerseList
                                  verseInline={verseInline}
                                  chapter={chapter}
                                  screen={screen}
                                  showParallel={showParallel}
                                  showMapPoints={showMapPoints}
                                  showDictionaries={showDictionaries}
                                  currentTranslate={currentTranslate?.code}
                                  handleSelecting={this.handleSelecting}
                                  handleSelectionFinish={this.handleSelectionFinish}
                                  selectedItems={selectedItems}
                                  favoriteVerse={favoriteVerse}
                                  userLoggedIn={userLoggedIn}
                                  addFavorite={this.addFavorite}
                                  editFavorite={this.editFavorite}
                                  addNote={this.addNote}
                                  bold={this.state.verseBold}
                                  location={location}
                                  short={shortChapterCode}
                                  isLoadTranslate={isLoadTranslate}
                                  filteredMapPoints={filteredMapPoints}
                                  isNoteAdd={notesList.some(i => i.short === shortChapterCode)}
                                />
                              ) : (
                                <BiblePreloader />
                              )}
                            </>
                          )}

                          {activeContent === 3 && (
                            <ChapterInterpretations
                              isLoading={isLoadingChapterInterpretations}
                              handleSelect={this.selectInter}
                              selected={selectedInter}
                              currentTranslate={currentTranslate?.code}
                              userLoggedIn={userLoggedIn}
                              addedToFavorite={favoriteInter}
                              verseWithInterpretations={verseWithInterpretations}
                              linkText={`/bible/${bookCode}/${chapterId}`}
                              addFavorite={this.addFavorite}
                              editFavorite={this.editFavorite}
                              addNote={this.addNote}
                              short={shortChapterCode}
                              isNoteAdd={notesList.some(i => i.short === shortChapterCode)}
                            />
                          )}
                        </div>
                      )}
                    </div>
                  </div>
                )}
                <BibleMediaContentRight
                  left={left}
                  collapse={right}
                  extend={content && !screen.phone}
                  chapter={chapter?.chapters[0]}
                  bookCode={bookCode}
                  activeItem={activePreacher}
                  baseUrl="bible"
                  locationCustom={location}
                  screen={screen}
                  userLoggedIn={userLoggedIn}
                  editFavorite={this.editFavorite}
                  deleteFavorite={this.deleteFavorite}
                  extendLayout={{ content, left, right }}
                />
              </div>
            </div>
          </div>
        </>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLoadingChapter: getChapterIsLoading(state),
  isLoadingChapterInterpretations: getChapterInterpretationsIsLoading(state),
  chapter: getChapter(state),
  errorChapter: getErrorChapter(state),
  errorExeget: getErrorExeget(state),
  chapterInterpretations: getChapterInterpretations(state),
  userLoggedIn: userLoggedIn(state),
  favoriteVerse: getVerseList(state),
  favoriteInter: getInterList(state),
  verseWithInterpretations: getVerseWithInterpretations(state),
  notesList: selectorNotes(state),
  screen: getScreen(state),
  currentTranslate: getCurrentTranslate(state),
  choosenTag: getChosenTag(state),
  favoritesVerse: getFavoriteList(state),
  arrayOfIds: getArrayOfIds(state),
  getFavTagsWithId: getFavTagsWithId(state),
  metaData: setMetaData(state),
  isLoadingMeta: setIsLoadingMeta(state),
});

const mapDispatchToProps = {
  ...chapterActions,
  ...interpretationsActions,
  ...exegetesActions,
  ...bookmarksActions,

  getFavoriteVerse,
  setSessionTag,
  addFavToStore,
  getFavoriteInter,
  addFavInterpToStore,
  getNotes,
  updateFavVerse,
  deleteFavoriteVerse,
  getMetaData,
  setClearMeta,
};

export default connect(mapStateToProps, mapDispatchToProps)(withCookies(Chapter));
