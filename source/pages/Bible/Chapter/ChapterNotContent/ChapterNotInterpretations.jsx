import React from 'react';
import { Link } from 'react-router-dom';

const ChapterNotInterpretations = ({ ekzegetId, ekzegetCode, chapterNotContent }) =>
  ekzegetId ? (
    <Link className="verse__preacher" to={`/all-about-bible/ekzegets/${ekzegetCode}/`}>
      {chapterNotContent}
    </Link>
  ) : (
    <p className="verse__preacher">{chapterNotContent}</p>
  );

export default ChapterNotInterpretations;
