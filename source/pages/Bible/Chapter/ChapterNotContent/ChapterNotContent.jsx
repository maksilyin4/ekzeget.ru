import React from 'react';

import './chapterNotContent.scss';

const ChapterNotContent = () => (
  <div className="chapter-not-content">
    <p>
      Либо выбранный экзегет не толковал данную главу, либо его толкование в нашей базе отсутствует!
    </p>
    <p>
      Но Вы можете выбрать толкование любого другого экзегета из списка справа. Также Вы можете
      воспользоваться списком параллельных стихов, чтобы найти толкования на схожие места Писания.
    </p>
  </div>
);

export default ChapterNotContent;
