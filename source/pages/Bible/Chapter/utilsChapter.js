export const findTranslateChapterInterpretations = ({ verses, currentTranslate }) => {
  const [verse] = verses;

  const filterCode = ({ code }) => code.code === currentTranslate;

  const createNewArray = ({ id, text }) => ({ id, text, number: verse.number });

  const verseTranslate =
    currentTranslate === 'st_text'
      ? verses
      : verse.verseTranslates.filter(filterCode).map(createNewArray);

  return verseTranslate;
};

export const getIsMap = chapter => {
  if (chapter) {
    return (
      chapter.chapters.length && chapter.chapters[0].verses.some(i => i.bibleMapsPoints.length > 0)
    );
  }
  return false;
};

export const setAudioSrc = ({ audioTranslate, chapter }) => {
  return `${audioTranslate}/${chapter?.legacy_code}/${chapter?.legacy_code}-${chapter?.chapters
    .length && chapter?.chapters[0].number}-${audioTranslate}.mp3`;
};

export const getCurrentTranslateVerse = ({ chapter, currentTranslate = 'st_text' }) => {
  if (!chapter) {
    return [];
  }

  return (
    chapter.chapters.length &&
    chapter?.chapters[0].verses.reduce((prev, { verseTranslates, text, ...otherVerseProps }) => {
      if (currentTranslate === 'st_text') {
        prev.push({ ...otherVerseProps, text });
      } else {
        const translatedVerse = verseTranslates.find(
          item =>
            currentTranslate === 'st_text' ||
            (item.code.code === currentTranslate && item.text !== ''),
        );

        if (translatedVerse) {
          prev.push({ ...otherVerseProps, text: translatedVerse.text });
        }
      }
      return prev;
    }, [])
  );
};

export const changeVerseTextForShare = verseInfoState => {
  const sortVerseText =
    verseInfoState.length > 1
      ? [...verseInfoState].sort((a, b) => a.verseNumber.toString().localeCompare(b.verseNumber))
      : verseInfoState;

  const textMap = sortVerseText.map(
    ({ verseNumber, verseText }) => `Ст.${verseNumber}. ${verseText}`,
  );

  return [sortVerseText, textMap];
};
