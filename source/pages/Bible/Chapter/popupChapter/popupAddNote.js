import React from 'react';

import AddNote from '~components/Popups/AddNote';

import { Popup } from '../../../../dist/browserUtils';

const popupAddNote = ({ book }) => {
  Popup.create(
    {
      title: null,
      content: (
        <AddNote
          sendData={`${book.short_title}. ${book.chapters[0].number}`}
          title={`${book.title} ${book.chapters[0].title}`}
        />
      ),
      className: 'addNote_popup',
    },
    true,
  );
};

export default popupAddNote;
