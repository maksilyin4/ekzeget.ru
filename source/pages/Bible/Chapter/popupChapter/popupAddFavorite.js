import R from 'ramda';
import React from 'react';

import AddFavoritesInterp from '~components/Popups/AddFavoriteInterp';
import AddFavoritesVerse from '~components/Popups/AddFavoriteVerse';

import { Popup } from '../../../../dist/browserUtils';

const popupAddFavorite = ({ props, state, clearSelected, setState }) => {
  const {
    chapter: { book },
    favoriteVerse,
    chapterInterpretations,
    addFavInterpToStore,
    addFavToStore,
    getFavoriteInter,
    getFavoriteVerse,
  } = props;
  const { selectedItems, activeContent, selectedInter } = state;

  Popup.close();

  const interpretInfo = R.pathOr([], ['interpretations'], chapterInterpretations).find(
    i => i.id === selectedInter,
  );

  Popup.create(
    {
      title: null,
      content:
        activeContent !== 3 ? (
          <AddFavoritesVerse
            listIds={selectedItems.filter(i => !favoriteVerse.hasOwnProperty(i))}
            title={`${book.title}, ${book.short_title}. ${book.chapters[0].number}:`}
            items={book.chapters[0].verses.reduce(
              (sum, i) =>
                selectedItems.includes(i.id) && !favoriteVerse.hasOwnProperty(i.id)
                  ? [...sum, i.number]
                  : sum,
              [],
            )}
            clearSelected={clearSelected}
            addFavToStore={addFavToStore}
            getFavoriteVerse={getFavoriteVerse}
          />
        ) : (
          <AddFavoritesInterp
            listIds={selectedInter}
            title={`${R.pathOr('', ['ekzeget', 'name'], interpretInfo)}, `}
            items={selectedInter ? [R.pathOr('', ['verse', 0, 'short'], interpretInfo)] : []}
            clearSelected={() => setState({ selectedInter: null })}
            addFavToStore={addFavInterpToStore}
            getFavoriteInter={getFavoriteInter}
          />
        ),
      className: 'popup_add_favorite_verse',
    },
    true,
  );
};

export default popupAddFavorite;
