// import React from 'react';

// import { Popup } from '../../../../dist/browserUtils';

const setLinkForManuscript = ({ book, chapter, verse }) => {
  const verseQuery = verse ? `verse=${verse}&` : '';

  return `http://www.codex-sinaiticus.net/ru/manuscript.aspx?book=${book}&chapter=${chapter}&lid=ru&side=r&${verseQuery}zoomSlider=0`;
};

const popupShowManuscript = ({ chapter, book, verse }) => {
  const linkManuscript = setLinkForManuscript({ book, chapter, verse });
  return window.open(linkManuscript, '_blank');
  // TODO до выяснения обстоятельств, если что удалить 27.03.20
  // return screen.phone || screen.tablet
  //   ? window.open(linkManuscript, '_blank')
  //   : Popup.create(
  //       {
  //         title: (
  //           <div className="manuscript-popup__title">
  //             <span>Манускрипт</span> <a href={linkManuscript}>Посмотреть в полном размере</a>
  //           </div>
  //         ),
  //         content: (
  //           <iframe title="Манускрипт" src={linkManuscript} frameBorder="0" allowFullScreen />
  //         ),
  //         className: 'chapter__manuscript-popup',
  //       },
  //       true,
  //     );
};
export default popupShowManuscript;
