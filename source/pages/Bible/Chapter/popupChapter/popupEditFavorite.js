import R from 'ramda';
import React from 'react';

import AddFavoritesVerse from '~components/Popups/AddFavoriteVerse';

import { Popup } from '../../../../dist/browserUtils';

const popupEditFavorite = ({ favoritesVerse, favoriteId, updateFavVerse }) => {
  Popup.close();
  const test = favoritesVerse.filter(i => i.verse.id === favoriteId);
  if (!R.isEmpty(test)) {
    Popup.create(
      {
        title: null,
        content: (
          <AddFavoritesVerse
            listIds={[test[0].verse.id]}
            title="Редактировать стих "
            items={[test[0].verse.short]}
            tags={test[0].tags}
            color={test[0].color}
            id={test[0].id}
            updateFavVerse={updateFavVerse}
          />
        ),
        className: 'popup_add_favorite_verse',
      },
      true,
    );
  } else {
    Popup.create(
      {
        title: 'Редактировать стих',
        content: 'Вы ничего не выбрали либо данный стих еще не добавлен в избранное',
      },
      true,
    );
  }
};

export default popupEditFavorite;
