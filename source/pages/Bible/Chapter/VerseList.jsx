/* eslint-disable react/no-unused-state */
import React, { Component } from 'react';
import cx from 'classnames';

import { LS } from '../../../dist/browserUtils';
import VerseItem from './VerseItem';

import { getCurrentTranslateVerse } from './utilsChapter';

export default class VerseList extends Component {
  constructor() {
    super();
    this.state = {
      position: 'absolute',
      right: 30,
      left: '',
      transform: '',
      visibility: 'hidden',
      test: [],
    };
  }

  componentDidMount() {
    if (typeof document === 'undefined') return;
    document.addEventListener('scroll', this.getScrollPosition, false);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.selectedItems.length > 0 && nextProps.selectedItems.length === 0) {
      if (this.clearBtn) this.clearBtn.click();
    }
  }

  componentDidUpdate(prevProps) {
    if (typeof document === 'undefined') return;

    if (
      (prevProps.selectedItems.length === 0 && this.props.selectedItems.length > 0) ||
      prevProps.location.pathname !== this.props.location.pathname
    ) {
      this.getScrollPosition();
    }
  }

  componentWillUnmount() {
    if (typeof document === 'undefined') return;
    document.removeEventListener('scroll', this.getScrollPosition, false);
  }

  getScrollPosition = () => {
    const { scrollable } = this.refs;
    if (this.refs.hasOwnProperty('popup') && scrollable) {
      const coords = scrollable.getBoundingClientRect();

      const { top } = coords;
      if (top < 0) {
        this.setState({
          position: 'fixed',
          right: '',
          left: coords.left + scrollable.clientWidth - 30,
          transform: 'translateX(-100%)',
          visibility: 'visible',
        });
      } else {
        this.setState({
          position: 'absolute',
          right: 30,
          left: '',
          transform: '',
          visibility: 'hidden',
        });
      }
    }
  };

  render() {
    const {
      verseInline,
      chapter,
      showParallel,
      showMapPoints,
      showDictionaries,
      currentTranslate,
      selectedItems,
      handleSelectionFinish,
      favoriteVerse,
      userLoggedIn,
      bold,
      screen,
    } = this.props;
    const localToken = LS.get('token');
    // Delete "translate" prop & replace "text" to translated text
    const currentTranslateVerse = getCurrentTranslateVerse({ chapter, currentTranslate });
    const currentTranslateVerseData =
      currentTranslateVerse.length > 0
        ? [...currentTranslateVerse].sort((a, b) => a.number - b.number)
        : [];

    return (
      <div
        ref="scrollable"
        className={cx('chapter__verses-area content-area-for-side', {
          chapter__withOut_inline: verseInline || !localToken,
        })}>
        <div
          className={cx('chapter__verses', {
            chapter__verses_inline: verseInline,
          })}>
          <div style={{ textAlign: verseInline ? 'justify' : '' }} className="chapter__text-area">
            {currentTranslateVerseData.length ? (
              currentTranslateVerseData.map(verse => {
                return (
                  verse.text && (
                    <VerseItem
                      key={verse.id}
                      handleClickItem={handleSelectionFinish}
                      screen={screen}
                      selected={selectedItems.includes(verse.id)}
                      selectableKey={verse.id}
                      verse={verse}
                      localToken={localToken}
                      code={chapter.code}
                      showParallel={showParallel}
                      showMapPoints={showMapPoints}
                      showDictionaries={showDictionaries}
                      currentTranslate={currentTranslate}
                      isCheckboxVisible={selectedItems.length > 0}
                      inFavList={
                        favoriteVerse.hasOwnProperty(verse.id) && favoriteVerse[verse.id].color
                      }
                      bold={bold}
                      userLoggedIn={userLoggedIn}
                      verseInline={verseInline}
                    />
                  )
                );
              })
            ) : (
              <p className="not-translate">Выбранный Вами перевод на данную главу отсутствует</p>
            )}
          </div>
        </div>
      </div>
    );
  }
}
