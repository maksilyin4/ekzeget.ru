import React from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';

import { bookmarks, getIsLoading } from '../../../dist/selectors/Bookmarks';

import AddBookmark from '~components/BibleBookmarks/AddBookmarks/AddBookmark';
import { GetBookmark } from '~components/BibleBookmarks/GetBookmarks/GetBookmark';
import BibleTabActionItem from '~components/Bible/BibleTabActionItem';
import ChapterVerseAction from '~components/Bible/BibleManuscriptAction';

const ChapterTabActions = ({
  userLoggedIn: { isLoading },
  addFavorite,
  addNote,
  bookmarks,
  short,
  isNoteAdd,
  deleteFavoriteChapter,
  showManuscript,
}) => {
  return (
    <div className="tab__action-list">
      <BibleTabActionItem
        key="short"
        className="tab__action-item"
        title="Добавить закладку"
        onClick={() =>
          deleteFavoriteChapter(!bookmarks.bookmark[short], bookmarks.bookmark[short])
        }>
        {bookmarks.bookmark[short] && isLoading ? (
          <GetBookmark short={short} />
        ) : (
          <AddBookmark short={short} />
        )}
      </BibleTabActionItem>

      <BibleTabActionItem
        key="favorites"
        className="tab__action-item"
        title={'Добавить в избранное'}
        onClick={addFavorite}
        icon="favorites"
      />
      <BibleTabActionItem
        key="note"
        className={cx('tab__action-item', {
          'note-added': isNoteAdd,
        })}
        title="Добавить заметку"
        onClick={addNote}
        icon="note"
      />
      <ChapterVerseAction
        classNameIcon="tab-action-icon"
        className="tab__action-item"
        title="Показать манускрипт"
        key="manuscript"
        onClick={showManuscript}
        icon="manuscript"
        type="manuscript"
      />
    </div>
  );
};

const mapStateToProps = state => ({
  userLoggedIn: state.user.userLoggedIn,
  getIsLoading: getIsLoading(state),
  bookmarks: bookmarks(state),
});

export default connect(mapStateToProps)(ChapterTabActions);
