import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Link } from 'react-router-dom';

import Icon from '../../../../components/Icons/Icons';

import closedIcon from '../images/closed.svg';

const propTypes = {
  classes: PropTypes.object,
  isOpen: PropTypes.bool,
  localToken: PropTypes.string,
  screen: PropTypes.object,
  setContainerRef: PropTypes.func,
  toggleParallel: PropTypes.func,
  verse: PropTypes.object,
  verseInline: PropTypes.bool,
};

export default class ChapterVerseParallel extends React.Component {
  render() {
    const {
      verseInline,
      localToken,
      verse,
      isOpen,
      toggleParallel,
      verseId,
      classes,
      setContainerRef,
      screen,
      icon,
    } = this.props;

    if (!verse.parallels.length) {
      return null;
    }
    return (
      <div className="prallels-container">
        <Icon icon={icon} className="extra-info__icon" />
        <div
          className={cx('chapter__verse-parallels not-selectable', {
            checkbox__hidden_par: verseInline || !localToken,
          })}
          ref={setContainerRef}>
          {verse.parallels.map(parallel => (
            <div key={parallel.verse_id} className="chapter__verse-parallel-wrap">
              {screen.desktop ? (
                <Fragment>
                  <Link
                    className="chapter__verse-parallel"
                    to={`/bible/${parallel.book_code}/glava-${parallel.chapter}/?verse=${parallel.number}`}>
                    {parallel.short}
                  </Link>
                  <div className="chapter__verse-parallel-hidden">
                    <div className="chapter__verse-parallel-hidden-title">{parallel.short}</div>
                    <div
                      className={cx('chapter__verse-parallel-hidden-text', classes)}
                      dangerouslySetInnerHTML={{
                        __html: parallel.text,
                      }}
                    />
                  </div>
                </Fragment>
              ) : (
                <Fragment>
                  <button
                    className="chapter__verse-parallel"
                    onClick={() => toggleParallel(parallel.verse_id)}>
                    {parallel.short}
                  </button>
                  {isOpen && verseId === parallel.verse_id && (
                    <div
                      className={cx('chapter__verse-parallel-hidden', {
                        parallelOpen: isOpen,
                      })}>
                      <div className="chapter__verse-parallel-header">
                        <button
                          className="chapter__parallel-close"
                          onClick={() => toggleParallel(parallel.verse_id)}>
                          <img src={closedIcon} className="chapter__parallel-img" alt="Закрыть" />
                        </button>
                        <Link
                          to={`/bible/${parallel.book_code}/glava-${parallel.chapter}/?verse=${parallel.number}/`}
                          className="chapter__verse-parallel-hidden-title chapter__verse-parallel">
                          {parallel.short}
                        </Link>
                      </div>
                      <div
                        className={cx('chapter__verse-parallel-hidden-text', classes)}
                        dangerouslySetInnerHTML={{
                          __html: parallel.text,
                        }}
                      />
                    </div>
                  )}
                </Fragment>
              )}
            </div>
          ))}
        </div>
      </div>
    );
  }
}

ChapterVerseParallel.propTypes = propTypes;
