import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { getDictonaries } from '../../../../dist/actions/Dictonaries';
import { getDictonariesSelect } from '../../../../dist/selectors/Dictonaries';

import './chapterExtraContent.scss';

const propTypes = {
  extraInfo: PropTypes.array,
  getDictonaries: PropTypes.func,
};

class ChapterVerseMapPoints extends React.Component {
  componentDidMount() {
    this.props.getDictonaries();
  }

  render() {
    const { extraInfo, dictionaries } = this.props;
    return (
      <div className={cx('extra-info not-selectable')}>
        {extraInfo.map(item => (
          <div className="extra-info__wrapper" key={item.id}>
            {item.title ? (
              <Link to={`/bibleyskaya-karta/${item.id}`} className="extra-info__title">
                {item.title}
              </Link>
            ) : (
              dictionaries.length > 0 && (
                <Link
                  to={`/all-about-bible/dictionaries/${dictionaries[item.type_id - 1].name}/${
                    item.code
                  }`}
                  className="extra-info__title">
                  {item.word}
                </Link>
              )
            )}
          </div>
        ))}
      </div>
    );
  }
}
const mapStateToProps = state => ({
  dictionaries: getDictonariesSelect(state),
});

const mapDispatchToProps = {
  getDictonaries,
};

ChapterVerseMapPoints.propTypes = propTypes;

export default connect(mapStateToProps, mapDispatchToProps)(ChapterVerseMapPoints);
