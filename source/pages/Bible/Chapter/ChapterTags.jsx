import R from 'ramda';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './chapter.scss';

class ChapterTags extends Component {
  state = {
    tags: {},
  };

  componentDidMount() {
    const tags = {};
    this.props.getFavTagsWithId.forEach(element => {
      element.tags.forEach(i => {
        tags[i] += ` ${element.number}`;
      });
    });
    this.setState({
      tags,
    });
  }

  render() {
    const { choosenTag, setSessionTag, chapterId } = this.props;
    return (
      <div className="favorite">
        <div className="favorite__verse">
          <Link to="/lk/favorites/" className="favorite__description">
            Теги
          </Link>
          <ul className="favorite__verseList__allTags tags-chapter">
            {!choosenTag &&
              !R.isEmpty(this.state.tags) &&
              Object.keys(this.state.tags).map(i => (
                <div key={i} className="allTags__chapter">
                  <p className="allTags__chapterId">
                    {chapterId}: {this.state.tags[i].replace('undefined', '')}
                  </p>
                  <Link to="/lk/favorites/">
                    <p onClick={() => setSessionTag(i)} className="allTags__tag">
                      {`${i}`}
                    </p>
                  </Link>
                </div>
              ))}
          </ul>
        </div>
      </div>
    );
  }
}

export default ChapterTags;
