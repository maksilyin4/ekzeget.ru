/* eslint-disable react/prefer-stateless-function */
import R from 'ramda';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Preloader from '~components/Preloader/Preloader';

import ChapterTabsActions from './ChapterTabActions';
import ChapterInterpretationsContent from './ChapterInterpretationsContent';
import ChapterNotContent from './ChapterNotContent/ChapterNotContent';
import ChapterNotInterpretations from './ChapterNotContent/ChapterNotInterpretations';

export default class ChapterInterpretations extends Component {
  static propTypes = {
    isLoading: PropTypes.bool,
    selected: PropTypes.number,
    handleSelect: PropTypes.func,
    userLoggedIn: PropTypes.bool,
    addedToFavorite: PropTypes.object,
    verseWithInterpretations: PropTypes.object,
    currentTranslate: PropTypes.string,
  };

  constructor(props) {
    super(props);
    this.state = {
      isVisibleCheckBox: false,
    };
  }

  componentDidMount() {
    if (!process.env.BROWSER) return;
    document.addEventListener('scroll', this.getScrollPosition, false);
  }

  componentDidUpdate(prevProps) {
    if (!process.env.BROWSER) return;
    if (!prevProps.selected && this.props.selected) {
      this.getScrollPosition();
    }
  }

  componentWillUnmount() {
    if (!process.env.BROWSER) return;
    document.removeEventListener('scroll', this.getScrollPosition, false);
  }

  onMouseEnter = () => {
    this.setState({
      isVisibleCheckBox: true,
    });
  };

  onMouseLeave = () => {
    this.setState({
      isVisibleCheckBox: false,
    });
  };

  getScrollPosition = () => {
    if (this.refs.hasOwnProperty('popup')) {
      const { scrollable } = this.refs;
      const coords = scrollable.getBoundingClientRect();
      const { top } = coords;
      if (top < 0) {
        this.setState({
          style: {
            position: 'fixed',
            right: '',
            left: coords.left + scrollable.clientWidth - 30,
            transform: 'translateX(-100%)',
            visibility: 'visible',
          },
        });
      } else {
        this.setState({
          style: {
            position: 'absolute',
            right: 30,
            left: '',
            transform: '',
            visibility: 'hidden',
          },
        });
      }
    }
  };

  render() {
    const {
      isLoading,
      selected,
      handleSelect,
      userLoggedIn,
      addedToFavorite,
      verseWithInterpretations,
      linkText,
      addFavorite,
      addNote,
      short,
      isNoteAdd,
      currentTranslate,
    } = this.props;

    const ekzegetId = R.pathOr(0, ['ekzegetId'], verseWithInterpretations);
    const chapterNotContent = R.pathOr('Толкований нет', ['ekzeget'], verseWithInterpretations);

    const ekzegetCode = R.pathOr(0, ['ekzegetCode'], verseWithInterpretations);
    const isContent = verseWithInterpretations.verses.some(i => !!i.interpretation);

    return (
      <div ref="scrollable" className="bible-tabs__content content-area-for-side">
        {isLoading ? (
          <Preloader />
        ) : (
          R.pathOr([], ['verses'], verseWithInterpretations).length > 0 && (
            <div>
              <ChapterNotInterpretations
                ekzegetId={ekzegetId}
                chapterNotContent={chapterNotContent}
                ekzegetCode={ekzegetCode}
              />
              {isContent ? (
                <ChapterInterpretationsContent
                  verseWithInterpretations={verseWithInterpretations}
                  currentTranslate={currentTranslate}
                  selected={selected}
                  handleSelect={handleSelect}
                  addedToFavorite={addedToFavorite}
                  linkText={linkText}
                  ekzegetCode={ekzegetCode}
                  isVisibleCheckBox={this.state.isVisibleCheckBox}
                />
              ) : (
                <ChapterNotContent />
              )}
            </div>
          )
        )}
        {selected && (
          <div ref="popup" className="modal-popup-addFavorite" style={this.state.style}>
            <ChapterTabsActions
              userLoggedIn={userLoggedIn}
              addFavorite={addFavorite}
              addNote={addNote}
              short={short}
              isNoteAdd={isNoteAdd}
            />
          </div>
        )}
      </div>
    );
  }
}
