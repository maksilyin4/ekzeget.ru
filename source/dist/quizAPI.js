import { _AXIOS } from './ApiConfig';
import { DEFAULT_QUIZ_URL, pushToQuestion } from './utils';

export const getTutorial = () =>
  _AXIOS({ url: 'queeze/tutorial/' })
    .then(({ url }) => ({ url, error: null }))
    .catch(({ message, status }) => ({ url: null, error: { message, status } }));

export const getAnswer = (quizId, questionId) => {
  if (quizId && questionId) {
    return _AXIOS({ url: `/queeze/answer/${quizId}/${questionId}/` });
  }

  return console.log('Не верно построен запрос');
};

export const createQuiz = (
  paramText,
  paramInterpretation,
  paramBooks,
  paramChapterId = '',
  paramVerseId = '',
  paramQuestionId = '',
  push,
  getCurrentQueeze,
  updateWarningMsg,
  updateLoadQuiz,
) =>
  _AXIOS({
    url: `/queeze/create/${paramText}${paramInterpretation}${paramBooks}${paramChapterId}${paramVerseId}${paramQuestionId}`,
    method: 'POST',
  })
    .then(({ queeze }) => {
      // В случае если впервый раз пользователь начинает
      getCurrentQueeze(true, queeze);
      pushToQuestion(queeze, push);
    })
    .catch(() => {
      // Если пользователь хочет начать новую, когда есть не завершенная
      updateWarningMsg(['Викторина под заданные вами настройки не найдена.']);
      updateLoadQuiz(false);
    });

export const createQuizPlan = (book_id, paramChapterId, planId, getCurrentQueeze, push, day, questionId) =>
  _AXIOS({
    url: `/queeze/create/?is_related_to_text=1&is_related_to_interpretations=0&chapter_id=${paramChapterId}&book_ids[]=${book_id}&plan_id=${planId}${questionId ? `&question_id=${questionId}` : ''}`,
    method: 'POST',
  }).then(({ queeze }) => {
    getCurrentQueeze(true, queeze);
    pushToQuestion(queeze, push, { day, planId });
  });

export const completeQuiz = (push, url) => {
  if (push && url) {
    _AXIOS({ url: 'queeze/complete/' }).then(() => push(url));
  } else {
    _AXIOS({ url: 'queeze/complete/' });
  }
};

export const continueQuiz = (
  id,
  getCurrentQueeze,
  push,
  currentQuestion = DEFAULT_QUIZ_URL,
  params = {},
) => {
  _AXIOS({ url: `/queeze/continue/${id}/` }).then(({ queeze }) => {
    getCurrentQueeze(true, queeze);
    push(`/bibleyskaya-viktorina/voprosi/${currentQuestion}/`, params.state);
  });
};
