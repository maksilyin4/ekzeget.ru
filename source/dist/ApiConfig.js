import axios from 'axios';
import R from 'ramda';

import { LS } from './browserUtils';
import { store } from '../getStore';
import { needAuth } from './actions/User';
import * as https from 'https';

export const AXIOS = axios.create({
  baseURL: '/api/v1/',
  // timeout: 60000,
  timeout: 180000,
  withCredentials: true,
});

export const _AXIOS = ({
  url,
  method = 'GET',
  data,
  isAuth = false,
  cookies,
  query,
  cancelToken,
}) => {
  const params = { url, method, headers: { 'Cache-Control': 'max-age=3600' } };
  // const params = {
  //   url,
  //   method,
  //   headers: {
  //     'Cache-Control': 'max-age=3600',
  //     Connection: 'Keep-Alive',
  //     'Keep-Alive': 'timeout=0, max=0',
  //   },
  // };
  let error = null;

  if (cancelToken) {
    params.cancelToken = cancelToken;
  }
  if (query) {
    params.params = query;
  }
  if (data) {
    params.data = data;
    params.headers['Content-Type'] = 'application/x-www-form-urlencoded';
  }

  if (cookies) {
    params.headers.Cookie = cookies;
  }

  if (isAuth) {
    params.headers['X-Authentication-Token'] = LS.get('token');
  }

  if (process.env.NODE_ENV === 'development') {
    params.httpsAgent = new https.Agent({
      rejectUnauthorized: false,
    });
  }

  return AXIOS(params)
    .then(response => {
      const { data, status } = response;

      if (status === 200) {
        // SSR
        return !process.env.BROWSER ? response : data;
      }

      error = { status, message: data.error };
      throw error;
    })
    .catch(({ response, message }) => {
      const data = R.pathOr('Catch Data error in Api Config', ['data'], response);
      const status = R.pathOr('Catch Status error in Api Config', ['status'], response);
      if (!error) {
        error = { status, message, data };
      }

      if (status === 401) {
        store.dispatch(needAuth());
      }

      return Promise.reject(error);
    });
};
