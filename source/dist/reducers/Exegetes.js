import {
  EXEGETES_FETCH,
  EXEGETES_FETCH_SUCCESS,
  EXEGETES_FETCH_FAILURE,
} from '../actions/Exegetes';

const initialState = {
  isLoading: true,
  isLoaded: false,
  isFailed: false,
  data: {},
  error: null,
};

const Exegetes = (state = initialState, action) => {
  switch (action.type) {
    case EXEGETES_FETCH:
      return {
        ...state,
        isLoading: true,
        isLoaded: false,
        isFailed: false,
      };
    case EXEGETES_FETCH_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isLoaded: true,
        isFailed: false,
        data: action.payload.data,
      };
    case EXEGETES_FETCH_FAILURE:
      return {
        ...state,
        isLoading: false,
        isLoaded: false,
        isFailed: true,
        error: action.error,
      };
    default:
      return {
        ...state,
      };
  }
};

export default Exegetes;
