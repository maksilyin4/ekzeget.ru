import { combineReducers } from 'redux';

import {
  LAST_VIDEOS_PENDING,
  LAST_VIDEOS_FULFILLED,
  LAST_VIDEOS_REJECTED,
} from '../actions/LastVideos';

const isFetching = (state = false, action) => {
  switch (action.type) {
    case LAST_VIDEOS_REJECTED:
    case LAST_VIDEOS_FULFILLED:
      return false;
    case LAST_VIDEOS_PENDING:
      return true;
    default:
      return state;
  }
};

const error = (state = '', action) => {
  switch (action.type) {
    case LAST_VIDEOS_REJECTED:
      return action.payload;
    default:
      return state;
  }
};

const intitalState = {
  lastVideosOldTestament: [],
  lastVideosNewTestament: [],
  data: [],
};

const lastVideos = (state = intitalState, { payload, type, other }) => {
  const setName =
    other === 'video-vethogo-zaveta' ? 'lastVideosOldTestament' : 'lastVideosNewTestament';
  switch (type) {
    case LAST_VIDEOS_FULFILLED:
      return { ...state, [setName]: payload, data: payload };
    default:
      return state;
  }
};

const lastVideosFirst = null;
const lastVideosSecond = null;

const reducer = combineReducers({
  isFetching,
  error,
  lastVideos,
  lastVideosFirst,
  lastVideosSecond,
});

export default reducer;
