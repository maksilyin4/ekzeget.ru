import {
  INTERPRETATIONS_FETCH,
  INTERPRETATIONS_FETCH_SUCCESS,
  INTERPRETATIONS_FETCH_FAILURE,
} from '../actions/ChapterInterpretations';

const initialState = {
  isLoading: true,
  isLoaded: false,
  isFailed: false,
  data: {},
  error: null,
};

const Interpretations = (state = initialState, action) => {
  switch (action.type) {
    case INTERPRETATIONS_FETCH:
      return {
        ...state,
        isLoading: true,
        isLoaded: false,
        isFailed: false,
      };
    case INTERPRETATIONS_FETCH_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isLoaded: true,
        isFailed: false,
        data: action.payload.data,
      };
    case INTERPRETATIONS_FETCH_FAILURE:
      return {
        ...state,
        isLoading: false,
        isLoaded: false,
        isFailed: true,
        error: action.error,
      };
    default:
      return {
        ...state,
      };
  }
};

export default Interpretations;
