import R from 'ramda';
import { combineReducers } from 'redux';
import {
  REQUEST_FAVORITES,
  RECEIVE_FAVORITES_SUCCESS,
  RECEIVE_FAVORITES_INTER_SUCCESS,
  RECEIVE_FAVORITES_FAILURE,
  DELETE_FAVORITE_VERSE,
  DELETE_FAVORITE_INTER,
  ADD_FAV_VERSE,
  ADD_FAV_INTERP,
  GET_NOTES,
  UPDATE_NOTES,
  DELETE_NOTE,
  CHOOSE_TAG,
  UPDATE_FAV_VERSE,
  CREATE_NOTE,
  SET_SESSION_TAG,
} from '../actions/favorites';

const isFetching = (state = false, action) => {
  switch (action.type) {
    case REQUEST_FAVORITES:
      return true;
    case RECEIVE_FAVORITES_SUCCESS:
    case RECEIVE_FAVORITES_INTER_SUCCESS:
    case RECEIVE_FAVORITES_FAILURE:
    case DELETE_FAVORITE_VERSE:
    case DELETE_FAVORITE_INTER:
    case GET_NOTES:
    case UPDATE_NOTES:
    case DELETE_NOTE:
    case UPDATE_FAV_VERSE:
    case CREATE_NOTE:
      return false;
    default:
      return state;
  }
};

// Object with all verse added to favorite
const verseList = (state = {}, action) => {
  switch (action.type) {
    case RECEIVE_FAVORITES_SUCCESS:
      return action.payload.reduce(
        (obj, fav) => ({
          ...obj,
          [R.pathOr(0, ['verse', 'id'], fav)]: {
            verse: fav.verse,
            color: fav.color,
          },
        }),
        {},
      );
    case UPDATE_FAV_VERSE:
      return {
        ...state,
        [action.payload.verseId]: {
          ...state[action.payload.verseId],
          color: action.payload.color,
        },
      };
    case RECEIVE_FAVORITES_FAILURE:
    case 'USER_AUTH_NEED':
      return {};
    case ADD_FAV_VERSE:
      return {
        ...state,
        ...action.payload.verse.reduce(
          (sum, verse) => ({
            ...sum,
            [verse]: {
              color: action.payload.color,
              verse: { id: verse },
            },
          }),
          {},
        ),
      };

    case DELETE_FAVORITE_VERSE:
      if (action.payload.verseId) {
        delete state[action.payload.verseId];
      }
      return { ...state };
    default:
      return state;
  }
};

// list of favorites verse IDs
const favoritesVerseIds = (state = [], action) => {
  switch (action.type) {
    case RECEIVE_FAVORITES_SUCCESS:
      return action.payload.map(fav => fav.id);
    case RECEIVE_FAVORITES_FAILURE:
    case 'USER_AUTH_NEED':
      return [];
    case DELETE_FAVORITE_VERSE:
      return state.filter(fav => fav !== action.payload.id);
    default:
      return state;
  }
};

// object of favorite verses by ids
const favoritesVerseByIds = (state = {}, action) => {
  switch (action.type) {
    case RECEIVE_FAVORITES_SUCCESS:
      return action.payload.reduce(
        (sum, fav) => ({
          ...sum,
          [fav.id]: {
            ...fav,
            verse: R.pathOr(0, ['verse', 'id'], fav),
          },
        }),
        {},
      );
    case UPDATE_FAV_VERSE:
      return {
        ...state,
        [action.payload.id]: {
          ...state[action.payload.id],
          color: action.payload.color,
          tags: action.payload.tags,
        },
      };
    case RECEIVE_FAVORITES_FAILURE:
    case 'USER_AUTH_NEED':
      return {};
    default:
      return state;
  }
};

const allTags = (state = [], action) => {
  switch (action.type) {
    case RECEIVE_FAVORITES_SUCCESS:
      return R.uniq(action.payload.reduce((sum, i) => [...sum, ...i.tags.split(',')], []));
    case UPDATE_FAV_VERSE:
      return R.uniq([...state, ...action.payload.tags.split(',')]);
    default:
      return state;
  }
};

const chosenTag = (state = '', action) => {
  switch (action.type) {
    case CHOOSE_TAG:
      return action.payload;
    default:
      return state;
  }
};

// list of favorites interpretations with id verse
const favoritesInter = (state = [], action) => {
  switch (action.type) {
    case RECEIVE_FAVORITES_INTER_SUCCESS:
      return action.payload.map(inter => ({
        id: inter.id,
        ekzeget: {
          name: R.pathOr('', ['ekzeget', 'name'], inter),
          id: R.pathOr(1, ['ekzeget', 'id'], inter),
          interpretationLink: `/bible/${R.pathOr(
            'mf',
            ['verse', 0, 'book', 'code'],
            inter,
          )}/glava-${R.pathOr('1', ['verse', 0, 'chapter'], inter)}/stih-${R.pathOr(
            '1',
            ['verse', 0, 'number'],
            inter,
          )}/${R.pathOr('', ['ekzeget', 'code'], inter)}`,
        },
        verse: {
          short: R.pathOr('', ['verse', 0, 'short'], inter),
          verseLink: `/bible/${R.pathOr(
            'mf',
            ['verse', 0, 'book', 'code'],
            inter,
          )}/glava-${R.pathOr('1', ['verse', 0, 'chapter'], inter)}/stih-${R.pathOr(
            '1',
            ['verse', 0, 'number'],
            inter,
          )}/`,
          verseText: R.pathOr('', ['verse', 0, 'text'], inter),
        },
        interpretation: {
          id: inter.interpretation.id,
          comment: inter.interpretation.comment,
        },
      }));
    case RECEIVE_FAVORITES_FAILURE:
    case 'USER_AUTH_NEED':
      return [];
    case DELETE_FAVORITE_INTER:
      return state.filter(fav => fav.id !== action.payload);
    default:
      return state;
  }
};

const interList = (state = {}, action) => {
  switch (action.type) {
    case RECEIVE_FAVORITES_FAILURE:
    case 'USER_AUTH_NEED':
      return {};
    case RECEIVE_FAVORITES_INTER_SUCCESS:
      return action.payload.reduce(
        (acc, { interpretation }) => {
          return { ...acc, [interpretation.id]: 'here will be color' };
        },
        [state],
      );

    case ADD_FAV_INTERP:
      return {
        ...state,
        [action.payload.interpretation_id]: 'here will be color',
      };
    default:
      return state;
  }
};

// TODO: it can be normalized
const notes = (state = [], action) => {
  switch (action.type) {
    case GET_NOTES:
      return action.payload;
    case RECEIVE_FAVORITES_FAILURE:
    case 'USER_AUTH_NEED':
      return [];
    case CREATE_NOTE:
      return [...state, action.payload];
    case UPDATE_NOTES:
      // eslint-disable-next-line no-case-declarations
      const noteIndex = state.findIndex(i => i.id === action.payload.note_id);
      return [
        ...state.slice(0, noteIndex),
        { ...state[noteIndex], text: action.payload.text },
        ...state.slice(noteIndex + 1),
      ];
    case DELETE_NOTE:
      return state.filter(i => i.id !== action.payload);
    default:
      return state;
  }
};

const sessionTag = (state = {}, action) => {
  switch (action.type) {
    case SET_SESSION_TAG:
      return action.payload;
    default:
      return state;
  }
};

const reducer = combineReducers({
  isFetching,
  verseList,
  favoritesVerseByIds,
  favoritesVerseIds,
  favoritesInter,
  interList,
  notes,
  allTags,
  chosenTag,
  sessionTag,
});

export default reducer;
