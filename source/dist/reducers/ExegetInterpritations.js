import {
  EXEGET_INTERPRITATIONS_FETCH,
  EXEGET_INTERPRITATIONS_FETCH_SUCCESS,
  EXEGET_INTERPRITATIONS_FETCH_FAILURE,
} from '../actions/ExegetInterpritations';

const initialState = {
  isLoading: true,
  isLoaded: false,
  isFailed: false,
  data: {},
  error: null,
};

const ExegetInterpritations = (state = initialState, action) => {
  switch (action.type) {
    case EXEGET_INTERPRITATIONS_FETCH:
      return {
        ...state,
        isLoading: true,
        isLoaded: false,
        isFailed: false,
      };
    case EXEGET_INTERPRITATIONS_FETCH_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isLoaded: true,
        isFailed: false,
        data: action.payload.data,
      };
    case EXEGET_INTERPRITATIONS_FETCH_FAILURE:
      return {
        ...state,
        isLoading: false,
        isLoaded: false,
        isFailed: true,
        error: action.error,
      };
    default:
      return {
        ...state,
      };
  }
};

export default ExegetInterpritations;
