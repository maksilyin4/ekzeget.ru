import {
  VIDEO_PLAYLIST_FETCH,
  VIDEO_PLAYLIST_SUCCESS,
  VIDEO_PLAYLIST_FAILURE,
  VIDEO_DETAIL_FETCH,
  VIDEO_DETAIL_SUCCESS,
  VIDEO_DETAIL_FAILURE,
  VIDEO_DETAIL_CLEAR,
} from '../actions/Video';

const initialState = {
  videoPlaylistLoading: true,
  videoPlaylistLoaded: false,
  videoPlaylistFailed: false,
  videoDetailLoading: true,
  videoDetailLoaded: false,
  videoDetailFailed: false,
  videoPlaylist: {},
  videoDetail: {},
  error: null,
};

const Video = (state = initialState, action) => {
  switch (action.type) {
    case VIDEO_PLAYLIST_FETCH:
      return {
        ...state,
        videoPlaylistLoading: true,
        videoPlaylistLoaded: false,
        videoPlaylistFailed: false,
      };
    case VIDEO_PLAYLIST_SUCCESS:
      return {
        ...state,
        videoPlaylistLoading: false,
        videoPlaylistLoaded: true,
        videoPlaylistFailed: false,
        videoPlaylist: action.payload.data,
      };
    case VIDEO_PLAYLIST_FAILURE:
      return {
        ...state,
        videoPlaylistLoading: false,
        videoPlaylistLoaded: false,
        videoPlaylistFailed: true,
        error: action.error,
      };
    case VIDEO_DETAIL_FETCH:
      return {
        ...state,
        videoDetailLoading: true,
        videoDetailLoaded: false,
        videoDetailFailed: false,
      };
    case VIDEO_DETAIL_SUCCESS:
      return {
        ...state,
        videoDetailLoading: false,
        videoDetailLoaded: true,
        videoDetailFailed: false,
        videoDetail: action.payload.data,
      };
    case VIDEO_DETAIL_FAILURE:
      return {
        ...state,
        videoDetailLoading: false,
        videoDetailLoaded: false,
        videoDetailFailed: true,
        error: action.error,
      };
    case VIDEO_DETAIL_CLEAR:
      return { ...state, videoDetail: {} };
    default:
      return {
        ...state,
      };
  }
};

export default Video;
