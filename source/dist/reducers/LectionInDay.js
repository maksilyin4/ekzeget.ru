import { LID_FETCH, LID_FETCH_SUCCESS, LID_FETCH_FAILURE } from '../actions/LectionInDay';

const initialState = {
  isLoading: true,
  isLoaded: false,
  isFailed: false,
  data: {
    reading: {
      title: [],
    },
  },
  error: null,
};

const LID = (state = initialState, action) => {
  switch (action.type) {
    case LID_FETCH:
      return {
        ...state,
        isLoading: true,
        isLoaded: false,
        isFailed: false,
      };
    case LID_FETCH_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isLoaded: true,
        isFailed: false,
        data: action.payload.data,
      };
    case LID_FETCH_FAILURE:
      return {
        ...state,
        isLoading: false,
        isLoaded: false,
        isFailed: true,
        error: action.error,
      };
    default:
      return state;
  }
};

export default LID;
