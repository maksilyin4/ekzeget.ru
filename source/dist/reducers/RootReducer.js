import { combineReducers } from 'redux';
import books from './Bible';
import book from './Book';
import chapter from './Chapter';
import verse from './Verse';
import preachingBible from './BiblePreaching';
import preaching from './preaching';
import interpretators from './ChapterInterpretators';
import chapterInterpretations from './ChapterInterpretations';
import interpretation from './Interpretation';
import exegetes from './Exegetes';
import exeget from './Exeget';
import exegetInterpritations from './ExegetInterpritations';
import dictonaries from './Dictonaries';
import user from './user';
import readingPlan from './ReadingPlan';
import lid from './LectionInDay';
import bookmarks from './Bookmarks';
import favorites from './favorites';
import myInterpretations from './myInterpretations';
import lecture from './Lecture';
import video from './Video';
import search from './search';
import translates from './translates';
import screen from './screen';
import bibleMap from './BibleMap';
import motivator from './Motivators';
import bibleGroupUpdates from './BibleGroupUpdates';
import playLists from './PlayLists';
import lastVideos from './LastVideos';
import mediaLibrary from './BibleMediaLibrary';
import quiz from './Quiz';
import metaData from '../../store/metaData/reducer';
import browsers from '../../store/browser/reducer';
import booksList from '../../store/booksList/reducer';
import questionsBible from '../../store/bible/bibleMediaQuestions/reducer';
import mediateka from '../../store/mediateka/reducer';
import readingCreatePlan from '../../store/createPlan/reducer';
import groups from '../../store/groups/reducer';
import commentsPlan from '../../store/commentsPlan/reducer';

const rootReducer = combineReducers({
  books,
  book,
  chapter,
  verse,
  interpretators,
  chapterInterpretations,
  interpretation,
  preaching,
  preachingBible,
  exegetes,
  exeget,
  exegetInterpritations,
  dictonaries,
  user,
  readingPlan,
  lid,
  bookmarks,
  lecture,
  video,
  favorites,
  myInterpretations,
  search,
  translates,
  screen,
  bibleMap,
  motivator,
  bibleGroupUpdates,
  playLists,
  lastVideos,
  mediaLibrary,
  quiz,
  metaData,
  browsers,
  booksList,
  questionsBible,
  mediateka,
  readingCreatePlan,
  groups,
  commentsPlan,
});

export default rootReducer;
