import {
  INTERPRETATORS_FETCH,
  INTERPRETATORS_FETCH_SUCCESS,
  INTERPRETATORS_FETCH_FAILURE,
} from '../actions/ChapterInterpretators';

const initialState = {
  isLoading: false,
  isLoaded: false,
  isFailed: false,
  data: {},
  error: null,
};

const Interpretators = (state = initialState, action) => {
  switch (action.type) {
    case INTERPRETATORS_FETCH:
      return {
        ...state,
        isLoading: true,
        isLoaded: false,
        isFailed: false,
      };
    case INTERPRETATORS_FETCH_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isLoaded: true,
        isFailed: false,
        data: action.payload.data,
      };
    case INTERPRETATORS_FETCH_FAILURE:
      return {
        ...state,
        isLoading: false,
        isLoaded: false,
        isFailed: true,
        error: action.error,
      };
    default:
      return {
        ...state,
      };
  }
};

export default Interpretators;
