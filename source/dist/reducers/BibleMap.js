import { combineReducers } from 'redux';

import {
  BIBLE_MAP_GET_ERROR,
  BIBLE_MAP_GET_SUCCESS,
  BIBLE_MAP_PEDING,
  BIBLE_MAP_BY_BOOK_PENDING,
  BIBLE_MAP_BY_BOOK_FULFILLED,
  BIBLE_MAP_BY_BOOK_REJECTED,
  BIBLE_MAP_POINT_PENDING,
  BIBLE_MAP_POINT_FULFILLED,
  BIBLE_MAP_POINT_REJECTED,
} from '../actions/BibleMap';

const markersData = (state = [], action) => {
  switch (action.type) {
    case BIBLE_MAP_GET_SUCCESS:
    case BIBLE_MAP_BY_BOOK_FULFILLED:
    case BIBLE_MAP_POINT_FULFILLED:
      return action.payload;
    case BIBLE_MAP_GET_ERROR:
    case BIBLE_MAP_BY_BOOK_REJECTED:
    case BIBLE_MAP_POINT_REJECTED:
      return action.error;
    default:
      return state;
  }
};

const isFetching = (state = true, action) => {
  switch (action.type) {
    case BIBLE_MAP_PEDING:
    case BIBLE_MAP_BY_BOOK_PENDING:
    case BIBLE_MAP_POINT_PENDING:
      return true;
    case BIBLE_MAP_GET_ERROR:
    case BIBLE_MAP_GET_SUCCESS:
    case BIBLE_MAP_BY_BOOK_FULFILLED:
    case BIBLE_MAP_BY_BOOK_REJECTED:
    case BIBLE_MAP_POINT_FULFILLED:
    case BIBLE_MAP_POINT_REJECTED:
      return false;
    default:
      return state;
  }
};

const reducer = combineReducers({
  markersData,
  isFetching,
});

export default reducer;
