import { combineReducers } from 'redux';

import {
  SET_SCREEN_PHONE,
  SET_SCREEN_TABLET,
  SET_SCREEN_DESKTOP,
  SET_SCREEN_TABLET_MEDIUM,
} from '../actions/screen';

const phone = (state = false, { type }) => {
  switch (type) {
    case SET_SCREEN_PHONE:
      return true;
    case SET_SCREEN_TABLET:
    case SET_SCREEN_TABLET_MEDIUM:
    case SET_SCREEN_DESKTOP:
      return false;
    default:
      return state;
  }
};
const tablet = (state = false, { type }) => {
  switch (type) {
    case SET_SCREEN_TABLET:
      return true;
    case SET_SCREEN_TABLET_MEDIUM:
    case SET_SCREEN_PHONE:
    case SET_SCREEN_DESKTOP:
      return false;
    default:
      return state;
  }
};

const tabletMedium = (state = false, { type }) => {
  switch (type) {
    case SET_SCREEN_TABLET_MEDIUM:
      return true;
    case SET_SCREEN_DESKTOP:
    case SET_SCREEN_TABLET:
    case SET_SCREEN_PHONE:
      return false;
    default:
      return state;
  }
};

const desktop = (state = false, { type }) => {
  switch (type) {
    case SET_SCREEN_DESKTOP:
      return true;
    case SET_SCREEN_TABLET_MEDIUM:
    case SET_SCREEN_TABLET:
    case SET_SCREEN_PHONE:
      return false;
    default:
      return state;
  }
};

export default combineReducers({
  phone,
  tablet,
  desktop,
  tabletMedium,
});
