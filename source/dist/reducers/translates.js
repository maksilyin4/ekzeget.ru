import { combineReducers } from 'redux';

import {
  RECEIVE_TRANSLATES_FETCH,
  RECEIVE_TRANSLATES_SUCCESS,
  RECEIVE_TRANSLATES_FAILURE,
  CHOOSE_TRANSLATE,
} from '../actions/translates';

const translates = (state = { data: [], isLoading: true }, action) => {
  switch (action.type) {
    case RECEIVE_TRANSLATES_FETCH: {
      return { ...state, isLoading: true };
    }
    case RECEIVE_TRANSLATES_SUCCESS:
      return { ...state, isLoading: false, data: action.payload };
    case RECEIVE_TRANSLATES_FAILURE:
      return { ...state, isLoading: false, data: action.error };
    default:
      return state;
  }
};

const currentTranslate = (state = {}, action) => {
  switch (action.type) {
    case CHOOSE_TRANSLATE:
      return action.payload;
    default:
      return state;
  }
};

const reducer = combineReducers({
  translates,
  currentTranslate,
});

export default reducer;
