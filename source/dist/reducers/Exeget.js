import {
  EXEGET_FETCH,
  EXEGET_FETCH_SUCCESS,
  EXEGET_FETCH_CLEAR,
  EXEGET_FETCH_FAILURE,
} from '../actions/Exeget';

const initialState = {
  isLoading: true,
  data: {},
  error: null,
};

const Exeget = (state = initialState, action) => {
  switch (action.type) {
    case EXEGET_FETCH:
      return {
        ...state,
        isLoading: true,
        error: null,
      };
    case EXEGET_FETCH_SUCCESS:
      return {
        ...state,
        isLoading: false,
        data: action.payload.data,
      };
    case EXEGET_FETCH_CLEAR:
      return {
        ...state,
        isLoading: true,
        data: {},
        error: null,
      };
    case EXEGET_FETCH_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    default:
      return {
        ...state,
      };
  }
};

export default Exeget;
