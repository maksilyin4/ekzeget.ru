import { combineReducers } from 'redux';
import {
  BOOK_FETCH,
  BOOK_FETCH_SUCCESS,
  BOOK_FETCH_FAILURE,
  BOOKINFO_FETCH,
  BOOKINFO_FETCH_SUCCESS,
  BOOKINFO_FETCH_FAILURE,
} from '../actions/Book';

const isLoadingBookData = (state = false, action) => {
  switch (action.type) {
    case BOOK_FETCH:
      return true;
    case BOOK_FETCH_SUCCESS:
    case BOOK_FETCH_FAILURE:
      return false;
    default:
      return state;
  }
};

const isLoadingBookInfo = (state = false, action) => {
  switch (action.type) {
    case BOOKINFO_FETCH:
      return true;
    case BOOKINFO_FETCH_SUCCESS:
    case BOOKINFO_FETCH_FAILURE:
      return false;
    default:
      return state;
  }
};

const bookData = (state = { chapters: [] }, action) => {
  switch (action.type) {
    case BOOK_FETCH_SUCCESS:
      return action.payload;
    case BOOK_FETCH_FAILURE:
    default:
      return state;
  }
};

const bookInfo = (state = {}, action) => {
  switch (action.type) {
    case BOOKINFO_FETCH_SUCCESS:
      return action.payload;
    case BOOKINFO_FETCH_FAILURE:
    default:
      return state;
  }
};

const reducer = combineReducers({
  isLoadingBookData,
  isLoadingBookInfo,
  bookData,
  bookInfo,
});

export default reducer;
