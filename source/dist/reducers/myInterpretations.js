import { combineReducers } from 'redux';

import {
  REQUEST_MY_INTER,
  RECEIVE_MY_INTER_SUCCESS,
  RECEIVE_MY_INTER_FAILURE,
} from '../actions/myInterpretations';

const isFetching = (state = false, action) => {
  switch (action.type) {
    case REQUEST_MY_INTER:
      return true;
    case RECEIVE_MY_INTER_SUCCESS:
    case RECEIVE_MY_INTER_FAILURE:
    case 'USER_AUTH_NEED':
      return false;
    default:
      return state;
  }
};

const myIntIds = (state = [], action) => {
  switch (action.type) {
    case RECEIVE_MY_INTER_SUCCESS:
      return action.payload.map(i => i.id);
    case RECEIVE_MY_INTER_FAILURE:
    case 'USER_AUTH_NEED':
      return [];
    default:
      return state;
  }
};

const myIntById = (state = {}, action) => {
  switch (action.type) {
    case RECEIVE_MY_INTER_SUCCESS:
      return action.payload.reduce(
        (obj, int) => ({
          ...obj,
          [int.id]: int,
        }),
        {},
      );
    case RECEIVE_MY_INTER_FAILURE:
    case 'USER_AUTH_NEED':
      return {};
    default:
      return state;
  }
};

const reducer = combineReducers({
  isFetching,
  myIntIds,
  myIntById,
});

export default reducer;
