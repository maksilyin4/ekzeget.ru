import { combineReducers } from 'redux';
import {
  QUEEZE_FAILURE,
  QUEEZE_SUCCESS,
  QUEEZE_FETCH,
  CURRENT_QUEEZE_FAILURE,
  CURRENT_QUEEZE_SUCCESS,
  CURRENT_QUEEZE_FETCH,
  QUESTION_FAILURE,
  QUESTION_SUCCESS,
  QUESTIONS_FAILURE,
  QUESTIONS_FETCH,
  QUESTIONS_SUCCESS,
  CURRENT_QUEEZE_SUCCESS_PLAN,
} from '../actions/Quiz';

const isLoadingQueeze = (state = true, action) => {
  switch (action.type) {
    case QUEEZE_FETCH:
      return true;
    case QUEEZE_SUCCESS:
    case QUEEZE_FAILURE:
      return false;
    default:
      return state;
  }
};

const isLoadingList = (state = true, action) => {
  switch (action.type) {
    case CURRENT_QUEEZE_FETCH:
      return true;
    case CURRENT_QUEEZE_SUCCESS:
    case CURRENT_QUEEZE_FAILURE:
      return false;
    default:
      return state;
  }
};

const queezeData = (state = [], action) => {
  switch (action.type) {
    case QUEEZE_SUCCESS:
      return action.payload;
    case QUEEZE_FAILURE:
    default:
      return state;
  }
};

const completedQueezeData = (state = [], action) => {
  switch (action.type) {
    case CURRENT_QUEEZE_SUCCESS:
      return action.payload;
    case CURRENT_QUEEZE_FAILURE:
    default:
      return state;
  }
};

const completedQueezePlan = (state = null, action) => {
  switch (action.type) {
    case CURRENT_QUEEZE_SUCCESS_PLAN:
      return action.payload;
    default:
      return state;
  }
};

const currentQuestionData = (state = {}, action) => {
  switch (action.type) {
    case QUESTION_SUCCESS:
      return action.payload;
    case QUESTION_FAILURE:
    default:
      return state;
  }
};

const isLoadedCurrentQuestion = (state = false, action) => {
  switch (action.type) {
    case QUESTION_SUCCESS:
      return true;
    case QUESTION_FAILURE:
    default:
      return state;
  }
};

const questionsData = (state = {}, action) => {
  switch (action.type) {
    case QUESTIONS_SUCCESS:
      return action.payload;
    case QUESTIONS_FAILURE:
    default:
      return state;
  }
};

const isLoadedQuestions = (state = false, action) => {
  switch (action.type) {
    case QUESTIONS_SUCCESS:
    case QUESTIONS_FAILURE:
      return true;
    case QUESTIONS_FETCH:
    default:
      return state;
  }
};

const errorAllQuestions = (state = {}, action) => {
  switch (action.type) {
    case QUESTIONS_FAILURE:
      return action.error;
    case QUESTIONS_SUCCESS:
    default:
      return state;
  }
};

const errorQuestion = (state = '', action) => {
  switch (action.type) {
    case QUESTION_FAILURE:
      return action.error;
    case QUESTION_SUCCESS:
    default:
      return state;
  }
};

const reducer = combineReducers({
  isLoadingQueeze,
  queezeData,
  isLoadingList,
  completedQueezeData,
  completedQueezePlan,
  currentQuestionData,
  isLoadedCurrentQuestion,
  questionsData,
  isLoadedQuestions,
  errorAllQuestions,
  errorQuestion,
});

export default reducer;
