import { combineReducers } from 'redux';
import {
  PREACHER_FETCH,
  PREACHER_FETCH_SUCCESS,
  PREACHER_FETCH_FAILURE,
  PREACHING_FETCH,
  PREACHING_FETCH_SUCCESS,
  PREACHING_FETCH_FAILURE,
  PREACHING_EXCUSE_FETCH,
  PREACHING_EXCUSE_FETCH_SUCCESS,
  PREACHING_EXCUSE_FETCH_FAILURE,
  PREACHING_CELEBRATION_FETCH,
  PREACHING_CELEBRATION_FETCH_SUCCESS,
  PREACHING_CELEBRATION_FETCH_FAILURE,
  PREACHING_FETCH_CLEAR,
  PREACHING_FETCH_CLEAR_ERROR,
  PREACHING_FETCH_ERROR,
} from '../actions/Preaching';

const isPreaching = (state = false, action) => {
  switch (action.type) {
    case PREACHING_FETCH:
      return true;

    case PREACHING_FETCH_SUCCESS:
    case PREACHING_FETCH_FAILURE:
      return false;
    default:
      return state;
  }
};

const isFetching = (state = false, action) => {
  switch (action.type) {
    case PREACHER_FETCH:
    case PREACHING_EXCUSE_FETCH:
    case PREACHING_CELEBRATION_FETCH:
      return true;
    case PREACHER_FETCH_SUCCESS:
    case PREACHER_FETCH_FAILURE:
    case PREACHING_EXCUSE_FETCH_SUCCESS:
    case PREACHING_EXCUSE_FETCH_FAILURE:
    case PREACHING_CELEBRATION_FETCH_SUCCESS:
    case PREACHING_CELEBRATION_FETCH_FAILURE:
      return false;
    default:
      return state;
  }
};

const isError = (state = false, action) => {
  switch (action.type) {
    case PREACHER_FETCH:
    case PREACHING_EXCUSE_FETCH:
    case PREACHING_CELEBRATION_FETCH:
    case PREACHING_FETCH_CLEAR_ERROR:
      return false;

    case PREACHING_FETCH_ERROR:
    case PREACHER_FETCH_FAILURE:
    case PREACHING_EXCUSE_FETCH_FAILURE:
    case PREACHING_CELEBRATION_FETCH_FAILURE:
      return true;
    default:
      return state;
  }
};

const excuses = (state = [], action) => {
  switch (action.type) {
    case PREACHING_EXCUSE_FETCH_SUCCESS:
      return action.payload;
    default:
      return state;
  }
};

const preacher = (state = [], action) => {
  switch (action.type) {
    case PREACHER_FETCH:
      return [];
    case PREACHER_FETCH_SUCCESS:
      return action.payload;
    default:
      return state;
  }
};

const preaching = (state = {}, action) => {
  switch (action.type) {
    case PREACHING_FETCH_SUCCESS:
      return action.payload.data;
    case PREACHING_FETCH_CLEAR:
      return {};
    default:
      return state;
  }
};

const celebrations = (state = [], action) => {
  switch (action.type) {
    case PREACHING_CELEBRATION_FETCH_SUCCESS:
      return action.payload;
    default:
      return state;
  }
};

const reducer = combineReducers({
  isPreaching,
  isFetching,
  excuses,
  preacher,
  preaching,
  celebrations,
  isError,
});

export default reducer;
