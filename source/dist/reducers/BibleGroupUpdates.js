import { combineReducers } from 'redux';

import {
  GET_BIBLE_GROUP_UPDATES_FULFILLED,
  GET_BIBLE_GROUP_UPDATES_REJECTED,
  GET_BIBLE_GROUP_UPDATES_PENDING,
  GET_BIBLE_GROUP_NOTES_UPDATES_FULFILLED,
  GET_BIBLE_GROUP_NOTES_UPDATES_REJECTED,
  GET_BIBLE_GROUP_NOTES_UPDATES_PENDING,
  GET_BIBLE_GROUP_NOTES_UPDATES_CLEAR,
} from '../actions/BibleGroupUpdates';

const isFetchingGroupUpdates = (state = false, action) => {
  switch (action.type) {
    case GET_BIBLE_GROUP_UPDATES_REJECTED:
    case GET_BIBLE_GROUP_UPDATES_FULFILLED:
      return false;
    case GET_BIBLE_GROUP_UPDATES_PENDING:
      return true;
    default:
      return state;
  }
};

const isFetchingGroupNotesUpdates = (state = false, action) => {
  switch (action.type) {
    case GET_BIBLE_GROUP_NOTES_UPDATES_REJECTED:
    case GET_BIBLE_GROUP_NOTES_UPDATES_FULFILLED:
      return false;
    case GET_BIBLE_GROUP_NOTES_UPDATES_PENDING:
      return true;
    default:
      return state;
  }
};

const error = (state = '', action) => {
  switch (action.type) {
    case GET_BIBLE_GROUP_UPDATES_REJECTED:
      return action.payload;
    default:
      return state;
  }
};

const errorNotes = (state = '', action) => {
  switch (action.type) {
    case GET_BIBLE_GROUP_NOTES_UPDATES_REJECTED:
      return action.payload;
    default:
      return state;
  }
};

const bibleGroups = (state = [], action) => {
  switch (action.type) {
    case GET_BIBLE_GROUP_UPDATES_FULFILLED:
      return action.payload;
    default:
      return state;
  }
};

const bibleGroupsNotes = (state = [], action) => {
  switch (action.type) {
    case GET_BIBLE_GROUP_NOTES_UPDATES_FULFILLED:
      return action.payload;

    default:
      return state;
  }
};

// Defaults for SSR

const bibleGroupNoteInfo = (state = {}, action) => {
  switch (action.type) {
    case GET_BIBLE_GROUP_NOTES_UPDATES_CLEAR:
      return {};
    default:
      return state;
  }
};

const currentGroup = {};

const bibleGroupUpdates = combineReducers({
  isFetchingGroupUpdates,
  isFetchingGroupNotesUpdates,
  error,
  errorNotes,
  bibleGroups,
  bibleGroupsNotes,
  bibleGroupNoteInfo,
  currentGroup,
});

export default bibleGroupUpdates;
