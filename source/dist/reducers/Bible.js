import { combineReducers } from 'redux';
import { BOOKS_FETCH, BOOKS_FETCH_SUCCESS, BOOKS_FETCH_FAILURE } from '../actions/Bible';

const testamentIsLoading = (state = true, action) => {
  switch (action.type) {
    case BOOKS_FETCH:
    case BOOKS_FETCH_FAILURE:
      return true;
    case BOOKS_FETCH_SUCCESS:
      return false;
    default:
      return state;
  }
};

const testament = (state = [], action) => {
  switch (action.type) {
    case BOOKS_FETCH_SUCCESS:
      return action.payload;
    case BOOKS_FETCH_FAILURE:
      return [];
    default:
      return state;
  }
};

const reducer = combineReducers({
  testamentIsLoading,
  testament,
});

export default reducer;
