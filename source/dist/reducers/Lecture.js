import {
  LECTURE_GROUP_FETCH,
  LECTURE_GROUP_FETCH_SUCCESS,
  LECTURE_GROUP_FETCH_FAILURE,
  LECTURE_GROUP_LIST_FETCH,
  LECTURE_GROUP_LIST_FETCH_SUCCESS,
  LECTURE_GROUP_LIST_FETCH_FAILURE,
  LECTURE_DETAIL_FETCH,
  LECTURE_DETAIL_FETCH_SUCCESS,
  LECTURE_DETAIL_FETCH_FAILURE,
  LECTURE_GROUP_LIST_CLEAR,
  LECTURE_DETAIL_CLEAR,
  LECTURE_DETAIL_CLEAR_ERROR,
} from '../actions/Lecture';

const initialState = {
  getLectureGroupLoading: true,
  getLectureGroupLoaded: false,
  getLectureGroupFailed: false,
  getLectureGroupListLoading: true,
  getLectureGroupListLoaded: false,
  getLectureGroupListFailed: false,
  getLectureDetailLoading: true,
  getLectureDetailLoaded: false,
  getLectureDetailFailed: false,
  lectureGroup: {},
  lectureGroupList: {},
  lectureDetail: {},
  error: null,
};

const Lecture = (state = initialState, action) => {
  switch (action.type) {
    case LECTURE_GROUP_FETCH:
      return {
        ...state,
        getLectureGroupLoading: true,
        getLectureGroupLoaded: false,
        getLectureGroupFailed: false,
      };
    case LECTURE_GROUP_FETCH_SUCCESS:
      return {
        ...state,
        getLectureGroupLoading: false,
        getLectureGroupLoaded: true,
        getLectureGroupFailed: false,
        lectureGroup: action.payload.data,
      };
    case LECTURE_GROUP_FETCH_FAILURE:
      return {
        ...state,
        getLectureGroupLoading: false,
        getLectureGroupLoaded: false,
        getLectureGroupFailed: true,
        error: action.error,
      };
    case LECTURE_GROUP_LIST_FETCH:
      return {
        ...state,
        getLectureGroupListLoading: true,
        getLectureGroupListLoaded: false,
        getLectureGroupListFailed: false,
      };
    case LECTURE_GROUP_LIST_FETCH_SUCCESS:
      return {
        ...state,
        getLectureGroupListLoading: false,
        getLectureGroupListLoaded: true,
        getLectureGroupListFailed: false,
        lectureGroupList: action.payload.data,
      };
    case LECTURE_GROUP_LIST_FETCH_FAILURE:
      return {
        ...state,
        getLectureGroupListLoading: false,
        getLectureGroupListLoaded: false,
        getLectureGroupListFailed: true,
        error: action.error,
      };
    case LECTURE_GROUP_LIST_CLEAR:
      return {
        ...state,
        lectureGroupList: {},
        getLectureGroupListFailed: false,
      };
    case LECTURE_DETAIL_FETCH:
      return {
        ...state,
        getLectureDetailLoading: true,
        getLectureDetailLoaded: false,
        getLectureDetailFailed: false,
      };
    case LECTURE_DETAIL_FETCH_SUCCESS:
      return {
        ...state,
        getLectureDetailLoading: false,
        getLectureDetailLoaded: true,
        getLectureDetailFailed: false,
        lectureDetail: action.payload.data,
      };
    case LECTURE_DETAIL_FETCH_FAILURE:
      return {
        ...state,
        getLectureDetailLoading: false,
        getLectureDetailLoaded: false,
        getLectureDetailFailed: true,
        error: action.error,
      };

    case LECTURE_DETAIL_CLEAR:
      return {
        ...state,
        lectureDetail: {},
        getLectureDetailLoading: true,
        getLectureDetailLoaded: false,
      };

    case LECTURE_DETAIL_CLEAR_ERROR:
      return {
        ...state,
        getLectureDetailFailed: false,
      };

    default:
      return {
        ...state,
      };
  }
};

export default Lecture;
