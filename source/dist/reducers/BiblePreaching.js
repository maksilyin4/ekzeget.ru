import {
  PREACHING_FETCH,
  PREACHING_FETCH_SUCCESS,
  PREACHING_FETCH_FAILURE,
  PREACHING_BY_BOOK_FETCH,
  PREACHING_BY_BOOK_FETCH_SUCCESS,
  PREACHING_BY_BOOK_FETCH_FAILURE,
} from '../actions/BiblePreaching';

const initialState = {
  isLoading: true,
  isLoaded: false,
  isFailed: false,
  data: {},
  error: null,
};

const Preaching = (state = initialState, action) => {
  switch (action.type) {
    case PREACHING_FETCH:
    case PREACHING_BY_BOOK_FETCH:
      return {
        ...state,
        isLoading: true,
        isLoaded: false,
        isFailed: false,
      };
    case PREACHING_FETCH_SUCCESS:
    case PREACHING_BY_BOOK_FETCH_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isLoaded: true,
        isFailed: false,
        data: action.payload.data,
      };
    case PREACHING_FETCH_FAILURE:
    case PREACHING_BY_BOOK_FETCH_FAILURE:
      return {
        ...state,
        isLoading: false,
        isLoaded: false,
        isFailed: true,
        error: action.error,
      };
    default:
      return state;
  }
};

export default Preaching;
