import { combineReducers } from 'redux';

import { LS } from '../browserUtils';

const current_token = LS.get('token');

const userDataIsLoading = (state = false, action) => {
  switch (action.type) {
    case 'USER_DATA_FETCH':
    case 'USER_UPDATE_FETCH':
    case 'USER_REG_FETCH':
    case 'USER_AUTH_FETCH':
      return true;
    case 'USER_DATA_SUCCESS':
    case 'USER_DATA_FAILURE':
    case 'USER_UPDATE_SUCCESS':
    case 'USER_UPDATE_FAILURE':
    case 'USER_REG_SUCCESS':
    case 'USER_REG_CONFIRM':
    case 'USER_REG_FAILURE':
    case 'USER_AUTH_SUCCESS':
    case 'USER_AUTH_FAILURE':
      return false;
    default:
      return state;
  }
};

const identityProviders = (state, action) => {
  if (action.error) {
    return action.error;
  }
  return action.payload?.data || [];
};

const userData = (state = { user: {} }, action) => {
  switch (action.type) {
    case 'USER_DATA_SUCCESS':
    case 'USER_UPDATE_SUCCESS':
      return action.payload.data;
    case 'USER_AUTH_NEED': {
      return { user: {} };
    }
    case 'USER_UPDATE_FAILURE':
    default:
      return state;
  }
};

const userLoggedIn = (state = { isLoading: !!current_token, userId: '' }, action) => {
  switch (action.type) {
    case 'USER_REG_CONFIRM':
      return { ...state, isLoading: true, userId: '' };
    case 'USER_AUTH_SUCCESS':
      if (action.payload.data.status === 'ok') {
        LS.set('token', action.payload.data.user.token);
        return { ...state, isLoading: true, userId: action.payload.data.user.id };
      }
      return state;
    case 'USER_AUTH_NEED':
      localStorage.removeItem('token');
      return { ...state, isLoading: false, userId: '' };
    default:
      return state;
  }
};

const error = (state = null, action) => {
  switch (action.type) {
    case 'USER_DATA_FAILURE':
    case 'USER_UPDATE_FAILURE':
    case 'USER_REG_FAILURE':
      return action.error;
    case 'USER_AUTH_FETCH':
    case 'USER_REG_FETCH':
    case 'USER_REG_CONFIRM':
      return null;
    case 'USER_REG_SUCCESS':
      if (action.payload.data.status === 'ok') {
        return null;
      }
      if (action.payload.data.status === 'error') {
        return action.payload.data.error;
      }
      return state;
    case 'USER_AUTH_SUCCESS':
      if (action.payload.data.status === 'ok') {
        return null;
      }
      if (action.payload.data.status === 'error') {
        return action.payload.data.error;
      }
      return state;
    case 'USER_AUTH_FAILURE':
      localStorage.removeItem('token');
      return {
        response: action.error,
        name: 'Ошибка сервера, обратитесь к администратору',
      };
    default:
      return state;
  }
};

const reducer = combineReducers({
  userDataIsLoading,
  userData,
  userLoggedIn,
  error,
  identityProviders,
});

export default reducer;
