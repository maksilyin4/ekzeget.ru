import {
  VERSE_FETCH,
  VERSE_FETCH_SUCCESS,
  VERSE_FETCH_FAILURE,
  LINK_TO_VERSE_FETCH,
  LINK_TO_VERSE_SUCCESS,
  LINK_TO_VERSE_FAILURE,
  VERSE_PARALLEL_FETCH,
  VERSE_PARALLEL_FETCH_SUCCESS,
  VERSE_PARALLEL_FETCH_FAILURE,
} from '../actions/Verse';

const initialState = {
  isLoading: false,
  isLoaded: false,
  isFailed: false,
  linkToVerseLoading: true,
  linkToVerseLoaded: false,
  linkToVerseFailed: false,
  parallelLoading: true,
  data: {},
  linkToVerse: {},
  parallelData: {},
  error: null,
};

const verse = (state = initialState, action) => {
  switch (action.type) {
    case VERSE_FETCH:
      return {
        ...state,
        isLoading: true,
        isLoaded: false,
        isFailed: false,
        error: null,
      };
    case VERSE_FETCH_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isLoaded: true,
        isFailed: false,
        data: action.payload.data,
        error: null,
      };
    case VERSE_FETCH_FAILURE:
      return {
        ...state,
        isLoading: false,
        isLoaded: false,
        isFailed: true,
        error: action.error,
      };

    case LINK_TO_VERSE_FETCH:
      return {
        ...state,
        linkToVerseLoading: true,
        linkToVerseLoaded: false,
        linkToVerseFailed: false,
      };
    case LINK_TO_VERSE_SUCCESS:
      return {
        ...state,
        linkToVerseLoading: false,
        linkToVerseLoaded: true,
        linkToVerseFailed: false,
        linkToVerse: action.payload.data,
      };
    case LINK_TO_VERSE_FAILURE:
      return {
        ...state,
        linkToVerseLoading: false,
        linkToVerseLoaded: false,
        linkToVerseFailed: true,
      };
    case VERSE_PARALLEL_FETCH:
      return {
        ...state,
        parallelLoading: true,
      };
    case VERSE_PARALLEL_FETCH_SUCCESS:
      // eslint-disable-next-line no-case-declarations
      const { verse_id, data } = action.payload;

      return {
        ...state,
        parallelLoading: false,
        parallelData: { ...state.parallelData, [verse_id]: data },
      };
    case VERSE_PARALLEL_FETCH_FAILURE:
      return {
        ...state,
        parallelLoading: false,
        parallelData: action.error,
      };

    default:
      return state;
  }
};

export default verse;
