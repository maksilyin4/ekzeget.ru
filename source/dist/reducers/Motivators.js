import { combineReducers } from 'redux';

import {
  GET_MOTIVATORS_FULFILLED,
  GET_MOTIVATORS_REJECTED,
  GET_MOTIVATORS_PENDING,
} from '../actions/Motivators';

const isFetching = (state = false, action) => {
  switch (action.type) {
    case GET_MOTIVATORS_REJECTED:
    case GET_MOTIVATORS_FULFILLED:
      return false;
    case GET_MOTIVATORS_PENDING:
      return true;
    default:
      return state;
  }
};

const error = (state = '', action) => {
  switch (action.type) {
    case GET_MOTIVATORS_REJECTED:
      return action.payload;
    default:
      return state;
  }
};

const motivators = (state = [], action) => {
  switch (action.type) {
    case GET_MOTIVATORS_FULFILLED:
      return action.payload;
    default:
      return state;
  }
};

const motivator = combineReducers({
  isFetching,
  error,
  motivators,
});

export default motivator;
