import {
  BOOKMARKS_FETCH,
  BOOKMARKS_FETCH_SUCCESS,
  BOOKMARKS_FETCH_FAILURE,
  BOOKMARK_ADD_FETCH,
  BOOKMARK_ADD_FETCH_SUCCESS,
  BOOKMARK_ADD_FETCH_FAILURE,
  BOOKMARK_DELETE_FETCH,
  BOOKMARK_DELETE_FETCH_SUCCESS,
  BOOKMARK_DELETE_FETCH_FAILURE,
} from '../actions/Bookmarks';

const initialState = {
  getIsLoading: true,
  getIsLoaded: false,
  getIsFailed: false,
  addIsLoading: true,
  addIsLoaded: false,
  addIsFailed: false,
  deleteIsLoading: true,
  deleteIsLoaded: false,
  deleteIsFailed: false,
  bookmarks: {
    bookmark: {},
  },
  addStatus: {},
  deleteStatus: {},
  needUpdate: false,
  error: null,
};

const Bookmarks = (state = initialState, action) => {
  switch (action.type) {
    case BOOKMARKS_FETCH:
      return {
        ...state,
        getIsLoading: true,
        getIsLoaded: false,
        getIsFailed: false,
      };
    case BOOKMARKS_FETCH_SUCCESS:
      return {
        ...state,
        getIsLoading: false,
        getIsLoaded: true,
        getIsFailed: false,
        bookmarks: action.payload.data,
        needUpdate: false,
      };
    case BOOKMARKS_FETCH_FAILURE:
      return {
        ...state,
        needUpdate: true,
        error: action.error,
      };
    case BOOKMARK_ADD_FETCH:
      return {
        ...state,
        addIsLoading: true,
        addIsLoaded: false,
        addIsFailed: false,
      };
    case BOOKMARK_ADD_FETCH_SUCCESS:
      return {
        ...state,
        addIsLoading: false,
        addIsLoaded: true,
        addIsFailed: false,
        addStatus: action.payload.data,
        needUpdate: true,
      };
    case BOOKMARK_ADD_FETCH_FAILURE:
      return {
        ...state,
        addIsLoading: false,
        addIsLoaded: false,
        addIsFailed: true,
        error: action.error,
      };
    case BOOKMARK_DELETE_FETCH:
      return {
        ...state,
        deleteIsLoading: true,
        deleteIsLoaded: false,
        deleteIsFailed: false,
      };
    case BOOKMARK_DELETE_FETCH_SUCCESS:
      return {
        ...state,
        deleteIsLoading: false,
        deleteIsLoaded: true,
        deleteIsFailed: false,
        deleteStatus: action.payload.data,
        needUpdate: true,
      };
    case BOOKMARK_DELETE_FETCH_FAILURE:
      return {
        ...state,
        deleteIsLoading: false,
        deleteIsLoaded: false,
        deleteIsFailed: true,
        error: action.error,
      };
    default:
      return {
        ...state,
      };
  }
};

export default Bookmarks;
