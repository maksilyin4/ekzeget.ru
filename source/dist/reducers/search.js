import { combineReducers } from 'redux';
import * as actions from '../actions/search';

export const HANDLE_SEARCH = 'search/HANDLE_SEARCH';
export const REQUEST_SEARCH = 'search/REQUEST_SEARCH';

// it's value of input in header
const searchField = (state = '', action) => {
  switch (action.type) {
    case actions.HANDLE_SEARCH:
      return action.payload;
    case actions.RESET_SEARCH:
      return '';
    default:
      return state;
  }
};

const reducer = combineReducers({
  searchField,
});

export default reducer;
