import {
  DICTONARIES_FETCH,
  DICTONARIES_FETCH_SUCCESS,
  DICTONARIES_FETCH_FAILURE,
  DICTONARIES_WORD_FETCH,
  DICTONARIES_WORD_FETCH_SUCCESS,
  DICTONARIES_WORD_FETCH_FAILURE,
  DICTONARIES_WORD_FETCH_CLEAR,
  DICTONARIES_WORD_ERROR_CLEAR,
  MEANING_WORD_FETCH,
  MEANING_WORD_FETCH_SUCCESS,
  MEANING_WORD_FETCH_FAILURE,
  MEANING_WORD_CLEAR,
} from '../actions/Dictonaries';

const initialState = {
  isDictonariesLoading: true,
  isDictonariesLoaded: false,
  isDictonariesFailed: false,
  isDictonariesWordLoading: true,
  isDictonariesWordLoaded: false,
  isDictonariesWordFailed: false,
  isMeaningWordLoading: true,
  isMeaningWordLoaded: false,
  isMeaningWordFailed: false,
  dictonaries: {},
  words: {
    dictionary: [],
    pages: {},
  },
  word: {},
  test: [],
  error: null,
};

const Dictonaries = (state = initialState, action) => {
  switch (action.type) {
    case DICTONARIES_FETCH:
      return {
        ...state,
        isDictonariesLoading: true,
        isDictonariesLoaded: false,
        isDictonariesFailed: false,
      };
    case DICTONARIES_FETCH_SUCCESS:
      return {
        ...state,
        isDictonariesLoading: false,
        isDictonariesLoaded: true,
        isDictonariesFailed: false,
        dictonaries: action.payload.data,
      };
    case DICTONARIES_FETCH_FAILURE:
      return {
        ...state,
        isDictonariesLoading: false,
        isDictonariesLoaded: false,
        isDictonariesFailed: true,
        error: action.error,
      };
    case DICTONARIES_WORD_FETCH:
      return {
        ...state,
        isDictonariesWordLoading: true,
        isDictonariesWordLoaded: false,
        isDictonariesWordFailed: false,
      };
    case DICTONARIES_WORD_FETCH_SUCCESS: {
      const { dictionary, pages } = action.payload.data;
      return {
        ...state,
        isDictonariesWordLoading: false,
        isDictonariesWordLoaded: true,
        isDictonariesWordFailed: false,
        words: {
          dictionary: [...state.words.dictionary, ...dictionary],
          pages,
        },
      };
    }
    case DICTONARIES_WORD_FETCH_FAILURE:
      return {
        ...state,
        isDictonariesWordLoading: false,
        isDictonariesWordLoaded: false,
        isDictonariesWordFailed: true,
        error: action.error,
      };
    case DICTONARIES_WORD_FETCH_CLEAR:
      return {
        ...state,
        isDictonariesWordLoading: false,
        isDictonariesWordLoaded: false,
        words: {
          dictionary: [],
          pages: {},
        },
      };

    case DICTONARIES_WORD_ERROR_CLEAR:
      return {
        ...state,
        isDictonariesWordLoading: false,
        isDictonariesWordLoaded: false,
        isDictonariesWordFailed: false,
      };
    case MEANING_WORD_FETCH:
      return {
        ...state,
        isMeaningWordLoading: true,
        isMeaningWordLoaded: false,
        isMeaningWordFailed: false,
      };
    case MEANING_WORD_FETCH_SUCCESS:
      return {
        ...state,
        isMeaningWordLoading: false,
        isMeaningWordLoaded: true,
        isMeaningWordFailed: false,
        word: action.payload.data,
      };
    case MEANING_WORD_FETCH_FAILURE:
      return {
        ...state,
        isMeaningWordLoading: false,
        isMeaningWordLoaded: false,
        isMeaningWordFailed: true,
        error: action.error,
      };
    case MEANING_WORD_CLEAR:
      return {
        ...state,
        word: {},
        isMeaningWordLoading: false,
        isMeaningWordLoaded: false,
        isMeaningWordFailed: true,
      };
    default:
      return {
        ...state,
      };
  }
};

export default Dictonaries;
