import R from 'ramda';
import { combineReducers } from 'redux';
import {
  READING_PLANS_FETCH,
  READING_PLANS_SUCCESS,
  READING_PLANS_FAILURE,
  READING_PLAN_DETAIL_FETCH,
  READING_PLAN_DETAIL_SUCCESS,
  READING_PLAN_DETAIL_FAILURE,
  USER_RP_GET_SUCCESS,
  USER_RP_GET_FAILURE,
  USER_RP_ADD_FETCH,
  USER_RP_ADD_SUCCESS,
  USER_RP_ADD_FAILURE,
  USER_RP_DEL_FETCH,
  USER_RP_DEL_SUCCESS,
  USER_RP_DEL_FAILURE,
  SUBSCRIBE_PLAN,
  DELETE_DAYS,
  USER_RP_TAB,
  TOGGLE_OPEN_READING_PLAN,
  COMPLETE_CHAPTER,
  READING_PLAN_DELETE,
  GET_ARCHIVE_PENDING,
  GET_ARCHIVE_SUCCESS,
  GET_ARCHIVE_FAILURE,
} from '../actions/ReadingPlan';

const readingPlansIsLoading = (state = false, action) => {
  switch (action.type) {
    case READING_PLANS_FETCH:
      return true;
    case READING_PLANS_SUCCESS:
    case READING_PLANS_FAILURE:
      return false;
    default:
      return state;
  }
};

const activeTab = (state = '', action) => {
  switch (action.type) {
    case USER_RP_TAB:
      return action.payload || '';
    default:
      return state;
  }
};

const toggleOpenPlan = (state = true, action) => {
  switch (action.type) {
    case TOGGLE_OPEN_READING_PLAN:
      return action.payload;
    default:
      return state;
  }
};

const plans = (state = [], action) => {
  switch (action.type) {
    case READING_PLANS_SUCCESS:
      return action.payload;
    case READING_PLAN_DELETE: {
      return state.filter(plan => plan.id !== Number(action.payload));
    }

    case READING_PLANS_FAILURE:
    default:
      return state;
  }
};

const userRPIsLoading = (state = 0, action) => {
  switch (action.type) {
    case USER_RP_ADD_FETCH:
    case USER_RP_DEL_FETCH:
      return action.payload;
    case USER_RP_GET_SUCCESS:
    case USER_RP_GET_FAILURE:
    case USER_RP_ADD_SUCCESS:
    case USER_RP_ADD_FAILURE:
    case USER_RP_DEL_SUCCESS:
    case USER_RP_DEL_FAILURE:
    case SUBSCRIBE_PLAN:
      return 0;
    default:
      return state;
  }
};

const archivedLoading = (state = false, action) => {
  switch (action.type) {
    case GET_ARCHIVE_PENDING:
      return true;
    case GET_ARCHIVE_SUCCESS:
    case GET_ARCHIVE_FAILURE:
      return false;
    default:
      return state;
  }
};

const archivedPlans = (state = [], action) => {
  switch (action.type) {
    case GET_ARCHIVE_SUCCESS:
      return action.payload;
    default:
      return state;
  }
};

const userPlans = (state = {}, action) => {
  switch (action.type) {
    case USER_RP_GET_SUCCESS:
    case USER_RP_ADD_SUCCESS:
      return action.payload;
    case USER_RP_DEL_SUCCESS:
      return R.omit([action.payload], state);
    case SUBSCRIBE_PLAN:
      return {
        [action.payload.plan_id]: {
          ...state[action.payload.plan_id],
          subscribe: action.payload.subscribe,
        },
      };
    case COMPLETE_CHAPTER: {
      const updatedPlan = Object.keys(state).reduce(
        (plan, id) => ({
          ...plan,
          [id]: {
            ...state[id],
            reading: state[id].reading.flat().map(read => [
              {
                ...read,
                is_closed: read.id === action.payload ? true : read.is_closed,
              },
            ]),

            closedPlans: [
              ...state[id].closedPlans,
              state[id].reading.flat().find(el => el.id === action.payload),
            ],
          },
        }),
        {},
      );
      return updatedPlan;
    }
    case DELETE_DAYS:
      // Note: затираются все непрочтеные дни всех чтений, перед самим последним
      return Object.keys(state).reduce(
        (plan, id) => ({
          ...plan,
          [id]: {
            ...state[id],
            remainder_days: [],
          },
        }),
        {},
      );
    case USER_RP_GET_FAILURE:
    default:
      return state;
  }
};

const planDetail = (state = {}, action) => {
  switch (action.type) {
    case READING_PLAN_DETAIL_SUCCESS:
      return action.payload;
    default:
      return state;
  }
};

const readingPlanDetailIsLoading = (state = false, action) => {
  switch (action.type) {
    case READING_PLAN_DETAIL_FETCH:
      return true;
    case READING_PLAN_DETAIL_SUCCESS:
    case READING_PLAN_DETAIL_FAILURE:
      return false;
    default:
      return state;
  }
};

const reducer = combineReducers({
  readingPlansIsLoading,
  plans,
  activeTab,
  userRPIsLoading,
  toggleOpenPlan,
  userPlans,
  archivedPlans,
  archivedLoading,
  planDetail,
  readingPlanDetailIsLoading,
});

export default reducer;
