import { combineReducers } from 'redux';

import {
  LIST_OF_PLAYLISTS_FULFILLED,
  LIST_OF_PLAYLISTS_REJECTED,
  LIST_OF_PLAYLISTS_PENDING,
} from '../actions/PlayLists';

const isFetching = (state = false, action) => {
  switch (action.type) {
    case LIST_OF_PLAYLISTS_REJECTED:
    case LIST_OF_PLAYLISTS_FULFILLED:
      return false;
    case LIST_OF_PLAYLISTS_PENDING:
      return true;
    default:
      return state;
  }
};

const error = (state = '', action) => {
  switch (action.type) {
    case LIST_OF_PLAYLISTS_REJECTED:
      return action.payload;
    default:
      return state;
  }
};

const listOfPlayLists = (state = [], action) => {
  switch (action.type) {
    case LIST_OF_PLAYLISTS_FULFILLED:
      return action.payload;
    default:
      return state;
  }
};

const reducer = combineReducers({
  isFetching,
  error,
  listOfPlayLists,
});

export default reducer;
