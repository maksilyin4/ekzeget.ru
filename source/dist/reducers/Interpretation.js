import {
  INTERPRETATION_URL_FULFILLED,
  INTERPRETATION_URL_REJECTED,
  INTERPRETATION_URL_PEDNING,
  INTERPRETATION_URL_CLEAR,
} from '../actions/Interpretation';

const initialState = {
  isLoading: false,
  isLoaded: false,
  isFailed: false,
  data: [],
  error: null,
};

const Interpretation = (state = initialState, action) => {
  switch (action.type) {
    case INTERPRETATION_URL_PEDNING:
      return {
        ...state,
        isLoading: true,
        error: null,
      };
    case INTERPRETATION_URL_FULFILLED:
      return {
        ...state,
        isLoading: false,

        data: action.payload.interpretations,
      };
    case INTERPRETATION_URL_REJECTED:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    case INTERPRETATION_URL_CLEAR:
      return {
        ...state,

        data: [],
      };
    default:
      return {
        ...state,
      };
  }
};

export default Interpretation;
