import {
  CHAPTER_FETCH,
  CHAPTER_FETCH_SUCCESS,
  CHAPTER_FETCH_FAILURE,
  CHAPTER_CLEAR,
} from '../actions/Chapter';

const initialState = {
  isLoading: true,
  isLoaded: false,
  isFailed: false,
  data: {},
  error: null,
};

const Chapter = (state = initialState, action) => {
  switch (action.type) {
    case CHAPTER_FETCH:
      return {
        ...state,
        isLoading: true,
        isLoaded: false,
        isFailed: false,
        error: null,
      };
    case CHAPTER_FETCH_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isLoaded: true,
        isFailed: false,
        data: action.payload.data,
        error: null,
      };
    case CHAPTER_FETCH_FAILURE:
      return {
        ...state,
        isLoading: false,
        isLoaded: false,
        isFailed: true,
        error: action.error,
      };

    case CHAPTER_CLEAR:
      return {
        ...state,
        isLoading: false,
        isLoaded: false,
        isFailed: false,
        data: { data: [] },
        error: null,
      };
    default:
      return {
        ...state,
      };
  }
};

export default Chapter;
