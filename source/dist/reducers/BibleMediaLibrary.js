import { combineReducers } from 'redux';

import {
  BIBLE_MEDIA_LIBRARY_PENDING,
  BIBLE_MEDIA_LIBRARY_FULFILLED,
  BIBLE_MEDIA_LIBRARY_REJECTED,
} from '../actions/BibleMediaLibrary';

const isFetching = (state = false, action) => {
  switch (action.type) {
    case BIBLE_MEDIA_LIBRARY_REJECTED:
    case BIBLE_MEDIA_LIBRARY_FULFILLED:
      return false;
    case BIBLE_MEDIA_LIBRARY_PENDING:
      return true;
    default:
      return state;
  }
};

const error = (state = '', action) => {
  switch (action.type) {
    case BIBLE_MEDIA_LIBRARY_REJECTED:
      return action.payload;
    default:
      return state;
  }
};

const mediaBible = (state = [], action) => {
  switch (action.type) {
    case BIBLE_MEDIA_LIBRARY_FULFILLED:
      return action.payload;
    default:
      return state;
  }
};

const reducer = combineReducers({
  isFetching,
  error,
  mediaBible,
});

export default reducer;
