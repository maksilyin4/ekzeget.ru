import { _AXIOS } from '../ApiConfig';

export const REQUEST_MY_INTER = 'myInterpretation/REQUEST_MY_INTER';
export const RECEIVE_MY_INTER_SUCCESS = 'myInterpretation/REQUEST_MY_INTER_SUCCESS';
export const RECEIVE_MY_INTER_FAILURE = 'myInterpretation/REQUEST_MY_INTER_FAILURE';

export function getMyInterp() {
  return dispatch => {
    dispatch(requestMyInterp());

    _AXIOS({ url: 'user/interpretation/', isAuth: true }).then(({ interpretation }) =>
      dispatch(receiveMyInterpSuccess(interpretation)),
    );
  };
}

const requestMyInterp = () => ({
  type: REQUEST_MY_INTER,
});

const receiveMyInterpSuccess = payload => ({
  type: RECEIVE_MY_INTER_SUCCESS,
  payload,
});
