import { AXIOS } from '../ApiConfig';

export const LIST_OF_PLAYLISTS_FULFILLED = 'LIST_OF_PLAYLISTS_FULFILLED';
export const LIST_OF_PLAYLISTS_REJECTED = 'LIST_OF_PLAYLISTS_REJECTED';
export const LIST_OF_PLAYLISTS_PENDING = 'LIST_OF_PLAYLISTS_PENDING';

const getListOfPlayLists = testament => dispatch => {
  dispatch(getListOfPlayListsPending());

  return AXIOS.get(`/media/video-preview/${testament}`).then(
    response => dispatch(getListOfPlayListsFulfilled(response.data)),
    error => dispatch(getListOfPlayListsRejected(error)),
  );
};

export const getListOfPlayListsFulfilled = data => ({
  type: LIST_OF_PLAYLISTS_FULFILLED,
  payload: data,
});

export const getListOfPlayListsRejected = error => ({
  type: LIST_OF_PLAYLISTS_REJECTED,
  payload: error,
});

export const getListOfPlayListsPending = () => ({
  type: LIST_OF_PLAYLISTS_PENDING,
});

export default getListOfPlayLists;
