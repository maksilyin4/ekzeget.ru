import R from 'ramda';
import { AXIOS } from '../ApiConfig';

export const PREACHING_FETCH = 'PREACHING_FETCH';
export const PREACHING_FETCH_SUCCESS = 'PREACHING_FETCH_SUCCESS';
export const PREACHING_FETCH_FAILURE = 'PREACHING_FETCH_FAILURE';
export const PREACHING_FETCH_CLEAR = 'PREACHING_FETCH_CLEAR';

export const PREACHER_FETCH = 'PREACHER_FETCH';
export const PREACHER_FETCH_SUCCESS = 'PREACHER_FETCH_SUCCESS';
export const PREACHER_FETCH_FAILURE = 'PREACHER_FETCH_FAILURE';

export const PREACHING_EXCUSE_FETCH = 'PREACHING_EXCUSE_FETCH';
export const PREACHING_EXCUSE_FETCH_SUCCESS = 'PREACHING_EXCUSE_FETCH_SUCCESS';
export const PREACHING_EXCUSE_FETCH_FAILURE = 'PREACHING_EXCUSE_FETCH_FAILURE';

export const PREACHING_CELEBRATION_FETCH = 'PREACHING_CELEBRATION_FETCH';
export const PREACHING_CELEBRATION_FETCH_SUCCESS = 'PREACHING_CELEBRATION_FETCH_SUCCESS';
export const PREACHING_CELEBRATION_FETCH_FAILURE = 'PREACHING_CELEBRATION_FETCH_FAILURE';

export const PREACHING_FETCH_CLEAR_ERROR = 'PREACHING_FETCH_CLEAR_ERROR';
export const PREACHING_FETCH_ERROR = 'PREACHING_FETCH_ERROR';

const getPreachingExcuse = () => dispatch => {
  dispatch(getPreachingExcusePending());

  return AXIOS.get('/celebration/excuse/').then(
    response => {
      dispatch(getPreachingExcuseFulfilled(R.pathOr([], ['data', 'excuse'], response)));
    },
    error => {
      dispatch(getPreachingExcuseRejected(error));
    },
  );
};

export const getPreachingExcuseFulfilled = payload => ({
  type: PREACHING_EXCUSE_FETCH_SUCCESS,
  payload,
});

export const getPreachingExcusePending = () => ({
  type: PREACHING_EXCUSE_FETCH,
});

export const getPreachingExcuseRejected = error => ({
  type: PREACHING_EXCUSE_FETCH_FAILURE,
  error,
});

// Получение поводов
const getPreachingCelebration = params => dispatch => {
  dispatch(getPreachingCelebrationPending());

  return AXIOS.get(`/preaching/excuse/${params}`).then(
    response => {
      dispatch(getPreachingCelebrationFulfilled(R.pathOr([], ['data', 'excuse'], response)));
    },
    error => {
      dispatch(getPreachingCelebrationRejected(error));
    },
  );
};

export const getPreachingCelebrationFulfilled = payload => ({
  type: PREACHING_CELEBRATION_FETCH_SUCCESS,
  payload,
});

export const getPreachingCelebrationPending = () => ({
  type: PREACHING_CELEBRATION_FETCH,
});

export const getPreachingCelebrationRejected = error => ({
  type: PREACHING_CELEBRATION_FETCH_FAILURE,
  error,
});

// Получение проповедей
const getPreaching = params => dispatch => {
  dispatch(getPreachingPending());

  return AXIOS.get(`/preaching/${params}`).then(
    response => {
      dispatch(getPreachingFulfilled(response.data));
    },
    error => {
      dispatch(getPreachingRejected(error));
    },
  );
};

const getPreachingClear = () => dispatch =>
  dispatch({
    type: PREACHING_FETCH_CLEAR,
  });

export const getPreachingFulfilled = data => ({
  type: PREACHING_FETCH_SUCCESS,
  payload: {
    data,
  },
});

export const getPreachingPending = () => ({
  type: PREACHING_FETCH,
});

export const getPreachingRejected = error => ({
  type: PREACHING_FETCH_FAILURE,
  error,
});

// Получение проповедников
const getPreacher = params => dispatch => {
  dispatch(getPreacherPending());

  return AXIOS.get(`/preaching/preacher/${params}`).then(
    response => {
      dispatch(getPreacherFulfilled(R.pathOr([], ['data', 'preacher'], response)));
    },
    error => {
      dispatch(getPreacherRejected(error));
    },
  );
};

export const getPreacherFulfilled = payload => ({
  type: PREACHER_FETCH_SUCCESS,
  payload,
});

export const getPreacherPending = () => ({
  type: PREACHER_FETCH,
});

export const getPreacherRejected = error => ({
  type: PREACHER_FETCH_FAILURE,
  error,
});

export const setPreachingIsError = () => ({
  type: PREACHING_FETCH_ERROR,
});

export const getClearPreachingIsError = () => ({
  type: PREACHING_FETCH_CLEAR_ERROR,
});

export default {
  getPreaching,
  getPreacher,
  getPreachingExcuse,
  getPreachingCelebration,
  getPreachingClear,
  getClearPreachingIsError,
  setPreachingIsError,
};
