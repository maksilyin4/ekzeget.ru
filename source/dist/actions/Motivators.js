import { AXIOS } from '../ApiConfig';

export const GET_MOTIVATORS_FULFILLED = 'GET_MOTIVATORS_FULFILLED';
export const GET_MOTIVATORS_REJECTED = 'GET_MOTIVATORS_REJECTED';
export const GET_MOTIVATORS_PENDING = 'GET_MOTIVATORS_PENDING';

export const getMotivatorsFulfilled = data => ({
  type: GET_MOTIVATORS_FULFILLED,
  payload: data,
});

export const getMotivatorsRejected = error => ({
  type: GET_MOTIVATORS_REJECTED,
  payload: error,
});

export const getMotivatorsPending = () => ({
  type: GET_MOTIVATORS_PENDING,
});

function getMotivators(id) {
  return dispatch => {
    dispatch(getMotivatorsPending());

    AXIOS.get(`/motivator/${id}`).then(
      response => {
        if (response.data.status === 'ok') {
          dispatch(getMotivatorsFulfilled(response.data.motivators));
        } else {
          dispatch(getMotivatorsRejected('error'));
        }
      },
      error => {
        dispatch(getMotivatorsRejected(error));
      },
    );
  };
}

export default getMotivators;
