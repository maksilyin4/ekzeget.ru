export const HANDLE_SEARCH = 'search/HANDLE_SEARCH';
export const RESET_SEARCH = 'search/RESET_SEARCH';

export const handleSearch = payload => {
  return {
    type: HANDLE_SEARCH,
    payload,
  };
};

export const handleReset = () => ({
  type: RESET_SEARCH,
});
