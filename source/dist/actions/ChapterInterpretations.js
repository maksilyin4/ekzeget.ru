import { _AXIOS } from '../ApiConfig';

export const INTERPRETATIONS_FETCH = 'INTERPRETATIONS_FETCH';
export const INTERPRETATIONS_FETCH_SUCCESS = 'INTERPRETATIONS_FETCH_SUCCESS';
export const INTERPRETATIONS_FETCH_FAILURE = 'INTERPRETATIONS_FETCH_FAILURE';

// Получение Главы книги
const getChapterInterpretations = (book_code, chapterId, investigator_id) => dispatch => {
  dispatch(getChapterInterpretationsPending());
  return _AXIOS({
    url: `/interpretation/${book_code}/?chapter=${chapterId}&interpretator_id=${investigator_id}&per-page=1000`,
  })
    .then(data => dispatch(getChapterInterpretationsFulfilled(data)))
    .catch(error => dispatch(getChapterInterpretationsRejected(error)));
};

export const getChapterInterpretationsFulfilled = data => ({
  type: INTERPRETATIONS_FETCH_SUCCESS,
  payload: {
    data,
  },
});

export const getChapterInterpretationsPending = () => ({
  type: INTERPRETATIONS_FETCH,
});

export const getChapterInterpretationsRejected = error => ({
  type: INTERPRETATIONS_FETCH_FAILURE,
  error,
});

export default {
  getChapterInterpretations,
};
