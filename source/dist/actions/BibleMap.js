import { _AXIOS } from '../ApiConfig';
import { META_CONTEXT_DATA } from '../../constants';

export const BIBLE_MAP_GET_SUCCESS = 'bible-map/BIBLE_MAP_GET_SUCCESS';
export const BIBLE_MAP_GET_ERROR = 'bible-map/BIBLE_MAP_GET_ERROR';
export const BIBLE_MAP_PEDING = 'bible-map/BIBLE_MAP_PEDING';

export const BIBLE_MAP_BY_BOOK_PENDING = 'bible-map/BIBLE_MAP_BY_BOOK_PENDING';
export const BIBLE_MAP_BY_BOOK_FULFILLED = 'bible-map/BIBLE_MAP_BY_BOOK_FULFILLED';
export const BIBLE_MAP_BY_BOOK_REJECTED = 'bible-map/BIBLE_MAP_BY_BOOK_REJECTED';

export const BIBLE_MAP_POINT_PENDING = 'bible-map/BIBLE_MAP_POINT_PENDING';
export const BIBLE_MAP_POINT_FULFILLED = 'bible-map/BIBLE_MAP_POINT_FULFILLED';
export const BIBLE_MAP_POINT_REJECTED = 'bible-map/BIBLE_MAP_POINT_REJECTED';

export const getSuccess = payload => ({
  type: BIBLE_MAP_GET_SUCCESS,
  payload,
});

export const setMetaContext = payload => ({
  type: META_CONTEXT_DATA,
  payload,
});

export const getError = error => ({
  type: BIBLE_MAP_GET_ERROR,
  error,
});

export const getPending = () => ({
  type: BIBLE_MAP_PEDING,
});

// Показать все маркеры или определенный завет
// id = 1/2 (Старый/Новый завет)

export function getAllMarkers() {
  return dispatch => {
    dispatch(getPending());
    _AXIOS({ url: `/bible-maps/?per-page=3000` })
      .then(({ points }) => {
        dispatch(getSuccess(points));
        dispatch(setMetaContext(points));
      })
      .catch(error => dispatch(getError(error)));
  };
}

// Точки по книге

// export const getPointsByBookFulfilled = payload => ({
//   type: BIBLE_MAP_BY_BOOK_FULFILLED,
//   payload,
// });
//
// export const getPointsByBookRejected = error => ({
//   type: BIBLE_MAP_BY_BOOK_REJECTED,
//   error,
// });
//
// export const getPointsByBookPending = () => ({
//   type: BIBLE_MAP_BY_BOOK_PENDING,
// });
//
// export const getPointsByBook = (book, chapter) => dispatch => {
//   dispatch(getPointsByBookPending());
//
//   return AXIOS.get(`/bible-maps/points-by-book/${book}/?chapter=${chapter}`).then(
//     // переделать запрос
//     response => {
//       dispatch(getPointsByBookFulfilled(response.data));
//     },
//     error => {
//       dispatch(getPointsByBookRejected(error));
//     },
//   );
// };

// export const getPointByParamsFulfilled = payload => ({
//   type: BIBLE_MAP_POINT_FULFILLED,
//   payload,
// });
//
// export const getPointByParamsRejected = error => ({
//   type: BIBLE_MAP_POINT_REJECTED,
//   error,
// });
//
// export const getPointByParamsPending = () => ({
//   type: BIBLE_MAP_POINT_PENDING,
// });
//
// export const getPointByParams = params => dispatch => {
//   dispatch(getPointByParamsPending());
//   return AXIOS.get(`/bible-maps/search/?q=${params}`).then(
//     response => {
//       dispatch(getPointByParamsFulfilled(response.data));
//     },
//     error => {
//       dispatch(getPointByParamsRejected(error));
//     },
//   );
// };
