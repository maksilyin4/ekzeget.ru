import { AXIOS } from '../ApiConfig';
import { parseLID } from '../utils';

export const LID_FETCH = 'LID_FETCH';
export const LID_FETCH_SUCCESS = 'LID_FETCH_SUCCESS';
export const LID_FETCH_FAILURE = 'LID_FETCH_FAILURE';

const getObjReadingData = (url, data) => ({
  title: url,
  verse: data.verse,
  info: {
    link_title: url,
    book_code: data.verse[0].book.code,
    chapter: data.verse[0].chapter,
    verses: url.replace(/[а-яА-Я]{2,5}\.\s/gim, ''),
  },
});

const getLID = date => dispatch => {
  dispatch(getLIDPending());
  return AXIOS.get(`/reading/${date}/?per-page=1000`).then(
    async response => {
      const result = response.data;
      // Методов в объекте нет, все ок должно быть
      const currentResult = JSON.parse(JSON.stringify(result));
      const ReadingPromise = [];

      if (result.reading) {
        if (result.reading.apostolic_reading !== '' && result.reading.apostolic_reading !== null) {
          const ar = parseLID(result.reading.apostolic_reading);

          currentResult.reading.apostolic_reading = [];

          if (ar.length > 0) {
            ar.forEach(item => ReadingPromise.push(item));
          }
        }
        if (result.reading.gospel_reading !== '' && result.reading.gospel_reading !== null) {
          const ar = parseLID(result.reading.gospel_reading);

          currentResult.reading.gospel_reading = [];

          if (ar.length > 0) {
            ar.forEach(item => ReadingPromise.push(item));
          }
        }

        if (result.reading.more_reading !== '') {
          const ar = parseLID(result.reading.more_reading);
          currentResult.reading.more_reading = [];

          if (ar?.length > 0) {
            ar.forEach(item => ReadingPromise.push(item));
          }
        }

        if (result.reading.morning_reading !== '' && result.reading.morning_reading !== null) {
          const ar = parseLID(result.reading.morning_reading);
          currentResult.reading.morning_reading = [];

          if (ar.length > 0) {
            ar.forEach(item => ReadingPromise.push(item));
          }
        }
      }
      await Promise.all(ReadingPromise.map(url => AXIOS.get(`/verse/info/${url}/?per-page=1000`)))
        .then(dataArr =>
          dataArr.map(response => {
            const url = response.config.url
              .replace('/verse/info/', '')
              .replace('/?per-page=1000', '');
            if (result.reading.apostolic_reading.includes(url))
              currentResult.reading.apostolic_reading.push(getObjReadingData(url, response.data));

            if (result.reading.gospel_reading.includes(url))
              currentResult.reading.gospel_reading.push(getObjReadingData(url, response.data));

            if (result.reading.more_reading.includes(url))
              currentResult.reading.more_reading.push(getObjReadingData(url, response.data));

            if (result.reading.morning_reading.includes(url))
              currentResult.reading.morning_reading.push(getObjReadingData(url, response.data));
          }),
        )
        .then(() => {
          dispatch(getLIDFulfilled(currentResult));
        })
        .catch(error => {
          dispatch(getLIDRejected(error));
        });
    },
    error => {
      dispatch(getLIDRejected(error));
    },
  );
};

export const getLIDFulfilled = data => ({
  type: LID_FETCH_SUCCESS,
  payload: {
    data,
  },
});

export const getLIDPending = () => ({
  type: LID_FETCH,
});

export const getLIDRejected = error => ({
  type: LID_FETCH_FAILURE,
  error,
});

export default getLID;