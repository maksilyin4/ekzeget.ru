import { _AXIOS } from '../ApiConfig';

export const VIDEO_PLAYLIST_FETCH = 'VIDEO_PLAYLIST_FETCH';
export const VIDEO_PLAYLIST_SUCCESS = 'VIDEO_PLAYLIST_SUCCESS';
export const VIDEO_PLAYLIST_FAILURE = 'VIDEO_PLAYLIST_FAILURE';

export const VIDEO_DETAIL_FETCH = 'VIDEO_DETAIL_FETCH';
export const VIDEO_DETAIL_SUCCESS = 'VIDEO_DETAIL_SUCCESS';
export const VIDEO_DETAIL_FAILURE = 'VIDEO_DETAIL_FAILURE';
export const VIDEO_DETAIL_CLEAR = 'VIDEO_DETAIL_CLEAR';

export const getVideoPlaylistFetch = () => ({
  type: VIDEO_PLAYLIST_FETCH,
});

export const getVideoPlaylistSuccess = data => ({
  type: VIDEO_PLAYLIST_SUCCESS,
  payload: {
    data,
  },
});

export const getVideoPlaylistFailure = error => ({
  type: VIDEO_PLAYLIST_FAILURE,
  error,
});

const getVideoPlaylist = (testament, params = '') => dispatch => {
  dispatch(getVideoPlaylistFetch());

  return _AXIOS({ url: `/media/video-preview/${testament}/${params}` })
    .then(data => dispatch(getVideoPlaylistSuccess(data)))
    .catch(error => dispatch(getVideoPlaylistFailure(error)));
};

export const getVideoDetailFetch = () => ({
  type: VIDEO_DETAIL_FETCH,
});

export const getVideoDetailSuccess = data => ({
  type: VIDEO_DETAIL_SUCCESS,
  payload: {
    data,
  },
});

export const getVideoDetailFailure = error => ({
  type: VIDEO_DETAIL_FAILURE,
  error,
});

const getVideoDetail = params => dispatch => {
  dispatch(getVideoDetailFetch());

  // TODO предложить бате сделать кнопку в плейлисте для подгрузки видео, грузить все через per-page 1000 вообще плохая идея, но пока так
  return _AXIOS({ url: `/media/video/${params}/?per-page=1000` })
    .then(data => dispatch(getVideoDetailSuccess(data)))
    .catch(error => dispatch(getVideoDetailFailure(error)));
};

const getVideoDetailClear = () => dispatch =>
  dispatch({
    type: VIDEO_DETAIL_CLEAR,
  });

export default {
  getVideoPlaylist,
  getVideoDetailClear,
  getVideoDetail,
};
