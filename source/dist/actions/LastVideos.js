import { _AXIOS } from '../ApiConfig';

export const LAST_VIDEOS_PENDING = 'LAST_VIDEOS_PENDING';
export const LAST_VIDEOS_FULFILLED = 'LAST_VIDEOS_FULFILLED';
export const LAST_VIDEOS_REJECTED = 'LAST_VIDEOS_REJECTED';

export const getLastVideosPending = () => ({
  type: LAST_VIDEOS_PENDING,
});

export const getLastVideosFulfilled = (data, testamentId) => ({
  type: LAST_VIDEOS_FULFILLED,
  payload: data,
  other: testamentId,
});

export const getLastVideosRejected = error => ({
  type: LAST_VIDEOS_REJECTED,
  payload: error,
});

const getLastVideos = testamentId => dispatch => {
  dispatch(getLastVideosPending());

  return _AXIOS({ url: `/media/last-video/${testamentId}` })
    .then(({ video }) => dispatch(getLastVideosFulfilled(video, testamentId)))
    .catch(error => dispatch(getLastVideosRejected(error)));
};

export default getLastVideos;
