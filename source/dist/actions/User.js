import R from 'ramda';
import { _AXIOS } from '../ApiConfig';
import { LS } from '../browserUtils';
import { sendSocialActions } from '../utils';

export const USER_DATA_FETCH = 'USER_DATA_FETCH';
export const USER_DATA_SUCCESS = 'USER_DATA_SUCCESS';
export const USER_DATA_FAILURE = 'USER_DATA_FAILURE';

export const USER_IDENTITY_PROVIDERS_SUCCESS = 'USER_IDENTITY_PROVIDERS_SUCCESS';
export const USER_IDENTITY_PROVIDERS_FAILURE = 'USER_IDENTITY_PROVIDERS_FAILURE';

export const USER_UPDATE_FETCH = 'USER_UPDATE_FETCH';
export const USER_UPDATE_SUCCESS = 'USER_UPDATE_SUCCESS';
export const USER_UPDATE_FAILURE = 'USER_UPDATE_FAILURE';

export const USER_REG_FETCH = 'USER_REG_FETCH';
export const USER_REG_SUCCESS = 'USER_REG_SUCCESS';
export const USER_REG_FAILURE = 'USER_REG_FAILURE';
export const USER_REG_CONFIRM = 'USER_REG_CONFIRM';

export const USER_AUTH_FETCH = 'USER_AUTH_FETCH';
export const USER_AUTH_SUCCESS = 'USER_AUTH_SUCCESS';
export const USER_AUTH_FAILURE = 'USER_AUTH_FAILURE';

export const USER_RESET_PASSWORD_FETCH = 'USER_RESET_PASSWORD_FETCH';
export const USER_RESET_PASSWORD_SUCCESS = 'USER_RESET_PASSWORD_SUCCESS';
export const USER_RESET_PASSWORD_FAILURE = 'USER_RESET_PASSWORD_FAILURE';

export const USER_AUTH_NEED = 'USER_AUTH_NEED';

export const getUserFetch = () => ({
  type: USER_DATA_FETCH,
});

export const getUserSuccess = data => ({
  type: USER_DATA_SUCCESS,
  payload: {
    data,
  },
});

export const getUserFailure = error => ({
  type: USER_DATA_FAILURE,
  error,
});

export const getIdentityProvidersSuccess = data => ({
  type: USER_IDENTITY_PROVIDERS_SUCCESS,
  payload: {
    data,
  },
});

export const getIdentityProvidersFailure = error => ({
  type: USER_IDENTITY_PROVIDERS_FAILURE,
  error,
});

export const needAuth = () => ({
  type: USER_AUTH_NEED,
});

// Получение данных о пользователе
export const getUser = () => dispatch => {
  dispatch(getUserFetch());

  return _AXIOS({ url: '/user/', isAuth: true })
    .then(data => dispatch(getUserSuccess(data)))
    .catch(({ message }) => dispatch(getUserFailure(message)));
};

export const updateUserFetch = () => ({
  type: USER_UPDATE_FETCH,
});
export const updateUserSuccess = data => ({
  type: USER_UPDATE_SUCCESS,
  payload: {
    data,
  },
});
export const updateUserFailure = error => ({
  type: USER_UPDATE_FAILURE,
  error,
});

// Обновление данных о пользователе
const updateUser = data => dispatch => {
  dispatch(updateUserFetch());

  return _AXIOS({ url: '/user/update/', method: 'POST', isAuth: true, data })
    .then(res => dispatch(updateUserSuccess(res)))
    .catch(({ data: { error } }) =>
      dispatch(updateUserFailure(R.pathOr('Ошибка сервера, попробуйте позже', ['name'], error))),
    );
};

export const authUserFetch = () => ({
  type: USER_AUTH_FETCH,
});
export const authUserSuccess = data => ({
  type: USER_AUTH_SUCCESS,
  payload: {
    data,
  },
});
export const authUserFailure = error => ({
  type: USER_AUTH_FAILURE,
  error,
});

// Авторизация пользователя
const authUser = data => dispatch => {
  dispatch(authUserFetch());

  return _AXIOS({ url: '/user-profile/login/', method: 'POST', data })
    .then(data => {
      sendSocialActions({
        ga: ['event', 'event_name', { event_category: 'authorization', event_action: 'send' }],
        ym: 'authorization',
      });
      return dispatch(authUserSuccess(data));
    })
    .catch(({ data: { error } }) =>
      dispatch(getUserFailure(R.pathOr('Ошибка сервера, попробуйте позже', ['name'], error))),
    );
};

export const regUserFetch = () => ({
  type: USER_REG_FETCH,
});
export const regUserSuccess = data => ({
  type: USER_REG_SUCCESS,
  payload: {
    data,
  },
});
export const regUserFailure = error => ({
  type: USER_REG_FAILURE,
  error,
});

const userRegConfirm = payload => ({
  type: USER_REG_CONFIRM,
  payload,
});

// Деавторизация пользователя
const logoutUser = () => dispatch => {
  dispatch(needAuth());
};

// Регистрация пользователя
const regUser = body => dispatch => {
  dispatch(regUserFetch());

  return _AXIOS({ url: '/user-profile/create/', method: 'POST', data: body })
    .then(data => dispatch(regUserSuccess(data)))
    .catch(({ data: { error } }) =>
      dispatch(regUserFailure(R.pathOr('Ошибка сервера, попробуйте позже', ['name'], error))),
    );
};

export const regConfirm = secret => dispatch => {
  dispatch(regUserFetch());
  return _AXIOS({ url: '/user-profile/registration-confirm/', method: 'POST', data: secret })
    .then(data => {
      LS.set('token', data.user.token);
      dispatch(userRegConfirm(data));
      dispatch(regUserFailure(data.message));
    })
    .catch(({ data: { error } }) => {
      const name = R.pathOr('Ошибка сервера, попробуйте позже', ['name'], error);
      dispatch(regUserFailure({ name }));
    });
};

export const resetPasswordFetch = () => ({
  type: USER_RESET_PASSWORD_FETCH,
});
export const resetPasswordSuccess = data => ({
  type: USER_RESET_PASSWORD_SUCCESS,
  payload: {
    data,
  },
});
export const resetPasswordFailure = error => ({
  type: USER_RESET_PASSWORD_FAILURE,
  error,
});

const resetPassword = data => dispatch => {
  dispatch(resetPasswordFetch());

  return _AXIOS({ url: '/user-profile/reset-password/', method: 'POST', data })
    .then(data => dispatch(resetPasswordSuccess(data)))
    .catch(({ message }) => dispatch(resetPasswordFailure(message)));
};

const getIdentityProviders = () => dispatch => {
  return _AXIOS({ url: '/oauth2/list' })
    .then(data => dispatch(getIdentityProvidersSuccess(data)))
    .catch(({ data: { error } }) =>
      dispatch(
        getIdentityProvidersFailure(R.pathOr('Ошибка сервера, попробуйте позже', ['name'], error)),
      ),
    );
};

export default {
  getUser,
  getIdentityProviders,
  updateUser,
  authUser,
  regUser,
  logoutUser,
  resetPassword,
};
