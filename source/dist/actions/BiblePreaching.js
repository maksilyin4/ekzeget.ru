import { _AXIOS } from '../ApiConfig';

export const PREACHING_FETCH = 'bible/PREACHING_FETCH';
export const PREACHING_FETCH_SUCCESS = 'bible/PREACHING_FETCH_SUCCESS';
export const PREACHING_FETCH_FAILURE = 'bible/PREACHING_FETCH_FAILURE';

export const PREACHING_BY_BOOK_FETCH = 'bible/PREACHING_BY_BOOK_FETCH';
export const PREACHING_BY_BOOK_FETCH_SUCCESS = 'bible/PREACHING_BY_BOOK_FETCH_SUCCESS';
export const PREACHING_BY_BOOK_FETCH_FAILURE = 'bible/PREACHING_BY_BOOK_FETCH_FAILURE';

export const getPreachingFulfilled = data => ({
  type: PREACHING_FETCH_SUCCESS,
  payload: {
    data,
  },
});

export const getPreachingPending = () => ({
  type: PREACHING_FETCH,
});

export const getPreachingRejected = error => ({
  type: PREACHING_FETCH_FAILURE,
  error,
});

export const getPreachingByBookFulfilled = data => ({
  type: PREACHING_BY_BOOK_FETCH_SUCCESS,
  payload: {
    data,
  },
});

export const getPreachingByBookPending = () => ({
  type: PREACHING_BY_BOOK_FETCH,
});

export const getPreachingByBookRejected = error => ({
  type: PREACHING_BY_BOOK_FETCH_FAILURE,
  error,
});

// Получение проповедей
export const getPreaching = (params = '', cancelToken) => dispatch => {
  dispatch(getPreachingPending());

  const tokenCancel = cancelToken ? { cancelToken } : {};

  return _AXIOS({ url: `/preaching/${params}`, ...tokenCancel }).then(
    response => {
      dispatch(getPreachingFulfilled(response));
    },
    error => {
      dispatch(getPreachingRejected(error));
    },
  );
};

export const getPreachingByBook = ({ book, chapter, perPage = 100, page = 1 }) => dispatch => {
  dispatch(getPreachingByBookPending());
  return _AXIOS({
    url: `/preaching/by-book-and-chapter/?`,
    query: {
      book,
      chapter,
      page,
      'per-page': perPage,
    },
  }).then(
    response => {
      dispatch(getPreachingByBookFulfilled(response));
    },
    error => {
      dispatch(getPreachingByBookRejected(error));
    },
  );
};

export default {
  getPreaching,
  getPreachingByBook,
};
