import { _AXIOS } from '../ApiConfig';

export const LECTURE_GROUP_FETCH = 'LECTURE_GROUP_FETCH';
export const LECTURE_GROUP_FETCH_SUCCESS = 'LECTURE_GROUP_FETCH_SUCCESS';
export const LECTURE_GROUP_FETCH_FAILURE = 'LECTURE_GROUP_FETCH_FAILURE';

export const LECTURE_GROUP_LIST_FETCH = 'LECTURE_GROUP_LIST_FETCH';
export const LECTURE_GROUP_LIST_FETCH_SUCCESS = 'LECTURE_GROUP_LIST_FETCH_SUCCESS';
export const LECTURE_GROUP_LIST_FETCH_FAILURE = 'LECTURE_GROUP_LIST_FETCH_FAILURE';
export const LECTURE_GROUP_LIST_CLEAR = 'LECTURE_GROUP_LIST_CLEAR';

export const LECTURE_DETAIL_FETCH = 'LECTURE_DETAIL_FETCH';
export const LECTURE_DETAIL_FETCH_SUCCESS = 'LECTURE_DETAIL_FETCH_SUCCESS';
export const LECTURE_DETAIL_FETCH_FAILURE = 'LECTURE_DETAIL_FETCH_FAILURE';

export const LECTURE_DETAIL_CLEAR = 'LECTURE_DETAIL_CLEAR';
export const LECTURE_DETAIL_CLEAR_ERROR = 'LECTURE_DETAIL_CLEAR_ERROR';

// Категории лектория
const getLectureGroup = () => dispatch => {
  dispatch(getLectureGroupFetch());

  return _AXIOS({ url: '/lecture/category/' })
    .then(data => dispatch(getLectureGroupSuccess(data)))
    .catch(error => dispatch(getLectureGroupFailure(error)));
};
export const getLectureGroupFetch = () => ({
  type: LECTURE_GROUP_FETCH,
});
export const getLectureGroupSuccess = data => ({
  type: LECTURE_GROUP_FETCH_SUCCESS,
  payload: {
    data,
  },
});
export const getLectureGroupFailure = error => ({
  type: LECTURE_GROUP_FETCH_FAILURE,
  error,
});

// Список статей группы лектория
const getLectureGroupList = group_id => dispatch => {
  dispatch(getLectureGroupListFetch());

  return _AXIOS({ url: `/lecture/${group_id}` })
    .then(data => {
      if (data && data?.lecture.length) {
        dispatch(getLectureGroupListSuccess(data));
      } else {
        dispatch(getLectureGroupListFailure('404'));
      }
    })
    .catch(error => dispatch(getLectureGroupListFailure(error)));
};
export const getLectureGroupListFetch = () => ({
  type: LECTURE_GROUP_LIST_FETCH,
});
export const getLectureGroupListSuccess = data => ({
  type: LECTURE_GROUP_LIST_FETCH_SUCCESS,
  payload: {
    data,
  },
});
export const getLectureGroupListFailure = error => ({
  type: LECTURE_GROUP_LIST_FETCH_FAILURE,
  error,
});

const cleartLectureGroupList = () => ({
  type: LECTURE_GROUP_LIST_CLEAR,
});

// Детальная страница
const getLectureDetail = id => dispatch => {
  dispatch(getLectureDetailFetch());

  return _AXIOS({ url: `/lecture/detail/${id}/` })
    .then(data => dispatch(getLectureDetailSuccess(data)))
    .catch(error => dispatch(getLectureDetailFailure(error)));
};
export const getLectureDetailFetch = () => ({
  type: LECTURE_DETAIL_FETCH,
});
export const getLectureDetailSuccess = data => ({
  type: LECTURE_DETAIL_FETCH_SUCCESS,
  payload: {
    data,
  },
});
export const getLectureDetailFailure = error => ({
  type: LECTURE_DETAIL_FETCH_FAILURE,
  error,
});

const lectureDetailClear = () => ({
  type: LECTURE_DETAIL_CLEAR,
});

const lectureDetailClearError = () => ({
  type: LECTURE_DETAIL_CLEAR_ERROR,
});

export default {
  getLectureGroup,
  getLectureGroupList,
  getLectureDetail,
  cleartLectureGroupList,
  lectureDetailClear,
  lectureDetailClearError,
};
