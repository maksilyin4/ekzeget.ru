import axios from 'axios';
import { _AXIOS } from '../ApiConfig';
import { META_CONTEXT_DATA } from '../../constants';

const CancelToken = axios.CancelToken;
let source = CancelToken.source();

export const EXEGET_FETCH = 'EXEGET_FETCH';
export const EXEGET_FETCH_SUCCESS = 'EXEGET_FETCH_SUCCESS';
export const EXEGET_FETCH_FAILURE = 'EXEGET_FETCH_FAILURE';
export const EXEGET_FETCH_CLEAR = 'EXEGET_FETCH_CLEAR';

export const setMetaContext = payload => ({
  type: META_CONTEXT_DATA,
  payload,
});

export const getExeget = ({ id = '', info }) => (dispatch, getStore) => {
  dispatch(getExegetPending());
  const ekzegetId = id.replace('tolkovatel-', '');

  source?.cancel();
  source = CancelToken.source();
  const query = info ? { id: ekzegetId, info } : { id: ekzegetId };
  return _AXIOS({
    url: `/ekzeget/?`,
    cancelToken: source?.token,
    query,
  })
    .then(data => {
      const store = getStore();
      if (data && data.ekzeget) {
        dispatch(getExegetFulfilled(data));
        dispatch(
          setMetaContext({
            book: store?.metaData?.metaContext?.book ?? {},
            verseMeta: store?.metaData?.metaContext?.verseMeta ?? {},
            tolkovatel: data?.ekzeget,
          }),
        );
      } else {
        // eslint-disable-next-line no-throw-literal
        throw { message: '404' };
      }
    })
    .catch(error => dispatch(getExegetRejected(error)));
};

export const getExegetFulfilled = data => ({
  type: EXEGET_FETCH_SUCCESS,
  payload: {
    data,
  },
});

export const getExegetPending = () => ({
  type: EXEGET_FETCH,
});

export const getExegetClear = () => ({
  type: EXEGET_FETCH_CLEAR,
});

export const getExegetRejected = error => ({
  type: EXEGET_FETCH_FAILURE,
  error,
});

export default {
  getExegetClear,
  getExeget,
};
