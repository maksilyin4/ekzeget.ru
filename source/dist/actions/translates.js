import { _AXIOS } from '../ApiConfig';
// import { setCookie } from '../utils';
// import { LS } from '../browserUtils';

export const REQUEST_TRANSLATES = 'translates/REQUEST_TRANSLATES';

export const RECEIVE_TRANSLATES_FETCH = 'translates/RECEIVE_TRANSLATES_FETCH';
export const RECEIVE_TRANSLATES_SUCCESS = 'translates/RECEIVE_TRANSLATES_SUCCESS';
export const RECEIVE_TRANSLATES_FAILURE = 'translates/RECEIVE_TRANSLATES_FAILURE';
export const CHOOSE_TRANSLATE = 'translates/CHOOSE_TRANSLATE';

export const getTranslatesPending = () => ({
  type: RECEIVE_TRANSLATES_FETCH,
});

const getTranslatesSuccess = payload => ({
  type: RECEIVE_TRANSLATES_SUCCESS,
  payload,
});
export const getTranslatesRejected = () => ({
  type: RECEIVE_TRANSLATES_FAILURE,
});

export const chooseTranslate = payload => {
  return {
    type: CHOOSE_TRANSLATE,
    payload,
  };
};

export function getTranslates() {
  return dispatch => {
    dispatch(getTranslatesPending());
    _AXIOS({ url: '/book/translate-list' })
      .then(({ translates }) => dispatch(getTranslatesSuccess(translates)))
      .catch(error => dispatch(getTranslatesRejected(error)));
  };
}
