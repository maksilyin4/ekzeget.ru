import axios from 'axios';
import { _AXIOS } from '../ApiConfig';

const CancelToken = axios.CancelToken;
let source = CancelToken.source();

export const INTERPRETATION_URL_FULFILLED = 'INTERPRETATION_URL_FULFILLED';
export const INTERPRETATION_URL_REJECTED = 'INTERPRETATION_URL_REJECTED';
export const INTERPRETATION_URL_PEDNING = 'INTERPRETATION_URL_PEDNING';

export const INTERPRETATION_URL_CLEAR = 'INTERPRETATION_URL_CLEAR';

const getInterpretationByUrl = ({ book_code, chapter_num, verse_id, ekzeget }) => dispatch => {
  dispatch(getInterpretationByUrlPending());

  source?.cancel();
  source = CancelToken.source();

  const interpretatorId = ekzeget ? { interpretator_id: ekzeget.replace('tolkovatel-', '') } : {};
  return _AXIOS({
    url: `/interpretation/${book_code}/?`,
    cancelToken: source?.token,
    query: {
      chapter: chapter_num,
      number: verse_id,
      ...interpretatorId,
    },
  })
    .then(data => dispatch(getInterpretationByUrlFulfilled(data)))
    .catch(error => dispatch(getInterpretationByUrlRejected(error)));
};

export const getInterpretationByUrlFulfilled = data => ({
  type: INTERPRETATION_URL_FULFILLED,
  payload: data,
});

export const getInterpretationByUrlPending = () => ({
  type: INTERPRETATION_URL_PEDNING,
});

export const getInterpretationByUrlRejected = error => ({
  type: INTERPRETATION_URL_REJECTED,
  error,
});

export const clearInterpretationByUrl = () => ({
  type: INTERPRETATION_URL_CLEAR,
});

export default getInterpretationByUrl;
