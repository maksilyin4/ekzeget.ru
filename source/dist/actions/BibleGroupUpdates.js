import { AXIOS } from '../ApiConfig';

export const GET_BIBLE_GROUP_UPDATES_FULFILLED = 'GET_BIBLE_GROUP_UPDATES_FULFILLED';
export const GET_BIBLE_GROUP_UPDATES_REJECTED = 'GET_BIBLE_GROUP_UPDATES_REJECTED';
export const GET_BIBLE_GROUP_UPDATES_PENDING = 'GET_BIBLE_GROUP_UPDATES_PENDING';

export const GET_BIBLE_GROUP_NOTES_UPDATES_FULFILLED = 'GET_BIBLE_GROUP_NOTES_UPDATES_FULFILLED';
export const GET_BIBLE_GROUP_NOTES_UPDATES_REJECTED = 'GET_BIBLE_GROUP_NOTES_UPDATES_REJECTED';
export const GET_BIBLE_GROUP_NOTES_UPDATES_PENDING = 'GET_BIBLE_GROUP_NOTES_UPDATES_PENDING';
export const GET_BIBLE_GROUP_NOTES_UPDATES_CLEAR = 'GET_BIBLE_GROUP_NOTES_UPDATES_CLEAR';

export const getBibleGroupUpdatesFulfilled = data => ({
  type: GET_BIBLE_GROUP_UPDATES_FULFILLED,
  payload: data,
});

export const getBibleGroupUpdatesRejected = error => ({
  type: GET_BIBLE_GROUP_UPDATES_REJECTED,
  payload: error,
});

export const getBibleGroupUpdatesPending = () => ({
  type: GET_BIBLE_GROUP_UPDATES_PENDING,
});

export const getBibleGroupNotesUpdatesFulfilled = data => ({
  type: GET_BIBLE_GROUP_NOTES_UPDATES_FULFILLED,
  payload: data,
});

export const getBibleGroupNotesUpdatesRejected = error => ({
  type: GET_BIBLE_GROUP_NOTES_UPDATES_REJECTED,
  payload: error,
});

export const getBibleGroupNotesUpdatesPending = () => ({
  type: GET_BIBLE_GROUP_NOTES_UPDATES_PENDING,
});

export const getBibleGroupNotesUpdatesClear = () => ({
  type: GET_BIBLE_GROUP_NOTES_UPDATES_CLEAR,
});

// Получение библейских групп
export function getBibleGroupUpdates(limit = '') {
  return dispatch => {
    dispatch(getBibleGroupUpdatesPending());
    AXIOS.get(`/bible-group/?limit=${limit}`).then(
      response => {
        if (response.data.status === 'ok') {
          dispatch(getBibleGroupUpdatesFulfilled(response.data.group));
        } else {
          dispatch(getBibleGroupUpdatesRejected('error'));
        }
      },
      error => {
        dispatch(getBibleGroupUpdatesRejected(error));
      },
    );
  };
}

// Материалы для ведущих
export function getBibleGroupNotesUpdates() {
  return dispatch => {
    dispatch(getBibleGroupNotesUpdatesPending());
    AXIOS.get('/bible-group/note').then(
      response => {
        if (response.data.status === 'ok') {
          dispatch(getBibleGroupNotesUpdatesFulfilled(response.data.note));
        } else {
          dispatch(getBibleGroupNotesUpdatesRejected('error'));
        }
      },
      error => {
        dispatch(getBibleGroupNotesUpdatesRejected(error));
      },
    );
  };
}

export default { getBibleGroupUpdates, getBibleGroupNotesUpdates, getBibleGroupNotesUpdatesClear };
