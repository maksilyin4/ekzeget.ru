export const SET_SCREEN_PHONE = 'screen/SET_SCREEN_PHONE';
export const SET_SCREEN_TABLET = 'screen/SET_SCREEN_TABLET';
export const SET_SCREEN_TABLET_MEDIUM = 'screen/SET_SCREEN_TABLET_MEDIUM';
export const SET_SCREEN_DESKTOP = 'screen/SET_SCREEN_DESKTOP';

export const setWidth = () => (dispatch, getState) => {
  const {
    screen: { desktop, tablet, phone, tabletMedium },
  } = getState();

  if (window.matchMedia('(max-width: 767px)').matches && !phone) {
    dispatch({ type: SET_SCREEN_PHONE });
  } else if (
    window.matchMedia('(min-width: 768px)').matches &&
    window.matchMedia('(max-width: 1023px)').matches &&
    !tablet
  ) {
    dispatch({ type: SET_SCREEN_TABLET });
  } else if (
    window.matchMedia('(min-width: 1024px)').matches &&
    window.matchMedia('(max-width: 1439px)').matches &&
    !tabletMedium
  ) {
    dispatch({ type: SET_SCREEN_TABLET_MEDIUM });
  } else if (window.matchMedia('(min-width: 1440px)').matches && !desktop) {
    dispatch({ type: SET_SCREEN_DESKTOP });
  }
};
