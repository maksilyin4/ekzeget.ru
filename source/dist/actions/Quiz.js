import { _AXIOS } from '../ApiConfig';
import { DEFAULT_COUNT_QUESTIONS } from '../utils';

export const QUEEZE_FETCH = 'quiz/QUEEZE_FETCH';
export const QUEEZE_SUCCESS = 'quiz/QUEEZE_SUCCESS';
export const QUEEZE_FAILURE = 'quiz/QUEEZE_FAILURE';

export const CURRENT_QUEEZE_FETCH = 'quiz/CURRENT_QUEEZE_FETCH';
export const CURRENT_QUEEZE_SUCCESS = 'quiz/CURRENT_QUEEZE_SUCCESS';
export const CURRENT_QUEEZE_SUCCESS_PLAN = 'quiz/CURRENT_QUEEZE_SUCCESS_PLAN';
export const CURRENT_QUEEZE_FAILURE = 'quiz/CURRENT_QUEEZE_FAILURE';

export const QUESTION_FETCH = 'quiz/QUESTION_FETCH';
export const QUESTION_SUCCESS = 'quiz/QUESTION_SUCCESS';
export const QUESTION_FAILURE = 'quiz/QUESTION_FAILURE';

export const QUESTIONS_FETCH = 'quiz/QUESTIONS_FETCH';
export const QUESTIONS_SUCCESS = 'quiz/QUESTIONS_SUCCESS';
export const QUESTIONS_FAILURE = 'quiz/QUESTIONS_FAILURE';

const getQueezeFetch = () => ({
  type: QUEEZE_FETCH,
});

const getQueezeSuccess = data => ({
  type: QUEEZE_SUCCESS,
  payload: data,
});

const getQueezeFailure = error => ({
  type: QUEEZE_FAILURE,
  error,
});

const getCurrentQueezeFetch = () => ({
  type: CURRENT_QUEEZE_FETCH,
});

const getCurrentQueezeSuccess = data => ({
  type: CURRENT_QUEEZE_SUCCESS,
  payload: data,
});

const getCurrentQueezePlanIdSuccess = data => ({
  type: CURRENT_QUEEZE_SUCCESS_PLAN,
  payload: data,
});

const getCurrentQueezeFailure = error => ({
  type: CURRENT_QUEEZE_FAILURE,
  error,
});

const getQuestionFetch = () => ({
  type: QUESTION_FETCH,
});

const getQuestionSuccess = data => ({
  type: QUESTION_SUCCESS,
  payload: data,
});

const getQuestionFailure = error => ({
  type: QUESTION_FAILURE,
  error,
});

const getQuestionsFetch = () => ({
  type: QUESTIONS_FETCH,
});

const getQuestionsSuccess = data => ({
  type: QUESTIONS_SUCCESS,
  payload: data,
});

const getQuestionsFailure = error => ({
  type: QUESTIONS_FAILURE,
  payload: error,
});

export const getQuestions = (page = 1, perPage = DEFAULT_COUNT_QUESTIONS) => dispatch => {
  dispatch(getQuestionsFetch());

  return _AXIOS({ url: `/queeze/question-list?per-page=${perPage}&page=${page}` })
    .then(({ questions = [], totalCount = 0 }) =>
      dispatch(getQuestionsSuccess({ questions, totalCount })),
    )
    .catch(({ data: { error } }) => dispatch(getQuestionsFailure(error)));
};

export const getCurrentQueeze = (isCreate = false, data = []) => dispatch => {
  dispatch(getQueezeFetch());

  if (isCreate) {
    return dispatch(getQueezeSuccess(data));
  }

  return _AXIOS({ url: '/queeze/current/' })
    .then(({ queeze }) => dispatch(getQueezeSuccess(queeze)))
    .catch(error => dispatch(getQueezeFailure(error)));
};

export const getQuizList = () => dispatch => {
  dispatch(getCurrentQueezeFetch());

  return _AXIOS({ url: '/queeze/list/' })
    .then(({ queezes = [] }) => dispatch(getCurrentQueezeSuccess(queezes)))
    .catch(({ name }) => dispatch(getCurrentQueezeFailure(name)));
};

export const getQuizByPlanId = plan_id => dispatch => {
  return _AXIOS({ url: `/queeze/list/?plan_id=${plan_id}` })
    .then(({ queezes = [] }) => dispatch(getCurrentQueezePlanIdSuccess(queezes?.at(-1))))
    .catch(() => null);
};

export const getCurrentQuestion = code => dispatch => {
  dispatch(getQuestionFetch());

  return _AXIOS({ url: `/queeze/question/${code}/` })
    .then(res => {
      // Если пользователь уже начал викторину и у него есть такой вопрос
      if (res.exists) {
        dispatch(getQueezeSuccess(res.current));
      }

      // В любом случае необходимо, тк нормально получить сессию и куки
      // на SSR нет возможности с 100% вероятностью
      dispatch(getQuestionSuccess(res));
    })
    .catch(({ status }) => dispatch(getQuestionFailure(status)));
};
