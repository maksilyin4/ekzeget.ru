import { _AXIOS } from '../ApiConfig';

export const BIBLE_MEDIA_LIBRARY_PENDING = 'bible/BIBLE_MEDIA_LIBRARY_PENDING';
export const BIBLE_MEDIA_LIBRARY_FULFILLED = 'bible/BIBLE_MEDIA_LIBRARY_FULFILLED';
export const BIBLE_MEDIA_LIBRARY_REJECTED = 'bible/BIBLE_MEDIA_LIBRARY_REJECTED';

export const getBibleMediaLibraryFulfilled = data => ({
  type: BIBLE_MEDIA_LIBRARY_FULFILLED,
  payload: {
    data,
  },
});

export const getBibleMediaLibraryPending = () => ({
  type: BIBLE_MEDIA_LIBRARY_PENDING,
});

export const getBibleMediaLibraryRejected = error => ({
  type: BIBLE_MEDIA_LIBRARY_REJECTED,
  error,
});

export const getBibleMediaLibrary = (book, chapter, verse) => dispatch => {
  dispatch(getBibleMediaLibraryPending());

  return _AXIOS({ url: `/mediateka/?book_id=${book}&chapter=${chapter}&verse_id=${verse}` })
    .then(data => dispatch(getBibleMediaLibraryFulfilled(data)))
    .catch(error => dispatch(getBibleMediaLibraryRejected(error)));
};

export default getBibleMediaLibrary;
