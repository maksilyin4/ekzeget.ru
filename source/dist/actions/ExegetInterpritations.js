import { AXIOS } from '../ApiConfig';

export const EXEGET_INTERPRITATIONS_FETCH = 'EXEGET_INTERPRITATIONS_FETCH';
export const EXEGET_INTERPRITATIONS_FETCH_SUCCESS = 'EXEGET_INTERPRITATIONS_FETCH_SUCCESS';
export const EXEGET_INTERPRITATIONS_FETCH_FAILURE = 'EXEGET_INTERPRITATIONS_FETCH_FAILURE';

const getExegetInterpritations = (interpretator_id, request = '') => dispatch => {
  dispatch(getExegetInterpritationsPending());

  return AXIOS.get(`/ekzeget/verse-list/${interpretator_id}/${request}`).then(
    response => {
      dispatch(getExegetInterpritationsFulfilled(response.data));
    },
    error => {
      dispatch(getExegetInterpritationsRejected(error));
    },
  );
};

export const getExegetInterpritationsFulfilled = data => ({
  type: EXEGET_INTERPRITATIONS_FETCH_SUCCESS,
  payload: {
    data,
  },
});

export const getExegetInterpritationsPending = () => ({
  type: EXEGET_INTERPRITATIONS_FETCH,
});

export const getExegetInterpritationsRejected = error => ({
  type: EXEGET_INTERPRITATIONS_FETCH_FAILURE,
  error,
});

export default {
  getExegetInterpritations,
};
