import { _AXIOS } from '../ApiConfig';
import { sendSocialActions } from '../utils';

export const BOOKMARKS_FETCH = 'BOOKMARKS_FETCH';
export const BOOKMARKS_FETCH_SUCCESS = 'BOOKMARKS_FETCH_SUCCESS';
export const BOOKMARKS_FETCH_FAILURE = 'BOOKMARKS_FETCH_FAILURE';

export const BOOKMARK_ADD_FETCH = 'BOOKMARK_ADD_FETCH';
export const BOOKMARK_ADD_FETCH_SUCCESS = 'BOOKMARK_ADD_FETCH_SUCCESS';
export const BOOKMARK_ADD_FETCH_FAILURE = 'BOOKMARK_ADD_FETCH_FAILURE';

export const BOOKMARK_DELETE_FETCH = 'BOOKMARK_DELETE_FETCH';
export const BOOKMARK_DELETE_FETCH_SUCCESS = 'BOOKMARK_DELETE_FETCH_SUCCESS';
export const BOOKMARK_DELETE_FETCH_FAILURE = 'BOOKMARK_DELETE_FETCH_FAILURE';

const getBookmarks = () => dispatch => {
  dispatch(getBookmarksPending());

  return _AXIOS({ url: '/user/bookmark/?per-page=1000', isAuth: true })
    .then(data => {
      dispatch(getBookmarksFulfilled(data));
    })
    .catch(error => {
      dispatch(getBookmarksRejected(error));
    });
};

export const getBookmarksFulfilled = data => ({
  type: BOOKMARKS_FETCH_SUCCESS,
  payload: {
    data,
  },
});

export const getBookmarksPending = () => ({
  type: BOOKMARKS_FETCH,
});

export const getBookmarksRejected = error => ({
  type: BOOKMARKS_FETCH_FAILURE,
  error,
});

const addBookmark = data => dispatch => {
  dispatch(addBookmarkPending());

  return _AXIOS({ url: '/user/bookmark/', method: 'PUT', data, isAuth: true })
    .then(data => {
      dispatch(addBookmarkFulfilled(data));
      sendSocialActions({
        ga: ['event', 'event_name', { event_category: 'Dobavit_stih', event_action: 'click' }],
        ym: 'Dobavit_stih',
      });
    })
    .catch(error => {
      dispatch(addBookmarkRejected(error));
    });
};

export const addBookmarkFulfilled = data => ({
  type: BOOKMARK_ADD_FETCH_SUCCESS,
  payload: {
    data,
  },
});

export const addBookmarkPending = () => ({
  type: BOOKMARK_ADD_FETCH,
});

export const addBookmarkRejected = error => ({
  type: BOOKMARK_ADD_FETCH_FAILURE,
  error,
});

export const deleteBookmark = id => dispatch => {
  dispatch(deleteBookmarkPending());
  const data = `mark_id=${id}`;

  return _AXIOS({ url: '/user/bookmark/', method: 'DELETE', data, isAuth: true })
    .then(data => dispatch(deleteBookmarkFulfilled(data)))
    .catch(error => dispatch(deleteBookmarkRejected(error)));
};

export const deleteBookmarkPending = () => ({
  type: BOOKMARK_DELETE_FETCH,
});

export const deleteBookmarkFulfilled = data => ({
  type: BOOKMARK_DELETE_FETCH_SUCCESS,
  payload: {
    data,
  },
});

export const deleteBookmarkRejected = error => ({
  type: BOOKMARK_DELETE_FETCH_FAILURE,
  error,
});

export default {
  getBookmarks,
  addBookmark,
  deleteBookmark,
};
