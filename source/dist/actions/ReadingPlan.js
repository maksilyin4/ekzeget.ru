import R from 'ramda';
import qs from 'qs';
import { AXIOS } from '../ApiConfig';
import { LS } from '../browserUtils';

export const READING_PLANS_FETCH = 'lk/plan/READING_PLANS_FETCH';
export const READING_PLANS_SUCCESS = 'lk/plan/READING_PLANS_SUCCESS';
export const READING_PLANS_FAILURE = 'lk/plan/READING_PLANS_FAILURE';

export const READING_PLAN_DETAIL_FETCH = 'lk/plan/READING_PLAN_DETAIL_FETCH';
export const READING_PLAN_DETAIL_SUCCESS = 'lk/plan/READING_PLAN_DETAIL_SUCCESS';
export const READING_PLAN_DETAIL_FAILURE = 'lk/plan/READING_PLAN_DETAIL_FAILURE';

export const USER_RP_GET_FETCH = 'lk/plan/USER_RP_GET_FETCH';
export const READING_PLAN_DELETE = 'lk/plan/READING_PLAN_DELETE';
export const READING_PLAN_DELETE_FAILURE = 'lk/plan/READING_PLAN_DELETE_FAILURE';
export const USER_RP_GET_SUCCESS = 'lk/plan/USER_RP_GET_SUCCESS';
export const USER_RP_GET_FAILURE = 'lk/plan/USER_RP_GET_FAILURE';

export const GET_ARCHIVE_SUCCESS = 'lk/plan/GET_ARCHIVE_SUCCESS';
export const GET_ARCHIVE_PENDING = 'lk/plan/GET_ARCHIVE_PENDING';
export const GET_ARCHIVE_FAILURE = 'lk/plan/GET_ARCHIVE_FAILURE';

export const USER_RP_ADD_FETCH = 'lk/plan/USER_RP_ADD_FETCH';
export const USER_RP_TAB = 'USER_RP_TAB';
export const USER_RP_ADD_SUCCESS = 'lk/plan/USER_RP_ADD_SUCCESS';
export const USER_RP_ADD_FAILURE = 'lk/plan/USER_RP_ADD_FAILURE';

export const USER_RP_DEL_FETCH = 'lk/plan/USER_RP_DEL_FETCH';
export const USER_RP_DEL_SUCCESS = 'lk/plan/USER_RP_DEL_SUCCESS';
export const USER_RP_DEL_FAILURE = 'lk/plan/USER_RP_DEL_FAILURE';

export const SUBSCRIBE_PLAN = 'lk/plan/SUBSCRIBE_PLAN';
export const TOGGLE_OPEN_READING_PLAN = 'lk/plan/TOGGLE_OPEN_READING_PLAN';

export const COMPLETE_DAY = 'lk/plan/COMPLETE_DAY';
export const COMPLETE_CHAPTER = 'lk/plan/COMPLETE_CHAPTER';

export const DELETE_DAYS = 'lk/plan/DELETE_DAYS';

// Получение списка планов чтений
const getReadingPlans = () => dispatch => {
  dispatch(getReadingPlansFetch());

  return AXIOS.get('/reading/plan/').then(
    response => dispatch(getReadingPlansSuccess(response.data.plan)),
    error => dispatch(getReadingPlansFailure(error)),
  );
};
export const getReadingPlansFetch = () => ({
  type: READING_PLANS_FETCH,
});
export const getReadingPlansSuccess = payload => ({
  type: READING_PLANS_SUCCESS,
  payload,
});

export const setActiveTab = payload => ({
  type: USER_RP_TAB,
  payload,
});

export const toggleOpenPlanAction = payload => ({
  type: TOGGLE_OPEN_READING_PLAN,
  payload,
});

export const getReadingPlansFailure = error => ({
  type: READING_PLANS_FAILURE,
  error,
});

// Получение графика по плану чтения
const getReadingPlanDetail = plan_id => dispatch => {
  dispatch(getReadingPlanDetailFetch());
  return AXIOS.get(`/reading/plan-detail/${plan_id}`).then(
    response => {
      if (response.data.status === 'ok') {
        dispatch(getReadingPlanDetailSuccess(response.data.plan));
      } else {
        dispatch(getReadingPlanDetailFailure({ message: 'ошибка получения плана' }));
      }
    },
    error => dispatch(getReadingPlanDetailFailure(error)),
  );
};

export const getReadingPlanDetailFetch = () => ({
  type: READING_PLAN_DETAIL_FETCH,
});

export const getReadingPlanDetailSuccess = payload => ({
  type: READING_PLAN_DETAIL_SUCCESS,
  payload,
});

export const getReadingPlanDetailFailure = error => ({
  type: READING_PLAN_DETAIL_FAILURE,
  error,
});

// удаление плана

export const deleteReadingPlanSuccess = payload => ({
  type: READING_PLAN_DELETE,
  payload,
});

export const deleteReadingPlanFailure = error => ({
  type: READING_PLAN_DELETE_FAILURE,
  error,
});

export const deleteReadingPlan = plan_id => dispatch => {
  return AXIOS.delete(`/reading/plan/${plan_id}`, {
    headers: {
      'X-Authentication-Token': LS.get('token'),
    },
  }).then(
    response => {
      if (response.data.status === 'ok') {
        dispatch(deleteReadingPlanSuccess(plan_id));
      }
    },
    error => dispatch(deleteReadingPlanFailure(error)),
  );
};

// Получение плана чтения для текущего пользователя

export const getUserReadingPlanFetch = () => ({
  type: USER_RP_GET_FETCH,
});
export const getUserReadingPlanSuccess = payload => ({
  type: USER_RP_GET_SUCCESS,
  payload,
});
export const getUserReadingPlanFailure = error => ({
  type: USER_RP_GET_FAILURE,
  error,
});

export const getUserReadingPlan = (day = '') => dispatch => {
  dispatch(getUserReadingPlanFetch());

  return AXIOS.get(`/user/plan/${day}`, {
    headers: {
      'X-Authentication-Token': LS.get('token'),
    },
  }).then(
    response => {
      if (response.data.status === 'ok') {
        dispatch(getUserReadingPlanSuccess(response.data.plan));
      } else {
        dispatch(getUserReadingPlanFailure({ message: 'Ошибка получения плана чтения' }));
      }
    },
    error => dispatch(getUserReadingPlanFailure(error)),
  );
};

export const getArchivedSuccess = payload => ({
  type: GET_ARCHIVE_SUCCESS,
  payload,
});

export const getArchivedPending = payload => ({
  type: GET_ARCHIVE_PENDING,
  payload,
});

export const getArchivedFailure = error => ({
  type: GET_ARCHIVE_FAILURE,
  error,
});

export const getArchivedPlans = () => async dispatch => {
  dispatch(getArchivedPending());
  try {
    const { data } = await AXIOS.get(`/reading/archive`, {
      headers: {
        'X-Authentication-Token': LS.get('token'),
      },
    });
    dispatch(getArchivedSuccess(data.archive));
  } catch (e) {
    getArchivedFailure(e);
  }
};

// Подключение плана чтения для текущего опльзователя
export const addUserReadingPlanFetch = payload => ({
  type: USER_RP_ADD_FETCH,
  payload,
});

export const addUserReadingPlanSuccess = payload => ({
  type: USER_RP_ADD_SUCCESS,
  payload,
});

export const addUserReadingPlanFailure = error => ({
  type: USER_RP_ADD_FAILURE,
  error,
});

export const subscribePlan = payload => ({
  type: SUBSCRIBE_PLAN,
  payload,
});

const addUserReadingPlan = params => dispatch => {
  dispatch(addUserReadingPlanFetch(params.plan_id));
  const data = qs.stringify(params);
  return AXIOS.put('/user/plan/', data, {
    headers: {
      'X-Authentication-Token': LS.get('token'),
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  }).then(
    response => {
      if (response.data.status === 'ok') {
        const respPlan = R.pathOr({ plan_id: 0 }, ['data', 'plan'], response);
        const planToStore = { [respPlan.plan_id]: respPlan };

        // подключение/отключение подписки
        if ('subscribe' in params) {
          dispatch(subscribePlan(params));
        } else {
          dispatch(addUserReadingPlanSuccess(planToStore));
        }
      } else {
        dispatch(addUserReadingPlanFailure(''));
      }
    },
    error => dispatch(addUserReadingPlanFailure(error)),
  );
};

export const editUserReadingPlan = body => dispatch => {
  return AXIOS.patch('/user/plan/', body, {
    headers: {
      'X-Authentication-Token': LS.get('token'),
    },
  }).then(
    response => {
      if (response.data.status === 'ok') {
        const respPlan = R.pathOr({ plan_id: 0 }, ['data', 'plan'], response);
        const planToStore = { [respPlan.plan_id]: respPlan };
        dispatch(addUserReadingPlanSuccess(planToStore));
      } else {
        dispatch(addUserReadingPlanFailure(''));
      }
    },
    error => dispatch(addUserReadingPlanFailure(error)),
  );
};

// Отключение плана чтения для текущего опльзователя

export const deleteUserReadingPlanFetch = payload => ({
  type: USER_RP_DEL_FETCH,
  payload,
});

export const deleteUserReadingPlanSuccess = payload => ({
  type: USER_RP_DEL_SUCCESS,
  payload,
});

export const deleteUserReadingPlanFailure = error => ({
  type: USER_RP_DEL_FAILURE,
  error,
});

export const deleteUserReadingPlan = plan_id => (dispatch, getState) => {
  dispatch(deleteUserReadingPlanFetch(plan_id));
  const deletedId = R.pathOr('', ['readingPlan', 'userPlans', plan_id, 'id'], getState());

  return AXIOS.delete('/user/plan/', {
    data: `id=${deletedId}`,
    headers: {
      'X-Authentication-Token': LS.get('token'),
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  }).then(
    response => {
      if (response.data.status === 'ok') {
        dispatch(deleteUserReadingPlanSuccess(plan_id));
      } else {
        dispatch(deleteUserReadingPlanFailure(''));
      }
    },
    error => dispatch(deleteUserReadingPlanFailure(error)),
  );
};

// complete day action

const completeChapter = payload => ({
  type: COMPLETE_CHAPTER,
  payload,
});

export function sendCompleteChapter(plan_id) {
  return dispatch => {
    return AXIOS.post(`/user/plan/?plan_id=${plan_id}`, null, {
      headers: {
        'X-Authentication-Token': LS.get('token'),
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    }).then(() => {
      dispatch(completeChapter(plan_id));
    });
  };
}

// reset days

const deleteDays = payload => ({
  type: DELETE_DAYS,
  payload,
});

export function sendResetDays() {
  return dispatch =>
    AXIOS.post(
      '/user/reset-plan/',
      {},
      {
        headers: {
          'X-Authentication-Token': LS.get('token'),
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      },
    ).then(res => {
      if (res.data.status === 'ok') {
        dispatch(deleteDays());
      }
    });
}

export const saveSelectedDays = (schedule, id) => dispatch =>
  AXIOS.post('/user/plan-schedule/', qs.stringify({ schedule, plan_id: id }), {
    headers: {
      'X-Authentication-Token': LS.get('token'),
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  })
    .then(() => {
      dispatch(getUserReadingPlanFetch());
    })
    .catch(() => {});

export default {
  getReadingPlans,
  getUserReadingPlan,
  setActiveTab,
  addUserReadingPlan,
  deleteUserReadingPlan,
  getReadingPlanDetail,
  deleteReadingPlan,
  sendCompleteChapter,
  sendResetDays,
  saveSelectedDays,
};
