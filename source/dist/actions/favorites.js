import R from 'ramda';
import qs from 'qs';
import { _AXIOS } from '../ApiConfig';

export const REQUEST_FAVORITES = 'favorites/REQUEST_FAVORITES';
export const RECEIVE_FAVORITES_SUCCESS = 'favorites/RECEIVE_FAVORITES_SUCCESS';
export const RECEIVE_FAVORITES_INTER_SUCCESS = 'favorites/RECEIVE_FAVORITES_INTER_SUCCESS';
export const RECEIVE_FAVORITES_FAILURE = 'favorites/RECEIVE_FAVORITES_FAILURE';
export const DELETE_FAVORITE_VERSE = 'favorites/DELETE_FAVORITE_VERSE';
export const DELETE_FAVORITE_INTER = 'favorites/DELETE_FAVORITE_INTER';
export const ADD_FAV_VERSE = 'favorites/ADD_FAV_VERSE';
export const ADD_FAV_INTERP = 'favorites/ADD_FAV_INTERP';
export const GET_NOTES = 'favorites/GET_NOTES';
export const CREATE_NOTE = 'favorites/CREATE_NOTE';
export const UPDATE_NOTES = 'favorites/UPDATE_NOTES';
export const DELETE_NOTE = 'favorites/DELETE_NOTE';
export const CHOOSE_TAG = 'favorites/CHOOSE_TAG';
export const UPDATE_FAV_VERSE = 'favorites/UPDATE_FAV_VERSE';
export const SET_SESSION_TAG = 'favorites/SET_SESSION_TAG';
// Verse Actions

export function getFavoriteVerse(tag = '') {
  return dispatch => {
    dispatch(requestFav());
    _AXIOS({ url: 'user/verse-fav/', isAuth: true, query: { 'per-page': 3000, tag } })
      .then(({ verse }) => dispatch(receiveFavSuccess(verse)))
      .catch(() => {
        dispatch(receiveFavFailure());
      });
  };
}

export function deleteFavoriteVerse({ id, verseId }) {
  return dispatch => {
    dispatch(requestFav());

    _AXIOS({
      method: 'DELETE',
      url: 'user/verse-fav/',
      isAuth: true,
      data: qs.stringify({ id }),
    })
      .then(() => dispatch(deleteFavVerse({ id, verseId })))
      .catch(() => 'error');
  };
}

export const addFavToStore = payload => dispatch => {
  dispatch({
    type: ADD_FAV_VERSE,
    payload,
  });
};

const receiveFavSuccess = payload => ({
  type: RECEIVE_FAVORITES_SUCCESS,
  payload,
});

const deleteFavVerse = payload => ({
  type: DELETE_FAVORITE_VERSE,
  payload,
});

export const chooseTag = payload => ({
  type: CHOOSE_TAG,
  payload,
});

export const updateFavVerse = payload => ({
  type: UPDATE_FAV_VERSE,
  payload,
});

// Interpretation actions

export function getFavoriteInter() {
  return dispatch => {
    dispatch(requestFav());

    _AXIOS({ url: 'user/interpretation-fav/?per-page=3000', isAuth: true })
      .then(({ interpretation }) => dispatch(receiveInterSuccess(interpretation)))
      .catch(() => dispatch(receiveFavFailure()));
  };
}

export function deleteFavoriteInter(id) {
  return dispatch => {
    dispatch(requestFav());

    _AXIOS({
      method: 'DELETE',
      url: 'user/interpretation-fav/',
      isAuth: true,
      data: qs.stringify({ id }),
    })
      .then(() => dispatch(deleteFavInter(id)))
      .catch(() => 'error');
  };
}

export const addFavInterpToStore = payload => dispatch => {
  dispatch({
    type: ADD_FAV_INTERP,
    payload,
  });
};

const receiveInterSuccess = payload => ({
  type: RECEIVE_FAVORITES_INTER_SUCCESS,
  payload,
});

const deleteFavInter = payload => ({
  type: DELETE_FAVORITE_INTER,
  payload,
});

// Notes Actions

export function getNotes() {
  return dispatch => {
    dispatch(requestFav());

    _AXIOS({ url: 'user/note/?per-page=3000', isAuth: true })
      .then(({ note }) => dispatch(receiveNotes(note)))
      .catch(() => dispatch(receiveFavFailure()));
  };
}

export function createNote(body) {
  return dispatch => {
    dispatch(requestFav());

    return _AXIOS({
      method: 'POST',
      url: 'user/note/',
      isAuth: true,
      data: qs.stringify(body),
    })
      .then(() => dispatch(createNoteSuccess({ short: body.verse, text: body.text })))
      .catch(error => {
        const errorMessage = { error: R.pathOr(null, ['data', 'error', 'message'], error) };
        if (!errorMessage.error) {
          errorMessage.error = 'Внутренняя ошибка сервера';
        }
        dispatch(receiveFavFailure());
        return errorMessage;
      });
  };
}

export function updateNotes(body) {
  return dispatch => {
    dispatch(requestFav());

    return _AXIOS({
      method: 'PUT',
      url: 'user/note/',
      isAuth: true,
      data: qs.stringify(body),
    })
      .then(() => dispatch(updateNoteSuccess(body)))
      .catch(error => {
        const errorMessage = { error: R.pathOr(null, ['data', 'error', 'message'], error) };
        if (!errorMessage.error) {
          errorMessage.error = 'Внутренняя ошибка сервера';
        }
        dispatch(receiveFavFailure());
        return errorMessage;
      });
  };
}

export function deleteNote(id) {
  return dispatch => {
    dispatch(requestFav());

    _AXIOS({
      method: 'DELETE',
      url: 'user/note/',
      isAuth: true,
      data: qs.stringify({ note_id: id }),
    })
      .then(() => dispatch(removeNote(id)))
      .catch(() => 'error');
  };
}

const receiveNotes = payload => ({
  type: GET_NOTES,
  payload,
});

const createNoteSuccess = payload => ({
  type: CREATE_NOTE,
  payload,
});

const updateNoteSuccess = payload => ({
  type: UPDATE_NOTES,
  payload,
});

const removeNote = payload => ({
  type: DELETE_NOTE,
  payload,
});

// Common Actions

const requestFav = () => ({
  type: REQUEST_FAVORITES,
});

const receiveFavFailure = () => ({
  type: RECEIVE_FAVORITES_FAILURE,
});

export function setSessionTag(tag) {
  return dispatch => {
    dispatch(sessTag(tag));
  };
}

const sessTag = tag => ({
  type: SET_SESSION_TAG,
  payload: tag,
});
