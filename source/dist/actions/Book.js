import { _AXIOS } from '../ApiConfig';

export const BOOK_FETCH = 'bible/BOOK_FETCH';
export const BOOK_FETCH_SUCCESS = 'bible/BOOK_FETCH_SUCCESS';
export const BOOK_FETCH_FAILURE = 'bible/BOOK_FETCH_FAILURE';
export const BOOKINFO_FETCH = 'bible/BOOKINFO_FETCH';
export const BOOKINFO_FETCH_SUCCESS = 'bible/BOOKINFO_FETCH_SUCCESS';
export const BOOKINFO_FETCH_FAILURE = 'bible/BOOKINFO_FETCH_FAILURE';

export const getBookFulfilled = data => ({
  type: BOOK_FETCH_SUCCESS,
  payload: data,
});

export const getBookPending = () => ({
  type: BOOK_FETCH,
});

export const getBookRejected = error => ({
  type: BOOK_FETCH_FAILURE,
  error,
});

export const getBookInfoFulfilled = data => ({
  type: BOOKINFO_FETCH_SUCCESS,
  payload: data,
});

export const getBookInfoPending = () => ({
  type: BOOKINFO_FETCH,
});

export const getBookInfoRejected = error => ({
  type: BOOKINFO_FETCH_FAILURE,
  error,
});

// Получение Конкретной книги
const getBook = book_code => dispatch => {
  dispatch(getBookPending());

  return _AXIOS({ url: `/book/info/${book_code}` })
    .then(({ book = {} }) => dispatch(getBookFulfilled(book)))
    .catch(error => dispatch(getBookRejected(error)));
};

// Получение сведений о книге
const getBookInfo = book_id => dispatch => {
  dispatch(getBookInfoPending());

  return _AXIOS({ url: `/book/description/${book_id}` })
    .then(({ descriptor = [] }) => dispatch(getBookInfoFulfilled(descriptor)))
    .catch(error => dispatch(getBookInfoRejected(error)));
};

export default {
  getBook,
  getBookInfo,
};
