import R from 'ramda';
import { AXIOS } from '../ApiConfig';

export const BOOKS_FETCH = 'testament/BOOKS_FETCH';
export const BOOKS_FETCH_SUCCESS = 'testament/BOOKS_FETCH_SUCCESS';
export const BOOKS_FETCH_FAILURE = 'testament/BOOKS_FETCH_FAILURE';

export const getBooksFulfilled = data => ({
  type: BOOKS_FETCH_SUCCESS,
  payload: data,
});

export const getBooksPending = () => ({
  type: BOOKS_FETCH,
});

export const getBooksRejected = error => ({
  type: BOOKS_FETCH_FAILURE,
  error,
});

// Получение книг
export const getBooks = () => dispatch => {
  dispatch(getBooksPending());

  return AXIOS.get(`/book/list/?per-page=1000`).then(
    response => {
      if (response.data.status === 'ok') {
        dispatch(getBooksFulfilled(R.pathOr([], ['data'], response)));
      }
    },
    error => {
      dispatch(getBooksRejected(error));
    },
  );
};

export default {
  getBooks,
};
