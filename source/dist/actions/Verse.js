import axios from 'axios';
import { _AXIOS } from '../ApiConfig';
import { getPreaching } from './BiblePreaching';
import { setMetaContext } from './Chapter';

const CancelToken = axios.CancelToken;
let source = CancelToken.source();

export const VERSE_FETCH = 'VERSE_FETCH';
export const VERSE_FETCH_SUCCESS = 'VERSE_FETCH_SUCCESS';
export const VERSE_FETCH_FAILURE = 'VERSE_FETCH_FAILURE';

export const LINK_TO_VERSE_FETCH = 'LINK_TO_VERSE_FETCH';
export const LINK_TO_VERSE_SUCCESS = 'LINK_TO_VERSE_SUCCESS';
export const LINK_TO_VERSE_FAILURE = 'LINK_TO_VERSE_FAILURE';

export const VERSE_PARALLEL_FETCH = 'VERSE_PARALLEL_FETCH';
export const VERSE_PARALLEL_FETCH_SUCCESS = 'VERSE_PARALLEL_FETCH_SUCCESS';
export const VERSE_PARALLEL_FETCH_FAILURE = 'VERSE_PARALLEL_FETCH_FAILURE';

export const getLinkToVersePending = () => ({
  type: LINK_TO_VERSE_FETCH,
});
export const getLinkToVerseFulfilled = data => ({
  type: LINK_TO_VERSE_SUCCESS,
  payload: {
    data,
  },
});
export const getLinkToVerseRejected = error => ({
  type: LINK_TO_VERSE_FAILURE,
  error,
});
export const getVerseFulfilled = data => ({
  type: VERSE_FETCH_SUCCESS,
  payload: {
    data,
  },
});

export const getVersePending = () => ({
  type: VERSE_FETCH,
});

export const getVerseRejected = error => ({
  type: VERSE_FETCH_FAILURE,
  error,
});

// На стих ссылаются
const getLinkToVerse = (verse_id, cancelToken) => dispatch => {
  dispatch(getLinkToVersePending());

  const tokenCancel = cancelToken ? { cancelToken } : {};

  return _AXIOS({ url: `/interpretation/link/${verse_id}/`, ...tokenCancel })
    .then(data => dispatch(getLinkToVerseFulfilled(data)))
    .catch(error => dispatch(getLinkToVerseRejected(error)));
};

// Получение Главы книги
export const getVerse = (params, pathname) => (dispatch, getStore) => {
  dispatch(getVersePending());

  let urlLength;

  if (pathname) {
    urlLength = pathname.split('/').length;
  }

  source?.cancel();
  source = CancelToken.source();

  const chapterNumber = params?.chapter_num?.replace('glava-', '');

  const url =
    typeof params === 'object'
      ? `verse/detail-by-number/${params.book_code}/${params.chapter_num}/${params.verse_id}`
      : `/verse/detail/${params}/`;

  return _AXIOS({ url, cancelToken: source?.token })
    .then(data => {
      const store = getStore();
      dispatch(getVerseFulfilled(data));
      dispatch(
        setMetaContext(
          // если мы находимся и в книге и в стихе и в толкователе
          urlLength === 7
            ? store?.metaData?.metaContext?.book
              ? {
                  book: store?.metaData?.metaContext?.book,
                  tolkovatel: store?.metaData?.metaContext?.tolkovatel,
                  verseMeta: data?.verse,
                }
              : {
                  verseMeta: data?.verse,
                }
            : store?.metaData?.metaContext?.book
            ? {
                book: store.metaData.metaContext.book,
                // chapterNumber: store.metaData.metaContext.book?.chapters ? '' : chapterNumber,
                chapterNumber,
                verseNumber: params.verse_id.replace('stih-', ''),
              }
            : {
                chapterNumber,
                verseNumber: params.verse_id.replace('stih-', ''),
              },
        ),
      );
      if (data.verse.hasOwnProperty('id')) {
        dispatch(getLinkToVerse(data.verse.id, source?.token));
        dispatch(getPreaching(`?verse_id=${data.verse.id}`, source?.token));
      }
    })
    .catch(error => dispatch(getVerseRejected(error)));
};

export const getVerseParallelPending = () => ({
  type: VERSE_PARALLEL_FETCH,
});

export const getVerseParallelFulfilled = (data, verse_id) => ({
  type: VERSE_PARALLEL_FETCH_SUCCESS,
  payload: {
    data,
    verse_id,
  },
});

export const getVerseParallelRejected = error => ({
  type: VERSE_PARALLEL_FETCH_FAILURE,
  error,
});

// Получение всех переводов параллельных стихов текущего стиха

export const getVerseParallel = verse_id => dispatch => {
  dispatch(getVerseParallelPending());

  return _AXIOS({ url: `/verse/parallel/${verse_id}/` })
    .then(data => dispatch(getVerseParallelFulfilled(data, verse_id)))
    .catch(error => dispatch(getVerseParallelRejected(error)));
};

export default {
  getVerse,
  getLinkToVerse,
  getVerseParallel,
};
