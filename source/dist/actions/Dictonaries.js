import { _AXIOS } from '../ApiConfig';
import { META_CONTEXT_DATA } from '../../constants';

export const DICTONARIES_FETCH = 'DICTONARIES_FETCH';
export const DICTONARIES_FETCH_SUCCESS = 'DICTONARIES_FETCH_SUCCESS';
export const DICTONARIES_FETCH_FAILURE = 'DICTONARIES_FETCH_FAILURE';
export const DICTONARIES_WORD_FETCH = 'DICTONARIES_WORD_FETCH';

export const DICTONARIES_WORD_FETCH_SUCCESS = 'DICTONARIES_WORD_FETCH_SUCCESS';
export const DICTONARIES_WORD_FETCH_FAILURE = 'DICTONARIES_WORD_FETCH_FAILURE';
export const DICTONARIES_WORD_FETCH_CLEAR = 'DICTONARIES_WORD_FETCH_CLEAR';
export const DICTONARIES_WORD_ERROR_CLEAR = 'DICTONARIES_WORD_ERROR_CLEAR';

export const MEANING_WORD_FETCH = 'MEANING_WORD_FETCH';
export const MEANING_WORD_FETCH_SUCCESS = 'MEANING_WORD_FETCH_SUCCESS';
export const MEANING_WORD_FETCH_FAILURE = 'MEANING_WORD_FETCH_FAILURE';

export const MEANING_WORD_CLEAR = 'MEANING_WORD_CLEAR';

export const getDictonariesFulfilled = data => ({
  type: DICTONARIES_FETCH_SUCCESS,
  payload: {
    data,
  },
});

export const getDictonariesPending = () => ({
  type: DICTONARIES_FETCH,
});

export const getDictonariesRejected = error => ({
  type: DICTONARIES_FETCH_FAILURE,
  error,
});

export const setMetaContext = payload => ({
  type: META_CONTEXT_DATA,
  payload,
});

// передаем в эту функцию путь специально, чтобы была возможность в мета контекст поставить
// именно конкретный объект со словарем, который выбрал Пользователь
export const getDictonaries = pathname => dispatch => {
  dispatch(getDictonariesPending());

  return _AXIOS({ url: 'dictionary/list/' })
    .then(data => {
      let dictionaryName;
      let context;

      if (pathname) {
        dictionaryName = pathname.split('/')[3];
        context = data?.dictionary.find(el => el.code === dictionaryName);
      }

      setTimeout(() => {
        dispatch(getDictonariesFulfilled(data));
        // при запросе на слово из словаря, данная функция запроса списка словарей
        // вызывается снова и перетирает нужный нам контекст для мет (со словами).
        // Но при этом идет более длинный url, поэтому проверяем в какой момент отрабатывает
        // эта функция и если во время запроса на слова - то контекст не меняем
        if (pathname && pathname?.split('/').length <= 5) {
          dispatch(setMetaContext({ numberDictionary: context }));
        }
      });
    })
    .catch(error => dispatch(getDictonariesRejected(error)));
};

export const getDictonariesWordFulfilled = data => ({
  type: DICTONARIES_WORD_FETCH_SUCCESS,
  payload: {
    data,
  },
});

export const getDictonariesWordPending = () => ({
  type: DICTONARIES_WORD_FETCH,
});

export const getDictonariesWordRejected = error => ({
  type: DICTONARIES_WORD_FETCH_FAILURE,
  error,
});

const getDictonariesWord = (dictonary_id, letter, params) => dispatch => {
  dispatch(getDictonariesWordPending());

  return _AXIOS({ url: `/dictionary/${dictonary_id}/${letter}/${params}` })
    .then(data => dispatch(getDictonariesWordFulfilled(data)))
    .catch(error => dispatch(getDictonariesWordRejected(error)));
};

const getDictonariesWordClear = () => ({
  type: DICTONARIES_WORD_FETCH_CLEAR,
});

const setDictonariesWordClearError = () => ({
  type: DICTONARIES_WORD_ERROR_CLEAR,
});

export const getMeanibgWordFulfilled = data => ({
  type: MEANING_WORD_FETCH_SUCCESS,
  payload: {
    data,
  },
});

export const getMeanibgWordPending = () => ({
  type: MEANING_WORD_FETCH,
});

export const getMeanibgWordRejected = error => ({
  type: MEANING_WORD_FETCH_FAILURE,
  error,
});

const getMeanibgWordClear = () => ({
  type: MEANING_WORD_CLEAR,
});

const getMeaningWord = id => async (dispatch, getStore) => {
  dispatch(getMeanibgWordPending());

  const store = getStore();
  const dictionaryName = store?.metaData?.metaContext?.numberDictionary;

  return _AXIOS({ url: `dictionary/detail/${id}/` })
    .then(data => {
      dispatch(getMeanibgWordFulfilled(data));
      dispatch(setMetaContext({ numberDictionary: dictionaryName, wordDetailForMeta: data?.word }));
    })
    .catch(error => dispatch(getMeanibgWordRejected(error)));
};

export default {
  getDictonaries,
  getDictonariesWord,
  getMeaningWord,
  getDictonariesWordClear,
  getMeanibgWordClear,
  setDictonariesWordClearError,
};
