import { _AXIOS } from '../ApiConfig';

export const INTERPRETATORS_FETCH = 'INTERPRETATORS_FETCH';
export const INTERPRETATORS_FETCH_SUCCESS = 'INTERPRETATORS_FETCH_SUCCESS';
export const INTERPRETATORS_FETCH_FAILURE = 'INTERPRETATORS_FETCH_FAILURE';

export const getInterpretators = (book_code, chapter, verse) => dispatch => {
  dispatch(getInterpretatorsPending());

  return _AXIOS({
    url: `/ekzeget/list-for-chapter/${book_code}/${chapter}/${verse || ''}?per-page=1000`,
    isAuth: true,
  })
    .then(data => {
      dispatch(getInterpretatorsFulfilled(data));
    })
    .catch(error => {
      dispatch(getInterpretatorsRejected(error));
    });
};

export const getInterpretatorsFulfilled = data => ({
  type: INTERPRETATORS_FETCH_SUCCESS,
  payload: {
    data,
  },
});

export const getInterpretatorsPending = () => ({
  type: INTERPRETATORS_FETCH,
});

export const getInterpretatorsRejected = error => ({
  type: INTERPRETATORS_FETCH_FAILURE,
  error,
});

export default {
  getInterpretators,
};
