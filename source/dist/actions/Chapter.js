import axios from 'axios';
import { AXIOS } from '../ApiConfig';
import { META_CONTEXT_DATA } from '../../constants';

const CancelToken = axios.CancelToken;
const source = CancelToken.source();

export const CHAPTER_FETCH = 'CHAPTER_FETCH';
export const CHAPTER_FETCH_SUCCESS = 'CHAPTER_FETCH_SUCCESS';
export const CHAPTER_FETCH_FAILURE = 'CHAPTER_FETCH_FAILURE';
export const CHAPTER_CLEAR = 'CHAPTER_CLEAR';

export const getChapterFulfilled = data => ({
  type: CHAPTER_FETCH_SUCCESS,
  payload: {
    data,
  },
});

export const getChapterPending = () => ({
  type: CHAPTER_FETCH,
});

export const getChapterRejected = error => ({
  type: CHAPTER_FETCH_FAILURE,
  error,
});

export const getChapterClear = () => ({
  type: CHAPTER_CLEAR,
});

export const setMetaContext = payload => ({
  type: META_CONTEXT_DATA,
  payload,
});

// Получение Главы книги
export const getChapter = (book_code, chapter_id, pathname) => (dispatch, getStore) => {
  dispatch(getChapterPending());

  let urlLength = [];
  if (pathname) {
    urlLength = pathname.split('/');
  }

  return AXIOS.get(`/book/info/${book_code}/${chapter_id}/`, { cancelToken: source?.token }).then(
    response => {
      dispatch(getChapterFulfilled(response.data));
      const store = getStore();
      // если pathname === 5, значит мы находимся в разделе Книга и нам не нужен доп.уонтекст,
      // иначе мы находимся или в Стихе или в Толкованиях и тогда сохраняем доп.контекст
      dispatch(
        setMetaContext(
          urlLength?.length === 5
            ? response?.data
            : {
                book: response?.data?.book,
                tolkovatel: store?.metaData?.metaContext?.tolkovatel ?? {},
                verseNumber: store?.metaData?.metaContext?.verseNumber ?? '',
                verseMeta:
                  store?.metaData?.metaContext?.verseMeta && urlLength?.length === 7
                    ? store?.metaData?.metaContext?.verseMeta
                    : {},
              },
        ),
      );
    },
    error => {
      if (error.message) {
        dispatch(getChapterRejected(error));
      }
    },
  );
};

export default {
  getChapter,
  getChapterClear,
};
