import { _AXIOS } from '../ApiConfig';

export const EXEGETES_FETCH = 'EXEGETES_FETCH';
export const EXEGETES_FETCH_SUCCESS = 'EXEGETES_FETCH_SUCCESS';
export const EXEGETES_FETCH_FAILURE = 'EXEGETES_FETCH_FAILURE';

const getExegetes = (request = '') => dispatch => {
  dispatch(getExegetesPending());

  return _AXIOS({ url: `/ekzeget/?per-page=1000${request}` })
    .then(data => dispatch(getExegetesFulfilled(data)))
    .catch(error => dispatch(getExegetesRejected(error)));
};

export const getExegetesFulfilled = data => ({
  type: EXEGETES_FETCH_SUCCESS,
  payload: {
    data,
  },
});

export const getExegetesPending = () => ({
  type: EXEGETES_FETCH,
});

export const getExegetesRejected = error => ({
  type: EXEGETES_FETCH_FAILURE,
  error,
});

export default {
  getExegetes,
};
