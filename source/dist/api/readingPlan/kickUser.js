import { LS } from '~dist/browserUtils';
import { AXIOS } from '../../ApiConfig';

export const kickUser = async id => {
  try {
    const data = await AXIOS.delete(`/reading/group/users/${id}`, {
      headers: {
        'X-Authentication-Token': LS.get('token'),
      },
    });
    return data;
  } catch (e) {
    console.error(e);
  }
};
