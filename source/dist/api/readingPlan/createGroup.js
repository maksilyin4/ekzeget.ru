import { AXIOS } from '../../../dist/ApiConfig';
import { LS } from '../../../dist/browserUtils';

export const deleteGroup = async () => {
  try {
    await AXIOS.delete('/reading/group/', {
      headers: {
        'X-Authentication-Token': LS.get('token'),
      },
    });
  } catch (e) {
    console.error(e);
  }
};

export const createGroup = async (groupInfo, isEdit) => {
  const headers = {
    'X-Authentication-Token': LS.get('token'),
  };
  const { plan_id, ...editInfo } = groupInfo;
  try {
    const { data } = isEdit
      ? await AXIOS.put('/reading/group/', editInfo, { headers })
      : await AXIOS.post('/reading/group/', groupInfo, { headers });
    return data;
  } catch (e) {
    console.error(e);
  }
};

export const editGroup = async groupInfo => {
  try {
    const { data } = await AXIOS.put('/reading/group/', groupInfo, {
      headers: {
        'X-Authentication-Token': LS.get('token'),
      },
    });
    return data;
  } catch (e) {
    console.error(e);
  }
};

export const joinGroup = async id => {
  try {
    const { data } = await AXIOS.get(`/reading/group/${id}`, {
      headers: {
        'X-Authentication-Token': LS.get('token'),
      },
    });
    return data;
  } catch (e) {
    console.error(e);
  }
};
