import { AXIOS } from '../../../dist/ApiConfig';
import { LS } from '../../../dist/browserUtils';

export const joinGroup = async groupId => {
  try {
    const { data } = await AXIOS.get(
      `/reading/group/${groupId}`,
      {},
      {
        headers: {
          'X-Authentication-Token': LS.get('token'),
        },
      },
    );
    return data;
  } catch (e) {
    console.error(e);
  }
};
