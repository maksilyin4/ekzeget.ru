import { AXIOS } from '../../../dist/ApiConfig';
import { LS } from '../../../dist/browserUtils';

export const getGroupAsUser = async () => {
  try {
    const { data } = await AXIOS.get(`/reading/group/user/`, {
      headers: {
        'X-Authentication-Token': LS.get('token'),
      },
    });
    return data;
  } catch (e) {
    console.error(e);
  }
};

export const getGroupAsMentor = async () => {
  try {
    const { data } = await AXIOS.get(`/reading/group/mentor/`, {
      headers: {
        'X-Authentication-Token': LS.get('token'),
      },
    });
    return data;
  } catch (e) {
    console.error(e);
  }
};

export const getGroupPreview = async groupId => {
  try {
    const { data } = await AXIOS.get(`/reading/group/${groupId}/preview/`, {
      headers: {
        'X-Authentication-Token': LS.get('token'),
      },
    });
    return data;
  } catch (e) {
    console.error(e);
  }
};

export const getGroupInfo = async groupId => {
  try {
    const { data } = await AXIOS.get(`/reading/group/${groupId}/info/`, {
      headers: {
        'X-Authentication-Token': LS.get('token'),
      },
    });
    return data;
  } catch (e) {
    console.error(e);
  }
};

export const getUserPlanStats = async planId => {
  try {
    const { data } = await AXIOS.get(`/user/plan/statistics/${planId}/`, {
      headers: {
        'X-Authentication-Token': LS.get('token'),
      },
    });
    return data;
  } catch (e) {
    console.error(e);
  }
};
