import R from 'ramda';

import { _AXIOS } from './ApiConfig';
import { LS, Popup } from './browserUtils';

export const DEFAULT_COUNT_QUESTIONS = 20;

export const DEFAULT_TEXT_QUIZ = 'Библейская викторина Экзегет';

export const DEFAULT_URL_TO_ALL_QUESTIONS = '/bibleyskaya-viktorina/voprosi-1/';

export const DEFAULT_QUIZ_URL = 'vopros-1';

export function declOfNum(number, titles) {
  if (number % 1 !== 0) {
    // eslint-disable-next-line radix
    number = parseInt(number.toString().split('.')[2]);
  }
  const cases = [2, 0, 1, 1, 1, 2];
  const key = number % 100 > 4 && number % 100 < 20 ? 2 : cases[number % 10 < 5 ? number % 10 : 5];
  return titles[key];
}
export function formatMoney(number, n, x, s, c) {
  const re = `\\d(?=(\\d{${x || 3}})+${n > 0 ? '\\D' : '$'})`;

  // eslint-disable-next-line no-bitwise
  const num = number.toFixed(Math.max(0, ~~n));

  return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), `$&${s || ' '}`);
}
export function formatMoneyP(number) {
  return formatMoney(number, 2, 3, ' ', '.');
}

export function declOfNumStr(number, titles) {
  return `${number} ${declOfNum(number, titles)}`;
}
export function declOfNumMoneyStr(number, titles) {
  return `${formatMoneyP(number)} ${declOfNum(number, titles)}`;
}

export function parseGetParams(params) {
  const strSearch = params.substr(1);

  const strPattern = /([^=]+)=([^&]+)&?/gi;

  let arrMatch = strPattern.exec(strSearch);

  const objRes = {};
  while (arrMatch != null) {
    // eslint-disable-next-line prefer-destructuring
    objRes[arrMatch[1]] = arrMatch[2];
    arrMatch = strPattern.exec(strSearch);
  }
  return objRes;
}

export function parseLID(stroke) {
  const regexp = /(\d\s)?[а-яА-Я]{2,5}\.\s\d{1,2}\:\d{1,2}[^;]*/gim;
  return stroke.match(regexp);
}

export function parseVerseSelect(stroke) {
  const regexp = /\d{1,2}[\-|\–|\—]\d{1,2}|\d{1,2}[\-|\–|\—]{0}/gim;

  const str = decodeURIComponent(stroke);
  const verses = [];

  let verse = {};
  str.match(regexp).map(item => {
    if (item.split(/[\-|\–|\—]/gim).length === 1) {
      verse = {
        start: Number(item.split(/[\-|\–|\—]/gim)[0]),
        end: Number(item.split(/[\-|\–|\—]/gim)[0]),
      };
    } else {
      verse = {
        start: Number(item.split(/[\-|\–|\—]/gim)[0]),
        end: Number(item.split(/[\-|\–|\—]/gim)[1]),
      };
    }

    return verses.push(verse);
  });
  return verses;
}

export const verseTranslate = [
  {
    name: 'Синодальный перевод',
    value: 'st_text',
  },
  {
    name: 'Церковно-славянский',
    value: 'csya_old',
  },
  {
    name: 'Церковно-славянский (транслит)',
    value: 'csya',
  },
  // {
  //     name: 'Радостная весть 2004 г.',
  //     value: 'new_text'
  // },
  {
    name: 'Аверинцев С.С.',
    value: 'averincev',
  },
  {
    name: 'Кассиан (Безобразов)',
    value: 'kassian',
  },
  {
    name: 'Український І. Огієнка',
    value: 'ukr',
  },
  {
    name: 'English (NKJV)',
    value: 'nkjv',
  },
  {
    name: 'Latina Vulgata',
    value: 'latin',
  },
  {
    name: 'Ελληνικη',
    value: 'grek',
  },
  {
    name: 'Подстрочный',
    value: 'podstr',
  },
];

export function validateEmail(email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

export const mediaQuery = {
  phone: 767,
  tablet: 1024,
  tabletWide: 1200,
};

export function setAnimatedScrollTo(element, additionalOffset = 0) {
  if (typeof document === 'undefined') return;

  setTimeout(() => {
    const w = window.pageYOffset;

    const t = element.getBoundingClientRect().top;

    let start = null;
    requestAnimationFrame(step);
    function step(time) {
      if (start === null) start = time;
      const progress = time - start;

      const r = t < 0 ? Math.max(w - progress, w + t) : Math.min(w + progress, w + t);
      window.scrollTo(0, r + additionalOffset);
      if (r !== w + t) {
        requestAnimationFrame(step);
      }
    }
  }, 300);
}

export const sendSocialActions = ({ ga, ym }) => {
  if (typeof gtag === 'function') gtag(...ga);
  if (typeof yaCounter23728522 !== 'undefined' && typeof yaCounter23728522.reachGoal === 'function')
    yaCounter23728522.reachGoal(ym);
};

/* eslint-disable */
export const setCookie = (name, value, props) => {
  props = props || {};

  let exp = props.expires;

  if (typeof exp === 'number' && exp) {
    const d = new Date();
    d.setTime(d.getTime() + exp * 1000);

    exp = props.expires = d;
  }

  if (exp && exp.toUTCString) {
    props.expires = exp.toUTCString();
  }

  value = encodeURIComponent(value);

  const updatedCookie = Object.keys(props).reduce((prev, propName) => {
    prev += `; ${propName}`;
    const propValue = props[propName];
    if (propValue !== true) {
      prev += `=${propValue}`;
    }
    return prev;
  }, `${name}=${value}`);

  document.cookie = updatedCookie;
};

export const googleAPIKey = 'AIzaSyBOXPmTBjeshiddi6UmNtOOZoK76ANAhkE';

export const googleGEOAPIKey = 'AIzaSyAV-3DjHI9JNINovrrgszR8kSJYl-vqL0A';

export const alphSort = (item, typePosition) => {
  item.sort((a, b) => {
    const aLower = a.title.toLowerCase();
    const bLower = b.title.toLowerCase();

    if (aLower < bLower) {
      return -1;
    }

    if (aLower > bLower) {
      return 1;
    }

    return 0;
  });

  if (typePosition !== undefined && typePosition === 'lower') {
    item.reverse();
  }

  return item;
};

export const declesion = count => {
  const result = 'нов';
  let numberEndingByOne;
  let numberEndingByEleven;
  const temp = String(count).split('');
  if (temp.slice(-1)[0] === '1') {
    numberEndingByOne = temp.slice().join('');
  } else if (temp.slice(-2).join('') === '11') {
    numberEndingByEleven = temp.slice().join('');
  }
  switch (count) {
    case +numberEndingByOne: {
      return `${result}ое видео`;
    }
    case +numberEndingByEleven: {
      return `${result}ых видео`;
    }
    default: {
      return `${result}ых видео`;
    }
  }
};

export function getVerseBold(search, currentChapter) {
  if (!search.match(/verse/)) return '';

  return search.replace(`?verse=${currentChapter.replace('glava-', '')}:`, '').replace('/', '');
}

export const getMetaData = code => {
  return _AXIOS({ url: `/meta/get-by-code?code=${code}` })
    .then(({ meta }) => meta)
    .catch(() => 'Error');
};

export const isMetaData = (name, meta, exception) => {
  return meta && meta !== Error && meta?.name && meta[name].length > 0 ? meta[name] : exception;
};

export const setDataToStoreFromBible = (pathname, search) => {
  LS.set('isFirstLoad', 'few');
  LS.set('urlFromBible', `${pathname}${search}`);
};

export const scrollTo = (x = 0, y = 0, timeout = 500) => {
  if (process.env.BROWSER) {
    setTimeout(() => window.scrollTo(x, y), timeout);
  }
};

/**
 * Плавный скролл, недоступен для IE, Safari, для этих браузеров будет выполнен резкий скачек
 * @param timeout {number} - Задержка перед скроллом для UI на момент прелоадеров
 * @param params {Object.<{ top: number, behavior: string }>} - Параметры скролла
 * @returns {number}
 */
export const smoothScrollTo = (timeout = 300, params = { top: 0, behavior: 'smooth' }) => {
  if (process.env.BROWSER) {
    return setTimeout(() => window.scrollTo(params), timeout);
  }
};

export const statusesTypes = {
  OK: 'OK',
  NO: 'NO',
  NA: 'NA',
};

export const popupClose = (timeout = 1000) => setTimeout(Popup.close, timeout);

export const pushToQuestion = (queeze, push, params = {}) => {
  const { day, planId } = params;
  const question = R.pathOr([], ['questions', 0], queeze);
  const questionCode = R.pathOr(DEFAULT_QUIZ_URL, ['code'], question);
  const questionTitle = R.pathOr('Вопрос 1', ['title'], question);
  const questionId = R.pathOr('1', ['num'], question);
  if (day && planId) {
    push(`/bibleyskaya-viktorina/voprosi/day-${day}/${planId}/${questionCode}/`, {
      questionTitle,
      questionId,
    });
  } else {
    push(`/bibleyskaya-viktorina/voprosi/${questionCode}/`, { questionTitle, questionId });
  }
};

export const getQuestionParams = (questions, questionId, questionNum = 1, addStr) => {
  const questionTitle = R.pathOr(`Вопрос ${questionNum}`, [questionId, 'title'], questions);
  const questionNewId = R.pathOr(questionNum, [questionId, 'num'], questions);
  const questionCode = R.pathOr(DEFAULT_QUIZ_URL, [questionId, 'code'], questions);
  const pathname = `/bibleyskaya-viktorina/voprosi/${addStr ? addStr : ''}${questionCode}/`;

  return {
    pathname,
    state: {
      questionTitle,
      questionId: questionNewId,
    },
  };
};

export const parseTextPunctuation = text => {
  const regex = new RegExp(/&quot?;/, 'gi');
  return text.replace(regex, '"');
};

export const HOLY_FATHERS_ID = 1;
