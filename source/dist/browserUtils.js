import { STORAGE_CDN } from '../../ecosystem.config';

export const Popup = process.env.BROWSER ? require('react-popup').default : null;

export const LS = {
  get: token => process.env.BROWSER && localStorage.getItem(token),
  set: (token, value) => process.env.BROWSER && localStorage.setItem(token, value),
};

export const urlToAPI =
  process.env.NODE_ENV !== 'production'
    ? 'https://165104.selcdn.ru/ekzeget/dev'
    : (process.env.BROWSER && window.STORAGE_CDN) || STORAGE_CDN;
