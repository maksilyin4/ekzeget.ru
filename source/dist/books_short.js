export default function parseBookLink(short) {
  const regexp = /(?:(?:[123]|I{1,3})\s*)?(?:[А-Я][а-яА-Я]+).?\s*(?:1?[0-9]?[0-9]):\s*\d{1,3}(?:[,-–]\s*\d{1,3})*(?:;\s*(?:(?:[123]|I{1,3})\s*)?(?:[А-Я][а-яА-Я]+)?.?\s*(?:1?[0-9]?[0-9]):\s*\d{1,3}(?:[,-–]\s*\d{1,3})*)*/gu;
  const match = short.match(regexp);
  const links = [];

  // eslint-disable-next-line no-unused-expressions
  !!match &&
    match.map(item => {
      const book_code = books_short[item.match(/\d?\s?[а-яА-Я]{2,5}/gim)];
      const chapter = item.match(/[\d]+(?=:)/u);
      const verses = item
        .match(
          /[\d]+(:)[\d]+(-|–|—)[\d]+(:)[\d]+|([\d]{1,2}(?!:)(?!\d))((-|–|—)\d+)?((,\s)?[\d]+((-|–|—)\d+)?((,\s)?)?[\d]+((-|–|—)\d+)?(=(,\s)?)?)?/gu,
        )[0]
        .replace(/,\s/, '_');

      return links.push({
        title: item,
        bookCode: book_code,
        chapter,
        verses,
      });
    });

  return links;
}

const books_short = {
  Быт: 'bytie',
  Исх: 'ishod',
  Лев: 'levit',
  Чис: 'cisla',
  Втор: 'vtorozakonie',
  Нав: 'kniga-iisusa-navina',
  Суд: 'kniga-sudej-izrailevyh',
  Руф: 'kniga-ruf',
  '1 Цар': '1a-kniga-carstv',
  '2 Цар': '2a-kniga-carstv',
  '3 Цар': '3a-kniga-carstv',
  '4 Цар': '4a-kniga-carstv',
  '1 Пар': '1a-kniga-paralipomenon',
  '2 Пар': '2a-kniga-paralipomenon',
  '1 Езд': '1a-kniga-ezdry',
  '2 Езд': '3a-kniga-ezdry',
  '3 Езд': '2a-kniga-ezdry',
  Неем: 'kniga-neemii',
  Тов: 'kniga-tovita',
  Иудиф: 'kniga-iudif',
  Есф: 'kniga-esfiri',
  Иов: 'kniga-iova',
  Пс: 'psaltir',
  Притч: 'pritci-solomona',
  Еккл: 'kniga-ekkleciasta',
  Песн: 'pesn-pesnej-solomona',
  Прем: 'kniga-premudrosti-solomona',
  Сир: 'kniga-premudrosti-iisusa-syna-sirahova',
  Ис: 'kniga-proroka-isaii',
  Иер: 'kniga-proroka-ieremii',
  Плач: 'plac-ieremii',
  Посл: 'poslanie-ieremii',
  Вар: 'kniga-proroka-varuha',
  Иез: 'kniga-proroka-iezekiila',
  Дан: 'kniga-proroka-daniila',
  Ос: 'kniga-proroka-osii',
  Иоил: 'kniga-proroka-ioila',
  Ам: 'kniga-proroka-amosa',
  Авд: 'kniga-proroka-avdia',
  Ион: 'kniga-proroka-iony',
  Мих: 'kniga-proroka-mihea',
  Наум: 'kniga-proroka-nauma',
  Авв: 'kniga-proroka-avvakuma',
  Соф: 'kniga-proroka-sofonii',
  Агг: 'kniga-proroka-aggea',
  Зах: 'kniga-proroka-zaharii',
  Мал: 'kniga-proroka-malahii',
  '1 Мак': '1a-kniga-makkavejskaa',
  '2 Мак': '2a-kniga-makkavejskaa',
  '3 Мак': '3a-kniga-makkavejskaa',
  Мф: 'evangelie-ot-matfea',
  Мк: 'evangelie-ot-marka',
  Лк: 'evangelie-ot-luki',
  Ин: 'evangelie-ot-ioanna',
  Деян: 'deania-apostolov',
  Иак: 'poslanie-ap-iakova',
  '1 Пет': '1oe-poslanie-ap-petra',
  '2 Пет': '2oe-poslanie-ap-petra',
  '1 Ин': '1oe-poslanie-ap-ioanna',
  '2 Ин': '2oe-poslanie-ap-ioanna',
  '3 Ин': '3oe-poslanie-ap-ioanna',
  Иуд: 'poslanie-ap-iudy',
  Рим: 'k-rimlanam-poslanie-ap-pavla',
  '1 Кор': '1oe-poslanie-k-korinfanam-ap-pavla',
  '2 Кор': '2oe-poslanie-k-korinfanam-ap-pavla',
  Гал: 'k-galatam-poslanie-ap-pavla',
  Еф: 'k-efesanam-poslanie-ap-pavla',
  Флп: 'k-filippijcam-poslanie-ap-pavla',
  Кол: 'k-kolossanam-poslanie-ap-pavla',
  '1 Фес': '1oe-poslanie-k-fessalonikijcam-ap-pavla',
  '2 Фес': '2oe-poslanie-k-fessalonikijcam-ap-pavla',
  '1 Тим': '1oe-poslanie-k-timofeu-ap-pavla',
  '2 Тим': '2oe-poslanie-k-timofeu-ap-pavla',
  Тит: 'k-titu-poslanie-ap-pavla',
  Флм: 'k-filimonu-poslanie-ap-pavla',
  Евр: 'k-evream-poslanie-ap-pavla',
  Откр: 'otkrovenie-ap-ioanna-bogoslova',
};
