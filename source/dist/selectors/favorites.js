import { createSelector } from 'reselect';
import R from 'ramda';

import { interpretationsSelector } from './myInterpretations';
import { userLoggedIn } from './User';

export const bookCode = store => R.pathOr('', ['book', 'bookData', 'code'], store);
export const favorites = store => store.favorites;

export const getIsFetching = store => store.favorites.isFetching;
export const getSessionTag = store => store.favorites.sessionTag;

const favVerseIds = createSelector(favorites, fav => fav.favoritesVerseIds);

export const favVerseByIds = createSelector(favorites, fav => fav.favoritesVerseByIds);

export const getVerseList = createSelector(favorites, fav => fav.verseList);
export const getInterList = createSelector(favorites, fav => fav.interList);

// get array with all denormalize data
// TODO: rename getFavoriteVerseList
export const getFavoriteList = createSelector(
  favVerseIds,
  favVerseByIds,
  getVerseList,
  (favIds, favByIds, verse) =>
    favIds.map(i => ({
      ...favByIds[i],
      verse: R.pathOr({}, ['verse'], verse[favByIds[i].verse]),
    })),
);

export const getArrayOfIds = createSelector(getFavoriteList, fav => {
  if (!R.isEmpty(fav)) {
    fav.map(i => i.verse.id);
  }
});

export const getFavTagsWithId = createSelector(getFavoriteList, favList =>
  favList.map(i => ({
    number: i.verse.number,
    tags: i.tags.split(','),
  })),
);

export const favoriteInterListSelector = createSelector(favorites, fav => fav.favoritesInter);

export const getAlltags = createSelector(favorites, fav => fav.allTags.sort());
export const getChosenTag = createSelector(favorites, fav => fav.chosenTag);

export const getNotes = createSelector(favorites, fav => fav.notes);

export const getSessionTagArray = createSelector(favorites, fav => [
  ...fav.allTags.filter(i => i === getSessionTag),
]);

export const favoritesInterpretationsSelector = createSelector(
  favoriteInterListSelector,
  interpretationsSelector,
  userLoggedIn,
  /**
   * @param favoriteInterList {Array.<{ id: number,
   * ekzeget: ekzegetForInterpretationSkeleton,
   * interpretation: interpretationSkeleton, verse: Object.<any>
   * }>} - Актуальный список избранных толкователей конкретного пользователя;
   * @param userInterpretations {Array.<interpretationSkeleton>} - Список толкователей текущей главы/стиха пользователя;
   * @param isUserLogged {boolean} - Проверка, авторизованный ли у нас юзер.
   * Данная сортировка нужна именно для авторизованных пользователей (у которых звездочка блэт есть)
   * Далее нам необходимо найти ID у обоих списков, чтобы определить, есть ли у текущего
   * толкователя избранный ID, их может быть > 1, так что составляем объект по ID толкователей
   * @returns {{} | Object.<{interpretationId: ekzegetForInterpretationSkeleton}>}
   */
  (favoriteInterList, userInterpretations, isUserLogged) => {
    const favoritesInterpretations = {};

    if (favoriteInterList.length > 0 && isUserLogged) {
      favoriteInterList.forEach(favorite => {
        const favoriteInterpretationId = favorite.interpretation.id;

        const isCurrentFavoriteInterpretation = userInterpretations.some(
          ({ id }) => id === favoriteInterpretationId,
        );

        if (isCurrentFavoriteInterpretation) {
          favoritesInterpretations[favoriteInterpretationId] = favorite.ekzeget;
        }
      });
    }

    return favoritesInterpretations;
  },
);
