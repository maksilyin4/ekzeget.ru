import { createSelector } from 'reselect';

export const getLIDIsLoading = state => state.lid.isLoading;
export const getLID = createSelector(
  state => state.lid.data,
  i => i,
);
