import { createSelector } from 'reselect';

const bookReducer = state => state.book;

export const getBookIsLoading = state => state.book.isLoadingBookData;
export const getBookInfoIsLoading = state => state.book.isLoadingBookInfo;

export const getBook = createSelector(
  bookReducer,
  i => i.bookData,
);
export const getBookInfo = createSelector(
  bookReducer,
  i => i.bookInfo,
);

export const getBookForPreaching = createSelector(
  bookReducer,
  i => i.bookData,
);
