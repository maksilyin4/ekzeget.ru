import R from 'ramda';
import { createSelector } from 'reselect';

export const booksReducer = state => state.books;

export const getTestamentIsLoading = state => state.books.testamentIsLoading;

export const getNewTestament = createSelector(
  booksReducer,
  i => R.pathOr([], ['testament', 'testament-1', 'books'], i),
);
export const getOldTestament = createSelector(
  booksReducer,
  i => R.pathOr([], ['testament', 'testament-2', 'books'], i),
);

export const getNewOldTestament = createSelector(
  booksReducer,
  i => {
    const newTestament = R.pathOr([], ['testament', 'testament-2', 'books'], i);
    const oldTestament = R.pathOr([], ['testament', 'testament-1', 'books'], i);
    return { newTestament, oldTestament };
  },
);

export const getNewTestamentSelect = createSelector(
  getNewTestament,
  newTestaments => newTestaments.map(i => ({ value: i.id, label: i.menu })),
);
export const getOldTestamentSelect = createSelector(
  getOldTestament,
  OldTestaments => OldTestaments.map(i => ({ value: i.id, label: i.menu })),
);
