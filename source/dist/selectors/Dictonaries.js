import R from 'ramda';
import { createSelector } from 'reselect';

export const getDictonariesIsLoading = state => state.dictonaries.isDictonariesLoading;
export const getDictonaries = createSelector(
  state => state.dictonaries.dictonaries,
  i => i,
);

export const getWordsIsLoading = state => state.dictonaries.isDictonariesWordLoading;
export const setIsDictonariesWordFailed = state => state.dictonaries.isDictonariesWordFailed;
export const getDictonariesWord = createSelector(
  state => state.dictonaries.words,
  i => i,
);

export const getMeaningIsLoading = state => state.dictonaries.isMeaningWordLoading;
export const getMeaningWord = createSelector(
  state => state.dictonaries.word || {},
  i => i,
);

export const getDictonariesSelect = createSelector(getDictonaries, dicts => {
  const dict = R.pathOr([], ['dictionary'], dicts);
  return dict.map(i => {
    return { value: i.id, label: i.title, name: i.code, letter: i.letters[0] };
  });
});
