import R from 'ramda';
import { createSelector } from 'reselect';

const motivators = store => store.motivator;

export const getMotivatorsIsFetching = store => store.motivator.isFetching;

export const getMotivatorsFromStore = createSelector(
  motivators,
  m => R.pathOr([], ['motivators'], m),
);
