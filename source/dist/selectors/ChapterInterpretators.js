import R from 'ramda';
import { createSelector } from 'reselect';

const interp = store => store.interpretators;

export const getIsLoading = state => state.interpretators.isLoading;

export const getInterpretators = createSelector(
  interp,
  inter => R.pathOr([], ['data', 'ekzeget'], inter),
);

export const getEkzegetTypes = createSelector(
  getInterpretators,
  inter => {
    return R.uniqBy(R.prop('id'), [
      { id: 0, title: 'Все экзегеты' },
      ...inter.map(({ ekzeget_type: type }) => {
        if (type) {
          return { id: type.id, title: type.title };
        }
      }),
    ]);
  },
);
