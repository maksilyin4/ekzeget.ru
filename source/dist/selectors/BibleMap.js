import R from 'ramda';
import { createSelector } from 'reselect';

const pointsByBook = state => state.bibleMap.markersData;

export const markers = state => state.bibleMap;

export const searchMarkers = state => state.bibleMap.searchMarkers;

export const getPointsByBookSelector = createSelector(
  pointsByBook,
  pb => R.pathOr([], ['points'], pb),
);
