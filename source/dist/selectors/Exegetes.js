import R from 'ramda';
import { createSelector } from 'reselect';

export const getExegetesIsLoading = state => state.exegetes.isLoading;
export const setExegetIsError = state => state.exegetes.isFailed;

export const getExegetes = createSelector(
  state => state.exegetes.data,
  i => i,
);

export const getEkzegetTypes = createSelector(getExegetes, ekzegets => {
  const allTypes = !R.isEmpty(ekzegets)
    ? R.uniqBy(R.path(['ekzeget_type', 'title']), ekzegets.ekzeget)
    : [];
  return [
    { value: '', label: 'Все экзегеты' },
    ...allTypes.map(i => ({ value: i.ekzeget_type.code, label: i.ekzeget_type.title })),
  ];
});
