import R from 'ramda';
import { createSelector } from 'reselect';

const bibleGroups = store => store.bibleGroupUpdates;
const bibleGroupsNotes = store => store.bibleGroupUpdates;

export const bibleGroupNoteInfo = store => store.bibleGroupUpdates.bibleGroupNoteInfo || {};
export const isFetching = store => store.bibleGroupUpdates.isFetchingGroupUpdates;
export const getAllBibleGroups = store => store.bibleGroupUpdates.bibleGroups;
export const bibleGroupNotes = store => store.bibleGroupUpdates.bibleGroupsNotes;

export const getBibleGroups = createSelector(
  bibleGroups,
  bibleGroupsNotes,
  (g, gn) => {
    const firstOfBibleGroups = R.pathOr([], ['bibleGroups'], g).slice(0, 2);
    const firstOfBibleGroupsNotes = R.pathOr([], ['bibleGroupsNotes'], gn).slice(0, 2);

    return [...firstOfBibleGroups, ...firstOfBibleGroupsNotes];
  },
);
