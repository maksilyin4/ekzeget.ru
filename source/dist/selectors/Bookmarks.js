export const bookmarks = state => state.bookmarks.bookmarks;
export const getIsLoading = state => state.bookmarks.getIsLoading;
export const addIsLoading = state => state.bookmarks.addIsLoading;
export const addStatus = state => state.bookmarks.addStatus;
export const deleteIsLoading = state => state.bookmarks.deleteIsLoading;
export const deleteStatus = state => state.bookmarks.deleteStatus;
export const bookmarkNeedUpd = state => state.bookmarks.needUpdate;
