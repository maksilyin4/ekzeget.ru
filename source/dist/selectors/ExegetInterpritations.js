import { createSelector } from 'reselect';

export const getExegetInterpritationsIsLoading = state => state.exegetInterpritations.isLoading;

export const getExegetInterpritations = createSelector(
  state => state.exegetInterpritations.data,
  i => i,
);
