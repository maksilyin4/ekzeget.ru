import { createSelector } from 'reselect';

const screenReducer = store => store.screen;

export const getIsDesktop = store => screenReducer(store).desktop;
export const getIsTablet = store => screenReducer(store).tablet;
export const getIsTabletMedium = store => screenReducer(store).tabletMedium;
export const getIsMobile = store => screenReducer(store).phone;

export const getIsAdaptive = createSelector(screenReducer, i => i.phone || i.tablet);

export const getIsTablets = createSelector(screenReducer, i => i.tabletMedium || i.tablet);

export const getIsTabletPhone = createSelector(screenReducer, i => i.tablet || i.phone);

export default createSelector(screenReducer, screen => screen);
