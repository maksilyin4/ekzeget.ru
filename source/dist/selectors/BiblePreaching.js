import R from 'ramda';
import { createSelector } from 'reselect';

const preachingReducer = state => state.preachingBible.data;

export const getPreachingIsLoading = state => state.preachingBible.isLoading;
export const getPreachingByBookIsLoading = state => state.preachingBible.isLoading;

export const getPreachingBySheet = createSelector(preachingReducer, i =>
  R.pathOr({}, ['pages'], i),
);

export const getPreaching = createSelector(preachingReducer, i => R.pathOr([], ['preaching'], i));

export const getPreachingForBible = createSelector(preachingReducer, preacherBible => {
  return R.pathOr([], ['preaching'], preacherBible);
});

export const getPreachingForVerse = createSelector(
  getPreachingBySheet,
  getPreaching,
  (gbs, gp) => ({
    gbs,
    gp,
  }),
);

export const getPreachingByBookSelector = createSelector(preachingReducer, i =>
  R.pathOr({}, ['data'], i),
);
