import R from 'ramda';
import { createSelector } from 'reselect';

export const getChapterInterpretationsIsLoading = state => state.chapterInterpretations.isLoading;

const chapter = state => state.chapter.data;

const verse = createSelector(chapter, c => R.pathOr([], ['book', 'chapters', 0, 'verses'], c));

export const getChapterInterpretations = state => state.chapterInterpretations.data;

const interpretations = createSelector(getChapterInterpretations, i =>
  R.pathOr([], ['interpretations'], i),
);

const interpretationsByVerseId = createSelector(interpretations, i =>
  i.reduce((sum, item) => {
    item.verse.forEach(({ id }) => {
      // eslint-disable-next-line no-param-reassign
      if (item.comment) sum[id] = { id: item.id, comment: item.comment };
    });
    return sum;
  }, {}),
);

const ekzeget = store => R.pathOr({}, ['exeget', 'data', 'ekzeget'], store);

// Стихи берутся из главы state.chapter, т.к.там они отсортированы.
export const getVerseWithInterpretations = createSelector(
  verse,
  interpretationsByVerseId,
  ekzeget,

  (_verses, _interpretationsByVerseId, _ekzeget) => {
    return {
      verses: _verses.reduce((sum, { id, text, number, verseTranslates }, i) => {
        const prevInter =
          i > 0 ? R.pathOr({}, [_verses[i-1].id], _interpretationsByVerseId) : {};
        const currInter = R.pathOr({}, [id], _interpretationsByVerseId);

        if (prevInter.id && prevInter.id === currInter.id) {
          sum[sum.length - 1].verses.push({ id, text, number });
        } else if (currInter.id) {
          sum.push({
            id,
            verses: [{ id, text, number, verseTranslates }],
            interpretation: currInter,
          });
        } else {
          sum.push({ id: id + i, verses: [{ id, text, number, verseTranslates }] });
        }

        return sum;
      }, []),
      ekzeget: R.pathOr('Толкований нет', ['name'], _ekzeget),
      ekzegetId: R.pathOr(0, ['id'], _ekzeget),
      ekzegetCode: R.pathOr(null, ['code'], _ekzeget),
    };
  },
);
