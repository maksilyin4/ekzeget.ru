import R from 'ramda';
import { createSelector } from 'reselect';

export const userDataIsLoading = state => state.user.userDataIsLoading;
export const userData = state => state.user.userData;
export const userDataInfo = state => state.user.userData.user;

export const userUpdateIsLoading = state => state.user.userDataIsLoading;

export const userRegIsLoading = state => state.user.userDataIsLoading;

export const userAuthIsLoading = state => state.user.userDataIsLoading;
export const userIdentityProviders = state => state.user.identityProviders;

export const userLoggedIn = state => state.user.userLoggedIn.isLoading;
export const userLoggedInUserId = state => state.user.userLoggedIn.userId;

export const userError = state => state.user.error;
export const getUserError = createSelector(userError, i => R.pathOr('', ['name'], i));

export const getUserInformationForSubscription = createSelector(userData, ud =>
  R.pathOr({}, ['user'], ud),
);
