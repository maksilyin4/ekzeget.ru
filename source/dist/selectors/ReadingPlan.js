import R from 'ramda';
import { createSelector } from 'reselect';

const readingPlanReducer = state => state.readingPlan;

export const readingPlansIsLoading = state => state.readingPlan.readingPlansIsLoading;
export const plans = createSelector(readingPlanReducer, i => i.plans);
export const activeTabUserRP = createSelector(readingPlanReducer, i => i.activeTab);
export const readingPlanDetailIsLoading = createSelector(
  readingPlanReducer,
  i => i.readingPlanDetailIsLoading,
);

export const planDetail = createSelector(readingPlanReducer, i => i.planDetail);
export const getTogglePlan = state => state.readingPlan.toggleOpenPlan;

export const userRPGetIsLoading = state => state.readingPlan.userRPIsLoading;

export const userPlans = createSelector(readingPlanReducer, i => i.userPlans);
export const archivedPlans = createSelector(readingPlanReducer, i => i.archivedPlans);
export const archivedPlansLoading = createSelector(readingPlanReducer, i => i.archivedPlansLoading);

export const getUserPlans = (store, planId) => {
  const userPlan = userPlans(store);
  return R.pathOr({}, [planId], userPlan);
};

export const getUserSelectPlans = createSelector(getUserPlans, selectPlans => {
  let value = 0;

  selectPlans.map(i => ({ value: value++, label: i.closed_days[value] }));
});

export const getUserPlanId = createSelector(userPlans, plans =>
  R.pathOr('', [0], Object.keys(plans)),
);
