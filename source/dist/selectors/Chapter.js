import R from 'ramda';
import { createSelector } from 'reselect';

const currentVerseFromState = state => state.verse;

export const getChapterIsLoading = state => state.chapter.isLoading;

export const getChapter = state => state.chapter?.data;

export const getErrorChapter = state => {
  return state.chapter.error;
};

export const chapterVerseLength = createSelector(getChapter, i =>
  R.pathOr(0, ['book', 'chapters', 0, 'verses', 'length'], i),
);

export const verseListFromChapter = createSelector(getChapter, i =>
  R.pathOr([], ['book', 'chapters', 0, 'verses'], i),
);

export const getBookInfo = createSelector(getChapter, i => ({
  title: R.pathOr('', ['book', 'title'], i),
  short_title: R.pathOr('', ['book', 'short_title'], i),
  code: R.pathOr('', ['book', 'code'], i),
}));

export const getBooktitle = createSelector(getChapter, i =>
  R.pathOr({}, ['book', 'chapters', 0], i),
);

export const getTestament = createSelector(getChapter, i =>
  R.pathOr(1, ['book', 'testament', 'id'], i),
);

export const getCurrentVerse = createSelector(currentVerseFromState, cv =>
  R.pathOr('', ['data', 'verse', 'id'], cv),
);

export const getVerseIDs = createSelector(
  verseListFromChapter,
  getCurrentVerse,
  (verseList, currentVerse) =>
    verseList.filter(
      verse =>
        verse.id !== currentVerse && {
          id: verse.id,
          number: verse.number,
        },
    ),
);
