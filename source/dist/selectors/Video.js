import R from 'ramda';
import { createSelector } from '../../../node_modules/reselect/lib/index';

export const videoPlaylistLoading = state => state.video.videoPlaylistLoading;
export const videoPlaylist = state => state.video.videoPlaylist;

export const videoPreviewFirst = state => R.pathOr([], ['videoPreviewFirst', 'video'], state.video);
export const videoPreviewSecond = state =>
  R.pathOr([], ['videoPreviewSecond', 'video'], state.video);

export const lastVideosFirst = state => state.video.lastVideosFirst;
export const lastVideosSecond = state => state.video.lastVideosSecond;

export const videoDetailLoading = state => state.video.videoDetailLoading;
export const videoDetail = state => state.video.videoDetail;

export const lastVideos = state => state.lastVideos;

export const getLastVideosData = createSelector(
  lastVideos,
  i => R.pathOr([], ['lastVideos'], i),
);

export const getLastVideosDataFiltered = createSelector(
  getLastVideosData,
  i => i,
);
