import R from 'ramda';
import { createSelector } from 'reselect';
import { alphSort } from '../utils';

export const getLectureGroupLoading = state => state.lecture.getLectureGroupLoading;
export const lectureGroup = state => state.lecture.lectureGroup;

export const getLectureGroupListLoading = state => state.lecture.getLectureGroupListLoading;
export const lectureGroupList = state => state.lecture.lectureGroupList;
export const setIsErrorLectureGroupList = state => state.lecture.getLectureGroupListFailed;

export const alphSortLectureList = createSelector(lectureGroupList, lectures => {
  const newArrLecture = R.pathOr([], ['lecture'], lectures);

  return alphSort(newArrLecture);
});

export const getLectureDetailLoading = state => state.lecture.getLectureDetailLoading;
export const lectureDetail = state => state.lecture.lectureDetail;
export const setIsErrorLectureDetail = state => state.lecture.getLectureDetailFailed;

export const lectureSelect = createSelector(lectureGroup, lectures => {
  const lecture = R.pathOr([], ['categories'], lectures);

  return [
    ...lecture.map(i => ({ value: i.code, label: i.title })),
    {
      value: 1,
      label: 'Вечная пасхалия',
    },
  ];
});

export const lectureGroupSelect = createSelector(lectureGroupList, lecture => {
  const lectures = R.pathOr([], ['lecture'], lecture);

  return [...lectures.map(i => ({ value: i.code, label: i.title }))];
});
