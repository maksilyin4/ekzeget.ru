import R from 'ramda';
import { createSelector } from 'reselect';

const myInterp = store => store.myInterpretations;
export const interpretationsSelector = store => store.interpretation.data;

const IDs = createSelector(myInterp, i => i.myIntIds);
export const byIDs = createSelector(myInterp, i => i.myIntById);

export const getMyInterpList = createSelector(IDs, byIDs, (IDs, byIDs) => IDs.map(i => byIDs[i]));

export const getIsFetch = store => store.myInterpretations.isFetching;

export const getGroupInterpretations = createSelector(interpretationsSelector, i =>
  R.pathOr([], ['interpretations', 0, 'verse'], i),
);
