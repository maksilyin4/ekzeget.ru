import R from 'ramda';
import { createSelector } from 'reselect';

export const getVerseIsLoading = state => state.verse.isLoading;
export const getVerse = state => state.verse.data;
export const getErrorVerse = state => state.verse.error;

export const getVerseIsLoadingParallel = state => state.verse.parallelLoading;

export const getVerseDataParallel = state => state.verse.parallelData;

export const linkToVerseLoading = state => state.verse.linkToVerseLoading;
export const linkToVerse = state => state.verse.linkToVerse;

const verses = createSelector(getVerse, i => R.pathOr([], ['verse'], i));
export const verseTranslates = createSelector(verses, verse => [
  ...R.pathOr([], ['translate'], verse)
    .map(item => ({
      id: item.id,
      code: R.pathOr('', ['code', 'code'], item),
      codeTitle: R.pathOr('', ['code', 'title'], item),
      number: R.pathOr(2, ['code', 'number'], item),
      text: item.text,
    }))
    .sort((a, b) => a.number - b.number),
]);
