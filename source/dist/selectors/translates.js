import R from 'ramda';
import { createSelector } from 'reselect';

const translateReducer = store => store.translates;

export const translateReducerData = createSelector(translateReducer, t => t.translates.data);
export const getTranslateIsLoading = createSelector(translateReducer, t => t.translates.isLoading);

export const getStTextTranslate = createSelector(translateReducerData, t =>
  t.filter(({ code }) => code === 'st_text' || null),
);

export const getGreekTranslate = createSelector(translateReducerData, t =>
  t.filter(({ code }) => code === 'grek' || null),
);

export const getChapterTranslates = state => state.chapter.data;

export const translatesChapter = createSelector(getChapterTranslates, i =>
  R.pathOr([], ['book', 'chapters', 0, 'verses', 0, 'verseTranslates'], i),
);

export const getTranslatesForSelect = createSelector(
  translateReducerData,
  translatesChapter,
  (t, ch) => {
    return t
      .map(i => ({ ...i, value: i.id, label: i.select }))
      .filter(i => {
        const filtered = ch.some(
          k => (Number(i.id) === Number(k.code.id) && k.text !== '') || i.code === 'st_text',
        );
        return filtered;
      })
      .sort((a, b) => a.number - b.number);
  },
);

export const getTranslatesForSelectBible = createSelector(getTranslatesForSelect, translates => {
  const count = translates.length / 2;
  const maxArr = 2;
  return [...translates].reduce(
    (acc, arr) => {
      if (acc[acc.length - 1].length > count && acc.length < maxArr) {
        acc.push([]);
      }
      acc[acc.length - 1].push(arr);
      return acc;
    },
    [[]],
  );
});

export const getTranslatesForSelectLk = createSelector(translateReducerData, t =>
  t.map(i => ({ ...i, value: i.id, label: i.select })).sort((a, b) => a.number - b.number),
);

export const getTranslatesSelectSearch = createSelector(translateReducerData, t =>
  t.map(i => ({ ...i, value: i.code, label: i.select })).sort((a, b) => a.number - b.number),
);

export const getTranslatesForSelectSearch = createSelector(
  getTranslatesSelectSearch,
  translates => {
    if (translates.length) {
      const count = translates.length / 2;
      const maxArr = 2;

      const [rus, other] = [...translates].reduce(
        (acc, arr) => {
          if (acc[acc.length - 1].length > count && acc.length < maxArr) {
            acc.push([]);
          }
          acc[acc.length - 1].push(arr);

          return acc;
        },
        [[]],
      );
      const rusAB = rus.sort((a, b) => a.select.localeCompare(b.select));
      const otherAB = other.sort((a, b) => a.select.localeCompare(b.select));
      return [rusAB, otherAB];
    }
    return translates;
  },
);

export const getCurrentTranslate = createSelector(translateReducer, reducer =>
  R.isEmpty(reducer.currentTranslate) ? reducer.translates.data[0] : reducer.currentTranslate,
);

export const getCurrentTranslateLk = createSelector(
  translateReducer,
  getTranslatesForSelectLk,
  (reducer, trOpts) => (R.isEmpty(reducer.currentTranslate) ? trOpts[0] : reducer.currentTranslate),
);

export const getCurrentTranslateCode = createSelector(getCurrentTranslate, i =>
  R.pathOr('', ['code'], i),
);

export const getCurrentTranslateCodeLk = createSelector(getCurrentTranslateLk, i =>
  R.pathOr('', ['code'], i),
);
