import { createSelector } from 'reselect';

const preachingReducer = state => state.preaching;

export const getPreachingExcuseIsLoading = state => state.preaching.isFetching;
export const getPreachingExcuse = createSelector(preachingReducer, i => i.excuses);

export const getPreachingCelebrationIsLoading = state => state.preaching.isFetching;
export const getPreachingCelebration = createSelector(preachingReducer, i => i.celebrations);

export const getPreachingIsLoading = state => state.preaching.isFetching;
export const getPreaching = createSelector(preachingReducer, i => i.preaching);

export const getPreacherIsLoading = state => state.preaching.isFetching;
export const getPreacher = createSelector(preachingReducer, i => i.preacher);

export const getPreachingIsError = state => state.preaching.isError;

export const getPreachersNames = createSelector(
  getPreacher,

  preachers => {
    return [
      { value: 'propovedniki', label: 'Проповедники' },
      ...preachers.map(i => ({ value: i.code, label: i.name })),
    ];
  },
);

export const getPreacherTypes = createSelector(getPreachingExcuse, excuses =>
  excuses.map(i => ({ value: i.code, label: i.title })),
);
