import R from 'ramda';
import { createSelector } from 'reselect';

export const getIsLoading = state => state.exeget.isLoading;
export const getErrorExeget = state => state.exeget.error;

export const getExeget = createSelector(
  state => state.exeget.data,
  i => i,
);

export const ekzegetNameId = createSelector(getExeget, i => ({
  name: R.pathOr('Экзегет', ['ekzeget', 'name'], i),
  id: R.pathOr(1, ['ekzeget', 'id'], i),
  code: R.pathOr('', ['ekzeget', 'code'], i),
}));
