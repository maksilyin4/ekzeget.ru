import R from 'ramda';
import { createSelector } from 'reselect';
import { getQuestionParams } from '../utils';

const questionsReducer = state => state.quiz;

export const getQuestionsIsLoading = state => state.quiz.isLoadingQueeze;

export const getResultsIsLoading = state => state.quiz.isLoadingList;
export const completedQueezePlan = state => state.quiz.completedQueezePlan;

export const getCurrentQuestionData = state => state.quiz.currentQuestionData || {};

export const getIsLoadedCurrentQuestion = state => state.quiz.isLoadedCurrentQuestion;

const getQuestionsData = state => state.quiz.questionsData;

export const getIsQuestionsLoaded = state => state.quiz.isLoadedQuestions;

export const errorAllQuestions = state => state.quiz.errorAllQuestions;

export const getErrorQuestion = state => state.quiz.errorQuestion;

export const getAllQuestions = createSelector(getQuestionsData, R.prop('questions'));

export const getTotalQuestions = createSelector(getQuestionsData, R.prop('totalCount'));

export const getQuestions = createSelector(questionsReducer, i =>
  R.pathOr([], ['queezeData', 'questions'], i),
);

export const getLatestQuestion = createSelector(questionsReducer, i => {
  const currentQuestion = R.pathOr(1, ['queezeData', 'current_question'], i);
  const questions = R.pathOr([], ['queezeData', 'questions'], i);
  const questionParams = getQuestionParams(questions, currentQuestion - 1);

  return questionParams;
});

export const getQuestionsStatus = createSelector(getQuestions, questions =>
  questions.map(question => question.status),
);

export const getCompletedQuestions = createSelector(questionsReducer, i => {
  const results = R.pathOr([], ['completedQueezeData'], i);

  // Лучше конечно на беке
  // Первым не должен идти тот элемент, который имеет статус R

  if (results.length >= 2) {
    const firstItem = results[0];
    const secondItem = results[1];

    if (firstItem.status === 'R' || secondItem.status === 'R') {
      results[0] = secondItem;
      results[1] = firstItem;
    }
  }

  return results;
});
