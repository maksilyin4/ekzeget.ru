import R from 'ramda';
import { createSelector } from 'reselect';

export const bibleMediaLibrary = state => state.mediaLibrary;
export const bibleMediaLibraryLoadingSelector = state => state.mediaLibrary.isFetching;

export const getBibleMediaLibrarySelector = createSelector(
  bibleMediaLibrary,
  bml => R.pathOr([], ['mediaBible', 'data', 'items'], bml),
);
