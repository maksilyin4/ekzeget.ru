/**
 * Основные данные толкователя
 * @typedef {{ id: number, comment: string }} interpretationSkeleton
 */

/**
 * Основные данные экзегета конкретного толкователя
 * @typedef {{ id: number, interpretationLink: string, name: string }} ekzegetForInterpretationSkeleton
 */
