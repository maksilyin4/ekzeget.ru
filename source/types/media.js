import PropTypes from 'prop-types';

export const mediaContentTypes = {
  id: PropTypes.string.isRequired,
  statistics_views_count: PropTypes.number.isRequired,
  image: PropTypes.shape({
    id: PropTypes.string,
    url: PropTypes.string,
  }),
  type: PropTypes.oneOfType([
    PropTypes.oneOf([null]).isRequired,
    PropTypes.string.isRequired,
  ]).isRequired,
  related_media_types: PropTypes.array,
};
