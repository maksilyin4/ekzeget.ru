import { gql, useMutation } from '../../apolloSetup';

export const unSubscriber = gql`
  mutation($subscriber_id: Int, $checksum: String) {
    unsubscribe(subscriber_id: $subscriber_id, checksum: $checksum) {
      success
    }
  }
`;

export default () => {
  const [func, { data, ...rest }] = useMutation(unSubscriber);
  if (data && Object.keys(data).length) {
    const { success } = data.unsubscribe;
    return [func, { data: success, ...rest }];
  }

  return [func, { data: undefined, ...rest }];
};
