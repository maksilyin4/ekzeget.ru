import { gql, useMutation } from '../../apolloSetup';

export const addComments = gql`
  mutation($comment: String, $media_id: Int, $username: String, $user_email: String) {
    create_comment(
      comment: $comment
      media_id: $media_id
      username: $username
      user_email: $user_email
    ) {
      id
      comment
      username
      user_email
      date
      media_id
    }
  }
`;

export default () => useMutation(addComments);
