import { gql, useMutation } from '../../apolloSetup';

export const addSubscriber = gql`
  mutation(
    $media_id: Int!
    $user_email: String
    $user_id: Int
    $username: String
    $back_url: String
  ) {
    new_media_subscriber(
      media_id: $media_id
      user_email: $user_email
      user_id: $user_id
      username: $username
      back_url: $back_url
    ) {
      id
      user_id
      media_id
      username
      user_email
    }
  }
`;

export default () => useMutation(addSubscriber);
