import { gql, useLazyQuery } from '../../apolloSetup';

const getPreachers = gql`
  query {
    preachers(limit: 1000) {
      id
      name
      code
    }
  }
`;
export default () => {
  const [getFunc, { data = {} }, ...rest] = useLazyQuery(getPreachers);
  const { preachers: preachersData } = data;

  if (preachersData) {
    const alphabetSet = new Set();
    const preachers = [...preachersData]
      .sort((a, b) => a.name.trim().localeCompare(b.name.trim()))
      .map(({ id, name }) => {
        const firstLetter = name.charAt(0).toUpperCase();
        alphabetSet.add(firstLetter);
        return {
          value: id,
          label: name,
        };
      });

    const alphabet = [...alphabetSet];

    return [getFunc, preachers, alphabet, ...rest];
  }

  return [getFunc, [], [], ...rest];
};
