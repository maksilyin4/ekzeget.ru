import { gql, useQuery } from '../../apolloSetup';
import { getNetworkStatus } from './utilsQueryLibrary';

export const getMedia = gql`
  query(
    $category_code: String
    $limit: Int
    $offset: Int
    $tag_ids: [Int]
    $connected_to_book_id: Int
    $connected_to_chapter_id: Int
    $connected_to_verse_id: [Int]
    $connected_to_verse_only: Boolean
    $connected_to_chapter_only: Boolean
    $order: Int
    $orderBy: String
    $search_query: String
    $author_id: Int
  ) {
    medias(
      category_code: $category_code
      limit: $limit
      offset: $offset
      tag_ids: $tag_ids
      connected_to_book_id: $connected_to_book_id
      connected_to_chapter_id: $connected_to_chapter_id
      connected_to_verse_id: $connected_to_verse_id
      connected_to_verse_only: $connected_to_verse_only
      connected_to_chapter_only: $connected_to_chapter_only
      order: $order
      orderBy: $orderBy
      search_query: $search_query
      author_id: $author_id
    ) {
      items {
        ...mediaBody
      }
      total
    }
  }

  fragment mediaBody on media {
    id
    statistics_views_count
    active
    title
    length
    template
    year
    code
    type
    era
    location
    art_title
    art_created_date
    art_director
    related_media_types
    categories {
      id
      code
    }
    book {
      id
      title
      author
    }
    author {
      id
      title
    }
    tags {
      id
      title
      code
    }
    genre {
      id
      title
    }
    audio_href
    fb2
    epub
    pdf
    download_link
    video_href
    image {
      id
      url
      optimized(sizes: "default") {
        original
        webp
      }
    }
    media_bible {
      id
      book_id
      chapter_id

      verse_id
      book {
        id
        title
        short_title
        code
      }
      verse {
        id
        number
      }
      chapter {
        id
        number
      }
    }
  }
`;

export default variables => {
  const { data, networkStatus, error, ...rest } = useQuery(getMedia, {
    variables,
    // fetchPolicy: 'cache-and-network',
    notifyOnNetworkStatusChange: true,
  });

  const isLoading = getNetworkStatus(networkStatus);

  let item = { medias: [], isLoading, total: 0 };

  if (data) {
    const {
      medias: { items, total },
    } = data;

    if (networkStatus === 2) {
      item = { medias: items, isLoading: true, total };
    } else {
      item = {
        medias: items,
        isLoading: false,
        total,
        error: '',
      };
    }
  } else if (networkStatus === 7 && !data) {
    item = { medias: [], isLoading: false, total: 0, error: '404' };
  }

  return { ...item, ...rest };
};
