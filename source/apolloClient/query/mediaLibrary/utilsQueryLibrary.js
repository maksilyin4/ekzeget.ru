export const getQueryData = (func, data, store) => {
  const { data: d, loading = true, ...lastData } = data;
  return d ? [func, d[store], loading, lastData] : [func, [], loading, lastData];
};

export const getNetworkStatus = networkStatus => {
  switch (networkStatus) {
    case 1:
    case 3:
    case 7:
      return true;

    default:
      return false;
  }
};

export const parseObjToArr = obj => {
  return obj ? Object.values(obj)[0] || {} : {};
};
