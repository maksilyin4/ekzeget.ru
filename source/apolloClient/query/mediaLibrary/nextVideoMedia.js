import { gql, useQuery } from '../../apolloSetup';

const getNextVideoMedia = gql`
  query(
    $category_code: String
    $template: String
    $author_id: Int
    $current_item_id: Int
    $connected_to_book_id: Int
  ) {
    medias(
      category_code: $category_code
      template: $template
      author_id: $author_id
      current_item_id: $current_item_id
      connected_to_book_id: $connected_to_book_id
    ) {
      items {
        id
        title
        code
        length
        video_href
        author {
          id
          title
        }
        image {
          id
          url
          optimized(sizes: "default") {
            original
            webp
          }
        }
      }
    }
  }
`;

export default variables => {
  const { data, ...rest } = useQuery(getNextVideoMedia, { variables });

  if (data) {
    const {
      medias: { items },
    } = data;

    return { mediaNext: items, ...rest };
  }
  return { mediaNext: [], ...rest };
};
