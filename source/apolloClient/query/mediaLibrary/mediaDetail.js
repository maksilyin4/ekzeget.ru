import { gql, useQuery } from '../../apolloSetup';

export const getMediaDetail = gql`
  query($code: String) {
    medias(code: $code) {
      items {
        id
        title
        type
        text
        year
        template
        location
        era
        length
        art_title
        art_created_date
        art_director
        allow_comment
        comments_count
        statistics_downloads_read_count
        statistics_downloads_listen_count
        related_media_types
        audio_href
        epub
        fb2
        pdf
        video_href
        author {
          id
          title
        }
        meta {
          title
          description
          keywords
          h_one
        }
        media_bible {
          id
          book_id
          chapter_id

          verse_id
          book {
            id
            short_title
            code
          }
          verse {
            id
            number
          }
          chapter {
            id
            number
          }
        }
        book {
          id
        }
        genre {
          id
          title
        }
        author {
          id
          title
        }
        image {
          id
          url
          optimized(sizes: "default") {
            original
            webp
          }
        }
        main_category {
          id
          title
          code
          parent {
            id
            title
            code
          }
        }

        categories {
          id
          title
          code
          parent {
            id
            title
            code
          }
        }
      }
      total
    }
  }
`;

export default ({ variables }) => {
  const { data, error, ...rest } = useQuery(getMediaDetail, {
    variables,
    fetchPolicy: 'cache-and-network',
    notifyOnNetworkStatusChange: true,
  });

  if (data && Object.keys(data).length) {
    const {
      medias: { items: media },
    } = data;

    return { media, error: media.length === 0, ...rest };
  }

  return { media: [], error: false, ...rest };
};
