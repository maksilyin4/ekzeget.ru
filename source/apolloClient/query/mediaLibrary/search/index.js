import { useQuery } from '../../../apolloSetup';

import {
  getSearchMediateka,
  getSearchBible,
  getSearchInterpretations,
  getSearchDictionaries,
  getSearchQuiz,
  getSearchPreaching,
} from './searchQueries/';
import { parseObjToArr, getNetworkStatus } from '../utilsQueryLibrary';

const searchQuery = {
  mediateka: getSearchMediateka,
  bible: getSearchBible,
  interpretations: getSearchInterpretations,
  dictionaries: getSearchDictionaries,
  viktorina: getSearchQuiz,
  propovedi: getSearchPreaching,
};

const queryName = {
  mediateka: 'medias',
  bible: 'verses',
  interpretations: 'interpretations',
  dictionaries: 'dictionaries',
  propovedi: 'preaching_list',
  viktorina: 'queeze_questions_list',
};

export default (searchId = '', variables, search) => {
  const query = searchQuery[searchId];

  const { data: dataFetch = {}, loading, networkStatus, ...rest } = useQuery(
    query || getSearchMediateka,
    {
      variables,
      fetchPolicy: 'network-only',
      notifyOnNetworkStatusChange: true,
      skip: !search,
    },
  );

  const { items = [], total } = parseObjToArr(dataFetch);

  const isLoading = getNetworkStatus(networkStatus || 1);

  let item = { searchData: [], isLoading, total: 0 };

  if (!dataFetch[queryName[searchId]] && isLoading && !search) {
    item = { searchData: items, isLoading: false, total };
  }

  if (dataFetch !== null && dataFetch[queryName[searchId]] && networkStatus !== 2) {
    item = { searchData: items, isLoading: false, total };
  }
  // preloader - когда вводим в поиске
  if (networkStatus === 2 && dataFetch[queryName[searchId]]) {
    item = { searchData: items, isLoading: true, total };
  }
  if (networkStatus === 7 && !dataFetch && !dataFetch[queryName[searchId]]) {
    item = { searchData: [], isLoading: false, total: 0 };
  }

  if (networkStatus === 7 && items.length === 0 && rest.error) {
    item = { searchData: [], isLoading: false, total: 0 };
  }

  return { item: { loading, ...item }, ...rest };
};
