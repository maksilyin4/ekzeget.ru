import { gql } from '../../../../apolloSetup';

const getSearchMediateka = gql`
  query(
    $category_code: String
    $limit: Int
    $offset: Int
    $connected_to_book_id: Int
    $connected_to_chapter_id: Int
    $connected_to_verse_id: [Int]
    $order: Int
    $orderBy: String
    $search_query: String
    $author_id: Int
  ) {
    medias(
      category_code: $category_code
      limit: $limit
      offset: $offset
      connected_to_book_id: $connected_to_book_id
      connected_to_chapter_id: $connected_to_chapter_id
      connected_to_verse_id: $connected_to_verse_id
      order: $order
      orderBy: $orderBy
      search_query: $search_query
      author_id: $author_id
    ) {
      items {
        id
        title
        code
        text
        template
        author {
          id
          title
          description
        }
        categories {
          id
          code
          parent {
            id
            code
          }
        }

        media_bible {
          id
          book_id
          chapter_id

          verse_id
          book {
            id
            title
            short_title
            code
          }
          verse {
            id
            number
          }
          chapter {
            id
            number
          }
        }
      }
      total
    }
  }
`;

// const getSearchBible = gql`
//   query(
//     $limit: Int
//     $offset: Int
//     $order: Int
//     $orderBy: String
//     $connected_to_book_id: Int
//     $connected_to_chapter_id: Int
//     $connected_to_verse_id: [Int]
//     $search_query: String
//     $code: [TranslateEnumType]
//   ) {
//     verses {
//       items(
//         search_query: $search_query
//         limit: $limit
//         offset: $offset
//         order: $order
//         orderBy: $orderBy
//         connected_to_book_id: $connected_to_book_id
//         connected_to_chapter_id: $connected_to_chapter_id
//         connected_to_verse_id: $connected_to_verse_id
//       ) {
//         id
//         text
//         number
//         chapter {
//           id
//           book_id
//           number
//         }
//         book {
//           id
//           title
//           short_title
//           code
//           author
//         }
//         verse_translate(code: $code) {
//           id
//           code
//           text
//         }
//       }
//       total
//     }
//   }
// `;

const getSearchBible = gql`
  query(
    $limit: Int
    $offset: Int
    $order: Int
    $orderBy: String
    $connected_to_book_id: Int
    $connected_to_chapter_id: Int
    $connected_to_verse_id: [Int]
    $search_query: String
    $code: [TranslateEnumType]
  ) {
    verses(
      search_query: $search_query
      limit: $limit
      offset: $offset
      order: $order
      orderBy: $orderBy
      connected_to_book_id: $connected_to_book_id
      connected_to_chapter_id: $connected_to_chapter_id
      connected_to_verse_id: $connected_to_verse_id
    ) {
      items {
        id
        text
        number
        chapter {
          id
          book_id
          number
        }
        book {
          id
          title
          short_title
          code
          author
        }
        verse_translate(code: $code) {
          id
          code
          text
        }
      }
      total
    }
  }
`;

const getSearchInterpretations = gql`
  query(
    $limit: Int
    $offset: Int
    $order: Int
    $orderBy: String
    $connected_to_book_id: Int
    $connected_to_chapter_id: Int
    $connected_to_verse_id: [Int]
    $search_query: String
  ) {
    interpretations(
      search_query: $search_query
      limit: $limit
      offset: $offset
      order: $order
      orderBy: $orderBy
      connected_to_book_id: $connected_to_book_id
      connected_to_chapter_id: $connected_to_chapter_id
      connected_to_verse_id: $connected_to_verse_id
    ) {
      items {
        id
        active
        verses {
          id
          text
          number
          chapter {
            id
            number
          }
          book {
            id
            title
            short_title
            code
            author
          }
        }
        edited {
          id
          username
          email
        }
        added {
          id
          username
          email
        }
        comment
      }
      total
    }
  }
`;

const getSearchDictionaries = gql`
  query($limit: Int, $offset: Int, $order: Int, $orderBy: String, $search_query: String) {
    dictionaries(
      search_query: $search_query
      order: $order
      orderBy: $orderBy
      limit: $limit
      offset: $offset
    ) {
      items {
        id
        word
        code
        description
        type {
          id
          title
          alias
          code
        }
      }
      total
    }
  }
`;

const getSearchQuiz = gql`
  query(
    $limit: Int
    $offset: Int
    $order: Int
    $orderBy: String
    $connected_to_book_id: Int
    $connected_to_chapter_id: Int
    $connected_to_verse_id: [Int]
    $search_query: String
  ) {
    queeze_questions_list(
      limit: $limit
      offset: $offset
      order: $order
      orderBy: $orderBy
      connected_to_book_id: $connected_to_book_id
      connected_to_chapter_id: $connected_to_chapter_id
      connected_to_verse_id: $connected_to_verse_id
      search_query: $search_query
    ) {
      items {
        id
        title
        description
        code
      }
      total
    }
  }
`;

const getSearchPreaching = gql`
  query(
    $limit: Int
    $offset: Int
    $connected_to_book_id: Int
    $connected_to_chapter_id: Int
    $connected_to_verse_id: [Int]
    $search_query: String
    $preacher_id: Int
  ) {
    preaching_list(
      limit: $limit
      offset: $offset
      connected_to_book_id: $connected_to_book_id
      connected_to_chapter_id: $connected_to_chapter_id
      connected_to_verse_id: $connected_to_verse_id
      search_query: $search_query
      preacher_id: $preacher_id
    ) {
      items {
        id
        theme
        text
        preacher {
          id
          name
          code
        }
        excuse {
          id
          title
          code
          parent {
            id
            title
            code
          }
        }
      }
      total
    }
  }
`;

export {
  getSearchMediateka,
  getSearchBible,
  getSearchInterpretations,
  getSearchDictionaries,
  getSearchQuiz,
  getSearchPreaching,
};
