import { gql, useQuery } from '../../apolloSetup';
import { getNetworkStatus } from './utilsQueryLibrary';

export const getMedia = gql`
  query(
    $category_code: String
    $limit: Int
    $offset: Int
    $orderBy: String
  ) {
    medias(
      category_code: $category_code
      limit: $limit
      offset: $offset
      orderBy: $orderBy
    ) {
      items {
        ...mediaBody
      }
    }
  }
  
  fragment mediaBody on media {
    video_href
  }
`;

export default variables => {
  try {
    const {data, networkStatus, error, ...rest} = useQuery(getMedia, {
      variables,
      // fetchPolicy: 'cache-and-network',
      notifyOnNetworkStatusChange: true,
    });

    const isLoading = getNetworkStatus(networkStatus);

    let item = {medias: [], isLoading};

    if (data) {
      const {
        medias: {items},
      } = data;

      if (networkStatus === 2) {
        item = {medias: items, isLoading: true};
      } else {
        item = {
          medias: items,
          isLoading: false,
          error: '',
        };
      }
    } else if (networkStatus === 7 && !data) {
      item = {medias: [], isLoading: false, error: '404'};
    }

    return {...item, ...rest};
  } catch (e) {
    return { medias: [], loading: false, error: '400' };
  }
};