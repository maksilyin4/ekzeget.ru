import { gql, useQuery } from '../../apolloSetup';
import { getNetworkStatus } from './utilsQueryLibrary';

const getVideoList = gql`
  query(
    $category_code: String
    $template: String
    $limit: Int
    $offset: Int
    $author_id: Int
    $connected_to_book_id: Int
  ) {
    medias(
      category_code: $category_code
      template: $template
      limit: $limit
      offset: $offset
      author_id: $author_id
      connected_to_book_id: $connected_to_book_id
    ) {
      items {
        id
        title
        code
        length
        video_href
        author {
          id
          title
        }
        image {
          id
          url
          optimized(sizes: "default") {
            original
            webp
          }
        }
      }
      total
    }
  }
`;

export default variables => {
  const { data, networkStatus, ...rest } = useQuery(getVideoList, {
    variables,
    fetchPolicy: 'cache-and-network',
    notifyOnNetworkStatusChange: true,
  });

  const isLoading = getNetworkStatus(networkStatus);

  let item = { mediaList: [], isLoading, total: 0, ...rest };

  if (data) {
    const {
      medias: { items, total },
    } = data;

    item = { mediaList: items, isLoading: !isLoading, total, ...rest };
  } else if (networkStatus === 7 && !data) {
    item = { mediaList: [], isLoading: !isLoading, total: 0, ...rest };
  }
  return item;
};
