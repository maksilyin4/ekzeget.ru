import { gql, useLazyQuery } from '../../apolloSetup';

const getAuthor = gql`
  query {
    author_category {
      id
      title
      authors(limit: 1000) {
        id
        title
      }
    }
  }
`;

export default () => {
  const [getFunc, { data = {} }, ...rest] = useLazyQuery(getAuthor);
  const { author_category } = data;

  if (author_category) {
    const authorsCategory = author_category.map(({ id, title, ...last }) => ({
      value: id,
      label: title,
      authors: [...last.authors]
        .sort((a, b) => a.title.trim().localeCompare(b.title.trim()))
        .map(author => ({ label: author.title, value: author.id })),
    }));
    return [getFunc, authorsCategory, ...rest];
  }

  return [getFunc, [], ...rest];
};
