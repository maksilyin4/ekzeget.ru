import { gql, useQuery } from '../../apolloSetup';

export const getRelated = gql`
  query($media_id: Int!) {
    play: related_media(media_id: $media_id, type: "V", limit: 1000) {
      id
      active
      type
      title
    }
  }
`;

const A = gql`
  query($media_id: Int!, $type: String) {
    A: related_media(media_id: $media_id, type: $type, limit: 1500) {
      id
      sort
      grouping_name
      related_media_types
      active
      type
      title
      length
      audio_href
      number
      book {
        id
        title
        chapters {
          number
        }
      }
      media_bible {
        book {
          code
        }
        chapter {
          number
        }
        verse {
          number
        }
      }
    }
  }
`;

const V = gql`
  query($media_id: Int!, $type: String) {
    V: related_media(media_id: $media_id, type: $type, limit: 1000) {
      id
      active
      type
      title
      length
      video_href
      media_bible {
        book {
          code
        }
        chapter {
          number
          verses {
            id
          }
        }
        verse {
          number
        }
      }
      image {
        id
        url
        optimized(sizes: "default") {
          original
          webp
        }
      }
    }
  }
`;
const C = gql`
  query($media_id: Int!, $type: String) {
    C: related_media(media_id: $media_id, type: $type, limit: 1000) {
      id
      active
      type
      title
      fb2
      code
      epub
      pdf
      text
      download_link
    }
  }
`;

export const relatedObjGql = {
  A,
  V,
  C,
};

export default variables => {
  const { type } = variables;

  const related = relatedObjGql[type] || getRelated;

  const { data, ...rest } = useQuery(related, {
    variables: { ...variables },
  });

  if (data && data[type]) {
    return { relatedData: data[type], ...rest };
  }

  return { relatedData: [], ...rest };
};
