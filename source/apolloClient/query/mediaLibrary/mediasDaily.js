import { gql, useQuery } from '../../apolloSetup';
import { getNetworkStatus } from './utilsQueryLibrary';

export const getMediasDaily = gql`
  query($daily_media: String) {
    medias_daily(daily_media: $daily_media) {
      video_href
      
      celebration {
        id
        title
      }
    }
  }
`;

export default () => {
  try {
    const { data, networkStatus, error, ...rest } = useQuery(getMediasDaily, {
      variables: {
        daily_media: new Date().toISOString().slice(0, 10),
      },
      fetchPolicy: 'cache-and-network',
      notifyOnNetworkStatusChange: true,
    });

    const loading = getNetworkStatus(networkStatus);

    let item = { medias: [], loading };

    if (data) {
      const { medias_daily: items } = data;

      if (networkStatus === 2) {
        item = { medias: items, loading: true };
      } else {
        item = {
          medias: items,
          loading: false,
          error: '',
        };
      }
    } else if (networkStatus === 7 && !data) {
      item = { medias: [], loading: false, error: '404' };
    }

    return { ...item, ...rest };
  } catch (e) {
    return { medias: [], loading: false, error: '400' };
  }

};
