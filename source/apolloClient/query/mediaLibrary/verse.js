import { gql, useLazyQuery } from '../../apolloSetup';
import { getQueryData } from './utilsQueryLibrary';

const getVerseQuery = gql`
  query($connected_to_chapter_id: Int) {
    verse(connected_to_chapter_id: $connected_to_chapter_id) {
      id
      number
    }
  }
`;
export default () => {
  const [getFunc, data] = useLazyQuery(getVerseQuery);
  return getQueryData(getFunc, data, 'verse');
};
