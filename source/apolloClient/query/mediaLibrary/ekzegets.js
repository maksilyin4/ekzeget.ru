import { gql, useLazyQuery } from '../../apolloSetup';

const getEkzegets = gql`
  query {
    ekzeget_person(limit: 1000) {
      id
      name
      code
    }
  }
`;
export default () => {
  const [getFunc, { data = {} }, ...rest] = useLazyQuery(getEkzegets);

  const { ekzeget_person } = data;
  if (ekzeget_person) {
    const alphabetSet = new Set();
    const ekzegets = [...ekzeget_person]
      .sort((a, b) => a.name.trim().localeCompare(b.name.trim()))
      .map(({ id, name }) => {
        const firstLetter = name.charAt(0).toUpperCase();
        alphabetSet.add(firstLetter);
        return {
          value: id,
          label: name,
        };
      });

    const alphabet = [...alphabetSet];

    return [getFunc, ekzegets, alphabet, ...rest];
  }

  return [getFunc, [], [], ...rest];
};
