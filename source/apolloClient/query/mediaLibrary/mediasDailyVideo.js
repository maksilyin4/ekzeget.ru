import { gql, useQuery } from '../../apolloSetup';
import { getNetworkStatus } from './utilsQueryLibrary';

export const getMediasDailyVideo = gql`
  query($daily_media: String) {
    medias_daily(daily_media: $daily_media) {
      video_href
      celebration {
        id
        title
      }
    }
  }
`;

export default variables => {
  try {
    const { daily_media = new Date().toISOString().slice(0, 10) } = variables;
    const props = useQuery(getMediasDailyVideo, {
      variables: {
        daily_media,
      },
      fetchPolicy: 'cache-and-network',
      notifyOnNetworkStatusChange: true,
    });

    const { data, networkStatus, error, ...rest } = props;

    const loading = getNetworkStatus(networkStatus);

    let item = { medias: [], loading };

    if (data) {
      const { medias_daily: items } = data;

      if (networkStatus === 2) {
        const newItemWithCelebration = items.filter(({ celebration }) => celebration !== null);
        const newItems = items.length
          ? newItemWithCelebration.length
            ? newItemWithCelebration
            : items
          : [];
        item = { medias: newItems, loading: true };
      } else {
        // eslint-disable-next-line no-prototype-builtins
        const newItemWithCelebration = items.filter(({ celebration }) => celebration !== null);
        const newItems = items.length
          ? newItemWithCelebration.length
            ? newItemWithCelebration
            : items
          : [];
        item = {
          medias: newItems,
          loading: false,
          error: '',
        };
      }
    } else if (networkStatus === 7 && !data) {
      item = { medias: [], loading: false, error: '404' };
    }

    return { ...item, ...rest };
  } catch (e) {
    return { medias: [], loading: false, error: '400' };
  }
};
