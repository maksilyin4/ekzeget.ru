import { gql, useQuery } from '../../apolloSetup';

const getBookReady = gql`
  query($id: Int, $type: String, $bookId: Int!) {
    book(id: $id) {
      id
      media_authors(medias_filter: { type: $type }) {
        id
        title
        image
        book_media(book_id: $bookId, limit: 1) {
          id
          code
        }
      }
    }
  }
`;
export default variables => {
  const { data, ...props } = useQuery(getBookReady, variables);

  if (
    data &&
    data.book &&
    data.book.length &&
    data.book[0].media_authors &&
    data.book[0].media_authors.length
  ) {
    return { data: data.book[0].media_authors, ...props };
  }
  return { data: [], ...props };
};
