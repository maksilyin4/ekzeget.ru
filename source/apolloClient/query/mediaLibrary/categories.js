import { gql, useQuery } from '../../apolloSetup';

export const getCategories = gql`
  query($id: ID) {
    category(id: $id, parent_id: 0) {
      id
      title
      code
      template_code
      description
      children {
        id
        title
        code
        template_code
        parent {
          id
        }
        meta {
          title
          description
          keywords
          h_one
        }
      }
      meta {
        title
        description
        keywords
        h_one
      }
    }
  }
`;

export default () => {
  const { data, ...rest } = useQuery(getCategories);
  if (data) {
    return { categories: data.category, ...rest };
  }
  return { categories: [], ...rest };
};
