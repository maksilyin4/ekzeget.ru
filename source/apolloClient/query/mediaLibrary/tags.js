import { gql, useQuery } from '../../apolloSetup';

const getTags = gql`
  query {
    tag {
      id
      title
      code
    }
  }
`;

export default () => {
  const { data, ...rest } = useQuery(getTags);
  if (data) {
    return { tags: data.tag, ...rest };
  }
  return { tags: [], ...rest };
};
