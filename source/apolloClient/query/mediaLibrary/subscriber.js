import { gql, useLazyQuery } from '../../apolloSetup';

export const getUnSubscriber = gql`
  query($id: Int, $subscribe_id: Int!) {
    medias(id: $id) {
      items {
        id
        subscription(subscribe_id: $subscribe_id) {
          id
          checksum
        }
      }
    }
  }
`;

export default () => {
  const [func, { data, ...rest }] = useLazyQuery(getUnSubscriber);

  if (data && Object.keys(data).length) {
    const {
      medias: { items: media },
    } = data;
    const unSubscriber = media[0];
    return [func, unSubscriber, rest];
  }
  return [func, [], rest];
};
