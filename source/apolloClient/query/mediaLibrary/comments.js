import { gql, useQuery } from '../../apolloSetup';

const getComments = gql`
  query($media_id: Int, $limit: Int, $offset: Int, $excludeIds: [Int]) {
    comment(media_id: $media_id, limit: $limit, offset: $offset, excludeIds: $excludeIds) {
      id
      comment
      username
      date
      media_id
    }
  }
`;

export default ({ variables }) => {
  const { data, ...rest } = useQuery(getComments, { variables });

  if (data) {
    return { comments: data.comment, ...rest };
  }
  return { comments: [], ...rest };
};
