import { gql, useLazyQuery } from '../../apolloSetup';
import { getQueryData } from './utilsQueryLibrary';

const getChapterQuery = gql`
  query($book_id: Int) {
    chapter(book_id: $book_id, limit: 151) {
      id
      book_id
      number
    }
  }
`;
export default () => {
  const [getFunc, data] = useLazyQuery(getChapterQuery);
  return getQueryData(getFunc, data, 'chapter');
};
