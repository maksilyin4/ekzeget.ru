import { gql, useQuery } from '../../apolloSetup';

const getBook = gql`
  query {
    book {
      id
      menu
      testament_id
    }
  }
`;

export default () => {
  const { data, ...rest } = useQuery(getBook);
  const booksOld = [];
  const booksNew = [];
  if (data) {
    data.book.forEach(({ testament_id, ...book }) => {
      if (testament_id === 1) {
        booksOld.push(book);
      } else {
        booksNew.push(book);
      }
    });
    return { books: data.book, booksOld, booksNew, ...rest };
  }
  return { books: [], booksOld, booksNew, ...rest };
};
