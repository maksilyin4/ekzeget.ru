import { gql, useQuery } from '../../apolloSetup';

const getTemplate = gql`
  query($code: String) {
    category(code: $code) {
      id
      single_template
      template_code
    }
  }
`;
export default ({ variables }) => {
  const { data, ...rest } = useQuery(getTemplate, { variables });

  if (data) {
    return { templateData: data.category, ...rest };
  }
  return { templateData: [], ...rest };
};
