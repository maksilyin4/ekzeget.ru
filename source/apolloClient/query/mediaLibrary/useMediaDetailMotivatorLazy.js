import { gql, useLazyQuery } from '../../apolloSetup';

const getMediaDetailMotivator = gql`
  query($id: Int) {
    medias(id: $id) {
      items {
        id
        statistics_views_count
        active
        title
        length
        template
        year
        code
        type
        era
        location
        art_title
        art_created_date
        art_director
        related_media_types
        categories {
          id
          code
        }
        book {
          id
          title
          author
        }
        author {
          id
          title
        }
        tags {
          id
          title
          code
        }
        genre {
          id
          title
        }
        audio_href
        fb2
        epub
        pdf
        download_link
        video_href
        image {
          id
          url
        }
        media_bible {
          id
          book_id
          chapter_id

          verse_id
          book {
            id
            title
            short_title
            code
          }
          verse {
            id
            number
          }
          chapter {
            id
            number
          }
        }
      }
    }
  }
`;

export default () => {
  const [getFunc, data] = useLazyQuery(getMediaDetailMotivator);

  const { data: d, loading = true, ...lastData } = data;
  if (d) {
    const motivator = d.medias.items[0];
    return [getFunc, motivator, loading, lastData];
  }

  return [getFunc, {}, loading, lastData];
};
