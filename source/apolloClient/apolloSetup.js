import { HttpLink } from 'apollo-link-http';
import fetch from 'node-fetch';

import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';

import gql from 'graphql-tag';
import { useQuery, useMutation, useLazyQuery } from '@apollo/react-hooks';

const apolloClient = url => {
  const httpLink = new HttpLink({
    fetch,
    uri: `${url}/api/v2/graphql`,
    useGETForQueries: true,
  });

  return new ApolloClient({
    cache: new InMemoryCache().restore((process.env.BROWSER && window.__APOLLO_STATE__) || {}),
    link: httpLink,
    ssrForceFetchDelay: 100,
  });
};

export { apolloClient, gql, useQuery, useMutation, useLazyQuery };
