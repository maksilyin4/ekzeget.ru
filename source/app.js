import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from '@apollo/react-hooks';
import { Provider } from 'react-redux';
import { CookiesProvider } from 'react-cookie';
import { BrowserRouter as Router } from 'react-router-dom';

import Main from './components/Main/Main';
import { initialState, store, urlApollo } from './getStore';

import { apolloClient } from './apolloClient/apolloSetup';

ReactDOM.hydrate(
  <ApolloProvider client={apolloClient(urlApollo)}>
    <Provider store={store}>
      <CookiesProvider>
        <Router>
          <Main initialState={initialState} />
        </Router>
      </CookiesProvider>
    </Provider>
  </ApolloProvider>,
  document.getElementById('root'),
);