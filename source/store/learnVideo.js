import { _AXIOS } from '../dist/ApiConfig';

export const getLearnVideo = ({ query }) => _AXIOS({ url: `config?name=${query}` });
