import { combineReducers } from 'redux';

import { SET_BROWSER_SAFARI, SET_BROWSER_GOOGLE } from './action';

const isSafari = (state = false, { type }) => {
  switch (type) {
    case SET_BROWSER_SAFARI:
      return true;
    case SET_BROWSER_GOOGLE:
      return false;
    default:
      return state;
  }
};
const isGoogle = (state = false, { type }) => {
  switch (type) {
    case SET_BROWSER_SAFARI:
      return false;
    case SET_BROWSER_GOOGLE:
      return true;
    default:
      return state;
  }
};

export default combineReducers({
  isSafari,
  isGoogle,
});
