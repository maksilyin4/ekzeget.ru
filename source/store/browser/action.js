export const SET_BROWSER_SAFARI = 'SET_BROWSER_SAFARI';
export const SET_BROWSER_GOOGLE = 'SET_BROWSER_GOOGLE';

export const setBrowser = () => dispatch => {
  const ua = window.navigator.userAgent.toLowerCase();
  if (ua.indexOf('safari') !== -1) {
    if (ua.indexOf('chrome') > -1) {
      dispatch({ type: SET_BROWSER_GOOGLE });
    } else {
      dispatch({ type: SET_BROWSER_SAFARI });
    }
  }
};
