import { createSelector } from 'reselect';

const browserReducer = store => store.browsers;

export const getIsSafari = createSelector(
  browserReducer,
  i => i.isSafari,
);
export const getIsGoogle = createSelector(
  browserReducer,
  i => i.isGoogle,
);
