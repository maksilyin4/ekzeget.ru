export const bibleMediaQuestionsReducer = state => state.questionsBible.questions;
export const bibleMediaQuestionsIsLoading = state => state.questionsBible.isLoading;
