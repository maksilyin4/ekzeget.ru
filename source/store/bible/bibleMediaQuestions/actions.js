import { _AXIOS } from '../../../dist/ApiConfig';

export const BIBLE_MEDIA_QUESTIONS_FETCH = 'BIBLE_MEDIA_QUESTIONS_FETCH';
export const BIBLE_MEDIA_QUESTIONS_SUCCESS = 'BIBLE_MEDIA_QUESTIONS_SUCCESS';
export const BIBLE_MEDIA_QUESTIONS_FAILURE = 'BIBLE_MEDIA_QUESTIONS_FAILURE';

export const getBibleMediaQuestionsSuccess = ({ data, id, page, type }) => ({
  type: BIBLE_MEDIA_QUESTIONS_SUCCESS,
  payload: {
    data,
    id,
    page,
    type,
  },
});

export const getBibleMediaQuestionsFetch = () => ({
  type: BIBLE_MEDIA_QUESTIONS_FETCH,
});

export const getBibleMediaQuestionsFailure = ({ error, id, type }) => ({
  type: BIBLE_MEDIA_QUESTIONS_FAILURE,
  payload: {
    error,
    id,
    type,
  },
});

export const getBibleMediaQuestions = ({
  book_id,
  chapter_id,
  verse_id,
  perPage = 20,
  page = 1,
  type = 'T',
}) => dispatch => {
  const paramsQuestion = verse_id ? { verse_id } : { book_id, chapter_id };
  const id = verse_id || chapter_id;

  const query = {
    'per-page': perPage,
    page,
    type,
    ...paramsQuestion,
  };

  dispatch(getBibleMediaQuestionsFetch());

  return _AXIOS({
    url: '/queeze/question-list',
    query,
  })
    .then(data => dispatch(getBibleMediaQuestionsSuccess({ data, id, page, type })))
    .catch(error => dispatch(getBibleMediaQuestionsFailure({ error, id, type })));
};

export default getBibleMediaQuestions;
