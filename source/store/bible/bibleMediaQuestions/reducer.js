import {
  BIBLE_MEDIA_QUESTIONS_FETCH,
  BIBLE_MEDIA_QUESTIONS_SUCCESS,
  BIBLE_MEDIA_QUESTIONS_FAILURE,
} from './actions';
import { getQuestions } from './utils';

const initialState = {
  isLoading: false,
  questions: {},
  error: '',
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case BIBLE_MEDIA_QUESTIONS_FETCH:
      return { ...state, isLoading: true, error: '' };
    case BIBLE_MEDIA_QUESTIONS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        questions: { ...state.questions, ...getQuestions(state, payload) },
      };
    case BIBLE_MEDIA_QUESTIONS_FAILURE:
      return {
        ...state,
        isLoading: false,
        questions: {
          ...state.questions,
          [payload.id + payload.type]: { questions: [], totalCount: 0, page: 1 },
        },
        error: payload.error,
      };
    default:
      return state;
  }
};
