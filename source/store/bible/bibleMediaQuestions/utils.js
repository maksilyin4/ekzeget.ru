export const getQuestions = (state, payload) => {
  const { id, type, data, page } = payload;
  let questions;

  const questionId = id + type;
  if (!state.questions[questionId]) {
    questions = { [questionId]: { ...data, page: page + 1 } };
  } else {
    questions = {
      ...state.questions,
      [questionId]: {
        ...state.questions[questionId],
        questions: [...state.questions[questionId].questions, ...data.questions],
        page: page + 1,
      },
    };
  }
  return questions;
};
