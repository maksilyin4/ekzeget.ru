import { _AXIOS } from '../../dist/ApiConfig';

export const GET_META_DATA_FETCH = 'GET_META_DATA_FETCH';
export const GET_META_DATA_FETCH_SUCCESS = 'GET_META_DATA_FETCH_SUCCESS';
export const GET_META_DATA_FETCH_FAILURE = 'GET_META_DATA_FETCH_FAILURE';
export const SET_META_DATA_FETCH_CLEAR = 'SET_META_DATA_FETCH_CLEAR';

export const getFulfilled = data => ({
  type: GET_META_DATA_FETCH_SUCCESS,
  payload: data,
});

export const getPending = () => ({
  type: GET_META_DATA_FETCH,
});

export const getRejected = error => ({
  type: GET_META_DATA_FETCH_FAILURE,
  error,
});

export const setClearMeta = () => ({
  type: SET_META_DATA_FETCH_CLEAR,
});

export const getMetaData = code => dispatch => {
  dispatch(getPending());
  return _AXIOS({
    url: '/meta/get-by-code',
    query: {
      code,
    },
  })
    .then(data => dispatch(getFulfilled(data)))
    .catch(error => dispatch(getRejected(error)));
};
