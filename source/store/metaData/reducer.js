import {
  GET_META_DATA_FETCH,
  GET_META_DATA_FETCH_SUCCESS,
  GET_META_DATA_FETCH_FAILURE,
  SET_META_DATA_FETCH_CLEAR,
} from './actions';
import { META_CONTEXT_DATA } from '../../constants';

export default (
  state = { meta: null, metaContext: null, isLoadingMeta: true },
  { type, payload },
) => {
  switch (type) {
    case GET_META_DATA_FETCH:
      return { ...state, isLoadingMeta: true };
    case GET_META_DATA_FETCH_SUCCESS:
      return { ...state, meta: payload.meta, isLoadingMeta: false };
    case GET_META_DATA_FETCH_FAILURE:
      return { ...state, meta: null, isLoadingMeta: true };
    case SET_META_DATA_FETCH_CLEAR:
      return { ...state, meta: null, isLoadingMeta: true };
    case META_CONTEXT_DATA:
      return { ...state, metaContext: payload };
    default:
      return state;
  }
};
