import { createCommentAPI, deleteComment, getComments } from './api';

export const commentsActions = {
  GET_COMMENTS: '/comments/GET_COMMENTS',
  GET_COMMENTS_PENDING: '/comments/GET_COMMENTS_PENDING',
  GET_COMMENTS_SUCCESS: '/comments/GET_COMMENTS_SUCCESS',
  GET_COMMENTS_FAILURE: '/comments/GET_COMMENTS_FAILURE',
  CREATE_COMMENT: '/comments/CREATE_COMMENT',
  REPLY_COMMENT: '/comments/REPLY_COMMENT',
  DELETE_COMMENT: '/comments/DELETE_COMMENT',
};

export const setCommentsFulfilled = data => ({
  type: commentsActions.GET_COMMENTS_SUCCESS,
  payload: data,
});

export const setCommentsPending = () => ({
  type: commentsActions.GET_COMMENTS_PENDING,
});

export const setCommentsRejected = error => ({
  type: commentsActions.GET_COMMENTS_FAILURE,
  error,
});

export const replyCommentAction = (comment, replyId) => ({
  type: commentsActions.REPLY_COMMENT,
  payload: { comment, replyId },
});

export const deleteCommentAction = commentId => ({
  type: commentsActions.DELETE_COMMENT,
  payload: { commentId },
});

export const createCommentAction = data => ({
  type: commentsActions.CREATE_COMMENT,
  payload: data,
});

export const getCommentsPlanByDay = (planId, group_id) => async dispatch => {
  dispatch(setCommentsPending());
  try {
    const data = await getComments(planId, group_id);
    dispatch(setCommentsFulfilled(data));
  } catch (e) {
    dispatch(setCommentsRejected(e));
  }
};

export const replyComment = (planId, groupId, content, reply_to_comment_id) => async dispatch => {
  try {
    const data = await createCommentAPI(planId, groupId, content, reply_to_comment_id);

    dispatch(replyCommentAction(data, reply_to_comment_id));
  } catch (e) {
    console.error(e);
  }
};

export const deleteCommentThunk = comment_id => async dispatch => {
  try {
    await deleteComment(comment_id);
    dispatch(deleteCommentAction(comment_id));
  } catch (e) {
    dispatch(deleteCommentAction(comment_id));
  }
};

export const createComment = (planId, groupId, content) => async dispatch => {
  try {
    const data = await createCommentAPI(planId, groupId, content);
    dispatch(createCommentAction(data));
  } catch (e) {
    console.error(e);
  }
};
