import { commentsActions } from './actions';
import { combineReducers } from 'redux';

const commentsPlanReducer = (state = [], action) => {
  switch (action.type) {
    case commentsActions.GET_COMMENTS_SUCCESS:
      return action.payload;
    case commentsActions.REPLY_COMMENT:
      return state.map(comment => {
        if (Number(action.payload.replyId) === Number(comment.id)) {
          return { ...comment, replies: [...comment?.replies, action.payload.comment] };
        }
        return comment;
      });
    case commentsActions.DELETE_COMMENT:
      return state.filter(comment => comment.id !== action.payload.commentId);
    case commentsActions.CREATE_COMMENT:
      return [...state, { ...action.payload, replies: [] }];
    default:
      return state;
  }
};

export default combineReducers({
  commentsPlanReducer,
});
