import { AXIOS, _AXIOS } from '../../../dist/ApiConfig';
import { LS } from '../../../dist/browserUtils';

export const getComments = async (planId, groupId) => {
  const data = await _AXIOS({
    url: `/reading/plan/${planId}/comment?group_id=${groupId}`,
  });
  return data.comments;
};

export const createCommentAPI = async (planId, groupId, content, reply_to_comment_id) => {
  const { data } = await AXIOS.post(
    `/reading/plan/${planId}/comment/`,
    {
      group_id: groupId,
      content,
      reply_to_comment_id,
    },
    {
      headers: {
        'X-Authentication-Token': LS.get('token'),
      },
    },
  );
  return data.comment;
};

export const deleteComment = async comment_id => {
  const { data } = await AXIOS.delete(`/reading/plan/comment/${comment_id}`, {
    headers: {
      'X-Authentication-Token': LS.get('token'),
    },
  });
  return data.comment;
};
