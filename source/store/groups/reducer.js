import { combineReducers } from 'redux';
import { GROUP_ACTIONS_USER, GROUP_ACTIONS_MENTOR } from './actions';

const defaultState = {};
const mentorGroup = (state = defaultState, action) => {
  switch (action.type) {
    case GROUP_ACTIONS_MENTOR.MENTOR_GROUP_FETCH_SUCCESS: {
      return action.payload;
    }
    case GROUP_ACTIONS_USER.USER_GROUP_DELETED:
      return {};
    default:
      return state;
  }
};

const userGroup = (state = defaultState, action) => {
  switch (action.type) {
    case GROUP_ACTIONS_USER.USER_GROUP_FETCH_SUCCESS:
      return action.payload;
    case GROUP_ACTIONS_USER.USER_GROUP_DELETED:
      return {};
    default:
      return state;
  }
};

const mentorGroupLoading = (state = false, action) => {
  switch (action.type) {
    case GROUP_ACTIONS_MENTOR.MENTOR_GROUP_FETCH:
      return true;
    case GROUP_ACTIONS_MENTOR.MENTOR_GROUP_FETCH_FAILURE:
      return false;
    case GROUP_ACTIONS_MENTOR.MENTOR_GROUP_FETCH_SUCCESS:
      return false;
    default:
      return state;
  }
};

const userGroupLoading = (state = false, action) => {
  switch (action.type) {
    case GROUP_ACTIONS_USER.USER_GROUP_FETCH:
      return true;
    case GROUP_ACTIONS_USER.USER_GROUP_FETCH_FAILURE:
      return false;
    case GROUP_ACTIONS_USER.USER_GROUP_FETCH_SUCCESS:
      return false;
    default:
      return state;
  }
};

const userGroupError = (state = { hasError: false, error: null }, action) => {
  switch (action.type) {
    case GROUP_ACTIONS_USER.USER_GROUP_FETCH_FAILURE:
      return {
        hasError: true,
        error: action.error,
      };
    case GROUP_ACTIONS_USER.USER_GROUP_FETCH_SUCCESS:
      return {
        hasError: false,
        error: null,
      };
    default:
      return state;
  }
};

export default combineReducers({
  mentorGroupLoading,
  mentorGroup,
  userGroupError,
  userGroup,
  userGroupLoading,
});
