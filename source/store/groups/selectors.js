export const userGroup = state => state.groups.userGroup;
export const userGroupLoading = state => state.groups.userGroupLoading;
export const userGroupError = state => state.groups.userGroupError;
export const mentorGroup = state => state.groups.mentorGroup;
export const userGroups = state => state.groups;
export const mentorGroupLoading = state => state.groups.mentorGroupLoading;
