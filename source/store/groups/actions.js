import { deleteGroup } from '../../dist/api/readingPlan/createGroup';
import {
  getGroupAsMentor,
  getGroupAsUser,
  getGroupPreview,
} from '../../dist/api/readingPlan/getGroup';

export const GROUP_ACTIONS_USER = {
  USER_GROUP_FETCH_FAILURE: 'USER_GROUP_FETCH_FAILURE',
  USER_GROUP_FETCH: 'USER_GROUP_FETCH',
  USER_GROUP_FETCH_SUCCESS: 'USER_GROUP_FETCH_SUCCESS',
  USER_GROUP_DELETED: 'USER_GROUP_DELETED',
};

export const GROUP_ACTIONS_MENTOR = {
  MENTOR_GROUP_FETCH_FAILURE: 'MENTOR_GROUP_FETCH_FAILURE',
  MENTOR_GROUP_FETCH: 'MENTOR_GROUP_FETCH',
  MENTOR_GROUP_FETCH_SUCCESS: 'MENTOR_GROUP_FETCH_SUCCESS',
};

export const setGroupMentorFulfilled = data => ({
  type: GROUP_ACTIONS_MENTOR.MENTOR_GROUP_FETCH_SUCCESS,
  payload: data,
});

export const setGroupMentorPending = () => ({
  type: GROUP_ACTIONS_MENTOR.MENTOR_GROUP_FETCH,
});

export const setGroupMentorRejected = error => ({
  type: GROUP_ACTIONS_MENTOR.MENTOR_GROUP_FETCH_FAILURE,
  error,
});

export const setGroupUserFulfilled = data => ({
  type: GROUP_ACTIONS_USER.USER_GROUP_FETCH_SUCCESS,
  payload: data,
});

export const setGroupDeleted = () => ({
  type: GROUP_ACTIONS_USER.USER_GROUP_DELETED,
});

export const setGroupUserPending = () => ({
  type: GROUP_ACTIONS_USER.USER_GROUP_FETCH,
});

export const setGroupUserRejected = error => ({
  type: GROUP_ACTIONS_USER.USER_GROUP_FETCH_FAILURE,
  error,
});

// Получение групп юзера
export const getUserGroup = () => async dispatch => {
  dispatch(setGroupUserPending());
  try {
    const data = await getGroupAsUser();
    dispatch(setGroupUserFulfilled(data.group)); // тут приходят нужные данные
  } catch (e) {
    dispatch(setGroupUserRejected(e));
  }
};

export const deleteGroupThunk = () => async dispatch => {
  try {
    await deleteGroup();
    dispatch(setGroupDeleted()); // тут приходят нужные данные
  } catch (e) {
    console.error(e);
  }
};

// Получение групп ментора
export const getMentorGroup = () => async dispatch => {
  dispatch(setGroupMentorPending());
  try {
    const data = await getGroupAsMentor();
    dispatch(setGroupMentorFulfilled(data.group)); // тут приходят нужные данные
  } catch (e) {
    dispatch(setGroupMentorRejected(e));
  }
};

// Получение превью групп
export const getPreviewGroup = groupId => async dispatch => {
  try {
    const data = await getGroupPreview(groupId);
    dispatch(setGroupUserFulfilled(data.group)); // тут приходят нужные данные
  } catch (e) {
    dispatch(setGroupUserRejected(e));
  }
};
