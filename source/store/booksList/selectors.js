import { createSelector } from 'reselect';

export const booksListReducer = state => state.booksList.data;

export const getBookListIsLoading = state => state.booksList.isLoading;

const filterTestament = num => ({ testament_id }) => +testament_id === +num;

const bookTypes = { old: 1, new: 2 };

export const getNewBooksList = createSelector(
  booksListReducer,
  i => i.filter(filterTestament(bookTypes.new)),
);

export const getOldBooksList = createSelector(
  booksListReducer,
  i => i.filter(filterTestament(bookTypes.old)),
);
