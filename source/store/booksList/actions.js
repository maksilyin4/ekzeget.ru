import R from 'ramda';
import { AXIOS } from '../../dist/ApiConfig';

export const BOOKS_LIST_FETCH = 'BOOKS_LIST_FETCH';
export const BOOKS_LIST_FETCH_SUCCESS = 'BOOKS_LIST_FETCH_SUCCESS';
export const BOOKS_LIST_FETCH_FAILURE = 'BOOKS_LIST_FETCH_FAILURE';

const getBooksFulfilled = data => ({
  type: BOOKS_LIST_FETCH_SUCCESS,
  payload: data,
});

const getBooksPending = () => ({
  type: BOOKS_LIST_FETCH,
});

const getBooksRejected = error => ({
  type: BOOKS_LIST_FETCH_FAILURE,
  error,
});

// Получение книг
export const getBooksList = () => dispatch => {
  dispatch(getBooksPending());

  return AXIOS.get(`/queeze/get-books`).then(
    response => {
      if (response.data.status === 'ok') {
        dispatch(getBooksFulfilled(R.pathOr([], ['data'], response)));
      }
    },
    error => {
      dispatch(getBooksRejected(error));
    },
  );
};
