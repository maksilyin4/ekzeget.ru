import { BOOKS_LIST_FETCH, BOOKS_LIST_FETCH_SUCCESS, BOOKS_LIST_FETCH_FAILURE } from './actions';

export default (state = { isLoading: false, data: [] }, action) => {
  switch (action.type) {
    case BOOKS_LIST_FETCH:
      return { ...state, isLoading: true };
    case BOOKS_LIST_FETCH_SUCCESS:
      return { ...state, isLoading: false, data: action.payload.books };
    case BOOKS_LIST_FETCH_FAILURE:
      return { ...state, isLoading: false, data: action.error };
    default:
      return state;
  }
};
