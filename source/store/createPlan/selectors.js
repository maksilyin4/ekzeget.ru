export const readingFormValues = state => state.readingCreatePlan.readingCreatePlan;
export const valuesToSendPlan = state => state.readingCreatePlan.readingPlanValuesToSend;
export const previewPlan = state => state.readingCreatePlan.previewPlanReducer;
export const previewPlanLoading = state => state.readingCreatePlan.getPreviewIsLoading;
