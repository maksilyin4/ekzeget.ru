import { getPreviewPlan } from '../../pages/PreviewPlan/model/getPreviewPlan';

export const CREATE_PLAN_ACTIONS = {
  SET_VALUES: 'SET_VALUES',
  SET_VALUES_SEND: 'SET_VALUES_SEND',
  RESET_FORM: 'RESET_FORM',
  PLAN_FETCH_FAILURE: 'PLAN_FETCH_FAILURE',
  PLAN_FETCH: 'PLAN_FETCH',
  PLAN_FETCH_SUCCESS: 'PLAN_FETCH_SUCCESS',
};

export const setFormValues = data => ({
  type: CREATE_PLAN_ACTIONS.SET_VALUES,
  payload: data,
});

export const setFormValuesToSend = data => ({
  type: CREATE_PLAN_ACTIONS.SET_VALUES_SEND,
  payload: data,
});

export const resetFormValues = () => ({
  type: CREATE_PLAN_ACTIONS.RESET_FORM,
});

export const setPreviewFulfilled = data => ({
  type: CREATE_PLAN_ACTIONS.PLAN_FETCH_SUCCESS,
  payload: data,
});

export const setPlanPending = () => ({
  type: CREATE_PLAN_ACTIONS.PLAN_FETCH,
});

export const setPlanRejected = error => ({
  type: CREATE_PLAN_ACTIONS.PLAN_FETCH_FAILURE,
  error,
});

// Получение превью плана
export const getPreviewPlanThunk = plan => async dispatch => {
  dispatch(setPlanPending());
  try {
    const data = await getPreviewPlan(plan);
    dispatch(setPreviewFulfilled(data.data)); // тут приходят нужные данные
  } catch (e) {
    dispatch(setPlanRejected(e));
  }
};
