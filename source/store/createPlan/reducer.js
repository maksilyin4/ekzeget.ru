import { combineReducers } from 'redux';
import { CREATE_PLAN_ACTIONS } from './actions';

export const defaultState = {
  description: '',
  name: '',
  mentor: '',
  week_days: [],
  start_date: '',
  end_date: '',
  all_chapters: '',
  new_chapters: 0,
  parallel_new_chapters: 1,
  old_chapters: 0,
  parallel_old_chapters: 1,
  new_books: {
    branch1: [],
    branch2: [],
    branch3: [],
  },
  old_books: {
    branch1: [],
    branch2: [],
    branch3: [],
  },
};

export const defaultStateForm = {
  description: '',
  lenght: 0,
  comment: '',
  plan: [],
};

const readingCreatePlan = (state = defaultState, action) => {
  switch (action.type) {
    case CREATE_PLAN_ACTIONS.SET_VALUES:
      return action.payload;
    case CREATE_PLAN_ACTIONS.RESET_FORM: {
      return defaultState;
    }
    default:
      return state;
  }
};

const getPreviewIsLoading = (state = false, action) => {
  switch (action.type) {
    case CREATE_PLAN_ACTIONS.PLAN_FETCH:
      return true;
    case CREATE_PLAN_ACTIONS.PLAN_FETCH_FAILURE:
      return false;
    case CREATE_PLAN_ACTIONS.PLAN_FETCH_SUCCESS:
      return false;
    default:
      return state;
  }
};

const previewPlanReducer = (state = {}, action) => {
  switch (action.type) {
    case CREATE_PLAN_ACTIONS.PLAN_FETCH_FAILURE:
      return {};
    case CREATE_PLAN_ACTIONS.PLAN_FETCH_SUCCESS:
      return action.payload;
    default:
      return state;
  }
};

const readingPlanValuesToSend = (state = defaultStateForm, action) => {
  switch (action.type) {
    case CREATE_PLAN_ACTIONS.SET_VALUES_SEND:
      return action.payload;
    default:
      return state;
  }
};

export default combineReducers({
  readingCreatePlan,
  getPreviewIsLoading,
  previewPlanReducer,
  readingPlanValuesToSend,
});
