export const getIsLoadingInter = store => store.interpretation.isLoading;
export const getInterpretation = state => {
  if (state.interpretation.data.length) {
    return [...state.interpretation.data].sort((one, two) =>
      one?.ekzeget.name.localeCompare(two?.ekzeget.name),
    );
  }
};
