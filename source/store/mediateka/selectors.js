import { createSelector } from 'reselect';

export const mediatekaDownloads = store => store.mediateka.mediatekaDownloads.data;

export const getMediatekaBreadcrumbs = store => store.mediateka.mediatekaBreadcrumbs.breadcrumbs;

export const getMediatekaDownloadMediaId = mediaId =>
  createSelector(mediatekaDownloads, i => {
    const { read_cnt, listen_cnt } = i[mediaId]?.downloads || {};
    return { book: read_cnt, listen: listen_cnt };
  });

export const getMediatekaStatisticMediaId = mediaId =>
  createSelector(mediatekaDownloads, i => {
    const { view_count } = i[mediaId]?.views || {};
    return { viewCount: view_count || 0 };
  });
