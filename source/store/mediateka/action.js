import { _AXIOS } from '../../dist/ApiConfig';

export const MEDIATEKA_BREADCRUMBS = 'MEDIATEKA_BREADCRUMBS';
export const MEDIATEKA_CLEAR_BREADCRUMBS = 'MEDIATEKA_CLEAR_BREADCRUMBS';

export const MEDIATEKA_STATISTICS_VIEWS_COUNT_FETCH = 'MEDIATEKA_STATISTICS_VIEWS_COUNT_FETCH';
export const MEDIATEKA_DOWNLOADS_STATISTICS_COUNT_SUCCESS =
  'MEDIATEKA_DOWNLOADS_STATISTICS_COUNT_SUCCESS';
export const MEDIATEKA_STATISTICS_VIEWS_COUNT_FAILURE = 'MEDIATEKA_STATISTICS_VIEWS_COUNT_FAILURE';
export const MEDIATEKA_STATISTICS_VIEWS_SUCCESS = 'MEDIATEKA_STATISTICS_VIEWS_SUCCESS';

export const clearBread = () => ({
  type: MEDIATEKA_CLEAR_BREADCRUMBS,
});
export const setMediatekaBreadcrumbs = payload => ({
  type: MEDIATEKA_BREADCRUMBS,
  payload,
});

export const getMediatekaDownloadsSuccess = ({ data, id }) => ({
  type: MEDIATEKA_DOWNLOADS_STATISTICS_COUNT_SUCCESS,
  payload: {
    data,
    id,
  },
});
export const getMediaViewsSuccess = ({ data, id }) => ({
  type: MEDIATEKA_STATISTICS_VIEWS_SUCCESS,
  payload: {
    data,
    id,
  },
});

export const getMediatekaFetch = () => ({
  type: MEDIATEKA_STATISTICS_VIEWS_COUNT_FETCH,
});

export const getMediatekaFailure = ({ error, id }) => ({
  type: MEDIATEKA_STATISTICS_VIEWS_COUNT_FAILURE,
  payload: {
    error,
    id,
  },
});

export const getMediatekaDownloadsIncrease = (id, type) => dispatch => {
  dispatch(getMediatekaFetch());

  return _AXIOS({
    url: '/media/statistics-downloads-increase/',
    query: {
      mediaId: id,
      type,
    },
  })
    .then(data => dispatch(getMediatekaDownloadsSuccess({ data, id })))
    .catch(error => dispatch(getMediatekaFailure({ error, id })));
};

export const getMediaViews = id => dispatch => {
  dispatch(getMediatekaFetch());

  return _AXIOS({
    url: 'media/statistics-views-increase/',
    query: {
      mediaId: id,
    },
  })
    .then(data => dispatch(getMediaViewsSuccess({ data, id })))
    .catch(error => dispatch(getMediatekaFailure({ error, id })));
};

export default {
  getMediatekaDownloadsIncrease,
  getMediaViewsSuccess,
};
