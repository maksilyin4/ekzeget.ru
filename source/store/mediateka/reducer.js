import { combineReducers } from 'redux';

import {
  MEDIATEKA_STATISTICS_VIEWS_COUNT_FETCH,
  MEDIATEKA_STATISTICS_VIEWS_COUNT_FAILURE,
  MEDIATEKA_DOWNLOADS_STATISTICS_COUNT_SUCCESS,
  MEDIATEKA_STATISTICS_VIEWS_SUCCESS,
  MEDIATEKA_BREADCRUMBS,
  MEDIATEKA_CLEAR_BREADCRUMBS,
} from './action';

const mediatekaBreadcrumbs = (state = { breadcrumbs: {} }, { type, payload }) => {
  switch (type) {
    case MEDIATEKA_BREADCRUMBS: {
      return {
        ...state,
        breadcrumbs: payload,
      };
    }

    case MEDIATEKA_CLEAR_BREADCRUMBS: {
      return { ...state, breadcrumbs: { ...state.breadcrumbs, detailTitle: '' } };
    }

    default:
      return state;
  }
};

const initialState = {
  data: {},
  isLoading: false,
  error: '',
};

const mediatekaDownloads = (state = initialState, { type, payload }) => {
  switch (type) {
    case MEDIATEKA_STATISTICS_VIEWS_COUNT_FETCH:
      return { ...state, isLoading: true, error: '' };
    case MEDIATEKA_DOWNLOADS_STATISTICS_COUNT_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          [payload.id]: { ...state.data[payload.id], downloads: payload.data },
        },
        isLoading: false,
        error: '',
      };

    case MEDIATEKA_STATISTICS_VIEWS_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          [payload.id]: {
            ...state.data[payload.id],
            views: payload.data,
          },
        },

        isLoading: false,
        error: '',
      };
    case MEDIATEKA_STATISTICS_VIEWS_COUNT_FAILURE:
      return { ...state, isLoading: false, error: payload.error };

    default:
      return state;
  }
};

const reducer = combineReducers({
  mediatekaBreadcrumbs,
  mediatekaDownloads,
});

export default reducer;
