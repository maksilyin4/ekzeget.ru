export function updatePathname(url) {
  if (url) {
    return url.endsWith('/') ? url : `${url}/`;
  }
  return '/';
}
