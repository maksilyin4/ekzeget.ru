export const setTranslate = ({ chooseTranslate, translate, cookies }) => {
  chooseTranslate(translate);
  cookies.set('translates', translate, { path: '/' });
  cookies.set('translate', translate.code, { path: '/' });
};
