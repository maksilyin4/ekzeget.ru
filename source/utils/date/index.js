import dayjs from 'dayjs';
import ruLocale from 'dayjs/locale/ru';

export function getDateFull(date) {
  return dayjs(date)
    .locale('ru', ruLocale)
    .format('DD MMMM YYYY');
}
