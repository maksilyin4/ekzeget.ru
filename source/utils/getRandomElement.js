export function getRandomElement(arr) {
  if (!Array.isArray(arr) || arr.length === 0) {
    return null; // или можно выбросить ошибку, если требуется
  }
  const randomIndex = Math.floor(Math.random() * arr.length);
  return arr[randomIndex];
}
