export default function fancyTimeFormat(time) {
  if (time && !isNaN(time)) {
    // Hours, minutes and seconds
    const hrs = Math.floor(time / 3600);
    const mins = Math.floor((time % 3600) / 60);
    const secs = Math.floor(time % 60);

    let ret = '';

    if (hrs > 0) {
      ret += `${hrs}:${mins < 10 ? '0' : ''}`;
    }
    ret += `${mins}:${secs < 10 ? '0' : ''}`;
    ret += `${secs}`;
    return ret;
  }
  return '0:00';
}

const setTime = (count, time, length = 1) => +time[count - length] || 0;

export const setTimeFormat = (time = '0:00') => {
  const isCheckFormatForParser = isNaN(+time); // если true - строка в формате 00:00:00 || 00:00.00 || 00.00.00 , иначе пришли секунды в формате 000.000
  if (isCheckFormatForParser) {
    const timeArr = time.split(/\D/);

    const count = timeArr.length;

    const sec = setTime(count, timeArr);
    const mins = setTime(count, timeArr, 2);
    const hrs = setTime(count, timeArr, 3);

    return sec + Math.floor(mins * 60) + Math.floor(hrs * 3600);
  }
  return +time || 0;
};
