import { setSearch, queryFilter } from '~utils/common';

const setNewSearch = ({ searchQuery, name, stateStr, inx }) => {
  const newSearch = searchQuery.split('&');
  newSearch.splice(inx, 1, `${name}=${stateStr}`);
  return newSearch.join('&');
};

const setQuery = ({ searchNew, name, stateStr }) =>
  searchNew.length ? `${searchNew.includes('?') ? '' : '?'}${searchNew}` : `?${name}=${stateStr}`;

export const addQueryUrl = ({
  search,
  history,
  name = queryFilter.tags,
  state,
  stateHistory = '',
}) => {
  const searchQuery = search;
  const searchUrl = setSearch(searchQuery);
  // получаем название всех квери
  const stateStr = Array.isArray(state) ? state.filter(el => el).join(',') : state;

  if (search) {
    const inx = searchUrl.findIndex(el => el === name);

    if (inx !== -1) {
      const searchNew = setNewSearch({ searchQuery, name, stateStr, inx }); // заменяем существующий query на новый
      const query = setQuery({ searchNew, name, stateStr });
      history.replace(query, stateHistory);
    } else {
      history.replace(`${search}&${name}=${stateStr}`, stateHistory);
    }
  } else {
    history.replace(`?${name}=${stateStr}`, stateHistory);
  }
};
