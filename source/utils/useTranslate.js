import { useCookies } from 'react-cookie';
import { useDispatch } from 'react-redux';
import { chooseTranslate } from '~dist/actions/translates';

export const useTranslate = () => {
  const dispatch = useDispatch();

  const [, setCookies] = useCookies(['translates', 'translate']);

  const func = translate => {
    setCookies('translates', translate, { path: '/' });
    setCookies('translate', translate.code, { path: '/' });
    dispatch(chooseTranslate(translate));
  };

  return func;
};
