import { setSearch } from '~utils/common';

const setSearchDelete = (searchQuery, inx) => {
  const newSearch = searchQuery.split('&');
  newSearch.splice(inx, 1);
  return newSearch;
};

const setNewQuery = searchDelete =>
  searchDelete.length > 1 ? searchDelete.join('&') : searchDelete.join('');

export const deleteQueryUrl = ({ search, history, name = 'tags' }) => {
  const searchQuery = search;
  const searchUrl = setSearch(searchQuery);

  const inx = searchUrl.findIndex(el => el === name);

  if (inx !== -1) {
    const searchDelete = setSearchDelete(searchQuery, inx);
    const newQuery = setNewQuery(searchDelete);
    const query = newQuery.includes('?') ? '' : '?';

    history.replace(`${query}${newQuery}`);
  }
};
