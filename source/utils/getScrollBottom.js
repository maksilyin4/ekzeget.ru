export const getScrollBottom = currentTarget => {
  return (
    Math.round(currentTarget.clientHeight + currentTarget.scrollTop) >= currentTarget.scrollHeight
  );
};
