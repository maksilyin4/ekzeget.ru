import { getScrollBottom } from './getScrollBottom';

const defaultTranslate = {
  id: 1,
  number: 1,
  code: 'st_text',
  title: 'Синодальный перевод',
  select: 'Синодальный перевод',
  search: 'Синодальному переводу',
  value: 1,
  label: 'Синодальный перевод',
};

const bibleCDN = '//9c7f4e2b-86a9-4d73-b1db-6452d1399a9d.selcdn.net/';

const prefixForDetailCode = 'detail-';
const defaultUrlForBible = '/bible/evangelie-ot-matfea/glava-1/';

const bibleyskayaViktorina = 'bibleyskaya-viktorina';
const bibleyskayaViktorinaVoprosi = `/${bibleyskayaViktorina}/voprosi/`;
const voprosi = 'voprosi';
const bible = 'bible';
const mediateka = 'mediateka';
const allAboutBible = 'all-about-bible';
const bibleGroup = 'bible-group';

const linkOldSite = 'https://site.ekzeget.ru/';

const queryFilter = {
  tags: 'tags',
  book: 'book',
  chapter: 'chapter',
  verse: 'verse',
  author: 'author',
  cat: 'cat',
  sort: 'sort',
  motivator: 'motivator',
  play: 'play',
  online: 'online',
  search: 'search',
  glava: 'glava',
  authorsCategory: 'authorsCategory',
  translate: 'translate',
  preacher: 'preacher',
  ekzeget: 'ekzeget',
  letter: 'letter',
};

const searchUrl = {
  bible: '/search/bible',
  preachers: '/search/propovedi',
  interpretations: '/search/interpretations',
  quiz: '/search/viktorina',
};

const sortsDefault = { sort: 'alph', sortDirection: false };

const getQueryUrl = ({ search = '', tag = '' }) => {
  if (search) {
    return new URLSearchParams(search).get(tag);
  }
  return search;
};

const defaultHeightContentBible = 1686;

const setStyleHeight = ({ height = 0, defaultHeight = 0, styleName = 'height' }) => {
  return +height ? { [styleName]: `${+height - defaultHeight}px` } : {};
};

const deleteSearchUrl = ({ search, name = '' }) => {
  const searchUrl = search.split('&');
  const inx = searchUrl.findIndex(el => el.includes(name));
  if (inx !== -1) {
    searchUrl.splice(inx, 1);
    let searchNew = '';

    if (searchUrl.length > 1) {
      searchNew = searchUrl.join('&');
    } else {
      searchNew = searchUrl.join('');
    }

    const query = searchNew.includes('?') ? '' : '?';
    return `${query}${searchNew}`;
  }
  return search;
};

const setSearch = (search = '') => {
  return search
    .replace('?', '')
    .split('&')
    .map(el => el.split('=')[0]);
};

function fetchLoadScroll({
  event: { currentTarget },
  data,
  fetchMore,
  variables,
  setName = 'medias',
}) {
  const scrollBottom = getScrollBottom(currentTarget);

  if (scrollBottom) {
    fetchMore({
      variables: { ...variables, offset: data.length, limit: 10 },
      updateQuery: (prev, { fetchMoreResult }) => {
        if (!fetchMoreResult) return prev;
        return {
          ...prev,
          [setName]: {
            ...prev[setName],
            items: [...prev[setName].items, ...fetchMoreResult[setName].items],
          },
        };
      },
    });
  }
}

function fetchInfiniteScroll({ data, fetchMore, variables, setName = 'medias' }) {
  fetchMore({
    variables: { ...variables, offset: data.length, limit: 10 },
    updateQuery: (prev, { fetchMoreResult }) => {
      if (!fetchMoreResult) return prev;
      return {
        ...prev,
        [setName]: {
          ...prev[setName],
          items: [...prev[setName].items, ...fetchMoreResult[setName].items],
        },
      };
    },
  });
}

const setItemsCheckbox = ({ stateItems, key, item }) => {
  if (item) {
    const itemsArr = [...stateItems];
    const ind = itemsArr.findIndex(items => JSON.stringify(items).includes(key));

    if (ind !== -1) {
      itemsArr.splice(ind, 1);
    }

    return ind === -1 ? [...stateItems, item] : itemsArr;
  }
  return stateItems;
};

const changeQuery = ({ search, history, queryFilters = [], setName = 'nameQuery', value }) => {
  const searchArr = search.split('&');
  const newSearch = searchArr
    .filter(query => {
      if (
        queryFilters.filter(qf => qf).some(el => el.includes(query.split('=')[0].replace('?', '')))
      ) {
        return query;
      }
    })
    .map(el => el.replace('?', ''))
    .join('&');

  history.replace(`?${newSearch}&${setName}=${value}`);
};

export {
  changeQuery,
  defaultUrlForBible,
  bibleyskayaViktorina,
  bibleyskayaViktorinaVoprosi,
  voprosi,
  bible,
  mediateka,
  bibleGroup,
  allAboutBible,
  defaultHeightContentBible,
  queryFilter,
  searchUrl,
  linkOldSite,
  fetchLoadScroll,
  fetchInfiniteScroll,
  setSearch,
  deleteSearchUrl,
  setStyleHeight,
  getQueryUrl,
  defaultTranslate,
  setItemsCheckbox,
  sortsDefault,
  prefixForDetailCode,
  bibleCDN,
};
