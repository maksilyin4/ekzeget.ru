export const parsePathname = ({ pathname = '', index = 0, separator = '/', defaultReturn = 0 }) => {
  if (pathname) {
    const value = pathname.split(separator).filter(el => el)[index];
    return value || defaultReturn;
  }
};
