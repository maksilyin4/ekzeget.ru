import crypto from 'crypto';

const algorithm = 'aes-256-cbc';
const secret = Buffer.concat([
  Buffer.from('MySecretKey123MySecretKey123'),
  Buffer.alloc(32 - 'MySecretKey123MySecretKey123'.length),
]);

export const encrypt = number => {
  const iv = crypto.randomBytes(16); // Инициализирующий вектор
  const cipher = crypto.createCipheriv(algorithm, Buffer.from(secret), iv);
  let encrypted = cipher.update(number.toString(), 'utf8', 'hex');
  encrypted += cipher.final('hex');
  return { iv: iv.toString('hex'), encryptedData: encrypted };
};

export const decrypt = (iv, encryptedData) => {
  const decipher = crypto.createDecipheriv(algorithm, Buffer.from(secret), Buffer.from(iv, 'hex'));
  let decrypted = decipher.update(encryptedData, 'hex', 'utf8');
  decrypted += decipher.final('utf8');
  return parseInt(decrypted, 10);
};
