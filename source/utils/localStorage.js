import { LS } from '~dist/browserUtils';

const getLocalStorage = (name = '') => {
  const ls = LS.get(name);
  return JSON.parse(ls);
};

const setLocalStorage = (name = '', value = '') => {
  LS.set(name, JSON.stringify(value));
};

export { getLocalStorage, setLocalStorage };
