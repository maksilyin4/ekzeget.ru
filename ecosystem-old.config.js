module.exports = {
  apps: [
    {
      name: 'ssr-server-old',
      script: 'server/bin/prod.js',
      env: {
        NODE_ENV: 'production',
        URL_BASE: 'http://localhost:8080/api/v1/2',
      },
      env_dev: {
        NODE_PORT: 3000,
        NODE_ENV: 'production',
        URL_BASE: 'https://ekzeget.zetest.site/api/v1',
        URL_APOLLO: 'https://ekzeget.zetest.site',
      },
      env_preProd: {
        NODE_PORT: 3001,
        NODE_ENV: 'production',
        URL_BASE: 'https://ekzeget-prod.zetest.site/api/v1',
        URL_APOLLO: 'https://ekzeget-prod.zetest.site',
      },
      env_prod: {
        NODE_PORT: 3001,
        NODE_ENV: 'production',
        URL_BASE: 'https://prod.ekzeget.ru/api/v1',
        URL_APOLLO: 'https://prod.ekzeget.ru',
      },
      watch: false,
      log_date_format: 'DD-MM-YYYY HH:mm:ss',
    },
  ],
};
