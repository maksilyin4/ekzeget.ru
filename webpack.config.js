module.exports = env =>
  // eslint-disable-next-line import/no-dynamic-require
  require(`./config/webpack.config.${env.file || 'development'}.js`)(env.folder);
