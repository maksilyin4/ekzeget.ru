import { createHttpLink } from 'apollo-link-http';
import fetch from 'node-fetch';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { URL_APOLLO } from '../../config';

export const urlApolloSSR =
  process.env.NODE_ENV === 'development' ? 'http://localhost:3000' : URL_APOLLO;

const httpLink = createHttpLink({
  uri: `${urlApolloSSR}/api/v2/graphql`,
  useGETForQueries: true,
  fetch,
});

export const fetchClient = () => {
  return new ApolloClient({
    ssrMode: true,
    cache: new InMemoryCache().restore((process.env.BROWSER && window.__APOLLO_STATE__) || {}),
    link: httpLink,
    // defaultOptions: {
    //   watchQuery: {
    //     fetchPolicy: 'no-cache',
    //     errorPolicy: 'ignore',
    //   },
    //   query: {
    //     fetchPolicy: 'no-cache',
    //     errorPolicy: 'all',
    //   },
    // },
  });
};
