/* eslint-disable object-curly-newline */
import React from 'react';
import { renderToString } from 'react-dom/server';

import Router from 'koa-router';
import ReduxThunk from 'redux-thunk';
import pretty from 'pretty';
import rootReducer from '../../source/dist/reducers/RootReducer';
import { getStore } from '../../source/getStore';
import { beforeRoot, afterRoot } from '../template';
import frontFixStore from '../libs/frontFixStore';
import { urlApolloSSR } from '../routes/mediateka';
import { STORAGE_CDN } from '../config';
import { metaHomeAtom } from '../../source/pages/Home/page-head-meta/model';

const router = new Router();

router.get('*', async ctx => {
  const {
    meta = [],
    title = 'Экзегет.ру',
    description = '',
    initialStore = {},
    directProps = {},
    canonical = '',
    client,
  } = ctx.locals || {};

  const middlewares = [ReduxThunk];
  const checkTitle = ctx.locals.title;

  if (checkTitle === undefined) {
    console.log('error, 404');
    //ctx.status = 404;
  } else {
    console.log('success, 200');
    ctx.status = 200;
  }

  const cookies = ctx.request.universalCookies;

  const store = getStore(rootReducer, initialStore, middlewares);
  const getApp = require('../../build-dev/app.ssr').default;

  const App = getApp(ctx.request.url, store, directProps, client, cookies);

  const element = React.createElement(App);

  const content = renderToString(element);

  ctx.body = pretty(
    beforeRoot(
      // проверяем, приходят ли меты с шаблонов из админки
      store.getState()?.metaData?.meta && Object.keys(store.getState().metaData.meta).length
        ? //    если да, то обрабатываем как объет и берем из store
          store.getState().metaData
        : // если нет, то берем массив meta
          meta,
      title,
      canonical,
      ctx.url,
      description,
    ) +
      content +
      afterRoot(
        frontFixStore(store.getState()),
        directProps,
        urlApolloSSR,
        client?.extract() || {},
        STORAGE_CDN,
        {
          metaHomeAtom: metaHomeAtom.getState(),
        },
      ),
    { ocd: true },
  );
});

export default router;
