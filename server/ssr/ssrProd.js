import React from 'react';
import { renderToNodeStream } from 'react-dom/server';

import Router from 'koa-router';
import ReduxThunk from 'redux-thunk';
import fs from 'fs';
import 'colors';
import rootReducer from '../../source/dist/reducers/RootReducer';
import { getStore } from '../../source/getStore';
import { beforeRoot, afterRoot } from '../template';
import frontFixStore from '../libs/frontFixStore';

import { urlApolloSSR } from './apolloSetupSSR';
import { STORAGE_CDN } from '../config';
import { metaHomeAtom } from '../../source/pages/Home/page-head-meta/model';

const router = new Router();

const pipe = (from, to, options) =>
  new Promise((resolve, reject) => {
    from.pipe(to, options);
    from.on('error', reject);
    from.on('end', resolve);
  });

router.get('*', async ctx => {
  const {
    meta = [],
    title = 'Экзегет.ру',
    description = '',
    initialStore = {},
    directProps = {},
    canonical = '',
    client,
  } = ctx.locals || {};
  const middlewares = [ReduxThunk];
  const checkTitle = ctx.locals.title;

  const store = getStore(rootReducer, initialStore, middlewares);

  ctx.set('Content-Type', 'text/html; charset=utf-8');

  if (checkTitle === undefined) {
    console.log('error, 404');
    //ctx.status = 404;
  } else {
    console.log('success, 200');
    ctx.status = 200;
  }

  ctx.flushHeaders();
  // старая реализация
  // ctx.res.write(beforeRoot(meta, title, canonical, ctx.url));

  // новая реализация
  ctx.res.write(
    beforeRoot(
      // проверяем, приходят ли меты с шаблонов из админки
      store.getState()?.metaData?.meta && Object.keys(store.getState().metaData.meta).length
        ? //    если да, то обрабатываем как объет и берем из store
          store.getState().metaData
        : // если нет, то берем массив meta
          meta,
      title,
      canonical,
      ctx.url,
      description,
    ),
  );

  if (!fs.existsSync('build/server/app.ssr.js')) {
    console.log(
      ' ╔══════════════════════════════════════════════════════════════════════════════════╗ '.white
        .bgRed,
    );
    console.log(
      ' ║ Need "npm run build" before start SSR server, build/server/app.ssr.js not found! ║ '.white
        .bgRed,
    );
    console.log(
      ' ╚══════════════════════════════════════════════════════════════════════════════════╝ '.white
        .bgRed,
    );
    process.exit(0);
  }

  const cookies = ctx.request.universalCookies;

  const getApp = require('../../build/server/app.ssr').default;
  const App = getApp(ctx.request.url, store, directProps, client, cookies);

  try {
    const element = React.createElement(App);
    const stream = renderToNodeStream(element);
    await pipe(stream, ctx.res, { end: false });
  } catch (error) {
    console.error(error);
  }

  console.log('STORAGE_CDN', STORAGE_CDN);

  ctx.res.write(
    afterRoot(
      frontFixStore(store.getState()),
      directProps,
      urlApolloSSR,
      client?.extract() || {},
      STORAGE_CDN,
      {
        metaHomeAtom: metaHomeAtom.getState(),
      },
    ),
  );
  ctx.res.end();
});

export default router;
