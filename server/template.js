import fs from 'fs';
import Handlebars from 'handlebars';
import { NODE_ENV } from './config';

if (NODE_ENV === 'production' && !fs.existsSync('build/client/index.html')) {
  console.log(
    ' ╔══════════════════════════════════════════════════════════════════════════════════╗ '.white
      .bgRed,
  );
  console.log(
    ' ║ Need "npm run build" before start SSR server, build/client/index.html not found! ║ '.white
      .bgRed,
  );
  console.log(
    ' ╚══════════════════════════════════════════════════════════════════════════════════╝ '.white
      .bgRed,
  );
  process.exit(0);
}
const metaToHTMLTag = meta => {
  return meta?.meta?.description
    ? // логика для отрисовки меты, которая пришла с шаблонов (объект)
      `<meta
            name="description"
            content="${Handlebars.compile(meta?.meta?.description || '')(meta?.metaContext)}"
        />
        <meta
            name="keywords"
            content="${Handlebars.compile(meta?.meta?.keywords || '')(meta?.metaContext)}"
        />`
    : Array.isArray(meta) && meta.length
    ? // логика для отрисовки меты, которые пришли из "список" из админки (массив) - (легаси)
      // если в файлах ./routes не установили meta в initialStore
      meta.map(({ name, content }) => `<meta name="${name}" content="${content}">\n`).join('')
    : `<meta
                name="description"
                content="Проект Экзегет - онлайн Библия"
            />
            <meta
                name="keywords"
                content="Библия, проект, Экзегет"
            />`;
};

const metaToTitle = meta => {
  return meta?.meta?.title
    ? // логика для отрисовки title, который пришел с шаблонов (объект)
      `${Handlebars.compile(meta?.meta?.title || '')(meta?.metaContext)}`
    : `Экзегет - Библия онлайн`;
};

function showTemplate(tpl, ctx) {
  return Handlebars.compile(tpl)(ctx);
}

const html =
  NODE_ENV === 'production'
    ? fs.readFileSync('build/client/index.html', { encoding: 'utf-8' })
    : fs.readFileSync('source/index.html', { encoding: 'utf-8' });

const [htmlBeforeRoot, htmlAfterRoot] = html.split('<ssr:root />');

export const beforeRoot = (meta, title, canonical, url, description) => {
  return htmlBeforeRoot
    .replace(/<ssr:meta \/>/, metaToHTMLTag(meta))
    .replace(/<ssr:title \/>/, metaToTitle(meta))
    .replace(
      /<ssr:ogTitle \/>/,
      title.length > 0
        ? `<meta property="og:title" content='${showTemplate(
            meta?.meta?.title || '',
            meta?.metaContext,
          )}'/>\n`
        : '',
    )
    .replace(
      /<ssr:ogDesc \/>/,
      description
        ? `<meta property="og:description" content='${showTemplate(
            meta?.meta?.description || '',
            meta?.metaContext,
          )}'/>`
        : '',
    )
    .replace(
      /<ssr:ogUrl \/>/,
      url.length > 0 ? `<meta property="og:url" content='https://ekzeget.ru${url}'/>` : ``,
    )
    .replace(
      /<ssr:canonical \/>/,
      canonical.length > 0 ? `<link rel="canonical" href="${canonical}">\n` : '',
    );
};

export const afterRoot = (
  initialState,
  directProps,
  URL_APOLLO,
  client,
  STORAGE_CDN,
  storeReatom,
) => {
  return htmlAfterRoot
    .replace(
      /<ssr:store \/>/,
      NODE_ENV === 'production'
        ? `<script type="text/javascript">
          window.REDUX_INITIAL_STORE = ${JSON.stringify(initialState)};
          window.DIRECT_PROPS = ${JSON.stringify(directProps)};
          window.URL_APOLLO = ${JSON.stringify(URL_APOLLO)};
          window.STORAGE_CDN = ${JSON.stringify(STORAGE_CDN)};
          window.__APOLLO_STATE__= ${JSON.stringify(client)};
          window.INIT_STATE_REATOM= ${JSON.stringify(storeReatom)};
       </script>`
        : `<script type="text/javascript">
          window.REDUX_INITIAL_STORE = ${JSON.stringify(initialState, null, 2)};
          window.DIRECT_PROPS = ${JSON.stringify(directProps, null, 2)};
          window.URL_APOLLO = ${JSON.stringify(URL_APOLLO)};
          window.STORAGE_CDN = ${JSON.stringify(STORAGE_CDN)};
          window.__APOLLO_STATE__= ${JSON.stringify(client, null, 2)};
          window.INIT_STATE_REATOM= ${JSON.stringify(storeReatom)};
       </script>`,
    )
    .replace(
      /<ssr:script \/>/,
      NODE_ENV === 'production'
        ? ''
        : '<script async type="text/javascript" src="/frontassets/js/vendor.chunk.js"></script><script async type="text/javascript" src="/frontassets/js/app-min.js"></script> ',
    );
};
