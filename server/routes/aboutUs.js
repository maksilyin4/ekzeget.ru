import R from 'ramda';
import Router from 'koa-router';
import 'colors';
import { getArticleDetail, getMeta, getTranslates } from '../libs/getData';
import { setInitialTranslates } from './utils';

const router = new Router();

router.get('/o-proekte/:id', async (ctx, next) => {
  let initialStore = {};
  let directProps = {};
  const { meta } = ctx.locals;
  const { id } = ctx.params;
  const canonical = `https://ekzeget.ru/o-proekte/${id}/`;

  let metaData = {};

  try {
    const [_content, _meta, _translates] = await Promise.all([
      getArticleDetail(id),
      // getMeta('o-proekte/about'),
      getMeta(ctx.url),
      getTranslates(),
    ]);

    metaData = R.pathOr({}, ['data', 'meta'], _meta);

    initialStore = {
      metaData: {
        metaContext: {},
        meta: metaData,
        isLoadingMeta: false,
      },
      translates: setInitialTranslates(_translates),
    };

    directProps = {
      [id]: {
        content: R.pathOr('', ['data', 'article', 'body'], _content),
        title: R.pathOr('', ['data', 'article', 'title'], _content),
      },
    };
  } catch (error) {
    console.error(error.message.red);
  }

  const { title, description } = metaData;

  meta.push({
    name: 'description',
    content: description,
  });

  ctx.locals = {
    initialStore,
    meta,
    directProps,
    title,
    canonical
  };
  await next();
});

export default router;
