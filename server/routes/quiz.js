import R from 'ramda';
import Router from 'koa-router';
import 'colors';
import { getAllQuestions, getQuiz, getTestament, getMeta } from '../libs/getData';
import {
  DESC_QUIZ,
  DESC_QUIZ_RESULTS,
  TITLE_QUIZ,
  TITLE_QUIZ_RESULTS,
} from '../../source/pages/Quiz/constants';
import { DEFAULT_TEXT_QUIZ } from '../../source/dist/utils';

const router = new Router();

router
  .get('/bibleyskaya-viktorina/', async (ctx, next) => {
    let initialStore = {};
    const { meta, headers } = ctx.locals;
    const title = TITLE_QUIZ;

    try {
      const _testament = await getTestament(headers);
      const _meta = await getMeta(ctx.url);

      initialStore = {
        books: {
          testamentIsLoading: false,
          testament: {
            'testament-1': R.pathOr([], ['data', 'testament-1'], _testament),
            'testament-2': R.pathOr([], ['data', 'testament-2'], _testament),
          },
        },
        metaData: {
          metaContext: {},
          meta: _meta.data.meta,
          isLoadingMeta: false,
        },
      };
    } catch (error) {
      console.log('quiz router', error.message.red);
    }

    meta.push({
      name: 'description',
      content: DESC_QUIZ,
    });

    ctx.locals = { initialStore, title, meta };
    await next();
  })
  .get('/bibleyskaya-viktorina/voprosi/:quiz_code/', async (ctx, next) => {
    let initialStore = {};
    const { meta } = ctx.locals;
    let title = TITLE_QUIZ;
    let description = DESC_QUIZ;

    try {
      let _quiz = await getQuiz(ctx.params.quiz_code);
      const _meta = await getMeta(ctx.url);

      _quiz = R.pathOr({}, ['data'], _quiz);
      const quizContext = _quiz?.question;

      initialStore = {
        quiz: {
          isLoadingQueeze: true,
          queezeData: [],
          isLoadingList: true,
          completedQueezeData: [],
          isLoadedCurrentQuestion: true,
          currentQuestionData: _quiz,
        },
        metaData: {
          metaContext: { quizContext },
          meta: _meta.data.meta,
          isLoadingMeta: false,
        },
      };

      const quizTitle = R.pathOr(null, ['question', 'title'], _quiz);

      if (quizTitle) {
        title = `${quizTitle} - Библейская викторина Экзегета`;
        description = `Библейская викторина Экзегета: ${quizTitle}`;
      }
    } catch (error) {
      console.log('quiz questions router', error.message.red);
    }

    meta.push({
      name: 'description',
      content: description,
    });

    ctx.locals = { initialStore, title, meta };
    await next();
  })
  .get('/bibleyskaya-viktorina/voprosi-:id/', async (ctx, next) => {
    let initialStore = {};
    const { meta } = ctx.locals;
    let title = DEFAULT_TEXT_QUIZ;
    let content = DEFAULT_TEXT_QUIZ;

    try {
      const _questionsData = await getAllQuestions(ctx.params.id);
      const _meta = await getMeta(ctx.url);
      const totalCount = R.pathOr(0, ['data', 'totalCount'], _questionsData);
      const questions = R.pathOr([], ['data', 'questions'], _questionsData);

      initialStore = {
        quiz: {
          isLoadingQueeze: true,
          queezeData: [],
          isLoadingList: true,
          completedQueezeData: [],
          isLoadedCurrentQuestion: true,
          currentQuestionData: [],
          questionsData: {
            totalCount,
            questions,
          },
          isLoadedQuestions: true,
          errorAllQuestions: {},
        },
        metaData: {
          metaContext: { questions },
          meta: _meta.data.meta,
          isLoadingMeta: false,
        },
      };

      title = `${title} - все вопросы викторины (страница ${ctx.params.id})`;
      content = `${content} - смотреть все вопросы викторины (страница ${ctx.params.id})`;
    } catch (error) {
      console.log('quiz all questions router', error.message.red);
    }

    meta.push({
      name: 'description',
      content,
    });

    ctx.locals = { initialStore, title, meta };
    await next();
  })
  .get('/bibleyskaya-viktorina/resultati/', async (ctx, next) => {
    let initialStore = {};
    const { meta } = ctx.locals;
    const title = TITLE_QUIZ_RESULTS;

    meta.push({
      name: 'description',
      content: DESC_QUIZ_RESULTS,
    });

    try {
      const _meta = await getMeta(ctx.url);

      initialStore = {
        metaData: {
          metaContext: {},
          meta: _meta.data.meta,
          isLoadingMeta: false,
        },
      };
    } catch (error) {
      console.log('quiz all questions router', error.message.red);
    }

    ctx.locals = { initialStore, title };
    await next();
  });

export default router;
