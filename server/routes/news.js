import R from 'ramda';
import Router from 'koa-router';
import 'colors';
import { getArticle, getTranslates, getArticleDetail, getMeta } from '../libs/getData';
import { setInitialTranslates } from './utils';

const router = new Router();

router
  .get('/novosti-i-obnovleniya/', async (ctx, next) => {
    let initialStore = {};
    let directProps = {};
    const { meta } = ctx.locals;
    let metaData = {};

    try {
      const [_articles, _meta, _translates] = await Promise.all([
        getArticle({}, 1),
        getMeta(ctx.url),
        getTranslates(),
      ]);

      metaData = R.pathOr({}, ['data', 'meta'], _meta);
      const { article, pages } = R.pathOr({}, ['data'], _articles);

      initialStore = {
        translates: setInitialTranslates(_translates),
        metaData: {
          metaContext: { article },
          meta: metaData,
          isLoadingMeta: false,
        },
      };
      directProps = { article, pages };
    } catch (error) {
      console.error(error.message.red);
    }

    const { title, description } = metaData;

    meta.push({
      name: 'description',
      content: description,
    });

    ctx.locals = {
      initialStore,
      meta,
      directProps,
      title,
    };
    await next();
  })
  .get('/novosti-i-obnovleniya/novosti/', async (ctx, next) => {
    let initialStore = {};
    let directProps = {};
    const { meta } = ctx.locals;
    let metaData = {};

    try {
      const [_articles, _meta, _translates] = await Promise.all([
        getArticle({}, 1),
        getMeta(ctx.url),
        getTranslates(),
      ]);

      metaData = R.pathOr({}, ['data', 'meta'], _meta);
      const { article, pages } = R.pathOr({}, ['data'], _articles);

      initialStore = {
        translates: setInitialTranslates(_translates),
        metaData: {
          metaContext: { article },
          meta: metaData,
          isLoadingMeta: false,
        },
      };
      directProps = { article, pages };
    } catch (error) {
      console.error(error.message.red);
    }

    const { title, description } = metaData;

    meta.push({
      name: 'description',
      content: description,
    });

    ctx.locals = {
      initialStore,
      meta,
      directProps,
      title,
    };
    await next();
  })
  .get('/novosti-i-obnovleniya/:novosti/:id/', async (ctx, next) => {
    let initialStore = {};
    let directProps = {};
    const { meta } = ctx.locals;
    let title = '';

    try {
      const [_articleDetail, _translates, _meta] = await Promise.all([
        getArticleDetail(ctx.params.id),
        getTranslates(),
        getMeta(ctx.url),
      ]);

      let metaData = {};
      metaData = R.pathOr({}, ['data', 'meta'], _meta);
      const articleDetail = R.pathOr('', ['data', 'article'], _articleDetail);

      initialStore = {
        translates: setInitialTranslates(_translates),
        metaData: {
          metaContext: { articleDetail },
          meta: metaData,
          isLoadingMeta: false,
        },
      };
      directProps = { articleDetail };
      meta.push({
        name: 'description',
        content: articleDetail.title,
      });

      title = `${articleDetail.title} Новости || Экзегет.ру Библейский портал`;
    } catch (error) {
      console.error(error.message.red);
    }

    ctx.locals = {
      initialStore,
      meta,
      directProps,
      title,
    };
    await next();
  })
  .get('/novosti-i-obnovleniya/obnovleniya', async (ctx, next) => {
    let initialStore = {};
    const { meta } = ctx.locals;
    let metaData = {};

    try {
      const [_meta] = await Promise.all([getMeta(ctx.url)]);

      metaData = R.pathOr({}, ['data', 'meta'], _meta);

      initialStore = {
        metaData: {
          metaContext: {},
          meta: metaData,
          isLoadingMeta: false,
        },
      };
    } catch (error) {
      console.error(error.message.red);
    }

    ctx.locals = {
      initialStore,
      meta,
    };
    await next();
  });

export default router;
