import R from 'ramda';

export const setInitialTranslates = translates => ({
  isLoading: true,
  data: R.pathOr([], ['data', 'translates'], translates),
});
