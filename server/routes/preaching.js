import R from 'ramda';
import Router from 'koa-router';
import 'colors';
import {
  getPreachingExcuses,
  getPreachingCelebrations,
  getPreacher,
  getPreaching,
  getMeta,
  getTranslates,
} from '../libs/getData';
import { setInitialTranslates } from './utils';

const router = new Router();

async function getSSR(ctx) {
  let initialStore = {};

  let title = '';
  let description = '';

  const { meta, headers } = ctx.locals;
  const { excuse_id, preachingId } = ctx.params;

  try {
    const preacherData = await getPreacher(excuse_id);

    const preacher = R.pathOr({}, ['data', 'preacher'], preacherData);

    const preacherID = ctx.params.preacherID
      ? ctx.params.preacherID
      : R.pathOr(0, [0, 'code'], preacher);

    const [_excuse, _celebrations, _preaching, _translates, metaData] = await Promise.all([
      getPreachingExcuses(headers),
      getPreachingCelebrations(headers),
      getPreaching(preacherID, excuse_id),
      getTranslates(),
      getMeta(ctx.url),
    ]);

    const excuses = R.pathOr([], ['data', 'excuse'], _excuse);

    const currentExcuse = excuses ? excuses.find(el => el.code === preachingId) : null;

    if (!currentExcuse) {
      throw new Error('404');
    }

    const { apostolic, excuse, gospel, pages, preaching } = R.pathOr({}, ['data'], _preaching);

    const { title: titleMeta, description: descriptionMeta, h_one, keywords } =
      metaData.data.meta || {};

    title = titleMeta || `${excuse.title} - чтение проповеди онлайн на портале Экзегет`;
    description =
      descriptionMeta ||
      `Библейская проповедь ${excuse.title}. Читать онлайн христианские проповеди на портале Экзегет.`;

    meta.push(
      {
        name: 'description',
        content: description,
      },
      {
        name: 'keywords',
        content:
          keywords ||
          h_one ||
          'Проповеди - читать православные проповеди онлайн на портале Экзегет',
      },
    );

    initialStore = {
      preaching: {
        excuses,
        celebrations: R.pathOr([], ['data', 'excuse'], _celebrations),
        preaching: {
          apostolic,
          excuse,
          gospel,
          pages,
          preaching,
        },
        preacher,
        preachingExcusesIsLoading: false,
        preachingCelebrationIsLoading: false,
        preachingIsLoading: false,
        preacherIsLoading: false,
        isPreaching: false,
      },
      preachingBible: {
        data: {
          apostolic,
          excuse,
          gospel,
          pages,
          preaching,
        },
      },
      // metaData: {
      // meta: {
      //   title,
      //   description,
      //   h_one,
      //   keywords,
      // },
      // isLoadingMeta: false,
      // },
      metaData: {
        metaContext: {
          apostolic,
          excuse,
          gospel,
          preaching,
        },
        meta: metaData?.data?.meta,
        isLoadingMeta: false,
      },

      screen: { desktop: true },
      translates: setInitialTranslates(_translates),
    };
  } catch (error) {
    initialStore = {
      preaching: {
        excuses: [],
        celebrations: [],
        preacher: [],
        isPreaching: false,
        isError: true,
      },

      metaData: {
        meta: {
          title,
          description,
        },
        isLoadingMeta: false,
      },
    };
    console.error(error.message.red);
  }
  return { initialStore, meta, title, description };
}

router
  .get('/all-about-bible/propovedi/:preachingId', async (ctx, next) => {
    let initialStore = {};
    const { meta, headers } = ctx.locals;
    const { preachingId } = ctx.params;

    let title = '';
    let description = '';

    try {
      const [_excuse, _translates, metaData] = await Promise.all([
        getPreachingExcuses(headers),
        getTranslates(),
        getMeta(ctx.url),
      ]);

      const excuses = R.pathOr([], ['data', 'excuse'], _excuse);

      const excuse = excuses ? excuses.find(el => el.code === preachingId) : null;

      if (!excuse) {
        throw new Error('404');
      }

      const excusId = excuse ? excuse.code : 'voskresnye-chteniya';

      const celebrationsGet = await getPreachingCelebrations(headers);
      const celebrations = R.pathOr([], ['data', 'excuse'], celebrationsGet);

      const preacherId = celebrations.find(({ parent }) => parent.code?.includes(excusId));

      const preachers = celebrations ? await getPreacher(preacherId?.code) : [];
      const preacher = R.pathOr([], ['data', 'preacher'], preachers);

      const { title: titleMeta, description: descriptionMeta, h_one, keywords } =
        metaData.data.meta || {};

      title =
        titleMeta || 'Проповеди - читать православные проповеди онлайн на портале Экзегет 4343';
      description =
        descriptionMeta ||
        'Христианские проповеди в текстовом варианте. Читать библейские проповеди онлайн на сайте Экзегет.';

      meta.push(
        {
          name: 'description',
          content: description,
        },
        {
          name: 'keywords',
          content:
            keywords ||
            h_one ||
            'Проповеди - читать православные проповеди онлайн на портале Экзегет',
        },
      );

      initialStore = {
        preaching: {
          excuses,
          celebrations,
          preacher,
          isPreaching: false,
        },

        // metaData: {
        //   meta: {
        //     title,
        //     description,
        //     h_one,
        //     keywords,
        //   },
        //   isLoadingMeta: false,
        // },
        metaData: {
          metaContext: {
            excuses,
            celebrations,
          },
          meta: metaData?.data?.meta,
          isLoadingMeta: false,
        },

        screen: { desktop: true },
        translates: {
          isLoading: true,
          data: R.pathOr([], ['data', 'translates'], _translates),
        },
      };
    } catch (error) {
      title = undefined;
      initialStore = {
        preaching: {
          excuses: [],
          celebrations: [],
          preacher: [],
          isPreaching: false,
          isError: true,
        },

        metaData: {
          meta: {
            title,
            description,
          },
          isLoadingMeta: false,
        },
      };
      console.error(error.message.red);
    }

    ctx.locals = { initialStore, meta, title, description };
    await next();
  })
  .get('/all-about-bible/propovedi/:preachingId/:excuse_id/', async (ctx, next) => {
    ctx.locals = await getSSR(ctx);
    await next();
  })
  .get('/all-about-bible/propovedi/:preachingId/:excuse_id/:preacherID/', async (ctx, next) => {
    ctx.locals = await getSSR(ctx);
    await next();
  })
  .get('/all-about-bible/propovedi/:preachingId/:excuse_id/:preacherID/:id/', async (ctx, next) => {
    ctx.locals = await getSSR(ctx);
    await next();
  });

export default router;
