import R from 'ramda';
import Router from 'koa-router';
import 'colors';
import {
  getDictionaries,
  getDictionariesWords,
  getMeta,
  getTranslates,
  getDictionaryDetail,
} from '../libs/getData';
import { setInitialTranslates } from './utils';

const router = new Router();

router
  .get('/all-about-bible/dictionaries/', async (ctx, next) => {
    let initialStore = {};
    const { meta } = ctx.locals;

    let title = '';
    let description = '';

    try {
      const [dictionaries, words, metaData, translates] = await Promise.all([
        getDictionaries(),
        getDictionariesWords(),
        getMeta(ctx.url),
        getTranslates(),
      ]);

      const dictionary = R.pathOr([], ['data', 'dictionary'], dictionaries);

      const { title: titleMeta, description: descriptionMeta, keywords } = metaData.data.meta || {};

      title = titleMeta || '';
      description = descriptionMeta || '';

      meta.push(
        {
          name: 'description',
          content: description,
        },
        {
          name: 'keywords',
          content: keywords || '',
        },
      );

      initialStore = {
        dictonaries: {
          dictonaries: { dictionary },
          words: R.pathOr([], ['data'], words),
        },

        metaData: {
          metaContext: { dictionary },
          meta: metaData?.data?.meta,
          isLoadingMeta: false,
        },
        translates: setInitialTranslates(translates),
      };
    } catch (error) {
      console.error(error.message.red);
    }

    ctx.locals = { initialStore, meta, title, description };
    await next();
  })
  .get('/all-about-bible/dictionaries/:category/', async (ctx, next) => {
    let initialStore = {};
    const { meta } = ctx.locals;

    const { category } = ctx.params;

    let title = '';
    let description = '';

    try {
      const [dictionaries, metaData, translates] = await Promise.all([
        getDictionaries(),
        getMeta(ctx.url),
        getTranslates(),
      ]);
      const dictionary = R.pathOr([], ['data', 'dictionary'], dictionaries);

      const numberDictionary = dictionary.find(({ code }) => code === category);

      const { title: titleMeta, description: descriptionMeta, keywords } = metaData.data.meta || {};

      title = titleMeta || '';
      description = descriptionMeta || '';

      meta.push(
        {
          name: 'description',
          content: description,
        },
        {
          name: 'keywords',
          content: keywords || numberDictionary.title,
        },
      );

      const letter = numberDictionary.letters[0] || 'A';

      const words = await getDictionariesWords(numberDictionary.id, letter);
      // const metaWords = R.pathOr([], ['data'], words)?.dictionary;

      initialStore = {
        dictonaries: {
          dictonaries: { dictionary },
          words: R.pathOr([], ['data'], words),
          isDictonariesWordLoading: false,
          isDictonariesWordLoaded: true,
          isDictonariesWordFailed: false,
        },
        metaData: {
          // metaContext: { metaWords },
          metaContext: { numberDictionary },
          meta: metaData?.data?.meta,
          isLoadingMeta: false,
        },

        screen: { desktop: true },
        translates: setInitialTranslates(translates),
      };
    } catch (error) {
      title = undefined;
      initialStore = {
        dictonaries: {
          dictonaries: {},
          words: { dictionary: [], pages: { totalCount: 0 } },
          isDictonariesWordLoading: false,
          isDictonariesWordLoaded: false,
          isDictonariesWordFailed: true,
        },
        metaData: {
          meta: {
            title,
            description,
          },
          isLoadingMeta: false,
        },
      };
      console.error(error.message.red);
    }

    ctx.locals = { initialStore, meta, title, description };
    await next();
  })
  .get('/all-about-bible/dictionaries/:category/:id/', async (ctx, next) => {
    let initialStore = {};
    const { meta } = ctx.locals;
    const { category, id } = ctx.params;

    let title = '';
    let description = '';

    try {
      const [dictionaries, metaData, translates] = await Promise.all([
        getDictionaries(),
        getMeta(ctx.url),
        getTranslates(),
      ]);

      const dictionary = R.pathOr([], ['data', 'dictionary'], dictionaries);

      const numberDictionary = dictionary.find(({ code }) => code === category);

      const word = await getDictionaryDetail(id);

      const wordDetail = R.pathOr({ word }, ['data'], word);

      const currentWord = R.pathOr({ word }, ['word', 'word'], wordDetail);

      if (!wordDetail.word) {
        throw new Error('404');
      }

      const letter = wordDetail.letter || 'A';

      const words = await getDictionariesWords(numberDictionary.id, letter);

      const currentDictionary = numberDictionary?.title || '';
      // const {
      //   // title: titleMeta,
      //   // description: descriptionMeta,
      //   // h_one,
      //   // keywords,
      // } = metaData.data.meta || {};

      title = `${currentWord} - ${currentDictionary} БИБЛИЯ онлайн.`;
      description = `БИБЛИЯ онлайн - значение слова ${currentWord} в Библии - ${currentDictionary}`;

      const contentKeywords = `${currentDictionary}, ${currentWord}, библия, библия онлайн, Брокгауз, Библейская энциклопедия, библейский словарь, библейские термины.
      `;

      const wordDetailForMeta = wordDetail?.word;

      meta.push(
        {
          name: 'description',
          content: description,
        },
        {
          name: 'keywords',
          content: contentKeywords,
        },
      );

      initialStore = {
        dictonaries: {
          dictonaries: { dictionary },
          words: R.pathOr({}, ['data'], words),
          word: R.pathOr({ word }, ['data'], word),
          isMeaningWordLoading: false,
          isDictonariesWordLoading: false,
        },
        metaData: {
          metaContext: { numberDictionary, wordDetailForMeta },
          meta: metaData?.data?.meta,
          isLoadingMeta: false,
        },
        screen: { desktop: true },
        translates: setInitialTranslates(translates),
      };
    } catch (error) {
      title = undefined;
      initialStore = {
        dictonaries: {
          dictonaries: {},
          words: { dictionary: [], pages: { totalCount: 0 } },
          isDictonariesWordLoading: false,
          isDictonariesWordLoaded: false,
          isDictonariesWordFailed: true,
        },
        metaData: {
          meta: {
            title,
            description,
          },
          isLoadingMeta: false,
        },
      };
      console.error(error.message.red);
    }

    ctx.locals = { initialStore, meta, title, description };
    await next();
  });

export default router;
