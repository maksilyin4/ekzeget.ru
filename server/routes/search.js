import Router from 'koa-router';

const router = new Router();

// потом буду дорабатывать этот раздел
// const types = ['propovedi', 'interpretations', 'dictionaries', 'bible', 'mediateka', 'viktorina'];

const setSSR = ctx => {
  const { meta } = ctx.locals;

  const title = 'Результат поиска || Экзегет.ру';
  const description = 'Читать Библию, слушать Библию и смотреть видео по Библии на Экзегет.ру';

  meta.push({
    name: 'description',
    content: description,
  });

  ctx.locals = { meta, title };
};

router
  .get('/search/:type', async (ctx, next) => {
    setSSR(ctx);
    await next();
  })

  .get('/search/:type/:id', async (ctx, next) => {
    setSSR(ctx);
    await next();
  });

export default router;
