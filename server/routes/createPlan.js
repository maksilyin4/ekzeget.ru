import Router from 'koa-router';
import 'colors';
import R from 'ramda';
import { getTranslates, getMeta, getTestament } from '../libs/getData';
import { setInitialTranslates } from './utils';

const router = new Router();
router.get('/reading-plan/create-plan/', async (ctx, next) => {
  let initialStore = {};
  const { meta } = ctx.locals;
  let metaData = {};

  try {
    const [_meta, _translates, _testament] = await Promise.all([
      getMeta(ctx.url),
      getTranslates(),
      getTestament(),
    ]);

    metaData = R.pathOr({}, ['data', 'meta'], _meta);

    initialStore = {
      books: {
        isLoadingBookData: false,
        isLoadingBookInfo: false,
        testamentIsLoading: false,
        testament: {
          'testament-1': R.pathOr([], ['data', 'testament-1'], _testament),
          'testament-2': R.pathOr([], ['data', 'testament-2'], _testament),
        },
      },
      metaData: {
        metaContext: {},
        meta: metaData,
        isLoadingMeta: false,
      },
      translates: setInitialTranslates(_translates),
    };
  } catch (error) {
    console.error(error.message.red);
  }

  meta.push({
    name: 'description',
    content: '',
  });

  ctx.locals = {
    initialStore,
    meta,
    title: 'Создание плана чтений Библии',
  };
  await next();
});

export default router;
