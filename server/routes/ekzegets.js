import R from 'ramda';
import Router from 'koa-router';
import 'colors';
import {
  getExeget,
  getExegets,
  getExegetInterpritations,
  getTranslates,
  getMeta,
} from '../libs/getData';
import { setInitialTranslates } from './utils';

const router = new Router();

router
  .get('/all-about-bible/ekzegets/', async (ctx, next) => {
    let initialStore = {};
    const { meta } = ctx.locals;

    let title = '';
    let description = '';

    try {
      const [_ekzeget, metaData, _translates] = await Promise.all([
        getExegets(),
        getMeta(ctx.url),
        getTranslates(),
      ]);
      const { ekzeget, pages } = R.pathOr({}, ['data'], _ekzeget);

      const { title: titleMeta, description: descriptionMeta, keywords } = metaData.data.meta || {};

      title = titleMeta || '';
      description = descriptionMeta || '';

      meta.push(
        {
          name: 'description',
          content: description,
        },
        {
          name: 'keywords',
          content: keywords || '',
        },
      );

      const ekzegets = ekzeget;

      initialStore = {
        exegetes: { data: { ekzeget, pages }, isLoading: false, isFailed: false },
        screen: { desktop: true },
        translates: setInitialTranslates(_translates),
        metaData: {
          metaContext: { ekzegets },
          meta: metaData?.data?.meta,
          isLoadingMeta: false,
        },
      };
    } catch (error) {
      console.error(error.message.red);
    }

    ctx.locals = { initialStore, meta, title, description };
    await next();
  })
  .get('/all-about-bible/ekzegets/:route/', async (ctx, next) => {
    let initialStore = {};
    const { meta } = ctx.locals;
    const { route } = ctx.params;

    let title = '';
    let description = '';

    try {
      const [_ekzeget, _exegetInterpritations, _translates, metaData] = await Promise.all([
        getExeget(route, 1),
        getExegetInterpritations(route),
        getTranslates(),
        getMeta(ctx.url),
      ]);

      const { verse, pages } = R.pathOr({}, ['data'], _exegetInterpritations);
      const ekzeget = R.pathOr({}, ['data', 'ekzeget'], _ekzeget);

      // const {
      //    title: titleMeta,
      //    description: descriptionMeta,
      //   h_one,
      //    keywords,
      // } = metaData.data.meta || {};

      title = `Читать биографию - ${ekzeget.name}, православный лекторий.`;
      description = `${ekzeget.name} все толкования экзегета на Библию, представленные на сайте.`;
      const keywords = `${ekzeget.name}, житие, святой, биография, история Церкви, святые отцы, экзегеты, толкователи, богослов.`;

      meta.push(
        {
          name: 'description',
          content: description,
        },
        {
          name: 'keywords',
          content: keywords,
        },
      );

      initialStore = {
        exeget: { data: { ekzeget } },
        exegetInterpritations: { data: { verse, pages } },
        translates: setInitialTranslates(_translates),
        metaData: {
          metaContext: { ekzeget },
          meta: metaData?.data?.meta,
          isLoadingMeta: false,
        },
      };
    } catch (error) {
      title = undefined;

      initialStore = {
        exeget: { data: { ekzeget: {} }, error: { message: '404' } },
        exegetInterpritations: { data: { verse: [], pages: [] } },
        metaData: {
          meta: {
            title,
            description,
          },
          isLoadingMeta: false,
        },
      };
      console.error(error.message.red);
    }

    ctx.locals = { initialStore, meta, title, description };
    await next();
  });

export default router;
