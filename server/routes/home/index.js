import R from 'ramda';
import Router from 'koa-router';
import 'colors';
import dayjs from 'dayjs';
import ruLocale from 'dayjs/locale/ru';
import {
  getTestament,
  getReading,
  // getArticle,
  getInterpretations,
  getTranslates,
  getSlider,
  getMeta,
  getTextBlock,
} from '../../libs/getData';
import checkReading from './checkReading';
import titleToVerseInReading from './titleToVerseInReading';
import { setInitialTranslates } from '../utils';
import { metaHomeAtom } from '../../../source/pages/Home/page-head-meta/model';

const router = new Router();

const parseTestament = (testament = { books: [] }) => {
  const { books } = testament;

  return {
    books: books.map(({ id, code, short_title, menu, parts, title, testament, chapters }) => ({
      id,
      code,
      chapters,
      short_title,
      menu,
      parts,
      title,
      testament,
    })),
  };
};

const parseReading = (reading = []) => {
  if (reading) {
    return reading.map(({ title, info, verse }) => ({
      title,
      info,
      verse: verse.map(v => ({ id: v?.id, number: v?.number, text: v?.text })),
    }));
  }
  return '';
};

// const parseArticle = (article = []) => {
//   if (article) {
//     return article.map(({ id, updated_at, slug, body }) => ({
//       id,
//       slug,
//       updated_at,
//       body,
//     }));
//   }
//   return [];
// };

const parseInterpretations = (interpretations = []) => {
  if (interpretations) {
    return interpretations.map(({ id, date, author, code, verse, username }) => ({
      id,
      date,
      author,
      code,
      verse: {
        short: verse?.short,
        id: verse?.short,
        book_code: verse?.book_code,
        chapter: verse?.chapter,
        number: verse?.number,
      },
      username,
    }));
  }
  return [];
};

router.get('/', async (ctx, next) => {
  if (ctx.request.querystring && ctx.request.querystring !== 'auth-need') {
    ctx.redirect('/');
    return;
  }
  let initialStore = {};
  let directProps = {};
  const { meta, headers } = ctx.locals;
  let metaData = {};

  try {
    const [
      _testament,
      _reading,
      // _article,
      _interpretations,
      _sliders,
      _meta,
      _translates,
      _textBlock,
    ] = await Promise.all([
      getTestament(headers),
      getReading(headers),
      //  getArticle(headers),
      getInterpretations(headers),
      getSlider(),
      getMeta('main'),
      getTranslates(headers),
      getTextBlock('main'),
    ]);

    // TODO add to redux
    // const { interpretations, pages } = R.pathOr(_interpretations, 'data');
    const { gospel_reading, apostolic_reading, morning_reading, more_reading, ...other } = R.pathOr(
      [],
      ['data', 'reading'],
      _reading,
    );

    const [
      _gospel_reading,
      _apostolic_reading,
      _morning_reading,
      _more_reading,
    ] = await Promise.all([
      checkReading(gospel_reading),
      checkReading(apostolic_reading),
      checkReading(morning_reading),
      checkReading(more_reading),
    ]);

    metaData = R.pathOr({}, ['data', 'meta'], _meta);

    metaHomeAtom.setMetaHomeSSR.dispatch(metaData);

    initialStore = {
      books: {
        testamentIsLoading: false,
        testament: {
          'testament-1': parseTestament(R.pathOr([], ['data', 'testament-1'], _testament)),
          'testament-2': parseTestament(R.pathOr([], ['data', 'testament-2'], _testament)),
        },
      },
      lid: {
        data: {
          reading: {
            gospel_reading: parseReading(titleToVerseInReading(gospel_reading, _gospel_reading)),
            apostolic_reading: parseReading(
              titleToVerseInReading(apostolic_reading, _apostolic_reading),
            ),
            morning_reading: parseReading(titleToVerseInReading(morning_reading, _morning_reading)),
            more_reading: parseReading(titleToVerseInReading(more_reading, _more_reading)),
            ...other,
          },
        },
      },
      metaData: {
        metaContext: {},
        meta: metaData,
        isLoadingMeta: false,
      },
      translates: setInitialTranslates(_translates),
      text: _textBlock.text ? _textBlock.text.description : null,
    };

    directProps = {
      //   latestNewsList: parseArticle(R.pathOr([], ['data', 'article'], _article)),
      latestInterpretationsList: parseInterpretations(
        R.pathOr([], ['data', 'interpretations'], _interpretations).map(inter => ({
          id: inter.id,
          username: inter.added_by.username,
          date: dayjs
            .unix(inter.added_at)
            .locale('ru', ruLocale)
            .format('DD MMM YYYY'),
          verse: inter.verse[0],
          author: R.pathOr('', ['ekzeget', 'name'], inter),
          authorId: R.pathOr(0, ['ekzeget', 'id'], inter),
          investigated: inter.investigated,
          code: inter.ekzeget.code,
        })),
      ),
      latestInterpretationsTotal: R.pathOr(0, ['data', 'pages', 'totalCount'], _interpretations),
      latestSlider: R.pathOr([], ['data', 'sliders'], _sliders),
      latestMeta: metaData,
      text: _textBlock.text ? _textBlock.text.description : null,
    };
  } catch (error) {
    console.error(error);
  }

  const { title, description, keywords } = metaData;

  meta.push(
    {
      name: 'description',
      content: description,
    },
    {
      name: 'keywords',
      content: keywords,
    },
  );

  ctx.locals = {
    initialStore,
    meta,
    directProps,
    title,
  };
  await next();
});

export default router;
