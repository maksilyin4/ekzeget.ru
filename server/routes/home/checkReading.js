import parseLID from './parseLID';
import { getVerseInfo } from '../../libs/getData';
import emptyResolvePromise from '../../libs/emptyResolvePromise';

const checkReading = readingTitle => {
  if (readingTitle === '') return emptyResolvePromise();
  const lids = parseLID(readingTitle);

  return lids
    ? Promise.all(lids.map(verse_name => getVerseInfo(encodeURIComponent(verse_name))))
    : emptyResolvePromise();
};

export default checkReading;
