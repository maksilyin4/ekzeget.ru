const parseLID = stroke => {
  const regexp = /(\d\s)?[а-яА-Я]{2,5}\.\s\d{1,2}:\d{1,2}[^;]*/gim;
  return stroke.match(regexp);
};

export default parseLID;
