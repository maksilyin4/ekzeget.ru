import R from 'ramda';
import parseLID from './parseLID';

const titleToVerseInReading = (titles, readingsArray) => {
  if (!readingsArray) return '';

  const titleArray = parseLID(titles);
  return readingsArray.map((response, index) => {
    const title = titleArray[index];
    const verse = R.pathOr(null, ['data', 'verse'], response);

    return verse
      ? {
          title,
          verse,
          info: {
            link_title: title,
            book_code: verse[0].book.code,
            chapter: verse[0].chapter,
            verses: title.replace(/[а-яА-Я]{2,5}\.\s/gim, ''),
          },
        }
      : '';
  });
};

export default titleToVerseInReading;
