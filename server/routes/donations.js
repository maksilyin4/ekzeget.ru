import R from 'ramda';
import Router from 'koa-router';
import 'colors';
import { getTranslates, getMeta } from '../libs/getData';
import { setInitialTranslates } from './utils';

const router = new Router();

router.get('/pozhertvovaniya/', async (ctx, next) => {
  let initialStore = {};
  const { meta } = ctx.locals;
  let metaData = {};
  const canonical = 'https://ekzeget.ru/pozhertvovaniya/';

  try {
    const [_meta, _translates] = await Promise.all([getMeta(ctx.url), getTranslates()]);
    metaData = R.pathOr({}, ['data', 'meta'], _meta);

    initialStore = {
      metaData: {
        metaContext: {},
        meta: metaData,
        isLoadingMeta: false,
      },
      translates: setInitialTranslates(_translates),
    };
  } catch (error) {
    console.error(error.message.red);
  }

  const { title, description } = metaData;

  meta.push({
    name: 'description',
    content: description,
  });

  ctx.locals = {
    initialStore,
    meta,
    title,
    canonical,
  };
  await next();
});

export default router;
