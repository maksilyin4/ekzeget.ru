import Router from 'koa-router';
import 'colors';
import replaceStream from 'replacestream';
import request from 'request';
import { selfOrigin, manuscriptURL } from '../config';

const router = new Router();

router.get(`${manuscriptURL}/*`, async ctx => {
  const { method, url } = ctx;
  const origin = 'http://www.codex-sinaiticus.net';
  const host = 'www.codex-sinaiticus.net';
  const referer = ctx.get('referer').replace(new RegExp(selfOrigin), origin);

  const options = {
    uri: origin + url.replace(new RegExp(manuscriptURL), ''),
    method,
    headers: {
      Origin: origin,
      Referer: referer,
      Host: host,
    },
    timeout: 10000,
  };

  await new Promise((resolve, reject) => {
    const stream = request(options);

    if (ctx.req.headers.accept && ctx.req.headers.accept.startsWith('text/html')) {
      ctx.status = 200;
      stream
        .pipe(replaceStream(new RegExp(origin, 'g'), selfOrigin))
        .pipe(replaceStream(/(href|src)="\//g, `$1="${manuscriptURL}/`))
        .pipe(ctx.res)
        .on('finish', () => resolve())
        .on('error', error => reject(error));
    } else {
      stream
        .pipe(ctx.res)
        .on('finish', () => resolve())
        .on('error', error => reject(error));
    }
  });
});

export default router;
