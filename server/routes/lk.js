import Router from 'koa-router';
import R from 'ramda';
import 'colors';
import { getMeta } from '../libs/getData';

const router = new Router();

router
  .get('/lk/:id/', async (ctx, next) => {
    // let authUser = ctx.cookies.get('userId');
    if (typeof(ctx.cookies.get('userId'))==='undefined'){
      ctx.status = 403;
      const error = new Error("ACCESS DENIED PLEASE AUTHORIZE");
      ctx.message = "ACCESS DENIED PLEASE AUTHORIZE";
      ctx.body = "<div>Вы не авторзированы</div><div><a href='/?auth-need'>Войти</a></div>";
      /*const error = new Error("ACCESS DENIED PLEASE AUTHORIZE")
      error.code = "403"
      throw error;*/
      /*const exception = new Error();
      exception.name = "CustomError";

      exception.response = {
        status: 401,
        data: {
        detail: "This is a custom error",
        },
      };
      throw exception;*/
    }else{
      let initialStore = {};
      const { meta } = ctx.locals;
      let title = 'Личный кабинет на Экзегет ру';
  
      try {
        const [_meta] = await Promise.all([getMeta(ctx.url)]);
  
        let metaData = {};
        metaData = R.pathOr('', ['data', 'meta'], _meta);
  
        initialStore = {
          metaData: {
            metaContext: {},
            meta: metaData,
            isLoadingMeta: false,
          },
        };
      } catch (error) {
        console.error(error.message?.red);
      }
  
      if (ctx.params.id === 'personal') {
        title = 'Личный кабинет, персональные данные на Экзегет.ру';
      } else if (ctx.params.id === 'interpretations') {
        title = 'Личный кабинет, мои толкования на Экзегет.ру';
      } else if (ctx.params.id === 'favorites') {
        title = 'Личный кабинет, избранное на Экзегет.ру';
      } else if (ctx.params.id === 'bookmarks') {
        title = 'Личный кабинет, закладки на Экзегет.ру';
      } else if (ctx.params.id === 'reading-plan') {
        title = 'Личный кабинет, план чтений на Экзегет.ру';
      }
  
      meta.push({
        name: 'description',
        content: title,
      });
  
      ctx.locals = {
        initialStore,
        meta,
        title,
      };
  
      await next();
    }
    
  })
  .get('/lk/reading-plan/:id', async (ctx, next) => {
    if (typeof(ctx.cookies.get('userId'))==='undefined'){
      ctx.status = 403;
      const error = new Error("ACCESS DENIED PLEASE AUTHORIZE");
      ctx.message = "ACCESS DENIED PLEASE AUTHORIZE";
      ctx.body = "<div>Вы не авторзированы</div><div><a href='/?auth-need'>Войти</a></div>";
      /*const error = new Error("ACCESS DENIED PLEASE AUTHORIZE")
      error.code = "403"
      throw error;*/
      /*const exception = new Error();
      exception.name = "CustomError";

      exception.response = {
        status: 401,
        data: {
        detail: "This is a custom error",
        },
      };
      throw exception;*/
    }else{
    let initialStore = {};
    const { meta } = ctx.locals;
    const title = 'Личный кабинет план чтения на Экзегет ру';

    try {
      const [_meta] = await Promise.all([getMeta(ctx.url)]);

      let metaData = {};
      metaData = R.pathOr('', ['data', 'meta'], _meta);

      initialStore = {
        metaData: {
          metaContext: {},
          meta: metaData,
          isLoadingMeta: false,
        },
      };
    } catch (error) {
      console.error(error.message?.red);
    }

    meta.push({
      name: 'description',
      content: title,
    });

    ctx.locals = {
      initialStore,
      meta,
      title,
    };

    await next();
  }
  });

export default router;
