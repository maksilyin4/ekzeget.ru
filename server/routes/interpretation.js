import R from 'ramda';
import Router from 'koa-router';
import 'colors';
import {
  getOldTestament,
  getNewTestament,
  getTranslates,
  getChapter,
  getInterpretators,
  getInterpretation,
  getExeget,
  getVerse,
  getMeta,
} from '../libs/getData';
import { setInitialTranslates } from './utils';
import { fetchClient } from '../ssr/apolloSetupSSR';

const defaultLoadersStatuses = {
  isLoading: false,
  isLoaded: true,
  isFailed: false,
};

const defaultVerseLoadersStatuses = {
  isLoading: false,
  isLoaded: false,
  isFailed: false,
  linkToVerseLoading: true,
  linkToVerseLoaded: false,
  linkToVerseFailed: false,
  parallelLoading: true,
  data: {},
  linkToVerse: {},
  parallelData: {},
  error: null,
};

const router = new Router();

// легаси
// const setDefaultHeadline = (ekzeget, book, chapterNumber, verseNumber) => {
//   return `Толкование ${book.title} ${chapterNumber} глава ${verseNumber} стих - ${ekzeget.name ||
//     'Экзегет'}`;
// };
//
// const setDefaultTitle = (ekzeget, book, chapterNumber, verseNumber) => {
//   return `${book.title} ${chapterNumber} глава ${verseNumber} стих - ${ekzeget.name || 'Экзегет'}`;
// };
//
// const setDefaultDescription = verseText => `Толкование БИБЛИИ - ${verseText}`;
//
// const setDefaultKeywords = (ekzeget, book, chapterNumber, verseNumber) => {
//   return `${ekzeget.name || 'Экзегет'}, ${
//     book.title
//   } ${chapterNumber} глава ${verseNumber} стих, Толкование Библии, Святые отцы, Новый Завет, Ветхий Завет, Иисус Христос`;
// };

router.get('/bible/:code/:chapter/:verse/:interpretatorBible/', async (ctx, next) => {
  let initialStore = {};
  const { meta, headers } = ctx.locals;
  const { code, chapter, verse, interpretatorBible } = ctx.params;

  const client = fetchClient();
  const interpretator = interpretatorBible.includes('tolkovatel-')
    ? interpretatorBible.replace('tolkovatel-', '')
    : interpretatorBible;

  let directProps;
  let title = '';
  // легаси
  // let headline;
  // let description;
  // let keywords;

  try {
    const [
      _oldTestament,
      _newTestament,
      _chapter,
      _interpretators,
      _ekzeget,
      _interpretation, // TODO add to redux or directProps
      _translates,
      _verse,
      metaData,
    ] = await Promise.allSettled([
      getOldTestament(headers),
      getNewTestament(headers),
      getChapter(headers, code, chapter),
      getInterpretators(code, chapter),
      getExeget(interpretator),
      getInterpretation(code, chapter, verse, interpretator),
      getTranslates(),
      getVerse(code, chapter, verse),
      getMeta(ctx.url),
    ]);

    // легаси
    // const {
    //   title: titleMeta,
    //   description: descriptionMeta,
    //   h_one: headlineMeta,
    //   keywords: keywordsMeta,
    // } = metaData?.data?.meta || {};

    // if (Object.keys(_ekzeget?.data?.ekzeget).length === 0) {
    // eslint-disable-next-line no-throw-literal
    // throw '404';
    // }

    // const ekzeget = R.pathOr({}, ['data', 'ekzeget'], _ekzeget);

    const ekzeget = _interpretators?.value?.data?.ekzeget
      ? _interpretators?.value?.data?.ekzeget
      : [];
    const anotherEkzeget = _ekzeget?.value?.data?.ekzeget;

    // const book = R.pathOr({}, ['data', 'book'], _chapter);
    const book = _chapter?.value?.data?.book;

    // const interpretations = R.pathOr([], ['data', 'interpretations'], _interpretation);
    const interpretations = _interpretation?.value?.data?.interpretations;

    // const verseText = interpretations.map(i => i.verse.map(k => k.text).join(''));
    // const chapterNumber = chapter.replace('glava-', '');
    // const verseNumber = verse.replace('stih-', '');

    // title = titleMeta || setDefaultTitle(ekzeget, book, chapterNumber, verseNumber);
    // headline = headlineMeta || setDefaultHeadline(ekzeget, book, chapterNumber, verseNumber);
    // description = descriptionMeta || setDefaultDescription(verseText);
    // keywords = keywordsMeta || setDefaultKeywords(ekzeget, book, chapterNumber, verseNumber);

    // meta.push(
    //   {
    //     name: 'description',
    //     content: description,
    //   },
    //   {
    //     name: 'keywords',
    //     content: keywords,
    //   },
    // );

    const verseMeta = _verse?.value?.data?.verse;

    const storePages = _interpretators?.value?.data?.pages;

    // const oldTestament = R.pathOr([], ['data', 'books'], _oldTestament);
    // const oldTestament = _oldTestament;
    // const newTestament = R.pathOr([], ['data', 'books'], _newTestament);
    // const newTestament = _newTestament;

    initialStore = {
      books: {
        oldTestament: R.pathOr([], ['data', 'books'], _oldTestament),
        newTestament: R.pathOr([], ['data', 'books'], _newTestament),
      },
      exeget: { data: { anotherEkzeget } },
      chapter: { data: { book } },
      interpretators: {
        data: {
          // ekzeget: R.pathOr({}, ['data', 'ekzeget'], _interpretators),
          ekzeget: [...ekzeget],
          // pages: R.pathOr({}, ['data', 'pages'], _interpretators),
          pages: { storePages },
        },
        ...defaultLoadersStatuses,
      },
      verse: {
        ...defaultVerseLoadersStatuses,
        // легаси
        // data: R.pathOr({}, ['data'], _verse),
        data: { verseMeta },
      },
      interpretation: {
        ...defaultLoadersStatuses,
        data: _interpretation?.value?.data?.interpretations || [],
      },
      metaData: {
        metaContext: { tolkovatel: anotherEkzeget, verseMeta },
        meta: metaData?.value?.data?.meta,
        isLoadingMeta: false,
      },
      preachingBible: defaultLoadersStatuses,
      translates: setInitialTranslates(_translates),
    };

    directProps = { interpretations, isLoadingPage: true };
  } catch (error) {
    title = undefined;
    directProps = { isLoadingPage: false };
    initialStore.exeget = {
      error: { message: '404' },
      isLoading: false,
    };
    console.error('error', error.message?.red);
  }

  ctx.locals = {
    initialStore,
    meta,
    title,
    directProps,
    client,
  };
  await next();
});

export default router;
