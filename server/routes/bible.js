import R from 'ramda';
import Router from 'koa-router';
import emptyResolvePromise from '../libs/emptyResolvePromise';
import {
    getTestament,
    // getBookData,
    // getBookInfo,
    getChapter,
    getInterpretators,
    getVerse,
    getVerseLink,
    getPreaching,
    getChapterInterpretations,
    getExeget,
    getTranslates,
    getMeta,
    getInterpretation,
} from '../libs/getData';
import { setInitialTranslates } from './utils';
import { fetchClient } from '../ssr/apolloSetupSSR';

const defaultLoadersStatuses = {
    isLoading: false,
    isLoaded: true,
    isFailed: false,
    error: null,
};

const defaultVerseLoadersStatuses = {
    isLoading: false,
    isLoaded: true,
    isFailed: false,
    linkToVerseLoading: true,
    linkToVerseLoaded: false,
    linkToVerseFailed: false,
    error: null,
};

const router = new Router();

router
    .get('/bible', async (ctx) => {
        // TODO если не потребуется инфа для этого рута удалить 03.04.20
        ctx.status = 301;
        return ctx.redirect('/bible/evangelie-ot-matfea/glava-1/');
        // let initialStore = {};
        // const { meta, headers } = ctx.locals;
        // let metaData = {};

        // try {
        //   const [_testament, _meta, _translates] = await Promise.all([
        //     getTestament(headers),
        //     getMeta('bible'),
        //     getTranslates(),
        //   ]);

        //   metaData = R.pathOr('', ['data', 'meta'], _meta);

        //   initialStore = {
        //     books: {
        //       testamentIsLoading: false,
        //       testament: {
        //         'testament-1': R.pathOr([], ['data', 'testament-1'], _testament),
        //         'testament-2': R.pathOr([], ['data', 'testament-2'], _testament),
        //       },
        //     },
        //     translates: setInitialTranslates(_translates),
        //   };
        // } catch (error) {
        //   console.error(error.message?.red);
        // }

        // const { title = 'Библия онлайн || Экзегет.ру', description } = metaData;
        // meta.push({
        //   name: 'description',
        //   content: description,
        // });

        // ctx.locals = { initialStore, meta, title };
        // await next();
    })
    .get('/bible/:book_code', async (ctx) => {
        // let initialStore = {};
        // let title;
        // const { meta, headers } = ctx.locals;
        const { book_code } = ctx.params;
        ctx.status = 301;
        return ctx.redirect(`/bible/${book_code}/glava-1/`);

        // TODO если не потребуется инфа для этого рута удалить 03.04.20

        // try {
        //   const [_testament, _bookData, _bookInfo, _translates] = await Promise.all([
        //     getTestament(headers),
        //     getBookData(headers, book_code),
        //     getBookInfo(headers, book_code),
        //     getTranslates(),
        //   ]);

        //   const bookData = R.pathOr([], ['data', 'book'], _bookData);

        //   initialStore = {
        //     books: {
        //       testamentIsLoading: false,
        //       testament: {
        //         'testament-1': R.pathOr([], ['data', 'testament-1'], _testament),
        //         'testament-2': R.pathOr([], ['data', 'testament-2'], _testament),
        //       },
        //     },
        //     book: {
        //       bookData,
        //       bookInfo: R.pathOr([], ['data', 'descriptor'], _bookInfo),
        //     },
        //     translates: setInitialTranslates(_translates),
        //   };
        //   const testament = +bookData.testament_id === 2 ? 'Нового завета' : 'Ветхого завета';

        //   meta.push({
        //     name: 'description',
        //     content: `Толкование ${bookData.title} на сайте Экзегет. Читать онлайн главы Библии ${testament} в разных переводах.`,
        //   });

        //   title = `${bookData.title} - читать толкование ${testament} онлайн на портале Экзегет`;
        // } catch (error) {
        //   console.error(error.message.red);
        // }

        // ctx.locals = { initialStore, meta, title };
        // await next();
    })
    .get('/bible/:book_code/:chapter_num/', async (ctx, next) => {
        let initialStore = {};

        const { interpretator_id, investigator_id } = ctx.query;
        const ekzegetID = investigator_id || interpretator_id || null;
        let title = 'Экзегет.ру';
        const description = '';
        // const keywords = '';
        let canonical = '';
        const { meta, headers } = ctx.locals;
        const { book_code, chapter_num } = ctx.params;

        const client = fetchClient();

        try {
            const [
                _testament,
                _chapter,
                _interpretators,
                _interpretations,
                _exeget,
                _translates,
                metaData,
            ] = await Promise.all([
                getTestament(headers),
                getChapter(headers, book_code, chapter_num),
                getInterpretators(book_code, chapter_num),
                ekzegetID
                    ? getChapterInterpretations(
                          headers,
                          book_code,
                          chapter_num,
                          ekzegetID
                      )
                    : emptyResolvePromise(),
                ekzegetID ? getExeget(ekzegetID) : emptyResolvePromise(),
                getTranslates(),
                getMeta(ctx.url),
            ]);

            const book = R.pathOr({}, ['data', 'book'], _chapter);
            const ekzeget = R.pathOr({}, ['data', 'ekzeget'], _exeget);

            if (ctx.query.verse || ctx.query.tab) {
                canonical = `https://ekzeget.ru${ctx.url.split('?')[0]}`;
            }
            // const {
            //     title: titleMeta,
            //     description: descriptionMeta,
            //     keywords: keywordsMeta,
            // } = metaData.data.meta || {};

            // title = titleMeta || `11111111111111111111111`;
            // description =
            //     descriptionMeta || `БИБЛИЯ с толкованием,1111111111111`;
            // keywords = keywordsMeta || `1111111111111111`;

            // meta.push(
            //     {
            //         name: 'description',
            //         content: description,
            //     },
            //     {
            //         name: 'keywords',
            //         content: keywords,
            //     }
            // );

            initialStore = {
                books: {
                    isLoadingBookData: false,
                    isLoadingBookInfo: false,
                    testamentIsLoading: false,
                    testament: {
                        'testament-1': R.pathOr(
                            [],
                            ['data', 'testament-1'],
                            _testament
                        ),
                        'testament-2': R.pathOr(
                            [],
                            ['data', 'testament-2'],
                            _testament
                        ),
                    },
                },
                interpretators: {
                    isLoading: false,
                    isLoaded: true,
                    isFailed: false,
                    data: {
                        ekzeget: R.pathOr(
                            {},
                            ['data', 'ekzeget'],
                            _interpretators
                        ),
                        pages: R.pathOr({}, ['data', 'pages'], _interpretators),
                    },
                },
                chapterInterpretations: {
                    data: {
                        interpretations: R.pathOr(
                            [],
                            ['data', 'interpretations'],
                            _interpretations
                        ),
                        pages: R.pathOr(
                            [],
                            ['data', 'pages'],
                            _interpretations
                        ),
                    },
                    isLoading: false,
                    isLoaded: true,
                    isFailed: false,
                },
                chapter: {
                    data: { book: R.pathOr({}, ['data', 'book'], _chapter) },
                    isLoading: false,
                    isLoaded: true,
                    error: null,
                },
                metaData: {
                    metaContext: { book },
                    meta: metaData.data.meta,
                    isLoadingMeta: false,
                },

                exeget: ekzegetID
                    ? {
                          data: { ekzeget },
                          isLoading: true,
                          isLoaded: false,
                          error: null,
                      }
                    : {
                          data: {},
                          isLoading: true,
                          isLoaded: false,
                          error: null,
                      },
                translates: setInitialTranslates(_translates),
                screen: { desktop: true },
            };
        } catch (error) {
            title = undefined;

            initialStore.chapter = {
                data: {},
                error: { message: '404' },
                isLoading: false,
            };
            console.error(error.message.red, error);
        }

        ctx.locals = {
            initialStore,
            meta,
            title,
            description,
            canonical,
            client,
        };
        await next();
    })
    .get('/bible/:book_code/:chapter_num/:tolkovatel/', async (ctx, next) => {
        let initialStore = {};

        const { interpretator_id, investigator_id } = ctx.query;
        let ekzegetID = investigator_id || interpretator_id || null;
        let title = '';
        let description = '';
        let keywords = '';
        let canonical = '';
        const { meta, headers } = ctx.locals;
        const { book_code, chapter_num, tolkovatel } = ctx.params;
        const client = fetchClient();

        if (tolkovatel.includes('tolkovatel-')) {
            ekzegetID = tolkovatel.replace('tolkovatel-', '');

            try {
                const [
                    _testament,
                    _chapter,
                    _interpretators,
                    _interpretations,
                    _exeget,
                    _translates,
                    metaData,
                ] = await Promise.all([
                    getTestament(headers),
                    getChapter(headers, book_code, chapter_num),
                    getInterpretators(book_code, chapter_num),
                    ekzegetID
                        ? getChapterInterpretations(
                              headers,
                              book_code,
                              chapter_num,
                              ekzegetID
                          )
                        : emptyResolvePromise(),
                    ekzegetID ? getExeget(ekzegetID) : emptyResolvePromise(),
                    getTranslates(),
                    getMeta(ctx.url),
                ]);

                const {
                    title: titleMeta,
                    description: descriptionMeta,
                    keywords: keywordsMeta,
                } = metaData.data.meta || {};

                const book = R.pathOr({}, ['data', 'book'], _chapter);
                const ekzeget = R.pathOr({}, ['data', 'ekzeget'], _exeget);
                const chapterNumber = chapter_num.replace('glava-', '');

                if (Object.keys(_exeget.data.ekzeget).length === 0) {
                    throw new Error('404');
                }

                if (ctx.query.verse || ctx.query.tab) {
                    canonical = `https://ekzeget.ru${ctx.url.split('?')[0]}`;
                }

                title =
                    titleMeta ||
                    `${book.title} ${chapterNumber} глава - ${book.short_title} ${chapterNumber} | ${ekzeget.name}`;
                description =
                    descriptionMeta ||
                    `БИБЛИЯ с толкованием, ${ekzeget.name} - ${book.title} ${chapterNumber} глава. Читать, слушать и изучать️.`;
                keywords =
                    keywordsMeta ||
                    `${ekzeget.name}, ${book.title} ${chapterNumber} глава, ${book.short_title} ${chapterNumber}, Библия, Толкование Библии, Свящённое писание, Святые отцы, Новый Завет, Ветхий Завет, Иисус Христос`;

                meta.push(
                    {
                        name: 'description',
                        content: description,
                    },
                    {
                        name: 'keywords',
                        content: keywords,
                    }
                );

                initialStore = {
                    books: {
                        testamentIsLoading: false,
                        testament: {
                            'testament-1': R.pathOr(
                                [],
                                ['data', 'testament-1'],
                                _testament
                            ),
                            'testament-2': R.pathOr(
                                [],
                                ['data', 'testament-2'],
                                _testament
                            ),
                        },
                    },
                    interpretators: {
                        data: {
                            ekzeget: R.pathOr(
                                {},
                                ['data', 'ekzeget'],
                                _interpretators
                            ),
                            pages: R.pathOr(
                                {},
                                ['data', 'pages'],
                                _interpretators
                            ),
                        },
                    },
                    chapterInterpretations: {
                        data: {
                            interpretations: R.pathOr(
                                [],
                                ['data', 'interpretations'],
                                _interpretations
                            ),
                            pages: R.pathOr(
                                [],
                                ['data', 'pages'],
                                _interpretations
                            ),
                        },
                    },
                    chapter: {
                        data: {
                            book: R.pathOr({}, ['data', 'book'], _chapter),
                        },
                        isLoading: false,
                        isLoaded: true,
                    },
                    // metaData: {
                    //   meta: {
                    //     title,
                    //     description,
                    //     h_one: headline,
                    //     keywords,
                    //   },
                    //   isLoadingMeta: false,
                    // },
                    metaData: {
                        metaContext: { book, tolkovatel: ekzeget },
                        meta: metaData?.data?.meta,
                        isLoadingMeta: false,
                    },
                    exeget: ekzegetID
                        ? { data: { ekzeget } }
                        : { data: {}, isLoading: true, isLoaded: false },
                    translates: setInitialTranslates(_translates),
                };
            } catch (error) {
                title = undefined;

                initialStore.exeget = {
                    error: { message: '404' },
                    isLoading: false,
                };
                console.error(error.message, error);
            }
        }

        if (tolkovatel.includes('stih-')) {
            const verse_id = tolkovatel;

            try {
                const _verse = await getVerse(book_code, chapter_num, verse_id);
                const verse = R.pathOr({}, ['data', 'verse'], _verse);
                const verseID = verse.id;

                const [
                    _testament,
                    _chapter,
                    _verseLink,
                    _interpretators,
                    _interpretation,
                    _preaching,
                    _translates,
                    metaData,
                ] = await Promise.all([
                    getTestament(headers),
                    getChapter(headers, book_code, chapter_num),
                    getVerseLink(verseID),
                    getInterpretators(book_code, chapter_num),
                    getInterpretation(book_code, chapter_num, verse_id),
                    getPreaching(verseID),
                    getTranslates(),
                    getMeta(ctx.url),
                ]);

                const {
                    title: titleMeta,
                    description: descriptionMeta,
                    keywords: keywordsMeta,
                } = metaData.data.meta || {};

                /* eslint-disable object-curly-newline */
                const { apostolic, excuse, gospel, pages, preaching } =
                    R.pathOr({}, ['data'], _preaching);

                const chapterNumber = chapter_num.replace('glava-', '');
                const verseNumber = verse_id.replace('stih-', '');

                title =
                    titleMeta ||
                    `${verse.book.title} ${chapterNumber} глава ${verseNumber} стих - ${verse.book.short_title} ${chapterNumber}:${verseNumber} - Библия`;

                const description =
                    descriptionMeta+"1112" || `БИБЛИЯ ${verse.translate[0].text}`;

                const keywords =
                    keywordsMeta ||
                    `${verse.book.title} ${chapterNumber} глава ${verseNumber} стих, Иисус Христос, Библия, Священное Писание, Новый Завет, Ветхий Завет, Толкование Святых отцов, Видео, Мотиваторы, Библия в искусстве, Книги - Вопросы, Проповеди, Переводы`;

                /* eslint-enable */
                meta.push(
                    {
                        name: 'description',
                        content: description,
                    },
                    {
                        name: 'keywords',
                        content: keywords,
                    }
                );

                initialStore = {
                    books: {
                        testamentIsLoading: false,
                        testament: {
                            'testament-1': R.pathOr(
                                [],
                                ['data', 'testament-1'],
                                _testament
                            ),
                            'testament-2': R.pathOr(
                                [],
                                ['data', 'testament-2'],
                                _testament
                            ),
                        },
                    },
                    chapter: {
                        data: {
                            book: R.pathOr({}, ['data', 'book'], _chapter),
                        },
                    },
                    interpretators: {
                        data: {
                            ekzeget: R.pathOr(
                                {},
                                ['data', 'ekzeget'],
                                _interpretators
                            ),
                            pages: R.pathOr(
                                {},
                                ['data', 'pages'],
                                _interpretators
                            ),
                        },
                    },
                    interpretation: {
                        ...defaultLoadersStatuses,
                        data: _interpretation.data.interpretations || [],
                    },
                    verse: {
                        ...defaultVerseLoadersStatuses,
                        parallelLoading: false,
                        data: { verse },
                        linkToVerse: {
                            interpretation: R.pathOr(
                                {},
                                ['data', 'interpretation'],
                                _verseLink
                            ),
                        },
                    },
                    preaching: {
                        preaching: {
                            apostolic,
                            excuse,
                            gospel,
                            pages,
                            preaching,
                        },
                        preachingCelebrationIsLoading: true,
                        preachingExcusesIsLoading: true,
                        preacherIsLoading: true,
                        preachingIsLoading: true,
                    },
                    preachingBible: {
                        data: { apostolic, excuse, gospel, pages, preaching },
                    },
                    // metaData: {
                    //   meta: {
                    //     title,
                    //     description,
                    //     h_one: headline,
                    //     keywords,
                    //   },
                    //   isLoadingMeta: false,
                    // },

                    metaData: {
                        metaContext: {
                            book: verse.book,
                            chapterNumber,
                            verseNumber,
                            verseMeta: verse
                        },
                        meta: metaData?.data?.meta,
                        isLoadingMeta: false,
                    },
                    translates: setInitialTranslates(_translates),
                };
            } catch (error) {
                title = undefined;

                initialStore.exeget = {
                    error: { message: '404' },
                    isLoading: false,
                };
                console.error(error.message.red);
            }
        }

        if (
            !(
                tolkovatel.includes('stih-') ||
                tolkovatel.includes('tolkovatel-')
            )
        ) {
            title = undefined;

            initialStore.exeget = {
                error: { message: '404' },
                isLoading: false,
            };
        }
        ctx.locals = { initialStore, meta, title, canonical, client };
        await next();
    });

export default router;
