import Router from 'koa-router';

import 'colors';
import { getMeta } from '../libs/getData';

import { getCategories } from '../../source/apolloClient/query/mediaLibrary/categories';
import { getMedia } from '../../source/apolloClient/query/mediaLibrary/media';
import { getMediaDetail } from '../../source/apolloClient/query/mediaLibrary/mediaDetail';
import { relatedObjGql } from '../../source/apolloClient/query/mediaLibrary/related';
import { prefixForDetailCode } from '../../source/utils/common';
import { URL_APOLLO } from '../config';
import { fetchClient } from '../ssr/apolloSetupSSR';

export const urlApolloSSR =
  process.env.NODE_ENV === 'development' ? 'http://localhost:3000' : URL_APOLLO;

const typeVideo = 'V';

const getTitleDetailForVideo = (title = '', author = '') => {
  return `БИБЛИЯ онлайн: ${title}, ${author}`;
};

const getKeywordsDetailForVideo = (value = '') => {
  return `Библия, Библия онлайн, видео к Библии, лекции по Библии, смотреть Библию, видеоролики по Библии, православное видео, лекции библеистика, христианское видео, библейское видео ${value}, видео`;
};

const getDescriptionDetailForVideo = (title = '', author = '') => {
  return `БИБЛИЯ онлайн - смотреть видео толкование: ${title}, ${author}`;
};

const getSort = (sort = '') => sort.split(',').filter(el => el);
export const getArrSplitOrToNumber = arr => {
  if (arr) {
    const newArr = arr?.split(',');
    return newArr.map(el => +el);
  }
  return arr;
};

const setTypeValue = value => (isNaN(+value) ? value : +value);
const getValueForVariables = value => {
  return value ? setTypeValue(value) : undefined;
};

const setSearchQuery = url => {
  const query = url.split('?')[1] || '';
  const queryArr = query.split('&');
  const searchArr = queryArr.filter(el => !el.includes('cat='));
  const search = searchArr.join('&');
  return search ? `?${search}` : '';
};

const getCategory = async ({ categoryId, contentId, id, client }) => {
  const {
    data: { category: categories },
  } = await client.query({
    query: getCategories,
    variables: { id: id || categoryId },
  });

  const category = categories.find(({ code }) => code === categoryId);
  const subCategory = contentId && category.children.find(({ code }) => code === contentId);

  return [category, subCategory];
};

const checkCategory = async ({ ctx, categoryId, searchQuery }) => {
  ctx.status = 301;
  return ctx.redirect(`/mediateka/${categoryId}${searchQuery}`);
};

const router = new Router();

router
  .get('/mediateka/', async (ctx, next) => {
    const { meta } = ctx.locals;

    const client = fetchClient();

    const titleMeta = 'Медиатека на Экзегет.ру';
    const descriptionMeta =
      'Читать Библию, слушать Библию и смотреть видео по Библии на Экзегет.ру';
    const keywordsMeta =
      'Библия и толкование, видео к Библии, комментарии богословов и священников, смотреть видео, Библия отвечает, Евангелие, Новый завет, Ветхий завет';
    const h_oneMeta = 'Медиатека к Библии';

    const canonical = ctx.url.split('?')[0];

    try {
      const _meta = await getMeta(ctx.url);

      // if (data && data.meta) {
      //   const { title, description, keywords, h_one } = data.meta;
      //
      //   titleMeta = title;
      //   descriptionMeta = description;
      //   keywordsMeta = keywords;
      //   h_oneMeta = h_one;
      // }

      meta.push({
        name: 'h_one',
        content: h_oneMeta,
      });

      meta.push({
        name: 'title',
        content: titleMeta,
      });

      meta.push({
        name: 'description',
        content: descriptionMeta,
      });

      meta.push({
        name: 'keywords',
        content: keywordsMeta,
      });

      await client.query({
        query: getCategories,
        variables: {},
      });

      const { book, chapter, verse, author, sort, search, tags } = ctx.query || {};

      const connected_to_book_id = getValueForVariables(book);
      const connected_to_chapter_id = getValueForVariables(chapter);
      const connected_to_verse_id = getArrSplitOrToNumber(verse);
      const author_id = getValueForVariables(author);
      const search_query = getValueForVariables(search);

      const [orderBy, order] = getSort(sort);
      const tag_ids = getArrSplitOrToNumber(tags);

      await client.query({
        query: getMedia,
        variables: {
          offset: 0,
          limit: 15,
          author_id,
          category_code: '',
          connected_to_book_id,
          connected_to_chapter_id,
          connected_to_verse_id,
          order: +order || undefined,
          orderBy,
          search_query,
          tag_ids,
        },
      });

      ctx.locals = {
        initialStore: {
          screen: { desktop: true, tablet: false, phone: false },
          metaData: {
            metaContext: {},
            // meta: data?.meta,
            meta: _meta?.data?.meta,
            isLoadingMeta: false,
          },
        },
        meta,
        title: titleMeta,
        description: descriptionMeta,
        keywords: keywordsMeta,
        h_one: h_oneMeta,
        canonical,
        client,
      };
    } catch (error) {
      console.log('TCL: error', error);
    }

    await next();
  })
  .get('/mediateka/:categoryId/', async (ctx, next) => {
    // Ловит категорию или детальную страницу медиа
    const { meta } = ctx.locals;
    const { categoryId } = ctx.params;
    const canonical = ctx.url.split('?')[0];
    const client = fetchClient();

    const _meta = await getMeta(ctx.url);

    // const metaDataStore = await getMeta(ctx.url);

    if (categoryId.includes(prefixForDetailCode)) {
      try {
        const detailCode = categoryId.replace(prefixForDetailCode, '');

        const {
          data: { medias },
        } = await client.query({
          query: getMediaDetail,
          variables: { code: detailCode },
        });

        const { meta: metaData, type, author, related_media_types, id } =
          (medias && medias.items && medias.items[0]) || {};

        const mediasContext = medias?.items[0];

        const readyType = related_media_types.find(relatedMediaType =>
          relatedMediaType.includes('C'),
        );

        if (readyType) {
          try {
            await client.query({
              query: relatedObjGql[readyType],
              variables: {
                media_id: +id,
                type: readyType,
              },
            });
          } catch (error) {
            console.log('🚀 ~ file: mediateka.js ~ line 241 ~ .get ~ error', error);
          }
        }

        const { title, description, keywords } = metaData || {};

        const contentTitle =
          typeVideo === type ? getTitleDetailForVideo(title, author?.title) : title;
        const contentDescription =
          typeVideo === type ? getDescriptionDetailForVideo(title, author?.title) : description;

        meta.push(
          {
            name: 'description',
            content: contentDescription,
          },
          {
            name: 'keywords',
            content: keywords || (type === typeVideo && getKeywordsDetailForVideo(title)) || '',
          },
        );

        ctx.locals = {
          initialStore: {
            screen: { desktop: true, tablet: false, phone: false },
            metaData: {
              metaContext: { mediasContext },
              meta: _meta?.data?.meta,
              isLoadingMeta: false,
            },
            // metaData: {
            //   meta: {
            //     title: contentTitle,
            //     description: contentDescription,
            //     h_one: `${title} - ${author?.title || ''}`,
            //   },
            // },
          },
          meta,
          title: contentTitle,
          description: contentDescription,
          canonical,
          client,
        };
      } catch (error) {
        console.error('error', error);
      }
    } else {
      try {
        if (categoryId) {
          const [category] = await getCategory({ categoryId, client });

          const { title, description, keywords } = category.meta || {};

          await client.query({
            query: getCategories,
            variables: {},
          });

          const { book, chapter, verse, author, sort, search, tags } = ctx.query || {};

          const connected_to_book_id = getValueForVariables(book);
          const connected_to_chapter_id = getValueForVariables(chapter);
          const connected_to_verse_id = getArrSplitOrToNumber(verse);
          const author_id = getValueForVariables(author);
          const search_query = getValueForVariables(search);

          const [orderBy, order] = getSort(sort);
          const tag_ids = getArrSplitOrToNumber(tags);

          await client.query({
            query: getMedia,
            variables: {
              offset: 0,
              limit: 15,
              author_id,
              category_code: category ? category.code : '',
              connected_to_book_id,
              connected_to_chapter_id,
              connected_to_verse_id,
              order: +order || undefined,
              orderBy,
              search_query,
              tag_ids,
            },
          });

          meta.push(
            {
              name: 'description',
              content: description,
            },
            {
              name: 'keywords',
              content: keywords,
            },
          );

          ctx.locals = {
            initialStore: {
              screen: { desktop: true, tablet: false, phone: false },
              // metaData: { meta: { title, description, h_one, keywords } },
              metaData: {
                metaContext: { category },
                meta: _meta?.data?.meta,
                isLoadingMeta: false,
              },
            },
            meta,
            title:_meta?.data?.meta?_meta?.data?.meta:category.title,
            canonical,
            client,
          };
        }
      } catch (error) {
        console.error('error', error);
      }
    }
    await next();
  })
  .get('/mediateka/:categoryId/:contentId', async (ctx, next) => {
    // Дочерняя категория
    const { meta } = ctx.locals;
    const canonical = ctx.url.split('?')[0];
    const { categoryId, contentId } = ctx.params;
    const client = fetchClient();

    try {
      const searchQuery = setSearchQuery(ctx.url);
      const _meta = await getMeta(ctx.url);

      const [category, subCategory] = await getCategory({
        categoryId: categoryId.replace(prefixForDetailCode, ''),
        contentId,
        client,
      });

      if (category && !subCategory) {
        return checkCategory({ ctx, categoryId: contentId, searchQuery });
      }

      if (category) {
        const { book, chapter, verse, author, sort, search, tags } = ctx.query || {};

        const connected_to_book_id = getValueForVariables(book);
        const connected_to_chapter_id = getValueForVariables(chapter);
        const connected_to_verse_id = getArrSplitOrToNumber(verse);
        const author_id = getValueForVariables(author);
        const search_query = getValueForVariables(search);

        const [orderBy, order] = getSort(sort);
        const tag_ids = getArrSplitOrToNumber(tags);

        await client.query({
          query: getMedia,
          variables: {
            offset: 0,
            limit: 15,
            author_id,
            category_code: subCategory?.code,
            connected_to_book_id,
            connected_to_chapter_id,
            connected_to_verse_id,
            order: +order || undefined,
            orderBy,
            search_query,
            tag_ids,
          },
        });

        const metadata = subCategory?.meta;

        const { title, description, keywords } = metadata || {};

        meta.push({
          name: 'description',
          content: description,
        });

        meta.push({
          name: 'keywords',
          content: keywords,
        });

        ctx.locals = {
          initialStore: {
            screen: { desktop: true, tablet: false, phone: false },
            // metaData: {
            //   meta: metadata,
            // },
            metaData: {
              metaContext: { subCategory },
              meta: _meta?.data?.meta,
              isLoadingMeta: false,
            },
          },
          meta,
          title,
          description,
          canonical,
          client,
        };
      } else {
        try {
          const detailCode = categoryId.replace(prefixForDetailCode, '');

          const {
            data: { medias },
          } = await client.query({
            query: getMediaDetail,
            variables: { code: detailCode },
          });

          const { meta: metaData, type, author, related_media_types, id } =
            (medias && medias.items && medias.items[0]) || {};
          const { title, description, keywords } = metaData || {};

          const readyType = related_media_types.find(relatedMediaType =>
            relatedMediaType.includes(contentId === 'listen' ? 'A' : 'V'),
          );

          if (readyType) {
            try {
              await client.query({
                query: relatedObjGql[readyType],
                variables: {
                  media_id: +id,
                  type: readyType,
                },
              });
            } catch (error) {
              console.log('🚀 ~ file: mediateka.js ~ line 241 ~ .get ~ error', error);
            }
          }

          const contentTitle =
            typeVideo === type ? getTitleDetailForVideo(title, author?.title) : title;
          const contentDescription =
            typeVideo === type ? getDescriptionDetailForVideo(title, author?.title) : description;
          meta.push(
            {
              name: 'description',
              content: contentDescription,
            },
            {
              name: 'keywords',
              content: keywords || (type === typeVideo && getKeywordsDetailForVideo(title)) || '',
            },
          );

          const mediaContext = medias.items[0];

          // не нашел на сайте как попасть на страницу такого типа
          // console.log('mediaContext: ', mediaContext);

          ctx.locals = {
            initialStore: {
              screen: { desktop: true, tablet: false, phone: false },
              // metaData: {
              //   meta: {
              //     title: contentTitle,
              //     description: contentDescription,
              //     h_one: `${title} - ${author?.title || ''}`,
              //   },
              // },
              metaData: {
                metaContext: { mediaContext },
                meta: _meta?.data?.meta,
                isLoadingMeta: false,
              },
            },
            meta,
            title: contentTitle,
            description: contentDescription,
            canonical,
            client,
          };
        } catch (error) {
          console.error('error', error);
        }
      }
    } catch (error) {
      console.error('error', error);
    }

    await next();
  });

export default router;
