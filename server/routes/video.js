import Router from 'koa-router';
import 'colors';

const router = new Router();

const setRedirect = ctx => {
  ctx.status = 301;
  ctx.redirect('/mediateka/video/');
};

router
  .get('/video/', async ctx => {
    setRedirect(ctx);
  })
  .get('/video/:testament/', async ctx => {
    setRedirect(ctx);
  })
  .get('/video/:testament_id/:playlist_id/:book_id/', async ctx => {
    setRedirect(ctx);
  })
  .get('/video/:testament_id/:playlist_id/:book_id/:chapter/', async ctx => {
    setRedirect(ctx);
  });

export default router;
