import Router from 'koa-router';
import 'colors';
import R from 'ramda';
import { getMeta, getTranslates } from '../libs/getData';
import { setInitialTranslates } from './utils';

const router = new Router();

router.get('/contacts/', async (ctx, next) => {
  let initialStore = {};
  const { meta } = ctx.locals;
  const title = 'Контакты || Экзегет.ру';
  const canonical = `https://ekzeget.ru/contacts/`;

  let metaData = {};

  try {
    const [_translates, _meta] = await Promise.all([getTranslates(), getMeta(ctx.url)]);

    metaData = R.pathOr({}, ['data', 'meta'], _meta);

    initialStore = {
      metaData: {
        metaContext: {},
        meta: metaData,
        isLoadingMeta: false,
      },
      translates: setInitialTranslates(_translates),
    };

    meta.push({
      name: 'description',
      content: 'Контакты || Экзегет.ру Библейский портал',
    });
  } catch (error) {
    console.error(error.message.red);
  }

  ctx.locals = {
    initialStore,
    meta,
    title,
    canonical,
  };
  await next();
});

export default router;
