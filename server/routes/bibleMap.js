import R from 'ramda';
import Router from 'koa-router';
import 'colors';
import { getTranslates, getBibleMapMarkers, getTestament, getMeta } from '../libs/getData';
import { setInitialTranslates } from './utils';

const router = new Router();

router.get('/all-about-bible/bibleyskaya-karta/', async (ctx, next) => {
  let initialStore = {};
  const { meta, headers } = ctx.locals;

  let title = 'Библейские карты на портале Экзегет.ру';
  let description = '';

  try {
    const [_testament, _bibleMapMarkers, _translates, metaData] = await Promise.all([
      getTestament(headers),
      getBibleMapMarkers(),
      getTranslates(),
      getMeta(ctx.url),
    ]);

    const { title: titleMeta, description: descriptionMeta, keywords } = metaData.data.meta || {};

    title = titleMeta || '';
    description = descriptionMeta || '';

    meta.push(
      {
        name: 'description',
        content: description,
      },
      {
        name: 'keywords',
        content: keywords || '',
      },
    );

    const markersData = R.pathOr([], ['data', 'points'], _bibleMapMarkers);

    initialStore = {
      translates: setInitialTranslates(_translates),
      books: {
        testamentIsLoading: false,
        testament: {
          'testament-1': R.pathOr([], ['data', 'testament-1'], _testament),
          'testament-2': R.pathOr([], ['data', 'testament-2'], _testament),
        },
      },
      bibleMap: {
        markersData: R.pathOr([], ['data', 'points'], _bibleMapMarkers),
        isFetching: false,
      },
      metaData: {
        //   meta: {
        //     title,
        //     description,
        //     h_one,
        //     keywords,
        //   },
        //   isLoadingMeta: false,
        // },
        metaContext: { markersData },
        meta: metaData?.data?.meta,
        isLoadingMeta: false,
      },
    };
  } catch (error) {
    console.error(error.message.red);
  }

  ctx.locals = {
    initialStore,
    meta,
    title,
    description,
  };
  await next();
});

export default router;
