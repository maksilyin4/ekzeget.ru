import R from 'ramda';
import Router from 'koa-router';
import 'colors';
import { getTranslates, getMeta, getReadingPlans } from '../libs/getData';
import { setInitialTranslates } from './utils';

const router = new Router();

router.get('/reading-plan/', async (ctx, next) => {
  let initialStore = {};
  const { meta } = ctx.locals;
  let metaData = {};

  try {
    const [_meta, _translates, readingPlans] = await Promise.all([
      getMeta(ctx.url),
      getTranslates(),
      getReadingPlans(),
    ]);

    metaData = R.pathOr({}, ['data', 'meta'], _meta);

    initialStore = {
      metaData: {
        metaContext: {},
        meta: metaData,
        isLoadingMeta: false,
      },
      readingPlan: {
        readingPlansIsLoading: false,
        plans: readingPlans?.data?.plan || [],
      },

      translates: setInitialTranslates(_translates),
    };
  } catch (error) {
    console.error(error.message.red);
  }

  meta.push({
    name: 'description',
    content: '',
  });

  ctx.locals = {
    initialStore,
    meta,
    title: 'План чтений',
  };
  await next();
});

router.get('/reading-plan/:planId', async (ctx, next) => {
  let initialStore = {};
  const { meta } = ctx.locals;

  let metaData = {};

  try {
    const [_meta, _translates] = await Promise.all([getMeta(ctx.url), getTranslates()]);

    metaData = R.pathOr({}, ['data', 'meta'], _meta);

    initialStore = {
      metaData: {
        metaContext: {},
        meta: metaData,
        isLoadingMeta: false,
      },
      readingPlan: {
        readingPlansIsLoading: false,
      },
      translates: setInitialTranslates(_translates),
    };
  } catch (error) {
    console.error(error.message.red);
  }

  meta.push({
    name: 'description',
    content: '',
  });

  ctx.locals = {
    initialStore,
    meta,
    title: 'План чтений',
  };
  await next();
});

router.get('/reading-plan/monitoring/:planId', async (ctx, next) => {
  let initialStore = {};
  const { meta } = ctx.locals;

  let metaData = {};

  try {
    const [_meta, _translates] = await Promise.all([getMeta(ctx.url), getTranslates()]);

    metaData = R.pathOr({}, ['data', 'meta'], _meta);

    initialStore = {
      metaData: {
        metaContext: {},
        meta: metaData,
        isLoadingMeta: false,
      },
      readingPlan: {
        readingPlansIsLoading: false,
      },
      translates: setInitialTranslates(_translates),
    };
  } catch (error) {
    console.error(error.message.red);
  }

  meta.push({
    name: 'description',
    content: '',
  });

  ctx.locals = {
    initialStore,
    meta,
    title: 'Присоединение участников: План на 2023 год',
  };
  await next();
});

export default router;
