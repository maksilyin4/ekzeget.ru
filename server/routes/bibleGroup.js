import R from 'ramda';
import Router from 'koa-router';
import 'colors';
import { getNoteDetail, getTranslates, getGroup, getMeta, getNote } from '../libs/getData';
import { setInitialTranslates } from './utils';

const router = new Router();

const lib = {
  about: true,
  'for-leaders': true,
  video: true,
  search: true,
  'add-group': true,
};

router
  .get('/bible-group', async ctx => {
    ctx.redirect('/bible-group/about');
  })
  .get('/bible-group/about', async (ctx, next) => {
    let initialStore = {};
    const { meta } = ctx.locals;
    let metaData = {};

    try {
      const [_meta, _translates, _group] = await Promise.all([
        // getMeta('bible-group-about'),
        getMeta(ctx.url),
        getTranslates(),
        getGroup(4),
      ]);

      const group = R.pathOr({}, ['data', 'group'], _group);
      metaData = R.pathOr({}, ['data', 'meta'], _meta);

      initialStore = {
        translates: setInitialTranslates(_translates),
        bibleGroupUpdates: {
          isFetchingGroupUpdates: false,
          isFetchingGroupNotesUpdates: true,
          error: '',
          errorNotes: '',
          bibleGroups: group,
          bibleGroupsNotes: [],
        },
        metaData: {
          metaContext: {},
          meta: metaData,
          isLoadingMeta: false,
        },
      };
    } catch (error) {
      console.error(error.message.red);
    }

    const { title, description } = metaData;

    meta.push({
      name: 'description',
      content: description,
    });

    ctx.locals = { initialStore, meta, title };
    await next();
  })
  .get('/bible-group/for-leaders', async (ctx, next) => {
    let initialStore = {};
    const { meta } = ctx.locals;
    meta.push({
      name: 'description',
      content:
        'Православное изучение Писания в кругу единомышленников: найди или создай группу в своём родном городе!',
    });
    const title = 'Материалы для ведущих на Экзегет.ру';

    try {
      const [_translates, _note, _meta] = await Promise.all([
        getTranslates(),
        getNote(),
        getMeta(ctx.url),
      ]);

      let metaData = {};
      metaData = R.pathOr({}, ['data', 'meta'], _meta);
      const note = R.pathOr([], ['data', 'note'], _note);

      initialStore = {
        translates: setInitialTranslates(_translates),
        bibleGroupUpdates: {
          isFetchingGroupUpdates: true,
          isFetchingGroupNotesUpdates: false,
          error: '',
          errorNotes: '',
          bibleGroups: [],
          bibleGroupsNotes: note,
        },
        metaData: {
          metaContext: {},
          meta: metaData,
          isLoadingMeta: false,
        },
      };
    } catch (error) {
      console.error(error.message.red);
    }

    ctx.locals = { initialStore, meta, title };
    await next();
  })
  .get('/bible-group/for-leaders/:id', async (ctx, next) => {
    let initialStore = {};
    let title;
    const { meta } = ctx.locals;

    try {
      const [_note, _translates, _meta] = await Promise.all([
        getNoteDetail(ctx.params.id),
        getTranslates(),
        getMeta(ctx.url),
      ]);

      let metaData = {};
      metaData = R.pathOr({}, ['data', 'meta'], _meta);
      const noteDetail = R.pathOr({}, ['data', 'note'], _note);

      if (Number(ctx.params.id)) {
        ctx.status = 301;
        return ctx.redirect(`/bible-group/for-leaders/${noteDetail.code}/`);
      }

      initialStore = {
        translates: setInitialTranslates(_translates),
        bibleGroupUpdates: {
          isFetchingGroupUpdates: true,
          isFetchingGroupNotesUpdates: true,
          error: '',
          errorNotes: '',
          bibleGroups: [],
          bibleGroupsNotes: [],
          bibleGroupNoteInfo: noteDetail,
        },
        metaData: {
          metaContext: { noteDetail },
          meta: metaData,
          isLoadingMeta: false,
        },
      };
      meta.push({
        name: 'description',
        content: `Прочитать информацию на тему "${noteDetail.title}". Православный лекторий на официальном сайте Экзегет.`,
      });
      title = `${noteDetail.title} - раздел Лекторий на портале Экзегет`;
    } catch (error) {
      console.error(error.message.red);
    }

    ctx.locals = { initialStore, meta, title };
    await next();
  })
  .get('/bible-group/video', async (ctx, next) => {
    let initialStore = {};
    const { meta } = ctx.locals;
    meta.push({
      name: 'description',
      content:
        'Православное изучение Писания в кругу единомышленников: найди или создай группу в своём родном городе!',
    });
    const title = 'Видео Библейские группы || Экзегет.ру';

    try {
      const [_translates, _meta] = await Promise.all([getTranslates(), getMeta(ctx.url)]);

      let metaData = {};
      metaData = R.pathOr({}, ['data', 'meta'], _meta);

      initialStore = {
        translates: setInitialTranslates(_translates),
        metaData: {
          metaContext: {},
          meta: metaData,
          isLoadingMeta: false,
        },
      };
    } catch (error) {
      console.error(error.message.red);
    }

    ctx.locals = { initialStore, meta, title };
    await next();
  })
  .get('/bible-group/search', async (ctx, next) => {
    let initialStore = {};
    const { meta } = ctx.locals;
    meta.push({
      name: 'description',
      content:
        'Православное изучение Писания в кругу единомышленников: найди или создай группу в своём родном городе!',
    });
    const title = 'Поиск групп Библейские группы || Экзегет.ру';

    try {
      const [_meta, _group, _translates] = await Promise.all([
        getMeta(ctx.url),
        getGroup(0),
        getTranslates(),
      ]);
      const group = R.pathOr({}, ['data', 'group'], _group);

      initialStore = {
        translates: setInitialTranslates(_translates),
        bibleGroupUpdates: {
          isFetchingGroupUpdates: false,
          isFetchingGroupNotesUpdates: true,
          error: '',
          errorNotes: '',
          bibleGroups: group,
          bibleGroupsNotes: [],
        },
        metaData: {
          metaContext: {},
          meta: _meta?.data?.meta,
          isLoadingMeta: false,
        },
      };
    } catch (error) {
      console.error(error.message.red);
    }

    ctx.locals = { initialStore, meta, title };
    await next();
  })
  .get('/bible-group/add-group', async (ctx, next) => {
    let initialStore = {};
    const { meta } = ctx.locals;
    meta.push({
      name: 'description',
      content:
        'Православное изучение Писания в кругу единомышленников: найди или создай группу в своём родном городе!',
    });
    const title = 'Добавить группу Библейские группы || Экзегет.ру';

    try {
      const [_meta, _translates] = await Promise.all([getMeta(ctx.url), getTranslates()]);

      initialStore = {
        translates: setInitialTranslates(_translates),
        metaData: {
          metaContext: {},
          meta: _meta.data.meta,
          isLoadingMeta: false,
        },
      };
    } catch (error) {
      console.error(error.message.red);
    }

    ctx.locals = { initialStore, meta, title };
    await next();
  })
  .get('/bible-group/:id', async (ctx, next) => {
    if (lib[ctx.params.id]) {
      await next();
      return;
    }

    let initialStore = {};
    const { meta } = ctx.locals;
    let title = 'Поиск групп Библейские группы || Экзегет.ру';
    let currentGroup = 'Экзегет';

    try {
      const [_group, _translates, _meta] = await Promise.all([
        getGroup(0),
        getTranslates(),
        getMeta(ctx.url),
      ]);
      const group = R.pathOr({}, ['data', 'group'], _group);
      currentGroup =
        Object.keys(group).length &&
        group.find(i => {
          if (Number(ctx.params.id)) {
            return i.id === Number(ctx.params.id);
          }

          return i.code === ctx.params.id;
        });

      let metaData = {};
      metaData = R.pathOr({}, ['data', 'meta'], _meta);

      if (Number(ctx.params.id)) {
        ctx.status = 301;
        return ctx.redirect(`/bible-group/${currentGroup.code}/`);
      }

      initialStore = {
        translates: setInitialTranslates(_translates),
        bibleGroupUpdates: {
          isFetchingGroupUpdates: false,
          isFetchingGroupNotesUpdates: true,
          error: '',
          errorNotes: '',
          bibleGroups: group,
          bibleGroupsNotes: [],
        },
        metaData: {
          metaContext: { currentGroup },
          meta: metaData,
          isLoadingMeta: false,
        },
      };
    } catch (error) {
      console.error(error.message.red);
    }

    if (Object.keys(currentGroup).length) {
      title = `Библейская группа ${currentGroup.name} на Экзегет.`;
    }
    meta.push({
      name: 'description',
      content: `Библейская группа ${
        Object.keys(currentGroup).length ? currentGroup.name : 'Экзегет'
      }. Православный лекторий на официальном сайте Экзегет`,
    });

    ctx.locals = { initialStore, meta, title };
    await next();
  });

export default router;
