import R from 'ramda';
import Router from 'koa-router';
import {
  getTranslates,
  getLecture,
  getLectureDetail,
  getLectureGroup,
  getMeta,
} from '../libs/getData';
import { setInitialTranslates } from './utils';

const router = new Router();

router
  .get('/all-about-bible/about-bible/', async (ctx, next) => {
    let initialStore = {};
    const groupId = 'o-biblii';

    const { meta } = ctx.locals;

    let lectureDetails = {};

    let title = '';
    let description = '';

    try {
      const [lectureGroup, lectures, metaData, translates] = await Promise.all([
        getLectureGroup(),
        getLecture(groupId),
        getMeta(ctx.url),
        getTranslates(),
      ]);

      const categories = R.pathOr([], ['data', 'categories'], lectureGroup);
      const { lecture, pages } = R.pathOr({}, ['data'], lectures);

      const lectureGroupID = lecture[0].code;
      if (lectureGroupID) {
        lectureDetails = await getLectureDetail(lectureGroupID);
      }

      const lectureDetail = { lecture: R.pathOr({}, ['data', 'lecture'], lectureDetails) };

      const { title: titleMeta, description: descriptionMeta, keywords } = metaData.data.meta || {};

      title = titleMeta;
      description = descriptionMeta || '';

      meta.push(
        {
          name: 'description',
          content: description,
        },
        {
          name: 'keywords',
          content: keywords,
        },
      );

      initialStore = {
        lecture: {
          lectureGroup: { categories },
          lectureGroupList: { lecture, pages },
          getLectureDetailLoading: false,
          getLectureGroupLoading: false,
          getLectureGroupListLoading: false,
          lectureDetail,
        },
        metaData: {
          metaContext: { lectureDetail },
          meta: metaData?.data?.meta,
          isLoadingMeta: false,
        },
        screen: { desktop: true },
        translates: setInitialTranslates(translates),
      };
    } catch (error) {
      title = undefined;
      initialStore.lecture = {
        getLectureGroupListFailed: true,
      };
      console.error(error.message.red);
    }

    ctx.locals = { initialStore, meta, description, title };
    await next();
  })
  .get('/all-about-bible/about-bible/:groupId/', async (ctx, next) => {
    let initialStore = {};
    const { groupId } = ctx.params;

    const { meta } = ctx.locals;

    let lectureDetails = {};

    let title = '';
    let description = '';

    try {
      const [lectureGroup, lectures, metaData, translates] = await Promise.all([
        getLectureGroup(),
        getLecture(groupId),
        getMeta(ctx.url),
        getTranslates(),
      ]);

      const categories = R.pathOr([], ['data', 'categories'], lectureGroup);
      const { lecture, pages } = R.pathOr({}, ['data'], lectures);

      const lectureGroupID = lecture[0].code;
      if (lectureGroupID) {
        lectureDetails = await getLectureDetail(lectureGroupID);
      }

      const lectureDetail = { lecture: R.pathOr({}, ['data', 'lecture'], lectureDetails || {}) };

      const { title: titleMeta, description: descriptionMeta, keywords } = metaData.data.meta || {};

      title = titleMeta || '';
      description = descriptionMeta || '';

      meta.push(
        {
          name: 'description',
          content: description,
        },
        {
          name: 'keywords',
          content: keywords,
        },
      );

      initialStore = {
        lecture: {
          lectureGroup: { categories },
          lectureGroupList: { lecture, pages },
          getLectureDetailLoading: false,
          getLectureGroupLoading: false,
          getLectureGroupListLoading: false,
          lectureDetail,
        },
        metaData: {
          metaContext: { categories, lecture },
          meta: metaData?.data?.meta,
          isLoadingMeta: false,
        },

        screen: { desktop: true },
        translates: setInitialTranslates(translates),
      };
    } catch (error) {
      title = undefined;
      initialStore = {
        lecture: {
          lectureGroup: {},
          lectureGroupList: {},
          getLectureGroupListFailed: true,
        },
        metaData: {
          meta: {
            title,
            description,
          },
          isLoadingMeta: false,
        },
      };
      console.error(error.message.red);
    }

    ctx.locals = { initialStore, meta, description, title };
    await next();
  })

  .get('/all-about-bible/about-bible/:group_id/:lecture/', async (ctx, next) => {
    let initialStore = {};

    let lectureGroupID = ctx.params.group_id;
    const lectureID = ctx.params.lecture;

    const { meta } = ctx.locals;

    let title = '';
    let description = '';

    try {
      const lectureGroup = await getLectureGroup();
      const categories = R.pathOr([], ['data', 'categories'], lectureGroup);

      if (!lectureGroupID) {
        lectureGroupID = categories[0]?.code;
      }

      const [lectures, lectureDetails, metaData, translates] = await Promise.all([
        getLecture(lectureGroupID),
        getLectureDetail(lectureID),
        getMeta(ctx.url),
        getTranslates(),
      ]);

      const { lecture, pages } = R.pathOr({}, ['data'], lectures);
      const lectureDetail = { lecture: R.pathOr({}, ['data', 'lecture'], lectureDetails) };

      const { title: titleMeta, description: descriptionMeta, keywords } = metaData.data.meta || {};

      title = titleMeta || '';
      description = descriptionMeta || '';

      meta.push(
        {
          name: 'description',
          content: description,
        },
        {
          name: 'keywords',
          content: keywords,
        },
      );

      initialStore = {
        lecture: {
          lectureGroup: { categories },
          lectureGroupList: { lecture, pages },
          getLectureDetailLoading: false,
          getLectureGroupLoading: false,
          getLectureGroupListLoading: false,
          lectureDetail,
        },

        metaData: {
          metaContext: { categories, lectureDetail },
          meta: metaData?.data?.meta,
          isLoadingMeta: false,
        },

        screen: { desktop: true },
        translates: setInitialTranslates(translates),
      };

      meta.push(
        {
          name: 'description',
          content: description,
        },
        {
          name: 'keywords',
          content: keywords,
        },
      );
    } catch (error) {
      title = undefined;
      initialStore = {
        lecture: {
          lectureGroup: {},
          lectureGroupList: {},
          getLectureGroupListFailed: true,
        },
        metaData: {
          meta: {
            title,
            description,
          },
          isLoadingMeta: false,
        },
      };
      console.error(error.message.red);
    }

    ctx.locals = { initialStore, meta, title, description };
    await next();
  })

  .get('/all-about-bible/about-bible/easter/', async (ctx, next) => {
    let initialStore = {};
    const { meta } = ctx.locals;

    let title = '';
    let description = '';

    try {
      const [metaData, translates] = await Promise.all([getMeta(ctx.url), getTranslates()]);

      const { title: titleMeta, description: descriptionMeta, keywords } = metaData.data.meta || {};

      title = titleMeta || '';
      description = descriptionMeta || '';

      meta.push(
        {
          name: 'description',
          content: description,
        },
        {
          name: 'keywords',
          content: keywords,
        },
      );

      initialStore = {
        translates: setInitialTranslates(translates),
        metaData: {
          metaContext: {},
          meta: metaData?.data?.meta,
          isLoadingMeta: false,
        },
      };
    } catch (error) {
      console.error(error.message.red);
    }

    ctx.locals = { initialStore, meta, title, description };
    await next();
  });

export default router;
