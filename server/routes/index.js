import combineRouters from 'koa-combine-routers';
import home from './home';
import bible from './bible';
import ekzegets from './ekzegets';
import preaching from './preaching';
import dictonaries from './dictonaries';
import video from './video';
import lecture from './lecture';
import bibleGroup from './bibleGroup';
import interpretation from './interpretation';
import news from './news';
import aboutUs from './aboutUs';
import contacts from './contacts';
import donations from './donations';
import manuscript from './manuscript';
import search from './search';
import bibleMap from './bibleMap';
import mediateka from './mediateka';
import lk from './lk';
import quiz from './quiz';
import readingPlan from './reading-plan';
import createPlan from './createPlan';

import { ssrProd, ssrDev } from '../ssr';

const routers = [
  home,
  bible,
  ekzegets,
  preaching,
  dictonaries,
  createPlan,
  video,
  lecture,
  bibleGroup,
  interpretation,
  news,
  aboutUs,
  contacts,
  donations,
  manuscript,
  search,
  bibleMap,
  lk,
  quiz,
  mediateka,
  readingPlan,
  // DANGER!!! ssr forever last in this array!
  process.env.NODE_ENV === 'production' ? ssrProd : ssrDev,
];

export default combineRouters(...routers);
