import Koa from 'koa';
import koaLog from 'koa-log';
import favicon from 'koa-favicon';
import path from 'path';
import cookie from 'koa-cookie';
import send from 'koa-send';
import serve from 'koa-static';

import routes from './routes';
import timeoutPromise from './libs/timeoutPromise';
import 'colors';
import { bookArray, interpretationArray } from './config';

const cookiesMiddleware = require('universal-cookie-koa');

const app = new Koa();

if (process.env.NODE_ENV === 'development') {
  app.use(koaLog('dev'));
}

app.use(favicon(path.join(__dirname, '../source/assets/images/favicons/favicon.ico')));
app.use(cookie());
app.use(cookiesMiddleware());
app.use(serve(`${__dirname}/../build/client/frontassets/css`));
app.use(serve(`${__dirname}/../build/client`, { index: false }));

// TODO check device width on server
// app.use(userAgent);

app.use(async (ctx, next) => {
  try {
    const { translate = 'st_text' } = ctx.cookie || {};

    ctx.locals = {
      meta: [],
      headers: { Cookie: `translate=${translate}` },
    };
    await next();
  } catch (err) {
    console.error(err);
    ctx.status = err.status || 500;
    // ctx.body = NODE_ENV === 'development' ? err.message : 'Internal server error';
    ctx.body = err?.message;
  }
});

app.use(async (ctx, next) => {
  const url = ctx.url.split('/');
  if (ctx.url.includes('interpretation/') && ctx.url !== '/search/interpretation') {
    ctx.status = 301;
    const url = ctx.url.replace('interpretation', 'bible');
    ctx.redirect(url);
  } else if (url.length > 4 && url[1] === 'interpretation') {
    let isLegacy = false;

    // TODO FIND METHOD CHANGE
    for (let i = 0; i < bookArray.length; i++) {
      // Если нашли легаси код книги
      if (bookArray[i].legacy_code === url[2]) {
        isLegacy = true;
        url[2] = bookArray[i].code;
        break;
      }
    }

    if (!url[3].includes('glava-')) {
      url[3] = `glava-${url[3]}`;
      isLegacy = true;
    }
    if (!url[4].includes('stih-')) {
      url[4] = `stih-${url[4]}`;
      isLegacy = true;
    }

    for (let i = 0; i < interpretationArray.length; i++) {
      if (interpretationArray[i].id === url[5]) {
        url[5] = interpretationArray[i].code;
        isLegacy = true;
        break;
      }
    }

    if (isLegacy) {
      ctx.status = 301;
      ctx.redirect(url.join('/'));
    } else {
      await next();
    }
  } else if (ctx.url.includes('/bible-map')) {
    ctx.status = 301;
    ctx.redirect('/all-about-bible/bibleyskaya-karta/');
  } else if (ctx.url === '/bibleyskaya-karta/' || ctx.url === '/bibleyskaya-karta') {
    ctx.redirect('/all-about-bible/bibleyskaya-karta/');
  } else if (ctx.url.includes('/bible/') && !ctx.url.includes('?info') && url.length >= 3) {
    let newUrl = '/bible/';
    let isLegacy = false;
    let book = url[2];
    let chapter = url[3];
    let verse = url[4];

    if (book === 'bible') {
      book = url[3];
      chapter = url[4];
      verse = url[5];
    }

    // TODO FIND METHOD CHANGE
    for (let i = 0; i < bookArray.length; i++) {
      // Если нашли легаси код книги
      if (bookArray[i].legacy_code === book) {
        isLegacy = true;
        newUrl = `${newUrl}${bookArray[i].code}/`;
        break;
      }
    }

    // Если не нашли легаси у книги
    if (!isLegacy) {
      newUrl = `${newUrl}${book}/`;
    }

    // Если легаси код главы
    if (chapter && !chapter.includes('glava-') && !url[3].includes('book-')) {
      // В случае если цикл выдал не легаси код
      newUrl = `${newUrl}glava-${chapter}/`;
      isLegacy = true;
    } else if (chapter) {
      newUrl = `${newUrl}book-${chapter}/`;
    }

    // Аналогично как для главы, проверка стиха
    if (
      verse &&
      !verse.includes('?') &&
      !verse.includes('tolkovatel') &&
      !verse.includes('stih-')
    ) {
      newUrl = `${newUrl}stih-${verse}/`;
      isLegacy = true;
    } else if (verse && verse.includes('?interpretator_id=')) {
      // Если вместо Стиха окажется Толкователь
      newUrl = `${newUrl}${verse.replace('?interpretator_id=', '')}/`;
      isLegacy = true;
    } else if (verse) {
      newUrl = `${newUrl}${verse}/`;
    }

    // Чтобы не уйти в цикл используем флаг
    if (isLegacy) {
      ctx.status = 301;
      ctx.redirect(newUrl);
    } else {
      await next();
    }
  } else if (ctx.url.includes('preaching')) {
    ctx.status = 301;
    ctx.redirect('/all-about-bible/propovedi/');
  } else if (ctx.url === '/propovedi/' || ctx.url === '/propovedi') {
    ctx.status = 301;
    ctx.redirect('/all-about-bible/propovedi/');
  } else if (ctx.url.includes('donations')) {
    ctx.status = 301;
    ctx.redirect('/pozhertvovaniya/');
  } else if (ctx.url.includes('about-us')) {
    ctx.status = 301;
    ctx.redirect('/about/');
  } else if (ctx.url.includes('propovedi')) {
    const { url } = ctx;
    ctx.status = 301;
    if (!url.endsWith('/')) {
      ctx.redirect(`${url}/`);
    } else {
      await next();
    }
  } else if (ctx.url === '/dictonaries/' || ctx.url === '/dictonaries') {
    ctx.status = 301;
    ctx.redirect('/all-about-bible/dictionaries/');
  } else if (ctx.url === '/lecture/' || ctx.url === '/lecture') {
    ctx.status = 301;
    ctx.redirect('/all-about-bible/about-bible/o-biblii/bibliya/');
  } else if (ctx.url === '/ekzegets/' || ctx.url === '/ekzegets') {
    ctx.status = 301;
    ctx.redirect('/all-about-bible/ekzegets/');
  } else if (
    ctx.url === '/about/' ||
    ctx.url === '/about' ||
    ctx.url === '/o-proekte' ||
    ctx.url === '/o-proekte/'
  ) {
    ctx.status = 301;
    ctx.redirect('/o-proekte/about/');
  } else if (ctx.url.includes('news')) {
    ctx.status = 301;
    ctx.redirect('/novosti-i-obnovleniya/');
  } else if (ctx.url.includes('/sw.js') || ctx.url.includes('/precache-manifest')) {
    ctx.status = 200;
    await send(ctx, `/build/client${ctx.path}`);
  } else if (ctx.url.includes('workbox-')) {
    ctx.status = 200;
    await send(ctx, `/build/client${ctx.path}`);
  } else if (ctx.url.includes('/cmsmagazine')) {
    ctx.status = 301;
    await send(ctx, ctx.path);
  } else if (
    !ctx.url.includes('sw.js') &&
    !ctx.url.includes('cmsmagazine') &&
    !ctx.url.endsWith('/') &&
    !Object.keys(ctx.query).length
  ) {
    ctx.status = 301;
    ctx.redirect(`${ctx.url}/`);
  } else {
    await next();
  }
});

app.use(async (ctx, next) => {
  await timeoutPromise(next(), new Error(`Превышен таймаут ответа - ${ctx.url}`), 180000, 'reject');
});

app.use(routes());

module.exports = app;
