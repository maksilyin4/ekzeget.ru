/* eslint-disable import/no-extraneous-dependencies */
require('@babel/register')({
  presets: ['@babel/preset-env'],
});
require('core-js/stable');
require('colors');
const Loadable = require('react-loadable');
const http = require('http');
const chokidar = require('chokidar');
const clearRequire = require('clear-require');
const webpack = require('webpack');
const koaWebpack = require('koa-webpack');
const { PORT, HOST, NODE_ENV } = require('../config');

const config = require('../../config/webpack.config.ssr.development.js')();
const app = require('../index');

const compiler = webpack(config);

koaWebpack({ compiler }).then(middleware => {
  app.use(middleware);
});

let currentApp = app.callback();

const server = http.createServer(currentApp);

Loadable.preloadAll().then(() =>
  server.listen(PORT, HOST, () => {
    console.log(`SSR listening on port ${PORT} in ${NODE_ENV.yellow}`.blue);
  }),
);

const reloadServer = () => {
  server.removeListener('request', currentApp);
  clearRequire.all();
  currentApp = require('../index').callback(); // eslint-disable-line global-require
  server.on('request', currentApp);
  console.log('Server reloaded'.green);
};

compiler.plugin('done', () => {
  reloadServer();
});

chokidar
  .watch(['server', 'build/server'], { ignored: [/(^|[/\\])\../, /server\/bin/] })
  .on('change', path => {
    console.log('chokidar ::', path.yellow, '::', 'change'.green);
    reloadServer();
  });
