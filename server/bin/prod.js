/* eslint-disable import/no-extraneous-dependencies */
require('@babel/register')({
  presets: ['@babel/preset-env'],
});
require('core-js/stable');
require('colors');
const Loadable = require('react-loadable');
const server = require('../index');
const { PORT, HOST, NODE_ENV } = require('../config');

Loadable.preloadAll().then(() =>
  server.listen(PORT, HOST, () => {
    console.log(`API server listening on port ${PORT} in ${NODE_ENV.yellow}`.blue);
  }),
);
