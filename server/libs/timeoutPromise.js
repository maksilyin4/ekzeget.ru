/**
 * Wait for promise, on timeout call resolve || reject with response
 * @param {Promise} promise Waited Promise
 * @param {*} response Default timeout response
 * @param {Number} [delay=3000] Delay in ms
 * @param {String} [type=resolve] Type call on timeout resolve || reject
 */
const timeoutPromise = (promise, response, delay = 3000, type = 'resolve') =>
  Promise.race([
    promise,
    new Promise((resolve, reject) =>
      setTimeout(
        () => {
          if (type === 'resolve') resolve(response);
          if (type === 'reject') reject(response);
        },
        delay,
        response,
      ),
    ),
  ]);

export default timeoutPromise;
