const frontFixStore = store => {
  const {
    user, // Fix auth after page reload
    ...storeWithoutUser
  } = store;
  delete storeWithoutUser.translates.currentTranslate; // Fix currentTranslate after page reload
  return storeWithoutUser;
};

export default frontFixStore;
