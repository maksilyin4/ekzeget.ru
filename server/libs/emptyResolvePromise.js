const emptyResolvePromise = () => new Promise(resolve => resolve());

export default emptyResolvePromise;
