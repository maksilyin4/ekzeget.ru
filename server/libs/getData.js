import dayjs from 'dayjs';

import { _AXIOS } from '../../source/dist/ApiConfig';
import config from '../config';
import { DEFAULT_COUNT_QUESTIONS } from '../../source/dist/utils';

const { urlBase } = config;

export const metaDataDefault = { title: '', description: '' };

export const getTestament = (headers, perPage = 1000) =>
  _AXIOS({
    url: `${urlBase}/book/list/?per-page=${perPage}`,
    cookies: headers.Cookie,
  });

export const getOldTestament = (headers, perPage = 1000) =>
  _AXIOS({
    url: `${urlBase}/book/list/1/?per-page=${perPage}`,
    cookies: headers.Cookie,
  });
export const getNewTestament = (headers, perPage = 1000) =>
  _AXIOS({
    url: `${urlBase}/book/list/2/?per-page=${perPage}`,
    cookies: headers.Cookie,
  });
export const getBookData = (headers, book_code) =>
  _AXIOS({ url: `${urlBase}/book/info/${book_code}`, cookies: headers.Cookie });
export const getChapter = (headers, book_code, chapter_num) =>
  _AXIOS({
    url: `${urlBase}/book/info/${book_code}/${chapter_num}`,
    cookies: headers.Cookie,
  });
export const getBookInfo = (headers, book_code) =>
  _AXIOS({ url: `${urlBase}/book/description/${book_code}`, cookies: headers.Cookie });
export const getInterpretators = (book_code, chapter_num, perPage = 1000) =>
  _AXIOS({
    url: `${urlBase}/ekzeget/list-for-chapter/${book_code}/${chapter_num}/?per-page=${perPage}`,
  });
export const getVerse = (book_code, chapter_num, verse_id) =>
  _AXIOS({
    url: `${urlBase}/verse/detail-by-number/${book_code}/${chapter_num}/${verse_id}`,
  });
export const getVerseLink = verse_id =>
  _AXIOS({ url: `${urlBase}/interpretation/link/${verse_id}` });
export const getVerseInfo = (verse_name, perPage = 1000) =>
  _AXIOS({ url: `${urlBase}/verse/info/${verse_name}/?per-page=${perPage}` });
export const getTranslates = () => _AXIOS({ url: `${urlBase}/book/translate-list` });

export const getVerseParallel = verse_id => _AXIOS({ url: `/verse/parallel/${verse_id}/` });
export const getReading = (headers, perPage = 1000) =>
  _AXIOS({
    url: `${urlBase}/reading/${dayjs().format('DD.MM.YYYY')}/?per-page=${perPage}`,
    cookies: headers.Cookie,
  });
export const getArticle = (headers, articleType = 1, perPage = 8) =>
  _AXIOS({
    url: `${urlBase}/article/${articleType}/?per-page=${perPage}&img_sizes[]=news_image&img_sizes[]=news_image_mob`,
    cookies: headers.Cookie,
  });
export const getArticleDetail = type => _AXIOS({ url: `${urlBase}/article/detail/${type}` });
export const getInterpretations = (page = 1, perPage = 10, sort = 1) =>
  _AXIOS({
    url: `${urlBase}/interpretation/?sort=${sort}&page=${page}&per-page=${perPage}`,
  });
export const getChapterInterpretations = (headers, code, chapter, ekzeget, perPage = 1000) =>
  _AXIOS({
    url: `${urlBase}/interpretation/${code}/?chapter=${chapter}&interpretator_id=${ekzeget}&per-page=${perPage}`,
    cookies: headers.Cookie,
  });
export const getInterpretation = (code, chapter, verse, ekzeget) => {
  const interpretatorId = ekzeget ? { interpretator_id: ekzeget.replace('tolkovatel-', '') } : {};
  return _AXIOS({
    url: `${urlBase}/interpretation/${code}/?`,
    query: {
      chapter,
      number: verse,
      ...interpretatorId,
    },
  });
};
export const getPreaching = (preacher_id = '', excuse_id = '') =>
  _AXIOS({
    url: `${urlBase}/preaching/?preacher_id=${preacher_id}&excuse_id=${excuse_id}`,
  });
export const getPreachingExcuses = headers =>
  _AXIOS({ url: `${urlBase}/celebration/excuse`, cookies: headers.Cookie });
export const getPreacher = excuse_id =>
  _AXIOS({ url: `${urlBase}/preaching/preacher/?excuse_id=${excuse_id}` });
export const getPreachingCelebrations = (headers, perPage = 1000) =>
  _AXIOS({
    url: `${urlBase}/preaching/excuse/?per-page=${perPage}`,
    cookies: headers.Cookie,
  });
export const getDictionaries = () => _AXIOS({ url: `${urlBase}/dictionary/list/` });
export const getDictionariesWords = (dictionary = 1, letter = 'А', page = 1, perPage = 40) =>
  _AXIOS({
    url: encodeURI(
      `${urlBase}/dictionary/${dictionary}/${letter}/?page=${page}&per-page=${perPage}`,
    ),
  });

export const getDictionaryCode = id => _AXIOS({ url: `${urlBase}/dictionary/word/${id}/` });

export const getDictionaryDetail = code => _AXIOS({ url: `${urlBase}/dictionary/detail/${code}/` });
export const getExegets = (perPage = 1000) =>
  _AXIOS({ url: `${urlBase}/ekzeget/?per-page=${perPage}` });
export const getExeget = (id, info = 0) =>
  _AXIOS({ url: `${urlBase}/ekzeget/?info=${info}&id=${id}` });
export const getExegetInterpritations = (id, q = '', perPage = 20) =>
  _AXIOS({ url: `${urlBase}/ekzeget/verse-list/${id}/?q=${q}&per-page=${perPage}` });

export const getNote = () => _AXIOS({ url: `${urlBase}/bible-group/note` });
export const getNoteDetail = id => _AXIOS({ url: `${urlBase}/bible-group/note-detail/${id}` });
export const getGroup = (limit = 0) => _AXIOS({ url: `${urlBase}/bible-group/?limit=${limit}` });
export const getLectureGroup = () => _AXIOS({ url: `${urlBase}/lecture/category` });
export const getLecture = id => _AXIOS({ url: `${urlBase}/lecture/${id}` });
export const getLectureDetail = id => _AXIOS({ url: `${urlBase}/lecture/detail/${id}` });
export const getSearchResult = (url, query) =>
  _AXIOS({
    url: `${urlBase}/${url}/search${url === 'interpretation' ? '' : '/'}?q=${query}&page=1`,
  });
export const getSlider = () =>
  _AXIOS({
    url: `${urlBase}/slider`,
  });
export const getBibleMapMarkers = () => _AXIOS({ url: `${urlBase}/bible-maps?per-page=30000` });
export const getMeta = (code = '') => _AXIOS({ url: `${urlBase}/meta/get-by-code?code=${code}` });
export const getQuiz = (code = '') => _AXIOS({ url: `${urlBase}/queeze/question/${code}/` });
export const getAllQuestions = (page = 1, perPage = DEFAULT_COUNT_QUESTIONS) =>
  _AXIOS({ url: `${urlBase}/queeze/question-list?per-page=${perPage}&page=${page}` });

export const getMetaMediateka = data => {
  return _AXIOS({
    url: `${urlBase}/api/v2/graphql`,
    data: JSON.stringify(data),
  });
};

export const getReadingPlans = () => _AXIOS({ url: `${urlBase}/reading/plan/` });
export const getTextBlock = code => _AXIOS({ url: `${urlBase}/textBlock/${code}/` });