NVM := n exec auto

all: install

build: install

install:
	$(NVM) npm i
	$(NVM) npm run build

.PHONY: install
