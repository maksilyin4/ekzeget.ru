FROM ${CI_REGISTRY_IMAGE}/image:node

RUN apk update --no-cache
RUN apk add --no-cache bash gettext docker mariadb-client python3 build-base

# Install kubectl
RUN wget https://storage.googleapis.com/kubernetes-release/release/v1.18.5/bin/linux/amd64/kubectl -P /usr/local/bin -q && chmod +x /usr/local/bin/kubectl

WORKDIR /usr/src/app
