FROM ${CI_REGISTRY_IMAGE}/image:node
COPY build/ /usr/src/app/build
COPY package.json package-lock.json .babelrc /usr/src/app/
COPY server/ /usr/src/app/server
COPY source /usr/src/app/source
WORKDIR /usr/src/app/
RUN ln -s /usr/src/app/build/client/frontassets /usr/src/app/frontassets
RUN npm ci

CMD ["node", "server/bin/prod.js"]
