FROM node:14.21.3-alpine3.17

WORKDIR /app

COPY . .

RUN apk update  \
    && apk upgrade  \
    && apk add bash  \
    && chmod +x ./build.sh  \
    && npm install -g pm2  \
    && npm ci  \
    && npm run clean  \
    && npm run build

CMD ["pm2-runtime", "server/bin/prod.js"]
