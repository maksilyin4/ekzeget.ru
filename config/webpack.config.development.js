const Merge = require('webpack-merge');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const commonConfig = require('./webpack.config.common');
const ResourceHintWebpackPlugin = require('./webpack_plugins/resourse-hint-webpack-plugin');

const proxySSR = process.env.SSR ? { '*': 'http://localhost:3001' } : {};
const HWP =
  process.env.SSR === 'true'
    ? []
    : [
        new HtmlWebpackPlugin({
          filename: 'index.html',
          hash: true,
          inject: true,
          template: './source/index.html',
          preload: ['**/*.*'],
        }),
        new ResourceHintWebpackPlugin(),
        new ScriptExtHtmlWebpackPlugin({
          defaultAttribute: 'async',
        }),
      ];

module.exports = () =>
  Merge(commonConfig, {
    module: {
      rules: [
        {
          test: /\.s?css$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'style-loader',
            },
            {
              loader: 'css-loader',
              options: { sourceMap: true },
            },
            {
              loader: 'sass-loader',
              options: { sourceMap: true },
            },
          ],
        },
        {
          test: /\.css$/,
          loader: 'style-loader!css-loader',
          include: /node_modules/,
        },
      ],
    },
    devtool: 'inline-source-map',
    devServer: {
      historyApiFallback: {
        disableDotRule: true,
      },
      proxy: {
        '/api': {
          target: 'https://ekzeget.ru',
          //  target: 'http://localhost:4000',
          secure: false,
          changeOrigin: true,
        },
        '/site': {
          target: 'https://ekzeget.ru',
          secure: false,
          changeOrigin: true,
        },
        ...proxySSR,
      },
      progress: true,
    },
    plugins: [
      new BundleAnalyzerPlugin({
        analyzerMode: 'server',
        analyzerHost: '127.0.0.1',
        analyzerPort: 8888,
        openAnalyzer: false,
      }),
      // This fix Maximum call stack size exceeded
      // new webpack.HotModuleReplacementPlugin(),
      ...HWP,
    ],
  });
