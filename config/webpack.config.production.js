const Merge = require('webpack-merge');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const CompressionPlugin = require('compression-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const ResourceHintWebpackPlugin = require('./webpack_plugins/resourse-hint-webpack-plugin');
const GoogleAnalyticsPlugin = require('./webpack_plugins/google-analytics-webpack-plugin');
// const YandexMetricaPlugin = require('./webpack_plugins/yandex-metrika-webpack-plugin');
const  TagManagerPlugin= require('./webpack_plugins/tag-manager-webpack-plugin.js');
const RatingMailPlugin = require('./webpack_plugins/rating-mail-webpack-plugin');
const SocialPixelPlugin = require('./webpack_plugins/social-pixel-webpack-plugin');
const commonConfig = require('./webpack.config.common');
const { ga, tg, rm, pixel } = require('../server/config');

module.exports = folder =>
  Merge(commonConfig, {
    output: {
      filename: 'frontassets/js/app-min.[hash:10].js',
      path: path.join(__dirname, `../build-tmp/${folder}/client`),
      chunkFilename: 'frontassets/js/[name].chunk.[chunkhash:10].js',
      publicPath: '/',
    },
    module: {
      rules: [
        {
          test: /\.(sa|sc|c)ss$/,
          use: [MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'sass-loader'],
        },
      ],
    },
    plugins: [
      new webpack.HashedModuleIdsPlugin({
        hashFunction: 'md4',
        hashDigest: 'base64',
        hashDigestLength: 8,
      }),
      new MiniCssExtractPlugin({
        filename: 'frontassets/css/[name].[chunkhash:10].css',
        chunkFilename: 'frontassets/css/[id].[chunkhash:10].css',
      }),

      new OptimizeCssAssetsPlugin({
        assetNameRegExp: /\.css$/,
        cssProcessor: require('cssnano'),
        cssProcessorOptions: {
          zindex: false,
          discardComments: { removeAll: true },
        },
      }),
      new HtmlWebpackPlugin({
        filename: 'index.html',
        hash: true,
        inject: true,
        template: './source/index.html',
        preload: ['**/*.*'],
      }),
      // new ResourceHintWebpackPlugin(),
      new ScriptExtHtmlWebpackPlugin({
        defaultAttribute: 'async',
        preload: {
          test: /\.js$/
        }
      }),
      new CompressionPlugin({
        filename: '[path].gz[query]',
        algorithm: 'gzip',
        test: /\.js$|\.css$|\.html$|\.eot?.+$|\.ttf?.+$|\.woff?.+$|\.woff2?.+$|\.svg?.+$/,
        threshold: 10240,
        minRatio: 0.8,
        cache: true,
      }),

      new GoogleAnalyticsPlugin(ga),
      // new YandexMetricaPlugin(ym),
      new TagManagerPlugin(tg),
      new RatingMailPlugin(rm),
      new SocialPixelPlugin(pixel),
    ],
  });
