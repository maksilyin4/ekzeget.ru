const CODE = ID => `
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=${ID}"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', '${ID}');
</script>
`;

module.exports = class GoogleAnalyticsPlugin {
  constructor({ id }) {
    this.id = id;
  }

  apply(compiler) {
    compiler.hooks.compilation.tap('GoogleAnalyticsPlugin', compilation => {
      compilation.hooks.htmlWebpackPluginAfterHtmlProcessing.tap(
        'GoogleAnalyticsPlugin',
        ({ html }) => ({
          html: html.replace('</head>', `${CODE(this.id)}</head>`),
        }),
      );
    });
  }
};
