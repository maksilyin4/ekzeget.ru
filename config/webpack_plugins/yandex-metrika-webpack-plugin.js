const CODE = () => `
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(23728522, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true
   });
</script>
<noscript><img src="https://mc.yandex.ru/watch/23728522" style="position:absolute; left:-9999px;" alt="yandex" /></noscript>
<!-- /Yandex.Metrika counter -->
`;

module.exports = class YandexMetricaPlugin {
  constructor({ ...config }) {
    this.config = config;
  }

  apply(compiler) {
    compiler.hooks.compilation.tap('YandexMetricaPlugin', compilation => {
      compilation.hooks.htmlWebpackPluginAfterHtmlProcessing.tap(
        'YandexMetricaPlugin',
        ({ html }) => ({
          html: html.replace('</head>', `${CODE(this.config)}</head>`),
        }),
      );
    });
  }
};
