const CODE_HEAD = () => `
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P7LDJGNZ');</script>
<!-- End Google Tag Manager -->
`;
const CODE_BODY = () => `
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P7LDJGNZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
`;
module.exports = class TagManagerPlugin {
  constructor({ ...config }) {
    this.config = config;
  }

  apply(compiler) {
    compiler.hooks.compilation.tap('TagManagerPlugin', compilation => {
      compilation.hooks.htmlWebpackPluginAfterHtmlProcessing.tap(
        'TagManagerPlugin',
        ({ html }) => ({
          html: html.replace('</head>', `${CODE_HEAD(this.config)}</head>`),
        }),
      );
      compilation.hooks.htmlWebpackPluginAfterHtmlProcessing.tap(
        'TagManagerPlugin',
        ({ html }) => ({
          html: html.replace('<body>', `<body>${CODE_BODY(this.config)}`),
        }),
      );
    });
  }
};
