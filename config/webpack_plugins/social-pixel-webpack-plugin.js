const CODE = (fb, vk) => `
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '${fb}');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=${fb}&ev=PageView&noscript=1"
  alt="facebook"
/></noscript>
<!-- End Facebook Pixel Code -->
<!-- VKontakte Pixel Code -->
<script type="text/javascript">!function () { var t = document.createElement("script"); t.type = "text/javascript", t.async = !0, t.src = "https://vk.com/js/api/openapi.js?159", t.onload = function () { VK.Retargeting.Init("${vk}"), VK.Retargeting.Hit() }, document.head.appendChild(t) }();</script>
<noscript><img src="https://vk.com/rtrg?p=${vk}" style="position:fixed; left:-999px;" alt="vk" /></noscript>
<!-- End VKontakte Pixel Code -->
`;

module.exports = class GoogleAnalyticsPlugin {
  constructor({ id: { fb, vk } }) {
    this.fb = fb;
    this.vk = vk;
  }

  apply(compiler) {
    compiler.hooks.compilation.tap('SocialPixelPlugin', compilation => {
      compilation.hooks.htmlWebpackPluginAfterHtmlProcessing.tap(
        'SocialPixelPlugin',
        ({ html }) => ({
          html: html.replace('</head>', `${CODE(this.fb, this.vk)}</head>`),
        }),
      );
    });
  }
};
