const Merge = require('webpack-merge');
const WriteFilePlugin = require('write-file-webpack-plugin');
const path = require('path');
const commonSSRConfig = require('./webpack.config.ssr.common');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = () =>
  Merge(commonSSRConfig, {
    output: {
      path: path.resolve(__dirname, '../build-dev'),
    },
    watch: true,
    watchOptions: {
      ignored: [/node_modules/, /build/, /server/, /config/, /build-dev/],
    },
    mode: 'development',
    devtool: 'inline-source-map',

    module: {
      rules: [
        {
          test: /\.scss$/,
          use: [MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'sass-loader'],
        },
      ],
    },

    plugins: [
      new MiniCssExtractPlugin({
        filename: 'frontassets/css/[name].[chunkhash:10].css',
        chunkFilename: 'frontassets/css/[id].[chunkhash:10].css',
      }),
      new WriteFilePlugin({
        test: /ssr\.js$/,
      }),
    ],
  });
