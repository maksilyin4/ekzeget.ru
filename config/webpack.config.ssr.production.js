const Merge = require('webpack-merge');
const path = require('path');
const commonSSRConfig = require('./webpack.config.ssr.common');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = folder =>
  Merge(commonSSRConfig, {
    output: {
      path: path.resolve(__dirname, `../build-tmp/${folder}/server`),
    },
    mode: 'production',

    module: {
      rules: [
        {
          test: /\.scss$/,
          use: [MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'sass-loader'],
        },
      ],
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: 'frontassets/css/[name].[chunkhash:10].css',
        chunkFilename: 'frontassets/css/[id].[chunkhash:10].css',
      }),
    ],
  });
