const nodeExternals = require('webpack-node-externals');
const path = require('path');

module.exports = {
  entry: ['./source/app.ssr.js'],
  resolve: {
    extensions: ['.js', '.jsx', '.css', '.scss'],
    modules: ['node_modules'],
    alias: {
      src: path.resolve(__dirname, '../source'),
      Modules: path.resolve(__dirname, '../source/modules'),
      '~assets': path.resolve(__dirname, '../source/assets'),
      '~components': path.resolve(__dirname, '../source/components'),
      '~pages': path.resolve(__dirname, '../source/pages'),
      '~apollo': path.resolve(__dirname, '../source/apolloClient'),
      '~dist': path.resolve(__dirname, '../source/dist'),
      '~utils': path.resolve(__dirname, '../source/utils'),
      '~store': path.resolve(__dirname, '../source/store'),
      '~context': path.resolve(__dirname, '../source/context'),
    },
  },
  node: {
    console: false,
    global: false,
    process: true,
    Buffer: false,
    __filename: true,
    __dirname: true,
  },
  output: {
    filename: 'app.ssr.js',
    publicPath: '/',
    libraryTarget: 'commonjs2',
  },
  target: 'node',
  externals: nodeExternals(),
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: ['cache-loader', 'babel-loader'],
        exclude: /node_modules/,
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              emitFile: false,
              name: 'frontassets/images/[name].[ext]',
            },
          },
        ],
      },
      {
        test: /\.(s?css|ttf|eot|woff|woff2|otf)$/,
        exclude: /node_modules/,
        use: ['ignore-loader'],
      },
    ],
  },
};
