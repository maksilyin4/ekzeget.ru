const { GenerateSW } = require('workbox-webpack-plugin');

module.exports = [
  new GenerateSW({
    swDest: 'sw.js',
    clientsClaim: true,
    skipWaiting: true,
    runtimeCaching: [
      {
        urlPattern: /images/,
        handler: 'NetworkFirst',
        options: {
          cacheableResponse: { statuses: [0, 200] },
        },
      },
      {
        urlPattern: new RegExp('^https://fonts.(?:googleapis|gstatic).com/(.*)'),
        handler: 'CacheFirst',
        options: {
          cacheName: 'fonts-cache',
        },
      },
      {
        urlPattern: /\/api\/v1\/bible-maps/,
        handler: 'CacheFirst',
        options: {
          cacheableResponse: { statuses: [0, 200] },
        },
      },
      {
        urlPattern: /\/api\/v([0-9])/,
        handler: 'NetworkFirst',
        options: {
          cacheableResponse: { statuses: [0, 200] },
        },
      },
      {
        urlPattern: /\.css$/,
        handler: 'StaleWhileRevalidate',
        options: {
          cacheableResponse: { statuses: [0, 200] },
          cacheName: 'css-cache',
        },
      },
      {
        urlPattern: /\.(?:png|jpg|jpeg|svg)$/,
        handler: 'CacheFirst',
        options: {
          cacheableResponse: {
            statuses: [0, 200],
          },
          cacheName: 'img-assets',
          expiration: {
            maxEntries: 50,
            maxAgeSeconds: 7 * 24 * 60 * 60, // 7 days
          },
        },
      },
      {
        urlPattern: /.*/,
        handler: 'NetworkFirst',
        options: {
          cacheableResponse: { statuses: [0, 200] },
          networkTimeoutSeconds: 1000,
        },
      },
    ],
  }),
];
